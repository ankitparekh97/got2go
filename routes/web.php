<?php

use GuzzleHttp\Ring\Client\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
 */
Auth::routes();

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('google/callback', 'SocialAuthController@handleGoogleCallback');
Route::get('auth/rentaluser/google', 'SocialAuthController@rentalRedirectToGoogle');
Route::get('auth/tripperuser/google', 'SocialAuthController@rentalRedirectToGoogle');

Route::get('facebook/callback', 'SocialAuthController@handleFacebookCallback');
Route::get('auth/rentaluser/facebook', 'SocialAuthController@rentalRedirectToFacebook');
Route::get('auth/tripperuser/facebook', 'SocialAuthController@rentalRedirectToFacebook');

Route::get('/login/rentaluser', 'Auth\LoginController@showRentalLoginForm')->name('rentaluserloginform');
Route::get('/register/rentaluser', 'Auth\LoginController@showRentalRegisterForm')->name('rentalusersignupform');
Route::get('rentaluser', 'RentalUserController@index');
Route::get('register', 'Tripper\TripperController@registerForm')->name('tripper.registerForm');

Route::get('/admin', 'Auth\LoginController@showAdminLoginForm')->name('adminloginform');

//Super Admin Routes
Route::group(['namespace' => 'Admin','middleware' => ['jwtadmin.verify:admin','XssSanitizer']], function()
{
    Route::group(['prefix' => 'admin'], function()
      {
          Route::post('/transaction-logs', 'TransactionLogsController@transactionLogs')->name('transactionLogs');
          Route::get('/transaction-logs', [
            'uses' => 'TransactionLogsController@transactionLogs',
            'as' => 'transaction-list'
          ]);
         

          Route::post('/timeshare-list', 'TimeshareController@timeShareList')->name('timeShareList');
          Route::get('/timeshare-list', [
            'uses' => 'TimeshareController@timeShareList',
            'as' => 'timeshare-listing'
          ]);
          Route::get('/BulkTimeshareList', [
            'uses' => 'TimeshareController@BulkTimeshareList',
            'as' => 'BulkTimeshareList'
          ]);

          Route::get('/view_document/{type?}/{id?}', [
            'uses' => 'TimeshareController@view_document',
            'as' => 'view_document'
          ]);
          Route::post('/property-approve', 'TimeshareController@propertyApprove')->name('property-approve');
          Route::post('/property-deny', 'TimeshareController@propertyApprove')->name('property-deny');

          Route::post('/property-approve-all', 'TimeshareController@propertyApproveAll')->name('property-approve-all');
          Route::post('/property-deny-all', 'TimeshareController@propertyApproveAll')->name('property-deny-all');

          Route::post('/service-fee', 'ServiceFeeController@serviceFee')->name('serviceFee');
          Route::get('/service-fee', [
            'uses' => 'ServiceFeeController@serviceFee',
            'as' => 'servicefee-list'
          ]);
          Route::post('/servicefeesave', 'ServiceFeeController@serviceFeeSave')->name('servicefeesave');

          Route::post('/trippers-list', 'TripperController@tripperList')->name('tripperList');
          Route::get('/trippers-list', [
            'uses' => 'TripperController@tripperList',
            'as' => 'trippers-listing'
          ]);
          Route::post('/subscription-approve', 'TripperController@subscriptionApprove')->name('subscription-approve');
          Route::post('/subscription-cancel', 'TripperController@subscriptionCancel')->name('subscription-cancel');
          Route::post('/subscription-save', 'TripperController@subscriptionSave')->name('subscription-save');


          Route::post('/user-management', 'UserManagementController@userList')->name('userList');
          Route::get('/user-management', [
            'uses' => 'UserManagementController@userList',
            'as' => 'user-listing'
          ]);
          Route::post('/user-deactivate', 'UserManagementController@userActivate')->name('user-deactivate');
          Route::post('/property-list', 'UserManagementController@ownerProperty')->name('property-list');

          Route::get('/adminlogout', function () {
            Auth::guard('admin')->logout();
            Session::forget('rental_token');
            return response()->json(['success'=>'yes','redirect_url'=>url('/admin')]);
          })->name('adminlogout');

          Route::post('/kpi-dashboard', 'KpiDashboardController@chartList')->name('chartList');
          Route::get('/kpi-dashboard', [
            'uses' => 'KpiDashboardController@chartList',
            'as' => 'kpi-dashboard-listing'
          ]);
      });

});

Route::get('/list-your-stay', 'OwnerController@listYourStay')->name('owner.list-your-stay');
//Owner Panel Routes
//-------------------------------------------------------------------------------------
Route::group(['namespace' => 'OwnerPanel','prefix'=>'owner_panel','middleware' => ['jwtowner.verify:owner','XssSanitizer']], function()
{
    Route::get('/', 'OwnerController@index')->name('owner.home');

    Route::get('/property', 'PropertyController@index')->name('owner.property');
    Route::get('/property/add', 'PropertyController@create')->name('property.create');
    Route::get('/property/bulk-upload', 'PropertyController@bulkUploadForm')->name('property.bulkupload');
    Route::get('/property/bulk-upload/{unique_key}', 'PropertyController@bulkUploadForm')->name('property.re-bulkupload');

    Route::get('/property/edit/{property}', 'PropertyController@edit')->name('property.edit');
    Route::get('/property/view/{property}', 'PropertyController@show')->name('property.view');
    Route::get('/property/bulkUpload', 'PropertyController@bulkUpload');
    Route::get('/property/bulk-upload-review/{id}', 'PropertyController@bulkUploadReview')->name('bulkUploadReview');



    Route::get('/booking/list', 'BookingController@bookingList')->name('property.booking.list');
    Route::get('/booking/timeshare', 'BookingController@timesharelist')->name('timeshare.booking.list');
    Route::get('/settings', 'SettingsController@index')->name('owner.settings');

    //Owner Chat Routes
    Route::get('/chat', 'OwnerChatController@index')->name('owner.chat');
    Route::get('/load-latest-messages', 'OwnerChatController@getLoadLatestMessages')->name('chat.getmessages');
    Route::post('/send', 'OwnerChatController@postSendMessage')->name('chat.send');
    //Owner payment history Routes
    Route::get('/payment/list', 'PaymentHistoryController@historyList')->name('payment.list');

});

// Tripper and Rental Panel

Route::group(['middleware' => ['jwtrental.verify:rentaluser','XssSanitizer']], function()
{
    Route::get('/list-your-stay', 'OwnerController@listYourStay')->name('owner.list-your-stay');
    Route::get('/', 'HomeController@index')->name('rental.home');
    Route::get('view-stays', 'RentalPanel\RentalUserController@viewStays')->name('view-stays');
    Route::get('/search', 'HomeController@showsearch')->name('rental.search');
    Route::get('/propertydetail/{id}', 'PropertyDetailController@index')->name('propertydetails.index');
    Route::any('/review', 'PaymentController@index')->name('review.index');
    Route::group(['namespace' => 'Tripper'], function(){
        Route::get('tripper', 'TripperController@index')->name('tripper.index');
        Route::any('activation', 'TripperController@activation')->name('tripper.activation');
        Route::any('sign-up', 'TripperController@singUpForm')->name('tripper.singUpForm');
        Route::get('reg-success', 'TripperController@regSucess')->name('tripper.RegSucess');
        Route::any('dashboard', 'TripperController@dashboard')->name('tripper.dashboard');
    });

    Route::group(['namespace' => 'RentalPanel'], function()
      {
        //MYAccount Routes
        Route::get('/dashboard', 'RentalUserController@dashboard')->name('rental.dashboard');
        Route::get('/setting', 'RentalUserController@setting')->name('rental.setting');
        Route::get('/mystay', 'RentalUserController@mystay')->name('rental.mystay');
        Route::get('/mystay-details/{id}/{bid}', 'RentalUserController@mystayDetails')->name('rental.mystay-details');
        Route::get('/notifications', 'RentalUserController@myNotifications')->name('rental.notifications');
        Route::get('/', 'RentalUserController@index')->name('rental.home');

        //Tripboards Routes
        Route::get('tripboards', 'TripBoardController@tripboards')->name('tripboards');
        Route::get('tripboard-detail/{id}', 'TripBoardController@tripboarddetail')->name('tripboard.detail');
        Route::post('/tripboard/chatsend', 'TripBoardController@postSendMessage')->name('tripboard.chat.send');

        //Rental Or Tripper Chat Routes
        Route::get('/chat', 'ChatController@index')->name('rental.chat');
        Route::get('/load-latest-messages', 'ChatController@getLoadLatestMessages')->name('rental.chat.getmessages');
        Route::post('/send', 'ChatController@postSendMessage')->name('rental.chat.send');

        //Favriote tripboard
        Route::get('/favorites', 'FavoriteController@favorites')->name('favorites');
      });


});

Route::get('/ScriptToUpdateLatLongOfProperties', 'HomeController@ScriptToUpdateLatLongOfProperties');
Route::get('/ScriptToUpdateCommunicationPreference', 'HomeController@ScriptToUpdateCommunicationPreference');
Route::get('/privacy_policy', 'HomeController@privacy_policy')->name('privacy_policy');
Route::get('/terms_and_conditions', 'HomeController@terms_and_conditions')->name('terms_and_conditions');


Route::group([
    'as'         => 'web.',
], function(){
    Helper::includeRouteFiles(__DIR__.'/web/');
});
