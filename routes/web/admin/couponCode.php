<?php

use App\Http\Controllers\Admin\CouponCodeController;

Route::group([
    'middleware' => ['jwtadmin.verify:admin'],
    'as' => 'admin.',
    'prefix' => 'admin'
], function(){
    Route::get('coupon-codes', [CouponCodeController::class,'index'])->name('couponCodes');
});
