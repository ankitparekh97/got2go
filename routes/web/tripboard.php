<?php

use App\Http\Controllers\Tripper\TripperController;

Route::group([
    'middleware' => ['jwtrental.verify:rentaluser'],
    'as' => 'tripper.'
], function(){
    Route::get('my-offers', [TripperController::class,'myOffers'])->name('myOffers');
});
