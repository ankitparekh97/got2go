<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/authenticateOtp', 'Auth\RegisterController@authenticateOtp')->name('authenticateOtp');
Route::get('/resendOtp', 'Auth\RegisterController@resendOwnerOtp')->name('resendOtp');
Route::post('/forgotRenterTripperPassword', 'Auth\LoginController@forgotRenterTripperPassword')->name('forgotRenterTripperPassword');

Route::post('/login/rentaluser', 'Auth\LoginController@rentalLogin')->name('rentaluserlogin');
Route::post('/login/user', 'Auth\LoginController@userLogin')->name('userlogin');
Route::post('/register/rentaluser', 'Auth\RegisterController@createRental')->name('rentalusersignup');
Route::post('/register/user', 'Auth\RegisterController@createUser')->name('usersignup');

Route::post('/register/createRentalTripperUser', 'Auth\RegisterController@createRentalTripperUser')->name('createRentalTripperUser')->middleware('jwtrental.verify:rentaluser');

Route::post('/login/admin', 'Auth\LoginController@adminLogin')->name('adminlogin');


Route::post('/admin_forgot_password', 'Auth\LoginController@forgotAdminPassword')->name('forgotpassword');
Route::post('/propertyInstantBooking', 'PropertyDetailController@propertyCheckBooking')->name('propertydetails.propertyInstantBooking')->middleware('jwtrental.verify:rentaluser');
Route::post('/checkBooking', 'PropertyDetailController@checkBooking')->name('propertydetails.checkBooking')->middleware('jwtrental.verify:rentaluser');

Route::get('/logout','Auth\LoginController@userLogout' )->name('user.logout');

Route::get('/admin', 'Auth\LoginController@showAdminLoginForm')->name('adminloginform');
//Forgot and reset password of Admin panel
Route::get('/admin_forgot_password', 'Auth\LoginController@forgotAdminPassword')->name('admin_forgot_password');

//Owner Panel Routes
//-------------------------------------------------------------------------------------
Route::group(['namespace' => 'OwnerPanel','prefix'=>'owner_panel','middleware' => ['jwtowner.verify:owner','XssSanitizer']], function()
{
    Route::post('/property', 'PropertyController@index')->name('property.list');
    Route::post('/property/store', 'PropertyController@store')->name('property.store');
    Route::post('/property/edit/{id}', 'PropertyController@uploadmedia')->name('property.edit.get');
    Route::post('/property/upload', 'PropertyController@uploadmedia')->name('property.upload');
    Route::delete('/property/destroy', 'PropertyController@destroy')->name('property.destroy');

    Route::post('/update/booking/ownerstatus/', 'BookingController@updateOwnerStatus')->name('owner.bookingstatus');
    Route::post('/update/booking/propertystatus/', 'BookingController@updatePropertyStatus')->name('property.bookingstatus');
    Route::post('/property/booking', 'BookingController@bookingList')->name('property.bookingList');

    Route::post('/calculateownerrefund', 'BookingController@calculateRefund')->name('calculate.owner.refund');


    Route::post('/propertyBooking', 'BookingController@propertyBooking')->name('property.propertyBooking');
    Route::post('/confirmedPropertyBooking', 'BookingController@confirmedPropertyBooking')->name('property.confirmedPropertyBooking');
    Route::post('/publishProperty', 'PropertyController@publish')->name('property.publishProperty');
    Route::post('/pendingListings', 'PropertyController@pendingListings')->name('property.pendingListings');
    Route::post('/currentListings', 'PropertyController@currentListings')->name('property.currentListings');

    Route::post('/chatsearch', 'OwnerChatController@getData')->name('owner.chatsearch');
    Route::post('/unread', 'OwnerChatController@unreadFlag')->name('owner.unread.chat');
    Route::post('/paymentHistory', 'PaymentHistoryController@bookingHistory')->name('historyList');
    Route::post('/paymentMethod', 'PaymentHistoryController@paymentMethod')->name('owner.paymentMethods');
    Route::post('/paymentMethodCredit', 'PaymentHistoryController@paymentMethodCredit')->name('owner.paymentMethodCredit');
    Route::post('/deletePayment', 'PaymentHistoryController@deletePayment')->name('deletePayment');
    Route::post('/saveCard', 'PaymentHistoryController@saveCard')->name('saveCard');
    Route::post('/editCard', 'PaymentHistoryController@editCard')->name('editCard');
    Route::post('/editBankDetails', 'PaymentHistoryController@editBankDetails')->name('editBankDetails');
    Route::post('/saveBank', 'PaymentHistoryController@saveBank')->name('owner.saveBank');

    Route::any('/property/bulkDownload', 'PropertyController@bulkDownload')->name('bulkDownload');
    Route::post('/property/bulkUploadSubmit', 'PropertyController@bulkUploadSubmit')->name('bulkUploadSubmit');
    Route::any('/property/bulkUploadListing', 'PropertyController@bulkUploadListing')->name('bulkUploadListing');

});


//Super Admin Routes
Route::group(['namespace' => 'Admin','middleware' => ['jwtadmin.verify:admin','XssSanitizer']], function()
{

    
    Route::post('/updateOwnerStatus', 'UserOwnerController@updateStatus')->name('updateOwnerStatus');

    Route::group(['prefix' => 'admin'], function()
    {
        Route::post('/transaction-logs', 'TransactionLogsController@transactionLogs')->name('transactionLogs');
        Route::get('/transaction-logs', [
          'uses' => 'TransactionLogsController@transactionLogs',
          'as' => 'transaction-list'
        ]);

        Route::post('/refundBooking', 'TransactionLogsController@refundBooking')->name('refundBooking');

        Route::post('/timeshare-list', 'TimeshareController@timeShareList')->name('timeShareList');
        Route::get('/timeshare-list', [
          'uses' => 'TimeshareController@timeShareList',
          'as' => 'timeshare-listing'
        ]);
        Route::post('/property-approve', 'TimeshareController@propertyApprove')->name('property-approve');
        Route::post('/property-deny', 'TimeshareController@propertyApprove')->name('property-deny');

        Route::post('/service-fee', 'ServiceFeeController@serviceFee')->name('serviceFee');
        Route::get('/service-fee', [
          'uses' => 'ServiceFeeController@serviceFee',
          'as' => 'servicefee-list'
        ]);
        Route::post('/servicefeesave', 'ServiceFeeController@serviceFeeSave')->name('servicefeesave');

        Route::post('/trippers-list', 'TripperController@tripperList')->name('tripperList');
        Route::get('/trippers-list', [
          'uses' => 'TripperController@tripperList',
          'as' => 'trippers-listing'
        ]);
        Route::post('/subscription-approve', 'TripperController@subscriptionApprove')->name('subscription-approve');
        Route::post('/subscription-cancel', 'TripperController@subscriptionCancel')->name('subscription-cancel');
        Route::post('/subscription-save', 'TripperController@subscriptionSave')->name('subscription-save');

        Route::post('/user-management', 'UserManagementController@userList')->name('userList');
        Route::get('/user-management', [
          'uses' => 'UserManagementController@userList',
          'as' => 'user-listing'
        ]);
        Route::post('/user-deactivate', 'UserManagementController@userActivate')->name('user-deactivate');
        Route::post('/property-list', 'UserManagementController@ownerProperty')->name('property-list');

    });

});

// Tripper and Rental Routes
Route::any('resendRenterTripperOtp', 'Auth\RegisterController@resendRenterTripperOtp')->name('resendRenterTripperOtp')->middleware('jwtrental.verify:rentaluser');
Route::post('/authenticateUserOtp', 'Auth\RegisterController@authenticateUserOtp')->name('authenticateUserOtp');

Route::post('/tripperRegister', 'Auth\RegisterController@registerTripper')->name('tripperRegister')->middleware('jwtrental.verify:rentaluser');


Route::group(['middleware' => ['jwtrental.verify:rentaluser','XssSanitizer']], function()
{
    Route::post('/search', 'HomeController@search')->name('rental.api.search');
    Route::post('/searchCount', 'HomeController@getSearchCount')->name('rental.api.searchcount');
    Route::post('/hotdeals', 'HomeController@gethotdeals')->name('rental.api.hotdeals');
    Route::post('/propertyBooking', 'PaymentController@propertyBooking')->name('propertydetails.propertyBooking');


    Route::post('/send', 'PropertyDetailController@postSendMessage')->name('conatct.host.send');
    Route::post('/makeoffer', 'PropertyDetailController@makeOffer')->name('rental.api.makeoffer');

    Route::group(['namespace' => 'RentalPanel'], function()
    {
        /* TripBoard Routes */
        Route::post('/tripboards', 'TripBoardController@getTripBoards')->name('rental.api.tripboard');
        Route::get('/tripboard/chat', 'TripBoardController@getLoadLatestMessages')->name('tripboard.chat');
        Route::post('/tripboard/save', 'TripBoardController@saveTripBoards')->name('rental.api.save.tripboard');
        Route::post('/tripboard/saveBuddy', 'TripBoardController@saveTravelBuddy')->name('rental.api.save.buddy');
        Route::post('/tripboard/deleteBuddy', 'TripBoardController@deleteTravelBuddy')->name('rental.api.delete.buddy');
        Route::post('/tripboard/add', 'TripBoardController@addTripBoards')->name('rental.api.tripboard.add');
        Route::post('/tripboard/like', 'TripBoardController@likeTripBoard')->name('rental.api.tripboard.like');
        Route::post('/tripboard/unlike', 'TripBoardController@likeTripBoard')->name('rental.api.tripboard.unlike');
        Route::post('/tripboard/addProperty', 'TripBoardController@addPropertyToTripboard')->name('rental.api.tripboard.add.property');
        Route::post('/tripboard/vote', 'TripBoardController@voteTripboard')->name('rental.api.tripboard.vote');
        Route::post('/view-images', 'TripBoardController@viewImages')->name('view-images');
        Route::post('/tripboard-add', 'TripBoardController@tripBoardAdd')->name('tripboard-add');
        Route::post('/tripboard-remove-listing', 'TripBoardController@tripBoardRemoveListing')->name('tripboard-remove-listing');

        /* Favriote Tripboard */
        Route::post('/tripboard/saveunlike', 'FavoriteController@unlikeTripBoard')->name('rental.api.saved.unlike');

        /* MyACcount Routes */
        Route::post('/setting-save', 'RentalUserController@saveSetting')->name('setting-save');
        Route::post('/save-notifications', 'RentalUserController@saveNotifications')->name('save-notifications');
        Route::post('/save-profilepic', 'RentalUserController@saveProfilepic')->name('save-profilepic');
        Route::post('/savePaymentDetails', 'RentalUserController@savePayment')->name('savePaymentDetails');
        Route::post('/savePaymentReview', 'RentalUserController@savePaymentReview')->name('savePaymentReview');
        Route::post('/deletePaymentDetails', 'RentalUserController@deletePayment')->name('deletePaymentDetails');
        Route::post('/calculaterefund', 'RentalUserController@calculateRefund')->name('calculate.refund');
        Route::post('/cancelbooking', 'RentalUserController@cancelBooking')->name('cancel.booking');
        Route::post('/update-profile', 'RentalUserController@saveUpdateRentalProfile')->name('rental.saveUpdateRentalProfile');

        /* Renter/Tripper Chat Routes */
        Route::post('/chatsearch', 'ChatController@getData')->name('rental.chatsearch');
        Route::post('/unread', 'ChatController@unreadFlag')->name('rental.unread.chat');
    });

});

Route::group([
  'as'         => 'api.',
], function(){
   Helper::includeRouteFiles(__DIR__.'/api/');
});
