<?php

use App\Http\Controllers\RentalPanel\TripBoardController;
use Illuminate\Support\Facades\Route;

Route::group([
    'as'         => 'trip-boards.',
], function(){
    Route::get('trip-board/{tripBoardId}/properties',[TripBoardController::class,'getTripBoardProperties']);
});
