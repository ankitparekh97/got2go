<?php

Route::group([
    'as'        => 'pusher.',
    'prefix'    => 'pusher',
], function() {
    Route::post('verify-owner-auth', [\App\Http\Controllers\Api\PusherAuthController::class, 'authOwnerForPrivateChannel'])->name('verify.private.owner.post');
    Route::post('verify-tripper-auth', [\App\Http\Controllers\Api\PusherAuthController::class, 'authTripperForPrivateChannel'])->name('verify.private.tripper.post');
});
