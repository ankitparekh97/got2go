<?php

use App\Http\Controllers\Api\Tripper\PropertyOfferController;
use App\Http\Controllers\Api\Tripper\ReadNotificationController;
use App\Http\Controllers\Tripper\SubscriptionController;

Route::group([
    'as'        => 'tripper.',
    'prefix'    => 'tripper',
    'middleware' => 'jwtrental.verify:rentaluser'
], function(){
    Route::get('/{tripperId}/properties-offers',[PropertyOfferController::class,'getMyOffers'])
    ->name('propertyOffers.get');

    Route::get('/{tripperId}/properties-offer/{propertyOfferId}',[PropertyOfferController::class,'getPropertyOfferById'])
        ->name('propertyOffers.getByOfferId');

    Route::post(
        '/{tripperId}/property/{propertyId}/property-offer/{propertyOfferId}/counter-offer',
        [PropertyOfferController::class,'counterPropertyOffer']
    )->name('propertyOffers.counterPropertyOffer');

    Route::post(
        '/{tripperId}/property/{propertyId}/make-an-offer',
        [PropertyOfferController::class,'makeAnOffer']
    )->name('propertyOffers.makeAnPropertyOffer');

    Route::post(
        '/{tripperId}/property/{propertyId}/make-an-new-offer-on-existing-offer/{propertyOfferId}',
        [PropertyOfferController::class,'makeAnOfferOnExistingOffer']
    )->name('propertyOffers.makeAnNewOfferOnExistingOffer');

    Route::delete(
        '/{tripperId}/property/{propertyId}/delete-an-offer',
        [PropertyOfferController::class,'deleteAnPropertyOffer']
    )->name('propertyOffers.deleteAnPropertyOffer');

    Route::post(
        '/{tripperId}/property/{propertyId}/property-offer/{propertyOfferId}/cancel-an-offer/',
        [PropertyOfferController::class,'cancelPropertyOffer']
    )->name('propertyOffers.cancelPropertyOffer');

    Route::post(
        '/{tripperId}/cancel-subscription/',
        [SubscriptionController::class,'subscriptionCancel']
    )->name('subscription.cancelMySubscription');


    Route::post(
        'tripper/read-notification',
        [ReadNotificationController::class,'readOfferAcceptedNotifications']
    )->name('readOfferAcceptedNotifications');

});
