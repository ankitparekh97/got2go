<?php

Route::group([
    'as'        => 'rental.',
    'prefix'    => 'rental',
    'middleware' => 'jwtrental.verify:rentaluser'
], function() {

    Route::get('/{rentalId}/apply-coupon-code/{couponCode}',[\App\Http\Controllers\Api\Tripper\CouponCodeController::class,'applyCouponCode'])
        ->name('applyCoupon');

});
