<?php

use App\Http\Controllers\Api\Property\PropertyController;

Route::group([
    'as'        => 'property.',
    'prefix'    => 'property',
], function(){

    Route::get(
        '{propertyId}/check-availability/between/{checkInDate}/{checkOutDate}',
        [PropertyController::class,'checkAvailability']
    )->name('checkAvailabilityWithCheckinCheckOutDate');

});
