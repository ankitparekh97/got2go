<?php

use App\Http\Controllers\Api\OwnerPanel\PropertyOfferController;
use App\Http\Controllers\Api\OwnerPanel\ReadNotificationController;

Route::group([
    'as' => 'owner.',
    'middleware' => 'jwtowner.verify:owner'
], function(){
    Route::get('owner/{ownerId}/properties-offers',[PropertyOfferController::class,'getRequestedOffers'])
        ->name('propertyOffers.get');

    Route::get('owner/{ownerId}/properties-offer/{propertyOfferId}',[PropertyOfferController::class,'getPropertyOfferById'])
        ->name('propertyOffers.getByOfferId');

    Route::post(
        'owner/{ownerId}/property/{propertyId}/property-offer/{propertyOfferId}/accept-offer',
        [PropertyOfferController::class,'acceptPropertyOffer']
    )->name('propertyOffers.acceptOffer');

    Route::post(
        'owner/{ownerId}/property/{propertyId}/property-offer/{propertyOfferId}/decline-offer',
        [PropertyOfferController::class,'declinePropertyOffer']
    )->name('propertyOffers.declineOffer');

    Route::post(
        'owner/{ownerId}/property/{propertyId}/property-offer/{propertyOfferId}/counter-offer',
        [PropertyOfferController::class,'counterPropertyOffer']
    )->name('propertyOffers.counterPropertyOffer');

    Route::post(
        'owner/{ownerId}/property/{propertyId}/property-offer/{propertyOfferId}/cancel-offer',
        [PropertyOfferController::class,'cancelPropertyOffer']
    )->name('propertyOffers.cancelPropertyOffer');

    Route::post(
        'owner/read-notification',
        [ReadNotificationController::class,'readMakeAnOfferAndCounterOfferNotifications']
    )->name('readMakeAnOfferAndCounterOfferNotifications');

});
