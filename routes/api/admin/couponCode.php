<?php

use App\Http\Controllers\Admin\CouponCodeController;

Route::group([
    'middleware' => ['jwtadmin.verify:admin'],
    'as' => 'admin.',
    'prefix' => 'admin'
], function(){
    Route::post('coupon-code', [CouponCodeController::class,'saveCouponCode'])->name('saveCouponCode');
    Route::get('coupon-codes', [CouponCodeController::class,'getAllCouponCodes'])->name('getAllCouponCodes');
    Route::get('check-duplicate-coupon-code/{couponCodeName}', [CouponCodeController::class,'checkDuplicateCouponCode'])->name('checkDuplicateCouponCode');
    Route::get('{couponCodeId}/activate', [CouponCodeController::class,'activeCouponCode'])->name('activeCouponCode');
    Route::get('{couponCodeId}/de-activate', [CouponCodeController::class,'deActiveCouponCode'])->name('deActiveCouponCode');
});
