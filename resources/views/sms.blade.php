@extends('layouts.app')

@section('content')
    <?php
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: text/html');
    ?>
    <div class="d-flex flex-row flex-column-fluid container">
        <!--begin::Content Wrapper-->
        <div class="main d-flex flex-column flex-row-fluid">
            <!--begin::Subheader-->
            <div class="subheader py-2 py-lg-6 " id="kt_subheader">
                <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <h2 class="f-w-700 text-uppercase text-dark d-block w-100 align-items-center text-center">
                        Validate Your Registration
                    </h2>
                </div>
            </div>
            <!--end::Subheader-->
            <div class="content flex-column-fluid d-lg-flex" id="kt_content" style="background-image: url('assets/media/images/login-bg.jpg')">

                <!-- Register start -->
                <div class="login-container align-items-center w-100">
                    <div class="alert alert-danger errorUl" style="display:none;">
                        <ul style="margin-bottom:0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="alert alert-success successUI" style="display:none;">
                        <ul style="margin-bottom:0">
                            <li>New OTP has been sent to your registered mobile and email</li>
                        </ul>
                    </div>
                    <div class="d-table" style="height:0">
                        <div class="d-table-cell vertical-middle">
                            <form method="POST" id="validateotp" action="{{ route('authenticateOtp') }}">
                                @csrf
                                <input type="hidden" name="unique_key" id="unique_key" value="{{ $user->unique_key }}">
                                <p>Enter OTP from your CellPhone or Email</p>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-phone-alt"></i>
                                    </span>
                                    </div>

                                    <input type="number" class="form-control required  @error('otp') is-invalid @enderror" name="otp" id="otp" value="{{ old('otp') }}" placeholder="Enter OTP" autocomplete="off" />
                                </div>
                                @error('otp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                                <span>Didn't receive OTP ?</span> <a href="#" id="resend">Resend again</a>

                                <input type="submit" value="ACTIVATE MY ACCOUNT">
{{--                                <div class="signIn text-center">--}}
{{--                                    <a href="{{route('ownersignupform')}}">Click here</a> to register--}}
{{--                                </div>--}}
                            </form>

                        </div>
                    </div>
                </div>
                <!-- Register end -->
            </div>
        </div>
        <!--end::Content Wrapper-->

    </div>

    <script>
    $( document ).ready(function() {
        $('#validateotp').validate({
            rules: {
                otp: {
                    required: true
                },
            }
        });

        $('#validateotp').on('submit', function(e) {
            e.preventDefault();
            var $form = $(this);
            if(!$form.valid()) return false;
            $('#loader').show();
            $.ajax({
                type: "POST",
                url: "{{ route('authenticateOtp') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    $('#loader').hide();
                    if(msg.error==''){
                        if(msg.success == true){
                            $('.errorUl').hide();
                            window.location.href="{{ route('owner.home') }}" ;
                        } else {
                            $('.errorUl').show();
                        }
                    }else{
                        if(msg.error!=''){
                            $(".alert-danger").find("ul").html('');
                            $(".alert-danger").css('display','block');
                            $.each( msg.error, function( key, value ) {
                                $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                            });
                        }
                    }
                }
            });
        });

        $('#resend').click(function (e){
            e.preventDefault();
            $('#loader').show();
            $.ajax({
                type: "GET",
                url: "{{ route('resendOtp') }}",
                dataType: "JSON",
                data: 'key='+$('#unique_key').val(),
                processData: false,
                contentType: false,
                success: function(msg) {
                    $('#loader').hide();
                    if(msg.error==''){
                        if(msg.success == true){
                            $('.successUI').show();
                        } else {
                            $('.successUI').hide();
                        }
                    }else{
                        if(msg.error!=''){
                            $(".alert-danger").find("ul").html('');
                            $(".alert-danger").css('display','block');
                            $(".alert-danger").find("ul").append('<li>Could not send OTP. Please try again after sometime or contact site administrator</li>');
                        }
                    }
                }
            });
        })
    });
    </script>
@endsection
