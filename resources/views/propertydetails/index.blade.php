@extends('layouts.rentalApp')

@section('content')
<link href="https://fonts.googleapis.com/css2?family=ABeeZee&display=swap" rel="stylesheet">
<div class="modal fade" id="add_tripboard_popup" tabindex="-1" role="dialog" aria-labelledby="add_tripboard_popup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">


            <div class="modal-body text-center">
                <h1 class="title-new-tripboard">NEW <span class="light">TRIPBOARD</span></h1>
                <div class="add-tripboard-container align-items-center w-100">

                    <div class="vertical-middle">
                        <form method="post" id="addTripBoard" name="addTripBoard">
                            <p class="pt-5">Name your board</p>
                            <input type="hidden" class="form-control" name="property_id" value="{{ Request::route('id') }}">
                            <input type="text" class="form-control input-board mt-10" name="tripboard_name" placeholder="Say something like “Conference Weekend” or “Friend’s Bachelorette”">
                            <button class="btn-tripboard-fly mt-15 button-default-animate">ADD BOARD <svg class="ml-8" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="57.558" height="24.346" viewBox="0 0 57.558 24.346">
                                    <defs>
                                        <linearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                                            <stop offset="0" stop-color="#5495d1" />
                                            <stop offset="0.5" stop-color="#ddddb6" />
                                            <stop offset="0.632" stop-color="#eeda8c" />
                                            <stop offset="0.808" stop-color="#e6c469" />
                                            <stop offset="1" stop-color="#f6ab34" />
                                        </linearGradient>
                                    </defs>
                                    <path id="Path_1" data-name="Path 1" d="M147.455,1310.953c2.63,0,4.986.032,7.341-.009,2.154-.037,4.316-.053,6.457-.262,2.372-.231,4.721-.691,7.086-1.01a46.881,46.881,0,0,0,6.792-1.507c1.133-.329,2.264-.665,3.384-1.034.767-.252.834-.573.248-1.15a12.428,12.428,0,0,0-1.127-.916q-2.925-2.266-5.857-4.523a4.324,4.324,0,0,0-.792-.454,1.1,1.1,0,0,1-.626-1.6c.34-.132.665-.243.978-.381a5.2,5.2,0,0,1,3.441.066c1.257.3,2.5.654,3.751.993,2.019.547,4.033,1.112,6.055,1.65,1.11.3,2.234.544,3.347.832.7.181,1.383.425,2.086.594,1.3.313,2.365-.491,3.541-.77.966-.23,1.928-.5,2.871-.808a36.984,36.984,0,0,0,3.717-1.361,4.383,4.383,0,0,1,4.636,1.067,2.644,2.644,0,0,1-.294,2.653,7.564,7.564,0,0,1-2.018,1.276c-1.539.728-3.107,1.4-4.681,2.05-1.107.46-2.247.839-3.362,1.28a1.7,1.7,0,0,0-.706.475c-2.015,2.6-4.011,5.212-6.012,7.822q-2.077,2.709-4.152,5.419a3.6,3.6,0,0,1-2.7.771c-.569-.239-.419-.711-.326-1.144.225-1.05.44-2.1.7-3.144.3-1.221.664-2.428.973-3.647.187-.735.355-1.477.475-2.225.115-.711-.446-.522-.787-.471-.751.112-1.492.293-2.238.441-1.243.247-2.479.542-3.732.72-1.463.209-2.939.321-4.41.462-1.528.147-3.056.3-4.587.409a19.918,19.918,0,0,1-2.192.018c-1.67-.062-3.342-.112-5.007-.246-2.236-.178-4.479-.345-6.7-.676-1.739-.26-3.443-.756-5.157-1.167C147.76,1311.42,147.681,1311.225,147.455,1310.953Z" transform="translate(-147.455 -1297.832)" fill="url(#linear-gradient)" />
                                </svg>
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-detail-page-slider postion-relative">
    <div class="add-to-trip-dropdown">
        <div class="container">

            <div class="dropdown pdp-dropdown-menu {{($isLiked == 1) ? 'like-open active' : '' }}" id="favorticon">
                @if(Auth::guard('rentaluser')->user() )
                <a class="like-button " data-val="{{$isLiked}}"><i class="got got-heart"></i> </a>
                @else
                <div class="like-button login-modal"> <i class="got got-heart"></i></div>

                @endif
                @if(!Auth::guard('rentaluser')->user() )
                <button class="add-to-trip-drpdown-btn login-modal" id="add-trip-drp" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Save In <i class="fa fa-caret-down pl-2"></i></button>
                @else
                <button class="add-to-trip-drpdown-btn" id="add-trip-drp" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Save In <i class="fa fa-caret-down pl-2"></i></button>
                <div class="dropdown-menu" aria-labelledby="add-trip-drp">
                    <ul class="favorite-list">
                     @foreach($tripboards as $tripboard)
                        <li class="add-to-existing {{(count($tripboard->tripboardproperty) > 0)?'active':''}}" id="trip_{{$tripboard->id}}" data-id="{{$tripboard->id}}">
                            <div class="favorite-item">
                                <span>{{$tripboard->tripboard_name}} </span> <a href="javascript:void(0)" class="btn btn-pink {{(count($tripboard->tripboardproperty) > 0)?'active':''}}" data-type="{{(count($tripboard->tripboardproperty) > 0)?'save':''}}">{!! (count($tripboard->tripboardproperty) > 0)?'<i class="fas fa-check"></i>Saved':'Save' !!}</a>
                            </div>
                        </li>
                        @endforeach

                    </ul>
                    <a class="create-new-tripbrd" href="#" data-toggle="modal" data-target="#add_tripboard_popup"><em class="fa fa-plus"></em>  Create Tripboard</a>
                </div>
                @endif
            </div>


        </div>
    </div>
     <input type="hidden" value="{{(!empty($propertyDetails->propertyimages->toarray()))?$propertyDetails->propertyimages[0]->photo:''}}" class="property_image">
    <div class="slider slider-for">

        @foreach($propertyDetails->propertyimages as $images)
        <div>

            @if($images->media_type == 'video/mp4' || $images->media_type == 'video/quicktime')
                <div class="uploaded-image customVideo" >
                    <div class="image-thumb-button" >
                        <button class="play-video"  data-imageid="{{$images->id}}" data-state="pause"><i class="fa fa-play"></i></button>
                    </div>
                    <video id="{{$images->id}}" class="video" src="{{URL::asset('uploads/property/'.$images->photo)}}"></video>
                </div>
            @else
                <img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$images->photo)}}" alt="{{$propertyDetails->title}}" />
            @endif

            <div class="description">
                <span class="back-img"><a href="{{ URL::previous() }}">
                    <img data-src="{{URL::asset('media/images/my-dashboard/circle-black-arrow.png')}}" alt="back arrow" class="lazy-load d-block d-xs-block d-sm-none d-md-none d-lg-none"/>
                    <img data-src="{{URL::asset('media/images/property-back-icon.png')}}" alt="back icon"  class="lazy-load d-none d-xs-none d-sm-block d-md-block d-lg-block"/>

            </a></span>
                <div class="right-text">
                    <h2 class="title-large">{{$propertyDetails->title}}</h2>
                    <h4>@php
                        $address = explode(",",$propertyDetails->location_detail);
                        @endphp

                        @if($showStAdd == true)
                            {{ $address[0] }} |
                        @endif
                        {{$propertyDetails->city}}, {{$propertyDetails->state}} @empty(!$propertyDetails->zipcode) {{$propertyDetails->zipcode}} @endempty
                    </h4>
                </div>
            </div>

            <div class="desc-below-property-type desktop-hide">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.26087 16V11.102H10.087V16H13.913V9.14286H16V7.5102L8.68444 0.642532C8.29964 0.281296 7.70036 0.281295 7.31556 0.642531L0 7.5102V9.14286H2.08696V16H6.26087Z" fill="url(#paint0_linear)"/>
                    <defs>
                        <linearGradient id="paint0_linear" x1="16" y1="8" x2="0.636948" y2="14.1696" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E40046"/>
                            <stop offset="1" stop-color="#FCE300"/>
                        </linearGradient>
                    </defs>
                </svg>

                <h6>
                    @if($propertyDetails->type_of_property == 'vacation_rental')
                        Vacation Club
                    @else
                        Private Residence
                    @endif
                </h6>
            </div>

        </div>
        @endforeach
    </div>
</div>

<div class="page-content-start propert-detail-page">
    <input type="hidden" id="owner_id" value="{{$propertyDetails->owner_id}}">
    <div class="property-detail-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content">
                        <h3>ENTIRE HOME HOSTED BY </h3>
                    <div class="profile-info" id="profile-info" data-id="{{$propertyDetails->id}}">
                            <span class="image">
                                <img class="lazy-load "alt="property" data-src="{{ $propertyDetails->ownerdetails->photo ? url('uploads/owners/'.$propertyDetails->ownerdetails->photo) : url('/media/users/default.jpg') }}" alt="{{$propertyDetails->ownerdetails->first_name}}" />
                            </span>
                            <h1>{{$propertyDetails->ownerdetails->first_name}}</h1>
                            <h5>{{ $propertyDetails->no_of_guest > 1 ? $propertyDetails->no_of_guest ." guests" : $propertyDetails->no_of_guest ." guest" }} · {{ $propertyDetails->total_bedroom > 1 ? $propertyDetails->total_bedroom ." bedrooms" : $propertyDetails->total_bedroom. " bedroom" }} · {{ $propertyDetails->total_beds > 1 ? $propertyDetails->total_beds ." beds" : $propertyDetails->total_beds ." bed" }} · {{ $propertyDetails->total_bathroom > 1 ? $propertyDetails->total_bathroom ." baths" : $propertyDetails->total_bathroom ." bath" }}</h5>
                        </div>
                        <!-- contact-host-btn -->
                                    <div class="contact-host-action">
                                     @if(Auth::guard('rentaluser')->user() )
                                        <a href="#" class="btn btn-pink btn-800 btn-round button-default-animate" data-toggle="modal"   data-target="#Contacthost">Contact Host</a>
                                     @else
                                        <div class="btn btn-pink btn-800 btn-round button-default-animate login-modal"> Contact Host</div>
                                     @endif
                                    </div>
                                    <!-- End contact-host-btn -->
                        <div class="private-residence">
                            <div class="residence">
                                <div class="inner-box">
                                    <img class="lazy-load" data-src="{{URL::asset('media/images/house-gradient.png')}}" alt="house" />
                                    <h6>
                                        @if($propertyDetails->type_of_property == 'vacation_rental')
                                            VACATION CLUB
                                        @else
                                            PRIVATE RESIDENCE
                                        @endif
                                    </h6>
                                </div>
                            </div>
                            <div class="contact-host">
                                <div class="cancellation mb-6">
                                    <a class="cancel-policy-csstm" data-toggle="modal" data-target="#cancellation-policy">
                                        <span class="image test">
                                            <img class="lazy-load" data-src="{{URL::asset('media/images/policy-icon.png')}}" alt="policy" />
                                        </span>
                                        <p>{{$propertyDetails->cancellation_type}} Cancellation Policy</p>
                                    </a>

                                </div>
                                @if($propertyDetails->is_instant_booking_allowed)
                                    <div class="cancellation">
                                        <span class="image test">
                                            <img class="lazy-load" data-src="{{URL::asset('media/images/instant-booking.png')}}" alt="instant booking" />
                                        </span>
                                        <p>Book this listing instantly without<br>waiting for owner confirmation</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="description">
                        <h3>ABOUT THE PLACE</h3>
                            <p>{{$propertyDetails->description}}</p>
                            <h3>The space</h3>
                            <p>{{$propertyDetails->about_the_space}}</p>
                        </div>
                        <div class="filter-subox filter-stay-features description">
                            <h3>Amenities</h3>

                            <div class="owl-carousel owl-theme" id="propert-detail-page-slider">

                                @foreach($propertyDetails->propertyamenities as $amenity)
                                <div class="item">

                                        @php
                                        $amenity_css = str_replace(' ', '-', $amenity->masteramenity->amenity_name);
                                        $amenity_css = str_replace('/', '-', $amenity_css);
                                        $amenity_css = strtolower($amenity_css);
                                        @endphp
                                        <label class="checkbox-btn">
                                          <span>
                                            <input type="checkbox" style="display: none;" disabled value="" name="amenity_id[]" checked>
                                            <div class="bg-@php echo  $amenity_css @endphp amenity"></div>
                                            <span class="title">{{$amenity->masteramenity->amenity_name}}</span>
                                            <span class="checkmark-check"></span>
                                          </span>
                                        </label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 price-pdp-box">
                    <div class="right-content">
                        <h2><strong>${{((Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper == 0) || !Auth::guard('rentaluser')->user())?number_format($propertyDetails->price,2):number_format($tripperPrice,2)}}</strong> / NIGHT</h2>
                     <div class="d-block w-100">
                         <div class="d-flex justify-content-center align-items-center">
                        <div class="planes-image text-center {{(count($propertyDetails->propertyreviews) > 0)?'rating_'.round($propertyDetails->propertyreviews[0]->avg_rating):''}}"  style="background-image:url({{URL::asset('media/images/tripper-page/propert-planes-2.png')}})">
                        </div>

                        <h3 class="ratings text-center">{{(count($propertyDetails->propertyreviews) > 0)?number_format($propertyDetails->propertyreviews[0]->avg_rating,1):'0'}} ({{(count($propertyDetails->propertyreviews) > 0)?$propertyDetails->propertyreviews[0]->total_review:'0'}})</h3>
                        </div></div>
                        @if((Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper == 0) || !Auth::guard('rentaluser')->user())
                            <div class="pricing-table-notes">
                                <h4 class="blue text-center exclusive-text">EXCLUSIVE ACCESS TO STAYS, NEGOTIATION WITH OWNERS,  AND LESS  FEES.</h4>
                                <div class="text-center">
                                    <a href="{{route('tripper.index')}}" class="blue-btn">Become a Tripper</a>
                                </div>
                            </div>
                        @endif

                        <div class="hr"></div>
                        <h2 class="lets-break-it-down">LET US BREAK IT DOWN FOR YA</h2>
                        <div class="guest-info1" style="display: none">
                            <div class="row align-items-center" style="position:relative">
                                <div class="col-12 col-sm-4 ">
                                    <div class="display-inline">
                                        <span>CHECK-IN</span>
                                        <input type="text" class="form-control"  id="from_date" readonly  />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 text-center">
                                    <div class="display-inline">
                                        <span>GUESTS</span>
                                        <h3 class="guest">2</h3>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 text-right">
                                    <div class="display-inline">
                                        <span>CHECK-OUT</span>
                                        <input type="text" class="form-control" id="to_date" readonly  />
                                    </div>
                                </div>
                                <div class="cal-down-arrow-cir" style="bottom:-60px">
                                  <button type="button" class="previous-icon" id="datepicker">
                                    <img class="lazy-load" data-src="{{URL::asset('media/images/Down-arrow-round.png')}}" alt="down arrow">
                                  </button>
                                </div>
                            </div>
                        </div>
                        <form action="" id="propertyBooking" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{$propertyDetails->id}}" >
                        <div class="guest-info">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                     <div class="display-inline">
                                        <span>CHECK-IN</span>

                                        <div class="input-group date" >
                                            <span class="calendar">
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18.4615 21.4359C19.3029 21.4359 20 20.7421 20 19.9048V4.59341C20 3.75607 19.3029 3.06227 18.4615 3.06227H16.9231V1.91392C16.9231 0.861264 16.0577 0 15 0H14.2308C13.1731 0 12.3077 0.861264 12.3077 1.91392V3.06227H7.69231V1.91392C7.69231 0.861264 6.82692 0 5.76923 0H5C3.94231 0 3.07692 0.861264 3.07692 1.91392V3.06227H1.53846C0.697115 3.06227 0 3.75607 0 4.59341V19.9048C0 20.7421 0.697115 21.4359 1.53846 21.4359H18.4615ZM5.76923 5.74176H5C4.79567 5.74176 4.61538 5.56233 4.61538 5.35897V1.91392C4.61538 1.71057 4.79567 1.53114 5 1.53114H5.76923C5.97356 1.53114 6.15385 1.71057 6.15385 1.91392V5.35897C6.15385 5.56233 5.97356 5.74176 5.76923 5.74176ZM15 5.74176H14.2308C14.0264 5.74176 13.8462 5.56233 13.8462 5.35897V1.91392C13.8462 1.71057 14.0264 1.53114 14.2308 1.53114H15C15.2043 1.53114 15.3846 1.71057 15.3846 1.91392V5.35897C15.3846 5.56233 15.2043 5.74176 15 5.74176ZM5 11.1007H1.53846V7.65568H5V11.1007ZM9.61539 11.1007H5.76923V7.65568H9.61539V11.1007ZM14.2308 11.1007H10.3846V7.65568H14.2308V11.1007ZM18.4615 11.1007H15V7.65568H18.4615V11.1007ZM5 15.6941H1.53846V11.8663H5V15.6941ZM9.61539 15.6941H5.76923V11.8663H9.61539V15.6941ZM14.2308 15.6941H10.3846V11.8663H14.2308V15.6941ZM18.4615 15.6941H15V11.8663H18.4615V15.6941ZM5 19.9048H1.53846V16.4597H5V19.9048ZM9.61539 19.9048H5.76923V16.4597H9.61539V19.9048ZM14.2308 19.9048H10.3846V16.4597H14.2308V19.9048ZM18.4615 19.9048H15V16.4597H18.4615V19.9048Z" fill="#FCE300"/>
                                                </svg>
                                            </span>
                                            <input type="text" class="form-control"  value="05/20/2017" id="kt_datepicker_3"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                     <div class="display-inline">
                                        <span>CHECK-OUT</span>

                                        <div class="input-group date" >
                                            <span class="calendar">
                                                <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18.4615 21.4359C19.3029 21.4359 20 20.7421 20 19.9048V4.59341C20 3.75607 19.3029 3.06227 18.4615 3.06227H16.9231V1.91392C16.9231 0.861264 16.0577 0 15 0H14.2308C13.1731 0 12.3077 0.861264 12.3077 1.91392V3.06227H7.69231V1.91392C7.69231 0.861264 6.82692 0 5.76923 0H5C3.94231 0 3.07692 0.861264 3.07692 1.91392V3.06227H1.53846C0.697115 3.06227 0 3.75607 0 4.59341V19.9048C0 20.7421 0.697115 21.4359 1.53846 21.4359H18.4615ZM5.76923 5.74176H5C4.79567 5.74176 4.61538 5.56233 4.61538 5.35897V1.91392C4.61538 1.71057 4.79567 1.53114 5 1.53114H5.76923C5.97356 1.53114 6.15385 1.71057 6.15385 1.91392V5.35897C6.15385 5.56233 5.97356 5.74176 5.76923 5.74176ZM15 5.74176H14.2308C14.0264 5.74176 13.8462 5.56233 13.8462 5.35897V1.91392C13.8462 1.71057 14.0264 1.53114 14.2308 1.53114H15C15.2043 1.53114 15.3846 1.71057 15.3846 1.91392V5.35897C15.3846 5.56233 15.2043 5.74176 15 5.74176ZM5 11.1007H1.53846V7.65568H5V11.1007ZM9.61539 11.1007H5.76923V7.65568H9.61539V11.1007ZM14.2308 11.1007H10.3846V7.65568H14.2308V11.1007ZM18.4615 11.1007H15V7.65568H18.4615V11.1007ZM5 15.6941H1.53846V11.8663H5V15.6941ZM9.61539 15.6941H5.76923V11.8663H9.61539V15.6941ZM14.2308 15.6941H10.3846V11.8663H14.2308V15.6941ZM18.4615 15.6941H15V11.8663H18.4615V15.6941ZM5 19.9048H1.53846V16.4597H5V19.9048ZM9.61539 19.9048H5.76923V16.4597H9.61539V19.9048ZM14.2308 19.9048H10.3846V16.4597H14.2308V19.9048ZM18.4615 19.9048H15V16.4597H18.4615V19.9048Z" fill="#FCE300"/>
                                                </svg>
                                            </span>
                                            <input type="text" class="form-control"  value="05/20/2017" id="kt_datepicker_4"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="display-inline">
                                        <span>GUESTS</span>
                                        <div class="plus-minus-sec">
                                            <span class="guest-minus minus transition">-</span>
                                            <input id="guests" name="guests" type="text" max="{{$propertyDetails->no_of_guest}}" data-value="{{$propertyDetails->no_of_guest}}" class="cmn-add" readonly/>
                                            <span class="guest-plus plus transition">+</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <span class="availabelDtsErr"></span>

                            <div class="pricing-table-property competitor-sec">
                                <div class="competitor-text">
                                    Competitor Service Fee for Comparison: <div class="competitor-fee">$176</div>
                                    <span><svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <mask id="path-1-inside-1" fill="white">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 19C14.7467 19 19 14.7467 19 9.5C19 4.25329 14.7467 0 9.5 0C4.25329 0 0 4.25329 0 9.5C0 14.7467 4.25329 19 9.5 19ZM8.0166 9.88477V10.4727H10.1152V10.124C10.1152 9.92806 10.1745 9.75944 10.293 9.61816C10.416 9.47233 10.7168 9.24447 11.1953 8.93457C11.7604 8.57454 12.1706 8.19401 12.4258 7.79297C12.6855 7.38737 12.8154 6.90885 12.8154 6.35742C12.8154 5.58724 12.526 4.97884 11.9473 4.53223C11.3685 4.08561 10.571 3.8623 9.55469 3.8623C8.31966 3.8623 7.13477 4.18587 6 4.83301L6.9502 6.69238C7.87077 6.20475 8.67513 5.96094 9.36328 5.96094C9.64128 5.96094 9.86686 6.0179 10.04 6.13184C10.2132 6.24121 10.2998 6.3916 10.2998 6.58301C10.2998 6.82454 10.2178 7.03874 10.0537 7.22559C9.89421 7.41244 9.62988 7.62207 9.26074 7.85449C8.7959 8.14616 8.47233 8.44694 8.29004 8.75684C8.10775 9.06217 8.0166 9.43815 8.0166 9.88477ZM8.08496 11.8945C7.84342 12.1224 7.72266 12.446 7.72266 12.8652C7.72266 13.2845 7.85026 13.6081 8.10547 13.8359C8.36068 14.0592 8.71387 14.1709 9.16504 14.1709C9.60254 14.1709 9.94661 14.057 10.1973 13.8291C10.4525 13.6012 10.5801 13.2799 10.5801 12.8652C10.5801 12.4505 10.457 12.1292 10.2109 11.9014C9.9694 11.6689 9.62077 11.5527 9.16504 11.5527C8.69108 11.5527 8.33105 11.6667 8.08496 11.8945Z"/>
                                        </mask>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.5 19C14.7467 19 19 14.7467 19 9.5C19 4.25329 14.7467 0 9.5 0C4.25329 0 0 4.25329 0 9.5C0 14.7467 4.25329 19 9.5 19ZM8.0166 9.88477V10.4727H10.1152V10.124C10.1152 9.92806 10.1745 9.75944 10.293 9.61816C10.416 9.47233 10.7168 9.24447 11.1953 8.93457C11.7604 8.57454 12.1706 8.19401 12.4258 7.79297C12.6855 7.38737 12.8154 6.90885 12.8154 6.35742C12.8154 5.58724 12.526 4.97884 11.9473 4.53223C11.3685 4.08561 10.571 3.8623 9.55469 3.8623C8.31966 3.8623 7.13477 4.18587 6 4.83301L6.9502 6.69238C7.87077 6.20475 8.67513 5.96094 9.36328 5.96094C9.64128 5.96094 9.86686 6.0179 10.04 6.13184C10.2132 6.24121 10.2998 6.3916 10.2998 6.58301C10.2998 6.82454 10.2178 7.03874 10.0537 7.22559C9.89421 7.41244 9.62988 7.62207 9.26074 7.85449C8.7959 8.14616 8.47233 8.44694 8.29004 8.75684C8.10775 9.06217 8.0166 9.43815 8.0166 9.88477ZM8.08496 11.8945C7.84342 12.1224 7.72266 12.446 7.72266 12.8652C7.72266 13.2845 7.85026 13.6081 8.10547 13.8359C8.36068 14.0592 8.71387 14.1709 9.16504 14.1709C9.60254 14.1709 9.94661 14.057 10.1973 13.8291C10.4525 13.6012 10.5801 13.2799 10.5801 12.8652C10.5801 12.4505 10.457 12.1292 10.2109 11.9014C9.9694 11.6689 9.62077 11.5527 9.16504 11.5527C8.69108 11.5527 8.33105 11.6667 8.08496 11.8945Z" fill="#A7B0B4"/>
                                        <path d="M8.0166 10.4727H5.5166V12.9727H8.0166V10.4727ZM10.1152 10.4727V12.9727H12.6152V10.4727H10.1152ZM10.293 9.61816L8.38223 8.00598L8.37749 8.01164L10.293 9.61816ZM11.1953 8.93457L9.85202 6.82612L9.84417 6.83112L9.83636 6.83618L11.1953 8.93457ZM12.4258 7.79297L10.3205 6.44466L10.3166 6.45078L12.4258 7.79297ZM6 4.83301L4.76153 2.66133L2.6872 3.84428L3.77384 5.97064L6 4.83301ZM6.9502 6.69238L4.72403 7.83002L5.87851 10.0891L8.12042 8.90159L6.9502 6.69238ZM10.04 6.13184L8.666 8.22038L8.68541 8.23315L8.70506 8.24556L10.04 6.13184ZM10.0537 7.22559L8.17511 5.57608L8.16361 5.58918L8.1523 5.60243L10.0537 7.22559ZM9.26074 7.85449L10.5895 9.97215L10.5928 9.97007L9.26074 7.85449ZM8.29004 8.75684L10.4366 10.0384L10.4408 10.0314L10.4449 10.0244L8.29004 8.75684ZM8.08496 11.8945L6.38644 10.0601L6.37789 10.068L6.36941 10.076L8.08496 11.8945ZM8.10547 13.8359L6.44043 15.7008L6.44978 15.7091L6.4592 15.7174L8.10547 13.8359ZM10.1973 13.8291L8.53223 11.9643L8.52387 11.9717L8.51559 11.9792L10.1973 13.8291ZM10.2109 11.9014L8.47749 13.7028L8.49479 13.7195L8.51242 13.7358L10.2109 11.9014ZM16.5 9.5C16.5 13.366 13.366 16.5 9.5 16.5V21.5C16.1274 21.5 21.5 16.1274 21.5 9.5H16.5ZM9.5 2.5C13.366 2.5 16.5 5.63401 16.5 9.5H21.5C21.5 2.87258 16.1274 -2.5 9.5 -2.5V2.5ZM2.5 9.5C2.5 5.63401 5.63401 2.5 9.5 2.5V-2.5C2.87258 -2.5 -2.5 2.87258 -2.5 9.5H2.5ZM9.5 16.5C5.63401 16.5 2.5 13.366 2.5 9.5H-2.5C-2.5 16.1274 2.87258 21.5 9.5 21.5V16.5ZM10.5166 10.4727V9.88477H5.5166V10.4727H10.5166ZM10.1152 7.97266H8.0166V12.9727H10.1152V7.97266ZM7.61523 10.124V10.4727H12.6152V10.124H7.61523ZM8.37749 8.01164C7.85657 8.63273 7.61523 9.38037 7.61523 10.124H12.6152C12.6152 10.2811 12.5908 10.4729 12.5183 10.6793C12.4451 10.8878 12.3363 11.0723 12.2085 11.2247L8.37749 8.01164ZM9.83636 6.83618C9.41209 7.11095 8.7849 7.52876 8.38224 8.00598L12.2037 11.2303C12.1623 11.2794 12.1286 11.3141 12.1092 11.3334C12.089 11.3535 12.0756 11.3654 12.072 11.3686C12.0661 11.3738 12.0794 11.3617 12.1205 11.3306C12.2041 11.2673 12.3434 11.1695 12.5543 11.033L9.83636 6.83618ZM10.3166 6.45078C10.3209 6.44412 10.2267 6.58742 9.85202 6.82612L12.5386 11.043C13.2942 10.5617 14.0203 9.9439 14.5349 9.13516L10.3166 6.45078ZM10.3154 6.35742C10.3154 6.43638 10.3062 6.47158 10.3053 6.47501C10.3048 6.47707 10.3055 6.47382 10.3086 6.46687C10.3117 6.45985 10.3158 6.45208 10.3205 6.44466L14.531 9.14127C15.0843 8.27734 15.3154 7.31579 15.3154 6.35742H10.3154ZM10.42 6.51147C10.4264 6.51641 10.4154 6.50889 10.3967 6.48606C10.3773 6.46259 10.3573 6.43209 10.3408 6.39734C10.3244 6.36289 10.3171 6.33618 10.3145 6.32372C10.3121 6.31227 10.3154 6.32157 10.3154 6.35742H15.3154C15.3154 4.86059 14.7039 3.50164 13.4746 2.55299L10.42 6.51147ZM9.55469 6.3623C9.88449 6.3623 10.1047 6.39904 10.241 6.4372C10.3713 6.47369 10.4164 6.50871 10.42 6.51147L13.4746 2.55299C12.3159 1.65893 10.912 1.3623 9.55469 1.3623V6.3623ZM7.23847 7.00469C8.01853 6.55983 8.77928 6.3623 9.55469 6.3623V1.3623C7.86004 1.3623 6.251 1.81191 4.76153 2.66133L7.23847 7.00469ZM9.17636 5.55475L8.22616 3.69537L3.77384 5.97064L4.72403 7.83002L9.17636 5.55475ZM9.36328 3.46094C8.11073 3.46094 6.89136 3.89448 5.77997 4.48318L8.12042 8.90159C8.85018 8.51503 9.23953 8.46094 9.36328 8.46094V3.46094ZM11.4141 4.04329C10.7154 3.58366 9.96695 3.46094 9.36328 3.46094V8.46094C9.3156 8.46094 9.01829 8.45215 8.666 8.22038L11.4141 4.04329ZM12.7998 6.58301C12.7998 5.46399 12.2124 4.54697 11.375 4.01811L8.70506 8.24556C8.51003 8.12238 8.2613 7.90927 8.06831 7.57407C7.87116 7.23166 7.7998 6.87887 7.7998 6.58301H12.7998ZM11.9323 8.87509C12.484 8.24683 12.7998 7.45093 12.7998 6.58301H7.7998C7.7998 6.41455 7.82962 6.22395 7.90364 6.03069C7.97723 5.83852 8.07804 5.68664 8.17511 5.57608L11.9323 8.87509ZM10.5928 9.97007C11.0372 9.69024 11.5517 9.32134 11.9551 8.84874L8.1523 5.60243C8.18278 5.56673 8.20311 5.54752 8.20654 5.54432C8.21039 5.54071 8.2035 5.54744 8.18235 5.5642C8.13812 5.59929 8.05755 5.65779 7.92871 5.73891L10.5928 9.97007ZM10.4449 10.0244C10.3889 10.1195 10.3483 10.1515 10.3716 10.1299C10.3945 10.1086 10.4597 10.0536 10.5895 9.97215L7.93201 5.73683C7.29057 6.13931 6.59987 6.69936 6.13521 7.48928L10.4449 10.0244ZM10.5166 9.88477C10.5166 9.83011 10.5223 9.82502 10.5146 9.85682C10.5062 9.89126 10.4848 9.95764 10.4366 10.0384L6.14349 7.47531C5.6776 8.25567 5.5166 9.10116 5.5166 9.88477H10.5166ZM10.2227 12.8652C10.2227 12.8921 10.2191 13.0118 10.1554 13.1823C10.0875 13.3643 9.96942 13.5537 9.80051 13.713L6.36941 10.076C5.5028 10.8936 5.22266 11.9497 5.22266 12.8652H10.2227ZM9.77051 11.9711C9.93907 12.1216 10.0671 12.3101 10.144 12.5052C10.2164 12.6887 10.2227 12.8238 10.2227 12.8652H5.22266C5.22266 13.8228 5.53679 14.894 6.44043 15.7008L9.77051 11.9711ZM9.16504 11.6709C9.11655 11.6709 9.41874 11.6631 9.75173 11.9545L6.4592 15.7174C7.30262 16.4554 8.31118 16.6709 9.16504 16.6709V11.6709ZM8.51559 11.9792C8.66675 11.8418 8.82906 11.7573 8.96537 11.7122C9.09304 11.6699 9.17022 11.6709 9.16504 11.6709V16.6709C10.0377 16.6709 11.0465 16.4357 11.8789 15.679L8.51559 11.9792ZM8.08008 12.8652C8.08008 12.8182 8.08732 12.6797 8.16092 12.4944C8.23889 12.2981 8.36705 12.1117 8.53223 11.9643L11.8623 15.6939C12.76 14.8924 13.0801 13.8279 13.0801 12.8652H8.08008ZM8.51242 13.7358C8.34671 13.5823 8.2251 13.3952 8.15315 13.2074C8.08545 13.0306 8.08008 12.9024 8.08008 12.8652H13.0801C13.0801 11.9309 12.7832 10.876 11.9095 10.067L8.51242 13.7358ZM9.16504 14.0527C9.21143 14.0527 8.84838 14.0597 8.47749 13.7028L11.9444 10.0999C11.0904 9.2782 10.0301 9.05273 9.16504 9.05273V14.0527ZM9.78348 13.7289C9.40663 14.0779 9.06072 14.0527 9.16504 14.0527V9.05273C8.32144 9.05273 7.25548 9.25547 6.38644 10.0601L9.78348 13.7289Z" fill="#A7B0B4" mask="url(#path-1-inside-1)"/>
                                        </svg>
                                        <div class="competitor-service-text-inner">
                                            <em class="fas fa-caret-up"></em>
                                            Compared with other competitors that charge an average of 13% on fees. By choosing GOT2GO:
                                            <span class="custom-span-space text-center top-space renter-comp">SAVE <span>~$16</span> as a GOT2GO Renter</span>
                                            <span class="custom-span-space text-center tripper-comp">SAVE <span>~$134</span> as a GOT2GO Tripper</span>
                                        </div>
                                    </span>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>REGULAR PRICE</h5>
                                        <div class="row">
                                            <div class="col-8">
                                                <span>$<span class="regular_price pb-0">{{number_format($propertyDetails->price,2)}}</span> x <span class="regular_days pb-0">1</span> nights</span>
                                            </div>
                                            <div class="col-4">
                                                <span><span class="days_regular pb-0">1,350</span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <span>Cleaning Fee</span>
                                            </div>
                                            <div class="col-4">
                                                <span>$<span class="renter_cleaning_fee pb-0">{{number_format($propertyDetails->cleaning_fee + (0.1*$propertyDetails->cleaning_fee),2)}}</span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <span>Service Fee</span>
                                            </div>
                                            <div class="col-4">
                                                <span>$<span class="service_fee pb-0" data-type="{{$serviceType}}" data-value="{{$serviceFee}}"></span></span>
                                            </div>
                                        </div>
                                       <div class="row">
                                            <div class="col-8">
                                                <span>Occupancy Tax</span>
                                            </div>
                                            <div class="col-4">
                                                <span>$<span class="occupancy_fee pb-0"></span></span>
                                            </div>
                                        </div>
                                        <div class="bottom-row space">
                                            <div class="row">
                                                <div class="col-6">
                                                    <span>TOTAL</span>
                                                </div>
                                                <div class="col-6 text-right">
                                                    <span><span class="regular_total pb-0">1,524</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="blue-text">{{((Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper == 0) || !Auth::guard('rentaluser')->user())?"TRIPPER PRICE":"YOUR PRICE"}}</h5>
                                        <div class="row">
                                            <div class="col-8">
                                                <span>$<span class="tripper_price pb-0">{{number_format($tripperPrice,2)}}</span> x <span class="tripper_days pb-0">1</span> nights</span>
                                            </div>
                                            <div class="col-4">
                                                <span><span class="days_tripper pb-0">1,170</span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <span>Cleaning Fee</span>
                                            </div>
                                            <div class="col-4">
                                                <span>$<span class="cleaning_fee_tripper pb-0">{{number_format($propertyDetails->cleaning_fee,2)}}</span></span>
                                            </div>
                                        </div>
                                        <div class="row transaction-fee-row">
                                            <div class="col-8 blue-text">
                                                <span class="blue-text">Transaction Fee</span>
                                            </div>
                                            <div class="col-4">
                                                <span><span class="transication_fee blue-text pb-0"><s class="blue-text">$174</s></span></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8 blue-text">
                                                <span class="blue-text">Occupancy Tax</span>
                                            </div>
                                            <div class="col-4">
                                                <span class="blue-text">$<span class="occupancy_fee_tripper pb-0 blue-text"></span></span>
                                            </div>
                                        </div>
                                        <div class="bottom-row left-padding extra-space">
                                            <div class="row">
                                                <div class="col-8">
                                                    <span class="blue">{{((Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper == 0) || !Auth::guard('rentaluser')->user())?"TRIPPER TOTAL":"YOUR TOTAL"}}</span>
                                                </div>
                                                <div class="col-4">
                                                    <span class="blue"><span class="tripper_total blue pb-0">1,170</span></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-8">
                                                    <span class="blue light">Total Tripper Savings</span>
                                                </div>
                                                <div class="col-4">
                                                    <span class="blue light"><span class="total_save blue pb-0">$174</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="footer-buttons text-center">

                                <div class="d-block w-100 mb-10">
                                @if($propertyDetails->is_price_negotiable == 1)
                                    @if(!Auth::guard('rentaluser')->user() )
                                        <a style="cursor: pointer" href="#" class="login-modal blue-border-btn" data-toggle="modal" data-target="#login_popup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Make an Offer</a>
                                    @else
                                        @if(Auth::guard('rentaluser')->user()->is_tripper == 0)
                                            <a href="#" class="blue-border-btn" data-toggle="modal" data-target="#tripperpopup" >Make an Offer</a>
                                        @else
                                            <a href="javascript:void(0)" class="blue-border-btn" id="make_offer" data-id="{{$propertyDetails->id}}">Make an Offer</a>
                                        @endif
                                    @endif
                                @endif
                                </div>


                                <div class="d-block w-100">
                                <span id="lets_go_tab" class="d-block w-100 m-0">
                                    @if(!Auth::guard('rentaluser')->user() )
                                        <a style="cursor: pointer" href="#" class="login-modal red-btn" data-toggle="modal" data-target="#login_popup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LET’S GO
                                        </a>
                                    @else
                                            <a href="javascript:void(0)" class="red-btn" id="letsgo" data-id={{$propertyDetails->id}} data-test={{ getLoggedInRentalId() }}>LET’S GO</a>
                                    @endif

                                </span>
                                </div>
                                <h3 class="w-100 d-block mt-3">(We won’t charge you yet)</h3>
                                <!--  -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="check-neighborhood text-center">
    <div class="container">
        <h2>CHECK OUT THE NEIGHBORHOOD</h2>
        @if($showStAdd == false)
            <h5>(Exact location provided after booking)</h5>
        @endif

        <div id="map" style="height:620px;width:100%;"></div>
    </div>
</div>

<!-- booking success Modal -->
<div class="modal fade  cmn-popup default-popup large" id="complete-boooking-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
            </svg>

        </button>
      </div>
      <div class="modal-body" style="background-image: url({{URL::asset('media/images/plane-bg-popup.png')}})">
        <h2>Success!</h2>
        <p class="bold text">You’ve successfully booked “The California Fresh” for 07/17/2020 to 07/20/2020.  We will send you a confirmation email shortly.</p>
        <div class="footer-submit-btn">
            <button class="popup-footer-submit">View My Stays</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->

<!-- Modal -->
<div class="modal fade  cmn-popup" id="tripperpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-popup-logo.png')}}" alt="tripper">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Offers are only available to Trippers</h4>
        <p>Sign up to be a Tripper and get discounts on reservations, access to exclusive properties, and the option to negotiate pricing with owners.</p>
        <ul style="background-image: url({{URL::asset('media/images/Rectangle_Copy_2-4.png')}})">
            <li><img class="lazy-load" data-src="{{URL::asset('media/images/tick-image.png')}}" alt="tripper">BEST PRICES</li>
            <li><img class="lazy-load" data-src="{{URL::asset('media/images/tick-image.png')}}" alt="tripper">EXCLUSIVE PROPERTIES</li>
            <li><img class="lazy-load" data-src="{{URL::asset('media/images/tick-image.png')}}" alt="tripper">SPECIAL OFFERS</li>
        </ul>
        <span class="select-bg-image"><img class="lazy-load" data-src="{{URL::asset('media/images/select-round-arrow.png')}}" alt="tick"></span>
        <a href="{{route('tripper.index')}}" class="blue-gradient">Start  Saving!</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade  cmn-popup" id="makeoffer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="offer-popup-content">
            <form name="makeoffer_form" id="makeoffer_form">
                <input type="hidden" class="property_id" name="propertyId" id="hiddenMakeAnOfferPropertyId"/>
                <input type="hidden" class="owner_id" name="ownerId" id="hiddenMakeAnOfferOwnerId"/>
                <input type="hidden" name="tripper_id" id="hiddenMakeAnOfferTripperId" value="{{ checkLoggedInAndGetRentalUserId() }}" />
                <input type="hidden" class="numberOfGuests" name="numberOfGuests" id="hiddenMakeAnOfferNumberOfGuests"/>
                <div class="modal-header">
                    <h5 class="modal-title" style="text-transform: capitalize;" id="exampleModalLongTitle">Offer Card</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="header-image">
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/offer-img.png')}}" alt="offer" />
                    </div>
                    <div class="title">
                        <h4>CAFIFORNIA FRESH</h4>
                        <p>1420 Torrey Pines  |  Daytona, Florida 12345</p>
                    </div>
                </div>
                <div class="chekin-checkout-dates mobile-chekin-checkout-dates">
                    <div class="row">
                        <div class="col-md-4 col-checkin">
                            <div class="display-inline">
                                <span>Check-In Date</span>
                                <div class="input-group date" >
                                    <input type="text" class="form-control" value="05/20/2017" id="kt_datepicker_5" name="tripperOfferStartDate"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-checkin">
                            <div class="display-inline">
                                <span>Check-Out Date</span>
                                <div class="input-group date" >
                                    <input type="text" class="form-control" value="05/20/2017" id="kt_datepicker_6" name="tripperOfferEndDate"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mobile-chkinout-chkbox">
                            <span></span>
                            <label class="switchcstm">
                            <input type="checkbox" checked name="flexibleDates" value="1" id="flexible_dates">
                            <span class="slidercstm round"></span><span class="text flexibleDates-text">Dates are flexible</span>
                            </label>
                        </div>
                         <span class="availabelDtsErrOffer"></span>
                    </div>
                </div>
                <div class="currnt-night-price">
                    <div class="row">
                        <div class="col-8">
                            Current Nightly Rate
                        </div>
                        <div class="col-4 price_night">
                            $390
                        </div>
                    </div>
                </div>
                <div class="proposed-night-rate">
                    <h3>Proposed Nightly Rate</h3>
                    <div class="plus-minus-sec">
                        <span class="guest-minus minus transition">-</span>
                        <div class="propos-rates-block">
                            <span class="dollar-sign">$</span>
                            <input id="tet123" name="tripperOfferNightlyPrice" type="number" value="1" class="cmn-add"/>
                        </div>
                        <span class="guest-plus plus transition">+</span>
                    </div>
                </div>
                <div class="offer-bottom-form">
                        <textarea placeholder="Message" name="message" id="offer_message"></textarea>
                        <button type="submit"><img class="lazy-load" data-src="{{URL::asset('media/images/submit-offer.png')}}" alt="submit offer" /></button><br>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
        <div class="offer-popup-success text-center" style="display:none;">
            <div class="modal-header">
                <h5 class="modal-title" style="text-transform: capitalize; font-size:20px;">Success!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <p>Your offer has been submitted to the owner.  You will be notified when your offer has been accepted, declined or countered by the owner.</p>
            <a href="{{ route('web.tripper.myOffers') }}" class="view-my-offers">View My Offers</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cancellation-policy" tabindex="-1" role="dialog" aria-labelledby="cancellation-policy" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-primary svg-icon-3x">
                           ×
                    </span>
                </button>

                <div class="row">
                    <div class="col-md-12">
                        <div class="cancellation-sec">
                            <div class="form-group">
                                <div class="radio-list">
                                    <label class="radio">
                                        <input type="radio" checked="checked" name="cancellation">
                                        <span></span>
                                        <div class="w-100"><span class="f-w-700 common-fatfrank">
                                                @if($propertyDetails->cancellation_type =='Flexible')
                                                Flexible Cancellation Policy: </span>
                                            <div class="cancellation_description">Guests can request a full refund within a limited period. Cancellation requests received at least 24 hours before the check-in time will be given full refund.</div>
                                            @elseif($propertyDetails->cancellation_type =='Moderate')
                                            Moderate Cancellation Policy: </span>
                                            <div class="cancellation_description">Guests can request a refund. If a guest cancels within 5 days of the check-in date, then the first night of the booking will not be refunded and only 50% of the accommodation fees for the rest of the booking will then be refunded. If the guest decides to cancel after check-in then 50% of the accommodation fees for any unspent nights are refundable. However, any nights spent will not be refunded.</div>
                                            @else
                                            Strict Cancellation Policy: </span>
                                            <div class="cancellation_description">Guests may receive a full refund if they cancel within 48 hours of booking and at least 14 full days prior to the listing’s local check-in time. After 48 hours guests are only entitled to a 50% refund regardless of how far the check-in date is.</div>
                                            @endif
                                        </div>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="instant_booking_success_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="background-image: url('images/plane-bg.png')">
        <div class="description text-center">
            <h4>Success! </h4>
            <h5>
                Your reservation request is confirmed! You have booked {{$propertyDetails->title}} from {check_in_date} to {check_out_date}, the owner will reach out to you shortly!
            </h5>
            <a href="#" class="btn btn-default dashboard-btn instant_booking_success_close">Ok</a>
        </div>
        </div>
    </div>
</div>
 <!-- Modal -->
    <div class="modal fade custom-modal" id="Contacthost" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Contacthost" aria-modal="true">
        <div class="modal-dialog modal-lg cust-modal-lg modal-dialog-centered" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <h1> Contact Host</h1>
                    <div class="modal-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                        <div class="contact-host-wrapper">
                            <div class="form-group">
                                <textarea class="form-control" id="text1" rows="3" placeholder="Type your message here...."></textarea>
                            </div>

                            <div class="modal-action">
                                <button type="button" id="save_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5" data-owner-id ="{{(new \App\Helpers\Helper)->urlsafe_b64encode($propertyDetails->ownerdetails->id)}}" disabled>Send Message to Host</button>
                                <button type="button" id="cancel_btn" class="btn btn-800 btn-link mt-5 close" data-dismiss="modal" aria-label="Close">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!-- End Modal -->
 <div class="modal fade custom-modal" id="contact_host_success" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Contacthost" aria-modal="true">
        <div class="modal-dialog modal-lg cust-modal-lg modal-dialog-centered" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <h1> Success!</h1>
                    <div class="modal-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                        <div class="contact-host-wrapper">


                            <div class="modal-action">
                                <button type="button" class="button-default-animate button-default-custom btn-sunset-gradient mt-5" data-dismiss="modal" aria-label="Close" >OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade custom-modal" id="not_instant_booking_success_modal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Contacthost" aria-modal="true">
    <div class="modal-dialog modal-lg cust-modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" >
        <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
            <div class="modal-body bg-white" style="background-image: url({{ asset('media/images/plane-bg.png') }});background-repeat: no-repeat;background-position: center;">
                <h1> Success!</h1>
                <div class="modal-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                    <div class="contact-host-wrapper">

                        @php
                            // $formatMoneyShow = $propertyDetails->
                        @endphp
                        <h3>TOTAL TRIPPER SAVINGS TODAY: <span id="not_instant_booking_price"></span></h3>
                        <br/>
                        <p>We've sent you booking request to the owner. They have 24 hours to approve your request. Upon approval you will receive a booking and payment confirmation email. If approval is denied we will notify you and provide suggestions for similar stays.</p>

                        <div class="modal-action">
                            <a href="{{ route('rental.mystay') }}" class="btn btn-default dashboard-btn instant_booking_success_close">View My Stays</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerJs')
    @parent
<script>
    var addExistingURL = "{{route('rental.api.tripboard.add.property') }}";
    var likeUrl = "{{route('rental.api.tripboard.like') }}";
    var unlikeUrl = "{{route('rental.api.tripboard.unlike') }}";
    var property_id = "{{ Request::route('id') }}";
    var fDate = "{{request()->get('form_date')}}";
    var tDate = "{{request()->get('to_date')}}";
    var guest = "{{request()->get('guest')}}";
    var propertyInstantBooking = "{{route('propertydetails.propertyInstantBooking') }}";
    var contacthost = "{{route('conatct.host.send')}}";
    var chatURL = "{{route('rental.chat')}}";
    var checkBooking = "{{route('propertydetails.checkBooking') }}";
    var letsGo = "{{URL::asset('media/images/tripper-page/lets-go-btn.png')}}";
    var propertyId = "{{$propertyDetails->id}}";
    var isInstantBookingAllowed = "{{$propertyDetails->is_instant_booking_allowed}}";
    var isloggedIn = "{{ Auth::guard('rentaluser')->check()}}";
    var is_tripper = "{{ Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper ?  Auth::guard('rentaluser')->user()->is_tripper : 0 }}";
    var reviewPage = "{{route('review.index') }}";
    var propertyImagePath ="{{URL::asset('uploads/property/')}}";
    var makeOffer ="{{route('rental.api.makeoffer') }}";
    var makeAnPropertyOfferURL = '{{ route('api.tripper.propertyOffers.makeAnPropertyOffer',['tripperId' => '#tripperId', 'propertyId' => '#propertyId']) }}';
    var offerFlagPrice = "{{($propertyDetails->offer_price)?$propertyDetails->offer_price:10}}";
    var propertyPrice = "{{($propertyDetails->price)?$propertyDetails->price:10}}";
    var guestLimit = "{{($propertyDetails->no_of_guest)?$propertyDetails->no_of_guest:30}}";
    var getPropertyType = "{{$propertyDetails->type_of_property}}";
    var vacationRentalMarker = "{{URL::asset('media/images/vacation-club.png')}}";
    var privateResidenceMarker = "{{URL::asset('media/images/private-residence.png')}}";

    $(document).ready(function() {
        $('.like-button').hover(
       function(){ $("#favorticon").addClass('hover like-open') },
       function(){ $("#favorticon").removeClass('hover') }
        )
        $('.dropdown-menu').on('hide.bs.dropdown', function () {
            $("#favorticon").removeClass('like-open')
        });
        $('.add-to-existing ').hover(
          function(){
              console.log(1);
                var type = $(this).find('.favorite-item a').attr('data-type');
                if(type == 'save'){
                    $(this).find('.favorite-item a').html('Remove');
                    $(this).find('.favorite-item a').removeClass('active');
                }else if(type == 'remove'){
                    $(this).find('.favorite-item a').html('Save');
                    $(this).find('.favorite-item a').removeClass('active');
                }
          },
          function(){
                var type = $(this).find('.favorite-item a').attr('data-type');
                if(type == 'save'){
                    $(this).find('.favorite-item a').html('<i class="fas fa-check"></i>Saved');
                    $(this).find('.favorite-item a').addClass('active');
                }else if(type == 'remove'){
                    $(this).find('.favorite-item a').html('<i class="fas fa-check"></i>Removed');
                    $(this).find('.favorite-item a').addClass('active');
                }
          },
        );
        priceChange();
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            dots: true,
            autoHeight: true,
            prevArrow: '<button class="thumbnail-custom-arrows prev"><span class="prev" >‹</button>',
            nextArrow: '<button class="thumbnail-custom-arrows next"><span class="next" >›</button>'
        });
        jQuery('.slider-nav').slick({
            slidesToShow: false,
            slidesToScroll: false,
            vertical: false,
            dots: false,
            prevArrow: '<button class="thumbnail-custom-arrows prev"><span class="prev" >‹</button>',
            nextArrow: '<button class="thumbnail-custom-arrows next"><span class="next" >›</button>',
            focusOnSelect: true
        });

        $('.image-thumb-button .play-video').on('click', function (e) {
            e.preventDefault();
            let id = $(this).data('imageid');
            if($('video#'+id).get(0).paused == true){
                $('video#'+id).get(0).play();
                $(this).find('i').attr('class', 'fa fa-pause');
                return false;
            }else{
                $('video#'+id).get(0).pause();
                $(this).find('i').attr('class', 'fa fa-play');
            }
        });

        jQuery('#propert-detail-page-slider').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            dots: false,
            responsive: {
                0: {
                    items: 3
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });

        $('.like-button').click(function(e) {

            var isLike = $(this).attr('data-val');
            var url = likeUrl;

            if (url != '') {
                $.ajax({

                    type: "POST",
                    url: url,
                    dataType: "JSON",
                    headers: {
                        "Authorization": "Bearer {{session()->get('rental_token')}}",
                    },
                    data: {
                        property_id: property_id
                    },
                    success: function(data) {
                        if (data.success == true) {
                            isLike = (isLike == 1) ? 0 : 1;

                            $('.like-button').attr('data-val', isLike);
                            if (isLike == 1) {
                                $("#favorticon").addClass('active like-open');

                            } else {
                                $("#favorticon").removeClass('active like-open');
                                $('.pdp-dropdown-menu').removeClass('show');
                                $('.pdp-dropdown-menu').find('.dropdown-menu').removeClass('show');
                                $('.pdp-dropdown-menu').find('.dropdown-menu').hide();
                            }


                        }

                    },
                    error: function(xhr, status, error) {

                    }


                });
            }
        })

        $('.add-to-existing').click(function(e) {

            var tripboard_id = $(this).attr('data-id');
            $.ajax({

                type: "POST",
                url: addExistingURL,
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('rental_token')}}",
                },
                data: {
                    property_id: property_id,
                    tripboard_id: tripboard_id
                },
                success: function(data) {

                    if (data.success = true) {
                        $("#trip_" + tripboard_id).addClass('active');
                        $("#trip_" + tripboard_id + " .favorite-item a").addClass('active');
                        if(data.type == 'save'){
                            $("#trip_" + tripboard_id + " .favorite-item a").attr('data-type',data.type);
                            $("#trip_" + tripboard_id + " .favorite-item a").html('<i class="fas fa-check"></i>Saved');
                        }else{
                            $("#trip_" + tripboard_id + " .favorite-item a").attr('data-type',data.type);
                            $("#trip_" + tripboard_id + " .favorite-item a").html('<i class="fas fa-check"></i>Removed');
                        }
                    }

                },
                error: function(xhr, status, error) {

                }


            });
        });

        $('#addTripBoard').validate({
            rules: {
                tripboard_name: {
                    required: true
                }
            },
            messages: {
                tripboard_name: {
                    required: "Please add tripboard name."
                }
            }
        });

        $('.btn-tripboard-fly').click(function(e) {

            e.preventDefault();
            var inputBoard = $('.input-board').val();

            if (!$('#addTripBoard').valid()) {
                return false;
            }

            $.ajax({

                type: "POST",
                url: "{{ route('rental.api.tripboard.add') }}",
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('rental_token')}}",
                },
                data: new FormData($('#addTripBoard')[0]),
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data.success = 'true') {
                        var redirect_url = data.data['redirect_url'];
                        window.location.href = redirect_url;

                    }
                },
                error: function(xhr, status, error) {

                }


            });
        });
        jQuery('.fixed-items.keep-open').on('click', function(e) {
            e.stopPropagation();
        });



        jQuery('#kt_datepicker_3').datepicker({
            startDate: new Date(),
            format: 'mm/dd/yyyy',
            timepicker: false,
            autoclose: true,
            todayHighlight: false,
        }).on('changeDate', function(e) {
            if(!window.location.href.indexOf('?') == -1){
                $("#kt_datepicker_4").datepicker("destroy");
                var date2 = $('#kt_datepicker_3').datepicker('getDate', '+1d');
                date2.setDate(date2.getDate()+1);
                $('#kt_datepicker_4').datepicker('setStartDate', date2);
                date2.setDate(date2.getDate()+2);
                $('#kt_datepicker_4').datepicker('setDate', date2);

            }else{
                var date2 = $('#kt_datepicker_3').datepicker('getDate', '+1d');
                date2.setDate(date2.getDate()+1);
                $('#kt_datepicker_4').datepicker('setStartDate', date2);
                $('#kt_datepicker_4').datepicker('setDate', tDate);
                if(!moment($('#kt_datepicker_3').val()).isSame(fDate)){
                    $("#kt_datepicker_4").datepicker("destroy");
                    var date2 = $('#kt_datepicker_3').datepicker('getDate', '+1d');
                    date2.setDate(date2.getDate()+1);
                    $('#kt_datepicker_4').datepicker('setStartDate', date2);
                    date2.setDate(date2.getDate()+2);
                    $('#kt_datepicker_4').datepicker('setDate', date2);
                }
            }
        });

        $('#kt_datepicker_4').datepicker({
            autoApply: true,
            format: 'mm/dd/yyyy',
            startDate: new Date(),
            minDate: new Date(),
            autoclose: true,
        }).on('changeDate', function(start) {
            $('.error-span').remove();
            $('.availabelDtsErr').hide();
            $('.guest-info').removeClass('error');
            if(!moment($('#kt_datepicker_3').val()).isSame($('#kt_datepicker_4').val())){
                $('#lets_go_tab').find('#lets_go_0').bind('click', true);

                var minDate = start.format();
                $(this).datepicker('hide');
                var form_date =  new Date($('#kt_datepicker_3').val());
                var to_date = new Date($('#kt_datepicker_4').val());
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var diffDays = Math.round((to_date-form_date)/oneDay);
                $('.tripper_days').text(diffDays);
                $('.regular_days').text(diffDays);
                priceChange();
            }else{
                $('<span class="error-span">You can not select same dates</span>').insertAfter('.guest-info');
                $('#lets_go_tab').find('#lets_go_0').bind('click', false);
                return false;
            }

        });

        if(fDate == ''){
            fDate = moment().format('MM/DD/YYYY');
            tDate = moment().add(2,'days').format('MM/DD/YYYY');
        }
        $('.availabelDtsErr').hide();
        $('.guest-info').removeClass('error');

        $('#kt_datepicker_4').datepicker('setDate',tDate);
        $('#kt_datepicker_3').datepicker('setDate',fDate);
        var form_date =  new Date($('#kt_datepicker_3').val());
        var to_date = new Date($('#kt_datepicker_4').val());

        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var diffDays = Math.round((to_date-form_date)/oneDay);
        $('.tripper_days').text(diffDays);
        $('.regular_days').text(diffDays);
        if(guest == ''){guest=$('#guests').attr('data-value');}
        $('#guests').val(guest);
        priceChange();

        $('.instant_booking_success_close').click(function() {
            $('#instant_booking_success_modal').modal('hide');
            location.reload();
        });


    });

    function priceChange(){
        var tripperPrice = $('.tripper_price').text().replace(/,/g, '');
        var days = $('.tripper_days').text();
        var cleaningFee = $('.cleaning_fee_tripper').text();
        var rentalCleaningFee = $('.renter_cleaning_fee').text();
        var serviceFee = $('.service_fee').attr('data-value');
        var occupancyFee = $('.occupancy_fee').text();
        var regularPrice = $('.regular_price').text().replace(/,/g, '');
        var serviceType = $('.service_fee').attr('data-type');
        if(serviceType == 2){
            serviceFee = (serviceFee / 100) *  (regularPrice * days);
        }

        var totalTripper = parseFloat(tripperPrice*days) + parseFloat(cleaningFee);
        var totalRegular = parseFloat(regularPrice * days) + parseFloat(rentalCleaningFee) + parseFloat(serviceFee);

        let totalTripperPayoutFee = Math.round(HelperFunction.getPayOutFee(parseFloat(tripperPrice*days)) + HelperFunction.getPayOutFee(parseFloat(cleaningFee)) +0.25);
        let totalRegularPayoutFee = Math.round(HelperFunction.getPayOutFee(parseFloat(regularPrice*days)) + HelperFunction.getPayOutFee(parseFloat(rentalCleaningFee)) +0.25);


        let displayOccupanyFee = HelperFunction.getOccupancyFee(parseFloat(totalRegular));
        let displayTripperOccupanyFee =  HelperFunction.getOccupancyFee(parseFloat(totalTripper));

        totalTripper  = totalTripper + displayTripperOccupanyFee +totalTripperPayoutFee;
        totalRegular = totalRegular + displayOccupanyFee + totalRegularPayoutFee;

        var transactionFee = HelperFunction.getTransactionFee(parseFloat(totalTripper));
        var rentalServiceFee = HelperFunction.getTransactionFee(parseFloat(totalRegular));


        totalTripper  = totalTripper + parseFloat(transactionFee);
        totalRegular = totalRegular + parseFloat(rentalServiceFee);
        rentalServiceFee = parseFloat(rentalServiceFee) + parseFloat(serviceFee);


        let displayTransactionFee = transactionFee ;
        let displayServiceFee = rentalServiceFee ;

        var competitorFee =  parseFloat(0.13 * (regularPrice * days));
        var rentercompFee = parseFloat(competitorFee - displayServiceFee);
        var trippercompFee = parseFloat(competitorFee - displayTransactionFee);

        $('.service_fee').text(HelperFunction.numberWithCommas(displayServiceFee));
        $('.transication_fee').text('$'+HelperFunction.numberWithCommas(parseFloat(displayTransactionFee)));
        $('.days_tripper').text('$'+HelperFunction.numberWithCommas(parseFloat(tripperPrice*days)));
        $('.tripper_total').text('$'+HelperFunction.numberWithCommas(totalTripper));
        $('.days_regular').text('$'+HelperFunction.numberWithCommas(parseFloat(regularPrice * days)));
        $('.regular_total').text('$'+HelperFunction.numberWithCommas(totalRegular));
        $('.total_save').text('$'+HelperFunction.numberWithCommas((parseFloat(totalRegular.toFixed(2) - totalTripper.toFixed(2))).toFixed(2)));
        $('.competitor-fee').text('$'+HelperFunction.numberWithCommas(Math.round(competitorFee)));
        $('.renter-comp span').text('~$'+HelperFunction.numberWithCommas(Math.round(rentercompFee)));
        $('.tripper-comp span').text('~$'+HelperFunction.numberWithCommas(Math.round(trippercompFee)));
        $('.occupancy_fee').text(HelperFunction.numberWithCommas(displayOccupanyFee));
        $('.occupancy_fee_tripper').text(HelperFunction.numberWithCommas(displayTripperOccupanyFee));
    }
    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    }

    function priceChangeOnNotInstantBooking(totalRegular,totalTripper)
    {
        if(is_tripper > 0) {
            $("#not_instant_booking_price").html("$"+commaSeparateNumber(totalTripper));
        } else {
            $("#not_instant_booking_price").html("$"+commaSeparateNumber(totalRegular));
        }
    }


jQuery( document ).ready(function() {
    $(document).click(function(e) {

        if (!$(e.target).is('.like-open, .like-open *')) {
            $('.pdp-dropdown-menu').removeClass('show');
            $('.pdp-dropdown-menu').find('.dropdown-menu').removeClass('show');
            $('.pdp-dropdown-menu').find('.dropdown-menu').hide();
        }
    });
    var windowSize = $(window).width();

    if (windowSize > 767) {
        $('.pdp-dropdown-menu').on('show.bs.dropdown', function() {
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
        });

        // Add slideUp animation to Bootstrap dropdown when collapsing.
        $('.pdp-dropdown-menu').on('hide.bs.dropdown', function(e) {

            if (!$(e.target).is('.like-open, .like-open *')) {
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
                $(".dropdown-menu").css("transform", "translate3d(37px, 34px, 0px)");
                $(".dropdown-menu").css("display", "none");
            }{
                return false;
            }

        });
    }
});

jQuery( window ).resize(function() {
    var windowSize = $(window).width();

        if (windowSize > 767) {
            $('.pdp-dropdown-menu').on('show.bs.dropdown', function() {
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
            });

            // Add slideUp animation to Bootstrap dropdown when collapsing.
            $('.pdp-dropdown-menu').on('hide.bs.dropdown', function(e) {

            if (!$(e.target).is('.like-open, .like-open *')) {
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
                $(".dropdown-menu").css("transform", "translate3d(37px, 34px, 0px)");
                $(".dropdown-menu").css("display", "none");
            }{
                return false;
            }

        });
        }


});
</script>
<script src="{{URL::asset('js/property_detail/property_detail.js')}}"></script>
@endsection
