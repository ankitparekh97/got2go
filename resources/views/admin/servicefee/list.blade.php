@extends('layouts.adminApp')
@section('content')
<!-- Content Wrapper -->
<div id="success" class="custom-alert-msg alert alert-success" style="display:none">
    <em class="pr-3 fa fa-check" aria-hidden="true"></em>
    <div id="showmsg" ></div>
</div>
<div id="error" class="custom-alert-msg alert alert-danger" style="display:none">
    <em class="pr-3 fa fa-times" aria-hidden="true"></em>
    <div id="showerrormsg" ></div>
</div>
<div id="content-wrapper" class="d-flex flex-column pt-4">
<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->

        <h1 class="mb-2 title-page">GOT2GO Service Fee</h1>

        <form class="user">
        @csrf
        @php $fee_ratio = @explode(',',$data->service_fee); @endphp
            <input type="hidden" id="service_id" name="service_id" value="{{ ($data && $data->id) ? $data->id : ''}}">
            <input type="hidden" id="serviceFeeValues" name="serviceFeeValues" value="{{ @$fee_ratio[0]}}">
            <input type="hidden" id="serviceFeePercentageValues" name="serviceFeePercentageValues" value="{{@$fee_ratio[1]}}">
            <div class="form-group">
                <label class="label-text mb-4">Service Fee Amount</label>

                <div class="radio-inline mb-4">

                <input type="hidden" id="service_value" name="service_value" value="{{@$fee_ratio[0]}}">
                    <label class="radio pr-5">
                        <input type="radio" value="1" onclick="showdollor()" @if(@$fee_ratio[1] == 1 || @$fee_ratio[1] == '') checked @endif name="ratio" />
                        <span>Fixed amount</span>
                    </label>
                    <label class="radio">
                        <input type="radio" value="2" onclick="hidedollor()" @if(@$fee_ratio[1] == 2) checked @endif name="ratio" />
                        <span>Percent of transaction</span>

                    </label>
                </div>
                <div class="input-group dollar-control" >
                    <span class="fa fa-dollar-sign pt-2 pr-2" id="showdollorid" @if(@$fee_ratio[1] == 1) style="display:block" @else style="display:none" @endif ></span>
                    <input type="text" maxlength="6" class="form-control form-control-dollar" id="servicefee" name="servicefee" aria-describedby="dollar" placeholder="" value="{{@$fee_ratio[0]}}" onkeypress="return validateFloatKeyPress(this,event);">
                    <span class="fa fa-percent pt-2 ml-2" id="hidepercentage" @if(@$fee_ratio[1] == 2) style="display:block" @else style="display:none" @endif></span>

                </div>
                <div class="input-group mb-4" ><label class="error" id="discountPercentageError" style="display: none;">
                            Discount percentage cannot be greater than 99.99%.
                    </label> </div>
            </div>

            <a href="#" data-toggle="modal" data-target="#saveFee" onclick="saveservicefee(this)" class="btn button-default-custom btn-sunset-gradient mt-2">
                Save
            </a>
        </form>
    </div>
</div>
<!-- Modal -->
<div id="saveFee" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                <span id="status_show"></span><h1> Are you sure you want to change the GOT2GO service fee?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "saveServeiceFee(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
<style src="{{ URL::asset('js/admin/servicefee/servicefee.css') }}"></style>
<script>
    var serviceFeeSave = "{{route('servicefeesave')}}";
</script>
<script src="{{ URL::asset('js/admin/servicefee/servicefee.js') }}"></script>
@endsection
