@extends('layouts.adminApp')
@section('content')

    <div id="content-wrapper" class="d-flex flex-column pt-4">
        <!-- Main Content -->
        <div id="content">
            <div class="container-fluid">
                <h1 class="mb-2 title-page">Coupon Codes</h1>

                <form class="subscription" id="formCouponCode" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="alert alert-success" id="showsuccessmsgsub" style="display:none"></div>
                        <div class="alert alert-danger" id="showerrormsgsub" style="display:none"></div>

                        <div class="row justify-content-center align-items-center mt-4">
                            <div class="col-xl-4 col-lg-6 col-sm-12 mb-2">
                                <label for="coupon_code" class="label-text">Coupon Code</label>
                                <div class="input-group w-75 mt-2">

                                    <input type="text" class="form-control border-0 form-control-dollar" id="coupon_code" name="coupon_code" value="" maxlength="20" />
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-sm-12 mb-2">
                                <label for="discount_percentage" class="label-text">Discount Percentage</label>
                                <div class="input-group w-75 mt-2 custom-icon-perc">
                                    <span class="percentage-icon">
                                        <svg width="11" height="12" viewBox="0 0 11 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1.6543 3.98633C1.6543 4.76107 1.73861 5.34212 1.90723 5.72949C2.07585 6.11686 2.34928 6.31055 2.72754 6.31055C3.47493 6.31055 3.84863 5.53581 3.84863 3.98633C3.84863 2.44596 3.47493 1.67578 2.72754 1.67578C2.34928 1.67578 2.07585 1.86719 1.90723 2.25C1.73861 2.63281 1.6543 3.21159 1.6543 3.98633ZM4.78516 3.98633C4.78516 5.02539 4.6097 5.81152 4.25879 6.34473C3.91243 6.87337 3.40202 7.1377 2.72754 7.1377C2.08952 7.1377 1.59277 6.86654 1.2373 6.32422C0.886393 5.7819 0.710938 5.0026 0.710938 3.98633C0.710938 2.95182 0.879557 2.17253 1.2168 1.64844C1.55859 1.12435 2.06217 0.862305 2.72754 0.862305C3.38835 0.862305 3.89648 1.13346 4.25195 1.67578C4.60742 2.2181 4.78516 2.98828 4.78516 3.98633ZM7.66992 7.99219C7.66992 8.77148 7.75423 9.35482 7.92285 9.74219C8.09147 10.125 8.36719 10.3164 8.75 10.3164C9.13281 10.3164 9.41536 10.1273 9.59766 9.74902C9.77995 9.36621 9.87109 8.7806 9.87109 7.99219C9.87109 7.21289 9.77995 6.63639 9.59766 6.2627C9.41536 5.88444 9.13281 5.69531 8.75 5.69531C8.36719 5.69531 8.09147 5.88444 7.92285 6.2627C7.75423 6.63639 7.66992 7.21289 7.66992 7.99219ZM10.8076 7.99219C10.8076 9.02669 10.6322 9.81055 10.2812 10.3438C9.9349 10.8724 9.42448 11.1367 8.75 11.1367C8.10286 11.1367 7.60384 10.8656 7.25293 10.3232C6.90658 9.78092 6.7334 9.00391 6.7334 7.99219C6.7334 6.95768 6.90202 6.17839 7.23926 5.6543C7.58105 5.13021 8.08464 4.86816 8.75 4.86816C9.39714 4.86816 9.90072 5.13704 10.2607 5.6748C10.6253 6.20801 10.8076 6.98047 10.8076 7.99219ZM9.04395 1.00586L3.5 11H2.49512L8.03906 1.00586H9.04395Z" fill="#939FA4"/>
                                        </svg>
                                    </span>
                                    <input type="text" class="form-control border-0 form-control-dollar" id="discount_percentage" name="discount_percentage" value="" />
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-6 col-sm-12">
                                <div class="custom-checkbox">
                                    <label class="checkbox-outer">Single Use Coupon
                                      <input type="checkbox" class="form-control border-0 form-control-dollar" id="is_single_use" name="is_single_use" value="1" />
                                      <span class="checkmark"></span>
                                    </label>
                               </div>
                            </div>
                            <div class="col-xl-2 col-lg-6 col-sm-12 mb-2">
                                <button type="submit" href="#" id="saveCouponCode" class="btn button-default-custom btn-sunset-gradient my-3" >Save</button>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- DataTales Example -->
                <div class="card mb-4 no-bg-custom">
                    <div id="loader_spin" style="display:none">
                        <img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader">
                    </div>

                    <div class="card-body no-bg-custom">
                        <div class="table-responsive">
                            <div class="alert alert-success" id="showmsg" style="display:none"></div>
                            <table id="couponcodeList" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                                <caption></caption>
                                <thead>
                                <tr>
                                    <th scope="col"> Coupon Codes</th>
                                    <th scope="col"> Discount Percentage </th>
                                    <th scope="col"> Date Created </th>
                                    <th scope="col"> # of Times Used </th>
                                    <th scope="col"> Single use? </th>
                                    <th scope="col"> Status </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="activate" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                    <input type="hidden" id="coupon_code_id" name="coupon_code_id">
                    <span id="status_show"></span><h1> Are you sure you want to activate this coupon code?</h1></div>
                <div class="modal-footer">
                    <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "activateCouponCode()">Yes</a></div>
                    <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="deActivate" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                    <input type="hidden" id="coupon_code_id" name="coupon_code_id">
                    <span id="status_show"></span><h1> Are you sure you want to deactivate this coupon code?</h1></div>
                <div class="modal-footer">
                    <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "deActivateCouponCode()">Yes</a></div>
                    <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('headerCss')
    @parent
    <style src="{{ asset('js/admin/usermanagement/usermanagement.css') }}"></style>
@endsection

@section('footerJs')
    @parent
    <script>
        const saveCouponCodeRoute = '{{ route('api.admin.saveCouponCode') }}';
        const getAllCouponCodesRoute = '{{ route('api.admin.getAllCouponCodes') }}';
        const checkDuplicateCouponCodeRoute = '{{ route('api.admin.checkDuplicateCouponCode',['couponCodeName' => '#couponCodeName']) }}';
        const activateCouponCodeRoute = '{{ route('api.admin.activeCouponCode',['couponCodeId' => '#couponCodeId']) }}';
        const deActivateCouponCodeRoute = '{{ route('api.admin.deActiveCouponCode',['couponCodeId' => '#couponCodeId']) }}';
    </script>
    <script src="{{ asset('js/admin/couponCode/couponCode.js') }}"></script>
@endsection
