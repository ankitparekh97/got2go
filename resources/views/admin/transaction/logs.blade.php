@extends('layouts.adminApp')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column pt-4">
<!-- Main Content -->
<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="mb-2 title-page">Transaction Logs</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
        <div id="loader_spin" style="display:none"><img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader"></div>                   
            <div class="card-body">            
                <div class="table-responsive">
                <table id="transactionLogs" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                <caption></caption>                
                <thead>
                    <tr>                    
                        <th scope="col"> Reservation #</th>                        
                        <th scope="col"> Dates </th>
                        <th scope="col"> Transaction Amount </th>
                        <th scope="col"> Property Name</th>
                        <th scope="col"> Location</th>
                        <th scope="col"> Renter Name</th>
                        <th scope="col"> Owner Name</th>
                        <th scope="col"> Status</th>                                    
                        <th scope="col"> Action</th> 
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<div id="refund" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">                    
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                <input type="hidden" name="transaction_id" id="transaction_id">
                <h1>Are you sure you want to refund <span id="renter_name">{renter_name}</span> for their stay <span id="reservation_dates">{reservation_dates}</span>?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-target="#paymentrefund" data-toggle="modal" onclick = "paymentrefunded(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                
                    
                </div>
            </div>
        </div>
</div>
<!-- Modal -->
<div id="paymentrefund" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">                    
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div id="success" class="custom-alert-msg alert alert-success" style="display:none">
                    <em class="pr-3 fa fa-check" aria-hidden="true"></em>
                    <div id="showmsg" ></div>
                </div>
                <div id="error" class="custom-alert-msg alert alert-danger" style="display:none">
                    <em class="pr-3 fa fa-times" aria-hidden="true"></em>
                    <div id="showerrormsg" ></div>
                </div>  
                </div>
                <div class="modal-body">
                 
                <input type="hidden" name="transaction_id" id="transaction_id">
                <h1>Enter refund amount</h1>
                <div class="form-group">
                <div class="radio-inline mt-4">
                    <label class="radio radio-popup pr-5">
                        <input type="radio" value="1" onclick="showdollor()" checked name="ratio" />
                        <span>Fixed amount</span>
                    </label>
                    <label class="radio radio-popup">
                        <input type="radio" value="2" onclick="hidedollor()" name="ratio" />
                        <span>Percent of transaction</span>
                    </label>
                </div>
                <div class="input-group dollar-control-transaction" >                    
                    <span class="fa fa-dollar-sign pt-2 pr-2" id="showdollorid" ></span>                    
                    <input type="text" maxlength="8" class="form-control form-control-dollar" id="servicefee" name="servicefee" aria-describedby="dollar" onkeypress="return validateFloatKeyPress(this,event);">                    
                    <span class="fa fa-percent pt-2 ml-2" id="hidepercentage" style="display:none"></span>                    
                </div>
            </div>
            </div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" onclick = "paymentrefundConfirm(this)">Submit</a></div>
                <div class="d-block w-100 mt-2"><button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">Cancel</button></div>
                
                                        
                </div>
            </div>
        </div>
</div>
<style src="{{ URL::asset('js/admin/transaction/transaction.css') }}"></style>
<script>
    var loaderImg = "{{URL::asset('uploads/users/loader.gif')}}";
    var transactionList = "{{route('transaction-list')}}";
    var refundAmount = "{{route('refundBooking')}}";
</script>
<script src="{{ URL::asset('js/admin/transaction/transaction.js') }}"></script>
@endsection

        