@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Amenities</h1>
    <div>
    <a style="margin: 19px;" href="{{ route('amenity.create')}}" class="btn btn-primary">New Amenity</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Amenity Name</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($amenities as $amenity)
    <tr id="row_{{$amenity->id}}">
            <td>{{$amenity->id}}</td>
            <td>{{$amenity->amenity_name}}</td>
            <td>
                <a href="{{ route('amenity.edit',$amenity->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('amenity.destroy', $amenity->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$amenity->id}},`{{ route('amenity.destroy', $amenity->id)}}`)">Delete</button>
                </form>
                @endforeach
            </td>
        </tr>
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('amenity.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(amenity_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+amenity_id).remove();
                }
            });
        }


</script>
@endsection
