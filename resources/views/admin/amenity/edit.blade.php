@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update an Amenity</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" id="amenity_update" action="{{ route('amenity.update', $amenities->id) }}">
            {{method_field('put')}} 
            @csrf
            <div class="form-group">    
              <label for="amenity_name">Amenity Name:</label>
              <input type="text" class="form-control" name="amenity_name" value={{$amenities->amenity_name}}>
          </div> 
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>
$('#amenity_update').validate({ 
        rules: {
            amenity_name: {
                required: true
            },
        }
    });

$('#amenity_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('amenity.update', $amenities->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection