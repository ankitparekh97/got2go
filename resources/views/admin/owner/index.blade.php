@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
<meta name="csrf-token" content="{{ csrf_token() }}">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Owners</h1>   
    <div>
    <a style="margin: 19px;" href="{{ route('owners.create')}}" class="btn btn-primary">New Owner</a>
    </div>   
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($owners as $owner)
        <tr id="row_{{$owner->id}}">
            <td>{{$owner->id}}</td>
            <td>{{$owner->first_name}}</td>
            <td>{{$owner->email}}</td>
            <td>
                <a href="{{ route('owners.edit',$owner->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('owners.destroy', $owner->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                   <button class="btn btn-danger" type="button"  onclick="distroy({{$owner->id}},`{{ route('owners.destroy', $owner->id)}}`)">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('owners.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(user_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+user_id).remove();
                }
            });
        }


</script>
@endsection