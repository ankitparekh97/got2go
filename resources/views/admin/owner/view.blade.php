@extends('base') 
@section('main')
<h1>Showing Task {{ $owner->first_name." ". $owner->last_name }}</h1>
<div class="jumbotron text-center">
    <p>
        <strong>Name:</strong> {{ $owner->first_name." ".$owner->last_name }}<br>
        <strong>Email:</strong> {{ $owner->email }}
    </p>
</div>
<div class="col-sm-12">
    <h1 class="display-3">Property Booking</h1>
    <div>&nbsp;
    <!-- <a style="margin: 19px;" href="{{ route('properties.create')}}" class="btn btn-primary">New Proeprty</a> -->
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>name</td>
          <td>title</td>
          <td>property_type</td>
          <td>date</td>
          <td >Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($bookings as $booking)
    <tr id="row_{{$booking->booking_id}}">
            <td>{{$booking->booking_id}}</td>
            <td>{{$booking->rentaluser->full_name}}</td>
            <td>{{$booking->property->ownerproperty->title}}</td>
            <td>{{$booking->booking_type}}</td>
            <td>{{ Carbon\Carbon::parse($booking->check_in_date)->format('F-d')}} To {{ Carbon\Carbon::parse($booking->check_out_date)->format('F-d')}}</td>
            
            <td>
            @if($booking->owner_status == 'instant_booking' )
               <span  class="text-success"> Instant Booking</span>
            @else
             <span  class="{{($booking->owner_status == 'approved')?'text-success':(($booking->owner_status == 'pending')?'text-primary':'text-danger')}}">{{$booking->owner_status}}</span>

            @endif

            </td>
            <!-- <td>
                <form action="{{ route('properties.destroy', $booking->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$booking->id}},`{{ route('properties.destroy', $booking->id)}}`)">Delete</button>
                </form>
               
            </td> -->
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection