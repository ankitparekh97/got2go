@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a owner</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" id="owner_update" action="{{ route('owners.update', $owner->id) }}">
            {{method_field('put')}} 
            @csrf
            <div class="form-group">    
              <label for="first_name">First Name:</label>
              <input type="text" class="form-control" name="first_name" value={{$owner->first_name}}>
          </div>
          <div class="form-group">    
              <label for="last_name">Last Name:</label>
              <input type="text" class="form-control" name="last_name" value={{$owner->last_name}}>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email" readonly value={{$owner->email}}>
          </div>

          <div class="form-group">
              <label for="bio">Bio:</label>
              <input type="text" class="form-control" name="bio" value={{$owner->bio}}>
          </div> 

          <div class="form-group">
              <label for="birthdate">Birth Date:</label>
              <input type="text" class="form-control" name="birthdate" value={{$owner->birthdate}}>
          </div>

          <div class="form-group">
              <label for="phone">Phone:</label>
              <input type="text" class="form-control" name="phone" value={{$owner->phone}}>
          </div> 

          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address" value={{$owner->address}}>
          </div>

          <div class="form-group">
              <label for="city">City:</label>
              <input type="text" class="form-control" name="city" value={{$owner->city}}>
          </div>

          <div class="form-grouaddressp">
              <label for="state">State:</label>
              <input type="text" class="form-control" name="state" value={{$owner->state}}>
          </div>  

          <div class="form-group">
              <label for="currency">Currency:</label>
              <input type="text" class="form-control" name="currency" value={{$owner->currency}}>
          </div> 

          <div class="form-group">
              <label for="photo">Photo:</label>
              <input type="file" class="form-control" name="photo" value={{$owner->photo}}>
          </div>  

          <div class="form-group">
              <label for="languages_known">Languages Known:</label>
              <input type="text" class="form-control" name="languages_known" value={{$owner->languages_known}}>
          </div> 
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>
$('#owner_update').validate({ 
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            bio:{
              required: true
            },
            birthdate:{
              required: true
            },
            address:{
              required: true
            },
            city:{
              required: true
            },
            state:{
              required: true
            },
            currency:{
              required: true
            },
            languages_known:{
              required: true
            },
            phone: {
                required: true,
                digits: true
            },
            
            photo: {
                required: true,
                extension: "jpeg|png"
            },
        }
    });

$('#owner_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('owners.update', $owner->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection