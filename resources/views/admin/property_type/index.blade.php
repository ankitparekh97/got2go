@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Property Type</h1>
    <div>
    <a style="margin: 19px;" href="{{ route('propertytype.create')}}" class="btn btn-primary">New Proeprty Type</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Property Type</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($propertiesType as $property)
    <tr id="row_{{$property->id}}">
            <td>{{$property->id}}</td>
            <td>{{$property->property_type}}</td>
            <td>
                <a href="{{ route('propertytype.edit',$property->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('propertytype.destroy', $property->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$property->id}},`{{ route('propertytype.destroy', $property->id)}}`)">Delete</button>
                </form>
                @endforeach
            </td>
        </tr>
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('propertytype.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(property_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+property_id).remove();
                }
            });
        }


</script>
@endsection
