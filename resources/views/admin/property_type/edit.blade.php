@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a Property Type</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" id="property_update" action="{{ route('propertytype.update', $propertyType->id) }}">
            {{method_field('put')}} 
            @csrf
            <div class="form-group">    
              <label for="property_type">Property Type:</label>
              <input type="text" class="form-control" name="property_type" value={{$propertyType->property_type}}>
          </div> 
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>
$('#property_update').validate({ 
        rules: {
            propety_type: {
                required: true
            },
        }
    });

$('#property_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('propertytype.update', $propertyType->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection