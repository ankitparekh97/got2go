@extends('layouts.appAdmin')
@section('content')
<body class="login-bg">
    <div class="login-bg-img">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center min-vh-100">
                <div class="col-xl-6 col-lg-7 col-md-9 m-auto">
                    <div class="card o-hidden border-0 shadow-lg">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-10 m-auto">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="login-title text-uppercase font-fam-fatfrank">{{ __('got2go') }}<span class="pl-2 font-fam-open">{{ __('admin') }}</span></h1>                                            
                                            <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                                <div class="alert-icon">
                                                    <em class="flaticon-warning"></em>
                                                </div>
                                                <div class="alert-text">
                                                </div>
                                            </div>
                                            <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 tripperUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                                <div class="alert-icon">
                                                    <em class="flaticon-warning"></em>
                                                </div>
                                                <div class="alert-text" style="font-weight: bold;"> </div>
                                            </div>
                                            <form class="user mt-5" method="POST" id="adminlogin" action="{{ route('adminlogin') }}">
                                                @csrf
                                                <div class="form-group mb-3">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                      <img class="lazy-load" data-src="{{URL::asset('media/images/email-icon.png')}}" alt="">
                                                    </span>
                                                        </div>
                                                        <input type="email" required id="email" class="form-control login-input-tag font-fam-open" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus placeholder="Email">                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group mb-3">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                <img class="lazy-load" data-src="{{URL::asset('media/images/pass-icon.png')}}" alt="">
                                                  </span>
                                                        </div>
                                                        <input type="password" required id="password" class="form-control login-input-tag font-fam-open"  name="password" placeholder="Password" autocomplete="current-password">                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group mb-4 mt-2">                                                    
                                                    @if (Route::has('password.request'))
                                                    <a class="forgot-pwd-txt font-fam-open" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
                                                    @endif
                                                </div>
                                                <button type="submit" class="btn btn-primary btn-login-custom box-shadow-custom font-fam-fatfrank">
                                              Login <img class="ml-3 lazy-load" data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt=""></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    var adminlogin = "{{ route('adminlogin') }}";
    var transactionList = "{{ route('kpi-dashboard-listing') }}";
</script>
<script src="{{ URL::asset('js/admin/login/login.js') }}"></script>
@endsection
