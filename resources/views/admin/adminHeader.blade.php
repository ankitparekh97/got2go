<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('admin/kpi-dashboard')}}">
        <div class="sidebar-brand-icon">
            <img src="{{URL::asset('media/images/logo.svg')}}" alt="Got2Go Logo">
        </div>
        <div class="sidebar-brand-text mx-3">Admin <span>panel</span></div>
    </a>
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <span class="fa fa-bars"></span>
    </button>

    <ul class="navbar-nav ml-auto">
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="lazy-load img-profile rounded-circle" data-src="{{URL::asset('media/images/admin.png')}}" alt="">
                <span class="ml-2 d-none d-lg-inline text-grey">Hello, {{Session::get('admin')['name']}}</span>
                <span class="fas fa-fw fa-caret-down"></span>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                    <span class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></span> Profile
                </a>
                <a class="dropdown-item" href="#">
                    <span class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></span> Settings
                </a>
                <a class="dropdown-item" href="#">
                    <span class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></span> Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" style="cursor: pointer" onclick="adminLogout(`{{route('adminlogout')}}`)">
                    <span class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></span> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
<!-- End of Topbar -->
