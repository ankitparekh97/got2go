@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
<meta name="csrf-token" content="{{ csrf_token() }}">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Hotels</h1>   
    <div>
    <a style="margin: 19px;" href="{{ route('hotels.create')}}" class="btn btn-primary">New Hotel</a>
    </div>   
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($hotels as $hotel)
        <tr id="row_{{$hotel->id}}">
            <td>{{$hotel->id}}</td>
            <td>{{$hotel->name}}</td>
            <td>{{$hotel->email}}</td>
            <td>
                <a href="{{ route('hotels.edit',$hotel->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('hotels.destroy', $hotel->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                   <button class="btn btn-danger" type="button"  onclick="distroy({{$hotel->id}},`{{ route('hotels.destroy', $hotel->id)}}`)">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('hotels.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(hotel_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+hotel_id).remove();
                }
            });
        }


</script>
@endsection