@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a hotel</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" id="hotel_update" action="{{ route('hotels.update', $hotel->id) }}">
            {{method_field('put')}} 
            @csrf
            <div class="form-group">    
              <label for="name">Name:</label>
              <input type="text" class="form-control" name="name" value={{$hotel->first_name}}>
          </div>
          
          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email" readonly value={{$hotel->email}}>
          </div>

          <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" name="description" value={{$hotel->description}}>
          </div> 

          <div class="form-group">
              <label for="location">Location:</label>
              <input type="text" class="form-control" name="location" value={{$hotel->location}}>
          </div>

          <div class="form-group">
              <label for="city">City:</label>
              <input type="text" class="form-control" name="city" value={{$hotel->city}}>
          </div> 

          <div class="form-group">
              <label for="phone">Phone:</label>
              <input type="text" class="form-control" name="phone" value={{$hotel->phone}}>
          </div>

          <div class="form-group">
              <label for="website">website:</label>
              <input type="text" class="form-control" name="website" value={{$hotel->website}}>
          </div>

          <div class="form-grouaddressp">
              <label for="property_type">property_type:</label>
              <input type="text" class="form-control" name="property_type" value={{$hotel->property_type}}>
          </div>  

          <div class="form-group">
              <label for="check_in">check_in:</label>
              <input type="text" class="form-control" name="check_in" value={{$hotel->check_in}}>
          </div> 

          <div class="form-group">
              <label for="check_out">check_out:</label>
              <input type="file" class="form-control" name="photo" value={{$hotel->check_out}}>
          </div>  

          <div class="form-group">
              <label for="private_room_accommodation">private room accommodation:</label>
              <input type="text" class="form-control" name="private_room_accommodation" value={{$hotel->private_room_accommodation}}>
          </div> 
           <div class="form-group">
              <label for="cottage_room_accommodation">cottage room accommodation:</label>
              <input type="text" class="form-control" name="cottage_room_accommodation" value={{$hotel->cottage_room_accommodation}}>
          </div> 
           <div class="form-group">
              <label for="delux_room_accommodation">delux room accommodation:</label>
              <input type="text" class="form-control" name="delux_room_accommodation" value={{$hotel->delux_room_accommodation}}>
          </div> 
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>
$('#hotel_update').validate({ 
        rules: {
            name: {
                required: true
            },
            description: {
                required: true
            },
            location: {
                required: true
            },
            city:{
              required: true
            },
            state:{
              required: true
            },
            phone:{
              required: true
            },
            website:{
              required: true
            },
            property_type:{
              required: true
            },
            check_in:{
              required: true
            },
            check_out:{
              required: true
            },
            private_room_accommodation: {
                required: true
            },
            cottage_room_accommodation: {
                required: true
            },
             delux_room_accommodation: {
                required: true
            },
        }
    });

$('#hotel_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('hotels.update', $hotel->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection