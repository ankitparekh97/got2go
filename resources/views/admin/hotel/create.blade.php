@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a hotel</h1>
  <div>
    <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
      <form method="post" id="hotel_create" action="{{ route('hotels.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">    
              <label for="name">Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
         

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email"/>
          </div>                       
          <button type="submit" class="btn btn-primary-outline">Add Hotel</button>
      </form>
  </div>
</div>
</div>
<script>
$('#hotel_create').validate({ 
        rules: {
            name: {
                required: true
            },
           
            email: {
                required: true,
                email: true
            },
        }
    });

$('#hotel_create').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('hotels.store') }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection