<div class="modal-dialog modal-dialog-centered modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">View Document</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" style="height:600px !important;">
            <div class="_row">
             @php $document_count = 1; @endphp
                @if(!empty($Doc))

                    @foreach($Doc as $row_doc)
                        @php $url = public_path('uploads/property').'/'.$row_doc['document_name'];  @endphp
                        @if(file_exists($url))
                            @php $media_type = mime_content_type($url); @endphp
                            @if(strpos($media_type,'image') === false)
                                @php $image = 0; @endphp
                            @else
                                @php $image = 1; @endphp
                            @endif
                            <div class="back_uploaded_image">
                                <a href="{{ URL('uploads/property/') }}/{{$row_doc['document_name']}}" target="_blank">
                                    @if($image == 1)
                                    <img alt="{{ URL('uploads/property/') }}/{{$row_doc['document_name']}}" src="{{ URL('uploads/property/') }}/{{$row_doc['document_name']}}" class="lazy-load img-responsive" >
                                    @else
                                    <img alt="{{ URL('media/images/pdf-icon.svg') }}" src="{{ URL('media/images/pdf-icon.svg') }}" class="lazy-load img-responsive" >
                                    @endif

                                   <div style="min-height:60px"> <p>{{ $row_doc['originalName'] }}</p></div>
                                </a>
                            </div>
                            @php $document_count++; @endphp
                        @endif

                    @endforeach
                @endif
                @if($document_count == 1)
                    <h3>No Document Found</h3>
                @endif
            </div>

        </div>
    </div>
</div>
