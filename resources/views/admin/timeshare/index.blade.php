@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Timeshare</h1>
    <div>
    <a style="margin: 19px;" href="{{ route('timeshares.create')}}" class="btn btn-primary">New Timeshare</a>
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Timeshare Description</td>
          <td>Description</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($timeshares as $timeshare)
    <tr id="row_{{$timeshare->id}}">
            <td>{{$timeshare->id}}</td>
            <td>{{$timeshare->ownertimeshare->timeshare_description}}</td>
            <td>{{$timeshare->description}}</td>
            <td>
                <a href="{{ route('timeshares.edit',$timeshare->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('timeshares.destroy', $timeshare->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$timeshare->id}},`{{ route('timeshares.destroy', $timeshare->id)}}`)">Delete</button>
                </form>
                @endforeach
            </td>
        </tr>
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('timeshares.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(timeshare_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+$timeshare_id).remove();
                }
            });
        }


</script>
@endsection
