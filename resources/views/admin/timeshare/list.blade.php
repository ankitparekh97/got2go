@extends('layouts.adminApp')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column pt-4">
<!-- Main Content -->
    <div id="content">
        <div class="container-fluid">
            <!-- Page Heading -->
           <h1 class="mb-3 title-page">Approve Listings</h1>
            <div class="accordion admin-accrodian-listing" id="accordionExample">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Bulk Uploads<span class="fas fa-fw fa-chevron-up"></span>
                    </button>
                  </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <!-- DataTales Example -->
                    <div class="card shadow">
                        <div id="loader_spin" style="display:none"><img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader"></div>                   
                            <div class="card-body">
                                <div class="alert alert-success" id="showmsg_all" style="display:none"></div>                
                                <div class="table-responsive">
                                    <table id="BulkTimeshareList" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                                        <caption></caption>                
                                        <thead>
                                            <tr>                 
                                                <th scope="col"> Owner Name</th>                        
                                                <th scope="col"> Email </th>
                                                <th scope="col"> Government ID</th>
                                                <th scope="col"> Status</th>
                                                <th scope="col"> Listings</th>
                                                <th scope="col"> Action</th>                        
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Individual Listings<span class="fas fa-fw fa-chevron-up"></span>
                    </button>
                  </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <!-- DataTales Example -->
                    <div class="card shadow">
                        <div id="loader_spin" style="display:none"><img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader"></div>                   
                        <div class="card-body">
                            <div class="alert alert-success" id="showmsg" style="display:none"></div>                
                            <div class="table-responsive">
                                <table id="timesharelist" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                                    <caption></caption>                
                                    <thead>
                                        <tr>                 
                                            <th scope="col"> Owner Name</th>                        
                                            <th scope="col"> Email </th>
                                            <th scope="col"> Property Name </th>
                                            <th scope="col"> Government ID</th>
                                            <th scope="col"> Proof of Ownership</th>
                                            <th scope="col"> Status</th>
                                            <th scope="col"> Action</th>                        
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            
        </div>
        
    </div>
</div>
<!-- Modal -->
<div id="approve" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><input type="hidden" id="property_id" name="property_id"><h1>Are you sure?</h1></div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertyapprove(this)">Yes</a>
                </div>
            <div class="d-block w-100 mt-2">
                <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
</div>

<div id="deny" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><input type="hidden" id="property_id" name="property_id"><h1>Are you sure?</h1></div>
            
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertydenyMessage(this)">Yes</a>
                </div>
                <div class="d-block w-100 mt-2">
                    <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reasonDecline" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h1>Reason for denying this listing</h1>
                <input type="hidden" id="property_id" name="property_id">
                <div class="form-group">
                    <textarea id="deny_message" name="deny_message" rows="3" cols="40" placeholder="Please provide a brief explanation"></textarea>
                </div>
                <div class="form-group">
                    <label class="error" id="discountPercentageError" style="display: none;margin-left:30px;">
                        Please provide a brief explanation.
                    </label> 
                </div>
            </div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertydeny(this)">Submit</a>
                </div>
                <div class="d-block w-100 mt-2">
                    <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="approve_all" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="bulkupload_uniquekey" name="bulkupload_uniquekey"><h1>Would you like to accept all <span   id="approve_list_no"></span> listings from <span id="approve_owner_name"></span>?</h1>
            </div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertyapprove_all(this)">Yes</a>
                </div>
            <div class="d-block w-100 mt-2">
                <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button>
            </div>
        </div>
        </div>
    </div>
</div>

<div id="deny_all" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="bulkupload_uniquekey" name="bulkupload_uniquekey"><h1>Would you like to deny all <span id="deny_list_no"></span> listings from <span id="deny_owner_name"></span>?</h1>
            </div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertydenyMessage_all(this)">Yes</a>
                </div>
                <div class="d-block w-100 mt-2">
                    <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="individual" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="bulkupload_uniquekey" name="bulkupload_uniquekey"><h1>Are you sure you want to individually review all <span id="individual_list_no"></span> listings from <span id="individual_owner_name"></span>?</h1>
                <p>These listings will be moved to the individual listings section where you will need to Accept/Deny each one.</p>
            </div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertyreviewindividually_all(this)">Yes</a>
                </div>
                <div class="d-block w-100 mt-2">
                    <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reasonDecline_all" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="modal-header">                    
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h1>Reason for denying this listing</h1>
                <input type="hidden" id="bulkupload_uniquekey" name="bulkupload_uniquekey">
                <div class="form-group">
                    <textarea id="deny_message_all" name="deny_message" rows="3" cols="40" placeholder="Please provide a brief explanation"></textarea>
                </div>
                <div class="form-group">
                    <label class="error" id="discountPercentageError" style="display: none;margin-left:30px;">
                    Please provide a brief explanation.
                    </label> 
                </div>
            </div>
            <div class="modal-footer">
                <div class="d-block w-100">
                    <a class="btn button-default-custom btn-approve-custom" onclick = "propertydeny_all(this)">Submit</a>
                </div>
                <div class="d-block w-100 mt-2">
                    <button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="MyModal">
</div>

<style src="{{ URL::asset('js/admin/timeshare/timeshare.css') }}"></style>
<script>
    var propertyApprove = "{{route('property-approve')}}";
    var propertyDeny = "{{route('property-deny')}}";    
    var loaderImg = "{{URL::asset('uploads/users/loader.gif')}}";
    var timeshareListing = "{{route('timeshare-listing')}}";
    var view_document_url = "{{ url('admin/view_document') }}";
</script>
<script src="{{ URL::asset('js/admin/timeshare/timeshare.js') }}"></script>
<script>
    var propertyApprove_all = "{{route('property-approve-all')}}";
    var propertyDeny_all = "{{route('property-deny-all')}}"; 
    var BulkTimeshareList_url = "{{route('BulkTimeshareList')}}";
</script>
<script src="{{ URL::asset('js/admin/timeshare/BulkTimeshareList.js') }}"></script>


@endsection