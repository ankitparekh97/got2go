@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
<meta name="csrf-token" content="{{ csrf_token() }}">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Users</h1>   
    <div>
    <a style="margin: 19px;" href="{{ route('users.create')}}" class="btn btn-primary">New User</a>
    </div>   
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr id="row_{{$user->id}}">
            <td>{{$user->id}}</td>
            <td>{{$user->full_name}}</td>
            <td>{{$user->email}}</td>
            <td>
                <a href="{{ route('users.edit',$user->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('users.destroy', $user->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button"  onclick="distroy({{$user->id}},`{{ route('users.destroy', $user->id)}}`)">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('users.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function distroy(user_id,url){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: url,
                type: 'DELETE',
                data: {_token: CSRF_TOKEN},
                dataType: 'JSON',
                headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
                success: function (data) {
                    if(data.success)
                        $('#row_'+user_id).remove();
                }
            });
        }


</script>
@endsection