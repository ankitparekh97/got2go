@extends('base') 
@section('main')
<h1>Showing Task {{ $user->full_name }}</h1>
<div class="jumbotron text-center">
    <p>
        <strong>Name:</strong> {{ $user->full_name }}<br>
        <strong>Email:</strong> {{ $user->email }}
    </p>
</div>
@endsection