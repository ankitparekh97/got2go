@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a user</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" id="user_update" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
            {{method_field('put')}}
            @csrf
            <div class="form-group">    
              <label for="full_name">Name:</label>
              <input type="text" class="form-control" name="full_name" value={{$user->full_name}}>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" name="email" readonly value={{$user->email}}>
          </div>

          <div class="form-group">
              <label for="bio">Bio:</label>
              <input type="text" class="form-control" name="bio" value={{$user->bio}}>
          </div> 

          <div class="form-group">
              <label for="birthdate">Birth Date:</label>
              <input type="text" class="form-control" name="birthdate" value={{$user->birthdate}}>
          </div>

          <div class="form-group">
              <label for="gender">Gender:</label>
              <input type="text" class="form-control" name="gender" value={{$user->gender}}>
          </div>  

          <div class="form-group">
              <label for="phone">Phone:</label>
              <input type="text" class="form-control" name="phone" value={{$user->phone}}>
          </div> 

          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address" value={{$user->address}}>
          </div>

          <div class="form-group">
              <label for="city">City:</label>
              <input type="text" class="form-control" name="city" value={{$user->city}}>
          </div>

          <div class="form-grouaddressp">
              <label for="state">State:</label>
              <input type="text" class="form-control" name="state" value={{$user->state}}>
          </div>  

          <div class="form-group">
              <label for="zip">Zip Code:</label>
              <input type="text" class="form-control" name="zip" value={{$user->zip}}>
          </div>

          <div class="form-group">
              <label for="currency">Currency:</label>
              <input type="text" class="form-control" name="currency" value={{$user->currency}}>
          </div> 

          <div class="form-group">
              <label for="photo">Photo:</label>
              <input type="file" class="form-control" name="photo" value={{$user->photo}}>
          </div>  

          <div class="form-group">
              <label for="marketing_email">Marketing Email:</label>
              <input type="text" class="form-control" name="marketing_email" value={{$user->marketing_email}}>
          </div>  

          <div class="form-group">
              <label for="linked_paypal_account">Linked Paypal Account:</label>
              <input type="text" class="form-control" name="linked_paypal_account" value={{$user->linked_paypal_account}}>
            </div>
         
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
<script>

$('#user_update').validate({ 
        rules: {
            full_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            bio:{
              required: true
            },
            birthdate:{
              required: true
            },
            gender:{
              required: true
            },
            address:{
              required: true
            },
            city:{
              required: true
            },
            state:{
              required: true
            },
            zip:{
              required: true
            },
            currency:{
              required: true
            },
            marketing_email:{
              required: true
            },
            linked_paypal_account:{
              required: true
            },
            phone: {
                required: true,
                digits: true
            },
            
            photo: {
                required: true,
                extension: "jpeg|png"
            },
        }
    });

$('#user_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('users.update', $user->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection