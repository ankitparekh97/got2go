<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar accordion pt-4" id="accordionSidebar">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ (request()->is('admin/kpi-dashboard')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin/kpi-dashboard')}}">
            <span class="fas fa-chart-line"></span> KPI Dashboard
        </a>
    </li>
    <li class="nav-item {{ (request()->is('admin/trippers-list')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin/trippers-list')}}">
            <span class="fas fa-briefcase"></span> Tripper Dashboard
        </a>
    </li>
    <li class="nav-item {{ (request()->is('admin/service-fee')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin/service-fee')}}">
            <span class="fas fa-money-bill-alt"></span> GOT2GO Service Fee
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">
            <span class="fas fa-credit-card"></span> Banking Information
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">
            <span class="fas fa-fire"></span> Hot Deals
        </a>
    </li>
    <li class="nav-item {{ (request()->is('admin/timeshare-list')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('timeshare-listing', ['type' => 'property']) }}">
            <span class="fas fa-home"></span>Approve Listings
            <p class="p-left-custom mb-0"></p>
        </a>
    </li>
    <li class="nav-item {{ (request()->is('admin/user-management')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin/user-management')}}">
            <span class="fas fa-user"></span> Owner Management
        </a>
    </li>
    <!-- Nav Item - Tables -->
    <li class="nav-item {{ (request()->is('admin/transaction-logs')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin/transaction-logs')}}">
            <span class="fas fa-exchange-alt"></span> Transaction Logs
        </a>
    </li>
    <li class="nav-item {{ (route('web.admin.couponCodes') === url()->current()) ? 'active' : '' }}" >
        <a class="nav-link" href="{{ route('web.admin.couponCodes') }}">
            <span class="svg-cion-last">
                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17.0711 2.92891C15.1823 1.0402 12.6711 0 10 0C7.32891 0 4.8177 1.0402 2.92891 2.92891C1.0402 4.8177 0 7.32891 0 10C0 12.6711 1.0402 15.1823 2.92891 17.0711C4.8177 18.9598 7.32891 20 10 20C11.8286 20 13.6179 19.5016 15.1743 18.5589C15.5433 18.3353 15.6613 17.8549 15.4377 17.4859C15.2141 17.1168 14.7338 16.9989 14.3647 17.2224C13.0525 18.0173 11.5432 18.4375 10 18.4375C5.34754 18.4375 1.5625 14.6525 1.5625 10C1.5625 5.34754 5.34754 1.5625 10 1.5625C14.6525 1.5625 18.4375 5.34754 18.4375 10C18.4375 11.6636 17.9428 13.283 17.0068 14.683C16.767 15.0416 16.8634 15.5268 17.222 15.7666C17.5808 16.0065 18.0659 15.9101 18.3057 15.5514C19.4141 13.8935 20 11.9738 20 10C20 7.32891 18.9598 4.8177 17.0711 2.92891Z" fill="#FCE302"/>
                <path d="M7.59007 14.5935C7.70819 14.6574 7.83542 14.6877 7.96089 14.6877C8.23858 14.6877 8.50753 14.5392 8.64882 14.2778L12.8718 6.46527C13.077 6.08566 12.9356 5.61163 12.556 5.40652C12.1764 5.20132 11.7024 5.34273 11.4973 5.72226L7.27432 13.5348C7.06913 13.9143 7.21049 14.3884 7.59007 14.5935Z" fill="#FCE302"/>
                <path d="M8.71094 7.26562C8.71094 6.18867 7.83477 5.3125 6.75781 5.3125C5.68086 5.3125 4.80469 6.18867 4.80469 7.26562C4.80469 8.34258 5.68086 9.21875 6.75781 9.21875C7.83477 9.21875 8.71094 8.34258 8.71094 7.26562ZM6.75781 7.65625C6.54242 7.65625 6.36719 7.48102 6.36719 7.26562C6.36719 7.05023 6.54242 6.875 6.75781 6.875C6.9732 6.875 7.14844 7.05023 7.14844 7.26562C7.14844 7.48102 6.9732 7.65625 6.75781 7.65625Z" fill="#FCE302"/>
                <path d="M13.2031 14.6875C14.2801 14.6875 15.1562 13.8113 15.1562 12.7344C15.1562 11.6574 14.2801 10.7812 13.2031 10.7812C12.1262 10.7812 11.25 11.6574 11.25 12.7344C11.25 13.8113 12.1262 14.6875 13.2031 14.6875ZM13.2031 12.3438C13.4185 12.3438 13.5938 12.519 13.5938 12.7344C13.5938 12.9498 13.4185 13.125 13.2031 13.125C12.9877 13.125 12.8125 12.9498 12.8125 12.7344C12.8125 12.519 12.9877 12.3438 13.2031 12.3438Z" fill="#FCE302"/>
                </svg>

            </span> Coupon Codes
        </a>
    </li>
</ul>
<!-- End of Sidebar -->
