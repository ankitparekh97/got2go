@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a Property</h1>
  <div>
    <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
      <form method="post" id="property_create" action="{{ route('properties.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
              <label for="title">Name You Place:</label>
              <input type="text" class="form-control" name="title"/>
          </div>
          <div class="form-group">
              <label for="property_type">Property Type:</label>
              <input type="text" class="form-control" name="property_type"/>
          </div>

          <div class="form-group">
              <label for="description">Description:</label>
                <textarea name="description" id="description" cols="80" rows="5"></textarea>
            </div>


            <div class="form-group">
                <label for="guest_will_have">Guest Will Have	:</label>
                <input type="text" class="form-control" name="guest_will_have"/>
            </div>
            <div class="form-group">
                <label for="location_detail">Location Detail:</label>
                <input type="text" class="form-control" name="location_detail"/>
            </div>


            <div class="form-group">
                <label for="common_room">Common Room:</label>
                <input type="number" class="form-control" name="common_room"/>
            </div>


            <div class="form-group">
                <label for="sofa">Sofa:</label>
                <input type="number" class="form-control" name="sofa"/>
            </div>

            <div class="form-group">
                <label for="common_bathroom">Common Bathroom:</label>
                <input type="number" class="form-control" name="common_bathroom"/>
            </div>

            <div class="form-group">
                <label for="bathroom">Bathroom:</label>
                <input type="number" class="form-control" name="bathroom"/>
            </div>

            <div class="form-group">
                <label for="bed">bed:</label>
                <input type="number" class="form-control" name="bed"/>
            </div>

            <div class="form-group">
                <label for="bed_type">Bed Type:</label>
                <input type="number" class="form-control" name="bed_type"/>
            </div>

            <div class="form-group">
                <label for="guest_per_room">Guest Per  Room:</label>
                <input type="number" class="form-control" name="guest_per_room"/>
            </div>

            <div class="form-group">
                <label for="proof_of_ownership_or_lease">proof_of_ownership_or_lease:</label>
                <input type="file" class="form-control" name="proof_of_ownership_or_lease" />
            </div>

            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" class="form-control" name="price" step="0.01"/>
            </div>

            <div class="form-group">
                <label for="service_fees">Service Fees:</label>
                <input type="number" class="form-control" name="service_fees" step="0.01"/>
            </div>

            <div class="form-group">
                <label for="occupancy_taxes_and_fees">Occupancy Taxes And Fees:</label>
                <input type="number" class="form-control" name="occupancy_taxes_and_fees" step="0.01"/>
            </div>

            <div class="form-group">
                <label for="is_instant_booking_allowed">is Instant Booking Allowed:</label>
                <input type="radio" class="form-control" name="is_instant_booking_allowed" value="1"/>Yes
                <input type="radio" class="form-control" name="is_instant_booking_allowed" value="0"/>NO
            </div>


            <div class="form-group">
                <label for="is_partial_payment_allowed">is Partial Payment Allowed:</label>
                <input type="radio" class="form-control" name="is_partial_payment_allowed" value="1"/>Yes
                <input type="radio" class="form-control" name="is_partial_payment_allowed" value="0"/>NO
            </div>


            <div class="form-group">
                <label for="check_in">Check In:</label>
                <input type="time" class="form-control" name="check_in" />
            </div>

            <div class="form-group">
                <label for="check_out">Check Out:</label>
                <input type="time" class="form-control" name="check_out" />
            </div>


            <div class="form-group">
                <label for="cancellation_type">Cancellation Type:</label>
                <input type="text" class="form-control" name="cancellation_type" />
            </div>


            <div class="form-group">
                <label for="cover_photo">cover photo:</label>
                <input type="file" class="form-control" name="cover_photo" />
            </div>

            <div class="form-group">
                <label for="video">Video:</label>
                <input type="file" class="form-control" name="video" />
            </div>



          <button type="submit" class="btn btn-primary-outline">Add Property</button>
      </form>
  </div>
</div>
</div>
<script>
$('#property_create').validate({ 
         rules: {
            title: {
                required: true
            },
            property_type: {
                required: true,
            },
            description:{
              required: true
            },
            guest_will_have:{
              required: true
            },
            location_detail:{
              required: true
            },
            common_room:{
              required: true
            },
            sofa:{
              required: true
            },
            common_bathroom:{
              required: true
            },
            bathroom:{
              required: true
            },
            bed:{
              required: true
            },
            guest_per_room:{
              required: true
            },
            proof_of_ownership_or_lease:{
              required: true
            },
            price:{
              required: true
            },
            service_fees: {
                required: true,
            },
            occupancy_taxes_and_fees: {
                required: true,
            },
            is_instant_booking_allowed: {
                required: true,
            },
            is_partial_payment_allowed: {
                required: true,
            },
            check_in: {
                required: true,
            },
            check_out: {
                required: true,
            },
            cancellation_type: {
                required: true,
            },
            cover_photo: {
                required: true,
                extension: "jpeg|png"
            },
            video: {
                //required: true,
                extension: "jpeg|png"
            },
        }
    });
$('#property_create').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('properties.store') }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection
