@extends('base')
@section('main')
<h1>Showing Property {{ $property->title }}</h1>
<div class="jumbotron text-center">
    <p>
        <strong>location_detail:</strong> {{ $property->location_detail }}<br>
    </p>
</div>
@endsection