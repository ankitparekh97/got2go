@extends('layouts.adminApp')
@section('content')
<div id="content-wrapper" class="d-flex flex-column pt-4">
    <div id="content">
        <div class="container-fluid">
            <h1 class="mb-2 title-page d-block">KPI Dashboard</h1>
            <div class="row m-0 mt-5 w-100">
                <div class="col-xl-6 col-md-6 mb-4">
                    <div class="card shadow h-100 py-2">
                        <div class="card-body">
                            <div class="chart-area">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class="float-left">Revenue from Reservations</div>
                                        <div class="float-right">
                                            <select id="reservation" name="reservation">
                                                <option value="weekly">Weekly</option>
                                                <option value="monthly" selected>Monthly</option>
                                                <option value="annually">Annually</option>
                                            </select>
                                        </div>
                                    </div>                                   
                                </div>
                                <div>
                                    <canvas id="revenueReservationId"></canvas>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-4">
                    <div class="card shadow h-100 py-2">
                        <div class="card-body">
                            <div class="chart-area">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                    <div class="float-left">Revenue from Trippers</div>
                                    <div class="float-right">
                                            <select id="trippers" name="trippers">
                                                <option>Weekly</option>
                                                <option>Monthly</option>
                                                <option>Annually</option>
                                            </select>
                                        </div>                                      
                                        <div class="mb-2" style="margin: 10 auto;">
                                            <canvas id="canvas"></canvas>
                                        </div>                                       
                                    </div>
                                <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-6 mb-4">
    <div class="card shadow h-100 py-2">
    <div class="card-body">
    <div class="chart-area">
    <div class="chartjs-size-monitor">
    <div class="chartjs-size-monitor-expand">
    <div class="float-left">Revenue from Hot Deals</div>
    <div class="float-right">
        <select id="hotdeals" name="hotdeals">
            <option>Weekly</option>
            <option>Monthly</option>
            <option>Annually</option>
        </select>
    </div>      
    </div>
    <div class="chartjs-size-monitor-shrink">
    <div class=""></div>
    </div>
    </div>

    </div>
    </div>
    </div>
    </div>
    <div class="col-xl-6 col-md-6 mb-4">
    <div class="card shadow h-100 py-2">
    <div class="card-body">
    <div class="chart-area">
    <div class="chartjs-size-monitor">
    <div class="chartjs-size-monitor-expand">
    <div class="">Overall Conversion Rate</div> 
    </div>
    <div class="chartjs-size-monitor-shrink">
    <div class=""></div>
    </div>
    </div>

    </div>
    </div>
    </div>
    </div>

</div>



</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
<style src="{{ URL::asset('js/admin/kpidashboard/kpidashboard.css') }}"></style>
<script>
    var xAxisData = <?php echo json_encode($xAxisData); ?>;
    var yAxisData = <?php echo json_encode($yAxisData); ?>;    
</script>
<script src="{{ URL::asset('js/admin/kpidashboard/kpidashboard.js') }}"></script>
@endsection