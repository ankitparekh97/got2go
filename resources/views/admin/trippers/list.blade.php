@extends('layouts.adminApp')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column pt-4">
<!-- Main Content -->
<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="mb-2 title-page">Tripper Dashboard</h1>
        <form class="subscription custom-error-outer" id="formSubscriptionTripper">
            @csrf
            <input type="hidden" id="subscription_id" name="subscription_id" value="{{($subscription)?$subscription->id:''}}">

            <div class="form-group">
                <div class="alert alert-success" id="showsuccessmsgsub" style="display:none"></div>
                <div class="alert alert-danger" id="showerrormsgsub" style="display:none"></div>

                <div class="row mt-4">
                    <div class="col-xl-3 col-lg-6 col-sm-12 mb-2">
                        <label class="label-text">Annual Membership Fee</label>
                        <div class="input-group w-75 mt-2">
                            <span class="fa fa-dollar-sign pt-2 pr-2" id="showdollorid"></span>
                            <input type="text" maxlength="7" class="form-control form-control-dollar border-0" id="annual_subscription" name="annual_subscription" aria-describedby="dollar" placeholder="" value="{{($subscription)?$subscription->annual_subscription:''}}" />
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-sm-12 mb-2">
                        <label class="label-text">Strikethrough Price</label>
                        <div class="input-group w-75 mt-2">
                            <span class="fa fa-dollar-sign pt-2 pr-2"></span>
                            <input type="text" maxlength="7" class="form-control form-control-dollar border-0" id="annual_strike_subscription_amount" name="annual_strike_subscription_amount" aria-describedby="dollar" value="{{($subscription)?$subscription->strike_through_amount:''}}" />
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-sm-12 mb-2">
                        <label class="label-text ">Discount Percentage for all Trippers</label>
                        <div class="input-group w-75 mt-2" >
                            <input type="text" maxlength="5" class="form-control form-control-dollar border-0" id="discount_percentage" name="discount_percentage" aria-describedby="dollar" value="{{($subscription)?$subscription->discount_percentage:''}}" />
                            <span class="fa fa-percent pt-2 ml-2" id="hidepercentage"></span>
                            <label class="error" id="discountPercentageError" style="display: none;">
                            Discount percentage cannot be greater than 80%.
                            </label>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-sm-12 mb-2">
                        <button type="submit" href="#" id="savesubscription" class="btn button-default-custom btn-sunset-gradient my-3">Save</button>
                    </div>
                </div>
                <div class="row pt-2 pb-2 d-none" id="tripperSubscriptionFee">
                    <div class="col-xl-4 col-lg-6 col-sm-12 float-right">
                        <label class="error admin-error-cstm"><span class="error-icon"><svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5328 5.0888C11.3899 3.5449 13.6101 3.5449 14.4672 5.08879L20.1958 15.4079C21.0283 16.9076 19.9439 18.75 18.2286 18.75H6.77139C5.05612 18.75 3.97166 16.9076 4.8042 15.4079L10.5328 5.0888Z" stroke="#EC1D1D" stroke-width="1.5"/>
                        <path d="M11.583 8.99655C11.5383 8.46009 11.9617 8 12.5 8C13.0383 8 13.4617 8.46009 13.417 8.99655L13.0415 13.5017C13.0181 13.7834 12.7826 14 12.5 14C12.2174 14 11.9819 13.7834 11.9585 13.5017L11.583 8.99655Z" fill="#EC1D1D"/>
                        <circle cx="12.5249" cy="15.375" r="0.875" fill="#EC1D1D"/>
                        </svg>
                        </span>
                        You will be charged $<span id="spanTransactionFee"></span> for every new Tripper signup.</label>
                    </div>
                </div>
            
            </div>

        </form>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
        <div id="loader_spin" style="display:none"><img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader"></div>
            <div class="card-body">
                <div class="table-responsive">
                <div class="alert alert-success" id="showmsg" style="display:none"></div>
                <table id="tripperslist" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                <caption></caption>
                <thead>
                    <tr>
                        <th scope="col"> Name</th>
                        <th scope="col"> Age </th>
                        <th scope="col"> Email</th>
                        <th scope="col"> # Bookings</th>
                        <th scope="col"> Status</th>
                        <th scope="col"> Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div></div>
<!-- Modal -->
<div id="paused" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                @csrf
                <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                <input type="hidden" id="rental_user_id" name="rental_user_id">
                <input type="hidden" id="subscription" name="subscription">
                <h1>Are you sure you want to </h1><h1 id="status_show" class="dynamic_text"></h1> <h1>this Tripper’s subscription?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "subscriptionApprove(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
<!-- Modal -->
<div id="cancel" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                @csrf
                <div class="alert alert-danger" id="showcancelerrormsg" style="display:none"></div>
                <input type="hidden" id="rental_user_id" name="rental_user_id"><h1>Are you sure you want to cancel this Tripper’s subscription?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "cancelSubscription(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
@endsection

@section('headerCss')
    @parent
    <style src="{{ URL::asset('js/admin/tripper/tripper.css') }}"></style>
@endsection

@section('footerJs')
    @parent
    <script>
        var subscriptionSave = "{{route('subscription-save')}}";
        var subscriptionApproveRoute = "{{route('subscription-approve')}}";
        var subscriptionCancel = "{{route('subscription-cancel')}}";
        var loaderImg = "{{URL::asset('uploads/users/loader.gif')}}";
        var trippersListing = "{{route('trippers-listing')}}";
    </script>
    <script src="{{ URL::asset('js/admin/tripper/tripper.js') }}"></script>
@endsection
