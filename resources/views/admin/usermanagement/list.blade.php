@extends('layouts.adminApp')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column pt-4">
<!-- Main Content -->
<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="mb-2 title-page">Owner Management</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div id="loader_spin" style="display:none"><img class="lazy-load" data-src="{{URL::asset('uploads/users/loader.gif')}}" alt="loader"></div>
            <div class="card-body">
                <div class="table-responsive">
                <div class="alert alert-success" id="showmsg" style="display:none"></div>
                <table id="userslist" class="w-100 overflow-x table-border table-hover booking-table table display" style="width:100% !important;table-layout:fixed;">
                <caption></caption>
                <thead>
                    <tr>
                        <th scope="col"> Owner Name</th>
                        <th scope="col"> Email</th>
                        <th scope="col"> Total # of Properties</th>
                        <th scope="col"> Status</th>
                        <th scope="col"> Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
</div></div>
<!-- Modal -->
<div id="deactivate" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                <input type="hidden" id="user_id" name="user_id">
                <input type="hidden" id="propertyId" name="propertyId">
                <span id="status_show"></span><h1> Are you sure you want to deactivate this owner and all of their listings?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "deactivateOwner(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
<!-- Modal -->
<div id="cancel" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="alert alert-danger" id="showcancelerrormsg" style="display:none"></div>
                <input type="hidden" id="rental_user_id" name="rental_user_id"><h1>Are you sure you want to cancel this Tripper’s subscription?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "cancelSubscription(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
<!-- Modal -->
<div id="activate" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
                </div>
                <div class="modal-body">
                <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                <input type="hidden" id="active_user_id" name="user_id">
                <input type="hidden" id="active_propertyId" name="propertyId">
                <span id="status_show"></span><h1> Are you sure you want to activate this owner and all of their listings?</h1></div>
                <div class="modal-footer">
                <div class="d-block w-100"><a class="btn button-default-custom btn-approve-custom" data-toggle="modal" onclick = "activateUser(this)">Yes</a></div>
                <div class="d-block w-100 mt-2"><button id="btnClosePopup" class="btn button-default-custom btn-deny-custom" type="button" data-dismiss="modal">No</button></div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('headerCss')
    @parent
    <style src="{{ URL::asset('js/admin/usermanagement/usermanagement.css') }}"></style>
@endsection
@section('footerJs')
    @parent
    <script>
        var subscriptionSave = "{{route('subscription-save')}}";
        var userDeactivate = "{{route('user-deactivate')}}";
        var subscriptionCancel = "{{route('subscription-cancel')}}";
        var userListing = "{{route('user-listing')}}";
        var propertyList = "{{route('property-list')}}";
        var defaultUserImage = "{{URL::asset('media/users/default.jpg')}}";
        var userImage = "{{URL::asset('uploads/owners/')}}";
        var propertyImage = "{{URL::asset('uploads/property')}}";
        var propertyUrl = "{{ url('/propertydetail')}}";
        var loaderImg = "{{URL::asset('uploads/users/loader.gif')}}";
    </script>
    <script src="{{ URL::asset('js/admin/usermanagement/usermanagement.js') }}"></script>
@endsection
