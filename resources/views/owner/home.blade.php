@extends('layouts.app')

@section('content')
<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');
?>
<!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
       <div class="subheader py-2 py-lg-6 pb-0" id="kt_subheader">
            <h4 style="text-align:center;">Dashboard</h4>
            </div>
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    @if(Auth::guard('admin')->check())
                    <ul>
                        <li><a href="{{route('users')}}">users</a></li>
                        <li><a href="{{route('owners')}}">owners</a></li>
                        <li><a href="{{route('property')}}">property</a></li>
                        <li><a href="{{route('timeshare')}}">timeshare</a></li>
                        <li><a href="{{route('propertytype')}}">property type</a></li>
                        <li><a href="{{route('amenity')}}">amenity</a></li>
                        <li><a onclick="logout(`{{route('admin.logout')}}`)">logout</a></li>
                    </ul>
                    @elseif(Auth::guard('owner')->check())
                    <ul>
                        <li><a href="{{route('owner.property')}}">Property</a></li>
                        <li><a href="{{route('owner.timeshare')}}">timeshare</a></li>
                        <li><a href="{{route('property.booking.list')}}">Property Booking</a></li>
                        <li><a href="{{route('timeshare.booking.list')}}">Timeshare Booking</a></li>
                        <li><a onclick="logout(`{{route('owners.logout')}}`)">logout</a></li>
                    </ul>
                    @elseif(Auth::guard('rentaluser')->check())
                    <ul>
                        <!-- <li><a href="{{route('owner.property')}}">Property</a></li>
                        <li><a href="{{route('owner.timeshare')}}">timeshare</a></li> -->
                        <li><a href="{{route('rental.property.booking.list')}}">Property Booking</a></li>
                        <li><a href="{{route('rental.timeshare.booking.list')}}">Timeshare Booking</a></li>
                        <li><a onclick="logout(`{{route('rental.logout')}}`)">logout</a></li>
                    </ul>
                    @elseif(Auth::guard('hotel')->check())
                    <ul>
                        <!-- <li><a href="{{route('owner.property')}}">Property</a></li>
                        <li><a href="{{route('owner.timeshare')}}">timeshare</a></li> -->
                        <li><a href="{{route('hotel.changepasswordview')}}">Change Password</a></li>
                        <li><a href="{{route('rental.timeshare.booking.list')}}">Profile</a></li>
                        <li><a onclick="logout(`{{route('hotel.logout')}}`)">logout</a></li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div> -->

@endsection
