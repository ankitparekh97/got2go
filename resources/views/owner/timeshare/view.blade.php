@extends('layouts.app')

@section('content')

@include('layouts.listSessionmsg')
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <h2 class="f-w-700 text-uppercase text-dark d-block w-90 align-items-center">
                  View Timeshare                      
               </h2>
               <a href="{{route('timeshare.create')}}" class="f-w-700 btn btn-danger">Add Timeshare</a>
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid property-edit" id="kt_content">
            <div class="row" >
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="property-images">
                           <div class="row">
                                <div class="col-md-12">
                                    <img src="{{URL::asset('/media/images/property.jpeg')}}" alt="{{$timeshare->ownertimeshare->hotels->name}}" />
                                </div>
                                @foreach($timeshare->propertyimages->take(4) as $property_image)
                                    <div class="col-6 col-md-3">
                                        <img src="{{URL::asset('uploads/property/'.$property_image->photo)}}" alt="{{$property_image->photo}}" />
                                    </div>
                                @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-desc">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h2 class="f-w-700 mb-5">{{ $timeshare->ownertimeshare->hotels->name}}</h2>
                        <h6>{{ $timeshare->ownertimeshare->hotels->description}}</h6>
                        <label class="mark-place"><i class="fas fa-map-marker"></i>{{ $timeshare->location_detail}}</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="card mb-10" style="height: calc(100%);">
                     <div class="card-body">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="d-flex align-items-center" style="justify-content: center;flex-direction: column;height: 100%;">
                           <h6><strong>Price:</strong> {{ number_format($timeshare->price) }} per Night</h6>
                           <h6><strong>Tax:</strong> ${{ Config::get('property.service_fee')}}</h6>
                           <h6><strong>cancellation:</strong> {{ Config::get('property.cancellation_type.'.$timeshare->cancellation_type.'.title') }}</h6>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card">
                     <div class="card-body list-spacing-h6">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Property type:</strong>{{ $propertyType[$timeshare->property_type_id]->property_type }} </h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Room type:</strong> {{ Config::get('property.structure_type.'.$timeshare->ownertimeshare->structure_type)}}</h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Accommodates:</strong> {{$timeshare->ownertimeshare->no_of_guest}} Guests</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Unit size:</strong> {{$timeshare->ownertimeshare->unit_size}}</h6>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Check-In:</strong> {{date('m/d/Y',strtotime($timeshare->ownertimeshare->check_in_date))}}</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Check Out:</strong> {{date('m/d/Y',strtotime($timeshare->ownertimeshare->check_out_date))}}</h6>
                           </div>
                        </div>
                        <h6><strong>Minimum Days:</strong> {{$timeshare->ownertimeshare->min_days}}</h6>
                        <h6><strong>Unit Amenities</strong></h6>
                        <div class="row">
                            @foreach($timeshare->propertyamenities as $amenity)
                              <div class="col-6 col-sm-6">
                                 <h6>{{$amenity->masteramenity->amenity_name}}</h6>
                              </div>
                             @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="delete-listing mb-3">
            <a href="javascript:destroy({{$timeshare->id}})" class="btn btn-link-danger font-weight-bold" style="font-weight: 700;">Delete Listing</a>
            </div>
            <div class="posted-updated-date">
            <h6><span class="mr-5">Posted date: {{date('m/d/Y h:m A',strtotime($timeshare->ownertimeshare->created_at))}}</span><span>Updated date: {{date('m/d/Y h:m A',strtotime($timeshare->ownertimeshare->updated_at))}}</span></h6>
            </div>
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->

   <script>
      var timesharedestroy = '{{ route("timeshare.destroy", ":id") }}';
   </script>
   <script src="{{ URL::asset('js/owner/timeshare/view.js') }}"></script>
@endsection
