@extends('layouts.app')

@section('content')
  <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 pb-0" id="kt_subheader">
            <ul class="nav nav-tabs nav-tabs-line justify-content-center w-100">
                <li class="nav-item">
                    <a class="nav-link " href="{{route('owner.property')}}">Property</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_2">Timeshare</a>
                </li>
            </ul>

         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid d-lg-flex" id="kt_content">

            <div class="tab-content mt-5" id="myTabContent">
                <div class="tab-pane fade " id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                  <div class="text-right top-buttons">
                    <button type="button" class="f-w-700 btn btn-danger">Add New Listing</button>
                  </div>
                  <div class="mb-10">
                    <h6 class="f-w-700 mb-5 property-pending-container">Pending Approval</h6>
                    <div class="list-container">
                      <div class="row d-flex align-items-center">
                        <div class="col-md-3">
                          <div class="image">
                            <img src="assets/media/images/property.png" alt="" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="f-w-700 font-size-lg">2 BR Apartment</p>
                          <p class="font-size-lg"><i class="color-blue fas fa-map-marker-alt"></i> Manhattan, New York City</p>
                        </div>
                        <div class="col-md-3">
                          <p class="fw-700 font-size-lg">Price: <span class="f-w-700 color-blue">$200 per night</span></p>
                        </div>
                        <div class="col-md-3">
                          <div class="list-buttons">
                            <button type="button" class="f-w-700 btn btn-danger mb-4 w-100">Approval Pending</button><br>
                            <button type="button" class="f-w-700 btn btn-secondary w-100">Preview</button>
                          </div>

                        </div>
                      </div>
                      <div class="list-footer mt-4">
                        <div class="row d-flex align-items-center"> 
                          <div class="col-md-12">
                            <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Posted date: </span>01/02/2020 07:35pm</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <h6 class="f-w-700 mb-5 property-approval-container">Property</h6>
                    <div class="list-container">
                      <div class="row d-flex align-items-center">
                        <div class="col-md-3">
                          <div class="image">
                            <img src="assets/media/images/property.png" alt="" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="f-w-700 font-size-lg">2 BR Apartment</p>
                          <p class="font-size-lg"><i class="color-blue fas fa-map-marker-alt"></i> Manhattan, New York City</p>
                          <div class="list-star">
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,4.25932872 C12.1488635,4.25921584 12.3000368,4.29247316 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 L12,4.25932872 Z" fill="#000000" opacity="0.3"/><path d="M12,4.25932872 L12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.277344,4.464261 11.6315987,4.25960807 12,4.25932872 Z" fill="#000000"/></g></svg><span>
                          </div>  
                          <div class="list-reviews">
                            <a href="#">2 Reviews</a>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="fw-700 font-size-lg">Price: <span class="f-w-700 color-blue">$200 per night</span></p>

                        </div>
                        <div class="col-md-3">
                          <div class="list-buttons">
                            <button type="button" class="f-w-700 btn btn-light-danger small-btn mb-4 w-100">Unplublish</button><br>
                            <button type="button" class="f-w-700 btn btn-danger mb-4 w-100">Approval Pending</button><br>
                            <button type="button" class="f-w-700 btn btn-secondary w-100">Preview</button>
                          </div>

                        </div>
                      </div>
                      <div class="list-footer mt-4">
                        <div class="row d-flex align-items-center"> 
                          <div class="col-md-12">
                            <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Posted date: </span>01/02/2020 07:35pm</p>
                            <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Updated date: </span>03/02/2020 010:35pm</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="list-container">
                      <div class="row d-flex align-items-center">
                        <div class="col-md-3">
                          <div class="image">
                            <img src="assets/media/images/property.png" alt="" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="f-w-700 font-size-lg">2 BR Apartment</p>
                          <p class="font-size-lg"><i class="color-blue fas fa-map-marker-alt"></i> Manhattan, New York City</p>

                        </div>
                        <div class="col-md-3">
                          <p class="fw-700 font-size-lg">Price: <span class="f-w-700 color-blue">$200 per night</span></p>

                        </div>
                        <div class="col-md-3">
                          <div class="list-buttons">
                            <button type="button" class="f-w-700 btn btn btn-info mb-4 w-100">Publish Listing</button><br>
                            <button type="button" class="f-w-700 btn btn-secondary w-100">Preview</button>
                          </div>

                        </div>
                      </div>
                      <div class="list-footer mt-4">
                        <div class="row d-flex align-items-center"> 
                          <div class="col-md-12">
                            <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Posted date: </span>01/02/2020 07:35pm</p>
                            <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Updated date: </span>03/02/2020 010:35pm</p>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="tab-pane fade show active" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                  <div class="text-right top-buttons">
                    <a href="{{route('hotel.request')}}" class="f-w-700 btn btn btn-info">Request For New Timeshare</a>
                    <a href="{{route('timeshare.create')}}" class="f-w-700 btn btn-danger">Add Timeshare</a>
                  </div>
                  @if($timeshares->toarray())
                  <div class="mb-10">
                  @include('layouts.listSessionmsg')
                    <h6 class="f-w-700 mb-5 timeshare-pending-container">Pending Approval</h6>
                    @foreach($timeshares as $timeshare)
                      @if($timeshare->status == 'pending')
                          <div class="list-container">
                              <div class="row d-flex align-items-center">
                                <div class="col-md-3">
                                  <div class="image">
                                    <img src="{{URL::asset('/media/images/property.jpeg')}}" alt="{{$timeshare->ownertimeshare->hotels->name}}" />
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <p class="f-w-700 font-size-lg">{{$timeshare->ownertimeshare->hotels->name}}</p>
                                  <p class="font-size-lg"><i class="color-blue fas fa-map-marker-alt"></i> {{$timeshare->location_detail}}</p>
                                  <p class="font-size-lg"><span class="mr-4"><span class="f-w-700">Minimum Days:</span> {{$timeshare->ownertimeshare->min_days}}</span><span class="mr-4"><span class="f-w-700">Unit Size:</span> {{$timeshare->ownertimeshare->unit_size}}</span></p>
                                </div>
                                <div class="col-md-3">
                                  <p class="fw-700 font-size-lg"><strong>Price</strong>: <span class="f-w-700 color-blue">${{number_format($timeshare->price)}} per night</span></p>
                                </div>
                                <div class="col-md-3">
                                  <div class="list-buttons">
                                    <button type="button" class="f-w-700 btn btn-danger mb-4 w-100">Approval Pending</button><br>
                                    <a href="{{route('timeshare.view', $timeshare->id)}}"  class="f-w-700 btn btn-secondary w-100" onclick="">Preview</a>
                                  </div>

                                </div>
                              </div>
                              <div class="list-footer mt-4">
                                  <div class="row d-flex align-items-center"> 
                                    <div class="col-md-12">
                                      <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Posted date: </span>{{date('m/d/Y h:m A',strtotime($timeshare->created_at))}}</p>
                                    </div>
                                  </div>
                                </div>
                          </div>
                      @endif
                    @endforeach
                  </div>
                  <h6 class="f-w-700 mb-5 timeshare-approval-container">Timeshares</h6>
                  @foreach($timeshares as $timeshare)
                    @if($timeshare->status != 'pending')
                           <div class="list-container" data-id={{$timeshare->id}}>
                      <div class="row d-flex align-items-center">
                        <div class="col-md-3">
                          <div class="image">
                            <img src="{{URL::asset('/media/images/property.jpeg')}}" alt="{{$timeshare->ownertimeshare->hotels->name}}" />
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="f-w-700 font-size-lg">{{$timeshare->ownertimeshare->hotels->name}}</p>
                          <p class="font-size-lg"><i class="color-blue fas fa-map-marker-alt"></i> {{$timeshare->location_detail}}</p>
                          <p class="font-size-lg"><span class="mr-4"><span class="f-w-700">Minimum Days:</span> {{$timeshare->ownertimeshare->min_days}}</span><span class="mr-4"><span class="f-w-700">Unit Size:</span> {{$timeshare->ownertimeshare->unit_size}}</span></p>
                          <div class="list-star">
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.3476862,4.32173209 11.9473121,4.11839309 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 Z" fill="#000000"/></g></svg><span>
                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polygon points="0 0 24 0 24 24 0 24"/><path d="M12,4.25932872 C12.1488635,4.25921584 12.3000368,4.29247316 12.4425657,4.36281539 C12.6397783,4.46014562 12.7994058,4.61977315 12.8967361,4.81698575 L14.9389263,8.95491503 L19.5054023,9.61846284 C20.0519472,9.69788046 20.4306287,10.2053233 20.351211,10.7518682 C20.3195865,10.9695052 20.2170993,11.1706476 20.0596157,11.3241562 L16.7552826,14.545085 L17.5353298,19.0931094 C17.6286908,19.6374458 17.263103,20.1544017 16.7187666,20.2477627 C16.5020089,20.2849396 16.2790408,20.2496249 16.0843804,20.1472858 L12,18 L12,4.25932872 Z" fill="#000000" opacity="0.3"/><path d="M12,4.25932872 L12,18 L7.91561963,20.1472858 C7.42677504,20.4042866 6.82214789,20.2163401 6.56514708,19.7274955 C6.46280801,19.5328351 6.42749334,19.309867 6.46467018,19.0931094 L7.24471742,14.545085 L3.94038429,11.3241562 C3.54490071,10.938655 3.5368084,10.3055417 3.92230962,9.91005817 C4.07581822,9.75257453 4.27696063,9.65008735 4.49459766,9.61846284 L9.06107374,8.95491503 L11.1032639,4.81698575 C11.277344,4.464261 11.6315987,4.25960807 12,4.25932872 Z" fill="#000000"/></g></svg><span>
                          </div>  
                          <div class="list-reviews">
                            <a href="#">2 Reviews</a>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <p class="fw-700 font-size-lg">Price: <span class="f-w-700 color-blue">${{number_format($timeshare->price)}} per night</span></p>

                        </div>
                        <div class="col-md-3">
                              <div class="list-buttons">
                                  <button type="button" class="f-w-700 btn mb-4 w-100 change_publish {{($timeshare->publish == '1')?'small-btn btn-light-danger':'btn btn-info'}}">{{($timeshare->publish == '1')?'Unpublish':'Publish Listing'}}</button><br>
                                  <button type="button" class="f-w-700 btn btn-danger mb-4 w-100">Booking & Request</button><br>
                                  <a href="{{route('timeshare.view', $timeshare->id)}}" class="f-w-700 btn btn-secondary w-100">Preview</a>

                              </div>

                            </div>
                            <div class="list-footer mt-4">
                            <div class="row d-flex align-items-center"> 
                              <div class="col-md-12">
                                <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Posted date: </span>{{date('m/d/Y h:m A',strtotime($timeshare->created_at))}}</p>
                                <p class="font-size-xs p-b-0 mb-0"><span class="font-weight-bold">Updated date: </span>{{date('m/d/Y h:m A',strtotime($timeshare->updated_at))}}</p>
                              </div>
                            </div>
                          </div>
                      </div>

                    </div>
                    @endif
                  @endforeach
                  @else
                    <div>
                        <h3 style="text-align:center;">No Timeshare listed. <a href="{{route('timeshare.create')}}" class="f-w-700 btn btn-danger">Add Timeshare</a></h3>
                    </div>
                  @endif  
                </div>
            </div>

         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<script>
var timesharelist = "{{ route('timeshare.list') }}";
var propertypublish = "{{ route('property.publish') }}";
</script>
<script src="{{ URL::asset('js/owner/timeshare/index.js') }}"></script>
@endsection
