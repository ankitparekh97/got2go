@extends('layouts.app')

@section('content')
@include('layouts.listSessionmsg')
<!--begin::Container-->
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initMap"
            defer
    ></script>
    <!-- <script src="{{ URL::asset('js/googlemap.js') }}"></script> -->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">

         <div class="content flex-column-fluid d-lg-flex" id="kt_content">
            <!--begin::Wizard 6-->
            <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
               <!--begin::Container-->
               <div class="wizard-content d-flex flex-column mx-auto w-100">
                  <!--begin::Nav-->
                  <div class="d-flex flex-column-auto flex-column px-10 header-wizard">
                     <!--begin: Wizard Nav-->
                     <div class="wizard-nav d-flex flex-column  align-items-center align-items-md-start">
                        <!--begin::Wizard Steps-->
                        <div class="wizard-steps d-flex flex-column flex-md-row w-md-700px mx-auto w-100">
                           <!--begin::Wizard Step 1 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                              <div class="wizard-wrapper pr-lg-7 pr-5">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">1</span>
                                 </div>
                                 <div class="wizard-label mr-3">
                                    <h3 class="wizard-title">
                                       Basic
                                    </h3>

                                 </div>
                                 <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <polygon points="0 0 24 0 24 24 0 24"/>
                                          <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                          <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                       </g>
                                    </svg>
                                 </span>
                              </div>
                           </div>
                           <!--end::Wizard Step 1 Nav-->
                           <!--begin::Wizard Step 2 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                              <div class="wizard-wrapper pr-lg-7 pr-5">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">2</span>
                                 </div>
                                 <div class="wizard-label mr-3">
                                    <h3 class="wizard-title">
                                       Property details
                                    </h3>
                                 </div>
                                 <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <polygon points="0 0 24 0 24 24 0 24"/>
                                          <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                          <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                       </g>
                                    </svg>
                                 </span>
                              </div>
                           </div>
                           <!--end::Wizard Step 2 Nav-->
                           <!--begin::Wizard Step 3 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                              <div class="wizard-wrapper">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">3</span>
                                 </div>
                                 <div class="wizard-label">
                                    <h3 class="wizard-title">
                                       Pricing and Document Upload
                                    </h3>
                                 </div>
                              </div>
                           </div>
                           <!--end::Wizard Step 3 Nav-->
                        </div>
                        <!--end::Wizard Steps-->
                     </div>
                     <!--end: Wizard Nav-->
                  </div>
                  <!--end::Nav-->
                  <!--begin::Form-->
                  <div class="alert alert-danger" style="display:none;">
                     <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                     </ul>
                  </div><br />
                  <form class="px-10 pt-10" novalidate="novalidate" id="kt_wizard_form"  enctype="multipart/form-data" action="{{ route('timeshare.store') }}">
                  @csrf
                     <!--begin: Wizard Step 1-->
                     <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                        <h5 class="f-w-700">Basic</h5>
                        <div class="steps-container">
                           <!--begin::Form Group-->
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Property type</label>
                                    <select class="form-control" id="property_type" name="propertyTypeId">
                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label>Location</label>
                                    <input type="text" name="location" class="form-control" readonly  id="location_detail">
                                 </div>
                                 <!-- <div class="pt-5 pb-5 text-center">
                                    <h5>OR</h5>
                                 </div> -->
                                 <!-- <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label> -->
                                 <div class="map">
                                    <div id="map" style="height:300px;"></div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Resort/Hotel Name</label>
                                    <select class="form-control" id="hotels" name="hotel">
                                    </select>
                                 </div>
                                 <div class="pt-2 pb-5 text-center">
                                    <a href="{{route('hotel.request')}}">Resort/Hotel not listed here? Submit a Request.</a>
                                 </div>
                                 <div class="form-group">
                                    <label>Resort Description</label>
                                    <textarea class="form-control" rows="3" name="description" id="description"></textarea>
                                 </div>
                                 <div class="from-group">
                                    <label class="w-100"><h6>Resort Amenities</h6></label>
                                    <div class="row resort-amenities">
                                    </div>

                                    <!-- Add new amenities -->
                                    <!-- <div class="form-group">
                                       <label>Add New Amenities</label>

                                       <input id="kt_tagify_11" class="form-control tagify" name='tags' placeholder='type...' value='' autofocus data-blacklist='.NET,PHP'/>

                                       <div class="mt-3">
                                          <a href="javascript:;" id="kt_tagify_11_remove" class="btn btn-sm btn-light-primary font-weight-bold">Remove Amenities</a>
                                       </div>
                                    </div> -->
                                 </div>
                              </div>
                           </div>
                           <!--end::Form Group-->
                        </div>
                     </div>
                     <!--end: Wizard Step 1-->
                     <!--begin: Wizard Step 2-->
                     <div class="pb-5" data-wizard-type="step-content">
                        <!--begin::Title-->
                        <h5 class="f-w-700">Property details</h5>
                        <!--begin::Title-->
                        <div class="steps-container">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group"><label>Structure Type</label></div>
                                 <div class="row">
                                    @foreach(Config::get('property.structure_type') as $key=>$value)
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <label class="checkbox">
                                                <input type="radio" name="structureType" value="{{$key}}" checked="checked">
                                                <span></span>
                                               {{$value}}
                                             </label>
                                          </div>
                                       </div>
                                    @endforeach
                                 </div>


                                 <div class="form-group">
                                    <label>Number of guests it can hold?</label>
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <input id="number_guest" type="number" class="form-control" value="1" name="noOfGuest"/>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="form-group">
                                    <label>Unit Size</label>
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <input id="unit_size" type="number" class="form-control" value="1" name="unitSize"/>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Date Range</label>
                                    <div class="row">
                                       <div class="col-md-5">
                                          <div class="input-group date">
                                             <input type="text" class="form-control" id="kt_datepicker_2" readonly name="checkinDate" placeholder="Select date"/>
                                             <div class="input-group-append">
                                                <span class="input-group-text">
                                                   <i class="la la-calendar-check-o"></i>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-md-1 pt-2 pb-2 text-center">
                                          <label style="margin: 0;">To</label>
                                       </div>
                                       <div class="col-md-5">
                                          <div class="input-group date">
                                             <input type="text" class="form-control" id="kt_datepicker_3" readonly name="checkoutDate" placeholder="Select date"/>
                                             <div class="input-group-append">
                                                <span class="input-group-text">
                                                   <i class="la la-calendar-check-o"></i>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Minimum days per reservation?</label>
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <input type="number" class="form-control" name="minDays" id="min_days" />
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Allow booking before</label>
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <input type="number" class="form-control" name="bookingBeforeDays" id="bookingBeforeDays" />

                                       </div>
                                       <label class="col-sm-6" style="margin-left:-15px; margin-top:7px;">days</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Timeshare Description</label>
                                    <textarea class="form-control" rows="3" name="timeshareDescripion"></textarea>
                                 </div>
                                  <div class="form-group"><label class="w-100">Unit Amenities</label></div>
                                 <div class="row unit-amenities">
                                 </div>

                                 <!-- Add new amenities -->
                                 <!-- <div class="form-group">
                                    <label>Add New Amenities</label>

                                     <input id="kt_tagify_1" class="form-control tagify" name='tags' placeholder='type...' value='' autofocus data-blacklist='.NET,PHP'/>

                                     <div class="mt-3">
                                         <a href="javascript:;" id="kt_tagify_1_remove" class="btn btn-sm btn-light-primary font-weight-bold">Remove Amenities</a>
                                     </div>
                                </div> -->


                              </div>
                           </div>


                        </div>
                     </div>
                     <!--end: Wizard Step 2-->
                     <!--begin: Wizard Step 3-->
                     <div class="pb-5" data-wizard-type="step-content">
                        <!--begin::Title-->

                        <h5 class="f-w-700">Pricing and Document Upload</h5>
                        <div class="steps-container">
                           <div class="row">
                              <div class="col-md-6 border-right">
                                 <div class="row">
                                    <div class="col-md-8">
                                       <div class="form-group">
                                          <label>Price</label>
                                          <input type="text" name="price" class="form-control" placeholder="" required id="price" value="50">
                                          <label>Per Stay</label>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label><span class="f-w-700">Tax: </span>${{Config::get('property.service_fee')}} (Auto calculated as per state norms)</label>
                                    <label><span class="f-w-700">Occupancy Taxes and Fees: </span>${{Config::get('property.occupancy_taxt_and_fee')}} (Auto calculated as per state norms)</label>
                                 </div>
                                 <div class="form-group">
                                     <label class="checkbox">
                                         <input type="checkbox" checked="checked" name="partialPyament" id="partial_payment" value="1">
                                         <span></span>
                                         Allow Partial Payment
                                     </label>
                                  </div>
                                 <div class="row partial-payment-amount">
                                    <div class="col-md-8">
                                       <div class="form-group">
                                          <label>Booking Amount</label>
                                          <input type="number" class="form-control" placeholder="" name="partialPyamentAmount" id="partial_payment_amount" value="0">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cancellation-sec">
                                       <div class="form-group"><label>Cancellation</label></div>
                                       @foreach(Config::get('property.cancellation_type') as $key=>$value)
                                         <div class="form-group">
                                           <div class="radio-list">
                                              <label class="radio">
                                                 <input type="radio" checked="checked" name="cancellationType" value="{{$key}}">
                                                 <span></span>
                                                 <div class="w-100"><span class="f-w-700">{{$value['title']}}</span>{{$value['description']}}</div>
                                              </label>
                                           </div>
                                         </div>
                                      @endforeach
                                 </div>

                              </div>
                              <div class="col-md-6">
                                 <label>Long term Discount</label>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Time frame</label>
                                          <input type="text" name="ltdTimeFrame" class="form-control" placeholder="" required>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Discount Value</label>
                                          <input type="text" name="ltdTimeValue" class="form-control" placeholder="" required>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Timeshare Membership Number</label>
                                    <input type="text" name="membershipNumber" class="form-control" placeholder="" required>
                                 </div>
                                 <div class="form-group">
                                    <label>Upload Scan copy of document</label>
                                    <input type="file" name="document" id="document" class="form-control" required/>
                                 </div>
                              </div>
                           </div>

                        </div>

                        <!--end::Title-->
                        <!--begin::Section-->

                        <!--end::Section-->
                     </div>
                     <!--end: Wizard Step 3-->
                     <!--begin: Wizard Actions-->
                     <div class="d-flex justify-content-between pt-7">
                        <div class="mr-2">
                           <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                              <span class="svg-icon svg-icon-md mr-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/>
                                       <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                              Previous
                           </button>
                        </div>
                        <div>
                           <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_wizard_form_submit_button">
                              Submit
                              <span class="svg-icon svg-icon-md ml-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                       <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                           </button>
                           <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">
                              Next
                              <span class="svg-icon svg-icon-md ml-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                       <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                           </button>
                        </div>
                     </div>
                     <!--end: Wizard Actions-->
                  </form>
                  <!--end::Form-->
               </div>
               <!--end::Container-->
            </div>
            <!--end::Wizard 6-->
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->

<style>
    @media (min-width: 992px) {
        .content {
            padding: 40px 0;
        }
    }
</style>
   <script src="{{ URL::asset('js/jquery-3.2.1.min.js') }}"></script>
<script>
var ownerhotellist = "{{ route('owner.hotels.list') }}";
var ownerpropertytypelist = "{{ route('owner.property_type.list') }}";
var ownerhotelbyid = "{{ route('owner.hotelbyid') }}";
var owneramenitieslist= "{{ route('owner.amenities.list') }}";
var timesharestore = "{{ route('timeshare.store') }}";
</script>
<script src="{{ URL::asset('js/owner/timeshare/create.js') }}"></script>
@endsection
