@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a Timeshare</h1>

        <div class="alert alert-danger" style="display:none;">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br >
        <form method="post" action="{{ route('timeshare.update', $timeshare->id) }}" id="timeshare_update" enctype="multipart/form-data">
             {{method_field('put')}}
            @csrf
            <div class="form-group">
              <label for="hotel">Hotel:</label>
              <input type="text" class="form-control" name="hotel" value={{$timeshare->ownertimeshare->hotel_id}}>
          </div>
          <div class="form-group">
              <label for="propertyType">Property Type:</label>
              <input type="text" class="form-control" name="propertyType" value={{$timeshare->property_type_id}}>
          </div>

          <div class="form-group">
              <label for="timeshareDescripion">Timeshare Description:</label>
              <input type="text" class="form-control" name="timeshareDescripion" value={{$timeshare->ownertimeshare->timeshare_description}}>
          </div>

          <div class="form-group">
              <label for="description">Description:</label>
                <textarea name="description" id="description" cols="80" rows="5">{{$timeshare->description}}</textarea>
            </div>

            <div class="form-group">
                <label for="location">Location:</label>
                <input type="text" class="form-control" name="location" value={{$timeshare->location_detail}}>
            </div>

            <div class="form-group">
                <label for="noOfGuest">Number Of Guest:</label>
                <input type="text" class="form-control" name="noOfGuest" value={{$timeshare->ownertimeshare->no_of_guest}}>
            </div>

            <div class="form-group">
                <label for="unitSize">Unit Size:</label>
                <input type="number" class="form-control" name="unitSize" value={{$timeshare->ownertimeshare->unit_size}}>
            </div>


            <div class="form-group">
                <label for="minDays">Minimum Days:</label>
                <input type="number" class="form-control" name="minDays" value={{$timeshare->ownertimeshare->min_days}}>
            </div>

            <div class="form-group">
                <label for="bookingBeforeDays">Booking Before Days:</label>
                <input type="number" class="form-control" name="bookingBeforeDays" value={{$timeshare->ownertimeshare->allow_booking_before}}>
            </div>

            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" class="form-control" name="price" step="0.01" value={{$timeshare->price}}>
            </div>

            <div class="form-group">
                <label for="serviceFee">Service Fees:</label>
                <input type="number" class="form-control" name="serviceFee" step="0.01" value={{$timeshare->service_fees}}>
            </div>

            <div class="form-group">
                <label for="occupancyAndTaxFee">Occupancy Taxes And Fees:</label>
                <input type="number" class="form-control" name="occupancyAndTaxFee" step="0.01" value={{$timeshare->occupancy_taxes_and_fees}}>
            </div>

            <div class="form-group">
                <label for="partialPyament">is Partial Payment Allowed:</label>
                <input type="radio" class="form-control" name="partialPyament" value="1">Yes
                <input type="radio" class="form-control" name="partialPyament" value="0">NO
            </div>

            <div class="form-group">
                <label for="partialPyamentAmount">Partial Payment Amount:</label>
                <input type="number" class="form-control" name="partialPyamentAmount" step="0.01" value={{$timeshare->partial_payment_amount}}>
            </div>

            <div class="form-group">
                <label for="cancellationType">Cancellation Type:</label>
                <input type="radio" class="form-control" name="cancellationType" value="1">1
                <input type="radio" class="form-control" name="cancellationType" value="2">2
                <input type="radio" class="form-control" name="cancellationType" value="3">3
            </div>

            <div class="form-group">
                <label for="checkinDate">Check In:</label>
                <input type="date" class="form-control" name="checkinDate" value={{$timeshare->ownertimeshare->check_in_date}}>
            </div>

            <div class="form-group">
                <label for="checkoutDate">Check Out:</label>
                <input type="date" class="form-control" name="checkoutDate" value={{$timeshare->ownertimeshare->check_out_date}}>
            </div>

            <div class="form-group">
                <label for="ltdTimeFrame">LTD Time Frame:</label>
                <input type="number" class="form-control" name="ltdTimeFrame" step="0.01" value={{$timeshare->ownertimeshare->ltd_time_frame}}>
            </div>

            <div class="form-group">
                <label for="ltdTimeFrame">LTD Time Value:</label>
                <input type="number" class="form-control" name="ltdTimeValue" step="0.01" value={{$timeshare->ownertimeshare->ltd_value}}>
            </div>

            <div class="form-group">
                <label for="membershipNumber">Membership Number:</label>
                <input type="number" class="form-control" name="membershipNumber" step="0.01" value={{$timeshare->ownertimeshare->membership_number}}>
            </div>

            <div class="form-group">
                <label for="document">Document:</label>
                <input type="file" class="form-control" name="document" value={{$timeshare->ownertimeshare->document}}>
            </div>

            <div class="form-group">
                <label for="structureType">Structure Type:</label>
                <input type="radio" class="form-control" name="structureType" value="1">1
                <input type="radio" class="form-control" name="structureType" value="2">2
                <input type="radio" class="form-control" name="structureType" value="3">3
            </div>

            
            <button type="submit" class="btn btn-primary">Update Timeshare</button>
        </form>
    </div>
</div>
<script>
$('#timeshare_update').validate({ 
        rules: {
            hotel: {
                required: true
            },
            propertyType: {
                required: true,
            },
            description:{
              required: true
            },
            timeshareDescripion:{
              required: true
            },
            location:{
              required: true
            },
            noOfGuest:{
              required: true
            },
            unitSize:{
              required: true
            },
            checkinDate:{
              required: true
            },
            checkoutDate:{
              required: true
            },
            minDays:{
              required: true
            },
            bookingBeforeDays:{
              required: true
            },
            price:{
              required: true
            },
            serviceFee: {
                required: true,
            },
            occupancyAndTaxFee: {
                required: true,
            },
            partialPyament: {
                required: true,
            },
            partialPyamentAmount: {
                required: true,
            },
            cancellationType: {
                required: true,
            },
            ltdTimeFrame: {
                required: true,
            },
            ltdTimeValue: {
                required: true,
            },
            membershipNumber: {
                required: true,
            },
            structureType: {
                required: true,
            },
             document: {
                required: true,
                extension: "jpeg|png"
            },
        }
    });

$('#timeshare_update').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: "{{ route('timeshare.update', $timeshare->id) }}",
        dataType: "JSON",
        headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
            },
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(data) {
            if($.isEmptyObject(data.error)){

                window.location.href = data.redirect_url;
            }else{
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( data.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });

                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 2000);
            }

        },
    });
});
</script>
@endsection