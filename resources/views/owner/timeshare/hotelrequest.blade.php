@extends('layouts.app')

@section('content')
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initAutocomplete&libraries=places&v=weekly"
            defer
    ></script>
    <script src="{{ URL::asset('js/googlemap.js') }}"></script>

<!--begin::Container-->
<div class="d-flex flex-row flex-column-fluid  container ">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">

        <div class="content flex-column-fluid d-lg-flex" id="kt_content">
            <!--begin::Wizard 6-->
            <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
                <!--begin::Container-->
                <div class="wizard-content d-flex flex-column mx-auto w-100">
                    <!--begin::Nav-->
                    @include('layouts.listSessionmsg')
                    <!--end::Nav-->
                    <!--begin::Form-->
                    <form class="px-10 pt-10" method="post" id="hotelrequest" action="{{ route('web.post.hotel.request') }}" novalidate="novalidate" id="kt_wizard_form">
                    @csrf
                    <!--begin: Wizard Step 1-->
                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                            <h5 class="f-w-700"> New Resort/Hotel Request</h5>
                            <div class="steps-container">
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property type</label>
                                            <select class="form-control" name="property_type" id="exampleSelect1">
                                                <option>Resort</option>
                                                <option>Hotel</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter location details</label>
                                            <input type="text" name="location" id="location_detail" class="form-control" required="">
                                        </div>
                                        <!-- <div class="pt-5 pb-5 text-center">
                                            <h5>OR</h5>
                                        </div>
                                        <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label> -->
                                        <div class="map">
                                            <div id="map" style="height:300px;"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Resort/Hotel Name</label>
                                            <input type="text" name="hotelname" class="form-control" required="">
                                        </div>
                                        <!-- <div class="pt-2 pb-5 text-center">
                                    <a href="#">Resort/Hotel not listed here? Submit a Request.</a>
                                 </div> -->
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" name="country" class="form-control" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>State</label>
                                            <input type="text" name="state" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <!--end::Form Group-->
                            </div>
                        </div>
                        <!--end: Wizard Step 1-->

                        <!--begin: Wizard Actions-->



                        <div class="d-flex justify-content-between pt-7 float-right">

                            <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-8 py-4 my-3" data-wizard-type="submit">
                                Submit

                            </button>

                        </div>
                        <!--end: Wizard Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Wizard 6-->
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
</div>
<!--end::Container-->

<style>
    @media (min-width: 992px) {
        .content {
            padding: 40px 0;
        }
    }
</style>
<script src="{{ URL::asset('js/owner/timeshare/create.js') }}"></script>
@endsection
