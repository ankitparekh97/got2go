@extends('layouts.app')
@section('content')

<div class="">
	<div class="custom-container">
		<ul class="nav nav-tabs listing-tabs-current" id="myTab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="individual-account-tab" data-toggle="tab" href="#individual-account" role="tab" aria-controls="home" aria-selected="true">Individual  Account Settings</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="company-account-tab" data-toggle="tab" href="#company-account" role="tab" aria-controls="profile" aria-selected="false">Company  Acount Settings</a>
		  </li>
		</ul>
		<div class="tab-content owner-setting-tab" id="myTabContent1">
		  <div class="tab-pane fade show active" id="individual-account" role="tabpanel" aria-labelledby="individual-account-tab">
		  	<div class="row">
		  		<div class="col-xxl-8">
		  			<div class="setting-outer-box">
			  			<div class="row first-row">
					  		<div class="col-md-6">
					  			<h3>Personal Information</h3>
					  		</div>
					  		<div class="col-md-6 text-right">
					  			<label class="checkbox-container">Activate Individual Account
								  <input type="checkbox" data-toggle="modal" data-target="#activeindividual-account" checked="checked">
								  <span class="checkmark"></span>
								</label>
					  		</div>
					  	</div>
					  	<div class="row">
					  		<div class="col-md-6">
					  			<div class="field-outer">
						  			<label>Profile Picture</label>
						  			<div class="pro-pic-outer">
						  				<a href="#" class="pic-change">
						  					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<circle cx="12" cy="12" r="12" fill="#FCE300"/>
												<path d="M16.2857 16.2857C17.0726 16.2857 17.7143 15.644 17.7143 14.8572V9.85714C17.7143 9.07031 17.0726 8.42857 16.2857 8.42857H15.0357L14.7511 7.66964C14.6116 7.30134 14.1763 7 13.7857 7H10.9286C10.5379 7 10.1027 7.30134 9.96317 7.66964L9.67857 8.42857H8.42857C7.64174 8.42857 7 9.07031 7 9.85714V14.8572C7 15.644 7.64174 16.2857 8.42857 16.2857H16.2857ZM12.3571 14.8572C10.9788 14.8572 9.85715 13.7355 9.85715 12.3571C9.85715 10.9788 10.9788 9.85714 12.3571 9.85714C13.7355 9.85714 14.8572 10.9788 14.8572 12.3571C14.8572 13.7355 13.7355 14.8572 12.3571 14.8572ZM12.3571 13.9643C13.2444 13.9643 13.9643 13.2444 13.9643 12.3571C13.9643 11.4699 13.2444 10.75 12.3571 10.75C11.4699 10.75 10.75 11.4699 10.75 12.3571C10.75 13.2444 11.4699 13.9643 12.3571 13.9643Z" fill="black"/>
											</svg>
						  				</a>
						  				<img class="lazy-load" data-src="http://localhost/timeshareapp/public/media/images/jonathansidwell.png" alt="user">
						  			</div>
						  		</div>

						  		<div class="field-outer">
						  			<label>First Name</label>
						  			<input type="text" placeholder="John">
						  		</div>
						  		<div class="field-outer">
						  			<label>Last Name</label>
						  			<input type="text" placeholder="Smith">
						  		</div>
						  		<div class="field-outer">
						  			<label>Date of Birth</label>
							  			<div class='input-group date  calendar'>
	                                    <input type='text' id="datepicker" class="frmdatetimepicker" placeholder="    /        /    " />
                                	</div>
						  		</div>
						  		<div class="field-outer">
						  			<label>Social Security Number<span><svg width="26" height="27" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
										<mask id="path-1-inside-1" fill="white">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 23C17.7467 23 22 18.7467 22 13.5C22 8.25329 17.7467 4 12.5 4C7.25329 4 3 8.25329 3 13.5C3 18.7467 7.25329 23 12.5 23ZM11.0166 13.8848V14.4727H13.1152V14.124C13.1152 13.9281 13.1745 13.7594 13.293 13.6182C13.416 13.4723 13.7168 13.2445 14.1953 12.9346C14.7604 12.5745 15.1706 12.194 15.4258 11.793C15.6855 11.3874 15.8154 10.9089 15.8154 10.3574C15.8154 9.58724 15.526 8.97884 14.9473 8.53223C14.3685 8.08561 13.571 7.8623 12.5547 7.8623C11.3197 7.8623 10.1348 8.18587 9 8.83301L9.9502 10.6924C10.8708 10.2048 11.6751 9.96094 12.3633 9.96094C12.6413 9.96094 12.8669 10.0179 13.04 10.1318C13.2132 10.2412 13.2998 10.3916 13.2998 10.583C13.2998 10.8245 13.2178 11.0387 13.0537 11.2256C12.8942 11.4124 12.6299 11.6221 12.2607 11.8545C11.7959 12.1462 11.4723 12.4469 11.29 12.7568C11.1077 13.0622 11.0166 13.4382 11.0166 13.8848ZM11.085 15.8945C10.8434 16.1224 10.7227 16.446 10.7227 16.8652C10.7227 17.2845 10.8503 17.6081 11.1055 17.8359C11.3607 18.0592 11.7139 18.1709 12.165 18.1709C12.6025 18.1709 12.9466 18.057 13.1973 17.8291C13.4525 17.6012 13.5801 17.2799 13.5801 16.8652C13.5801 16.4505 13.457 16.1292 13.2109 15.9014C12.9694 15.6689 12.6208 15.5527 12.165 15.5527C11.6911 15.5527 11.3311 15.6667 11.085 15.8945Z"/>
										</mask>
										<path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 23C17.7467 23 22 18.7467 22 13.5C22 8.25329 17.7467 4 12.5 4C7.25329 4 3 8.25329 3 13.5C3 18.7467 7.25329 23 12.5 23ZM11.0166 13.8848V14.4727H13.1152V14.124C13.1152 13.9281 13.1745 13.7594 13.293 13.6182C13.416 13.4723 13.7168 13.2445 14.1953 12.9346C14.7604 12.5745 15.1706 12.194 15.4258 11.793C15.6855 11.3874 15.8154 10.9089 15.8154 10.3574C15.8154 9.58724 15.526 8.97884 14.9473 8.53223C14.3685 8.08561 13.571 7.8623 12.5547 7.8623C11.3197 7.8623 10.1348 8.18587 9 8.83301L9.9502 10.6924C10.8708 10.2048 11.6751 9.96094 12.3633 9.96094C12.6413 9.96094 12.8669 10.0179 13.04 10.1318C13.2132 10.2412 13.2998 10.3916 13.2998 10.583C13.2998 10.8245 13.2178 11.0387 13.0537 11.2256C12.8942 11.4124 12.6299 11.6221 12.2607 11.8545C11.7959 12.1462 11.4723 12.4469 11.29 12.7568C11.1077 13.0622 11.0166 13.4382 11.0166 13.8848ZM11.085 15.8945C10.8434 16.1224 10.7227 16.446 10.7227 16.8652C10.7227 17.2845 10.8503 17.6081 11.1055 17.8359C11.3607 18.0592 11.7139 18.1709 12.165 18.1709C12.6025 18.1709 12.9466 18.057 13.1973 17.8291C13.4525 17.6012 13.5801 17.2799 13.5801 16.8652C13.5801 16.4505 13.457 16.1292 13.2109 15.9014C12.9694 15.6689 12.6208 15.5527 12.165 15.5527C11.6911 15.5527 11.3311 15.6667 11.085 15.8945Z" fill="#A7B0B4"/>
										<path d="M11.0166 14.4727H8.5166V16.9727H11.0166V14.4727ZM13.1152 14.4727V16.9727H15.6152V14.4727H13.1152ZM13.293 13.6182L11.3822 12.006L11.3775 12.0116L13.293 13.6182ZM14.1953 12.9346L12.852 10.8261L12.8442 10.8311L12.8364 10.8362L14.1953 12.9346ZM15.4258 11.793L13.3205 10.4447L13.3166 10.4508L15.4258 11.793ZM9 8.83301L7.76153 6.66133L5.6872 7.84428L6.77384 9.97064L9 8.83301ZM9.9502 10.6924L7.72403 11.83L8.87851 14.0891L11.1204 12.9016L9.9502 10.6924ZM13.04 10.1318L11.666 12.2204L11.6854 12.2331L11.7051 12.2456L13.04 10.1318ZM13.0537 11.2256L11.1751 9.57608L11.1636 9.58918L11.1523 9.60243L13.0537 11.2256ZM12.2607 11.8545L13.5895 13.9722L13.5928 13.9701L12.2607 11.8545ZM11.29 12.7568L13.4366 14.0384L13.4408 14.0314L13.4449 14.0244L11.29 12.7568ZM11.085 15.8945L9.38644 14.0601L9.37789 14.068L9.36941 14.076L11.085 15.8945ZM11.1055 17.8359L9.44043 19.7008L9.44978 19.7091L9.4592 19.7174L11.1055 17.8359ZM13.1973 17.8291L11.5322 15.9643L11.5239 15.9717L11.5156 15.9792L13.1973 17.8291ZM13.2109 15.9014L11.4775 17.7028L11.4948 17.7195L11.5124 17.7358L13.2109 15.9014ZM19.5 13.5C19.5 17.366 16.366 20.5 12.5 20.5V25.5C19.1274 25.5 24.5 20.1274 24.5 13.5H19.5ZM12.5 6.5C16.366 6.5 19.5 9.63401 19.5 13.5H24.5C24.5 6.87258 19.1274 1.5 12.5 1.5V6.5ZM5.5 13.5C5.5 9.63401 8.63401 6.5 12.5 6.5V1.5C5.87258 1.5 0.5 6.87258 0.5 13.5H5.5ZM12.5 20.5C8.63401 20.5 5.5 17.366 5.5 13.5H0.5C0.5 20.1274 5.87258 25.5 12.5 25.5V20.5ZM13.5166 14.4727V13.8848H8.5166V14.4727H13.5166ZM13.1152 11.9727H11.0166V16.9727H13.1152V11.9727ZM10.6152 14.124V14.4727H15.6152V14.124H10.6152ZM11.3775 12.0116C10.8566 12.6327 10.6152 13.3804 10.6152 14.124H15.6152C15.6152 14.2811 15.5908 14.4729 15.5183 14.6793C15.4451 14.8878 15.3363 15.0723 15.2085 15.2247L11.3775 12.0116ZM12.8364 10.8362C12.4121 11.1109 11.7849 11.5288 11.3822 12.006L15.2037 15.2303C15.1623 15.2794 15.1286 15.3141 15.1092 15.3334C15.089 15.3535 15.0756 15.3654 15.072 15.3686C15.0661 15.3738 15.0794 15.3617 15.1205 15.3306C15.2041 15.2673 15.3434 15.1695 15.5543 15.033L12.8364 10.8362ZM13.3166 10.4508C13.3209 10.4441 13.2267 10.5874 12.852 10.8261L15.5386 15.043C16.2942 14.5617 17.0203 13.9439 17.5349 13.1352L13.3166 10.4508ZM13.3154 10.3574C13.3154 10.4364 13.3062 10.4716 13.3053 10.475C13.3048 10.4771 13.3055 10.4738 13.3086 10.4669C13.3117 10.4598 13.3158 10.4521 13.3205 10.4447L17.531 13.1413C18.0843 12.2773 18.3154 11.3158 18.3154 10.3574H13.3154ZM13.42 10.5115C13.4264 10.5164 13.4154 10.5089 13.3967 10.4861C13.3773 10.4626 13.3573 10.4321 13.3408 10.3973C13.3244 10.3629 13.3171 10.3362 13.3145 10.3237C13.3121 10.3123 13.3154 10.3216 13.3154 10.3574H18.3154C18.3154 8.86059 17.7039 7.50164 16.4746 6.55299L13.42 10.5115ZM12.5547 10.3623C12.8845 10.3623 13.1047 10.399 13.241 10.4372C13.3713 10.4737 13.4164 10.5087 13.42 10.5115L16.4746 6.55299C15.3159 5.65893 13.912 5.3623 12.5547 5.3623V10.3623ZM10.2385 11.0047C11.0185 10.5598 11.7793 10.3623 12.5547 10.3623V5.3623C10.86 5.3623 9.251 5.81191 7.76153 6.66133L10.2385 11.0047ZM12.1764 9.55475L11.2262 7.69537L6.77384 9.97064L7.72403 11.83L12.1764 9.55475ZM12.3633 7.46094C11.1107 7.46094 9.89136 7.89448 8.77997 8.48318L11.1204 12.9016C11.8502 12.515 12.2395 12.4609 12.3633 12.4609V7.46094ZM14.4141 8.04329C13.7154 7.58366 12.9669 7.46094 12.3633 7.46094V12.4609C12.3156 12.4609 12.0183 12.4521 11.666 12.2204L14.4141 8.04329ZM15.7998 10.583C15.7998 9.46399 15.2124 8.54697 14.375 8.01811L11.7051 12.2456C11.51 12.1224 11.2613 11.9093 11.0683 11.5741C10.8712 11.2317 10.7998 10.8789 10.7998 10.583H15.7998ZM14.9323 12.8751C15.484 12.2468 15.7998 11.4509 15.7998 10.583H10.7998C10.7998 10.4145 10.8296 10.224 10.9036 10.0307C10.9772 9.83852 11.078 9.68664 11.1751 9.57608L14.9323 12.8751ZM13.5928 13.9701C14.0372 13.6902 14.5517 13.3213 14.9551 12.8487L11.1523 9.60243C11.1828 9.56673 11.2031 9.54752 11.2065 9.54432C11.2104 9.54071 11.2035 9.54744 11.1824 9.5642C11.1381 9.59929 11.0576 9.65779 10.9287 9.73891L13.5928 13.9701ZM13.4449 14.0244C13.3889 14.1195 13.3483 14.1515 13.3716 14.1299C13.3945 14.1086 13.4597 14.0536 13.5895 13.9722L10.932 9.73683C10.2906 10.1393 9.59987 10.6994 9.13521 11.4893L13.4449 14.0244ZM13.5166 13.8848C13.5166 13.8301 13.5223 13.825 13.5146 13.8568C13.5062 13.8913 13.4848 13.9576 13.4366 14.0384L9.14349 11.4753C8.6776 12.2557 8.5166 13.1012 8.5166 13.8848H13.5166ZM13.2227 16.8652C13.2227 16.8921 13.2191 17.0118 13.1554 17.1823C13.0875 17.3643 12.9694 17.5537 12.8005 17.713L9.36941 14.076C8.5028 14.8936 8.22266 15.9497 8.22266 16.8652H13.2227ZM12.7705 15.9711C12.9391 16.1216 13.0671 16.3101 13.144 16.5052C13.2164 16.6887 13.2227 16.8238 13.2227 16.8652H8.22266C8.22266 17.8228 8.53679 18.894 9.44043 19.7008L12.7705 15.9711ZM12.165 15.6709C12.1166 15.6709 12.4187 15.6631 12.7517 15.9545L9.4592 19.7174C10.3026 20.4554 11.3112 20.6709 12.165 20.6709V15.6709ZM11.5156 15.9792C11.6668 15.8418 11.8291 15.7573 11.9654 15.7122C12.093 15.6699 12.1702 15.6709 12.165 15.6709V20.6709C13.0377 20.6709 14.0465 20.4357 14.8789 19.679L11.5156 15.9792ZM11.0801 16.8652C11.0801 16.8182 11.0873 16.6797 11.1609 16.4944C11.2389 16.2981 11.3671 16.1117 11.5322 15.9643L14.8623 19.6939C15.76 18.8924 16.0801 17.8279 16.0801 16.8652H11.0801ZM11.5124 17.7358C11.3467 17.5823 11.2251 17.3952 11.1532 17.2074C11.0855 17.0306 11.0801 16.9024 11.0801 16.8652H16.0801C16.0801 15.9309 15.7832 14.876 14.9095 14.067L11.5124 17.7358ZM12.165 18.0527C12.2114 18.0527 11.8484 18.0597 11.4775 17.7028L14.9444 14.0999C14.0904 13.2782 13.0301 13.0527 12.165 13.0527V18.0527ZM12.7835 17.7289C12.4066 18.0779 12.0607 18.0527 12.165 18.0527V13.0527C11.3214 13.0527 10.2555 13.2555 9.38644 14.0601L12.7835 17.7289Z" fill="#A7B0B4" mask="url(#path-1-inside-1)"/>
										</svg>
										</span>
									</label>
						  			<input type="password" placeholder="1234 5678 9012">
						  		</div>
					  		</div>
					  		<div class="col-md-6">
					  			<div class="field-outer">
						  			<label>Phone Number</label>
						  			<input type="number" placeholder="(898) 309-3903">
						  		</div>
						  		<div class="field-outer">
						  			<label>Email Address</label>
						  			<input type="text" placeholder="john@johnsmith.com">
						  		</div>
						  		<div class="field-outer">
						  			<label>Address</label>
						  			<input type="text" placeholder="Address 1" >
						  			<input type="text" placeholder="Address 2" >
						  			<input type="text" placeholder="City" >
						  			<div class="row">
						  				<div class="col-md-7">
						  					<input type="text" placeholder="State" >
						  				</div>
						  				<div class="col-md-5">
						  					<input type="text" placeholder="Zip Code" >
						  				</div>
						  			</div>
						  		</div>
						  		<div class="field-outer">
						  			<label>Country</label>
						  			<input type="text" placeholder="United States">
						  		</div>
					  		</div>
					  	</div>
					  	<div class="hr"></div>
					  	<div class="row">
					  		<div class="col-md-12">
					  			<label>Government Issued ID<span><svg width="26" height="27" viewBox="0 0 26 27" fill="none" xmlns="http://www.w3.org/2000/svg">
									<mask id="path-1-inside-1" fill="white">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 23C17.7467 23 22 18.7467 22 13.5C22 8.25329 17.7467 4 12.5 4C7.25329 4 3 8.25329 3 13.5C3 18.7467 7.25329 23 12.5 23ZM11.0166 13.8848V14.4727H13.1152V14.124C13.1152 13.9281 13.1745 13.7594 13.293 13.6182C13.416 13.4723 13.7168 13.2445 14.1953 12.9346C14.7604 12.5745 15.1706 12.194 15.4258 11.793C15.6855 11.3874 15.8154 10.9089 15.8154 10.3574C15.8154 9.58724 15.526 8.97884 14.9473 8.53223C14.3685 8.08561 13.571 7.8623 12.5547 7.8623C11.3197 7.8623 10.1348 8.18587 9 8.83301L9.9502 10.6924C10.8708 10.2048 11.6751 9.96094 12.3633 9.96094C12.6413 9.96094 12.8669 10.0179 13.04 10.1318C13.2132 10.2412 13.2998 10.3916 13.2998 10.583C13.2998 10.8245 13.2178 11.0387 13.0537 11.2256C12.8942 11.4124 12.6299 11.6221 12.2607 11.8545C11.7959 12.1462 11.4723 12.4469 11.29 12.7568C11.1077 13.0622 11.0166 13.4382 11.0166 13.8848ZM11.085 15.8945C10.8434 16.1224 10.7227 16.446 10.7227 16.8652C10.7227 17.2845 10.8503 17.6081 11.1055 17.8359C11.3607 18.0592 11.7139 18.1709 12.165 18.1709C12.6025 18.1709 12.9466 18.057 13.1973 17.8291C13.4525 17.6012 13.5801 17.2799 13.5801 16.8652C13.5801 16.4505 13.457 16.1292 13.2109 15.9014C12.9694 15.6689 12.6208 15.5527 12.165 15.5527C11.6911 15.5527 11.3311 15.6667 11.085 15.8945Z"/>
									</mask>
									<path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 23C17.7467 23 22 18.7467 22 13.5C22 8.25329 17.7467 4 12.5 4C7.25329 4 3 8.25329 3 13.5C3 18.7467 7.25329 23 12.5 23ZM11.0166 13.8848V14.4727H13.1152V14.124C13.1152 13.9281 13.1745 13.7594 13.293 13.6182C13.416 13.4723 13.7168 13.2445 14.1953 12.9346C14.7604 12.5745 15.1706 12.194 15.4258 11.793C15.6855 11.3874 15.8154 10.9089 15.8154 10.3574C15.8154 9.58724 15.526 8.97884 14.9473 8.53223C14.3685 8.08561 13.571 7.8623 12.5547 7.8623C11.3197 7.8623 10.1348 8.18587 9 8.83301L9.9502 10.6924C10.8708 10.2048 11.6751 9.96094 12.3633 9.96094C12.6413 9.96094 12.8669 10.0179 13.04 10.1318C13.2132 10.2412 13.2998 10.3916 13.2998 10.583C13.2998 10.8245 13.2178 11.0387 13.0537 11.2256C12.8942 11.4124 12.6299 11.6221 12.2607 11.8545C11.7959 12.1462 11.4723 12.4469 11.29 12.7568C11.1077 13.0622 11.0166 13.4382 11.0166 13.8848ZM11.085 15.8945C10.8434 16.1224 10.7227 16.446 10.7227 16.8652C10.7227 17.2845 10.8503 17.6081 11.1055 17.8359C11.3607 18.0592 11.7139 18.1709 12.165 18.1709C12.6025 18.1709 12.9466 18.057 13.1973 17.8291C13.4525 17.6012 13.5801 17.2799 13.5801 16.8652C13.5801 16.4505 13.457 16.1292 13.2109 15.9014C12.9694 15.6689 12.6208 15.5527 12.165 15.5527C11.6911 15.5527 11.3311 15.6667 11.085 15.8945Z" fill="#A7B0B4"/>
									<path d="M11.0166 14.4727H8.5166V16.9727H11.0166V14.4727ZM13.1152 14.4727V16.9727H15.6152V14.4727H13.1152ZM13.293 13.6182L11.3822 12.006L11.3775 12.0116L13.293 13.6182ZM14.1953 12.9346L12.852 10.8261L12.8442 10.8311L12.8364 10.8362L14.1953 12.9346ZM15.4258 11.793L13.3205 10.4447L13.3166 10.4508L15.4258 11.793ZM9 8.83301L7.76153 6.66133L5.6872 7.84428L6.77384 9.97064L9 8.83301ZM9.9502 10.6924L7.72403 11.83L8.87851 14.0891L11.1204 12.9016L9.9502 10.6924ZM13.04 10.1318L11.666 12.2204L11.6854 12.2331L11.7051 12.2456L13.04 10.1318ZM13.0537 11.2256L11.1751 9.57608L11.1636 9.58918L11.1523 9.60243L13.0537 11.2256ZM12.2607 11.8545L13.5895 13.9722L13.5928 13.9701L12.2607 11.8545ZM11.29 12.7568L13.4366 14.0384L13.4408 14.0314L13.4449 14.0244L11.29 12.7568ZM11.085 15.8945L9.38644 14.0601L9.37789 14.068L9.36941 14.076L11.085 15.8945ZM11.1055 17.8359L9.44043 19.7008L9.44978 19.7091L9.4592 19.7174L11.1055 17.8359ZM13.1973 17.8291L11.5322 15.9643L11.5239 15.9717L11.5156 15.9792L13.1973 17.8291ZM13.2109 15.9014L11.4775 17.7028L11.4948 17.7195L11.5124 17.7358L13.2109 15.9014ZM19.5 13.5C19.5 17.366 16.366 20.5 12.5 20.5V25.5C19.1274 25.5 24.5 20.1274 24.5 13.5H19.5ZM12.5 6.5C16.366 6.5 19.5 9.63401 19.5 13.5H24.5C24.5 6.87258 19.1274 1.5 12.5 1.5V6.5ZM5.5 13.5C5.5 9.63401 8.63401 6.5 12.5 6.5V1.5C5.87258 1.5 0.5 6.87258 0.5 13.5H5.5ZM12.5 20.5C8.63401 20.5 5.5 17.366 5.5 13.5H0.5C0.5 20.1274 5.87258 25.5 12.5 25.5V20.5ZM13.5166 14.4727V13.8848H8.5166V14.4727H13.5166ZM13.1152 11.9727H11.0166V16.9727H13.1152V11.9727ZM10.6152 14.124V14.4727H15.6152V14.124H10.6152ZM11.3775 12.0116C10.8566 12.6327 10.6152 13.3804 10.6152 14.124H15.6152C15.6152 14.2811 15.5908 14.4729 15.5183 14.6793C15.4451 14.8878 15.3363 15.0723 15.2085 15.2247L11.3775 12.0116ZM12.8364 10.8362C12.4121 11.1109 11.7849 11.5288 11.3822 12.006L15.2037 15.2303C15.1623 15.2794 15.1286 15.3141 15.1092 15.3334C15.089 15.3535 15.0756 15.3654 15.072 15.3686C15.0661 15.3738 15.0794 15.3617 15.1205 15.3306C15.2041 15.2673 15.3434 15.1695 15.5543 15.033L12.8364 10.8362ZM13.3166 10.4508C13.3209 10.4441 13.2267 10.5874 12.852 10.8261L15.5386 15.043C16.2942 14.5617 17.0203 13.9439 17.5349 13.1352L13.3166 10.4508ZM13.3154 10.3574C13.3154 10.4364 13.3062 10.4716 13.3053 10.475C13.3048 10.4771 13.3055 10.4738 13.3086 10.4669C13.3117 10.4598 13.3158 10.4521 13.3205 10.4447L17.531 13.1413C18.0843 12.2773 18.3154 11.3158 18.3154 10.3574H13.3154ZM13.42 10.5115C13.4264 10.5164 13.4154 10.5089 13.3967 10.4861C13.3773 10.4626 13.3573 10.4321 13.3408 10.3973C13.3244 10.3629 13.3171 10.3362 13.3145 10.3237C13.3121 10.3123 13.3154 10.3216 13.3154 10.3574H18.3154C18.3154 8.86059 17.7039 7.50164 16.4746 6.55299L13.42 10.5115ZM12.5547 10.3623C12.8845 10.3623 13.1047 10.399 13.241 10.4372C13.3713 10.4737 13.4164 10.5087 13.42 10.5115L16.4746 6.55299C15.3159 5.65893 13.912 5.3623 12.5547 5.3623V10.3623ZM10.2385 11.0047C11.0185 10.5598 11.7793 10.3623 12.5547 10.3623V5.3623C10.86 5.3623 9.251 5.81191 7.76153 6.66133L10.2385 11.0047ZM12.1764 9.55475L11.2262 7.69537L6.77384 9.97064L7.72403 11.83L12.1764 9.55475ZM12.3633 7.46094C11.1107 7.46094 9.89136 7.89448 8.77997 8.48318L11.1204 12.9016C11.8502 12.515 12.2395 12.4609 12.3633 12.4609V7.46094ZM14.4141 8.04329C13.7154 7.58366 12.9669 7.46094 12.3633 7.46094V12.4609C12.3156 12.4609 12.0183 12.4521 11.666 12.2204L14.4141 8.04329ZM15.7998 10.583C15.7998 9.46399 15.2124 8.54697 14.375 8.01811L11.7051 12.2456C11.51 12.1224 11.2613 11.9093 11.0683 11.5741C10.8712 11.2317 10.7998 10.8789 10.7998 10.583H15.7998ZM14.9323 12.8751C15.484 12.2468 15.7998 11.4509 15.7998 10.583H10.7998C10.7998 10.4145 10.8296 10.224 10.9036 10.0307C10.9772 9.83852 11.078 9.68664 11.1751 9.57608L14.9323 12.8751ZM13.5928 13.9701C14.0372 13.6902 14.5517 13.3213 14.9551 12.8487L11.1523 9.60243C11.1828 9.56673 11.2031 9.54752 11.2065 9.54432C11.2104 9.54071 11.2035 9.54744 11.1824 9.5642C11.1381 9.59929 11.0576 9.65779 10.9287 9.73891L13.5928 13.9701ZM13.4449 14.0244C13.3889 14.1195 13.3483 14.1515 13.3716 14.1299C13.3945 14.1086 13.4597 14.0536 13.5895 13.9722L10.932 9.73683C10.2906 10.1393 9.59987 10.6994 9.13521 11.4893L13.4449 14.0244ZM13.5166 13.8848C13.5166 13.8301 13.5223 13.825 13.5146 13.8568C13.5062 13.8913 13.4848 13.9576 13.4366 14.0384L9.14349 11.4753C8.6776 12.2557 8.5166 13.1012 8.5166 13.8848H13.5166ZM13.2227 16.8652C13.2227 16.8921 13.2191 17.0118 13.1554 17.1823C13.0875 17.3643 12.9694 17.5537 12.8005 17.713L9.36941 14.076C8.5028 14.8936 8.22266 15.9497 8.22266 16.8652H13.2227ZM12.7705 15.9711C12.9391 16.1216 13.0671 16.3101 13.144 16.5052C13.2164 16.6887 13.2227 16.8238 13.2227 16.8652H8.22266C8.22266 17.8228 8.53679 18.894 9.44043 19.7008L12.7705 15.9711ZM12.165 15.6709C12.1166 15.6709 12.4187 15.6631 12.7517 15.9545L9.4592 19.7174C10.3026 20.4554 11.3112 20.6709 12.165 20.6709V15.6709ZM11.5156 15.9792C11.6668 15.8418 11.8291 15.7573 11.9654 15.7122C12.093 15.6699 12.1702 15.6709 12.165 15.6709V20.6709C13.0377 20.6709 14.0465 20.4357 14.8789 19.679L11.5156 15.9792ZM11.0801 16.8652C11.0801 16.8182 11.0873 16.6797 11.1609 16.4944C11.2389 16.2981 11.3671 16.1117 11.5322 15.9643L14.8623 19.6939C15.76 18.8924 16.0801 17.8279 16.0801 16.8652H11.0801ZM11.5124 17.7358C11.3467 17.5823 11.2251 17.3952 11.1532 17.2074C11.0855 17.0306 11.0801 16.9024 11.0801 16.8652H16.0801C16.0801 15.9309 15.7832 14.876 14.9095 14.067L11.5124 17.7358ZM12.165 18.0527C12.2114 18.0527 11.8484 18.0597 11.4775 17.7028L14.9444 14.0999C14.0904 13.2782 13.0301 13.0527 12.165 13.0527V18.0527ZM12.7835 17.7289C12.4066 18.0779 12.0607 18.0527 12.165 18.0527V13.0527C11.3214 13.0527 10.2555 13.2555 9.38644 14.0601L12.7835 17.7289Z" fill="#A7B0B4" mask="url(#path-1-inside-1)"/>
									</svg>
									</span>
								</label>
					  		</div>
					  		<div class="col-md-6">
					  			<div class="field-outer doc-proof">
						  			<label>Front</label>
						  			<img class="lazy-load" data-src="{{URL::asset('media/images/front-image1.png')}}" alt="clck" />
						  			<a href="#">Update photo</a>
						  		</div>
					  		</div>
					  		<div class="col-md-6">
					  			<div class="field-outer doc-proof">
						  			<label>Back</label>
						  			<img class="lazy-load" data-src="{{URL::asset('media/images/backimage1.png')}}" alt="back" />
						  			<a href="#">Update photo</a>
						  		</div>
					  		</div>
					  	</div>
					</div>
				  	
			  	</div>
			  	<div class="col-xxl-4">
			  		<div class="setting-outer-box">
			  			<div class="row first-row">
					  		<div class="col-md-12">
					  			<h3>Payment Methods</h3>
					  		</div>
					  	</div>
					  	<div class="bank-account-list">
					  		<div class="account-list">
					  			<label>Bank Accounts</label>
						  		<div class="outer-box">
						  			<div class="d-flex">
							  			<img class="lazy-load logo" data-src="{{URL::asset('media/images/subtract1.png')}}" alt="logo" />
							  			<span class="center-text">
							  				<h5>Chase Checking Account </h5>
							  				<h5>• • • • • • • • • 9884</h5>
							  			</span>
							  			<a href="#" data-toggle="modal" data-target="#edit-bank-account" class="edit-account"><img class="lazy-load" data-src="{{URL::asset('media/images/edit-account-icon.png')}}" alt="edit icon" /></a>
							  		</div>
							  		<span class="default-method">Default For Receiving Payment</span>
						  		</div>
						  		<a href="#" data-toggle="modal" data-target="#add-bank-account" class="rounded-button gray">Link another bank account</a>
						  	</div>
					  	</div>
					  	<div class="bank-account-list">
					  		<div class="account-list">
					  			<label>Cards</label>
						  		<div class="outer-box">
						  			<div class="d-flex">
							  			<img class="logo lazy-load" data-src="{{URL::asset('media/images/subtract1.png')}}" alt="subtract" />
							  			<span class="center-text">
							  				<h5>Chase Checking Account </h5>
							  				<h5>• • • • • • • • • 9884</h5>
							  			</span>
							  			<a href="#" data-toggle="modal" data-target="#edit-bank-card" class="edit-account"><img class="lazy-load" data-src="{{URL::asset('media/images/edit-account-icon.png')}}" alt="edit icon" /></a>
							  		</div>
							  		<span class="default-method">Default For Receiving Payment</span>
						  		</div>
						  		<a href="#" data-toggle="modal" data-target="#add-bank-account" class="rounded-button gray">Link another card</a>
						  	</div>
					  	</div>
			  		</div>
			  	</div>
		  	</div>
		  </div>
		  <div class="tab-pane fade" id="company-account" role="tabpanel" aria-labelledby="company-account-tab">
		  	<div class="row">
		  		<div class="col-xxl-8">
		  			<div class="setting-outer-box">
			  			<div class="row first-row">
					  		<div class="col-md-6">
					  			<h3>Personal Information</h3>
					  		</div>
					  		<div class="col-md-6 text-right">
					  			<label class="checkbox-container">Activate Company Account
								  <input type="checkbox" checked="checked">
								  <span class="checkmark"></span>
								</label>
					  		</div>
					  	</div>
					  	<div class="row">
					  		<div class="col-md-6">
					  			<div class="field-outer">
						  			<label>Profile Picture</label>
						  			<div class="pro-pic-outer">
						  				<a href="#" class="pic-change">
						  					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<circle cx="12" cy="12" r="12" fill="#FCE300"/>
												<path d="M16.2857 16.2857C17.0726 16.2857 17.7143 15.644 17.7143 14.8572V9.85714C17.7143 9.07031 17.0726 8.42857 16.2857 8.42857H15.0357L14.7511 7.66964C14.6116 7.30134 14.1763 7 13.7857 7H10.9286C10.5379 7 10.1027 7.30134 9.96317 7.66964L9.67857 8.42857H8.42857C7.64174 8.42857 7 9.07031 7 9.85714V14.8572C7 15.644 7.64174 16.2857 8.42857 16.2857H16.2857ZM12.3571 14.8572C10.9788 14.8572 9.85715 13.7355 9.85715 12.3571C9.85715 10.9788 10.9788 9.85714 12.3571 9.85714C13.7355 9.85714 14.8572 10.9788 14.8572 12.3571C14.8572 13.7355 13.7355 14.8572 12.3571 14.8572ZM12.3571 13.9643C13.2444 13.9643 13.9643 13.2444 13.9643 12.3571C13.9643 11.4699 13.2444 10.75 12.3571 10.75C11.4699 10.75 10.75 11.4699 10.75 12.3571C10.75 13.2444 11.4699 13.9643 12.3571 13.9643Z" fill="black"/>
											</svg>
						  				</a>
						  				<img class="lazy-load" data-src="http://localhost/timeshareapp/public/media/images/jonathansidwell.png" alt="user">
						  			</div>
						  		</div>

						  		<div class="field-outer">
						  			<label>Company Name</label>
						  			<input type="text" placeholder="John">
						  		</div>
						  		<div class="field-outer">
						  			<label>Industry</label>
						  			<select>
						  				<option>Travel</option>
						  				<option>Travel</option>
						  				<option>Travel</option>
						  			</select>
						  		</div>
						  		<div class="field-outer">
						  			<label>Business Website</label>
							  			<div class='input-group date  calendar'>
	                                    <input type="text" placeholder="got2go.com">
                                	</div>
						  		</div>
						  		<div class="field-outer">
						  			<label>Employer Identification Number</label>
						  			<input type="password" placeholder="39-4829104">
						  		</div>
					  		</div>
					  		<div class="col-md-6">
					  			<div class="field-outer">
						  			<label>Phone Number</label>
						  			<input type="number" placeholder="(898) 309-3903">
						  		</div>
						  		<div class="field-outer">
						  			<label>Email Address</label>
						  			<input type="text" placeholder="john@johnsmith.com">
						  		</div>
						  		<div class="field-outer">
						  			<label>Address</label>
						  			<input type="text" placeholder="Address 1" >
						  			<input type="text" placeholder="Address 2" >
						  			<input type="text" placeholder="City" >
						  			<div class="row">
						  				<div class="col-md-7">
						  					<input type="text" placeholder="State" >
						  				</div>
						  				<div class="col-md-5">
						  					<input type="text" placeholder="Zip Code" >
						  				</div>
						  			</div>
						  		</div>
						  		<div class="field-outer">
						  			<label>Country</label>
						  			<input type="text" placeholder="United States">
						  		</div>
					  		</div>
					  	</div>
					  	
					</div>
				  	
			  	</div>
			  	<div class="col-xxl-4">
			  		<div class="setting-outer-box">
			  			<div class="row first-row">
					  		<div class="col-md-12">
					  			<h3>Payment Methods</h3>
					  		</div>
					  	</div>
					  	<div class="bank-account-list">
					  		<div class="account-list">
					  			<label>Bank Accounts</label>
						  		<div class="outer-box">
						  			<div class="d-flex">
							  			<img class="logo lazy-load" data-src="{{URL::asset('media/images/subtract1.png')}}" alt="subtract" />
							  			<span class="center-text">
							  				<h5>Chase Checking Account </h5>
							  				<h5>• • • • • • • • • 9884</h5>
							  			</span>
							  			<a href="#" class="edit-account"><img class="lazy-load" data-src="{{URL::asset('media/images/edit-account-icon.png')}}" alt="edit account" /></a>
							  		</div>
							  		<span class="default-method">Default For Receiving Payment</span>
						  		</div>
						  		<a href="#" class="rounded-button gray">Link another bank account</a>
						  	</div>
					  	</div>
					  	<div class="bank-account-list">
					  		<div class="account-list">
					  			<label>Cards</label>
						  		<div class="outer-box">
						  			<div class="d-flex">
							  			<img class="logo lazy-load" data-src="{{URL::asset('media/images/subtract1.png')}}" alt="subtract" />
							  			<span class="center-text">
							  				<h5>Chase Checking Account </h5>
							  				<h5>• • • • • • • • • 9884</h5>
							  			</span>
							  			<a href="#" class="edit-account"><img class="lazy-load" data-src="{{URL::asset('media/images/edit-account-icon.png')}}" alt="edit account" /></a>
							  		</div>
							  		<span class="default-method">Default For Receiving Payment</span>
						  		</div>
						  		<a href="#" class="rounded-button gray">Link another card</a>
						  	</div>
					  	</div>
			  		</div>
			  	</div>
		  	</div>
		  </div>
		  
		</div>
	</div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade cmn-modal-owner" id="activeindividual-account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         	<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M2.5 2.5L20.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			<path d="M20.5 2.5L2.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			</svg>
        </button>
      <div class="modal-body">
        <h3>Are you sure?</h3>
        <p>Do you want to activate your individual account?  This will disable your company profile as the owner account.</p>
        <a href="#" class="gradient-btn">Activate Individual Owner Account</a><br>
        <a href="#" data-dismiss="modal" class="cancel-popup">Cancel </a>                                                                                                                     
      </div>
    </div>
  </div>
</div>

<div class="modal fade cmn-modal-owner large" id="edit-bank-account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         	<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M2.5 2.5L20.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			<path d="M20.5 2.5L2.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			</svg>
        </button>
      <div class="modal-body">
        <h3>Edit Bank Account</h3>
        <div class="inner-form">
	        <form>
	        	<div class="row">
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Bank Account Name</label>
		        			<input type="text" placeholder="Bank Account Name">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Routing Number</label>
		        			<input type="number" placeholder="Routing Number">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Account Number</label>
		        			<input type="number" placeholder="Account Number">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Institution Type</label>
		        			<input type="text" placeholder="Bank">
		        		</div>
	        		</div>
	        		<div class="col-md-12">
	        			<label class="toggle-title">Make this my default method to receive payment</label>
	        			<label class="switch-custom">
						  <input type="checkbox" checked>
						  <span class="slider-custom round"></span>
						</label>
	        		</div>
	        		<div class="col-md-12">
	        			<div class="field-container">
	        				<a href="#" class="remove-account">Remove this Bank Account</a>
	        			</div>
	        		</div>
	        	</div>
	        </form>
	    </div>
        <a href="#" class="gradient-btn">Save Changes</a><br>
        <a href="#" data-dismiss="modal" class="cancel-popup">Cancel </a>                                                                                                                     
      </div>
    </div>
  </div>
</div>

<div class="modal fade cmn-modal-owner large" id="add-bank-account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         	<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M2.5 2.5L20.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			<path d="M20.5 2.5L2.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			</svg>
        </button>
      <div class="modal-body">
        <h3>Add Bank Account</h3>
        <div class="inner-form" style="margin-bottom: 40px;">
	        <form>
	        	<div class="row">
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Bank Account Name</label>
		        			<input type="text" placeholder="Bank Account Name">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Routing Number</label>
		        			<input type="number" placeholder="Routing Number">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Account Number</label>
		        			<input type="number" placeholder="Account Number">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Institution Type</label>
		        			<input type="text" placeholder="Bank">
		        		</div>
	        		</div>
	        		<div class="col-md-12">
	        			<label class="toggle-title">Make this my default method to receive payment</label>
	        			<label class="switch-custom">
						  <input type="checkbox">
						  <span class="slider-custom round"></span>
						</label>
	        		</div>
	        		
	        	</div>
	        </form>
	    </div>
        <a href="#" class="gradient-btn">Save Bank Account </a><br>
        <a href="#" data-dismiss="modal" class="cancel-popup">Cancel </a>                                                                                                                     
      </div>
    </div>
  </div>
</div>

<div class="modal fade cmn-modal-owner large" id="edit-bank-card" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         	<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M2.5 2.5L20.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			<path d="M20.5 2.5L2.5 20.5" stroke="#3D3D3D" stroke-width="5"/>
			</svg>
        </button>
      <div class="modal-body">
        <h3>Edit Card</h3>
        <div class="inner-form">
	        <form>
	        	<div class="row">
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Cardholder Name</label>
		        			<input type="text" placeholder="Cardholder Name">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Card Name</label>
		        			<input type="number" placeholder="Card Name">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="field-container">
		        			<label>Card Number</label>
		        			<input type="number" placeholder="Card Number">
		        		</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div class="row">
	        				<div class="col-md-8">
	        					<div class="field-container">
				        			<label>Exp. Date</label>
				        			<input type="number" placeholder="Exp. Date">
				        		</div>
	        				</div>
	        				<div class="col-md-4">
	        					<div class="field-container">
				        			<label>CVV</label>
				        			<input type="password" placeholder="CVV">
				        		</div>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="col-md-12">
	        			<label class="toggle-title">Make this my default payment method</label>
	        			<label class="switch-custom">
						  <input type="checkbox">
						  <span class="slider-custom round"></span>
						</label>
	        		</div>
	        		<div class="col-md-12">
	        			<div class="field-container">
	        				<a href="#" class="remove-account">Remove this Bank Account</a>
	        			</div>
	        		</div>
	        	</div>
	        </form>
	    </div>
        <a href="#" class="gradient-btn">Save Changes</a><br>
        <a href="#" data-dismiss="modal" class="cancel-popup">Cancel </a>                                                                                                                     
      </div>
    </div>
  </div>
</div>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
@endsection
