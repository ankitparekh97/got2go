@extends('layouts.app')

@section('content')
<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');
?>
<style>
#kt_header_menu_wrapper { display: none;}

.input-group i.flaticon-lock::after,
.input-group i.flaticon-lock::before {
    font-weight: bolder !important;
}
a.text-hover-primary:hover{
   color: #fff !important;
   font-weight: bold;
}
</style>
<div class="d-flex flex-row flex-column-fluid container">
     <!--begin::Content Wrapper-->
     <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <h2 class="f-w-700 text-uppercase text-dark d-block w-100 align-items-center text-center">
                Register Here
            </h2>
            </div>
        </div>
        <!--end::Subheader-->
        <div class="content flex-column-fluid d-lg-flex" id="kt_content" style="background-image: url('assets/media/images/login-bg.jpg')">

            <!-- Register start -->
            <div class="login-container align-items-center w-100">
                <div class="alert alert-danger" style="display:none;">
                    <ul style="margin-bottom:0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="d-table" style="height:0">
                    <div class="d-table-cell vertical-middle">
                        <form method="POST" id="ownersignup" action="{{ route('ownersignup') }}">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-user"></i>
                                            </span>
                                        </div>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') }}" autocomplete="off" placeholder="First Name" required autofocus>
                                    </div>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-user"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control required @error('last_name') is-invalid @enderror" name="last_name" id="lasst_name" value="{{ old('last_name') }}" placeholder="Last Name" autocomplete="off" autofocus>
                                    </div>
                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-envelope"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control required email @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off" />
                            </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-phone-alt"></i>
                                    </span>
                                </div>
                                <input type="number" class="form-control required @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{ old('phone') }}" placeholder="Contact Number" autocomplete="off"  />
                            </div>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="flaticon-lock"></i>
                                </span>
                                </div>
                                <input type="password" passwordCheck="passwordCheck" class="form-control required @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" autocomplete="off"  />
                            </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="flaticon-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control required @error('confirm_password') is-invalid @enderror" name="confirm_password" id="confirm_password" placeholder="Confirm Password" autocomplete="off"  />
                            </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="input-group">
                                <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA') }}" data-callback="recaptchaCallback"></div>
                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                                <script src='https://www.google.com/recaptcha/api.js'></script>
                            </div>
                            <div class="input-group">
                                <div class="">
                                    <label class="checkbox">
                                        <input type="checkbox" name="terms" id="terms" required>
                                        <span style="border:1px solid #000"></span>&nbsp;By signing up, you agree to our &nbsp;<a class="termsConditions" href="#" target="_blank">Terms & Conditions</a>
                                    </label>
                                </div>
                            </div>
                            <input type="submit" value="SIGN UP">
                            <div class="signIn text-center">
                                Already have a account? <a href="{{route('ownerloginform')}}">Sign In</a>
                            </div>
                        </form>

                        <div class="pt-10 pb-5 text-center">
                            <h3>OR</h3>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('auth/owner/google') }}" class="yellow btn btn-light-primary font-weight-bolder px-8 py-4 my-3 font-size-lg">
                                <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:assets/media/svg/social-icons/google.svg--><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                                    <path d="M19.9895 10.1871C19.9895 9.36767 19.9214 8.76973 19.7742 8.14966H10.1992V11.848H15.8195C15.7062 12.7671 15.0943 14.1512 13.7346 15.0813L13.7155 15.2051L16.7429 17.4969L16.9527 17.5174C18.879 15.7789 19.9895 13.221 19.9895 10.1871Z" fill="#4285F4"></path>
                                    <path d="M10.1993 19.9313C12.9527 19.9313 15.2643 19.0454 16.9527 17.5174L13.7346 15.0813C12.8734 15.6682 11.7176 16.0779 10.1993 16.0779C7.50243 16.0779 5.21352 14.3395 4.39759 11.9366L4.27799 11.9466L1.13003 14.3273L1.08887 14.4391C2.76588 17.6945 6.21061 19.9313 10.1993 19.9313Z" fill="#34A853"></path>
                                    <path d="M4.39748 11.9366C4.18219 11.3166 4.05759 10.6521 4.05759 9.96565C4.05759 9.27909 4.18219 8.61473 4.38615 7.99466L4.38045 7.8626L1.19304 5.44366L1.08875 5.49214C0.397576 6.84305 0.000976562 8.36008 0.000976562 9.96565C0.000976562 11.5712 0.397576 13.0882 1.08875 14.4391L4.39748 11.9366Z" fill="#FBBC05"></path>
                                    <path d="M10.1993 3.85336C12.1142 3.85336 13.406 4.66168 14.1425 5.33717L17.0207 2.59107C15.253 0.985496 12.9527 0 10.1993 0C6.2106 0 2.76588 2.23672 1.08887 5.49214L4.38626 7.99466C5.21352 5.59183 7.50242 3.85336 10.1993 3.85336Z" fill="#EB4335"></path>
                                </svg><!--end::Svg Icon-->
                                </span>
                                Sign up with Google
                            </a>
                            <a href="{{ url('auth/owner/facebook') }}" class="yellow btn btn-light-primary font-weight-bolder px-8 py-4 my-3 font-size-lg">
                                <span class="svg-icon svg-icon-md">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="2500" height="2500" viewBox="126.445 2.281 589 589"><circle cx="420.945" cy="296.781" r="294.5" fill="#3c5a9a"/><path d="M516.704 92.677h-65.239c-38.715 0-81.777 16.283-81.777 72.402.189 19.554 0 38.281 0 59.357H324.9v71.271h46.174v205.177h84.847V294.353h56.002l5.067-70.117h-62.531s.14-31.191 0-40.249c0-22.177 23.076-20.907 24.464-20.907 10.981 0 32.332.032 37.813 0V92.677h-.032z" fill="#fff"/></svg>
                               </span>
                               Sign up with Facebook
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Register end -->
        </div>
     </div>
     <!--end::Content Wrapper-->

</div>
<script>
    var ownersignup = "{{ route('ownersignup') }}";
</script>
<script src="{{ URL::asset('js/owner/login/register.js') }}"></script>
@endsection
