@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{URL::asset('css/paymenthistory.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/booking.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <div class="custom-container">
        <!--begin::Content Wrapper-->
        <div class="paymenthistory-wrapper">
            <div class="property-edit">
                <ul class="nav nav-tabs listing-tabs-current" id="listingtab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="history-tab" data-tab="history" data-toggle="tab" href="#history" role="tab" aria-controls="home" aria-selected="true">Payment History</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="confirmed-tab" data-tab="confirmed" data-toggle="tab" href="#confirmed" role="tab" aria-controls="profile" aria-selected="false">Payment Methods</a>
                    </li>
                </ul>
                <div class="tab-content" id="listingtabcontent">
                    <div class="tab-pane fade active show" id="history" role="tabpanel" aria-labelledby="history-tab">
                        <div class="booking-list-outer">
                            <div class="booking-container-outer border-bottom">
                                <div class="table-responsive custom-resp">
                                    <table id="historylist" class="booking-table table display propertyBooking custom-data-table">
                                    <caption></caption>
                                    <thead>
                                        <tr>
                                            <th scope="col"> </th>
                                            <th scope="col">Reservation #</th>
                                            <th scope="col">Payment Date </th>
                                            <th scope="col">Payment Status </th>
                                            <th scope="col">Payment Amount</th>
                                            <th scope="col">Renter </th>
                                            <th scope="col">Property </th>
                                            <th scope="col">Actions </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="confirmed" role="tabpanel" aria-labelledby="confirmed-tab">
                        <div class="booking-list-outer">
                            <div class="booking-container-outer">
                                <div class="table-responsive custom-resp">
                                    <table id="paymentMethodsBank" class="table display mycustomdata-table custom-data-table1">
                                    <caption></caption>
                                        <thead>
                                            <tr>
                                                <th scope="col">Bank Accounts</th>
                                                <th scope="col">Institution Type </th>
                                                <th scope="col">Account Number </th>
                                                <th scope="col">Routing Number</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div class="account-link mt-4 mb-4 "><a href="#" id="addBankHistoryModal" data-event="add""> Link another bank account </a> </div>
                                    <table id="paymentMethodsCredit" class="table display mycustomdata-table custom-data-table1">
                                    <caption></caption>
                                        <thead>
                                            <tr>
                                                <th scope="col">Card</th>
                                                <th scope="col">Card Type </th>
                                                <th scope="col">Card Number </th>
                                                <th scope="col">Expiration Date</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="account-link mt-4 mb-4 "><a href="#" id="addCardModal" data-event="add"> Add new debit or credit card </a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Content Wrapper-->
    </div>
    <!-- Approve Modal -->
    <div class="modal fade cmn-popup approve" id="cancelBooking" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Please confirm refund details below ?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="refund-wrap">
                        <ul class="unstyled-list">
                            <li>
                                <div class="refund-subtitle">Reservation #</div>
                                <div class="refund-title"><span id="reservationId"></span></div>
                            </li>
                            <li>
                                <div class="refund-subtitle">Refund Amount</div>
                                <div class="refund-title"><span id="refundamount"></span></div>
                            </li>
                        </ul>
                    </div>
                    <!-- <p class="text-center content">Reservation ID: <span id="reservationId"></span>
                    <br>Refunded Amount: <span id="refundamount"></span></p>                     -->
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn approveBooking">Send Refund</button><br>
                        <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Add Bank Account Modal -->
     <div class="modal fade cmn-popup payment-modal  approve" id="addBankAccount">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="mb-8 bankEvent">Add Bank Account</h4>
                    <button type="button" class="close cancelBankInfo" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-60">
                    <form id="bankDetailsSave" name="bankDetailsSave" method="POST">
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Account Holder Name</label>
                                    <input type="text" class="form-control" id="accountHolderName" name="accountHolderName" placeholder="Account Holder name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Routing Number</label>
                                    <input type="text" class="form-control" id="routingNumber" name="routingNumber" placeholder="Routing Number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Account Number</label>
                                    <input type="text" class="form-control accountNumber" id="accountNumber" name="accountNumber" placeholder="Account Number" maxlength="16">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Institution Type</label>
                                    <input type="text" class="form-control" readonly id="accountHolderType" name="accountHolderType" value="individual" placeholder="Institution Type">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="date-atcion">
                                    <span class="switch cust-switch ">
                                        <label>
                                            <em class="switch-txt mr-3">Make this my default payment method</em>
                                            <input type="checkbox" checked="checked" name="default_option" id="default_option">
                                            <span></span>

                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="text-center delete-btns">
                            <button type="submit" class="btn btn-default red-btn-cmn approveBooking savebank">Save</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn cancelBankInfo">Cancel Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
      <!-- End Bank Account Modal -->

         <!-- Edit Bank Account Modal -->
     {{-- <div class="modal fade cmn-popup payment-modal  approve" id="EditBankAccount" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="mb-8">Edit Bank Account</h4>
                    <button type="button" class="close cancelEditBankDetails" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-60">
                    <form id="bankDetailsEdit" name="bankDetailsEdit" method="POST">
                        <input type="hidden" name="bank_account_id" id="bank_account_id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Account Holder Name</label>
                                    <input type="text" class="form-control" id="account_holder_name" name="account_holder_name" placeholder="Account Holder Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Routing Number</label>
                                    <input type="text" class="form-control"  readonly id="routing_number_edit" name="routing_number_edit" placeholder="• • • • • 9884">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Account Number</label>
                                    <input type="text" class="form-control" readonly id="account_number_edit" name="account_number_edit" placeholder="• • • • • 9884">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Institution Type</label>
                                    <input type="text" class="form-control" readonly id="accountHolderType" name="accountHolderType" value="individual" placeholder="Institution Type">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="date-atcion">
                                    <span class="switch cust-switch ">
                                        <label>
                                            <em class="switch-txt mr-3">Make this my default payment method</em>
                                            <input type="checkbox" checked="checked" name="default_option_edit_bank" id="default_option_edit_bank">
                                            <span></span>

                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="text-center delete-btns">
                            <button type="submit" class="btn btn-default red-btn-cmn approveBooking editBank_details">Save Changes</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn cancelEditBankDetails">Cancel Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
      <!-- End Edit Bank Account Modal -->

       <!-- Add Card Modal -->
     <div class="modal fade cmn-popup payment-modal  approve" id="addCard">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="mb-8 event">Add Card</h4>
                    <button type="button" class="close cancelCardInfo" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-60">
                    <form id="creditCardSave" name="creditCardSave" method="POST">
                    <input type="hidden" name="paymentMethodNonce" id="paymentMethodNonce">
                    <input type="hidden" id="card_type" name="card_type">
                    <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for=" ">Cardholder Name</label>
                                    <input type="text" class="form-control" id="name_on_card" name="name_on_card" placeholder="Name on card">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group card">
                                    <label for=" ">Card Number</label>
                                    <input type="text" class="form-control" maxlength="19" name="last_four" id="last_four" placeholder="Card Number">
                                    <span class="card_icon"></span>
                                    <p class="status" style="display: none;">
                                        <span class="status_message" style="display: none;"></span>
                                    </p>
                                </div>
                        </div>
                        </div>
                        <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label for=" ">Exp. Date</label>
                                            <input type="text" class="form-control" name="expiration_date" id="expiration_date" maxlength='9' placeholder="MM/YYYY">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <label for=" ">CVV</label>
                                            <input type="password" class="form-control" name="cvv" id="cvv" placeholder="CVV">
                                        </div>
                                    </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="date-atcion">
                                    <span class="switch cust-switch ">
                                        <label>
                                            <em class="switch-txt mr-3">Make this my default payment method</em>
                                            <input type="checkbox" checked="checked" name="is_default_card" id="is_default_card">
                                            <span></span>

                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="text-center delete-btns">
                            <button type="submit" class="btn btn-default red-btn-cmn saveCardinfo">Save</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn cancelCardInfo">Cancel Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
      <!-- End Add Card Modal -->

       <!-- Edit Card Modal -->
    {{-- <div class="modal fade cmn-popup payment-modal  approve" id="editCard" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="mb-8">Edit Card</h4>
                    <button type="button" class="close cancelEditCardDetails" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-60">
                <form id="editCreditCardSave" name="editCreditCardSave" method="POST">
                <input type="hidden" name="credit_card_id" id="credit_card_id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=" ">Cardholder Name</label>
                                    <input type="text" class="form-control" id="name_on_card" name="name_on_card" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                    <label for=" ">Card Number</label>
                                    <input type="text" class="form-control" readonly id="card_number_edit" placeholder="• • • •   • • • •   • • • •  0598">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for=" ">Exp. Date</label>
                                            <input type="text" class="form-control" name="expiration_date_edit" id="expiration_date_edit"  maxlength='9' placeholder="MM/YYYY">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for=" ">CVV</label>
                                            <input type="password" class="form-control" readonly id="cvv_number" placeholder="CVV">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="date-atcion">
                                    <span class="switch cust-switch ">
                                        <label>
                                            <em class="switch-txt mr-3">Make this my default payment method</em>
                                            <input type="checkbox" checked="checked" name="default_option_edit" id="default_option_edit">
                                            <span></span>


                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="text-center delete-btns">
                            <button type="submit" class="btn btn-default red-btn-cmn approveBooking edit_card_details">Save Changes</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn cancelEditCardDetails">Cancel Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
      <!-- End Edit Card Modal -->

          <!-- Delete Bank Modal -->
     <div class="modal fade cmn-popup payment-modal  approve" id="delete-credit" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-60">
                    <form>
                       <h3 class="sub-heading mb-8"> Are you sure you want to delete this payment method? </h3>
                        <input type="hidden" id="card_id" name="card_id">
                        <input type="hidden" id="card_type" name="card_type">
                        <div class="text-center delete-btns">
                            <button type="button" class="btn btn-default red-btn-cmn approveBooking deleteCredit" id="deleteCredit">Yes</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">No</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<style>
        .card{
    border: none;
}
 .card .card_icon {
    background: transparent url(/media/credit_card_sprites.png) no-repeat 30px 0;
    position: absolute;

    left: 234px !important;
    top: 47px;
}
        /* #cardNumber,#expirationDate,#cvv,#nameOnCard  {
            font-size: 13px;
            padding: 15px 20px;
            height: 55px;
            background:#F9F9F9;
            border-radius:25px;
            margin-bottom: 5px;
        }
        span.card_icon {
            /*background-image: url(http://localhost:8000/media/images/cc-icons/visa.png);
            background-repeat: no-repeat;
            background-position: top left;
            float: right;
            width: 27px;
            height: 19px;
            position: absolute;
            right: 35px;
            top: 30px;
        } */

    </style>
<script>
    app = {
        monthAndSlashRegex: /^\d\d \/ $/, // regex to match "MM / "
        monthRegex: /^\d\d$/, // regex to match "MM"

        el_cardNumber: '.ccFormatMonitor',
        el_expDate: '#expiration_date',
        el_cvv: '.cvv',
        el_ccUnknown: 'cc_type_unknown',
        el_ccTypePrefix: 'cc_type_',
        el_monthSelect: '#monthSelect',
        el_yearSelect: '#yearSelect',

        cardTypes: {
        'American Express': {
                    name: 'American Express',
                    code: 'ax',
                    security: 4,
                    pattern: /^3[47]/,
                    valid_length: [15],
                    formats: {
                    length: 15,
                    format: 'xxxx xxxxxxx xxxx'
                    }
        },
        'Visa': {
                    name: 'Visa',
                    code: 'vs',
                    security: 3,
                    pattern: /^4/,
                    valid_length: [16],
                    formats: {
                            length: 16,
                            format: 'xxxx xxxx xxxx xxxx'
                        }
                },
        'Maestro': {
                    name: 'Maestro',
                    code: 'ma',
                    security: 3,
                    pattern: /^(50(18|20|38)|5612|5893|63(04|90)|67(59|6[1-3])|0604)/,
                    valid_length: [16],
                    formats: {
                            length: 16,
                            format: 'xxxx xxxx xxxx xxxx'
                        }
                },
        'Mastercard': {
                    name: 'Mastercard',
                    code: 'mc',
                    security: 3,
                    pattern: /^5[1-5]/,
                    valid_length: [16],
                    formats: {
                            length: 16,
                            format: 'xxxx xxxx xxxx xxxx'
                        }
                }
        }
    };

    app.addListeners = function() {
        $(app.el_expDate).on('keydown', function(e) {
            app.removeSlash(e);
        });

        $(app.el_expDate).on('keyup', function(e) {
            app.addSlash(e);
        });

        $(app.el_expDate).on('blur', function(e) {
            app.populateDate(e);
        });

        $(app.el_cvv +', '+ app.el_expDate).on('keypress', function(e) {
            return e.charCode >= 48 && e.charCode <= 57;
        });
    };

    app.addSlash = function (e) {
        var isMonthEntered = app.monthRegex.exec(e.target.value);
        if (e.key >= 0 && e.key <= 9 && isMonthEntered) {
        e.target.value = e.target.value + " / ";
        }
    };

    app.removeSlash = function(e) {
        var isMonthAndSlashEntered = app.monthAndSlashRegex.exec(e.target.value);
        if (isMonthAndSlashEntered && e.key === 'Backspace') {
        e.target.value = e.target.value.slice(0, -3);
        }
    };

    app.populateDate = function(e) {
        var month, year;

        if (e.target.value.length == 7) {
        month = parseInt(e.target.value.slice(0, -5));
        year = "20" + e.target.value.slice(5);

        if (app.checkMonth(month)) {
            $(app.el_monthSelect).val(month);
        } else {
            $(app.el_monthSelect).val(0);
        }

        if (app.checkYear(year)) {
            $(app.el_yearSelect).val(year);
        } else {
            $(app.el_yearSelect).val(0);
        }

        }
    };

    app.checkMonth = function(month) {
        if (month <= 12) {
        var monthSelectOptions = app.getSelectOptions($(app.el_monthSelect));
        month = month.toString();
        if (monthSelectOptions.includes(month)) {
            return true;
        }
        }
    };

    app.checkYear = function(year) {
        var yearSelectOptions = app.getSelectOptions($(app.el_yearSelect));
        if (yearSelectOptions.includes(year)) {
        return true;
        }
    };

    app.getSelectOptions = function(select) {
        var options = select.find('option');
        var optionValues = [];
        for (var i = 0; i < options.length; i++) {
        optionValues[i] = options[i].value;
        }
        return optionValues;
    };

    app.setMaxLength = function ($elem, length) {
        if($elem.length && app.isInteger(length)) {
        $elem.attr('maxlength', length);
        }else if($elem.length){
        $elem.attr('maxlength', '');
        }
    };

    app.isInteger = function(x) {
        return (typeof x === 'number') && (x % 1 === 0);
    };

    app.createExpDateField = function() {
        $(app.el_monthSelect +', '+ app.el_yearSelect).hide();
        $(app.el_monthSelect).parent().prepend('<input type="text" class="ccFormatMonitor">');
    };


    app.isValidLength = function(cc_num, card_type) {
        for(var i in card_type.valid_length) {
        if (cc_num.length <= card_type.valid_length[i]) {
            return true;
        }
        }
        return false;
    };

    app.getCardType = function(cc_num) {
        for(var i in app.cardTypes) {
        var card_type = app.cardTypes[i];
        if (cc_num.match(card_type.pattern) && app.isValidLength(cc_num, card_type)) {
            return card_type;
        }
        }
    };

    app.getCardFormatString = function(cc_num, card_type) {
        for(var i in card_type.formats) {
        var format = card_type.formats[i];
        if (cc_num.length <= format.length) {
            return format;
        }
        }
    };

    app.formatCardNumber = function(cc_num, card_type) {
        var numAppendedChars = 0;
        var formattedNumber = '';
        var cardFormatIndex = '';

        if (!card_type) {
        return cc_num;
        }

        var cardFormatString = app.getCardFormatString(cc_num, card_type);
        for(var i = 0; i < cc_num.length; i++) {
        cardFormatIndex = i + numAppendedChars;
        if (!cardFormatString || cardFormatIndex >= cardFormatString.length) {
            return cc_num;
        }

        if (cardFormatString.charAt(cardFormatIndex) !== 'x') {
            numAppendedChars++;
            formattedNumber += cardFormatString.charAt(cardFormatIndex) + cc_num.charAt(i);
        } else {
            formattedNumber += cc_num.charAt(i);
        }
        }

        return formattedNumber;
    };

    app.monitorCcFormat = function($elem) {
        var cc_num = $elem.val().replace(/\D/g,'');
        var card_type = app.getCardType(cc_num);
        $elem.val(app.formatCardNumber(cc_num, card_type));
        app.addCardClassIdentifier($elem, card_type);
    };

    app.addCardClassIdentifier = function($elem, card_type) {
        var classIdentifier = app.el_ccUnknown;
        if (card_type) {
        classIdentifier = app.el_ccTypePrefix + card_type.code;
        app.setMaxLength($(app.el_cvv), card_type.security);
        } else {
        app.setMaxLength($(app.el_cvv));
        }

        if (!$elem.hasClass(classIdentifier)) {
        var classes = '';
        for(var i in app.cardTypes) {
            classes += app.el_ccTypePrefix + app.cardTypes[i].code + ' ';
        }
        $elem.removeClass(classes + app.el_ccUnknown);
        $elem.addClass(classIdentifier);
        }
    };


    app.init = function() {

        $(document).find(app.el_cardNumber).each(function() {
        var $elem = $(this);
        if ($elem.is('input')) {
            $elem.on('input', function() {
            app.monitorCcFormat($elem);
            });
        }
        });

        app.addListeners();

    }();
</script>
<script src="{{URL::asset('js/cardcheck.js')}}"></script>
<script>
    var historyListing = "{{route('historyList')}}";
    var paymentMethods = "{{ route('owner.paymentMethods') }}";
    var paymentMethodCredit = "{{ route('owner.paymentMethodCredit') }}";
    var deletePayment = "{{ route('deletePayment') }}";
    var saveCard =  "{{ route('saveCard') }}";
    var processingImg = "{{URL::asset('media/images/loader.gif')}}";
    var saveBankDetails =  "{{ route('owner.saveBank') }}";
    var editPayment = "{{ route('editCard') }}";
    var editBankPayment = "{{ route('editBankDetails') }}";
</script>
<script src="{{ URL::asset('js/owner/payment/history.js') }}"></script>
@endsection
