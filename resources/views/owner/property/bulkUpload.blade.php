@extends('layouts.app')
@section('content')

<div class="custom-container">
  <link rel="stylesheet" href="{{ URL::asset('css/jquery.dm-uploader.min.css') }}">
<style>
#debug {
  overflow-y: scroll !important;
  height: 180px;
}

.dm-uploader {
  border: 0.25rem dashed #A5A5C7;
  text-align: center;
}
.dm-uploader.active {
  border-color: red;

  border-style: solid;
}
.upload-start h5 {
    display: none;
}
.upload-start .btn.btn-primary.btn-block.mb-5 {
  display: none;
}
.file-list {
  display: none !important;
}
.upload-start .file-list {
  display: block !important;
}
</style>

<h1>Bulk Upload Listings</h1>
<div class="row">
   <div class="col-lg-3 left-listing-progress">
      <div class="list-white-bg">
         <ul class="nav nav-tabs custom-tab bulkupload" id="ListingTab" role="tablist">
            <li class="nav-item">
               <a class="nav-link active" id="basic-tab" data-toggle="tab" href="#basic" role="tab" aria-controls="home" aria-selected="true">Bulk Uploads</a>
            </li>
            <li class="nav-item">
               <a class="nav-link disabled" id="property-details-tab" data-toggle="tab" href="#property-details" role="tab" aria-controls="profile" aria-selected="false">Upload Files</a>
            </li>
            {{-- <li class="nav-item">
               <a class="nav-link disabled" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="contact" aria-selected="false">Review </a>
            </li> --}}
         </ul>
      </div>
   </div>
   <div class="col-lg-9">
      <div class="list-white-bg">
         <div class="">
            <div class="">
               <form id="bulk-upload-form">
                   <input type="hidden" id="unique_key" name="unique_key" value="{{$unique_key}}">
                   <input type="hidden" id="is_reupload" name="is_reupload" value="{{ ($unique_key) ? 1 : 0}}">
                  <div class="tab-content upload-tabs" id="myTabContent">
                     <div class="tab-pane fade show active" id="basic" role="tabpanel" aria-labelledby="basic-tab">
                        <div class="outer-table">
                           <div class="row-custom">
                              <div class="left-label">
                                 <label>What to expect when bulk uploading listings.</label>
                              </div>
                              <div class="right-input">
                                 <p>Bulk upload lets you load all your rental properties at once, whether you have just a handful or thousands of listings, in a streamlined manner. Instead of wasting hours uploading your listings one by one, follow the steps below:</p>
                                 <p class="bold">1. Curate your property details in our Excel template. </p>
                                 <p>Enter in, copy & paste, or import your property details into the spreadsheet we provide to ensure you are loading in all the relevant info we’ll need. </p>
                                 <p class="bold">2. Upload all your property images into relevant folders in a ZIP file.</p>
                                 <p>That’s right! You can even bulk upload all the photos to go along your listings. Just make sure the images are in folders labeled appropriately to match your property ID. Once done, zip it up and drag & drop!</p>
                                 <p class="bold">3. Attach verification documentation for each listing uploaded.</p>
                                 <p>We will need to verify you as the owner for every rental property - timeshare or private residence - before the listings can be published. We enable you to do this using the same workflow as the property images - just make sure they are in the right folder!</p>
                              </div>
                           </div>
                           <p class="bulk-upload-text">Have a question?  We are here for you!  Contact us by email at <a href="mailto:admin@got2go.com">admin@got2go.com</a> or by phone at (844) I-GOT2GO.</p>
                        </div>
                        <div class="outer-table">
                           <div class="row-custom">
                                <div class="left-label">
                                    <label>Please upload a CSV file in the correct format (see example).</label>
                                    <label class="light">Please download and correctly fill out the template below.  Once filled out, please save as a CSV file and upload it here.  </label>
                                    <a href="javascipt:void(0)" id="csv-download" class="download-template">Download Excel Template</a>
                                </div>
                                <div class="right-input">
                                    <!-- Our markup, the important part here! -->
                                    <div id="propertyCsv" class="propertyCsv drag-and-drop-zone dm-uploader p-5">
                                        <input type="hidden" id="UploadedCsv" class="hdnData" name="UploadedCsv">
                                        <ul class="list-unstyled p-2 files file-list" id="">
                                            <li class="text-muted text-center empty">No files uploaded.</li>
                                        </ul>
                                        <p class="error-label-outer-p" style="color: red; font-size:10px;padding-bottom: 0px;"><span class="error-icon-text"><img class="lazy-load" data-src="{{URL::asset('media/images/error-red-icon.png')}}"alt="icon"></span>Please be sure to save as a CSV file before attaching the bulk upload template.</p>
                                        <h5 class="text-muted">Drag & drop CSV file here</h5>

                                        <h5 class="light">or</h5>
                                        <div class="btn btn-primary btn-block mb-5">
                                            <span>Upload  File</span>
                                            <input type="file" name="propertyData" id="propertyData" title='Click to add Files' />
                                        </div>
                                    </div><!-- /uploader -->
                                    <span id="propertyCsvErr"></span>
                                </div>
                           </div>
                           <div class="row-custom">
                                <div class="left-label">
                                    <label>Please upload a ZIP file containing images of your listings.</label>
                                    <label class="light">Please name all files correctly to ensure they are assigned to the correct listings. </label>
                                    <a href="javascipt:void(0)" id="media-download" class="download-template">Download ZIP Template</a>
                                </div>
                                <div class="right-input zip">
                                    <!-- Our markup, the important part here! -->
                                    <div id="propertyPhotos" class="propertyPhotos drag-and-drop-zone dm-uploader p-5">
                                        <input type="hidden" id="UploadedImages" class="hdnData" name="UploadedImages">
                                        <ul class="list-unstyled p-2  files file-list" id="">
                                            <li class="text-muted text-center empty">No files uploaded.</li>
                                        </ul>
                                        <h5 class="text-muted">Drag & drop ZIP file here</h5>
                                        <h5 class="light">or</h5>
                                        <div class="btn btn-primary btn-block mb-5">
                                            <span>Upload  File</span>
                                            <input type="file" name="propertyImages" id="propertyImages" title='Click to add Files' />
                                        </div>

                                    </div><!-- /uploader -->
                                    <span id="propertyImageErr"></span>
                                </div>
                           </div>
                            <div class="row-custom">
                                <div class="left-label">
                                    <label>Please upload a ZIP file containing proof of ownership or lease for each listing.</label>
                                    <label class="smal-title">Acceptable forms include:</label>
                                    <ul>
                                        <li>• Timeshare ownership</li>
                                        <li>• Property deed</li>
                                        <li>• Lease agreement</li>
                                    </ul>
                                    <a href="javascipt:void(0)" id="verification-download" class="download-template">Download ZIP Template</a>
                                </div>
                                <div class="right-input zip">
                                    <!-- Our markup, the important part here! -->
                                    <div id="verificationDocs" class="verificationDocs drag-and-drop-zone dm-uploader p-5">
                                        <input type="hidden" id="UploadedVerificationDocs" class="hdnData" name="UploadedVerificationDocs">
                                        <ul class="list-unstyled p-2 d-flex flex-column col files file-list" id="">
                                            <li class="text-muted text-center empty">No files uploaded.</li>
                                        </ul>
                                        <h5 class="text-muted">Drag & drop ZIP file here</h5>
                                        <h5 class="light">or</h5>
                                        <div class="btn btn-primary btn-block mb-5">
                                            <span>Upload  File</span>
                                            <input type="file" name="propertyVerficationDocs" id="propertyVerficationDocs" title='Click to add Files' />
                                        </div>

                                    </div><!-- /uploader -->
                                    <span id="verificationDocsErr"></span>
                                 </div>
                           </div>
                           <div class="row-custom">
                                <div class="left-label">
                                    <label>Please upload a ZIP file with photos of your government issued ID.</label>
                                    <label class="smal-title">Acceptable forms include:</label>
                                    <ul>
                                        <li>• Drivers License (front and back)</li>
                                        <li>• State ID Card</li>
                                        <li>• Military ID</li>
                                        <li>• Passport</li>
                                    </ul>
                                    <a href="javascipt:void(0)" id="govtId-download" class="download-template">Download ZIP Template</a>
                                </div>
                                <div class="right-input zip">
                                    <!-- Our markup, the important part here! -->
                                    <div id="govtIdDocs" class="govtIdDocs drag-and-drop-zone dm-uploader p-5">
                                        <input type="hidden" id="UploadedGovtId" class="hdnData" name="UploadedGovtId">
                                        <ul class="list-unstyled p-2 d-flex flex-column col files file-list" id="">
                                            <li class="text-muted text-center empty">No files uploaded.</li>
                                        </ul>
                                        <h5 class="text-muted">Drag & drop ZIP file here</h5>
                                        <h5 class="light">or</h5>
                                        <div class="btn btn-primary btn-block mb-5">
                                            <span>Upload  File</span>
                                            <input type="file" name="propertyGovtId" id="propertyGovtId" title='Click to add Files' />
                                        </div>
                                    </div><!-- /uploader -->
                                    <span id="govtIdDocsErr"></span>
                                </div>
                            </div>
                            <p class="bulk-upload-text">Have a question?  We are here for you!  Contact us by email at <a href="mailto:admin@got2go.com">admin@got2go.com</a> or by phone at (844) I-GOT2GO.</p>
                        </div>
                        {{-- <div class="outer-table">
                            <div class="row-custom" style="text-align: center;font-size: 15px;font-weight: bold;">
                                Your upload is in progress. It may take sometime to complete upload. We will notify you through email.
                            </div>
                         </div> --}}
                     </div>
                  </div>
                  <div class="listing-footer-btns">
                     <div>
                        <button type="button" id="cancel-upload" onclick="cancelClick()">Cancel</button>
                        <button type="button" id="prevBtn" onclick="nextPrev1(-1)"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</button>
                        <button type="button" class="small-width-btn" id="nextBtn">Save Changes</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="form-submited-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content" style="background-image: url('images/plane-bg.png')">
         <div class="description text-center">
            <h4>Congratulations! </h4>
            <h5>Your property has been submitted to GOT2GO for review, and we will notify you as soon as your listing is approved and live on GOT2GO!</h5>
            <a href="{{route('owner.property')}}#bulk-upload"  class="btn btn-default dashboard-btn">Go to My Listings</a>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="validation-progress-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
       <div class="modal-content" style="background-image: url('images/plane-bg.png')">
          <div class="description text-center">
             {{-- <h4>In Progress </h4> --}}
             <h5>Waiting on uploaded files to be validated. Please don’t close or exit the screen else the bulk upload process will reset.</h5>

            </div>
       </div>
    </div>
 </div>

<div class="modal fade" id="form-validation-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
       <div class="modal-content" style="background-image: url('images/plane-bg.png')">
          <div class="description text-center">
             <h5>There were no property images/Verification Documents found for this listing. Please upload a valid ZIP file with at least 6 images, with the minimum dimensions of 1024x683px</h5>
                <a href="javascript:void(0)" class="btn btn-default dashboard-btn validation-close">Close</a>
            </div>
       </div>
    </div>
 </div>

<style>
   @media (min-width: 992px) {
    .content {
        padding: 40px 0;
    }
   }
   .select2-hidden-accessible {
        border: 0 !important;
        clip: rect(0 0 0 0) !important;
        height: 1px !important;
        margin: -1px !important;
        overflow: hidden !important;
        padding: 0 !important;
        position: absolute !important;
        width: 1px !important
   }
</style>

<script src="{{URL::asset('js/bulk-upload-ui.js')}}"></script>
<script src="{{URL::asset('js/bulk-upload-config.js')}}"></script>
<script>

    // window.onbeforeunload = function(e) {
    //     e = e || window.event;
    //     e.preventDefault = true;
    //     e.cancelBubble = true;
    //     e.returnValue = 'Are you sure you want to cancel your bulk upload?';
    // }

    var unsaved = false;
    $(':input').on('change keyup', function() {
        unsaved = true;
    });

    function unloadPage(){
        if(unsaved){
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    }

    window.onbeforeunload = unloadPage;

    var bulkDownload = "{{ route('bulkDownload') }}";
    var bulkUploadSubmit = "{{ route('bulkUploadSubmit') }}";

    var pathArray = window.location.pathname.split( '/' );
    var reUpload = pathArray[4];

    var csvTemplate = "{{URL::asset('Bulk Upload Demo_50_Listings.csv')}}";
    var mediaTemplate = "{{URL::asset('Property Images.zip')}}";
    var verificationTemplate = "{{URL::asset('Verification Documentss.zip')}}";
    var govtIdTemplate = "{{URL::asset('Government-Issued ID.zip')}}";


    var reuploadCsv = "{{ $reuploadCsv }}";
    var reuploadMedia = "{{ $reuploadMedia }}";
    var reuploadVerification = "{{ $reuploadVerfication }}";
    var reuploadgovtId = "{{ $reuploadGovtid }}";

    $('#UploadedCsv').val(reuploadCsv);
    $('#UploadedImages').val(reuploadMedia);
    $('#UploadedVerificationDocs').val(reuploadVerification);
    $('#UploadedGovtId').val(reuploadgovtId);

    $( ".download-template" ).click(function() {
        window.onbeforeunload = null;

        let templateId = $(this).attr('id');

        if(templateId == 'csv-download'){
            $(this).attr('href', csvTemplate);
        } else if(templateId == 'media-download'){
            $(this).attr('href', mediaTemplate);
        } else if(templateId == 'verification-download'){
            $(this).attr('href', verificationTemplate);
        } else if(templateId == 'govtId-download'){
            $(this).attr('href', govtIdTemplate);
        }
        $(this).click();
    });


    $( ".custom-accordian .accordian-header" ).click(function() {
        $(this).parent().toggleClass('show');
    });

    function cancelClick(){
        window.location.href = "{{ route('owner.property') }}";
    }

    $('#propertyData, #propertyImages, #propertyVerficationDocs, #propertyGovtId').on('change',function(e){

        setTimeout(function() {
            var form = $("#bulk-upload-form");
            form.valid();
        },1000);

    });

    $(document).ready(function(){

        var form = jQuery("#bulk-upload-form");
        form.validate({
            // ignore:":not(:visible)",
            rules: {
                "propertyData": {
                    required: true,
                    extension: "csv",
                    accept:"csv",
                },
                "propertyImages": {
                    required: true,
                    extension: "zip",
                    accept:"zip",
                },
                "propertyVerficationDocs": {
                    required: true,
                    extension: "zip",
                    accept:"zip",
                },
                "propertyGovtId": {
                    required: true,
                    extension: "zip",
                    accept:"zip",
                },
            },
            messages: {
                "propertyData":{
                    required: "Please upload a valid file. (Only CSV allowed)",
                    accept: "Please upload a valid file. (Only CSV allowed)",
                    extension: "Please upload a valid file. (Only CSV allowed)",
                },
                "propertyImages":{
                    required: "Please upload a valid file. (Only ZIP allowed)",
                    accept: "Please upload a valid file. (Only ZIP allowed)",
                    extension: "Please upload a valid file. (Only ZIP allowed)",
                },
                "propertyVerficationDocs":{
                    required: "Please upload a valid file. (Only ZIP allowed)",
                    accept: "Please upload a valid file. (Only ZIP allowed)",
                    extension: "Please upload a valid file. (Only ZIP allowed)",
                },
                "propertyGovtId":{
                    required: "Please upload a valid file. (Only ZIP allowed)",
                    accept: "Please upload a valid file. (Only ZIP allowed)",
                    extension: "Please upload a valid file. (Only ZIP allowed)",
                },
            },
            errorPlacement: function(error, element) {
                var propertyCsvErr = $('#propertyCsvErr');
                var propertyImageErr = $('#propertyImageErr');
                var verificationDocsErr = $('#verificationDocsErr');
                var govtIdDocsErr = $('#govtIdDocsErr');

                if (element.attr('name')=='propertyData') {
                    $(propertyCsvErr).append(error)
                } else if (element.attr('name')=='propertyImages') {
                    $(propertyImageErr).append(error)
                } else if (element.attr('name')=='propertyVerficationDocs') {
                    $(verificationDocsErr).append(error)
                } else if (element.attr('name')=='propertyGovtId') {
                    $(govtIdDocsErr).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#nextBtn').on('click',function(){
            var form = jQuery("#bulk-upload-form");
            if (form.valid() === false){
                return false;
            } else if(currentTab == 1) {
                form.submit();
                return true;
            } else {
                nextPrev(1);
                return true;
            }
        })

        $('.validation-close').on('click', function(e) {
            e.preventDefault();
            $('#form-validation-modal').modal('hide');
            $('#validation-progress-modal').modal('hide');
        });

        $('#bulk-upload-form').on('submit', function (e) {
            e.preventDefault();
            if(!$(this).valid()){
                return false;
            }

            showLoader();

            $.ajax({
                type: "POST",
                url: bulkUploadSubmit,
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('owner_token')}}",
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    hideLoader();
                    if($.isEmptyObject(msg.error)){
                        if(msg.success == true){
                            window.onbeforeunload = null;
                            $('#form-validation-modal').modal('hide');
                            $('#validation-progress-modal').modal('hide');
                            $('#form-submited-modal').modal('show');

                        } else {
                            window.onbeforeunload = null;

                            Swal.fire({
                                icon: 'warning',
                                title: 'Bulk uploading cannot start until all required documents are submitted.',
                                showConfirmButton: true,
                            })
                            return false;
                        }
                    }
                }
            });
        })
    });
</script>
<script src="{{URL::asset('js/bulkupload-custom.js')}}"></script>

<!-- File item template -->
<script type="text/html" id="files-template">
    <li class="media">
        <div class="media-body mb-1">
            <div class="row">
                <div class="col-2">
                    <div class="icon">
                        <svg width="35" height="50" viewBox="0 0 35 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3 0C1.34315 0 0 1.34315 0 3V47C0 48.6569 1.34315 50 3 50H32C33.6569 50 35 48.6569 35 47V8.75L26.25 0H3ZM33.6931 11.1768C33.712 11.2014 33.7309 11.2259 33.75 11.2503H24.9463C24.2868 11.2503 23.75 10.7135 23.75 10.054V1.25C23.7748 1.26951 23.7999 1.28877 23.8249 1.30802C23.9181 1.37975 24.0114 1.45147 24.0951 1.53666L33.4621 10.9037C33.5483 10.9883 33.6207 11.0826 33.6931 11.1768Z" fill="#F5A73B"/>
                        </svg>
                        <div class="filename">CSV</div>
                    </div>
                </div>
                <div class="col-10">
                <p class="mb-2">
                    <strong>%%filename%%</strong>
                    <i class="fa fa-check" aria-hidden="true"></i>
                </p>
                <div class="progress mb-2">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                    role="progressbar"
                    style="width: 0%"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    </div>
                </div>

                <p class="controls">
                    <button href="#" class="btn btn-sm btn-primary start" role="button">Start</button>
                    <button href="#" class="btn btn-sm btn-danger cancel" role="button" >Cancel</button>
                </p>
                </div>
            </div>
        <hr class="mt-1 mb-1" />
            </div>
            <!-- <p class="controls">
            <button href="#" class="btn btn-sm btn-primary start" role="button">Start</button>
            <button href="#" class="btn btn-sm btn-danger cancel" role="button" disabled="disabled">Cancel</button>
            </p>-->
            <div class="reupload-files">
            <br>  <span class="text-muted progressStatus">Waiting</span>
            <button href="#" class="btn btn-sm btn-primary reupload" role="button">Reupload</button>
            </div>
        </div>
    </li>
</script>

@endsection

