@extends('layouts.app')
@section('content')

<div class="custom-container">
  <link rel="stylesheet" href="{{ URL::asset('css/jquery.dm-uploader.min.css') }}">
<style>
#debug {
  overflow-y: scroll !important;
  height: 180px;
}

.dm-uploader {
  border: 0.25rem dashed #A5A5C7;
  text-align: center;
}
.dm-uploader.active {
  border-color: red;

  border-style: solid;
}
.upload-start h5 {
    display: none;
}
.upload-start .btn.btn-primary.btn-block.mb-5 {
  display: none;
}
.file-list {
  display: none !important;
}
.upload-start .file-list {
  display: block !important;
}
</style>

<h1>Bulk Upload Listings</h1>
<div class="row">
   <div class="col-lg-3 left-listing-progress">
      <div class="list-white-bg">
         <ul class="nav nav-tabs custom-tab bulkupload" id="ListingTab" role="tablist">
            {{-- <li class="nav-item">
               <a class="nav-link active" id="basic-tab" data-toggle="tab" href="#basic" role="tab" aria-controls="home" aria-selected="true">Bulk Uploads</a>
            </li>
            <li class="nav-item">
               <a class="nav-link disabled" id="property-details-tab" data-toggle="tab" href="#property-details" role="tab" aria-controls="profile" aria-selected="false">Upload Files</a>
            </li> --}}
            <li class="nav-item success">
               <a class="nav-link active" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="contact" aria-selected="false">Review </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="col-lg-9">
      <div class="list-white-bg">
         <div class="">
            <div class="">

                <div class="tab-content upload-tabs" id="myTabContent">
                    <div class="tab-pane fade show active" id="basic" role="tabpanel" aria-labelledby="basic-tab">

                        <div class="outer-table">
                            <div class="row-custom" style="text-align: center;font-size: 15px;font-weight: bold;">

                                <div class="custom-accordian">
                                    <div class="accordian-header">
                                        <h5>
                                            @if($total == 0)
                                                {{$total}} Listing Uploaded
                                            @else
                                                {{($total > 1) ? $total. ' Listings' : $total. ' Listing' }} Uploaded Successfully <span class="button"><a href="{{route('owner.property')}}#pending">View Listings</a></span>
                                            @endif
                                        </h5>
                                    </div>
                                    <div class="accordian-body">
                                        <div class="table-responsive">
                                            <table class="table three-col-large">
                                                <tbody>
                                                    <tr>
                                                        <th>Listing #</th>
                                                        <th>Listing Name</th>
                                                        <th></th>
                                                    </tr>
                                                    @if(count($property)>0)
                                                        @foreach($property as $logs)
                                                        <tr>
                                                            <td>{{$logs['bulk_upload_id']}}</td>
                                                            <td>{{$logs['title']}}</td>
                                                            <td><a href="{{route('property.edit',['property' => $logs['id']] ) }}" class="review-listing">Review Listing</a></td>
                                                        </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="2" style="text-align: center">Listings are either deleted or removed</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-accordian errors">
                                    <div class="accordian-header">
                                        <h5>
                                            @if(!empty($errorCnt) && $errorCnt != 0)
                                                {{($errorCnt > 1) ? $errorCnt. ' Listings' : $errorCnt. ' Listing' }} Not Uploaded Due To Errors<span class="button"><a href="{{route('property.re-bulkupload',$listings->bulkUpload->id)}}">Reupload Files</a></span>
                                                <p class="error-detected">Errors Detected</p>
                                            @else
                                                No Errors Detected
                                            @endif
                                        </h5>
                                    </div>
                                    <div class="accordian-body">
                                        <div class="table-responsive">
                                            <table class="table three-col-small">
                                                <tbody>
                                                <tr>
                                                    <th>File Name</th>
                                                    <th>Property ID</th>
                                                    <th>Errors Detected</th>
                                                </tr>
                                                @php
                                                    $cntErr = 0;
                                                    $csvHeaderCnt = 0;
                                                    $MediaHeaderCnt = 0;
                                                    $VerificationHeaderCnt = 0;
                                                    $GovtHeaderCnt = 0;
                                                    $csvInvalidIdCnt = 0;
                                                @endphp

                                                @if(!empty($errors))
                                                    @foreach($errors as $key=>$value)
                                                        @if(!empty($key) && !empty($value['CSV']) && count($value['CSV']) != 0)
                                                            <tr>
                                                                <td style="width: 10%">
                                                                    @if($csvHeaderCnt == 0)
                                                                        {{$listings->bulkUpload->property_doc}}
                                                                    @endif
                                                                </td>
                                                                <td style="width: 10%">{{$key}}</td>
                                                                <td>
                                                                    @php $cntErr = $cntErr + count($value['CSV']); $csvHeaderCnt++; @endphp
                                                                    <span style="float: left"><strong> {{count($value['CSV'])}} {{(count($value['CSV']) == 1) ? 'Error' : 'Errors'}} Found</strong></span><br>

                                                                    @foreach($value['CSV'] as $error)
                                                                        <ul>
                                                                            <li style="text-align: left">{!!$error!!}</li>
                                                                        </ul>
                                                                    @endforeach
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif

                                                @if(!empty($mediaErrors))
                                                    @foreach($mediaErrors as $key=>$value)
                                                        <tr>
                                                            <td style="width: 10%">
                                                                @if($MediaHeaderCnt == 0)
                                                                    {{$listings->bulkUpload->property_images}}
                                                                @endif
                                                            </td>
                                                            <td style="width: 10%">{{$key}}</td>
                                                            <td>
                                                                @if(!empty($value['Media']) && count($value['Media']) != 0)
                                                                    @php $cntErr = $cntErr + count($value['Media']); $MediaHeaderCnt++; @endphp
                                                                    <span style="float: left"><strong> {{count($value['Media'])}} {{(count($value['Media']) == 1) ? 'Error' : 'Errors'}} Found</strong></span><br>
                                                                @endif

                                                                @foreach($value['Media'] as $error)
                                                                    <ul>
                                                                        <li style="text-align: left">{{$error}}</li>
                                                                    </ul>
                                                                @endforeach
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                @if(!empty($verificationErrors))
                                                    @foreach($verificationErrors as $key=>$value)
                                                        <tr>
                                                            <td style="width: 10%">
                                                                @if($VerificationHeaderCnt == 0)
                                                                    {{$listings->bulkUpload->verfication_doc}}
                                                                @endif
                                                            </td>
                                                            <td style="width: 10%">{{$key}}</td>
                                                            <td>
                                                                @if(!empty($value['Verification']) && count($value['Verification']) != 0)
                                                                    @php $cntErr = $cntErr + count($value['Verification']); $VerificationHeaderCnt++; @endphp
                                                                    <span style="float: left"><strong> {{count($value['Verification'])}} {{(count($value['Verification']) == 1) ? 'Error' : 'Errors'}} Found</strong></span><br>
                                                                @endif

                                                                @foreach($value['Verification'] as $error)
                                                                    <ul>
                                                                        <li style="text-align: left">{{$error}}</li>
                                                                    </ul>
                                                                @endforeach
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                @if(!empty($govtErrors))
                                                    @foreach($govtErrors as $key=>$value)
                                                        <tr>
                                                            <td style="width: 10%">
                                                                @if($GovtHeaderCnt == 0)
                                                                    {{$listings->bulkUpload->govtid_doc}}
                                                                @endif
                                                            </td>
                                                            <td style="width: 10%">{{$key}}</td>
                                                            <td>
                                                                @if(!empty($value) && count($value) != 0)
                                                                    @php $cntErr = $cntErr + count($value); $GovtHeaderCnt++; @endphp
                                                                    <span style="float: left"><strong> {{count($value)}} {{(count($value) == 1) ? 'Error' : 'Errors'}} Found</strong></span><br>
                                                                @endif

                                                                @foreach($value as $error)
                                                                    <ul>
                                                                        <li style="text-align: left">{!!$error!!}</li>
                                                                    </ul>
                                                                @endforeach
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                @if(!empty($errors['InvalidPropertyID']))
                                                    <tr>
                                                        <td>
                                                            @if($csvInvalidIdCnt == 0)
                                                                {{$listings->bulkUpload->property_doc}}
                                                            @endif
                                                        </td>
                                                        <td> </td>
                                                        <td>
                                                            @php
                                                                $csvInvalidIdCnt++;
                                                                $cntErr = $cntErr + count($errors['InvalidPropertyID']);
                                                                $errcnt = (count($errors['InvalidPropertyID']) == 1) ? 'Error' : 'Errors';
                                                                $errheader = '<span style="float: left"><strong> '.count($errors['InvalidPropertyID']) . ' ' . $errcnt.' Found</strong></span><br>';
                                                            @endphp

                                                            {!!$errheader!!}

                                                            @foreach($errors['InvalidPropertyID'] as $key=>$value)
                                                                <ul style="margin-bottom: 12px;">
                                                                    <li style="text-align: left;margin-top: 10px;">Property ID: {{$key}} There was no property found with this Property ID in uploaded CSV, no property images were uploaded for this listing.</li>
                                                                </ul>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
         </div>
      </div>
   </div>
</div>

<script src="{{URL::asset('js/bulkupload-custom.js')}}"></script>

<script>
    var cntErrCount = '{{$cntErr}}';
    $('.error-detected').html(cntErrCount + ' Errors Detected');
    $( ".custom-accordian .accordian-header" ).click(function() {
        $(this).parent().toggleClass('show');
    });
</script>

@endsection

