@extends('layouts.app')

@section('content')

@include('layouts.listSessionmsg')
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <h2 class="f-w-700 text-uppercase text-dark d-block w-90 align-items-center">
                  View Listing
               </h2>
               <a href="{{route('property.create')}}" class="f-w-700 btn btn-danger">Add New Listing</a>
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid property-edit" id="kt_content">
            <div class="row" >
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="property-images">
                           <div class="row">
                              <div class="col-md-12">
                                <img src="{{URL::asset('uploads/property/'.$property->cover_photo)}}" alt="{{$property->title}}" />
                              </div>
                              @foreach($property->propertyimages as $property_image)
                              <div class="col-6 col-md-3">
                                 <img src="{{URL::asset('uploads/property/'.$property_image->photo)}}" alt="{{$property_image->photo}}" />
                              </div>
                             @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-desc">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h2 class="f-w-700 mb-5">{{$property->title}}</h2>
                        <h6>{{$property->description}}</h6>
                        <label class="mark-place"><i class="fas fa-map-marker"></i>{{$property->location_detail}}</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h5 class="f-w-700 mb-5">Unit Amenities</h5>
                        <div class="amenities-list-dsply">
                           <div class="row">
                               @foreach($property->propertyamenities as $amenity)
                              <div class="col-6 col-sm-6">
                                 <h6>{{$amenity->amenity_name}}</h6>
                              </div>
                             @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-spacing-h6">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="check-in-out-time border-bottom mb-5">
                           <div class="row">
                              <div class="col-md-6">
                                 <h6><strong>Check-In:</strong> {{date('h:m A',strtotime($property->check_in))}}</h6>
                              </div>
                              <div class="col-md-6">
                                 <h6><strong>Check-Out:</strong> {{date('h:m A',strtotime($property->check_out))}}</h6>
                              </div>
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Common Rooms:</strong>{{$property->common_room}}</h6>
                           </div>
                           <div class="col-md-8">
                              <h6>Sofa:</strong> {{$property->sofa}}</h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Property type:</strong> {{$property->propertytype->property_type}}</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Room type:</strong> {{$property->guest_will_have}}</h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <h6><strong>Accommodates:</strong> {{$property->propertybedrooms->sum->guest_per_room}} Guests</h6>
                           </div>

                        </div>
{{--                        <div class="row"> div class="col-md-6">--}}
{{--                              <h6><strong>Price:</strong> {{$property->price}} (Per Night)</h6>--}}
{{--                           </div>--}}
{{--                        </div>--}}
                        @foreach($property->propertybedrooms as $key=>$bedroom)
                        <h2 class="mb-5 border-bottom"><strong>Bedroom {{$key+1}}</strong></h2>
                        <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Bathrooms: </strong>  {{$bedroom->bathroom}}</h6>
                           </div>
                           <div class="col-md-3">
                              <h6><strong>Beds: </strong> {{$bedroom->bed}}</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Bed type: </strong>{{ Config::get('property.bed_types.'.$bedroom->bed_type)}} </h6>
                           </div>

                        </div>
                        @endforeach

                     </div>
                  </div>
               </div>
            </div>
            <div class="delete-listing mb-3">
               <a href="javascript:destroy({{$property->id}})" class="btn btn-link-danger font-weight-bold" style="font-weight: 700;">Delete Listing</a>
            </div>
            <div class="posted-updated-date">
               <h6><span class="mr-5">Posted date: {{date('m/d/Y h:m A',strtotime($property->created_at))}}</span><span>Updated date: {{date('m/d/Y h:m A',strtotime($property->updated_at))}}</span></h6>
            </div>
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<script>
var propertydestroy = '{{ route("property.destroy", ":id") }}';
</script>
<script src="{{ URL::asset('js/owner/timeshare/view.js') }}"></script>
@endsection
