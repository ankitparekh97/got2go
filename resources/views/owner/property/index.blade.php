@extends('layouts.app')

@section('content')
    <div class="">

    <div class="">
        <div class="custom-container">

            <div class="text-right add-new-listing">
            <div class="custom-dropdown-upload">
                <div class="add-new-listing">
                    <span><img class="lazy-load" data-src="{{URL::asset('media/images/dropdown-plus-icon.png')}}" alt="icon"></span>
                    ADD LISTING(S)
                </div>
                <div class="dropdown-list">
                   <a href="{{route('property.bulkupload')}}">BULK UPLOAD LISTINGS</a>
                   <a href="{{route('property.create')}}">ADD INDIVIDUAL LISTING</a>
                </div>
            </div>
            </div>

            <ul class="nav nav-tabs listing-tabs-current" id="listingtab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="current-tab" data-toggle="tab" href="#current" role="tab" aria-controls="home" aria-selected="true">Current Listings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="profile" aria-selected="false">Pending Listings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="hot-deals-tab" data-toggle="tab" href="#hot-deals" role="tab" aria-controls="contact" aria-selected="false">Hot Deals</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="bulk-upload-tab" data-toggle="tab" href="#bulk-upload" role="tab" aria-controls="contact" aria-selected="false">Bulk Uploads</a>
                </li>
            </ul>

            <div class="table-search-outer-cst">
                <input type="text" id="currentSearch" class="search-listings" placeholder="Search Listings">
                <input type="text" id="pendingSearch" class="search-listings" placeholder="Search Listings">
            </div>

            <div class="tab-content" id="listingtabcontent">
                <div class="tab-pane fade show active" id="current" role="tabpanel" aria-labelledby="current-tab">
                    <div class="table-responsive table-no-header table-only-search">
                        <table class="table currentlistings" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                    <div class="table-responsive table-no-header table-only-search" >
                        <table class="table pendinglistings" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="hot-deals" role="tabpanel" aria-labelledby="hot-deals-tab">...</div>

                <div class="tab-pane fade" id="bulk-upload" role="tabpanel" aria-labelledby="hot-deals-tab">
                    <div class="table-responsive dark-custom-table">
                        <table class="table bulkupload">
                            <thead>
                                <tr>
                                    <th>Upload #</th>
                                    <th>Listings</th>
                                    <th>Errors</th>
                                    <th>Upload Started</th>
                                    <th>Upload Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


     <!-- Hot Deal Modal -->
     <div class="modal fade promote-as-hot-deal" id="promote-as-hot-deal" tabindex="-1" role="dialog" aria-labelledby="promote-as-hot-deal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Promote Listing as a Hot Deal</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <ul class="tabs-upper">
                    <li><a href="javascript:void(0)">Select Tier & Price</a></li>
                    <li><a href="javascript:void(0)">Confirm Payment</a></li>
                </ul>

                <div class="modal-body">
                    <form id="regForm2" action="#">
                        <input type="hidden" id="PropertyId"></span>
                        <div class="tab">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="container-div">
                                        <input type="radio" name="hot_ratings">
                                        <span class="checkmark"></span>
                                        <div class="images-desc">
                                            <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                                <defs>
                                                <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#F80E41"/>
                                                <stop offset="1" stop-color="#FDE332"/>
                                                </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                                <defs>
                                                <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#F80E41"/>
                                                <stop offset="1" stop-color="#FDE332"/>
                                                </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                                <defs>
                                                <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#F80E41"/>
                                                <stop offset="1" stop-color="#FDE332"/>
                                                </linearGradient>
                                                </defs>
                                            </svg>
                                            <h5>Super Subscription</h5>
                                            <h5>$25.00/month</h5>
                                            <h6>Your listing will be featured on our homepage for 24 hours.</h6>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="container-div">
                                        <input type="radio" name="hot_ratings">
                                        <span class="checkmark"></span>
                                        <div class="images-desc">
                                            <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                                <defs>
                                                <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#F80E41"/>
                                                <stop offset="1" stop-color="#FDE332"/>
                                                </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                                <defs>
                                                <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#F80E41"/>
                                                <stop offset="1" stop-color="#FDE332"/>
                                                </linearGradient>
                                                </defs>
                                            </svg>
                                            <h5>Super Subscription</h5>
                                            <h5>$15.00/month</h5>
                                            <h6>Your listing will be featured on our Hot Deals page for 12 hours.</h6>
                                        </div>
                                    </label>
                                </div>
                                </label>
                            </div>
                        </div>
                        <p class="nightly-price">Hot Deal Nightly Price:<span class="input-box"><input type="number" name="nightly_price" placeholder="200"></span><span class="light-text">(We recommend at least a 10% discount.)</span></p>
                        </div>

                        <div class="tab">
                            <div class="payment-mode-outer">
                            <div class="payment-mode-select">
                                <h4>Payment Method</h4>
                                <div class="user-card-info">
                                <div class="row">
                                    <div class="col-md-6">
                                    <h5>JP Morgan Chase Debit Card</h5>
                                    <h5 class="red">Change Payment Method</h5>
                                    </div>
                                    <div class="col-md-6">
                                    <h5 class="number">• • • • • • • • • • • • • 9884</h5>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="payment-mode-add">
                                <div class="row">

                                <div class="col-md-6">
                                    <div class="form-row-custom">
                                    <label>Cardholder Name</label>
                                    <input type="text" name="card_holder">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-row-custom">
                                    <label>Card Name</label>
                                    <input type="text" name="card_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-row-custom">
                                    <label>Card Number</label>
                                    <input type="number" name="card_number">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-row-custom">
                                    <label>Exp. Date</label>
                                    <input type="number" name="card_exp">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-row-custom">
                                    <label>CVV</label>
                                    <input type="password" name="card_cvv">
                                    </div>
                                </div>

                                </div>
                            </div>
                            <p class="card-desc">This payment method will be charged a monthly fee of $25.00 beginning today once you subscribe this listing as a Hot Deal.  You can pause of cancel this subscription at any time.  </p>
                            </div>
                        </div>

                        <div class="tab">
                            <div class="payment-mode-outer last-step">
                                <div class="hot-dela-images">
                                    <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                        <defs>
                                        <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F80E41"/>
                                        <stop offset="1" stop-color="#FDE332"/>
                                        </linearGradient>
                                        </defs>
                                    </svg>
                                    <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                        <defs>
                                        <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F80E41"/>
                                        <stop offset="1" stop-color="#FDE332"/>
                                        </linearGradient>
                                        </defs>
                                    </svg>
                                    <svg width="24" height="31" viewBox="0 0 24 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M11.959 20.176C9.82567 22.306 8.61967 25.8267 8.341 30.738C3.47738 29.6981 0.00137192 25.4015 0 20.428C0 12.295 6.713 10.528 5.935 0C7.399 0.023 13.316 3.312 14.42 11.118C15.576 10.007 16.167 6.33 15.944 4.826C21.195 8.683 23.916 13.636 23.916 19.161C23.916 24.63 20.804 29.268 15.574 30.713C15.2967 25.8143 14.0917 22.302 11.959 20.176Z" fill="url(#paint0_linear)"/>
                                        <defs>
                                        <linearGradient id="paint0_linear" x1="0" y1="0" x2="0" y2="30.738" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F80E41"/>
                                        <stop offset="1" stop-color="#FDE332"/>
                                        </linearGradient>
                                        </defs>
                                    </svg>
                                </div>
                            <h5>Hot Deal Active!</h5>
                            <p>Your listing “Studio Apartment in the Heart of Manhattan!” is now being promoted on out website as a Hot Deal!  Your payment method will be charged $25.00 on the 13th of each month until your Hot Deal subscription is paused or cancelled.</p>
                            </div>
                        </div>
                        <div class="next-btn text-center">
                        <button type="button" class="nextbtn" id="nextBtn2" onclick="nextPrev2(1)">Next</button><br>
                        <button type="button" class="close" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
     </div>

    <!-- Delete Modal -->
    <div class="modal fade delete-listing-modal cmn-popup " id="delete-listing" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 >Are you sure?</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <p>Are you sure you want to delete this listing from your account?<br>You will not be able to undo this.</p>
            <div class="text-center delete-btns">
                <button type="button" class="btn btn-default red-btn-cmn deleteProperty" id="deleteProperty">Delete Listing</button><br>
                <button type="button" class="close-btn-cmn" data-dismiss="modal">Cancel</button>
            </div>
            </div>
        </div>
        </div>

    </div>


    <!-- Publish Modal -->
    <div class="modal fade publish-modal cmn-popup " id="publish" tabindex="-1" role="dialog" aria-labelledby="publish" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <p>Are you sure you want to delete this listing from your account?<br>You will not be able to undo this.</p> --}}
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn change_publish" id="change_publish">Yes</button><br>
                        <button type="button" class="close-btn-cmn" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footerJs')
    @parent
<script>
var propertylist = "{{ route('property.list') }}";
var propertypublish = "{{ route('property.publishProperty') }}";
var propertydestroy = "{{ route('property.destroy') }}";
var bulkUploadReview = "{{ route('bulkUploadListing') }}";
var processingImg = "{{URL::asset('media/images/loader.gif')}}";
var pendingListings = "{{ route('property.pendingListings') }}";
var currentListings = "{{ route('property.currentListings') }}";

$(document).ready(function() {

    currentlistings();
    propertyListings();

    // Javascript to enable link to tab
    var hash = document.location.hash;
    if (hash) {
        $('.nav-tabs a[href="'+hash+'"]').tab('show');
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        window.location.hash = e.target.hash;
    });

    if(hash == '#current' || hash==''){
        $('#currentSearch').show();
        $('#pendingSearch').hide();
    } else if(hash == '#pending'){
        $('#currentSearch').hide();
        $('#pendingSearch').show();
    } else {
         $('#pendingSearch').hide();
         $('#currentSearch').hide();
    }

    jQuery('label.container-div input').on('click', function() {
    if(jQuery('label.container-div input').is(':checked')) {
        jQuery("label.container-div").removeClass('active');
        jQuery(this).parent().addClass('active');
    }
    });
    jQuery('.user-card-info h5.red').on('click', function() {
    jQuery('.payment-mode-select').hide();
    jQuery('.payment-mode-add').show();
    });

    $("#delete-listing").on("click",".deleteProperty", function(){
        const id = $(this).attr('data-id');
        destroy(id);
    });

    $("#publish").on("click",".change_publish", function(){
        const id = $(this).attr('data-id');
        publish(id);
    });

});
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab2(currentTab); // Display the current tab

function showTab2(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  // if (n == 0) {
  //   document.getElementById("prevBtn2").style.display = "none";
  // } else {
  //   document.getElementById("prevBtn2").style.display = "inline";
  // }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn2").innerHTML = "Back to Listings";
  } else {
    document.getElementById("nextBtn2").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  //fixStepIndicator2(n)
}

function nextPrev2(n) {
  var form2 = jQuery("#regForm2");
  form2.validate({
    rules: {
      hot_ratings: {
        required: true,
      },
      nightly_price: {
        required: true,
      },
      card_holder: {
        required: true,
      },
      card_name: {
        required: true,
      },
      card_number: {
        required: true,
      },
      card_exp: {
        required: true,
      },
      card_cvv: {
        required: true,
      },
    },
    messages: {
      hot_ratings: {
        required: "This field is required!",
      },
      nightly_price: {
        required: "This field is required!",
      },
      card_holder: {
        required: "This field is required!",
      },
      card_name: {
        required: "This field is required!",
      },
      card_number: {
        required: "This field is required!",
      },
      card_exp: {
        required: "This field is required!",
      },
      card_cvv: {
        required: "This field is required!",
      }
    }
  });

  if (form2.valid() === true){

    jQuery('ul.tabs-upper').addClass('counter'+currentTab);
      // This function will figure out which tab to display
      var x = document.getElementsByClassName("tab");
      // Exit the function if any field in the current tab is invalid:
      //if (n == 1 && !validateForm2()) return false;
      // Hide the current tab:
      x[currentTab].style.display = "none";
      // Increase or decrease the current tab by 1:
      currentTab = currentTab + n;
      // if you have reached the end of the form...
      if (currentTab >= x.length) {
        // ... the form gets submitted:
        document.getElementById("regForm2").submit();
        return false;
      }
  }
      // Otherwise, display the correct tab:
    showTab2(currentTab);
}

function validateForm2() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator2(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

function showModal(id)
{
   $(".deleteProperty").attr('data-id',id)
   $("#delete-listing").modal('show');
}

function showhotdealModal(id)
{
   $("#promote-as-hot-deal .modal-body").find('#PropertyId').val('');
   $("#promote-as-hot-deal .modal-body").find('#PropertyId').val(id)
   $("#promote-as-hot-deal").modal();
}

function showPublishModal(id,publish)
{
    let text = '';
    if(publish == '1'){
        text = 'unpublish';
    } else if(publish == '0'){
        text = 'publish';
    }

   $(".change_publish").attr('data-id',id);
   $(".modal-header h4").empty().html('Are you sure you want to '+text+'?');
   $("#publish").modal('show');
}
</script>
<script src="{{ URL::asset('js/owner/property/index.js') }}"></script>
<script src="{{ URL::asset('js/owner/property/bulkupload-review.js') }}"></script>
@endsection



