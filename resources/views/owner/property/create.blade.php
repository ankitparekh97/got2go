@extends('layouts.app')

@section('content')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initAutocomplete&libraries=places&v=weekly" defer></script>
<script src="{{ URL::asset('js/googlemap.js') }}"></script>
<script src="{{ URL::asset('js/popper.min.js') }}"></script>
<div class="custom-container">

    <h1>Add New Listing</h1>

    <div class="row">
        <div class="col-lg-3 left-listing-progress">
            <div class="list-white-bg">
                <ul class="nav nav-tabs custom-tab listing-tab-add" id="ListingTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="basic-tab" href="javascript:showTab(0)" role="tab" aria-controls="home" aria-selected="true">Basics</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" id="property-details-tab" href="javascript:showTab(4)" role="tab" aria-controls="profile" aria-selected="false">Property Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" id="media-tab" href="javascript:showTab(9)" role="tab" aria-controls="contact" aria-selected="false">Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" id="verification-tab" href="javascript:showTab(10)" role="tab" aria-controls="contact" aria-selected="false">Verification</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" id="review-tab" href="javascript:showTab(12)" role="tab" aria-controls="contact" aria-selected="false">Review</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="list-white-bg">
                <div class="center-container">
                    <div class="">
                        <form id="owner-panel-form">
                            <input type="hidden" id="last_tab_id" name="last_tab_id">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="basic" role="tabpanel" aria-labelledby="basic-tab">
                                <input type="hidden" class="small" name="property_id" id="hdn_property_id">

                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Name your listing</label>
                                            </div>
                                            <div class="right-input">
                                                <textarea class="small name_your_listing" id="name_your_listing" maxlength="50" name="name_your_listing" placeholder="Something catchy, that will capture the reader’s attention! ( 50 character limit )"></textarea>
                                                <p class="charlength">(50 characters remaining)</p>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Describe your listing</label>
                                            </div>
                                            <div class="right-input">
                                                <textarea name="describe_listing" id="describe_listing" class="large"   placeholder="What should we know about your space?  What makes it special? Are any popular attractions nearby?"></textarea>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>About the space</label>
                                            </div>
                                            <div class="right-input">
                                                <textarea class="small"  id="about_space" name="about_space"   placeholder="Anything else guests should know about the space?  Is there a backyard, full kitchen, garage, storage, etc?"></textarea>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Listing as a company?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">Yes, I am posting as an organization.
                                                    <input type="radio" checked name="listing" value="0">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">No, I am an individual.
                                                    <input type="radio" name="listing" value="1">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <span id="listingErrorLabel"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Where is your listing?</label>
                                            </div>
                                            <div class="right-input">
                                                <input type="hidden" class="small" name="where_is_listing_clone" id="where_is_listing_clone">
                                                <input type="hidden" class="small" name="city" id="city">
                                                <input type="hidden" class="small" name="state" id="state">
                                                <input type="hidden" class="small" name="country" id="country">
                                                <input type="hidden" class="small" name="zipcode" id="zipcode">
                                                <input type="text" class="small" name="where_is_listing" id="where_is_listing" placeholder="Begin typing address here.">
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Is this the correct location?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="map iframe-map">
                                                    <div id="map" style="height:300px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What category does your property fall under?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">
                                                    <input type="radio" value="vacation_rental" class="type_of_property" name="type_of_property">
                                                    <strong class="d-block">
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="23.999" height="23.999" viewBox="0 0 23.999 23.999">
                                                            <g id="Group_477" data-name="Group 477" transform="translate(-493 -1303)">
                                                                <g id="Vacation_Club" data-name="Vacation Club" transform="translate(493 1303)">
                                                                <path id="Shape" d="M24,24H0V22.5H1.5V7.5H0v-3L12,0,24,4.5v3H22.5v15H24V24ZM13.5,15v7.5H18V15ZM6,15v3.3h4.5V15Zm7.5-7.5V12H18V7.5ZM6,7.5V12h4.5V7.5Z" transform="translate(0 0)" fill="#3d3d3d"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>Vacation Club</strong>Resort, hotel, villa, holiday cottage, condo, lodge

                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">
                                                    <input type="radio" value="property" class="type_of_property" name="type_of_property" checked>
                                                    <strong class="d-block">
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="24.999" viewBox="0 0 25 24.999">
                                                            <g id="Group_477" data-name="Group 477" transform="translate(-477 -1399)">
                                                                <g id="Group_451" data-name="Group 451" transform="translate(-15)">
                                                                <g id="Private_Residence" data-name="Private Residence" transform="translate(492 1399)">
                                                                    <path id="Path" d="M137.6,274.3l-11.562-11.083a.781.781,0,0,0-1.081,0l-11.574,11.095a1.32,1.32,0,0,0-.385.927,1.3,1.3,0,0,0,1.3,1.3h1.823v9.9A1.563,1.563,0,0,0,117.687,288h4.427a.782.782,0,0,0,.781-.781v-6.771a.261.261,0,0,1,.26-.26h4.688a.262.262,0,0,1,.26.26v6.771a.782.782,0,0,0,.781.781h4.427a1.563,1.563,0,0,0,1.562-1.562v-9.9H136.7a1.3,1.3,0,0,0,1.3-1.3A1.33,1.33,0,0,0,137.6,274.3Z" transform="translate(-113 -263)" fill="#3d3d3d"/>
                                                                </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>Private Residence</strong>House, apartment, studio, cabin, bungalow, estate
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row-custom" id="what-kind-of-residence">
                                            <div class="left-label">
                                                <label>What kind of private residence is this?</label>
                                            </div>
                                            <div class="right-input ">
                                                <select name="private_residence_this" id="property_type">
                                                    <option value="">Select One</option>
                                                    <option value="test">Condo / Apartment</option>
                                                    <option value="test">House</option>
                                                    <option value="test">Cabin</option>
                                                    <option value="test">Bungalow</option>
                                                    <option value="test">Villa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row-custom" id="what-kind-of-vacation">
                                            <div class="left-label">
                                                <label>What kind of vacation club is this?</label>
                                            </div>
                                            <div class="right-input">
                                                <select name="vacation_club_is_this" id="vacation_club">
                                                    <option>Select One</option>
                                                    <option value="test">Resort</option>
                                                    <option value="test">Hotel</option>
                                                    <option value="test">Villa</option>
                                                    <option value="test">Condo</option>
                                                    <option value="test">Lodge</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row-custom" id="what-kind-of-cabin">
                                            <div class="left-label">
                                                <label id="lblsubtype"></label>
                                            </div>
                                            <div class="right-input">
                                                <select name="what_kind_cabin" class="what_kind_cabin" id="subType">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="row-custom" id="what-is-resort-name">
                                            <div class="left-label">
                                                <label id="lblclubtype">What’s the name of the vacation club?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="input-button red">
                                                    <input type="text" id="txt-resort" name="txt_resort">
                                                    <a href="javascript:void(0)" id="save-new-resort">Save New Resort</a>
                                                </div>
                                                <p id="empty-resorts" style="display: none;">It looks like we don’t have this resort in our directory. Please make sure the resort name is entered correctly and save!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>How many guests are allowed?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="plus-minus-sec">
                                                    <span class="guest-minus minus transition active">-</span>
                                                    <input id="how-many-guests-allowed" name="how-many-guests-allowed" type="text" readonly value="1" class="cmn-add" />
                                                    <span class="guest-plus plus transition">+</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What will guests have access to?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">
                                                    <input type="radio" checked name="guest_have" value="0">
                                                    <strong>
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="29.964" viewBox="0 0 30 29.964">
                                                            <g id="Group_478" data-name="Group 478" transform="translate(-16 -24)">
                                                                <path id="Path_2470" data-name="Path 2470" d="M191.882,296.082a10.534,10.534,0,0,1,5.464,2.075l-5.464,5.464v-7.539Zm-7.229,2.076a10.533,10.533,0,0,1,5.464-2.076v7.539l-5.464-5.464Zm-4.1,6.712a10.559,10.559,0,0,1,2.774-5.537l5.537,5.537Zm18.11-5.538a10.557,10.557,0,0,1,2.774,5.538H193.13l5.538-5.538Zm-8.55,7.3h0v17.207a.44.44,0,0,0,.445.439h.874a.443.443,0,0,0,.445-.439V306.635h9.706v17.646h3.97a.445.445,0,0,1,.441.445v.874a.441.441,0,0,1-.441.445H176.441A.445.445,0,0,1,176,325.6v-.874a.441.441,0,0,1,.441-.445h3.97V306.635Zm-3.084,7.94a.444.444,0,0,0-.445.44v2.65a.44.44,0,0,0,.445.44h.874a.445.445,0,0,0,.445-.44v-2.65a.44.44,0,0,0-.445-.44Zm7.059,0a.444.444,0,0,0-.445.44v2.65a.44.44,0,0,0,.445.44h.874a.444.444,0,0,0,.445-.44v-2.65a.44.44,0,0,0-.445-.44Z" transform="translate(-160 -272.082)" fill="#3d3d3d" fill-rule="evenodd"/>
                                                            </g>
                                                        </svg>
                                                    </span>Entire Place</strong><br>Guests have the whole place to themselves. This usually includes a bedroom, a bathroom, and a kitchen.
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">
                                                    <input type="radio" name="guest_have"  value="1">
                                                    <strong>
                                                    <span>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="18.26" height="30.646" viewBox="0 0 18.26 30.646">
                                                            <g id="Group_479" data-name="Group 479" transform="translate(-497.618 -1413.553)">
                                                                <g id="Private_Room" data-name="Private Room" transform="translate(497.618 1413.553)">
                                                                <g id="Group_454" data-name="Group 454" transform="translate(0 0)">
                                                                    <path id="Private_Room-2" data-name="Private Room" d="M197.853,306.635l.025,30.646H181V306.635Zm-5.356,13.79a.772.772,0,0,0-.773.764v4.6a.765.765,0,0,0,.773.764h1.518a.772.772,0,0,0,.773-.764v-4.6a.765.765,0,0,0-.773-.764Z" transform="translate(-179.618 -306.636)" fill="#3d3d3d" fill-rule="evenodd"/>
                                                                </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>Private Room</strong><br>Guests have their own private room for sleeping.  Other areas are shared.
                                                    <span class="checkmark"></span>
                                                </label>
                                                <span id="guest_haveErrorLabel"></span>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Is this a dedicated guest space?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">Yes, it is set up for guests only.
                                                    <input type="radio" checked name="is_guest_only" value="0">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">No, there are some of my belongings.
                                                    <input type="radio" name="is_guest_only" value="1">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <span id="is_guest_onlyErrorLabel"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>How many bedrooms are there?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="plus-minus-sec">
                                                    <span class="guest-minus minus transition">-</span>
                                                    <input id="how-many-bedrooms-there" name="how-many-bedrooms-there" type="text" readonly value="0" class="cmn-add" />
                                                    <span class="guest-plus plus transition">+</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-row">
                                            <span id="totalBedrErr"></span>
                                            <div class="row cloneBedroom">
                                                <div class="col-md-6 col-xl-5" id="bedroomTypes" style="display:none;">
                                                    <div class="bedroom-outer-container">
                                                        <span class="bedTypeError"></span>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <label class="lblBedroom">Bedroom 1</label>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                <a href="javascript:void(0)" class="delBedroom"><img class="lazy-load" data-src="{{URL::asset('media/images/recyclebin.png')}}" alt="recyclebin" /></a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double-bed.png')}}" alt="bed" /></span>King</p>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                <div class="plus-minus-sec">
                                                                    <span class="guest-minus minus transition">-</span>
                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[King][]" value="0" class="cmn-add King" />
                                                                    <span class="guest-plus plus transition">+</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double-bed.png')}}" alt="bed" /></span>Queen</p>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                <div class="plus-minus-sec">
                                                                    <span class="guest-minus minus transition">-</span>
                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[Queen][]" value="0" class="cmn-add Queen" />
                                                                    <span class="guest-plus plus transition">+</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double.png')}}" alt="bed" /></span>Double</p>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                <div class="plus-minus-sec">
                                                                    <span class="guest-minus minus transition">-</span>
                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[Double][]" value="0" class="cmn-add Double" />
                                                                    <span class="guest-plus plus transition">+</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/single.png')}}" alt="bed" /></span>Single</p>
                                                            </div>
                                                            <div class="col-6 text-right">
                                                                <div class="plus-minus-sec">
                                                                    <span class="guest-minus minus transition">-</span>
                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[Single][]" value="0" class="cmn-add Single" />
                                                                    <span class="guest-plus plus transition">+</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="select-custom-dropdown">
                                                                    <a href="javascript:void(0);" class="click-btn">Add another type of bed</a>
                                                                    <div class="select-dropdown-inner">
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double.png')}}" alt="bed" /></span>Bunk Bed</p>
                                                                            </div>
                                                                            <div class="col-6 text-right">
                                                                                <div class="plus-minus-sec">
                                                                                    <span class="guest-minus minus transition">-</span>
                                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[BunkBed][]" value="0" class="cmn-add BunkBed" />
                                                                                    <span class="guest-plus plus transition">+</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double.png')}}" alt="bed" /></span>Futon</p>
                                                                            </div>
                                                                            <div class="col-6 text-right">
                                                                                <div class="plus-minus-sec">
                                                                                    <span class="guest-minus minus transition">-</span>
                                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[Futon][]" value="0" class="cmn-add Futon" />
                                                                                    <span class="guest-plus plus transition">+</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double.png')}}" alt="bed" /></span>Couch</p>
                                                                            </div>
                                                                            <div class="col-6 text-right">
                                                                                <div class="plus-minus-sec">
                                                                                    <span class="guest-minus minus transition">-</span>
                                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[Couch][]" value="0" class="cmn-add Couch" />
                                                                                    <span class="guest-plus plus transition">+</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-6">
                                                                                <p><span><img class="lazy-load" data-src="{{URL::asset('media/images/double.png')}}" alt="bed" /></span>Air Mattress</p>
                                                                            </div>
                                                                            <div class="col-6 text-right">
                                                                                <div class="plus-minus-sec">
                                                                                    <span class="guest-minus minus transition">-</span>
                                                                                    <input id="" type="text" readonly min="0" max="10" name="bedType[AirMattress][]" value="0" class="cmn-add AirMattress" />
                                                                                    <span class="guest-plus plus transition">+</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>How many other beds (not included above) are there?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="plus-minus-sec">
                                                    <span class="guest-minus minus transition">-</span>
                                                    <input id="how_many_beds_there" type="text" readonly name="how_many_beds_there" value="0" class="cmn-add" />
                                                    <span class="guest-plus plus transition">+</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label test">
                                                <label>How many bathrooms are available?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="plus-minus-sec">
                                                    <span class="guest-minus minus transition active" style="border-color: rgb(248, 14, 65);color: rgb(248, 14, 65);">-</span>
                                                    <input id="how_many_bathrooms_available" name="how_many_bathrooms_available" type="text" readonly value="1" class="cmn-add" />
                                                    <span class="guest-plus plus transition">+</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What amenities are available at this property?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="row">
                                                    @if(count($amenities)>0)
                                                    @foreach($amenities as $item)
                                                    @php
                                                    $amenity_css = str_replace(' ', '-', $item->amenity_name);
                                                    $amenity_css = str_replace('/', '-', $amenity_css);
                                                    $amenity_css = strtolower($amenity_css);
                                                    @endphp
                                                    <div class="col-md-6">
                                                        <label class="checkbox-btn">
                                                            <input type="checkbox" value="{{$item->id}}" name="amenity_id[]">
                                                            <div class="bg-@php echo  $amenity_css @endphp amenity"></div>

                                                            <span class="amenity_name">{{$item->amenity_name}}</span>
                                                            <span class="checkmark-check"></span>
                                                        </label>

                                                    </div>
                                                    @endforeach
                                                    @endif
                                                    <label id="AmenityErrorLabel"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What’s the nightly price?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="row align-items-center">
                                                    <div class="col-md-3 dollar">
                                                        <input type="number" value="" name="price" id="price" />
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <button type="button" class="red-small-btn">Calculate Rental Price</button>
                                                    </div>
                                                    <div class="col-md-3 dollar">
                                                        <input type="text" value="" readonly name="calculatedPrice" id="calculatedPrice" />
                                                    </div>
                                                </div>
                                                <label id="PrieceErrorLabel"></label>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>How much will you charge for cleaning?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="row align-items-center">
                                                    <div class="col-md-3 dollar">
                                                        <input type="number" value="" name="cleaning_fee" id="cleaning_fee" />
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="date-error-label availibility-message price"><label>GOT2GO will charge an additional 10% to non-member travelers, which will go in your pocket! </label></div>
                                                    </div>
                                                </div>
                                                <label id="cleaningFeeErrorLabel"></label>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Is the price negotiable?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">Yes, clients can make offers on my property.
                                                    <input type="radio" name="is_negotiable" value="1">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">No, the price is final.
                                                    <input type="radio" name="is_negotiable" value="0" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row-custom offerPrice">
                                            <div class="left-label">
                                                <label>What is the minimum nightly rate you'll accept?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="row align-items-center">
                                                    <div class="col-md-3 dollar">
                                                        <input type="number" value="" name="offer_price" id="offer_price" />
                                                    </div>
                                                </div>
                                                <label id="offerPriceLabel"></label>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Is instant booking allowed?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">Yes, clients can instantly book this property.
                                                    <input type="radio" name="is_instant_booking" value="1" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">No, I need to approve the client request before booking is finalized.
                                                    <input type="radio" name="is_instant_booking" value="0">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row-custom" style="display: none;">
                                            <div class="left-label">
                                                <label>Is the cleaning fee included in the nightly price?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn">Yes, the cleaning fee is included in the nightly price.
                                                    <input type="radio" name="is_partial_bookingdummy" value="1" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn">No, the cleaning fee is not included in the nighly price.
                                                    <input type="radio" name="is_partial_bookingdummy" value="0">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What time can guests check-in?</label>
                                                <div class="date-error-label check-time-error"></div>
                                            </div>
                                            <div class="right-input">
                                                <div class="row">
                                                    <div class="col-md-5" style="display: none;">
                                                        <div class='input-group date' id='guest_check_inDiv'>
                                                            <input type='text' name="guest_check_in" id="guest_check_in" class="" placeholder="12:00 PM" />
                                                            <span class="input-group-addon">
                                                                <img src="{{URL::asset('media/images/clock.png')}}" alt="clock" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 col-md-5">
                                                        <div class="addon-select">
                                                            <img src="{{URL::asset('media/images/timeicon.png')}}" alt="clock" />
                                                            <select class="date-picker" id="check-in-date" name="guest_checkin_time">
                                                                <option value="11:00" selected>11:00</option>
                                                                <option value="11:30">11:30</option>
                                                                <option value="12:00">12:00</option>
                                                                <option value="12:30">12:30</option>
                                                                <option value="1:00">1:00</option>
                                                                <option value="1:30">1:30</option>
                                                                <option value="2:00">2:00</option>
                                                                <option value="2:30">2:30</option>
                                                                <option value="3:00">3:00</option>
                                                                <option value="3:30">3:30</option>
                                                                <option value="4:00">4:00</option>
                                                                <option value="4:30">4:30</option>
                                                                <option value="5:00">5:00</option>
                                                                <option value="5:30">5:30</option>
                                                                <option value="6:00">6:00</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 col-md-3">
                                                        <select class="timestamp" id="sel-in-time" name="guest_checkin_time_format">
                                                            <option value="AM" selected="selected">AM</option>
                                                            <option value="PM">PM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What time do guests check-out?</label>
                                            </div>
                                            <div class="right-input">
                                                <div class="row">
                                                    <div class="col-md-5" style="display: none;">
                                                        <div class='input-group date' id='guest_checkout_dateDiv'>
                                                            <input type='text' name="guest_checkout_date1" id="guest_checkout_date" class="" placeholder="12:00 PM" />
                                                            <span class="input-group-addon">
                                                                <img src="{{URL::asset('media/images/clock.png')}}" alt="clock" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 col-md-5">
                                                        <div class="addon-select">
                                                            <img src="{{URL::asset('media/images/timeicon.png')}}" alt="clock" />
                                                            <select class="date-picker" id="check-out-date" name="guest_checkout_time">
                                                                <option value="9:00" selected>9:00</option>
                                                                <option value="9:30">9:30</option>
                                                                <option value="10:00">10:00</option>
                                                                <option value="10:30">10:30</option>
                                                                <option value="11:00">11:00</option>
                                                                <option value="11:30">11:30</option>
                                                                <option value="12:00">12:00</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 col-md-3">
                                                        <select class="timestamp" id="sel-out-time" name="guest_checkout_time_format">
                                                            <option value="AM" selected="selected">AM</option>
                                                            <option value="PM" selected="selected">PM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>When is the property available? </label>
                                                <div class="date-error-label availibility-message" id="msg_available_property" style="@if(isset($property->type_of_property) &&  $property->type_of_property == 'vacation_rental') display:block @else display:none @endif"><label>You will need to upload a timeshare proof of membership to validate each of the availabilities listed. Please be sure that all your availabilities added match the rental periods in the timeshare agreement you will upload.</label></div>
                                            </div>
                                            <div class="right-input avaiblityDates">
                                                <div class="row avaiblityDatesBlock" style="padding-bottom: 10px;display:none;" >
                                                    <div class="col-md-5">
                                                        <div class='input-group date  calendar'>
                                                            <input type='text' class="frmdatetimepicker" placeholder="    /        /    " />
                                                            <span class="input-group-addon">
                                                                <img src="{{URL::asset('media/images/calendar.png')}}" alt="clock" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="inside-to">to</div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class='input-group date calendar '>
                                                            <input type='text'   class="todatetimepicker" placeholder="    /        /    " />
                                                            <span class="input-group-addon">
                                                                <img src="{{URL::asset('media/images/calendar.png')}}" alt="clock" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="delete-availabiities">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 pt-2">
                                                        <div class="partial-availabiities pb-2">
                                                            <label class="radio-btn mb-3">Partial booking is not allowed for these dates.
                                                                <input type="radio" class="pa-radio" name="is_partial_booking_date" value="0" checked>
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <label class="radio-btn mb-3">Partial booking is allowed for these dates.
                                                                <input type="radio" class="pa-radio" name="is_partial_booking_date" value="1">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <div class="min_night-availabiities pl-5">
                                                                    <input type="number" value="" name="min_night" class="min_night" value="1" style="width:86px; padding-left: 5px; padding-right: 5px; text-align: center;"  />
                                                                    <label class="font-bold custom">Minimum nightly booking length.</label>
                                                                    <div id="min_nightError" style="position: relative;"><label  class="custom-error" style=""></label> </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <span id="availableError"><label for="property_available_when[]" class="custom-error" style="display:none;">Invalid dates</label> </span>
                                                <span id="availableDelError"><label class="custom-error" style="display:none;">Invalid dates</label> </span>
                                            <a href="javascript:void(0)" class="add-more-availibility">
                                                <span>
                                                    <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M13.5 5.73202e-06C17.0814 -0.00329374 20.5171 1.41796 23.0496 3.95042C25.582 6.48287 27.0033 9.91857 27 13.5C27.0033 17.0814 25.582 20.5171 23.0496 23.0496C20.5171 25.582 17.0814 27.0033 13.5 27C9.91857 27.0033 6.48287 25.582 3.95042 23.0496C1.41796 20.5171 -0.00329374 17.0814 5.73202e-06 13.5C-0.00329374 9.91857 1.41796 6.48287 3.95042 3.95042C6.48287 1.41796 9.91857 -0.00329374 13.5 5.73202e-06Z" fill="#04CD61"/>
                                                    <path d="M14.0617 13.2173H18.6391V14.0822H14.0617V18.6384H13.1969V14.0822H8.64062V13.2173H13.1969V8.64H14.0617V13.2173Z" fill="white"/>
                                                    </svg>
                                                </span>Add more availabilities</a>
                                            </div>
                                            <div class="left-label"></div>
                                            <div class="right-input"></div>
                                        </div>

                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>What is your cancellation policy?</label>
                                            </div>
                                            <div class="right-input">
                                                <label class="radio-btn"><strong><img src="{{URL::asset('media/images/flexible.png')}}" alt="vacation" />Flexible</strong>(Guest can request full refund withing limited period. Cancellation request recieved at least 24 hours before the check-in time will be given full refund.)
                                                    <input type="radio" name="cancellation_policy" value="Flexible" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn"><strong><img src="{{URL::asset('media/images/moderate.png')}}" alt="private" />Moderate</strong>(Guest can request refund. If a guest cancels within 5 days of the check-in date, then the first night of the booking will not be refunded and only 50% of the accommodation fees for the rest of the booking will then be refunded. If the guest decides to cancel after check-in then 50% of the accommodation fees for any unspent nights are refundable. However, any nights spent will not be refunded.)
                                                    <input type="radio" name="cancellation_policy" value="Moderate">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="radio-btn"><strong><img src="{{URL::asset('media/images/strict.png')}}" class="small-img" alt="private" />Strict</strong>(Guests may receive a full refund if they cancel within 48 hours of booking and at least 14 full days prior to the listing’s local check-in time. After 48 hours guests are only entitled to a 50% refund regardless of how far the check-in date is.)
                                                    <input type="radio" name="cancellation_policy"  value="Strict">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <span id="cancellation_policyErrorLabel"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Please upload photos and videos of your property.</label>
                                                <label class="sub-label">Need some pointers on taking quality photos & videos?</label>
                                                <ul class="label-points">
                                                    <li>Declutter your property</li>
                                                    <li>Use natural daylight</li>
                                                    <li>Avoid flash</li>
                                                    <li>Shoot from room corners using landscape mode</li>
                                                    <li>Highlight special decor and amenities</li>
                                                    <li>Add photos of every room</li>
                                                </ul>
                                            </div>
                                            <div class="right-input">
                                                <label class="text">Add photos of listing</label>
                                                <div class="photos">
                                                    <div class="inputUploader"></div>
                                                    <span id="PhotoError"></span>
                                                    <h5 class="file-upload-desc">You need at least 6 photos.</h5>
                                                    <p class="file-upload-desc">Files must be JPEG, PNG, MOV, or MP4 format and at least 1024x683 pixels. Up to 15 photos may be added.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Please upload photos of proof of ownership or lease. </label>
                                                <label class="sub-label">Acceptable documents include:</label>
                                                <ul class="label-points">
                                                    <li>Timeshare ownership</li>
                                                    <li>Property deed </li>
                                                    <li>Lease agreement</li>
                                                </ul>
                                            </div>
                                            <div class="right-input">
                                                <div class="proof_of_ownership">
                                                <div class="inputUploader"></div>
                                                <p class="file-upload-desc p-t-20">Files must be in JPEG, PNG, or PDF.<br>Up to 6 photos may be added.</p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-table">
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Please upload photos of your government issued ID.</label>
                                                <label class="sub-label">Acceptable forms include:</label>
                                                <ul class="label-points">
                                                    <li>Drivers license (front and back)</li>
                                                    <li>State ID card</li>
                                                    <li>Military ID</li>
                                                    <li>Passport</li>
                                                </ul>
                                            </div>
                                            <div class="right-input">
                                                <div class="gov_id">
                                                <div class="inputUploader"></div>
                                                <p class="file-upload-desc p-t-20">Files must be in JPEG, PNG, or PDF.<br>Up to 6 files may be added.</p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="outer-table review-page">
                                        <div class="review-title">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4>Review your property information!</h4>
                                                </div>
                                                <!-- <div class="col-md-6 text-right preview-changes-btn">
                                                    <a href="#" class="preview-changes" data-toggle="modal" data-target="#form-submited-modal">Preview as a renter</a>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Name, description & about the space.</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text "><span class="name_your_listing_prev">Cottage Style House Available in Echo Park!</span><a href="javascript:showTab(0,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                                <h5 class="light-text describe_text">Adorable cottage available in the Echo Park neighborhood of Los Angeles. Close to Hollywoodand Downtown Los Angeles, and just a quick car ride away from the Santa Monica, Venice or Malibu for a day at the beach!</h5>
                                                <h5 class="light-text about_space_txt">Just behind the main house is adjacent guest house for additional guests and privacy. The guest house features two full size beds,full kitchen and washer and dryer.</h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Media (Photos & Video)</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text"><a href="javascript:showTab(9,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                                <div class="row" id="propertyImages">
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/property-image-review.png')}}" alt="edit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Property Type</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text"><span class="propertyTypetxt">Category: Private Residence, Cabin, Cottage</span> <a href="javascript:showTab(2,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Rooms & Spaces</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list"><span class="rooms_space">Bedrooms: 4<br>Beds: 6<br>Bathrooms: 3.5</span><a href="javascript:showTab(4,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Guest Accomodations</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list">Accomodates: <span id="Accomodates"></span>
                                                <br>Guest Access: <span id="guest_access"></span>
                                                <br>Guests Only Space: <span id="guest_only"></span>
                                                <a href="javascript:showTab(3,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Amenities</label>
                                            </div>
                                            <div class="right-input amenities-lists">
                                                <h5 class="bold-text">
                                                    <a href="javascript:showTab(5,true)" class="edit">
                                                        <img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit">
                                                    </a>
                                                    <div class="row" id="checkedAmenities">
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/pool.png')}}" alt="vacation">Pool</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/pool.png')}}" alt="vacation">Pool</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/wifi.png')}}" alt="vacation">WiFe</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/wifi.png')}}" alt="vacation">WiFe</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/washer-dryer.png')}}" alt="vacation">Washer/Dryer</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/washer-dryer.png')}}" alt="vacation">Washer/Dryer</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/gym.png')}}" alt="vacation">Gym</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="checkbox-btn"><img src="{{URL::asset('media/images/gym.png')}}" alt="vacation">Gym</label>
                                                        </div>
                                                    </div>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Address</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text"><span id="Address">2015 Park Drive, Los Angeles, CA 90026 </span><a href="javascript:showTab(1,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Price & Fees</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list">Nightly Price: $<span id="pricetxt"></span>
                                                <h5 class="bold-text list">Cleaning Fee: $<span id="cleaningFeetxt"></span>
                                                <br>Negotiable Price: <span id="is_negotiabletxt"></span>
                                                <br>Instant Booking Allowed: <span id="is_booking_alllowtxt"></span>
                                                <br><span id="offerprice"> Minimum nightly rate: $<span id="offerPricetxt"></span></span>
                                                <a href="javascript:showTab(6,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Check In, Check Out & Availability</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list">
                                                    Check In: <span id="checkintxt"></span>
                                                    <br>Check Out: <span id="checkouttxt"></span>
                                                    <br>
                                                        <span class="available">
                                                        <span class="title">Available:</span>
                                                        <span class="desc" id="allavailabletxt">10/28/2020 - 05/31/2021
                                                            <br>07/01/2021 - 10/28/2021</span>
                                                        </span>
                                                         <span class="pastDates" style="display: none; color:red;">
                                                            All the above availabilities are in the past. Please edit to add future availabilities.
                                                        </span>
                                                        <a href="javascript:showTab(7,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Cancellation Policy</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list"><span id="Cancellationtxt"></span><a href="javascript:showTab(8,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Verification Documents</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list"><a href="javascript:showTab(10,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                                <div class="row" id="verificationDoc">
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/verification1.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/verification2.png')}}" alt="edit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-custom">
                                            <div class="left-label">
                                                <label>Goverment ID</label>
                                            </div>
                                            <div class="right-input">
                                                <h5 class="bold-text list"><a href="javascript:showTab(11,true)" class="edit"><img src="{{URL::asset('media/images/edit-icon.png')}}" alt="edit"></a></h5>
                                                <div class="row" id="govId">
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/verification1.png')}}" alt="edit">
                                                    </div>
                                                    <div class="col-6 col-md-4">
                                                        <img src="{{URL::asset('media/images/verification2.png')}}" alt="edit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <div class="listing-footer-btns">
                                <div>
                                    <button type="button" id="prevBtn" onclick="nextPrev1(-1)"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</button>
                                    <button type="button" class="extra-dark-cstm" id="nextBtn" >Save and Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="form-submited-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content" style="background-image: url({{ asset('media/images/plane-bg.png') }});background-repeat: no-repeat;background-position: center;">
            <div class="description text-center">
              <h4>Congratulations! </h4>
              <h5>Your property has been submitted to GOT2GO for review, and we will notify you as soon as your listing is approved and live on GOT2GO!</h5>
                <a href="{{route('owner.property')}}"  class="btn btn-default dashboard-btn">Go to My Listings</a>
            </div>
          </div>
        </div>
      </div>

    <style>
        @media (min-width: 992px) {
            .content {
                padding: 40px 0;
            }
        }
        .select2-hidden-accessible {
            border: 0 !important;
            clip: rect(0 0 0 0) !important;
            height: 1px !important;
            margin: -1px !important;
            overflow: hidden !important;
            padding: 0 !important;
            position: absolute !important;
            width: 1px !important
        }



    </style>
    <script>

        var propertystore = "{{ route('property.store') }}";
        var propertyupload = "{{  route('property.upload') }}";
        var PropertyTypesAll = @JSON($propertyType);
        var Property_DB = @JSON($property);
        var bedTypes = @JSON(config('property.bed_types'));
        var PropertyTypes = {};
        var subPropertyType = {};
        var mediaPath = "{{URL::asset('uploads/property')}}";
        var mediaPublicPath = "{{URL::asset('media')}}";
        for (x in PropertyTypesAll) {
            if (PropertyTypesAll[x].parent_id == '0') {
                if (!PropertyTypes[PropertyTypesAll[x].type_of_property]) {
                    PropertyTypes[PropertyTypesAll[x].type_of_property] = {}
                }
                PropertyTypes[PropertyTypesAll[x].type_of_property][PropertyTypesAll[x].id] = {}
                PropertyTypes[PropertyTypesAll[x].type_of_property][PropertyTypesAll[x].id] = PropertyTypesAll[x].property_type;
            } else {
                if (!subPropertyType[PropertyTypesAll[x].parent_id]) {
                    subPropertyType[PropertyTypesAll[x].parent_id] = {}
                }
                subPropertyType[PropertyTypesAll[x].parent_id][PropertyTypesAll[x].id] = {}
                subPropertyType[PropertyTypesAll[x].parent_id][PropertyTypesAll[x].id] = PropertyTypesAll[x].property_type;
            }
        }

        var availableTags = [];
        var hotels = @JSON($hotels);
        for (y in hotels) {
            availableTags[hotels[y].id] = hotels[y].name
        }

    </script>

    <script src="{{URL::asset('js/custom.js')}}"></script>
    <script src="{{ URL::asset('js/owner/property/create.js') }}"></script>
    @endsection
