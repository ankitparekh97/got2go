
<li class="date-separator">
    <div class="separator-info"><span>{{$date}}</span></div>
</li>
@foreach($messages as $message)
@if($message->from_user == \Auth::guard('owner')->user()->id && $message->from_user_type =='owner')
<li class="replies" data-message-id="{{ $message->id }}">
    <div class="img">
        @if($message->pairUser->ownerUser->photo != '')
            <img class="lazy-load" data-src="{{URL::asset('uploads/owners/'.$message->pairUser->ownerUser->photo)}}" alt="photo">
        @else
            <div id="profileImage" class="owner">{{$message->pairUser->ownerUser->first_name[0].$message->pairUser->ownerUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="message-wrapper">
        <div class="message-box">
            <p>{!! $message->messages !!}</p>
        </div>
        <div class="time">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</li>
@else
<li class="sent" data-message-id="{{ $message->id }}">
    <div class="img">
        @if($message->from_user_type =='admin')
            <img class="lazy-load" data-src="{{URL::asset('media/images/owner-fav.png')}}" alt="{{$message->pairUser->adminUser->name}}">
        @elseif($message->pairUser->rentalUser->photo != '')
            <img class="lazy-load" data-src="{{URL::asset('uploads/users/'.$message->pairUser->rentalUser->photo)}}" alt="photo">
        @else
            <div id="profileImage" class="rental">{{$message->pairUser->rentalUser->first_name[0].$message->pairUser->rentalUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="message-wrapper">
        <div class="message-box">
            <p> {!! $message->messages !!}</p>
        </div>
        <div class="time">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</li>
@endif
@endforeach                