<ul>
	@foreach($users as $user)
	<li class="chatcontact chat-toggle chat_contact_{{(new \App\Helpers\Helper)->urlsafe_b64encode($user->id)}}" id="chat_contact_{{(new \App\Helpers\Helper)->urlsafe_b64encode($user->id)}}" data-id="{{ (new \App\Helpers\Helper)->urlsafe_b64encode($user->id) }}" data-user="{{($user->type =='admin')?$user->adminUser->name:$user->rentalUser->first_name.' '.$user->rentalUser->last_name }}" data-image="{{($user->type =='admin')?'Gotgo Admin':$user->rentalUser->photo }}" data-touserid="{{ (new \App\Helpers\Helper)->urlsafe_b64encode($user->rental_id) }}" data-type="{{$user->type}}" style="background-color:{{($user->messages_count > 0)?'#4a4a4a24':''}}">
		<div class="wrap">
			<div class="meta-left">
				@if($user->type =='admin')
					<img class="lazy-load" data-src="{{URL::asset('media/images/owner-fav.png')}}" alt="{{$user->adminUser->name}}">
					<div class="pink-dot"></div>
				@elseif($user->rentalUser->photo != '')
					<img class="lazy-load" data-src="{{URL::asset('uploads/users/'.$user->rentalUser->photo)}}" alt="{{$user->rentalUser->full_name}}">
					<div class="pink-dot"></div>
				@else
					<div id="profileImage" class="rental">{{$user->rentalUser->first_name[0].$user->rentalUser->last_name[0]}}</div>
				@endif
			</div>
			<div class="meta">
				<div class="name-wrap">
					<div class="name {{($user->type =='admin')?'admin_name':''}}" >{{($user->type =='admin')?$user->adminUser->name:$user->rentalUser->first_name.' '.$user->rentalUser->last_name}}
					</div>
					<div class="chat-date">{{date('m/d/Y',strtotime((new \App\Helpers\Helper)->convertToLocal($user->lastMessages->created_at, Session::get('timezone'))))}}</div>
				</div>
				<div class="message-block">
				<div class="preview">{{$user->lastMessages->messages}}</div>
				@if($user->messages_count > 0)
				<span class="message-count">{{$user->messages_count}}</span>
				@endif
				</div>
			</div>
		</div>
	</li>
	@endforeach
</ul>