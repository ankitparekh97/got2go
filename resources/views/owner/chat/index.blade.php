@extends('layouts.app')

@section('content')
<div class="content-area">
		<div class="custom-container">
			
			<div class="chat-back-button main-back-button"><a href="{{url()->previous()}}"><img class="lazy-load" data-src="{{URL::asset('media/images/chat-back.svg')}}" alt="chat"></a></div> 
			<h1 class="back-heading">Chat</h1>
			<div class="chat-wrapper">
				<div class="row">
					<div class="col-lg-4 chat-left chat-contact-mobile">
						<div class="tile height-block mb-0 p-0">
							<div class="sidechatpanel">
									<div class="search-chat">
										<label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
										<input type="text" placeholder="Search..." name="name" id="search">
										<a href="javascript:;" class="searchclose" style="display:none"><i class="fa fa-close" aria-hidden="true"></i></a>
									</div>
							</div>
							<div class="chatcontact-list mCustomScrollbar"  data-mcs-theme="dark">
								
							</div>
						</div>
                    </div>
                    @include('owner.chat.chat-box')
					<div class="col-lg-8 chat-right chat-detail-mobile" id="chat-overlay">
						<div class="tile height-block mb-0 p-0">
							<p class="first-conversation">Click on chat thread to start conversation</p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <input type="hidden" id="current_user" value="{{ (new \App\Helpers\Helper)->urlsafe_b64encode(\Auth::guard('owner')->user()->id) }}" />
    <input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') }}" />
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}" />

    <script>
		var getMessage = "{{route('chat.getmessages')}}";
		var imagePath = "{{URL::asset('uploads/users/')}}";
		var sendMessage = "{{route('chat.send')}}";
		var chatSearch = "{{route('owner.chatsearch')}}";
		var imagesendPath = "{{URL::asset('uploads/owners/')}}";
		var unreadUrl = "{{route('owner.unread.chat')}}";
		var adminLogo = "{{URL::asset('media/images/owner-fav.png')}}";
		var chatId = "{{ app('request')->input('id')}}";
	</script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script src="{{ asset('js/chat.js') }}"></script>
    @endsection    