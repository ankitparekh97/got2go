<div class="tile height-block mb-0 p-0" id="chat_box" style="display:none">
    <div class="chatcontent-header">
        <div class="content-user">
        <div class="d-block d-xs-block d-sm-block d-md-none d-lg-none chat-back-button"><img class="lazy-load" data-src="{{URL::asset('media/images/chat-back.svg')}}" alt="chat"></div>
            <div class="content-userimg"></div>
            <div class="name-wrap"> 
                <div class="name"> 
                </div>
            </div>
        </div>
        <div class="content-link">
            <ul>
                <li class="active"> <a href="javascript:void(0)">Conversation </a> </li>
                <li> <a href="javascript:void(0)">Offers</a> </li>
            </ul>
        </div>
    </div>
    <div class="chatcontent-body"  data-mcs-theme="dark">
        <div class="message-list">
            <ul></ul>
        </div>
    </div>
    <div class="chatcontent-footer">
        <div class="chatcontent-footer-wrapper">
            <div class="chat-textarea"><textarea class="form-control chat_input" id="" rows="2" placeholder="Type your message here...."></textarea></div>
            <div class="chat-action">
                <button class="btn btn-pink btn-pill btn-round btn-chat" data-to-user="" data-id="">Send</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="to_user_id" value="" />
     <input type="hidden" id="id" value="" />
</div>