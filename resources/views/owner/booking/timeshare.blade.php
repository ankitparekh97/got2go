@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Timeshare Booking</h1>
    <div>&nbsp;

    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Timeshare name</td>
          <td>property_type</td>
          <td>date</td>
          <td >Owner Action</td>
          <td >Property Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($bookings as $booking)
    <tr id="row_{{$booking->booking_id}}">
            <td>{{$booking->booking_id}}</td>
            <td>{{$booking->full_name}}</td>
            <td>{{$booking->timeshare_description}}</td>
            <td>{{$booking->property_type}}</td>
            <td>{{ Carbon\Carbon::parse($booking->check_in_date)->format('F-d')}} To {{ Carbon\Carbon::parse($booking->check_out_date)->format('F-d')}}</td>

            <td>
            @if($booking->owner_status == 'pending' || $booking->owner_status == 'decline')
            <button id="approveBtn" class="btn btn-primary"type="button" onclick="updateOwnerStatus({{$booking->booking_id}}, 1)">Approve</button>

            @else
               <span  class="text-success"> Approved</span>
            @endif

            @if($booking->owner_status == 'pending' || $booking->owner_status == 'approved')
            <button id="declineBtn"  class="btn btn-danger"type="button" onclick="updateOwnerStatus({{$booking->booking_id}}, 0)">Decline</button>
            @else
            <span  class="text-danger"> Declined</span>
            @endif

            </td>

            <td>

            @if($booking->property_status == 'pending' || $booking->property_status == 'decline')
            <button id="approveBtn" class="btn btn-primary"type="button" onclick="updatePropertyStatus({{$booking->booking_id}}, 1)">Approve</button>

            @else
               <span  class="text-success"> Approved</span>
            @endif

            @if($booking->property_status == 'pending' || $booking->property_status == 'approved')
            <button id="declineBtn"  class="btn btn-danger"type="button" onclick="updatePropertyStatus({{$booking->booking_id}}, 0)">Decline</button>
            @else
            <span  class="text-danger"> Declined</span>
            @endif

            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('properties.list') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
            },
            success: function (data) {
                console.log(data);
            }
        });
    });

    function updatePropertyStatus(booking_id,status){
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: "{{ route('property.bookingstatus') }}",
                type: 'POST',
                data: {_token: CSRF_TOKEN, 'booking_id': booking_id, 'property_status':status},
                dataType: 'JSON',
                params: {'booking_id': booking_id, 'owner_status':status},
                headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
            },
                success: function (data, status) {
                    if(data.success)
                    window.location.reload();
                    else
                      alert(data.error);
                }
            });
        }

        function updateOwnerStatus(booking_id,status){
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                url: "{{ route('owner.bookingstatus') }}",
                type: 'POST',
                data: {_token: CSRF_TOKEN, 'booking_id': booking_id, 'owner_status':status},
                dataType: 'JSON',
                params: {'booking_id': booking_id, 'owner_status':status},
                headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
            },
                success: function (data, status) {
                    if(data.success)
                      window.location.reload();
                    else
                      alert(data.error);

                }
            });
        }




</script>
@endsection
