@extends('layouts.app')


@section('headerCss')
    @parent
    <link rel="stylesheet" href="{{URL::asset('css/booking.css')}}">
    <style>
        .datepicker table tr td.disabled {
            color: #777777 !important;
            opacity: 0.3;
            position: relative;
        }
        .datepicker table tr td.disabled:before {
            content: "";
            position: absolute;
            height: 1px;
            width: 50%;
            background: #000;
            opacity: 1;
            top: 18px;
            left: 10px;
        }
        .contentTitleH3 {
            cursor: pointer;
        }
        .datepic-inputgroup {
            border: 1px solid #C5CAD8 !important;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            -ms-border-radius: 6px;
            -o-border-radius: 6px;
            /* width: 150px; */
            display: flex;
            align-items: center;
            background-color: #fff;
        }
        .datepic-inputgroup.disabled {
            background-color: #e9ecef;
        }
        .datepic-inputgroup span.dateicn { display: flex;}
        .datepic-inputgroup span.dateicn i { font-size:24px; color:#3D3D3D}
        div#makeoffer th:first-child {
            width:210px
        }
        .counter-datepicker { display:flex; align-items: center;}
        .counter-datepicker .form-control{
            border:0;
            height: 46px;
            font-weight: 700;
            font-size:18px;
            width: 110px;
            display: inline-block;
            border: 0;
            padding: 15px 10px 15px 0;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            -ms-border-radius: 6px;
            -o-border-radius: 6px;
        }
        .proposed-input-wrap {
            border: 1px solid #C5CAD8 !important;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            -ms-border-radius: 6px;
            -o-border-radius: 6px;
            background-color: #fff;
        }
        .proposed-input-wrap .input-group-text {
            padding: 0 5px 0 15px;
            font-size: 16px;
            font-weight: 700;
            background-color: #ffffff;
            border: 0px;
            border-radius: 10px;
        }
        .proposed-input-wrap .form-control {
            width: 40px;
            padding: .65rem 1rem 0.65rem 5px;
            height: 48px;
            border: 0;
            border-radius: 16px;
            font-size: 18px;
            font-weight: 700;
        }
        div#makeoffer table.table thead tr th:last-child {
            text-align:center;
        }
        div#makeoffer table.table tbody tr.tripperData:first-child .rangeDates {
            border-top-left-radius: 6px;
            border-bottom-left-radius: 6px;
        }
        div#makeoffer table.table tbody tr.tripperData:first-child .totalNightlyStandard {
            border-top-right-radius: 6px;
            border-bottom-right-radius: 6px;
        }

        div#makeoffer table.table tbody tr.counterOffer:first-child .rangeDates,
        div#makeoffer table.table tbody tr.counterOffer .rangeDatesWithFlexible {
            border-top-left-radius: 6px;
            border-bottom-left-radius: 6px;
        }
        div#makeoffer table.table tbody tr.counterOffer:nth-child(3) .totalNightlyStandard {
            border-top-right-radius: 6px;
            border-bottom-right-radius: 6px;
        }
        .offer-accepted a.cancel{
            color:#727272;
            font-size:16px;
            font-weight:600;
        }
        .hide {
            display: none;
        }
    </style>
@endsection

@section('content')

<div class="alert-popup-box-red payment-error" style="display:none">
            <a href="#" class="close-btn1"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M2 2L13 13" stroke="white" stroke-width="3.5"></path>
<path d="M2 13L13 2" stroke="white" stroke-width="3.5"></path>
</svg>
</a>
<p>You have an offer pending your response, click here to respond to the Tripper</p> <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="0.511719" width="25.4881" height="24" fill="black" fill-opacity="0.01"></rect>
<path d="M5.82227 12C5.82227 11.4477 6.29774 11 6.88427 11H19.6283C20.2148 11 20.6903 11.4477 20.6903 12C20.6903 12.5523 20.2148 13 19.6283 13H6.88427C6.29774 13 5.82227 12.5523 5.82227 12Z" fill="white"></path>
<path d="M18.157 11.9701L13.0031 6.6761C12.7466 6.41279 12.6588 6.04064 12.7727 5.69984C12.8866 5.35903 13.185 5.10134 13.5554 5.02384C13.9258 4.94633 14.312 5.06079 14.5685 5.3241L20.4095 11.3241C20.7938 11.7188 20.7798 12.3285 20.3776 12.7071L14.0056 18.7071C13.739 18.9671 13.3441 19.0713 12.9728 18.9798C12.6015 18.8883 12.3115 18.6152 12.2143 18.2656C12.1171 17.916 12.2279 17.5442 12.5039 17.2931L18.157 11.9701Z" fill="white"></path>
</svg>

        </div>
    <div class="custom-container">
        <!--begin::Content Wrapper-->
        <div class="">

            <div class="property-edit">
                <div class="bookin-top-search" style="display: none">
                    <form action="" name="bookingId" id="bookingId">

                        <div class="search-box cmn-field label" style="width:200px">
                            <div class="form-group">
                                <i class="fas fa-search"></i>
                                <input type="" class="form-control bookin-top-search" name="search" id="searchbox" placeholder="Search..">
                            </div>
                        </div>
                        <div class="search-box cmn-field label">
                            <div class="form-group">
                                <label>Status:</label>
                                <select class="form-control" id="owner_status" name="owner_status" style="width: 180px;">
                                    <option value="">All</option>
                                    <option value="pending">Pending</option>
                                    <option value="approved">Approved</option>
                                    <option value="rejected">Rejected</option>
                                    <option value="auto_reject">Auto-Rejected</option>
                                    <option value="instant_booking">Auto-Approved</option>
                                    <option value="confirmed">Confirmed with Property</option>
                                </select>
                            </div>
                        </div>
                        <div class="search-box cmn-field label">
                            <div class="form-group">
                                <label> Type:</label>
                                <select class="form-control" id="booking_type" name="booking_type">
                                    <option value="">All</option>
                                    <option value="property">Property</option>
                                    <option value="vacation_rental">Timeshare</option>
                                </select>
                            </div>
                        </div>
                        <div class="search-box cmn-field submit-box">
                            <div class="form-group">
                                <button type="button" name="filter" id="filter" class="btn btn-danger font-weight-bold btn-square">Search</button>
                            </div>
                        </div>

                    </form>
                </div>
                <ul class="nav nav-tabs listing-tabs-current" id="listingtab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pending-tab" data-tab="pending" data-toggle="tab" href="#pending" role="tab" aria-controls="home" aria-selected="true">Booking Requests</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="confirmed-tab" data-tab="confirmed" data-toggle="tab" href="#confirmed" role="tab" aria-controls="profile" aria-selected="false">Accepted Bookings</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" id="offers-tab" data-tab="offers" data-toggle="tab" href="#offers" role="tab" aria-controls="contact" aria-selected="false">Offers</a>
                    </li>
                </ul>

                <div class="tab-content" id="listingtabcontent">
                    <div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                        <div class="booking-list-outer">
                            <div class="booking-container-outer border-bottom">
                                <div class="table-responsive custom-resp">
                                    <table class="booking-table table display propertyBooking custom-data-table">
                                        <thead>
                                        <tr>
                                            <th> </th>
                                            <th>Renter</th>
                                            <th>Requested Listings </th>
                                            <th>Booking Dates </th>
                                            <th>Earnings</th>
                                            <th>Status </th>
                                            <th>Actions </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="confirmed" role="tabpanel" aria-labelledby="confirmed-tab">
                        <div class="booking-list-outer">
                            <div class="booking-container-outer border-bottom">
                                <div class="table-responsive custom-resp">
                                    <table class="custom-data-table booking-table table display confirmedPropertyBooking">
                                        <thead>
                                        <tr>
                                            <th> </th>
                                            <th>Renter</th>
                                            <th>Requested Listings </th>
                                            <th>Booking Dates </th>
                                            <th>Earnings</th>
                                            <th>Status </th>
                                            <th>Actions </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                     <div class="tab-pane fade" id="offers" role="tabpanel" aria-labelledby="offers-tab">
                        <div class="offer-list">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!--end::Content-->
        </div>
        <!--begin::Content Wrapper-->
    </div>

    <!-- Cancel Acceptance -->
    <div class="modal fade cmn-popup approve" id="cancelAcceptance" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center content">Are you sure you want to cancel your acceptance to this offer?  We will notify  the Tripper immediately once you cancel your booking acceptance.</p>
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn approveBooking" onclick="OwnerOffer.cancelOffer()">Cancel Acceptance</button><br>
                        <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                    </div>
                    <input type="hidden" name="ownerId" id="cancelOfferPopupOwnerId" />
                    <input type="hidden" name="rentalId" id="cancelOfferRentalId" />
                    <input type="hidden" name="propertyId" id="cancelOfferPropertyId" />
                    <input type="hidden" name="propertyOfferId" id="cancelOfferPopupPropertyOfferId" />
                </div>
            </div>
        </div>
    </div>
    <!-- End of Cancel Acceptance -->

    <!-- Decline offer -->
    <div class="modal fade cmn-popup approve" id="declineOfferPop" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center content">Are you sure you want to decline this offer?</p>
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn approveBooking" onclick="OwnerOffer.declineOffer()">Decline Offer</button><br>
                        <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                    </div>
                    <input type="hidden" name="ownerId" id="declineOfferPopupOwnerId" />
                    <input type="hidden" name="rentalId" id="declineOfferRentalId" />
                    <input type="hidden" name="propertyId" id="declineOfferPropertyId" />
                    <input type="hidden" name="propertyOfferId" id="declineOfferPopupPropertyOfferId" />
                </div>
            </div>
        </div>
    </div>
    <!-- Decline offer -->

    <!-- Make counter offer -->
    <div class="modal fade cmn-popup approve" id="makeoffer" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Make Counter Offer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="makeACounterOfferForm" name="makeACounterOfferForm" method="post" >
                        <div class="error-label text-center hide">
                            The dates you selected are not available.
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Dates</th>
                                        <th scope="col">Nights</th>
                                        <th scope="col">$/Night</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tripperData">
                                        <td>Tripper’s Offer</td>
                                        <td class="rangeDates">10/28/2020 to 10/31/2020</td>
                                        <td class="totalNight">2</td>
                                        <td class="singleNightlyStandard">$200</td>
                                        <td class="totalNightlyStandard">$400</td>
                                    </tr>
                                    <tr class="standardData">
                                        <td>Standard Rate</td>
                                        <td class="rangeDates">10/28/2020 to 10/31/2020</td>
                                        <td class="totalNight">2</td>
                                        <td class="singleNightlyStandard">$200</td>
                                        <td class="totalNightlyStandard">$400</td>
                                    </tr>
                                    <tr class="counterOffer">
                                        <td>Your Counter Offer</td>
                                        <td class="rangeDatesWithFlexible">
                                            <div class="counter-datepicker">
                                                <div class="datepic-inputgroup">
                                                    <span class="dateicn"><i class="got got-date"></i></span>
                                                    <input type="text" name="ownerRangeStartDate" id="ownerRangeStartDate" class="form-control" />
                                                </div>
                                                <span>to</span>
                                                <div class="datepic-inputgroup">
                                                    <span class="dateicn"><i class="got got-date"></i></span>
                                                    <input type="text" name="ownerRangeEndDate" id="ownerRangeEndDate" class="form-control" />
                                                </div>
                                            </div>
                                        </td>
                                        <td class="totalNight">2</td>
                                        <td class="singleNightlyStandard">$200</td>
                                        <td class="totalNightlyStandard">
                                            <div class="proposed-input-wrap">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">$</div>
                                                    </div>
                                                    <input type="number" name="totalNightlyStandard" id="inputTotalNightlyStandard"  class="form-control" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="ownerId" id="makeAnOfferOwnerId" />
                            <input type="hidden" name="rentalId" id="makeAnOfferRentalId" />
                            <input type="hidden" name="propertyId" id="makeAnOfferPropertyId" />
                            <input type="hidden" name="propertyOfferId" id="makeAnOfferPropertyOfferId" />
                            <input type="hidden" name="propertyOwnerOfferStartDate" id="makeAnOfferPropertyOfferStartDateByOwner" />
                            <input type="hidden" name="propertyOwnerOfferEndDate" id="makeAnOfferPropertyOfferEndDateByOwner" />
                            <input type="hidden" name="propertyTripperOfferStartDate" id="makeAnOfferPropertyOfferStartDateByTripper" />
                            <input type="hidden" name="propertyTripperOfferEndDate" id="makeAnOfferPropertyOfferEndDateByTripper" />
                            <input type="hidden" name="propertyNumberOfGuests" id="makeAnOfferPropertyNumberOfGuests" />
                            <input type="hidden" name="propertyTripperOfferNightlyPrice" id="makeAnOfferPropertyNightlyPriceByTripper" />
                            <input type="hidden" name="propertyIsFlexibleDate" id="makeAnOfferPropertyIsFlexibleDate" />
                            <input type="hidden" name="propertyStandardPrice" id="makeAnOfferStandardPrice" />
                            <input type="hidden" name="propertyMinimumOffer" id="makeAnOfferPropertyMinimumOffer" />
                            <input type="hidden" name="propertyActualNightlyPrice" id="makeAnOfferPropertyActualNightlyPrice" />
                        </div>
                        <div class="text-center delete-btns">
                            <button type="button" onclick="OwnerOffer.createCounterOffer()" class="btn btn-default red-btn-cmn approveBooking">Submit Offer</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Approve Modal -->
    <div class="modal fade cmn-popup approve" id="approve"data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center content">Are you sure you want to approve this timeshare booking request? Following approval, you will need to submit a confirmation ID provided by your vacation club.</p>
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn approveBooking">Approve Request</button><br>
                        <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Confirmation Modal -->
     <div class="modal fade cmn-popup confirmBox" id="confirmBox" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Please Enter Confirmation ID Below</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="dashboard-forms" id="confirmation">
                        <p class="text-center content">Please enter confirmation ID from your resort below to confirm this booking for your timeshare unit.</p>
                        <div class="row m-0">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Confirmation ID:</label>
                                <input type="number" class="form-control" name="confirmation_id" id="confirmation_id">
                                <span id="confirmationIdErr"></span>
                            </div>
                        </div>
                        <div class="text-center delete-btns">
                            <button type="submit" class="btn btn-default red-btn-cmn approveBooking ">Submit Confirmation ID</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Success Modal -->
    <div class="modal fade cmn-popup congrats" id="congrats" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="congrats" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 >Congratulations!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center content"></p>
                    <div class="text-center delete-btns">
                        <button type="button" class="btn btn-default red-btn-cmn confirmedBookings">View Confirmed Bookings</button><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footerJs')
    @parent
    <script>
        jQuery( ".payment-error a.close-btn1" ).click(function() {
          jQuery(this).parent().addClass('d-none');
        });
        jQuery('.frmdatetimepicker').datepicker();

        jQuery('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        });
    </script>
    <script>
        var approve = "{{ route('property.bookingstatus') }}";
        var bookingData = "{{ route('property.propertyBooking') }}";
        var confirmedbookingData = "{{ route('property.confirmedPropertyBooking') }}";
        var processingImg = "{{URL::asset('media/images/loader.gif')}}";
        var calculateownerRefund = "{{route('calculate.owner.refund')}}";
        const getPropertyWebURL = '{{ route('propertydetails.index',['id' => '#propertyId' ]) }}';
        const getOwnerPropertyOffersURL = '{{ route('api.owner.propertyOffers.get',['ownerId' => getOwnerId() ]) }}';
        const getOwnerAcceptPropertyOfferURL = '{{ route('api.owner.propertyOffers.acceptOffer',['ownerId' => getOwnerId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const getOwnerDeclinePropertyOfferURL = '{{ route('api.owner.propertyOffers.declineOffer',['ownerId' => getOwnerId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const getPropertyOfferByIdURL = '{{ route('api.owner.propertyOffers.getByOfferId',['ownerId' => getOwnerId(), 'propertyOfferId' => '#propertyOfferId']) }}';
        const counterOfferPropertyURL = '{{ route('api.owner.propertyOffers.counterPropertyOffer',['ownerId' => getOwnerId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const getOwnerCancelPropertyOfferURL = '{{ route('api.owner.propertyOffers.cancelPropertyOffer',['ownerId' => getOwnerId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const getPropertyAvailabilityCheckURL = '{{ route('api.property.checkAvailabilityWithCheckinCheckOutDate',[ 'propertyId' => '#propertyId', 'checkInDate' => '#checkInDate', 'checkOutDate' => '#checkOutDate']) }}';

        const ownerId = '{{ getOwnerId() }}';
    </script>
    <script src="{{ URL::asset('js/owner/booking/booking.js') }}"></script>
@endsection
