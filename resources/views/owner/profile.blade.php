@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update Profile</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" id="profile_update" action="{{ route('profile.basic') }}" enctype="multipart/form-data">
            @csrf
            {{method_field('put')}}

            <div class="form-group">
                <label for="first_name">First Name:</label>
                <input type="text" class="form-control" name="first_name" value="{{ $owner->first_name }}" />
            </div>
            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" class="form-control" name="last_name" value="{{ $owner->last_name }}" />
            </div>

            <div class="form-group">
                <label for="bio">Bio:</label>
                <input type="text" class="form-control" name="bio" value="{{ $owner->bio }}" />
            </div>

            <div class="form-group">
                <label for="birthdate">birthdate:</label>
                <input type="date" class="form-control" name="birthdate" value="{{ $owner->birthdate }}" />
            </div>

            <div class="form-group">
                <label for="phone">phone:</label>
                <input type="text" class="form-control" name="phone" value="{{ $owner->phone }}" />
            </div>

            <div class="form-group">
                <label for="address">address:</label>
                <textarea type="text" class="form-control" name="address" >{{ $owner->address }} </textarea>
            </div>

            <div class="form-group">
                <label for="city">city:</label>
                <input type="text" class="form-control" name="city" value="{{ $owner->city }}" />
            </div>


            <div class="form-group">
                <label for="State">State:</label>
                <input type="text" class="form-control" name="state" value="{{ $owner->state }}" />
            </div>

            <button type="submit" class="btn btn-primary-outline">Edit Basic detail</button>
        </form>


        <form method="post" id="profile_change_password" action="{{ route('profile.change.password') }}" enctype="multipart/form-data">
            @csrf
            {{method_field('put')}}

            <div class="form-group">
                <label for="current_password">currnet password</label>
                <input type="password" class="form-control" name="current_password" value="" />
            </div>
            <div class="form-group">
                <label for="password">New password:</label>
                <input type="password" class="form-control" name="password" value="" />
            </div>

            <div class="form-group">
                <label for="password_confirmation">Confirm password:</label>
                <input type="password_confirmation" class="form-control" name="password_confirmation" value="" />
            </div>


            <button type="submit" class="btn btn-primary-outline">Update Password</button>
        </form>







    </div>
</div>

<script>
var profilebasic = "{{ route('profile.basic') }}";
var profilechangepassword = "{{ route('profile.change.password') }}";
</script>
<script src="{{ URL::asset('js/owner/login/profile.js') }}"></script>
@endsection
