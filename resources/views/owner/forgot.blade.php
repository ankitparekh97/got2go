@extends('layouts.app')

@section('content')
<style>
#kt_header_menu_wrapper { display: none;}
</style>
<!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <h2 class="f-w-700 text-uppercase text-dark d-block w-100 align-items-center text-center">
                  Forgot Password                              
               </h2>
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid d-lg-flex" id="kt_content" style="background-image: url('{{URL::asset('media/images/login-bg.jpg')}}')">
         
            <!-- Login start -->
               <div class="login-container align-items-center w-100">
               @if(session()->get('success'))
                  <div class="alert alert-success">
                     {{ session()->get('success') }}  
                  </div>
               @endif
               <div class="alert alert-danger" style="display:none;">
                     <ul style="margin-bottom:0">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
                  <div class="d-table">
                     <div class="d-table-cell vertical-middle">
                        <form method="POST" id="ownerforgot" action="{{ route('owner.forgot.password') }}">
                        @csrf
                           <div class="input-group">
                              <div class="input-group-prepend">
                                 <span class="input-group-text">
                                    <i class="icon-xl far fa-envelope"></i>
                                 </span>
                              </div>
                              <input id="email" type="email"  class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}">
                           </div>
         
                           <div class="forget-password text-right">
                              <a href="{{route('ownerloginform')}}">Sign in</a>
                           </div>
                           <input type="submit" value="RESET MY PASSWORD">
                        </form>
                     </div>
                  </div>
               </div>
            <!-- Login END -->
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<script>
    var ownerforgotpassword = "{{ route('owner.forgot.password') }}";
    var ownerforgot = "{{ route('owner.forgot') }}";
</script>
<script src="{{ URL::asset('js/owner/login/forgot.js') }}"></script>
@endsection
