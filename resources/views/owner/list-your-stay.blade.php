@extends('layouts.rentalApp')

@section('headerJs')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&callback=initAutocomplete&libraries=places&v=weekly" defer></script>
    <script src="{{ URL::asset('js/googlemap.js') }}"></script>
    <script src="{{ URL::asset('js/popper.min.js') }}"></script>
@endsection

@section('content')
    <div class="tripper-banner list-page" style="background-image: url({{URL::asset('media/images/view-stays-banner.jpg')}});">
        <div class="banner-comtent" id="refreshPart">
            <h1>LIST YOUR STAY<br>AND START EARNING</h1>
            @if(Session::get('owner')!='')
                <a href="{{route('owner.property')}}" class="red-btn">GO TO OWNER DASHBOARD</a>
            @else
                <a class="red-btn login-model-open">LOGIN</a>
            @endif
        </div>
    </div>

    <div class="your-property-worth">
        <div class="form">
                <!-- One "property-tab" for each step in the form: -->
                <div class="property-tab">
                    <form id="owner-panel-form" name="owner-panel-form">
                        <div class="steps-title">Step 1 of  3 - Location </div>
                        <h2>WHAT’S YOUR PROPERTY WORTH AS A RENTAL?</h2>
                        <input type="hidden" class="small" name="where_is_listing_clone" id="where_is_listing_clone">
                        <input type="hidden" class="small" name="city" id="city">
                        <input type="hidden" class="small" name="state" id="state">
                        <input type="hidden" class="small" name="country" id="country">
                        <input type="hidden" class="small" name="zipcode" id="zipcode">
                        <input type="hidden" class="small" name="place_id" id="place_id">
                        <input type="text" name="where_is_listing" autocomplete="off" id="where_is_listing" placeholder="Enter your property’s address"/>
                        <div class="input-group mb-4" >
                            <label class="error" id="discountPercentageError" style="display: none;">
                                Please enter valid property address.
                            </label>
                        </div>
                        <div id="iframe-map" class="map iframe-map" style="display: none;">
                            <div id="map" style="height:300px;"></div>
                        </div>
                    </form>
                </div>
                <div class="property-tab">
                    <div class="steps-title">Step 2 of  3 - Accommodations </div>
                    <h2>WHAT’S YOUR PROPERTY WORTH AS A RENTAL?</h2>
                    <div class="inner-inc-desc-sec">
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-8">
                                    How many guests are allowed?
                                </div>
                                <div class="col-md-4">
                                    <div class="field1">
                                        <button type="button" id="sub_guest" class="sub min"></button>
                                        <input type="number" id="no_of_guest" name="no_of_guest" value="1" min="1" max="30" />
                                        <button type="button" id="addguest" class="add">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-8">
                                    How many bedrooms are there?
                                </div>
                                    <div class="col-md-4">
                                    <div class="field1">
                                        <button type="button" id="bedroom" class="sub min"></button>
                                        <input type="number" id="no_of_bedroom" name="no_of_bedroom" value="1" min="1" max="30" />
                                        <button type="button" id="addbedroom" class="add">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-8">
                                    How many bathrooms are available?
                                </div>
                                    <div class="col-md-4">
                                    <div class="field1">
                                        <button type="button" id="subbath" class="subbath sub decrescimo"></button>
                                        <input type="number" id="no_of_bathrooms" name="no_of_bathrooms" value="1" min="0" max="15" />
                                        <button type="button" id="addbath" class="add acrescimo">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="property-tab">
                    <div class="annually-popup popup2" style="background-image: url({{URL::asset('media/images/calc-plane.png')}});{{((session()->get('owner_token') && @session()->get('user')['is_otp_verified'] == 0) || (!session()->get('owner_token')))?'display:block':'display:none'}}">
                        <h4>EARN UP TO</h4>
                        <h3>${{number_format($totalEarn)}}<span>ANNUALLY</span></h3>
                        <p>Based on the top 5% of properties similar to yours</p>
                        <div class="footer-btns">
                            <a href="javascript:void(0)" onclick="nextPrev(1)" class="sign-up gradient-btn large">LIST MY PROPERTY</a><br>
                            <a href="{{route('owner.list-your-stay')}}" class="close-btn-text">Exit Estimator Tool</a>
                        </div>
                    </div>
                    <div class="annually-popup popup3" style="background-image: url({{URL::asset('media/images/calc-plane.png')}});{{((session()->get('owner_token') && @session()->get('user')['is_otp_verified'] == 0) || (!session()->get('owner_token')))?'display:none':'display:block'}}">
                        <h4>EARN UP TO</h4>
                        <h3>${{number_format($totalEarn)}}<span>ANNUALLY</span></h3>
                        <p>Based on the top 5% of properties similar to yours</p>
                        <div class="footer-btns">
                            <a href="{{ url('owner_panel/property/add')}}" class="gradient-btn large">Start a New Listing</a><br>
                            <a href="{{route('owner.list-your-stay')}}" class="close-btn-text">Exit Estimator Tool</a>
                        </div>
                    </div>
                </div>
                <div class="property-tab">
                    <div class="steps-title">Step 3 of  3 </div>
                    <div class="property-tab3" style="{{(session()->get('owner_token') && @session()->get('user')['is_otp_verified'] == 0)?'display:none':'display:block'}}">
                        <div class="create-account-owner">
                            <h2>CREATE AN ACCOUNT TO FINISH LISTING YOUR PROPERTY.<span>Already have an account? <a class="owner-sign-up" href="javascript:void(0)">Login.</a></span></h2>
                            <form method="POST" id="ownersignup" name="ownersignup" action="">
                                @csrf
                                <div class="alert alert-danger" style="display:none;">
                                    <ul style="margin-bottom:0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="email" id="email" placeholder="Enter your email" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="registerphoneno" id="phone" placeholder="Enter your phone number" />
                                    </div>
                                </div>
                                <div style="overflow:auto;">
                                    <div class="text-center">
                                        <button type="button" class="prevBtn" onclick="nextPrev(-1)">Back</button>
                                        <input type="submit" value="Create Account" class="gradient-input">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                     <div class="property-tab32" style="display:none">
                        <div class="create-account-ownerregister">
                            <h2>CREATE AN ACCOUNT TO FINISH LISTING YOUR PROPERTY.<span>Already have an account? <a class="owner-sign-up" href="javascript:void(0)">Login.</a></span></h2>
                            <form method="POST" id="ownerregister" name="ownerregister" action="">
                                @csrf
                                <div class="alert alert-danger" style="display:none;">
                                    <ul style="margin-bottom:0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="first_name" id="name" placeholder="First Name" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="last_name" id="last_name" placeholder="Last Name" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="password" name="password" id="password" placeholder="Enter your password" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm your password" />
                                    </div>
                                </div>
                                <div style="overflow:auto;">
                                    <div class="text-center">
                                        <button type="button" class="prevBtn" onclick="nextPrev(-1)">Back</button>
                                        <input type="submit" value="Create Account" class="gradient-input">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="login-modal-owner login-modal1" style="display: none;">
                        <h2>LOGIN TO YOUR  ACCOUNT.<span>Don’t have an owner’s account? <a class="owner-login" href="javascript:void(0)">Sign Up.</a></span></h2>
                        <form method="POST" id="ownerlogin" action="{{route('userlogin')}}">
                            @csrf
                            <div class="alert alert-custom alert-notice alert-light-danger signupmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text"></div>
                            </div>

                            <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 tripperUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text" style="font-weight: bold;"> </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" id="email" name="email" placeholder="Email" />
                                </div>
                                <div class="col-md-6">
                                    <input type="password" id="password" name="password" placeholder="Password" />
                                </div>
                            </div>
                            <div style="overflow:auto;">
                                <div class="text-center">
                                    <button type="button" class="prevBtn" onclick="nextPrev(-1)">Back</button>
                                    <button type="submit" value="login"  class="gradient-input"> Login</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="otp-modal1" style="{{(session()->get('owner_token') && @session()->get('user')['is_otp_verified'] == 0)?'display:block':'display:none'}}" >
                        <h2>CREATE AN ACCOUNT TO FINISH LISTING YOUR PROPERTY.</h2>
                        <form method="POST" id="validateotp" name="validateotp">
                            <input type="hidden" name="unique_key" id="unique_key" value="{{@session()->get('user')['unique_key']}}">
                            @csrf
                            <div class="alert alert-danger" style="display:none;">
                                    <ul style="margin-bottom:0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                            </div>

                            <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 tripperUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text" style="font-weight: bold;"> </div>
                            </div>

                            <div class="row justify-content-center otp-row">
                                <div class="col-md-6">
                                    <p>ENTER YOUR ONE TIME PASSWORD</p>
                                    <input type="number" id="otp" name="otp_code" placeholder="Enter OTP" />
                                </div>
                                @error('otp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div style="overflow:auto;">
                                <div class="text-center">
                                    <button type="button" class="prevBtn" onclick="nextPrev(-1)">Back</button>
                                    <button type="submit" value="ACTIVATE MY ACCOUNT"  class="gradient-input">Verify Code</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div style="overflow:auto;">
                    <div class="text-center">
                      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Back</button>
                      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    </div>
                </div>
        </div>
    </div>

    <div class="page-content-start tripper-page calc">
      <div class="container">
        <h1 class="text-center">START EARNING</h1>
        <h3 class="text-center">IN THREE EASY STEPS!</h3>
        <div class="gray-line"></div>
      </div>
    </div>

    <div class="specification-div calc">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{ URL::asset('media/images/list-property1.png') }}" alt="property">
                    </div>
                    <div class="description">
                        <h3>1. LIST YOUR PROPERY</h3>
                        <p>We offer a large selection of property options - from vacation clubs to apartments to condos to homes. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{ URL::asset('media/images/advertise1.png') }}" alt="property">
                    </div>
                    <div class="description">
                        <h3>2. ADVERTISE & PROMOTE</h3>
                        <p>Take advantage of our exclusive in-site advertising opportunites - promote your property easily and efficiently!</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{ URL::asset('media/images/start-earnings.png') }}" alt="property">
                    </div>
                    <div class="description">
                        <h3>3. START EARNING!</h3>
                        <p>Turn your unused property into a source of income! </p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <div class="how-much-earn-btn text-center">
        <a href="{{route('owner.list-your-stay')}}" class="red-btn-lg" onclick="setValues()">SEE HOW MUCH YOU COULD EARN</a>
    </div>
@endsection

@section('footerJs')
    @parent
    <script type="text/javascript">
        var linkUrl = "{{route('owner.home')}}";
        var owner_name = "{{ Session::get('owner') }}";
        var ownerLogout = "{{route('user.logout')}}";
        var addListingUrl = "{{ url('owner_panel/property/add')}}";
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        //Login form change
        $(document).ready(function() {
            $('.login-model-open').on('click',function(){
                $('#login_popup').modal('show');
            });
        });

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("property-tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0 || n == 2 || n == 3) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == 3 ||n == 2) {
                document.getElementById("nextBtn").style.display = "none";
            } else {
                document.getElementById("nextBtn").style.display = "inline";
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
        }

        $.validator.addMethod('matchAddress', function (value, element, param) {
		    return $('#where_is_listing_clone').val().trim() === $(element).val().trim();
        }, 'Please select proper location.');

        var form = jQuery("#owner-panel-form");
        jQuery("#owner-panel-form").validate({
            ignore:":not(:visible)",
            rules: {
                where_is_listing:{
                    required: true,
                    matchAddress:true
                }
            },
            messages: {
                where_is_listing:{
                    required: "Please select your location.",
                }
            },
            errorPlacement: function(error, element) {
                var listing = $('#discountPercentageError');
                if (element.attr('name')=='listing') {
                    $(listing).append(error)
                }
            }
        });

        function nextPrev(n) {
            var listing = $('#where_is_listing').val();
            var no_of_guest = $('#no_of_guest').val();
            var no_of_bedroom = $('#no_of_bedroom').val();
            var no_of_bathrooms = $('#no_of_bathrooms').val();
            if($('#where_is_listing').val() == '') {
                $( '#discountPercentageError' ).css("display","block");
                return false;
            }
            if($('#where_is_listing_clone').val().trim() != $('#where_is_listing').val().trim()) {
                $( '#discountPercentageError' ).css("display","block");
                return false;
            }
            //alert(listing + '-' + no_of_guest + ':' + no_of_bedroom + ':' + no_of_bathrooms);
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("property-tab");
            // Exit the function if any field in the current tab is invalid:

            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function fixStepIndicator(n) {
          // This function removes the "active" class of all steps...
          var i, x = document.getElementsByClassName("step");
          for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
          }
          //... and adds the "active" class on the current step:
          x[n].className += " active";
        }

        function otpForm(id) {
            $('#unique_key').val(id);
            $('.property-tab3').css('display','block');
            $('.otp-modal1').css('display','block');
            $('.popup2').css('display','none');
            $('.create-account-owner').css('display','none');
        }

        function signupForm(id) {
            if(id == 1) {
                $(".popup3").css('display','block');
                $(".property-tab3").css('display','none');
                $(".popup2").css('display','none');
            } else {
                $('.property-tab3').css('display','block');
                $('.popup2').css('display','none');
            }
        }

        jQuery( ".your-property-worth .form h2 a.owner-sign-up" ).click(function() {
          jQuery(".login-modal-owner .otp-modal1").hide();
          jQuery(".your-property-worth .create-account-owner").hide();
          jQuery(".login-modal-owner").show();
          jQuery(".your-property-worth button#nextBtn").html('');
        });

        jQuery( ".your-property-worth .form h2 a.owner-login" ).click(function() {
          jQuery(".your-property-worth .create-account-owner").show();
          jQuery(".your-property-worth button#nextBtn").html('Create Account');
          jQuery(".your-property-worth .login-modal-owner").hide();
        });

        $("#where_is_listing").on('input', function() {
            $('#iframe-map').css('display','block');
        });

        var $input = $("#no_of_bathrooms");
        $input.val(1);

        jQuery('#addbedroom').click(function () {
                if(+jQuery(this).prev().val() + 1 == 30) {
                    $("#addbedroom").addClass("max");
                }
                if(+jQuery(this).prev().val()+ 1 == 2) {
                    $("#bedroom").removeClass("min");
                }
                if (jQuery(this).prev().val() < 30) {
                jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
                }
        });

        jQuery('#addguest').click(function () {
            if(+jQuery(this).prev().val() + 1 == 30) {
                    $("#addguest").addClass("max");
                }
                if(+jQuery(this).prev().val()+ 1 == 2) {
                    $("#sub_guest").removeClass("min");
                }
                if (jQuery(this).prev().val() < 30) {
                jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
                }
        });

        jQuery('#addbath').click(function () {
                if(+jQuery(this).prev().val()+ 1 == 1) {
                    $("#subbath").removeClass("min");
                }
                if(+jQuery(this).prev().val() + 1 == 15) {
                    $("#addbath").addClass("max");
                }
                if (jQuery(this).prev().val() < 15) {
                jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
                }
        });

        jQuery('#sub_guest').click(function () {
            if(+jQuery(this).next().val() - 1 == 29) {
                $("#addguest").removeClass("max");
            }
            if(+jQuery(this).next().val() - 1 == 1) {
                $("#sub_guest").addClass("min");
            }

            if (jQuery(this).next().val() > 1) {
                if (jQuery(this).next().val() > 1) {
                    jQuery(this).next().val(+jQuery(this).next().val() - 1);
                }
            }
        });

        jQuery('#bedroom').click(function () {
                if(+jQuery(this).next().val() - 1 < 30 ) {
                    $("#addbedroom").removeClass("max");
                }
                 if(+jQuery(this).next().val() - 1 == 1) {
                    $("#bedroom").addClass("min");
                }
                if (jQuery(this).next().val() > 1) {
                    if (jQuery(this).next().val() > 1){
                        jQuery(this).next().val(jQuery(this).next().val() - 1);
                    }
                }
        });

        $(".subbath").click(function(){
            if ($(this).hasClass('acrescimo')) {
                $input.val(parseInt($input.val()) + 1);
            }
            else if ($input.val()>=1) {
                $input.val(parseInt($input.val()) - 1);
            }

            if($input.val() == 0) {
                 $("#subbath").addClass("min");
             }

             if($input.val() == 15) {
                 $("#addbath").addClass("max");
             }

             if($input.val() < 15) {
                 $("#addbath").removeClass("max");
             }
        });



        // phone formatting
        const isNumericInput = (event) => {
            const key = event.keyCode;
            return ((key >= 48 && key <= 57) || // Allow number line
                (key >= 96 && key <= 105) // Allow number pad
            );
        };

        const isModifierKey = (event) => {
            const key = event.keyCode;
            return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
                (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
                (key > 36 && key < 41) || // Allow left, up, right, down
                (
                    // Allow Ctrl/Command + A,C,V,X,Z
                    (event.ctrlKey === true || event.metaKey === true) &&
                    (key === 65 || key === 67 || key === 86 || key === 88 || key === 90)
                )
        };

        const enforceFormat = (event) => {
            // Input must be of a valid number format or a modifier key, and not longer than ten digits
            if (!isNumericInput(event) && !isModifierKey(event)) {
                event.preventDefault();
            }
        };

        const formatToPhone = (event) => {
            if (isModifierKey(event)) {
                return;
            }
            // I am lazy and don't like to type things more than once
            const target = event.target;
            const input = event.target.value.replace(/\D/g, '').substring(0, 10); // First ten digits of input only
            const zip = input.substring(0, 3);
            const middle = input.substring(3, 6);
            const last = input.substring(6, 10);

            if (input.length > 6) {
                target.value = `(${zip}) ${middle}-${last}`;
            } else if (input.length > 3) {
                target.value = `(${zip}) ${middle}`;
            } else if (input.length > 0) {
                target.value = `(${zip}`;
            }
        };

        const inputElement = document.getElementById('phone');
        inputElement.addEventListener('keydown', enforceFormat);
        inputElement.addEventListener('keyup', formatToPhone);

        jQuery.validator.addMethod("passwordCheck",
            function (value, element, param) {
                if (this.optional(element)) {
                    return true;
                } else if (!/[A-Z]/.test(value)) {
                    return false;
                } else if (!/[a-z]/.test(value)) {
                    return false;
                } else if (!/[0-9]/.test(value)) {
                    return false;
                } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                    return false;
                }

                return true;
            },
            "Password should contain combination of uppercase, lowercase, digits & special characters"
        );

        $('#ownersignup').validate({
            rules: {
                
                email: {
                    required: '#phone:blank',
                    email:true,
                },
                registerphoneno: {
                    required:'#email:blank',
                    regx:/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
                },
                
            },
            messages: {
                
                email: {
                    required: "Email address is required",
                    email:"Please enter valid email address"
                },
                registerphoneno: {
                required:"Phone number is required",
                },
            }
        });

        $('#ownersignup').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            if(!$form.valid()) return false;
            showLoader();

            $.ajax({
                type: "POST",
                url: "{{ route('createRentalTripperUser') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        if(msg.rentalUser!= null){
                            $(".alert-danger").find("ul").html('');
                            $(".alert-danger").css('display', 'block');
                            $(".alert-danger").find("ul").append('<li>You already have an account.</li>');
                            
                        }else{
                            $(".otp-modal1").css('display','block');
                            $(".create-account-owner").css('display','none');
                        }

                    } else {

                        $(".alert-danger").find("ul").html('');
                        $(".alert-danger").css('display', 'block');
                        $.each(msg.error, function (key, value) {
                            $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                        });
                    }
                }
            });

        });

        $('#validateotp').validate({
            rules: {
                otp_code: {
                    required: true
                },
            }
        });

        $('#validateotp').on('submit', function(e) {
            e.preventDefault();
            var $form = $(this);
            if(!$form.valid()) return false;
            $(".alert-danger").find("ul").html('');
            $(".alert-danger").css('display', 'none');
            showLoader();
            $.ajax({
                type: "POST",
                url: "{{ route('authenticateUserOtp') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    hideLoader();
                    if(msg.error==''){
                        if($.isEmptyObject(msg.error)){
                            $(".property-tab32").css('display','block');
                            $(".otp-modal1").css('display','none');
                        } else {
                            $(".alert-danger").find("ul").html('');
                            $(".alert-danger").css('display', 'block');
                            $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                        }
                    }else{
                        $(".otp-modal1 .alert-danger").find("ul").html('');
                        $(".otp-modal1 .alert-danger").css('display', 'block');
                        $.each(msg.error, function (key, value) {
                            $(".otp-modal1 .alert-danger").find("ul").append('<li>' + value + '</li>');
                        });
                    }
                }
            });
        });

         $('#ownerregister').validate({
            rules: {
                first_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 30
                },
                last_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 30
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password",
                    minlength: 6,
                    maxlength: 20
                },
            },
            messages: {
                name: {
                    required: "Please enter valid First Name"
                },
                last_name: {
                    required: "Please enter valid Last Name"
                },
                password: {
                    required: "Password field is required",
                    equalTo: "Password and Confirm Password field should be same",
                    minlength: "Password should be minimum 6 characters",
                    maxlength: "Password must be maximum 20 characters",
                },
                confirm_password: {
                    required: "Confirm Password field is required",
                    equalTo: "Password and Confirm Password field should be same",
                    //minlength: "Confirm Password should be minimum 8 characters",
                },
            }
        });

        $('#ownerregister').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            if(!$form.valid()) return false;
            showLoader();

            $.ajax({
                type: "POST",
                url: "{{ route('usersignup') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        if(msg.success == true) {
                            nextPrev(-1);
                            $(".popup3").css('display','block');
                            $(".popup2").css('display','none');
                            $('.errorUl').hide();
                            $(".create-account-ownerregister").css('display','none');
                            $('#refreshPart').html(`<h1>LIST YOUR STAY<br>AND START EARNING</h1>
                            <a href="${linkUrl}" class="red-btn">GO TO OWNER DASHBOARD</a>`);
                            $('#sidenav #ownerLogin').text(msg.session.user.first_name);
                            $('#responsive-sidenav #ownerLogin').text(msg.session.user.first_name);
                            var listUrl = "{{route('property.create')}}";
                            var listText = "List a Stay";
                            var settingUrl = "{{url('setting')}}";
                            var logout = "{{route('user.logout')}}";
                            Authorization = "Bearer "+msg.session.rental_token;
                            $( '<a class="dropdown-item " href="'+settingUrl+'">My Account</a>' ).insertAfter( ".active-item-link" );
                            $('.dropdown-menu .extra-bold').text(listText)
                            $('.dropdown-menu .extra-bold').attr('href',listUrl);
                            $('#sidenav .dropdown-menu .dropdown-item').last().remove();
                            $('#responsive-sidenav .dropdown-menu .dropdown-item').last().remove();
                            $('.dropdown-menu').append( '<a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutRental(`'+logout+'`)">Logout </a>' );

                            Swal.fire({
                                icon: "success",
                                title: "Your registration is successful!",
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }

                    } else {

                        $(".alert-danger").find("ul").html('');
                        $(".alert-danger").css('display', 'block');
                        $.each(msg.error, function (key, value) {
                            $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                        });
                    }
                }
            });

        });

        $('#ownerlogin').validate({
             rules: {
                email: {
                    required: true,
                    email:true
                },
                password:{
                    required:true,
                    minlength:6
                },
            },
            messages: {
                email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email"
                },
                password: {
                    required:"Please enter password",
                    minlength:"The password you entered is incorrect."
                }
            }
        });

        $('#ownerlogin').on('submit', function(e) {
            console.log(5);
            e.preventDefault();
            let $form = $(this);
            if(!$form.valid()) return false;
            showLoader();
            $.ajax({
                type: "POST",
                url: "{{route('userlogin')}}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    hideLoader();
                    if($.isEmptyObject(msg.error))
                    {
                        nextPrev(-1);
                      $(".popup3").css('display','block');
                      $(".popup2").css('display','none');
                      //$(".property-tab3").css('display','none');
                      $('#refreshPart').html(`<h1>LIST YOUR STAY<br>AND START EARNING</h1>
                        <a href="${linkUrl}" class="red-btn">GO TO OWNER DASHBOARD</a>`);
                      $('#sidenav #ownerLogin').text(msg.session.user.first_name);
                      $('#responsive-sidenav #ownerLogin').text(msg.session.user.first_name);

                      if(msg.propertyCount > 0){
                          var listUrl = "{{route('owner.property')}}";
                          var listText = "My Listings";
                      }else{
                          var listUrl = "{{route('property.create')}}";
                          var listText = "List a Stay";
                      }
                      var settingUrl = "{{url('setting')}}";
                      var logout = "{{route('user.logout')}}";
                      Authorization = "Bearer "+msg.session.rental_token;
                      $( '<a class="dropdown-item " href="'+settingUrl+'">My Account</a>' ).insertAfter( ".active-item-link" );
                      $('.dropdown-menu .extra-bold').text(listText)
                      $('.dropdown-menu .extra-bold').attr('href',listUrl);
                      $('#sidenav .dropdown-menu .dropdown-item').last().remove();
                      $('#responsive-sidenav .dropdown-menu .dropdown-item').last().remove();
                       $('.dropdown-menu').append( '<a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutRental(`'+logout+'`)">Logout </a>' );
                      Swal.fire({
                          icon: "success",
                          title: "Your login is successful!",
                          showConfirmButton: false,
                          timer: 3000
                      })/* .then(() => {
                        window.location = addListingUrl;
                      }); */
                    } else {
                        setTimeout(function() {
                            $(".alert-light-danger").show();
                            $(".alert-text").html('');
                            $.each( msg.error, function( key, value ) {
                                $(".alert-text").append(value);
                            });
                        }, 1000);

                    }
                }
            });
        });

        $("body").delegate("#resendOtp", "click", function(e){
                e.preventDefault();
                $(".otp-modal1 .alert-danger").find("ul").html('');
                $(".otp-modal1 .alert-danger").css('display', 'none');
                $("#otp").val('');
                showLoader();

                $.ajax({
                    type: "POST",
                    url: "{{ route('resendRenterTripperOtp') }}",
                    dataType: "JSON",
                    data: {
                        id : $('#encryptedId').val()
                    },
                    success: function(msg) {
                        hideLoader();
                        if($.isEmptyObject(msg.error)) {
                            $(".otpmodal").hide();

                            if(msg.success == true){

                                Swal.fire({
                                    icon: 'success',
                                    title: 'A new registration code has been sent to your registered email or phone',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(() => {
                                    $(".some_div").empty();
                                    clearInterval(id);
                                    setCounter();
                                });
                            } else {
                                $('.otpSuccessmodal').hide();
                            }

                        }else{
                            if(msg.error!=''){

                                setTimeout(function() {

                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Could not send One Time Password. Please try again after sometime or contact site administrator',
                                        showConfirmButton: true,
                                    })

                                }, 2000);
                            }
                        }
                    }
                });
            })

    jQuery('#tripper-page-carousal').owlCarousel({
        loop:true,
        margin:30,
        center: true,
        nav:true,
        dots: false,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            },
            640:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

  /*   jQuery(".tripper-banner .banner-comtent a").click(function() {
        jQuery('html,body').animate({
            scrollTop: jQuery("#tripper-reg").offset().top},
        1000);
    }); */

    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0
    })

    function setValues() {
        var min = 10000;
        var max = 100000;
        var ranValues =  Math.random() * (max - min) + min;
        var newvalues = ranValues.toFixed(0);
        $('#replace_value').text(formatter.format(newvalues));
        $('#replace_value1').text(formatter.format(newvalues));
    }

    //back button nevigation
    var perfEntries = performance.getEntriesByType("navigation");

    if (perfEntries[0].type === "back_forward") {
        location.reload(true);
    }

    
</script>
@endsection

@section('headerCss')
    @parent
    <style>
    .prevBtn {
        border: 1px solid #979797 !important;
        width: 160px;
        text-align: center;
        color: #979797;
        font-family: 'fatfrank', sans-serif !important;
        font-size: 16px;
        letter-spacing: 1px;
        border-radius: 20px;
        padding: 8px;
        margin: 20px 20px 0 20px;
    }
    </style>
@endsection
