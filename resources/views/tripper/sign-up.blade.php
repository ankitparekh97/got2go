@extends('layouts.tripperApp')

@section('content')

<div class="tripper-registration tripper-create-account" id="tripper-reg" style="">
    <div class="container">
        <h2>CREATE <span>ACCOUNT</span></h2>

        <p>Sign up to explore all the benefits of being a Tripper</p>

        <form method="POST" id="tripperuserreg" action="">
            @csrf

            <input type="hidden" name="tripper" value="tripper">
        <input type="hidden" name="rentalUserEmailTemp" id="rentalUserEmailTemp" value="{{Session::get('rentalUserEmailTemp')}}">
            <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text">

                </div>
            </div>

            <div class="form-group-custm">
                <span>
                <svg xmlns="http://www.w3.org/2000/svg" width="16.732" height="20.92" viewBox="0 0 25.732 26.92">
                    <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                        <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                        <g id="Group_406" data-name="Group 406" transform="translate(0)">
                            <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#3D87CB"></path>
                        </g>
                        </g>
                        <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                        <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                            <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#3D87CB"></circle>
                        </g>
                        </g>
                    </g>
                    </svg>
                </span>
                <input class="form-class-cstm" type="text" name="first_name" id="first_name" placeholder="First Name" autocomplete="off" />
            </div>

            <div class="form-group-custm">
                <span>
                <svg xmlns="http://www.w3.org/2000/svg" width="16.732" height="20.92" viewBox="0 0 25.732 26.92">
                    <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                        <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                        <g id="Group_406" data-name="Group 406" transform="translate(0)">
                            <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#3D87CB"></path>
                        </g>
                        </g>
                        <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                        <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                            <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#3D87CB"></circle>
                        </g>
                        </g>
                    </g>
                    </svg>
                </span>
                <input class="form-class-cstm" type="text" name="last_name" id="last_name" placeholder="Last Name" autocomplete="off" />
            </div>

            <div class="form-group-custm">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="15.803" height="20.998" viewBox="0 0 19.803 25.998">
                        <g id="lock" transform="translate(-61)">
                            <g id="Group_401" data-name="Group 401" transform="translate(69.378 15.284)">
                            <g id="Group_400" data-name="Group 400">
                                <path id="Path_2466" data-name="Path 2466" d="M228.4,303.769a1.523,1.523,0,1,0-1.752,0,.762.762,0,0,1,.295.828l-.7,2.5H228.8l-.7-2.5A.762.762,0,0,1,228.4,303.769Z" transform="translate(-226 -301)" fill="#3D87CB"/>
                            </g>
                            </g>
                            <g id="Group_403" data-name="Group 403" transform="translate(61 10.714)">
                            <g id="Group_402" data-name="Group 402">
                                <path id="Path_2467" data-name="Path 2467" d="M78.518,211H63.285A2.287,2.287,0,0,0,61,213.285v10.663a2.331,2.331,0,0,0,2.285,2.336H78.518a2.331,2.331,0,0,0,2.285-2.336V213.285A2.287,2.287,0,0,0,78.518,211Zm-4.6,11.22a.761.761,0,0,1-.733.967h-4.57a.761.761,0,0,1-.733-.967l.84-3a3.047,3.047,0,1,1,4.357,0Z" transform="translate(-61 -211)" fill="#3D87CB"/>
                            </g>
                            </g>
                            <g id="Group_405" data-name="Group 405" transform="translate(64.047)">
                            <g id="Group_404" data-name="Group 404">
                                <path id="Path_2468" data-name="Path 2468" d="M127.855,0A6.855,6.855,0,0,0,121,6.855V9.191h3.047V6.855a3.808,3.808,0,0,1,7.617,0V9.191h3.047V6.855A6.855,6.855,0,0,0,127.855,0Z" transform="translate(-121)" fill="#3D87CB"/>
                            </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <input class="form-class-cstm tripperUserPassword" type="password" name="password" id="Password" placeholder="Password" passwordCheck="passwordCheck" autocomplete="off" />
                <span toggle="#password-field" class="fa fa-fw field-icon tripperPassword toggle-password-tripper fa-eye"></span>
                <div class="tripperPasswordError"></div>
            </div>

             <div class="text-center">
                <div class="form-group-custm">
                    <input class="form-class-cstm" type="submit" value="NEXT" />
                </div>
            </div>

        </form>
    </div>
</div>
@endsection
@section('footerJs')
    @parent
<script>

$(document).ready(function() {

    $(".toggle-password-tripper").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $(".tripperUserPassword");

        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    jQuery.validator.addMethod("passwordCheck",
        function(value, element, param) {
            if (this.optional(element)) {
                return true;
            } else if (!/[A-Z]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            } else if (!/[0-9]/.test(value)) {
                return false;
            } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                return false;
            }

            return true;
        }, "This password needs to have at least 6 characters with 1 uppercase letter, 1 number, and 1 special character."
    );

    $('#tripperuserreg').validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2,
                maxlength: 30
            },
            last_name: {
                required: true,
                minlength: 2,
                maxlength: 30
            },
            password:{
                required:true,
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {
            first_name: {
                required: "Please enter valid First Name"
            },
            last_name: {
                required: "Please enter valid Last Name"
            },
            password: {
                required:"Please enter in password.",
                minlength: "Password should be minimum 6 characters",
                maxlength: "Password must be maximum 20 characters",
            }
        },
        errorPlacement: function(error,element){
            var password = $('.tripperPasswordError');

            if (element.attr('id')=='Password') {
                $(password).append(error)
            } else  {
                error.insertAfter(element);
            }
        }
    });

    // sign up form
    $('#tripperuserreg').on('submit', function(e) {
        e.preventDefault();
        let $form = $(this);

        if(!$form.valid()) return false;

        showLoader();

        $.ajax({
            type: "POST",
            url: "{{ route('usersignup') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                hideLoader();

                if($.isEmptyObject(msg.error)){
                    //  window.onbeforeunload = null;
                     location.href = 'sign-up';

                } else {

                    setTimeout(function() {
                        $(".regmodal").show();
                        $.each( msg.error, function( key, value ) {
                            $(".regmodal .alert-text").empty().html(value);
                        });
                    })
                }

            }
        });
    });


});

</script>

@endsection
