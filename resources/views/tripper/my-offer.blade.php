
@extends('layouts.rentalApp')
@section('content')

    <div class="my-account-dashboard">
        @include('rentaluser.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column pt-20">
            <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
                <i class="fas fa-bars"></i>
            </button>
            <!-- Main Content -->
            <div id="content">
                <div class="container-fluid my-acc-dashbrd">
                    <!-- Start Page Heading -->
                    <div class="dashboard-title-wrapper">
                        <h2 class="dashboard-title">MY <strong>Offers</strong></h2>
                        <h4 class="d-mobile totalOfferCount"> 4 Offers </h4>
                    </div>
                    <!-- End Page Heading -->

                    <form class="dashboard-forms tripper-dboard-form">
                        <div class="row">
                            <div class="col-xl-12 ">
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <div class="my-offer-wrapper">
                                            <div class="myoffer-pageheader d-desktop ">
                                                <h4 class="totalOfferCount"> 0 Offers </h4>
                                            </div>
                                            <div class="myoffer-body" id="my-offer-body">
                                                <div class="row" id="my-offer-body-container-row">
                                                </div>
                                                <input type="hidden" name="offerTotalCount" id="offerTotalCount" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Make An New Offer Modal -->
    <div class="modal fade custom_modal" id="makeAnNewOffer" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <h1 class="modal-title"> Make a New Offer</h1>
                    <form name="makeAnNewOfferForm" id="makeAnNewOfferForm" method="post">
                        <div class="make-offer-body align-items-center w-10  vertical-middle ">
                            <div class="date-atcion">
                                <span class="switch cust-switch ">
                                    <label>
                                        <input type="checkbox" id="flexibleDates" name="flexibleDates" />
                                        <span></span>
                                        <em class="switch-txt flexibleDates-text">Dates are flexible</em>
                                    </label>
                                </span>
                            </div>

                            <div class="error-label d-none">
                                The dates you selected are not available.
                            </div>

                            <div class="row mb-4 offer-date-label ">
                                <div class="col-lg-5">
                                    <label class="list-label d-desktop">DATE</label>
                                </div>
                                <div class="col-lg-7">
                                    <ul class="list-unstyled offer-table-list">
                                        <li><label class="list-label d-desktop">Nights</label></li>
                                        <li><label class="list-label d-desktop">$/Nights</label></li>
                                        <li><label class="list-label d-desktop">Total</label></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row d-flex align-items-center mb-8">
                                <div class="col-lg-5">
                                    <div class="select-date-wrap">
                                        <ul class="list-unstyled">
                                            <li>
                                                <label class="list-label mb-6 d-mobile" for="offerStartDate">CHECK-IN DATE</label>
                                                <input type="text" class="form-control mydatepicker" value="MM-DD-YY" id="offerStartDate" name="offerStartDate" />
                                            </li>
                                            <li class="separetor">to</li>
                                            <li>
                                                <label class="list-label mb-6 d-mobile" for="offerEndDate">CHECK-OUT DATE</label>
                                                <input type="text" class="form-control mydatepicker" value="MM-DD-YY" id="offerEndDate" name="offerEndDate" />
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <ul class="list-unstyled offer-table-list">
                                        <li>
                                            <label class="list-label d-mobile">Nights</label>
                                            <span class="text-bold" id="numberOfNights"></span>
                                        </li>
                                        <li>
                                            <label class="list-label d-mobile">$/Nights</label>
                                            <span class="text-bold" id="standardNightlyPrice">250</span>
                                        </li>
                                        <li>
                                            <label class="list-label d-mobile">$/Nights</label>
                                            <span class="text-bold" id="totalPriceWithDateRange">500</span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="proposed-wrapper">
                                        <div class="proposed-title">Proposed Total</div>
                                        <div class="proposed-input-wrap">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">$</div>
                                                </div>
                                                <input type="text" class="form-control" id="offeredProposedTotalPrice" name="offeredProposedTotalPrice" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex flex-column justify-content-center border-0">
                                <button type="button" id="makeAndOfferModelSubmitOfferButton" onclick="TripperOffer.createMakeAnOffer();" class="button-default-animate button-default-custom btn-sunset-gradient btn-wd mt-5">Submit New Offer</button>
                                <button type="button" id="#" class="btn btn-800 btn-link mt-5" data-dismiss="modal">Cancel</button>
                            </div>

                            <input type="hidden" name="ownerId" id="makeAnOfferOwnerId" />
                            <input type="hidden" name="rentalId" id="makeAnOfferRentalId" />
                            <input type="hidden" name="propertyId" id="makeAnOfferPropertyId" />
                            <input type="hidden" name="propertyOfferId" id="makeAnOfferPropertyOfferId" />
                            <input type="hidden" name="propertyOfferStartDate" id="makeAnOfferPropertyOfferStartDate" />
                            <input type="hidden" name="propertyOfferEndDate" id="makeAnOfferPropertyOfferEndDate" />
                            <input type="hidden" name="propertyStandardPrice" id="makeAnOfferStandardPrice" />
                            <input type="hidden" name="propertyNumberOfGuests" id="makeAnOfferpropertyNumberOfGuests" />
                            <input type="hidden" name="propertyMinimumOffer" id="makeAnOfferPropertyMinimumOffer" />
                            <input type="hidden" name="propertyActualNightlyPrice" id="makeAnOfferPropertyActualNightlyPrice" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Make An New Offer Modal -->

    <div class="modal fade custom_modal" id="makeCounterOffer" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <h1 class="modal-title"> Make a New Offer</h1>
                    <form id="makeACounterOfferForm" name="makeACounterOfferForm" method="post" >
                        <div class="make-offer-body align-items-center w-10  vertical-middle ">
                            <div class="date-atcion">
                                <span class="switch cust-switch ">
                                    <label>
                                        <input type="checkbox" name="select" id="flexibleDates" name="flexibleDates" />
                                        <span></span>
                                        <em class="switch-txt flexibleDates-text">Dates are flexible</em>
                                    </label>
                                </span>
                            </div>

                            <div class="error-label d-none">
                                The dates you selected are not available.
                            </div>

                            <div class="row mb-4 offer-date-label ">
                                <div class="col-lg-5">
                                    <label class="list-label d-desktop">DATE</label>
                                </div>
                                <div class="col-lg-7">
                                    <ul class="list-unstyled offer-table-list">
                                        <li><label class="list-label d-desktop">Nights</label></li>
                                        <li><label class="list-label d-desktop">$/Nights</label></li>
                                        <li><label class="list-label d-desktop">Total</label></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row d-flex align-items-center mb-8">
                                <div class="col-lg-5">
                                    <div class="select-date-wrap">
                                        <ul class="list-unstyled">
                                            <li class="datepicker-li">
                                                <label class="list-label mb-6 d-mobile" for="offerStartDate">CHECK-IN DATE</label>
                                                <input type="text" class="form-control mydatepicker" value="MM-DD-YY" id="offerStartDate" name="offerStartDate" />
                                            </li>
                                            <li class="separetor">to</li>
                                            <li class="datepicker-li">
                                                <label class="list-label mb-6 d-mobile" for="offerEndDate">CHECK-OUT DATE</label>
                                                <input type="text" class="form-control mydatepicker" value="MM-DD-YY" id="offerEndDate" name="offerEndDate" />
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <ul class="list-unstyled offer-table-list">
                                        <li>
                                            <label class="list-label d-mobile">Nights</label>
                                            <span class="text-bold" id="numberOfNights"></span>
                                        </li>
                                        <li>
                                            <label class="list-label d-mobile">$/Nights</label>
                                            <span class="text-bold" id="standardNightlyPrice">250</span>
                                        </li>
                                        <li>
                                            <label class="list-label d-mobile">$/Nights</label>
                                            <span class="text-bold" id="totalPriceWithDateRange">500</span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="proposed-wrapper">
                                        <div class="proposed-title">Proposed Total</div>
                                        <div class="proposed-input-wrap">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">$</div>
                                                </div>
                                                <input type="text" class="form-control" id="offeredProposedTotalPrice" name="offeredProposedTotalPrice" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex flex-column justify-content-center border-0">
                                <button type="button" id="makeAndOfferModelSubmitOfferButton" onclick="TripperOffer.createCounterOffer();" class="button-default-animate button-default-custom btn-sunset-gradient btn-wd mt-5">Submit New Offer</button>
                                <button type="button" id="#" class="btn btn-800 btn-link mt-5" data-dismiss="modal">Cancel</button>
                            </div>

                            <input type="hidden" name="ownerId" id="makeAnOfferOwnerId" />
                            <input type="hidden" name="rentalId" id="makeAnOfferRentalId" />
                            <input type="hidden" name="propertyId" id="makeAnOfferPropertyId" />
                            <input type="hidden" name="propertyOfferId" id="makeAnOfferPropertyOfferId" />
                            <input type="hidden" name="propertyOfferStartDate" id="makeAnOfferPropertyOfferStartDate" />
                            <input type="hidden" name="propertyOfferEndDate" id="makeAnOfferPropertyOfferEndDate" />
                            <input type="hidden" name="propertyStandardPrice" id="makeAnOfferStandardPrice" />
                            <input type="hidden" name="propertyMinimumOffer" id="makeAnOfferPropertyMinimumOffer" />
                            <input type="hidden" name="propertyActualNightlyPrice" id="makeAnOfferPropertyActualNightlyPrice" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Make An New Offer Modal -->

    <!-- Delete Acceptance -->
    <div class="modal fade cmn-popup custom_modal approve" id="deleteOfferPopup" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1 class="modal-title">Are you sure you want to delete this?</h1>
                    <div class="make-offer-body">
                        <p class="pera-modal text-center">
                            Deleting this offer card will remove the offer card and delete your negotiation with the owner of the listing permanently.
                        </p>
                    </div>
                    <div class="text-center delete-btns modal-footer d-flex flex-column justify-content-center border-0 ">
                        <button type="button" class="  red-btn-cmn approveBooking button-default-animate button-default-custom btn-sunset-gradient btn-wd mt-5" onclick="TripperOffer.deleteOffer()">Yes, delete this offer card</button><br>
                        <button type="button" class="btn btn-800 btn-link mt-5 close-btn-cmn" data-dismiss="modal">No, don't delete this offer card</button>
                    </div>
                    <input type="hidden" name="ownerId" id="deleteOfferPopupOwnerId" />
                    <input type="hidden" name="rentalId" id="deleteOfferRentalId" />
                    <input type="hidden" name="propertyId" id="deleteOfferPropertyId" />
                    <input type="hidden" name="propertyOfferId" id="deleteOfferPopupPropertyOfferId" />
                </div>
            </div>
        </div>
    </div>
    <!-- End of Delete Acceptance -->

    <!-- Cancel Acceptance -->
    <div class="modal fade cmn-popup custom_modal approve" id="cancelAcceptedOfferPopup" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1 class="modal-title">Are you sure you want to cancel this negotiation?</h1>
                    <div class="make-offer-body">
                        <p class="pera-modal text-center">
                            Cancelling this negotiation will remove offer history and your negotiation with the owner of the listing permanetly.
                        </p>
                    </div>
                    <div class="text-center delete-btns modal-footer d-flex flex-column justify-content-center border-0 ">
                        <button type="button" class="  red-btn-cmn approveBooking button-default-animate button-default-custom btn-sunset-gradient btn-wd mt-5" onclick="TripperOffer.cancelOffer()">Yes, cancel this negotiation</button><br>
                        <button type="button" class="btn btn-800 btn-link mt-5 close-btn-cmn" data-dismiss="modal">No, don't cancel this negotiation</button>
                    </div>
                    <input type="hidden" name="ownerId" id="cancelAcceptedOfferPopupOwnerId" />
                    <input type="hidden" name="rentalId" id="cancelAcceptedOfferRentalId" />
                    <input type="hidden" name="propertyId" id="cancelAcceptedOfferPropertyId" />
                    <input type="hidden" name="propertyOfferId" id="cancelAcceptedOfferPopupPropertyOfferId" />
                </div>
            </div>
        </div>
    </div>
    <!-- End of Cancel Acceptance -->

@endsection

@section('footerJs')
    @parent
    <script>
        const getRentalPropertyOffersURL = '{{ route('api.tripper.propertyOffers.get',['tripperId' => getLoggedInRentalId() ]) }}';
        const getPropertyOfferByIdURL = '{{ route('api.tripper.propertyOffers.getByOfferId',['tripperId' => getLoggedInRentalId(),'propertyOfferId' => '#propertyOfferId' ]) }}';
        const counterOfferPropertyURL = '{{ route('api.tripper.propertyOffers.counterPropertyOffer',['tripperId' => getLoggedInRentalId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const makeAnPropertyOfferURL = '{{ route('api.tripper.propertyOffers.makeAnNewOfferOnExistingOffer',['tripperId' => getLoggedInRentalId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId' ]) }}';
        const getRentalCancelPropertyOfferURL = '{{ route('api.tripper.propertyOffers.cancelPropertyOffer',['tripperId' => getLoggedInRentalId(), 'propertyId' => '#propertyId', 'propertyOfferId' => '#propertyOfferId']) }}';
        const getRentalDeletePropertyOfferURL = '{{ route('api.tripper.propertyOffers.deleteAnPropertyOffer',['tripperId' => getLoggedInRentalId(), 'propertyId' => '#propertyId']) }}';
        const getRedirectToReviewPageURL = '{{ route('review.index') }}?propertyOfferId=#propertyOfferId';
        const getPropertyAvailabilityCheckURL = '{{ route('api.property.checkAvailabilityWithCheckinCheckOutDate',[ 'propertyId' => '#propertyId', 'checkInDate' => '#checkInDate', 'checkOutDate' => '#checkOutDate']) }}';
        const ownerId = '{{ getLoggedInRentalId() }}';
        const getPropertyWebURL = '{{ route('propertydetails.index',['id' => '#propertyId' ]) }}';
    </script>
    <script src="{{ URL::asset('js/tripper/my-offer.js?v=1.1') }}"></script>
@endsection
