@extends('layouts.tripperApp')

@section('content')
<script>

</script>

<div class="page-content-start tripper-page registration no-padding">

    <div class="tripper-registration" id="tripper-reg">
        <div class="container">
            <h2>TRIPPER <span>REGISTRATION</span></h2>
            <p>Please enter in the confirmation code you received to the inbox of the email address provided.</p>

            <form method="POST" id="tripperOtp" action="{{ route('authenticateUserOtp') }}">
                @csrf
                <input type="hidden" name="tripperUser" id="tripperUser" value="tripperUser">

                @if ($message = Session::get('error'))
                <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                    <div class="alert-icon">
                        <i class="flaticon-warning"></i>
                    </div>
                    <div class="alert-text">
                        {{$message}}
                    </div>
                </div>
                @endif

                <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                    <div class="alert-icon">
                        <i class="flaticon-warning"></i>
                    </div>
                    <div class="alert-text">

                    </div>
                </div>

                <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 successUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                    <div class="alert-icon">
                        <i class="flaticon-warning"></i>
                    </div>
                    <div class="alert-text successOTP" style="font-weight: bold;"> New One Time Password has been sent to your registered email</div>
                </div>

                <div class="form-group-custm no-icon">
                    <input type="text" class="form-class-cstm" name="otp_code" id="otp_code" placeholder="Registration Code" autocomplete="off" maxlength="5" oninput="numberOnly(this.id);" />
                    <span class="tripperResend">Didn't receive one-time password? <a href="#" id="resendTripperOtp" style="font-weight: bold;
                        text-decoration: underline;">Resend again</a></span>
                </div>

                <div class="text-center">
                    <div class="form-group-custm">
                        <input class="form-class-cstm" type="submit" value="NEXT" />
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
@section('footerJs')
    @parent
<script>
$( document ).ready(function() {
    $('#tripperOtp').validate({
        rules: {
            otp_code: {
                required: true,
                number : true,
                minlength:5,
            },
        },
        messages: {
            otp_code: {
                required: 'Enter valid One Time Password code',
                number : 'Enter valid One Time Password code',
                minlength : 'Enter valid One Time Password code'
            },
        }
    });

    $('#tripperOtp').on('submit', function(e) {
        e.preventDefault();
        var $form = $(this);

        if(!$form.valid()) {
            return false;
        }

        showLoader();

        $.ajax({
            type: "POST",
            url: "{{ route('authenticateUserOtp') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                $('.successUI').hide();
                if($.isEmptyObject(msg.error))
                {
                    if(msg.success == true){

                        setTimeout(function() {
                            hideLoader();

                            $('.errorUl').hide();
                            location.href = 'register';
                        }, 2000);

                    } else {
                        setTimeout(function() {
                            hideLoader();;

                            $('.errorUl').show();
                        }, 2000);
                    }
                }else{
                    if(msg.error!=''){
                        setTimeout(function() {
                            hideLoader();;

                            $(".alert-light-danger").show();
                            $(".alert-text").html('');
                            $.each( msg.error, function( key, value ) {
                                $(".alert-text").html(value);
                            });
                        }, 2000);
                    }
                }
            }
        });
    });

    // resend OTP
    $('#resendTripperOtp').click(function (e){
        e.preventDefault();

        showLoader();

        $.ajax({
            type: "POST",
            url: "{{ route('resendRenterTripperOtp') }}",
            dataType: "JSON",
            data: {
                id : $('#id').val()
            },
            success: function(msg) {

                if($.isEmptyObject(msg.error)) {
                    setTimeout(function() {
                        hideLoader();
                        $(".alert-light-danger").hide();

                        if(msg.success == true){
                            $('.successUI').show();
                            $(".alert-text").html('New One Time Password has been sent to your registered email');
                        } else {
                            $('.successUI').hide();
                        }

                    }, 2000);

                }else{
                    if(msg.error!=''){

                        setTimeout(function() {
                            hideLoader();

                            $(".alert-light-danger").show();
                            $(".alert-text").html('');
                            $(".alert-text").html('Could not send One Time Password. Please try again after sometime or contact site administrator');

                        }, 2000);
                    }
                }
            }
        });
    })
});

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}
</script>

@endsection
