@extends('layouts.tripperApp')

@section('content')

<div class="tripper-registration tripper-registration-box" id="tripper-reg">
    <div class="container">
        <h2>TRIPPER <span>REGISTRATION</span></h2>

        @if(!Auth::guard('rentaluser')->user())
        <div class="social-login-tripper">
            <span><a href="{{ url('auth/tripperuser/google') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/google.png') }}" alt="google" /></a></span>
            <span><a href="{{ url('auth/tripperuser/facebook') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/favebook-login-btn.png') }}" alt="facebook" /></a></span>
        </div>

        <p>Sign up to explore all the benefits of being a Tripper</p>
        @endif

        <form method="POST" id="tripperusersignup" action="">
            @csrf

            <input type="hidden" name="tripper" value="tripper">

            <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text">

                </div>
            </div>
            @if(!Auth::guard('rentaluser')->user())
                <div class="form-group-custm">
                    <span><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-mail-icon.png') }}" alt="mail icon" /></span>
                    <input class="form-class-cstm" type="text" name="email" id="email" placeholder="Email Address" autocomplete="off" />
                </div>
                {{-- <div class="input-group">
                    <div class="text-center recaptcha">
                        <div class="g-recaptcha" id="tripperRecaptcha" data-sitekey="{{ env('RECAPTCHA') }}" data-callback="recaptchaTripperCallback"></div>
                        <input type="hidden" class="hiddenRecaptcha required" name="tripperhiddenRecaptcha" id="tripperhiddenRecaptcha">
                    </div>
                    <script src='https://www.google.com/recaptcha/api.js?onload=tripperCaptchaCallback&render=explicit'></script>
                </div> --}}
                <div class="text-center">
                    <div class="form-group-custm">
                        <input class="form-class-cstm" type="submit" value="SIGN UP" />
                    </div>
                </div>
            @else
                  <input type="hidden" name="userId2" id="userId" value="{{ encrypt(Auth::guard('rentaluser')->user()->id)}}" />
                <div class="text-center">
                    <div class="form-group-custm">
                        <input class="form-class-cstm" type="submit" value="GET STARTED" />
                    </div>
                </div>
            @endif
        </form>
    </div>
</div>

@endsection

@section('footerJs')
    @parent
    <script type="application/javascript">
        @if(!empty(getLoggedInRental()) && isUserIsSubscribed())
            $('#tripperusersignup').attr('action', "{{ route('tripper.dashboard') }}");
        @else
            $('#tripperusersignup').attr('action', "{{ route('tripper.singUpForm') }}");
        @endif
    </script>
    <script type="application/javascript">

        @if(!Auth::guard('rentaluser')->user())

        var tripperCaptchaCallback = function() {
            var widgetId1;
            widgetId1 = grecaptcha.render('tripperRecaptcha', {'sitekey' : "{{env('RECAPTCHA')}}", 'callback' : recaptchaTripperCallback});
        };

        var recaptchaTripperCallback = function(response) {
            $("#tripperhiddenRecaptcha").val(response);
        };

        $('#tripperusersignup').validate({
            ignore: ".ignore",
            rules: {
                email: {
                    required: true,
                    email:true,
                    minlength:3
                },
                tripperRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                email: {
                    required: "Please enter in a valid email address",
                    email:"Please enter in a valid email address"
                },
            }
        });

        $('#tripperusersignup').on('submit', function(e) {

            e.preventDefault();
            let $form = $(this);
            if(!$form.valid()) {
                return false;
            }

            showLoader();

            $.ajax({
                type: "POST",
                url: "{{ route('createRentalTripperUser') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    if($.isEmptyObject(msg.error)){
                        hideLoader();

                        if(msg.rentalUser != null){
                            $(".alert-light-danger").show();
                            $(".alert-text").empty().html('You already have an account. Please <a href="" class="font-light loginPopUp" data-toggle="modal" data-target="#login_popup">log in </a>?');
                        } else {
                            // window.onbeforeunload = null;
                            location.href = 'activation';
                            return false;
                        }

                    } else {
                        setTimeout(function() {
                            hideLoader();
                            $(".alert-light-danger").show();
                            $(".alert-text").html('');
                            $.each( msg.error, function( key, value ) {
                                $(".alert-text").html(value);
                            });
                        }, 2000);
                    }
                }
            });
        });
        @endif

    </script>
@endsection
