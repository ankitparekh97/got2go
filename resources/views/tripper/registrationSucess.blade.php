@extends('layouts.tripperApp')

@section('content')

<div class="tripper-registration success" id="tripper-reg">
    <div class="container">
        <div class="inner">
            <div class="bg-gig">
                <iframe src="https://giphy.com/embed/5T06ftQWtCMy0XFaaI" width="480" height="296" class="giphy-embed" allowFullScreen></iframe>
            </div>
            <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-success.png')}}" alt="tripper success">
            <p>View your exclusive deals and start earning Tripper points on every single stay.</p>
            <a href="{{route('tripper.dashboard')}}">Go to dashboard</a>
        </div>
    </div>
</div>

@endsection
