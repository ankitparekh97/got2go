@extends('layouts.tripperApp')

@section('content')
<div class="tripper-registration" id="tripper-reg">
    <div class="tripper-container">
        <span class="display-block text-center strike text"><sup class="small">$</sup><s>{{ Helper::formatMoney($subscription->strike_through_amount,1,false) }} ANNUAL MEMBERSHIP</s></span>
        <h1 class="text-center"><sup class="small">$</sup>{{$subscription->annual_subscription? Helper::formatMoney($subscription->annual_subscription,1,false):0}} for {{ carbonGetCurrentYear() }}</h1>
        <div class="flight-image text-center">
            <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/flight-image.png')}}" alt="flight">
        </div>

        <div class="image-below-desc extra">
            <p>Trippers Only®, the only rewards program with exclusive access to premium listings, first in line offers on availability, and no point expiration.</p>
             @if(\Session::get('user')=='')
            <p>Already a Tripper? Log in <a href="" data-toggle="modal" data-target="#login_popup">here.</a></p>
             @endif
        </div>
        <div class="image-title">
            <h1><strong>TRIPPER</strong> REGISTRATION</h1>
        </div>

        <form method="POST" id="tripperSignUp" action="{{ route('tripperRegister') }}">

            @csrf
            <input type="hidden" name="id" id="id" value="{{$id }}">
            <input type="hidden" name="payloadNonce" id="payloadNonce" >
            <input type="hidden" name="is_tripper" id="1" >

            <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                <div class="alert-icon">
                    <i class="flaticon-warning"></i>
                </div>
                <div class="alert-text"></div>
            </div>

            <div class="tripper-registration-box">
                <p class="required text-left">*Required</p>
                <h3>My <strong>Credentials</strong></h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group-custm no-icon">
                        <input class="form-class-cstm disabled" disabled type="text" value="{{$user->first_name}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group-custm no-icon">
                            <input class="form-class-cstm disabled" disabled type="text" value="{{$user->last_name}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group-custm no-icon">
                            <input class="form-class-cstm no-icon" type="text" name="phoneNumber" id="phoneNumber" maxlength="14" placeholder="Phone Number" value="{{$user->phone}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="custm-birthdate-outer" style="margin-top: 15px;">
                            <div class="input-group input-group-solid date" id="kt_datetimepicker_3" data-target-input="nearest">
                                <input type="text" name="birthdate" id="birthdate" class="form-control custm-birthdate form-control-solid datetimepicker-input" data-target="#kt_datetimepicker_3"/>
                                <div class="input-group-append" data-target="#kt_datetimepicker_3" data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="errorbirthdate"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tripper-registration-box">


                <p class="required text-left">*Required</p>
                <h3>My <strong>Billing</strong></h3>
                    <div class="row">
                        <label id="card-errors" class="error">
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group-custm no-icon card" id="cardNumber">
                            </div>
                            <span class="card_icon"></span>
                            <label class="error" id="numberError" style="display: none;">
                                Please enter in a valid credit card number.
                            </label>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group-custm no-icon" id="expirationDate">
                            </div>
                            <label class="error" id="expirationDateError" style="display: none;">
                                Please enter in a valid expiration date.
                            </label>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group-custm no-icon" id="cvv">
                            </div>
                            <label class="error" id="cvvError" style="display: none;">
                                Please enter in a valid security code.
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group-custm no-icon">
                                <input class="form-class-cstm" type="text" name="nameOnCard" id="nameOnCard" placeholder="Name on card" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group-custm no-icon">
                                <input class="form-class-cstm" type="text" name="address" id="address" placeholder="Billing Street Address Line 1" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group-custm no-icon">
                                <input class="form-class-cstm" type="text" name="address2" id="address2" placeholder="Billing Street Address Line 2 (optional)" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group-custm no-icon">
                                <input class="form-class-cstm" type="text" name="city" id="city" placeholder="City" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group-custm no-icon">
                                <select class="form-class-cstm" name="state" id="state">
                                        <option value="">- Select -</option>
                                        @foreach(getStatesWithAbbr() as $abbr => $state)
                                            <option value="{{ $abbr }}">{{ $state }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <h4>Your card will not be billed until you select @if(Session::get('wasRecentlyCreated')) "Create Account" @else "Become A Tripper"  @endif at the bottom of the screen. </h4>

            </div>

            <div class="text-center tripper-create-account">
                @if(Session::get('wasRecentlyCreated'))
                    <input type="submit" value="Create Account">
                @else
                    <input type="submit" value="Become A Tripper">
                @endif
                <span class="become-tripper-prgph-text">Your card will be billed {{ \App\Helpers\Helper::formatMoney($subscription->annual_subscription,1) }}  after creating your account, and you will become a Tripper by clicking the button above. An annual fee of {{ \App\Helpers\Helper::formatMoney($subscription->annual_subscription,1) }} will be automatically charged each year following your initial year of membership.</span>
            </div>

        </form>
    </div>
</div>

   <select style="display:none;" id="monthSelect" name="dwfrm_billing_paymentMethods_creditCard_expiration_month" >
        <option class="select-option" label="Month" value="" selected="selected">Month</option>
        <option class="select-option" label="01" value="01">01</option>
        <option class="select-option" label="02" value="02">02</option>
        <option class="select-option" label="03" value="03">03</option>
        <option class="select-option" label="04" value="04">04</option>
        <option class="select-option" label="05" value="05">05</option>
        <option class="select-option" label="06" value="06">06</option>
        <option class="select-option" label="07" value="07">07</option>
        <option class="select-option" label="08" value="08">08</option>
        <option class="select-option" label="09" value="09">09</option>
        <option class="select-option" label="10" value="10">10</option>
        <option class="select-option" label="11" value="11">11</option>
        <option class="select-option" label="12" value="12">12</option>
    </select>

    <select style="display:none;" class="input-select year" id="yearSelect" aria-required="true" aria-describedby="dwfrm_billing_paymentMethods_creditCard_expiration_year-error" aria-invalid="false">
        <option class="select-option" label="Year" value="" selected="selected">Year</option>
        <option class="select-option" label="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}" value="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}">{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}</option>
    </select>
@endsection

@section('footerJs')
    @parent

    @if($user->is_verified == '1')
        <script>
            function preventBack() {
                window.history.forward();
            }

            setTimeout("preventBack()", 0);
            window.onunload = function () { null };
        </script>
    @endif

    <script src="{{URL::asset('js/cardcheck.js')}}"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        // min age validator
        $.validator.addMethod("minAge", function(value, element, min) {
            var today = new Date();
            var birthDate = new Date(value);
            var age = today.getFullYear() - birthDate.getFullYear();

            if (age > min+1) {
                return true;
            }

            var m = today.getMonth() - birthDate.getMonth();

            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            return age >= min;
        }, "You are not old enough!");

        $.validator.addMethod("notValidSpecialChar", function(value, element) {
            return this.optional(element) || HelperFunction.isValidString(value);
        });

        var tripperSignUp = jQuery('#tripperSignUp');

        const baseURL = '{{ URL::to('') }}';

        $( document ).ready(function() {

            // If JavaScript is enabled, hide fallback select field
            $('.no-js').removeClass('no-js').addClass('js');

            $("#birthdate").mask("00/00/0000", {placeholder: "Enter in your date of birth MM/DD/YYYY"})
            $('#phoneNumber').mask('(000) 000-0000',{placeholder: "Phone number (000) 000-0000"});

            jQuery('#kt_datetimepicker_3').datetimepicker({
                format: 'MM/DD/YYYY',
                timepicker: false,
                viewMode: 'years',
                icons: {
                    up: "fa fa-chevron-circle-up",
                    down: "fa fa-chevron-circle-down",
                    next: 'fa fa-chevron-circle-right',
                    previous: 'fa fa-chevron-circle-left'
                }
            });

            jQuery.validator.addMethod("lettersOnly", function(value, element){
                return this.optional(element) || /^[a-z ]+$/i.test(value);
            }, "Letters and spaces only please");
            jQuery.validator.addMethod("regx", function(value, element, regexpr) {
                return this.optional(element) || regexpr.test(value);
            }, "Please enter valid phone number.");
            tripperSignUp.validate({
                rules: {
                    phoneNumber: {
                        required: true,
                        regx:/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
                    },
                    birthdate: {
                        required: true,
                        minAge: 18
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true,
                        notValidSpecialChar: true
                    },
                    state: {
                        required: true
                    },
                    nameOnCard: {
                        required: true,
                        lettersOnly: true,
                        minlength:3
                    },
                },
                messages: {
                    phoneNumber: {
                        required: "Please enter in a phone number",
                    },
                    birthdate: {
                        required: "Please enter in a birthdate",
                        minAge: "You must be at least 18 years old to register as a Tripper."
                    },
                    address: {
                        required: "Please enter in a address"
                    },
                    city: {
                        required: "Please enter in a city",
                        notValidSpecialChar: "Special character are not support in a city"
                    },
                    state: {
                        required: "Please select state"
                    },
                    nameOnCard: {
                        required: "Name not valid",
                        minlength: "Name should be at least 3 characters",
                        lettersOnly: jQuery.format("Card name must consist of alphabetical characters only")
                    },
                },
                errorPlacement: function(error, element) {
                    // console.log(element.attr('data-target'));

                    let password = $('.errorTxt');
                    let errorConfirmPass = $('.errorConfirmPass');
                    let birthdate = $('.errorbirthdate');
                    if (element.attr('id')=='Password') {
                        $(password).append(error)
                    } else if (element.attr('id')=='confirm_pass') {
                        $(errorConfirmPass).append(error)
                    } else if (element.attr('data-target')=='#kt_datetimepicker_3') {
                        $(birthdate).append(error)
                    } else  {
                        error.insertAfter(element);
                    }
                }
            });

            function changeCardImage(ccCategoryName){
                let imagePath = baseURL +'/media/images/cc-icons/'+ccCategoryName+'.png';
                $('span.card_icon').css('backgroundImage','url("'+imagePath+'")');
            }

            function removeCardImage(){
                $('span.card_icon').css('backgroundImage','');
            }
            // Create a Stripe client.
            var stripe = Stripe('{{ env('STRIPE_PUBLIC_KEY') }}');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    fontSize: '13px',
                    padding: '15px 20px',
                    height: '55px',
                    background: '#F9F9F9',
                    borderRadius: '25px',
                    marginBottom: '5px'
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element.
            //var card = elements.create('card', {style: style});
            var cardNumberElm = elements.create('cardNumber');
            var cardExpiryElm = elements.create('cardExpiry');
            var cardCvcElm = elements.create('cardCvc');

            // Add an instance of the card Element into the `card-element` <div>.

            cardNumberElm.mount('#cardNumber');
            cardExpiryElm.mount('#expirationDate');
            cardCvcElm.mount('#cvv');

            // Handle real-time validation errors from the card Element.
            cardNumberElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#numberError").show();
                } else {
                    changeCardImage(event.brand);
                    $("#numberError").hide();
                }
            });

            cardExpiryElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                if (event.error) {
                    $("#expirationDateError").show();
                } else {
                    $("#expirationDateError").hide();
                }
            });

            cardCvcElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                if (event.error) {
                    $("#cvvError").show();
                } else {
                    $("#cvvError").hide();
                }
            });

            // Handle form submission.
            var form = document.getElementById('tripperSignUp');
            form.addEventListener('submit', function(event) {

                event.preventDefault();
                stripe.createToken(cardNumberElm,{
                    name : $("#nameOnCard").val(),
                    address_line1 : $("#address").val(),
                    address_line2 : $("#address2").val(),
                    address_city : $("#city").val(),
                    address_state : $("#state").val()
                }).then(function(result) {

                    if (result.error) {
                        if(result.error.code == "incomplete_number" || result.error.code == "invalid_number"){
                            $("#numberError").show();
                        }
                        if(result.error.code == "incomplete_expiry" || result.error.code == "invalid_expiry_year_past" || result.error.code == "invalid_expiry_month_past"){
                            $("#expirationDateError").show();
                        }
                        if(result.error.code == "incomplete_cvc" || result.error.code == "invalid_cvv"){
                            $("#cvvError").show();
                        }
                       /*  hideLoader();
                        // Inform the user if there was an error.
                        $("#card-errors").html(result.error.message).show(); */
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {

                $("#card-errors").html('').hide();
                // Submit the form
                showLoader();

                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('tripperSignUp');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                $.ajax({
                    type: "POST",
                    url: "{{ route('tripperRegister') }}",
                    dataType: "JSON",
                    data: new FormData(document.getElementById("tripperSignUp")),
                    processData: false,
                    contentType: false,
                    success: function(msg) {
                        if(msg.success === 'false' || msg.success === false ){
                            $("#card-errors").show().html(msg.error);
                        } else {
                            // window.onbeforeunload = null;
                            window.location.href = '{{ route('tripper.RegSucess') }}';
                        }
                        hideLoader();
                    }
                });
            }

            $("#address").focusout(function() {

                if($("#address").val() == ''){
                    $("#address2").val('');
                    $("#city").val('');
                    $("#city").removeAttr('readonly');
                    $("#state").val('');
                    $("#state").removeAttr('readonly');
                } else {
                    setTimeout(function(){
                        $("#city").valid();
                        $("#state").valid();
                    }, 1000);
                }

            })

        });


    </script>
@endsection

@section('headerCss')
    @parent
    <style>
        #cardNumber,#expirationDate,#cvv,#nameOnCard  {
            font-size: 13px;
            padding: 15px 20px;
            height: 48px;
            background:#F9F9F9;
            border-radius:25px;
            margin-bottom: 5px;
        }
        span.card_icon {
            background-repeat: no-repeat;
            background-position: top left;
            float: right;
            width: 35px;
            height: 25px;
            position: absolute;
            right: 35px;
            top: 20px;
            background-size: 100%;
        }
    </style>
@endsection
