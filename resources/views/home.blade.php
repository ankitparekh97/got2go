<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet'>
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'>

    <link rel="stylesheet" href="{{URL::asset('plugins/global/plugins.bundle.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.bundle.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental.css')}}">
    <link rel="icon" href="{{URL::asset('media/images/fav-icon.png')}}" type="image/png">
    <title>Got2Go Home</title>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initialize&libraries=places&v=weekly"
            defer
    ></script>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white">
            <a href="" class="navbar-brand">
                <img class="lazy-load" data-src="{{URL::asset('media/images/logo.svg')}}" alt="logo">
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item margin-right-menu-200">
                        <a  href="{{route('ownerloginform')}}" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="" class="nav-link btn-tripper"><img class="lazy-load" data-src="{{URL::asset('media/images/btn-tripper.svg')}}" alt="tripper-btn" /></a>
                    </li>
                    <li class="nav-item dropdown ">
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-user-text">Account</span> <img
                                class="lazy-load" data-src="{{URL::asset('media/images/user.svg')}}" alt="user" /></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active-item-link" href="#">Become a tripper</a>
                            <a class="dropdown-item" href="#">Tripboards</a>
                            <a class="dropdown-item" href="#">Rent a Stay</a>
                            <a class="dropdown-item" href="{{route('owner.list-your-stay')}}">List a Stay</a>
                            <a class="dropdown-item font-light" href="#">Login</a>
                            <a class="dropdown-item font-light" href="#">Help</a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img class="lazy-load" data-src="{{URL::asset('media/images/search.svg')}}" alt="search" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section id="slider" class="">
        <div class="">
            <div id="carouselExampleIndicators" class="carousel slide pointer-event" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>

                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="lazy-load d-block w-100" data-src="{{URL::asset('media/images/slider1.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/slider2.jpg')}}" alt="Second slide">
                    </div>

                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-2.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-3.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-4.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-5.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-6.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-7.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-1.jpg')}}" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>

    <!-- NEWSLETTER -->
    <section id="wheretogo" class="container py-5">
        <div class="row">
            <div class="col-md-12 where-to-go">
                <div class="row">
                    <div class="col-lg-4 col-md-6 paddingtop-30 ">
                        <div class="input-group">
                            <img class="lazy-load" data-src="{{URL::asset('media/images/location.svg')}}" alt="location">
                            <input type="text" class="form-control" name="location_detail" id="location_detail">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 paddingtop-30 ">
                        <div class="input-group" id="kt_daterangepicker_1_validate">
                            <img class="lazy-load" data-src="{{URL::asset('media/images/clock.svg')}}" alt="clock">
                            <input type="text" class="form-control" placeholder="Select date">
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6 paddingtop-30 ">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/person.svg')}}" alt="person">
                        <input type="text" class="form-control persons" placeholder="2 Adult(s) 1 Child">

                    </div>

                    <div class="col-lg-2 col-md-6 text-right">
                        <a href="" class="gobutton"><img class="lazy-load" data-src="{{URL::asset('media/images/gobutton.svg')}}" alt="go button"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="checkboxes-inline">
            <div class="row">
                <div class="col-md-4 offset-md-1">
                    <label class="custom-checkbox">Vacation clubs
                        <input type="checkbox" checked="checked">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="col-md-3 ">
                    <label class="custom-checkbox">hotels
                        <input type="checkbox" checked="checked">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="col-md-4 ">
                    <label class="custom-checkbox">private residences
                        <input type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
        </div>
    </section>

    @section('footerJs')
    @parent
    <script src="{{URL::asset('plugins/global/plugins.bundle.js')}}"></script>
    <script src="{{URL::asset('js/scripts.bundle.js')}}"></script>
    <script src="{{URL::asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
    <script src="{{URL::asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav.ml-auto li.nav-item a').click(function() {
                $('li.nav-item a').removeClass("active-menu-link");
                $(this).addClass("active-menu-link");
            });
        });

        function initialize() {

            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: "us"}
            };

            var input = document.getElementById('location_detail');
            var autocomplete = new google.maps.places.Autocomplete(input, options);
        }
    </script>
    @endsection
</body>

</html>
