@extends('layouts.rentalApp')

@section('content')
@php
if(count($rentalUser)>0){
    if($rentalUser[0]->card_type=='American Express'){
        $logo = 'american-express.png';
    } else if($rentalUser[0]->card_type=='MasterCard'){
        $logo = 'master-card.png';
    } else if($rentalUser[0]->card_type=='Visa'){
        $logo = 'visa.png';
    } else if($rentalUser[0]->card_type=='Discover'){
        $logo = 'discover.png';
    } else if($rentalUser[0]->card_type=='Diners Club'){
        $logo = 'diners-club.png';
    } else if($rentalUser[0]->card_type=='JCB'){
        $logo = 'jcb.png';
    }else if($rentalUser[0]->card_type=='Maestro'){
        $logo = 'maestro.png';
    }else if($rentalUser[0]->card_type=='UnionPay'){
        $logo = 'unionpay.png';
    }else {
        $logo = 'no-card.png';
    }
    $cardNo = $rentalUser[0]->last_four;
}
@endphp

<div class="alert-popup-box payment-error" style="display:none">
            <a href="#" class="close-popup-btn1"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M2 2L13 13" stroke="#EC1D1D" stroke-width="3.5"/>
                <path d="M2 13L13 2" stroke="#EC1D1D" stroke-width="3.5"/>
                </svg>

            </a>
            <span><svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.5328 5.0888C11.3899 3.5449 13.6101 3.5449 14.4672 5.08879L20.1958 15.4079C21.0283 16.9076 19.9439 18.75 18.2286 18.75H6.77139C5.05612 18.75 3.97166 16.9076 4.8042 15.4079L10.5328 5.0888Z" stroke="#EC1D1D" stroke-width="1.5"/>
            <path d="M11.583 8.99655C11.5383 8.46009 11.9617 8 12.5 8C13.0383 8 13.4617 8.46009 13.417 8.99655L13.0415 13.5017C13.0181 13.7834 12.7826 14 12.5 14C12.2174 14 11.9819 13.7834 11.9585 13.5017L11.583 8.99655Z" fill="#EC1D1D"/>
            <circle cx="12.5249" cy="15.375" r="0.875" fill="#EC1D1D"/>
            </svg>
            </span>
            <p>We could not process your payment at this time.  Please try again in a few minutes.</p>
</div>

<div class="booking-request-wrapper">
    <div class="container-custom pt-20">
        <div class="container">
            <div class="mainpage-title-wrap">
                <div class="mainpage-title-left">
                    <a href="{{ url('/propertydetail/'.$propertyDetails->id.'?form_date='.$checkInDate.'&to_date='.$checkOutDate.'&guest='.$no_of_guest.'')}}" class="back-link mb-2"><em class="got got-arrow-left-solid"> </em>Back</a>
                    <h2 class="mainpage-title"><strong>LET’S GO!</strong></h2>
                </div>
            </div>
            <div class="bookingrequt-content">
                <div class="row">
                    <div class="col-xl-6 order-12 order-sm-1">
                         <!-- Payment Method -->
                         <div class="bookingrequt-method-wrapper">
                            <div class="label-control mb-3">Payment Method</div>
                            <div class="btn-group payment-dropdown">
                                @if(count($rentalUser)>0)
                                    <a class="btn dropdown-toggle selcted-card" data-toggle="dropdown" href="#" data-type="" ><span class="payment-img"><img class="lazy-load" data-src="{{URL::asset('media/images/cc-icons/'.$logo)}}" alt="icon"/></span><span class="paycard-number">  <span class="card_no" data-id="{{$rentalUser[0]->card_token}}">{{$rentalUser[0]->card_type}} ******{{$cardNo}} </span><span class="exp_date">Exp Date: {{$rentalUser[0]->expiration_date}}</span></span></a>
                                @else
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#" data-type=""><span class="payment-img"></span> Add Debit/Credit Card </a>
                                @endif

                                <ul class="dropdown-menu">
                                    @if(count($rentalUser)>0)
                                        @foreach($rentalUser as $item)
                                            @php
                                             if($item->card_type=='American Express'){
                                                $logo = 'american-express.png';
                                             } else if($item->card_type=='MasterCard'){
                                                $logo = 'master-card.png';
                                             } else if($item->card_type=='Visa'){
                                                $logo = 'visa.png';
                                             } else if($item->card_type=='Discover'){
                                                $logo = 'discover.png';
                                             } else if($item->card_type=='Diners Club'){
                                                $logo = 'diners-club.png';
                                             } else if($item->card_type=='JCB'){
                                                $logo = 'jcb.png';
                                             } else if($item->card_type=='Maestro'){
                                                $logo = 'maestro.png';
                                            }else if($item->card_type=='UnionPay'){
                                                $logo = 'unionpay.png';
                                            } else {
                                                $logo = 'no-card.png';
                                             }
                                             $cardNo = $item->last_four;
                                            @endphp
                                         <li class="dropdown-item active">
                                            <a href="#">
                                                 <span class="payment-img"><img class="lazy-load" data-src="{{URL::asset('media/images/cc-icons/'.$logo)}}" alt="icon"/> </span>
                                                 <span class="card_no" data-id="{{$item->card_token}}">
                                                     {{$item->card_type}} ******{{$cardNo}} <br>
                                                    <span class="exp_date">Exp Date: {{$item->expiration_date}}</span>
                                                </span>
                                            </a>
                                        </li>
                                        @endforeach
                                    @endif
                                    <li class="dropdown-item {{ (count($rentalUser)>0) ? 'active' : '' }}"><a href="#"> <span class="payment-img addCard"></span> Add Debit/Credit Card </a></li>
                                </ul>
                            </div>
                            <span id="PaymentErr" style="color: red;display:none;">Please add payment details to proceed with booking</span>
                            <div class="payment-form cardDetails">
                                <form id="addnewPaymentDetails" action="" method="post">
                                    @csrf
                                    <input type="hidden" id="card_type" name="card_type">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group-custm no-icon card" id="cardNumber">
                                        </div>
                                        <span class="card_icon"></span>
                                        <label class="error" id="numberError" style="display: none;">
                                            Please enter in a valid credit card number.
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group-custm no-icon" id="expirationDate">
                                        </div>
                                        <label class="error" id="expirationDateError" style="display: none;">
                                            Please enter in a valid expiration date.
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group-custm no-icon" id="cvv">
                                        </div>
                                        <label class="error" id="cvvError" style="display: none;">
                                            Please enter in a valid security code.
                                        </label>
                                    </div>
                                </div>
                                <div class="payment-action">
                                    <button type="submit" class="btn  btn-700 btn-outline-orange btn-round saveCard"> Save Card</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Payment Method -->

                        <!-- Message Owner Method -->
                        <div class="bookingrequt-method-wrapper">
                            <div class="label-control mb-3">INCLUDE A MESSAGE FOR THE OWNER</div>
                            <div class="message-owner-info">
                                <div class="owner-content mb-3">
                                    <div class="owner-img">
                                        <img class="lazy-load" data-src="{{ $propertyDetails->ownerdetails->photo ? url('uploads/owners/'.$propertyDetails->ownerdetails->photo) : url('/media/users/default.jpg') }}" alt="{{$propertyDetails->ownerdetails->first_name}}" />
                                    </div>
                                    <div class="owner-name"> {{$propertyDetails->ownerdetails->first_name}}</div>
                                </div>
                                <div class="payment-form">
                                    <textarea class="form-control" id="message" rows="5" placeholder="Message"></textarea>
                                </div>

                            </div>
                        </div>
                        <!-- End Message Owner Method -->

                        <!-- CANCELLATION POLICY -->
                        <div class="bookingrequt-method-wrapper">
                            <div class="label-control mb-3">{{$propertyDetails->cancellation_type}} CANCELLATION POLICY</div>
                            <div class="message-owner-info">
                                <p class="pera">
                                    @if($propertyDetails->cancellation_type == 'Flexible')
                                        Free cancellation until {{Carbon\Carbon::parse($checkInDate)->subDays(1)->format('m/d/Y')}}. After this you will be charged 100% of the total upon cancellation.
                                    @elseif($propertyDetails->cancellation_type == 'Strict')
                                        Free cancellation until {{Carbon\Carbon::parse($checkInDate)->subDays(2)->format('m/d/Y')}}. After this you will be charged 50% of the total upon cancellation.
                                    @else
                                        Free cancellation until {{Carbon\Carbon::parse($checkInDate)->subDays(5)->format('m/d/Y')}}. After this you will be charged 50% of the total upon cancellation.
                                    @endif
                                </p>
                            </div>
                        </div>
                        <!-- CANCELLATION POLICY -->
                         <!-- DISCLAIMER POLICY -->
                         <div class="bookingrequt-method-wrapper">
                            <div class="label-control mb-3">DISCLAIMER</div>
                            <div class="message-owner-info">
                                <p class="pera">
                                Although GOT2GO seeks to verify all hosts' submitted information for accuracy in content, GOT2GO encourages all travelers and Trippers to use due diligence and discretion when booking with property owners through GOT2GO.
                                </p>
                            </div>
                        </div>
                        <!-- DISCLAIMER POLICY -->

                        @if(empty($propertyAvaliblity->toarray()))
                            <span style="color: red; font-weight:bold;">This property is not available for the above selected dates. Kindly select different dates.</span>
                        @else
                            <div class="bookingrequt-action text-center" id="lets_go_tab">
                                @php
                                    $emailLoggedIn = getLoggedInRental()->email;
                                    $phoneLoggedIn = getLoggedInRental()->phone;
                                @endphp
                                @if(empty($emailLoggedIn))
                                    <a href="javascript:void(0)" class="red-btn" data-toggle="modal" data-target="#complete-boooking-email" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >SUBMIT BOOKING REQUEST</a>
                                @endif
                                @if(empty($phoneLoggedIn))
                                    <a href="javascript:void(0)" class="red-btn" data-toggle="modal" data-target="#complete-boooking-phone" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >SUBMIT BOOKING REQUEST</a>
                                @endif
                                @if(!empty($emailLoggedIn) && !empty($phoneLoggedIn))
                                    <a href="javascript:void(0)" class="red-btn" id="letsgo" data-id="{{$propertyDetails->id}}">SUBMIT BOOKING REQUEST</a>
                                @endif
                            </div>
                        @endif
                    </div>

                    <div class="col-xl-6">
                        <div class="booking-detail-wrapper">
                            <div class="bookingdetail-innerblock">
                                <div class="bookingdetail-header">
                                    <div class="bookingdetail-img d-none d-sm-block d-md-block d-lg-block">
                                        <img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$propertyDetails->cover_photo.'')}}" alt="cover photo">
                                    </div>
                                    <div class="bookingdetail-info">
                                        <div class="booking-subtitle d-none d-sm-block d-md-block d-lg-block">
                                            @if($propertyDetails->type_of_property == 'vacation_rental')
                                                VACATION CLUB
                                            @else
                                                PRIVATE RESIDENCE
                                            @endif
                                        </div>
                                    <h4 class="booking-title">{{$propertyDetails->title}}</h4>
                                        <div class="booking-address @if(!$showStAdd) st-add @endif">
                                            @php
                                                $address = explode(",",$propertyDetails->location_detail);
                                            @endphp

                                            @if($showStAdd)
                                                {{ $address[0] }} |
                                            @endif

                                            <span>{{$propertyDetails->city}}, {{$propertyDetails->state}} {{$propertyDetails->zipcode}}</span>
                                        </div>
                                        <div class="booking-subtitle d-block d-sm-none d-md-none d-lg-none">
                                            @if($propertyDetails->type_of_property == 'vacation_rental')
                                                VACATION CLUB
                                            @else
                                                PRIVATE RESIDENCE
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="booking-checkin-info">
                                    <div class="row align-items-center">
                                        <div class="col-4 col-sm-4 text-center check-in-mobile-box">
                                            <div class="display-inline">
                                                <span>CHECK-IN</span>
                                                <h5>{{$checkInDate}}</h5>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-4 text-center">
                                            <div class="display-inline">
                                                <span>CHECK-OUT</span>
                                            <h5>{{$checkOutDate}}</h5>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-4 text-center guest-box">
                                            <div class="display-inline">
                                                <span>GUESTS</span>
                                                <h5>{{$no_of_guest}}</h5>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="pricing-table-breakdown">
                                <h5 class="">{{ (Auth::guard('rentaluser')->user()->is_tripper == '0') ? 'REGULAR PRICE' : 'TRIPPER PRICE'}}</h5>
                                    <ul class="list-unstyled mb-0">
                                        <li class="row">
                                            <div class="col-8">
                                                <span>${{$price}} x {{$days}} nights</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span>${{$reservationAmount}}</span>
                                            </div>
                                        </li>
                                        <li class="row">
                                            <div class="col-8">
                                                <span>Cleaning Fee</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span>${{$cleaningFee}}</span>
                                            </div>
                                        </li>
                                        <li class="row {{(Auth::guard('rentaluser')->user()->is_tripper == '1')?'transaction-fee':''}}">
                                            <div class="col-8">
                                                <span>{{(Auth::guard('rentaluser')->user()->is_tripper == '1')?'Transaction Fee':'Service Fee'}}</span>
                                            </div>
                                            <div class="col-4 text-right">

                                                <span>${{$transactionFee}}</span>

                                            </div>
                                        </li>
                                        <li class="row">
                                            <div class="col-8">
                                                <span>Occupancy taxes/fees</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span>${{$occupnactTax}}</span>
                                            </div>
                                        </li>
                                        <li class="row d-none coupon-applied-final-settlement">
                                            <div class="col-8">
                                                <span style="color:red">Coupon Applied</span>
                                                <span style="color:red" id="couponAppliedNameSpan"></span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span style="color:red" class="couponDiscountMinus">-</span>
                                                <span style="color:red" class="couponDiscountAmount"></span>
                                            </div>
                                        </li>
                                        <li class="row">
                                                <div class="col-md-12  coupon-code-box">
                                                <a href="javascript:void(0);" class="comm-link text-pink" onclick="showEnterCouponPopup();">Enter a coupon</a>
                                            </div>
                                        </li>

                                        {{--@if((Auth::guard('rentaluser')->user()->is_tripper == '1'))
                                        <li class="row price-total">
                                            <div class="col-8">
                                                <span class="blue-text">TOTAL TRIPPER SAVINGS TODAY</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span class="blue-text finalTotalAmount">${{$totalSaved}}</span>
                                            </div>
                                        </li>
                                        @endif--}}
                                        <li class="row price-total">
                                            <div class="col-8">
                                                <span>TOTAL</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span id="finalAmountToPay">{{ Helper::formatMoney($total) }}</span>
                                            </div>
                                        </li>
                                        <li class="row remove-coupon-code" style="display: none;">
                                            <div class="col-8">
                                                <span>COUPON30 has been applied</span>
                                            </div>
                                            <div class="col-4 text-right">
                                                <span class="remove-code">Remove</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <input type="hidden" name="propertyOfferId" id="propertyOfferId" value="{{ $propertyOfferId }}" />
                                <input type="hidden" name="couponCodeId" id="couponCodeName" value="" />
                                <input type="hidden" name="finalAmountToPayHidden" id="finalAmountToPayHidden" value="{{$total}}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- booking phone Modal -->
<div class="modal fade  cmn-popup default-popup" id="complete-boooking-phone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                    </svg>

                </button>
            </div>
            <div class="modal-body">
                <h2>Complete Your Booking!</h2>
                <p>We just need your phone number to send over updates about your booking.</p>
                <form id="rentalCompleteBookingPhoneForm">
                    <div class="center-input-box">
                            <input type="text" id="rentalPhoneNumber" name="rentalPhoneNumber" placeholder="Phone number" />
                    </div>
                    <div class="footer-submit-btn">
                        <input type="submit" id="saveRentalPhoneNumber" value="Save My Phone Number" class="popup-footer-submit" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- booking email Modal -->
<div class="modal fade  cmn-popup default-popup" id="complete-boooking-email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <h2>Complete Your Booking!</h2>
                <p>We just need your email address to send over updates about your booking.</p>
                <form id="rentalCompleteBookingEmailForm">
                    <div class="center-input-box">
                        <input type="text" id="rentalEmail" name="rentalEmail" placeholder="Email Address" />
                    </div>
                    <div class="footer-submit-btn">
                        <input type="submit" value="Save My Email" class="popup-footer-submit" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Booking Success modal -->
<div class="modal fade custom-modal" id="booking_success_modal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Contacthost" aria-modal="true">
    <div class="modal-dialog modal-lg cust-modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" >
            <div class="modal-body bg-white" style="background-image: url({{ asset('media/images/plane-bg.png') }});background-repeat: no-repeat;background-position: center;">
                <h1> Success!</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                    <div class="contact-host-wrapper">
                        @if((Auth::guard('rentaluser')->user()->is_tripper == '1'))
                            <span class="blue-text" style="font-weight: 800;">TOTAL TRIPPER SAVINGS TODAY:</span> <span class="blue-text" style="font-weight: 800;">{{$totalSaved}}</span>
                            <br> <br>
                        @endif
                        <p> </p>
                        <div class="modal-action">
                            <a href="{{ route('rental.mystay') }}" class="btn btn-default dashboard-btn instant_booking_success_close">View My Stays</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<select style="display:none;" id="monthSelect" name="dwfrm_billing_paymentMethods_creditCard_expiration_month" >
    <option class="select-option" label="Month" value="" selected="selected">Month</option>
    <option class="select-option" label="01" value="01">01</option>
    <option class="select-option" label="02" value="02">02</option>
    <option class="select-option" label="03" value="03">03</option>
    <option class="select-option" label="04" value="04">04</option>
    <option class="select-option" label="05" value="05">05</option>
    <option class="select-option" label="06" value="06">06</option>
    <option class="select-option" label="07" value="07">07</option>
    <option class="select-option" label="08" value="08">08</option>
    <option class="select-option" label="09" value="09">09</option>
    <option class="select-option" label="10" value="10">10</option>
    <option class="select-option" label="11" value="11">11</option>
    <option class="select-option" label="12" value="12">12</option>
</select>

<select style="display:none;" class="input-select year" id="yearSelect" aria-required="true" aria-describedby="dwfrm_billing_paymentMethods_creditCard_expiration_year-error" aria-invalid="false">
    <option class="select-option" label="Year" value="" selected="selected">Year</option>
    <option class="select-option" label="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}" value="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}">{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}</option>
</select>

    <!-- Coupon Code Modal -->
    <div class="modal fade  cmn-popup default-popup" id="coupon-code-model-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="applyCouponCodeForDiscountForm" method="post">
                        <h2>Apply a Coupon</h2>
                        <p id="showMessageCouponCode" class="d-none" style="color:red"></p>
                        <div class="center-input-box">
                            <input type="text" id="couponc_code_text" name="couponc_code_text" placeholder="Enter Coupon Code" />
                            <input type="hidden" name="couponCodeNameApplied" id="couponCodeNameApplied" value="" />
                            <input type="hidden" name="discountedPriceAfterCouponCode" id="discountedPriceAfterCouponCode" value="" />
                        </div>
                        <div class="footer-submit-btn">
                            <input type="submit" value="Apply" class="popup-footer-submit" id="applyCouponCodeSubmit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('headerCss')
    @parent
    <style>
        .card{
            border: none;
        }
        .card .card_icon {
            background: transparent url(../media/credit_card_sprites.png) no-repeat 30px 0;
            position: absolute;
            left: 262px !important;
            top: 8px;
        }
        #cardNumber,#expirationDate,#cvv,#nameOnCard  {
            font-size: 13px;
            padding: 15px 20px;
            height: 48px;
            background:#F9F9F9;
            border-radius:25px;
            margin-bottom: 5px;
        }
        span.card_icon {
            background-repeat: no-repeat;
            background-position: top left;
            float: right;
            width: 35px;
            height: 25px;
            position: absolute;
            right: 35px;
            top: 10px;
            background-size: 100%;
        }
    </style>
@endsection

@section('footerJs')
    @parent
<script src="https://js.stripe.com/v3/"></script>
<script>
    var propertyBooking = "{{route('propertydetails.propertyBooking') }}";
    var addCardDetails = "{{route('savePaymentReview') }}";
    var Authorization = "Bearer {{session()->get('rental_token')}}";
    var propertyId = "{{$propertyDetails->id}}";
    var checkIndate = "{{$checkInDate}}";
    var checkOutdate = "{{$checkOutDate}}";
    var guest = "{{$no_of_guest}}";
    var paymentDetails = "{{$rentalUser->count()}}";
    var name = "{{ Auth::guard('rentaluser')->user()->first_name . ' ' . Auth::guard('rentaluser')->user()->last_name}}";
    const updateRentalProfile = "{{ route('rental.saveUpdateRentalProfile') }}";

    $(document).ready(function() {
        (paymentDetails == '0') ? $('.cardDetails').css("display", "block") : $('.cardDetails').css("display", "none");
        $('#PaymentErr').hide();
        $('.payment-dropdown').on('click', '.dropdown-menu li a', function (e) {
            let selText = $(this).text();

            let cardDetail = selText. split(" ");
            let card = cardDetail[3];

            if(selText.trim() == 'Add Debit/Credit Card'){
                $("#addnewPaymentDetails")[0].reset();
                $('div.cardDiv').addClass('card');
                $('input').removeClass('error');
                $('select').removeClass('error');
                $('.error').css('display','none');
                //$(".error").html('');
                $('.card_icon').attr('class', 'card_icon');
                //$(".payment-form").show();
                $('.cardDetails').css("display", "block");
            } else {
                $('.cardDetails').css("display", "none");
                $(this).data('type',card);
            }
            $(this).parents('.btn-group').find('.dropdown-toggle').html($(this).html() );
        });
    });
    const baseURL = '{{ URL::to('') }}';

    function changeCardImage(ccCategoryName){
                let imagePath = baseURL +'/media/images/cc-icons/'+ccCategoryName+'.png';
                $('span.card_icon').css('backgroundImage','url("'+imagePath+'")');
            }

            function removeCardImage(){
                $('span.card_icon').css('backgroundImage','');
            }
            // Create a Stripe client.
            var stripe = Stripe('{{ env('STRIPE_PUBLIC_KEY') }}');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    fontSize: '13px',
                    padding: '15px 20px',
                    height: '55px',
                    background: '#F9F9F9',
                    borderRadius: '25px',
                    marginBottom: '5px'
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element.
            //var card = elements.create('card', {style: style});
            var cardNumberElm = elements.create('cardNumber');
            var cardExpiryElm = elements.create('cardExpiry');
            var cardCvcElm = elements.create('cardCvc');

            // Add an instance of the card Element into the `card-element` <div>.

            cardNumberElm.mount('#cardNumber');
            cardExpiryElm.mount('#expirationDate');
            cardCvcElm.mount('#cvv');

            // Handle real-time validation errors from the card Element.
            cardNumberElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#numberError").show();
                } else {
                    changeCardImage(event.brand);
                    $("#numberError").hide();
                }
            });

            cardExpiryElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#expirationDateError").show();
                } else {
                    //changeCardImage(event.brand);
                    $("#expirationDateError").hide();
                }
            });

            cardCvcElm.on('change', function(event) {
                $("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#cvvError").show();
                } else {
                    //changeCardImage(event.brand);
                    $("#cvvError").hide();
                }
            });

            // Handle form submission.
            var form = document.getElementById('addnewPaymentDetails');
            form.addEventListener('submit', function(event) {

                // Submit the form
                showLoader();

                event.preventDefault();

                stripe.createToken(cardNumberElm,{
                    name :name,
                    address_line1 : '7415 N. Fairfield Ave.',
                    address_line2 : '',
                    address_city : 'IL',
                    address_state : 'Chicago'
                }).then(function(result) {
                    if (result.error) {
                        hideLoader();
                        if(result.error.code == "incomplete_number" || result.error.code == "invalid_number"){
                            $("#numberError").show();
                        }
                        if(result.error.code == "incomplete_expiry" || result.error.code == "invalid_expiry_year_past" || result.error.code == "invalid_expiry_month_past"){
                            $("#expirationDateError").show();
                        }
                        if(result.error.code == "incomplete_cvc" || result.error.code == "invalid_cvv"){
                            $("#cvvError").show();
                        }
                        // Inform the user if there was an error.
                        //$("#card-errors").html(result.error.message).show();
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {

                let formIsInvalid = !jQuery('#addnewPaymentDetails').valid();

                if(formIsInvalid){
                    return;
                }

                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('addnewPaymentDetails');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                $.ajax({
                    type: "POST",
                    url: addCardDetails,
                    dataType: "JSON",
                    data: new FormData(document.getElementById("addnewPaymentDetails")),
                    processData: false,
                    contentType: false,
                    success: function (msg) {
                hideLoader();
                if ($.isEmptyObject(msg.error)) {
                    if (msg.success == true) {

                        // $('.cardDetails').css("visibility", "hidden");

                        $("#addnewPaymentDetails")[0].reset();
                        $('div').removeClass('card');
                        $('input').removeClass('error');
                        $('select').removeClass('error');
                        $('.error').text('');
                        $(".error").html('');

                        let paymentCards = msg.paymentCardsModel;
                        let $logo, $cardNo;
                        let expiration_date = paymentCards.expiration_date;

                        if (paymentCards.card_type == 'American Express') {
                            $logo = 'american-express.png';
                        } else if (paymentCards.card_type == 'MasterCard') {
                            $logo = 'master-card.png';
                        } else if (paymentCards.card_type == 'Visa') {
                            $logo = 'visa.png';
                        } else if (paymentCards.card_type == 'Discover') {
                            $logo = 'discover.png';
                        } else if (paymentCards.card_type == 'Diners Club') {
                            $logo = 'diners-club.png';
                        } else if (paymentCards.card_type == 'JCB') {
                            $logo = 'jcb.png';
                        } else if (paymentCards.card_type == 'Maestro') {
                            $logo = 'maestro.png';
                        }else if(paymentCards.card_type=='UnionPay'){
                            $logo = 'unionpay.png';
                        } else {
                            $logo = 'no-card.png';
                        }

                        $cardNo = paymentCards.last_four;
                        let cardToken = paymentCards.card_token;
                        let addedCard = '<span class="payment-img"><img alt="icon" src="../media/images/cc-icons/' + $logo + '" /></span>' +
                            '<span class="paycard-number">' +
                            '<span class="card_no" data-id="' + cardToken + '">' + paymentCards.card_type + ' ************' + $cardNo + '</span>' +
                            '<span class="exp_date">Exp Date: ' + expiration_date + '</span>' +
                            '</span>';
                        $(".payment-dropdown a.dropdown-toggle").html('');
                        $(".payment-dropdown a.dropdown-toggle").html(addedCard);
                        let addCardList = ' <li class="dropdown-item"><a href="#"> <span class="payment-img"><img alt="icon" src="../media/images/cc-icons/' + $logo + '" /></span> <span class="card_no" data-id="' + cardToken + '">' + paymentCards.card_type + ' ************' + $cardNo + ' <br> <span class="exp_date">Exp Date: ' + expiration_date + '</span></span></a></li>';
                        cardNumberElm.clear();
                        cardExpiryElm.clear();
                        cardCvcElm.clear();
                        $(".payment-dropdown .dropdown-menu li:last").before(addCardList);
                        $('.cardDetails').css("display", "none");

                        Swal.fire({
                            icon: "success",
                            title: msg.message,
                            showConfirmButton: false,
                            timer: 3000
                        })

                    } else {
                        Swal.fire({
                            icon: "error",
                            title: msg.message,
                            showConfirmButton: true
                        })
                    }
                }
            }, error: function (err) {

                hideLoader();
                Swal.fire({
                    icon: "error",
                    title: err.responseJSON.message,
                    showConfirmButton: true
                })
            }
                });
            }

    var $applyCouponCodeForDiscountValid = $("#applyCouponCodeForDiscountForm").validate({
        rules : {
            couponc_code_text : {
                required: true,
                minlength: 5,
                maxlength: 20,
                validateAlphaNumericWithoutUnderscore: true
            }
        },
        messages : {
            couponc_code_text : {
                required: 'Please enter coupon code.'
            }
        },
        submitHandler: function() {
            applyCouponCode();
        }
    });

    $('#couponc_code_text').on('input', function() {
        hideTopErrorMessage();
        disableEnterCouponPopup();

        if($("#applyCouponCodeForDiscountForm").valid()){
            enableEnterCouponPopup();
        }
    });

    function disableEnterCouponPopup(){
        let applyCouponCodeSubmit = $("#applyCouponCodeSubmit");
        applyCouponCodeSubmit.attr('disabled',true);
        applyCouponCodeSubmit.addClass('disabled');
        applyCouponCodeSubmit.val('Apply');
    }

    function enableEnterCouponPopup(){
        let applyCouponCodeSubmit = $("#applyCouponCodeSubmit");
        applyCouponCodeSubmit.attr('disabled',false);
        applyCouponCodeSubmit.removeClass('disabled');
    }

    function applyingButtonTextCouponPopup(){
        let applyCouponCodeSubmit = $("#applyCouponCodeSubmit");
        applyCouponCodeSubmit.attr('disabled',false);
        applyCouponCodeSubmit.removeClass('disabled');
        applyCouponCodeSubmit.val('Applying');
    }

    function showEnterCouponPopup() {
        disableEnterCouponPopup();
        $("#couponc_code_text").val('');
        hideTopErrorMessage();
        $("#coupon-code-model-popup").modal('show');
    }

    function hideEnterCouponPopup() {
        $("#coupon-code-model-popup").modal('hide');
    }

    function hideTopErrorMessage(){
        let showMessageCouponCodeHtml = $("#showMessageCouponCode");
        showMessageCouponCodeHtml.html('');
        showMessageCouponCodeHtml.addClass('d-none');
    }

    function showTopErrorMessage(msg){
        let showMessageCouponCodeHtml = $("#showMessageCouponCode");
        showMessageCouponCodeHtml.html(msg);
        showMessageCouponCodeHtml.removeClass('d-none');
    }

    function applyCouponCode(){

        hideTopErrorMessage();

        let couponCodeName = $("#couponc_code_text").val();
        let couponCodeNameAlreadyApplied = $("#couponCodeNameApplied").val();

        if(couponCodeName === couponCodeNameAlreadyApplied){
            showTopErrorMessage('Code has already been applied successfully');
            return false;
        }

        let applyCouponCodeURL = '{{ route('api.rental.applyCoupon',['rentalId'=> '#rentalId','couponCode' => '#couponCode']) }}';

        applyCouponCodeURL = applyCouponCodeURL.replace('#couponCode',couponCodeName);
        applyCouponCodeURL = applyCouponCodeURL.replace('#rentalId','{{ getLoggedInRentalId() }}');

        applyingButtonTextCouponPopup();

        $.ajax({
            type: "GET",
            url: applyCouponCodeURL,
            dataType: "JSON",
            success: function (data) {

                disableEnterCouponPopup();

                if(data.success === false){
                    showTopErrorMessage(data.message);
                    return;
                }

                let finalAmountToPayHtml = $("#finalAmountToPay");
                let finalAmountToPay = $("#finalAmountToPayHidden").val();
                let discountedPrice = (finalAmountToPay * data.data.discount_percentage)/100;
                discountedPrice = discountedPrice.toFixed(2);
                let finalAmountToPayAfterDiscount = finalAmountToPay - discountedPrice;
                finalAmountToPayAfterDiscount = finalAmountToPayAfterDiscount.toFixed(2);
                let prevDiscountedPrice = $("#discountedPriceAfterCouponCode").val();

                resetCouponCodeData();

                $("#couponCodeName").val(data.data.name);
                $("#couponCodeNameApplied").val(data.data.name);
                $("#discountedPriceAfterCouponCode").val(discountedPrice);
                $(".coupon-applied-final-settlement").removeClass('d-none');
                $('.couponDiscountAmount').html('$'+HelperFunction.numberWithCommas(discountedPrice));
                finalAmountToPayHtml.html('$'+HelperFunction.numberWithCommas(finalAmountToPayAfterDiscount));
                $('#couponAppliedNameSpan').html('('+data.data.name+')');

                hideEnterCouponPopup();
            }
        });

    }
    $("a.close-popup-btn1").click(function(){
      $(this).parent().hide();
    });

    function resetCouponCodeData(){
        let finalAmountToPayHtml = $("#finalAmountToPay");
        let finalAmountToPay = parseInt($("#finalAmountToPayHidden").val());

        $("#couponCodeName").val('');
        $(".coupon-applied-final-settlement").addClass('d-none');
        $('.couponDiscountAmount').html('0');
        $('#couponAppliedNameSpan').html('');
        finalAmountToPayHtml.html('$'+HelperFunction.numberWithCommas(finalAmountToPay));
    }
</script>
<script src="{{URL::asset('js/cardcheck.js')}}"></script>
<script src="{{URL::asset('js/property_detail/property_detail_review.js')}}"></script>
@endsection
