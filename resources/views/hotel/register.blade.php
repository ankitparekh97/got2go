@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Hotel Register') }}</div>

                <div class="card-body">
                <div class="alert alert-danger" style="display:none;">
                        <ul style="margin-bottom:0">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <form method="POST" id="hotelsignup" action="{{ route('hotelsignup') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                <a href="{{ url('auth/owner/facebook') }}" class="btn btn-facebook"> Facebook</a>
                                <a href="{{ url('auth/owner/google') }}" class="btn btn-facebook"> Google</a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#hotelsignup').validate({ 
         rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email:true
            },
            password:{
                required:true,
                //min:8
            }
            
        }
    });

    $('#hotelsignup').on('submit', function(e) {
        e.preventDefault();
        var $form = $(this);
        if(!$form.valid()) return false;
        $.ajax({
            type: "POST",
            url: "{{ route('hotelsignup') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {

                //window.location.href="{{ route('ownerloginform') }}" ;
                if($.isEmptyObject(msg.error)){
                    window.location.href="{{ route('hotelloginform') }}" ;
                }else{
                
                    $(".alert-danger").find("ul").html('');
                    $(".alert-danger").css('display','block');
                    $.each( msg.error, function( key, value ) {
                    $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                    });
                }
            }
        });
    });
    </script>
@endsection
