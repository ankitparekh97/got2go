@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                <div class="alert alert-danger" style="display:none;">
                        <ul style="margin-bottom:0">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <form method="POST" id="hotellogin" action="{{ route('hotellogin') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <a href="{{ url('auth/owner/facebook') }}" class="btn btn-facebook"> Facebook</a>
                                <a href="{{ url('auth/owner/google') }}" class="btn btn-facebook"> Google</a>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#hotellogin').validate({ 
         rules: {
            email: {
                required: true,
                email:true
            },
            password:{
                required:true,
                //max:8
            }
            
        }
    });

    $('#hotellogin').on('submit', function(e) {
        e.preventDefault();
        var $form = $(this);
        if(!$form.valid()) return false;
        $.ajax({
            type: "POST",
            url: "{{ route('hotellogin') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                if($.isEmptyObject(msg.error)){

                window.location.href="{{ route('hotel.home') }}" ;
            }else{
               
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( msg.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });
            }

            }
        });
    });
    </script>
@endsection
