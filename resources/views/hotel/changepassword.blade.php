@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Change Password') }}</div>

                <div class="card-body">
                 @if(session()->get('success'))
                    <div class="alert alert-success">
                    {{ session()->get('success') }}  
                    </div>
                @endif
                <div class="alert alert-danger" style="display:none;">
                        <ul style="margin-bottom:0">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <form method="POST" id="change_password" action="{{ route('hotel.changepassword') }}">
                        @csrf


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Change Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#change_password').validate({ 
         rules: {
            password:{
                required:true,
                //max:8
            },
            password_confirmation:{
                required:true,
                //max:8
            },
            
        }
    });

    $('#change_password').on('submit', function(e) {
        e.preventDefault();
        var $form = $(this);
        if(!$form.valid()) return false;
        $.ajax({
            type: "POST",
            url: "{{ route('hotel.changepassword') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                if($.isEmptyObject(msg.error)){

                window.location.href="{{ route('hotel.changepasswordview') }}" ;
            }else{
               
                $(".alert-danger").find("ul").html('');
                $(".alert-danger").css('display','block');
                $.each( msg.error, function( key, value ) {
                  $(".alert-danger").find("ul").append('<li>'+value+'</li>');
                });
            }

            }
        });
    });
    </script>
@endsection
