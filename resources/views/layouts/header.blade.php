   <header>
        <div class="responsive-menu-cstm">
        <ul class="resp-menu">
            <li class="{{((Request::segment(1) == 'owner_panel' && Request::segment(2) == 'property') || (Request::segment(1) == 'owner_panel' && Request::segment(2) == 'timeshare') || (Request::segment(1) == 'owner_panel' && Request::segment(2) == 'requestHotel') ? 'active' : '')  }}"><a href="{{route('owner.property')}}#current">LISTINGS</a></li>
            <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'booking' && Request::segment(3) == 'list') ? 'active' : '') }}"><a href="{{route('property.booking.list')}}" >BOOKINGS</a></li>
            <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'payment' && Request::segment(3) == 'list') ? 'active' : '') }}"><a href="{{route('payment.list')}}">PAYMENT</a></li>
            <li><a href="#">PROFILE</a></li>
                <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'chat') ? 'active' : '') }}"><a href="{{route('owner.chat')}}">CHAT</a></li>
        </ul>
        </div>
        <div class="custom-container">
            <div class="row align-items-center">
                <div class="col-5 col-md-7 col-lg-1 navbar-light">
                <button class="navbar-toggler collapsed d-block d-sm-block d-xs-block d-md-none d-lg-none" id="responsive-menu" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button> 
                    <a href="{{route('owner.property')}}"><img class="lazy-load" data-src="{{URL::asset('media/images/Logo.png')}}" alt="logo" /></a>
                </div>
                @if(Auth::guard('owner')->user())
                <div class="col-1 col-lg-8 responsive-menu-show transition text-center">
               
                    <ul class="desktop-menu">
                        <li class="{{((Request::segment(1) == 'owner_panel' && Request::segment(2) == 'property') || (Request::segment(1) == 'owner_panel' && Request::segment(2) == 'timeshare') || (Request::segment(1) == 'owner_panel' && Request::segment(2) == 'requestHotel') ? 'active' : '')  }}"><a href="{{route('owner.property')}}#current">LISTINGS</a></li>
                        <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'booking' && Request::segment(3) == 'list') ? 'active' : '') }}"><a href="{{route('property.booking.list')}}" >BOOKINGS</a></li>
                        <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'payment' && Request::segment(3) == 'list') ? 'active' : '') }}"><a href="{{route('payment.list')}}">PAYMENT</a></li>
                        <li><a href="#">PROFILE</a></li>
                         <li class="{{((Request::segment(1) == 'owner_panel'  && Request::segment(2) == 'chat') ? 'active' : '') }}"><a href="{{route('owner.chat')}}">CHAT</a></li>
                    </ul>
                    
                </div>


                <div class="col-7 col-md-5 col-lg-3 text-right dropdown">
                    <a href="{{route('rental.home')}}" class="grey-btn">Switch To Renter</a>
                    <div class="owner-menu-outer-link">
                        <a href="#" role="button" class="owner-account-dropdown">
                            <img class="lazy-load" data-src="{{URL::asset('media/images/user-image.png')}}" alt="user" />
                        </a>
                        <div class="dropdown-menu-cstm">
                            <a class="dropdown-item" href="#"  onclick="logout(`{{route('user.logout')}}`)">Log Out</a>
                        </div>
                    </div>
                  </div>
                @endif
            </div>
        </div>
    </header>

