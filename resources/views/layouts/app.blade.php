<!DOCTYPE html>

<html lang="en" >
<!--begin::Head-->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>GOT2GO | Owner</title>
    <meta name="description" content="Updates and statistics"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_url" content="{{ URL('') }}">
    <meta name="owner-timezone" content="">
    @if(Auth::guard('owner')->user())
        <link rel="icon" href="{{URL::asset('media/images/orangeplane.png')}}" type="image/png">
    @else
        <link rel="icon" href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">
    @endif


    @section('headerCss')
        <!--begin::Global Theme Styles(used by all pages)-->
        @if(Request::segment(3) != 'add' && Request::segment(3) != 'edit' && Request::segment(2) != 'chat' && Request::segment(1) != 'bulk-upload' )
            <link href="{{URL::asset('plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{URL::asset('plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{URL::asset('css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{URL::asset('css/style_login.css')}}" rel="stylesheet" >
        @endif

        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
        <link rel="icon" href="IMAGES/favicon.png" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('css/jquery.mCustomScrollbar.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('css/datepicker.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{URL::asset('owner-panel/css/style2.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/responsive.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/image-uploader.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('css/webfonts/got2gofont.css')}}">

    <!--end::Global Theme Styles-->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    @show

    @section('headerJs')
        <script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js" integrity="sha512-iceXjjbmB2rwoX93Ka6HAHP+B76IY1z0o3h+N1PeDtRSsyeetU3/0QKJqGyPJcX63zysNehggFwMC/bi7dvMig==" crossorigin="anonymous"></script>
        <script src="{{URL::asset('js/movment.js')}}"></script>
        <script src="{{URL::asset('js/jquery.mCustomScrollbar.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{URL::asset('js/jquery.blockUI.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script>


        <script type="application/javascript">
            (function($){
                var baseUrl = $('meta[name="_url"]').attr('content');
                var assetBaseUrl = "{{ asset('') }}";
            }) (jQuery)
        </script>

    @show
</head>
<!--end::Head-->

<!--begin::Body-->
<body>

    <div class="responsive-menu-layer"></div>

    <div id="loader" style="display: none;">
        <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
    </div>

    @include('layouts.header')

    <div class="content-area">
        @auth('owner')
            @include('layouts.partials.owner-alert-offer-popup')
        @endauth
        @yield('content')
    </div>

    @include('layouts.footer')

    @section('footerJs')

        {{-- <script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js" integrity="sha512-iceXjjbmB2rwoX93Ka6HAHP+B76IY1z0o3h+N1PeDtRSsyeetU3/0QKJqGyPJcX63zysNehggFwMC/bi7dvMig==" crossorigin="anonymous"></script>
        <script src="{{URL::asset('js/movment.js')}}"></script>
        <script src="{{URL::asset('js/jquery.mCustomScrollbar.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{URL::asset('js/jquery.blockUI.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script> --}}
        <script type="application/javascript">
            (function($){
                var baseUrl = $('meta[name="_url"]').attr('content');
                var assetBaseUrl = "{{ asset('') }}";
            }) (jQuery)
        </script>
        <script>

            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            $('body form').append('<input type="hidden" id="timezone" name="timezone" value="'+Intl.DateTimeFormat().resolvedOptions().timeZone+'">');

            var Authorization = "Bearer {{session()->get('owner_token')}}";
            var loaderImage = "{{URL::asset('media/images/loader.gif')}}";
            var KTAppSettings = {
                "breakpoints": {
                    "sm": 576,
                    "md": 768,
                    "lg": 992,
                    "xl": 1200,
                    "xxl": 1200
                },
                "colors": {
                    "theme": {
                        "base": {
                            "white": "#ffffff",
                            "primary": "#8950FC",
                            "secondary": "#E5EAEE",
                            "success": "#1BC5BD",
                            "info": "#6993FF",
                            "warning": "#FFA800",
                            "danger": "#F64E60",
                            "light": "#F3F6F9",
                            "dark": "#212121"
                        },
                        "light": {
                            "white": "#ffffff",
                            "primary": "#EEE5FF",
                            "secondary": "#ECF0F3",
                            "success": "#C9F7F5",
                            "info": "#E1E9FF",
                            "warning": "#FFF4DE",
                            "danger": "#FFE2E5",
                            "light": "#F3F6F9",
                            "dark": "#D6D6E0"
                        },
                        "inverse": {
                            "white": "#ffffff",
                            "primary": "#ffffff",
                            "secondary": "#212121",
                            "success": "#ffffff",
                            "info": "#ffffff",
                            "warning": "#ffffff",
                            "danger": "#ffffff",
                            "light": "#464E5F",
                            "dark": "#ffffff"
                        }
                    },
                    "gray": {
                        "gray-100": "#F3F6F9",
                        "gray-200": "#ECF0F3",
                        "gray-300": "#E5EAEE",
                        "gray-400": "#D6D6E0",
                        "gray-500": "#B5B5C3",
                        "gray-600": "#80808F",
                        "gray-700": "#464E5F",
                        "gray-800": "#1B283F",
                        "gray-900": "#212121"
                    }
                },
                "font-family": "Poppins"
            };

            function logout(url){
                showLoader();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    headers: {
                        "Authorization": Authorization,
                    },
                    success: function (data) {
                        hideLoader();
                        if(data.success){
                            window.location.href = data.redirect_url;
                        }
                    }
                });
            }

            function showLoader(){
                $.blockUI({
                    message: '<img src="'+loaderImage+'" alt="loader" class="rounded-circle" />',
                    css: {
                        border: 'none',
                        backgroundColor: 'transparent !important',
                    },
                    baseZ: 2000
                });
            }

            function hideLoader(){
                $.unblockUI();
            }

            const APP_URL = '{{ URL::to('') }}';
        </script>
        <!--end::Global Config-->

        <!--begin::Global Theme Bundle(used by all pages)-->
        @if(Request::segment(3) != 'add' && Request::segment(3) != 'edit' && Request::segment(1) != 'bulk-upload' )
            <script src="{{URL::asset('plugins/global/plugins.bundle.js')}}"></script>
            <script src="{{URL::asset('plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
            <script src="{{URL::asset('js/scripts.bundle.js')}}"></script>
        @endif


        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <!--end::Global Theme Bundle-->

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.3.5/dist/sweetalert2.all.min.js"></script>

        <!--begin::Page Scripts(used by this page)-->
        <script src="{{URL::asset('js/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('js/image-uploader.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
        <script src="{{ URL::asset('js/commonHelpers.js') }}"></script>
        <script src="{{URL::asset('js/dm-uploader.min.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.lazy.min.js') }}"></script>
        <script src="{{ URL::asset('js/lazy-load.js') }}"></script>

        <!--end::Page Scripts-->

        @auth('owner')
            <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
            <script type="application/javascript">

                // Enable pusher logging - don't include this in production
                Pusher.logToConsole = true;
                var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}',{
                    encrypted: true,
                    cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
                    authEndpoint: '{{ route('api.pusher.verify.private.owner.post') }}',
                    auth: {
                        headers: {
                            'Authorization': "Bearer {{ getJwtToken() }}",
                            'X-CSRF-Token': "{{ csrf_token() }}"
                        }
                    }
                });

                // Subscribe to the channel we specified in our Laravel Event
                var pusherChannel = pusher.subscribe('private-App.Owner.{{ getOwnerId() }}');

                pusherChannel.bind('pusher:subscription_error', function(err) {
                    console.log(err);
                });

            </script>
        @endauth

    @show

</body>
<!--end::Body-->
</html>
