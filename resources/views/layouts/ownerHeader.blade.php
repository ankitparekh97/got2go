<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800,900' rel='stylesheet'>
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('css/webfonts/got2gofont.css')}}">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL::asset('plugins/global/plugins.bundle.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.bundle.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental1.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental2.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/dashboard.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/webfonts/got2gofont.css')}}">

    <link rel="icon" href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">
    <title>GOT2GO TRAVEL</title>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&callback=initialize&libraries=places&v=weekly" defer></script>

    <script src="{{URL::asset('plugins/global/plugins.bundle.js')}}"></script>
    <script src="{{URL::asset('js/scripts.bundle.js')}}"></script>
    <script src="{{URL::asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
    <script src="{{URL::asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{URL::asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{URL::asset('js/lazy-load.js')}}"></script>


</head>

<body id="home" data-spy="scroll" data-target="#main-nav">

    <nav class="navbar navbar-expand-md navbar-light fixed-top {{(Request::segment(1) == '') ? 'homepage-nav':''}}"  id="main-nav">
        <div class="container-fluid bg-white p-0">
            <button class="navbar-toggler" id="moburger" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            @if(Auth::guard('owner')->user())
            <a href="{{route('owner.property')}}" class="navbar-brand {{(Request::segment(1) == '') ? 'mobile-hide':''}}" id="gradient-logo">
                <img class="lazy-load" data-src="{{URL::asset('media/images/logo.svg')}}" alt="logo">
            </a>
            @else
            <a href="{{route('owner.home')}}" class="navbar-brand  {{(Request::segment(1) == '') ? 'mobile-hide':''}}" id="gradient-logo">
                <img class="lazy-load" data-src="{{URL::asset('media/images/logo.svg')}}" alt="logo">
            </a>
            @endif
            <ul id="responsive-sidenav" class="navbar-nav btnlinks">
                <li class="nav-item dropdown ">
                    <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/user.svg')}}" alt="user" />
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="drpresponsivemenu">

                        @if(\Illuminate\Support\Facades\Auth::guard('owner')->user()!= '')
                        <a class="dropdown-item active-item-link" href="{{route('owner.home')}}">Dashboard</a>
                        @endif
                        <a class="dropdown-item" href="#">Rent a Stay</a>
                        <a class="dropdown-item" href="{{route('owner.list-your-stay')}}">List a Stay</a>
                        @if(\Illuminate\Support\Facades\Auth::guard('owner')->user()!='')
                        <a class="dropdown-item font-light" style="cursor: pointer" href="#">My Account </a>
                        <a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutOwner(`{{route('user.logout')}}`)">Sign Out </a>
                        @else
                        <a class="dropdown-item font-light" style="cursor: pointer" href="" data-toggle="modal" data-target="#login_popup">Login </a>
                        @endif
                    </div>
                </li>
            </ul>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="responsive-block-logo test">
                    <div class="res-logo"><img class="lazy-load" data-src="{{URL::asset('media/images/main-navigation/got2go-mobile-logo.svg')}}" alt="logo"></div>
                    <div class="res-close"><button type="button" id="closemenu"><img class="lazy-load" data-src="{{URL::asset('media/images/main-navigation/close-pink-icon.svg')}}" alt="icon"></button></div>

                </div>
                @include('layouts.topNav')

                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item dropdown ">
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="btn-user-text" id="ownerLogin">
                                @if(\Illuminate\Support\Facades\Auth::guard('owner')->user()!='')
                                {{\Illuminate\Support\Facades\Auth::guard('owner')->user()->first_name}}
                                @else
                                Account
                                @endif
                            </span> <img src="{{URL::asset('media/images/user.svg')}}" alt="user" />
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">


                            @if(\Illuminate\Support\Facades\Auth::guard('owner')->user()!='')
                            <a class="dropdown-item font-light" style="cursor: pointer" href="#">My Account </a>
                            <span id="ownerLogout">
                            <a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutOwner(`{{route('user.logout')}}`)">Sign Out </a>
                            </span>
                            @else
                            <a class="dropdown-item font-light" style="cursor: pointer" href="" data-toggle="modal" data-target="#login_popup">Login </a>
                            @endif
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="{{URL::asset('media/images/search.svg')}}" alt="search" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="menu-overlay"></div>
