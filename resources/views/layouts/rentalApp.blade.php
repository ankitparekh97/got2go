<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">
    <title>GOT2GO TRAVEL</title>

    @section('headerCss')
        <link rel="preload" as="font" href="/css/rental/got2go.woff" type="font/woff2" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800,900' rel='stylesheet'>
        <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.css" integrity="sha512-WfDqlW1EF2lMNxzzSID+Tp1TTEHeZ2DK+IHFzbbCHqLJGf2RyIjNFgQCRNuIa8tzHka19sUJYBO+qyvX8YBYEg==" crossorigin="anonymous" />
        <link rel="stylesheet" href="{{URL::asset('css/rental/all.css')}}">
    @show

    @section('headerJs')
    @show

</head>
<body id="home" data-spy="scroll" data-target="#main-nav" class="{{(Auth::guard('rentaluser')->user() && Auth::guard('rentaluser')->user()->is_tripper == 1)?'tripper-dashboard':''}}">

    @include('layouts.rentalHeader')

    @auth('rentaluser')
        @if(isLoggedInUserIsTripper())
            @include('layouts.partials.tripper-alert-offer-popup')
        @endif
    @endauth
    @include('layouts.listSessionmsg')
    @yield('content')

    @include('rentaluser.login')
    @include('rentaluser.signup')
    @include('rentaluser.forgot-password')

    @include('rentaluser.alert')

    @include('layouts.rentalSubsubribe')
    @include('layouts.rentalFooter')

    @section('footerJs')
    <script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    @if(in_array(url()->current(),array(route('owner.list-your-stay'))) === false)
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initialize&libraries=places&v=weekly" defer></script>
    @endif
    <script src="{{URL::asset('plugins/global/plugins.bundle.js')}}"></script>
    <script src="{{URL::asset('js/scripts.bundle.js')}}"></script>
    <script src="{{URL::asset('js/select2.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap-daterangepicker.min.js') }}"></script>
    <script src="{{URL::asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{URL::asset('js/jquery.blockUI.min.js')}}"></script>
    <script src="{{ URL::asset('js/commonHelpers.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/lazy-load.js')}}"></script>
    
        <script>
        var searchPath = "{{route('rental.search')}}";
        $( ".rental-search" ).click(function() {
        $(this).parent().toggleClass('active');
        $('.mainSearchTxt').focus()
        });
        $( document ).ready(function() {
            $('.mainSearchTxt').on("keyup", function(e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    var data = {
                        "property_type":['property','hotels','vacation_rental'],
                        "adults":1,
                        "child":0,
                        "searchText": $(this).val(),
                        "pageno":0,
                        "number_of_guest":1,
                    }
                    if($(this).val().trim() == ''){
                        return false;
                    }
                    window.location.href = searchPath+"?"+$.param(data);
                }
            });
            $('.search-icon-mobile').on('click',function(e){
                var data = {
                        "property_type":['property','hotels','vacation_rental'],
                        "adults":1,
                        "child":0,
                        "searchText": $('.mainSearchTxt').val(),
                        "pageno":0,
                        "number_of_guest":1,
                    }
                    console.log("data",data)
                window.location.href = searchPath+"?"+$.param(data);
            });
        });

        </script>
        <script type="text/javascript">

            $(document).on('keypress', function (e) {
                if (e.which == 27) {
                    swal.closeModal();
                }
            });

            var Authorization = "Bearer {{session()->get('rental_token')}}";
            const APP_URL = '{{ URL::to('') }}';
            var loaderImage = "{{URL::asset('media/images/loader.gif')}}";

            function showLoader(){
                $.blockUI({
                    message: '<img src="'+loaderImage+'" class="rounded-circle" />',
                    css: {
                        border: 'none',
                        backgroundColor: 'transparent !important',
                    },
                    baseZ: 2000
                });
            }

            function hideLoader(){
                $.unblockUI();
            }

            $('#moburger').on('click', function() {
                $(".menu-overlay").show();
                $('body').addClass('mob-nav-scroll');
            });

            $(".menu-overlay").click(function(event) {
                $("#moburger").trigger("click");
                $(".menu-overlay").hide();
                $('body').removeClass('mob-nav-scroll');
            });

            $("#closemenu").click(function(event) {
                $("#moburger").trigger("click");
                $(".menu-overlay").hide();
                $('body').removeClass('mob-nav-scroll');
            });

            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

            //get user timezone
            $('body form').append('<input type="hidden" id="timezone" name="timezone" value="'+Intl.DateTimeFormat().resolvedOptions().timeZone+'">');

            var isRentalAuth = false;
            @if(Auth::guard('rentaluser')->user())
                isRentalAuth = true;
            @endif

            function logoutRental(url){
                showLoader();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    headers: {
                        "Authorization": Authorization,
                    },
                    success: function (data) {
                        hideLoader();
                        if(data.success){
                            // window.onbeforeunload = null;
                            window.location = data.redirect_url;
                            // location.reload();
                        }
                    }
                });
            }

            function logoutOwner(url){
                showLoader();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    headers: {
                        "Authorization": Authorization,
                    },
                    success: function (data) {
                        hideLoader();
                        if(data.success){
                            window.location = data.redirect_url;
                        }
                    }
                });
            }

            $(document).ready(function() {
                $('ul#mainnavigatn li.nav-item a').click(function() {
                    $('li.nav-item a').removeClass("active-menu-link");
                    $(this).addClass("active-menu-link");
                });
            });

            let placeSearch;
            let autocompleteTripper;
            const componentForm = {
                city: "long_name",
                state: "short_name",
            };

            function initialize() {
                // call map in property detail page for detail page only
                let getPDPpath = window.location.pathname;
                let getDetailpath = getPDPpath.split('/');
                if(getDetailpath[1] == "propertydetail"){
                    initializePropertyDetailMap();
                }

                var options = {
                    types: ['(cities)'],
                    componentRestrictions: {country: "us"}
                };

                var input = document.getElementById('location_detail');
                var autocomplete_location_detail = new google.maps.places.Autocomplete(input, options);
                autocomplete_location_detail.addListener("place_changed", fzl_check_if_address_selected_location_detail);
                function fzl_check_if_address_selected_location_detail(){
                    const place = autocomplete_location_detail.getPlace();
                    var formatted_address = place.formatted_address;
                    if(formatted_address)
                    {
                        $('.address_selected_from_drop').val($('#location_detail').val());
                        $('#modal_location_detail').val($('#location_detail').val())
                       
                    }
                    var form = jQuery("#searchForm");
                    form.valid() === false
                   
                }

                var input1 = document.getElementById('modal_location_detail');
                var autocomplete_modal = new google.maps.places.Autocomplete(input1, options);
                autocomplete_modal.addListener("place_changed", fzl_check_if_address_selected_modal);
                function fzl_check_if_address_selected_modal(){
                    const place = autocomplete_modal.getPlace();
                    var formatted_address = place.formatted_address;
                    if(formatted_address)
                    {
                        $('.address_selected_from_drop').val($('#modal_location_detail').val());
                        $('#location_detail').val($('#modal_location_detail').val())
                    }
                   
                    var form = jQuery("#searchForm");
                    form.valid() === false
                }
                var input2 = document.getElementById('hotdeal_location');
                var autocomplete = new google.maps.places.Autocomplete(input2, options);

                var options = {
                    componentRestrictions: {country: "us"},
                    types: ["geocode"]
                };

                var billing_address = document.getElementById('address');
                autocompleteTripper = new google.maps.places.Autocomplete(billing_address, options);
                autocompleteTripper.setFields(["address_component"]);
                // When the user selects an address from the drop-down, populate the
                // address fields in the form.
                autocompleteTripper.addListener("place_changed", fillInAddress);


            }


            function fzl_check_if_address_selected(){
                const place = autocomplete.getPlace();
                var formatted_address = place.formatted_address;
                $('.address_selected_from_drop').val(formatted_address);
                var form = jQuery("#searchForm");
                form.valid() === false
            }

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                const place = autocompleteTripper.getPlace();
                let streetAddress = '';

                document.getElementById('address').value = '';
                for (const component in componentForm) {
                    document.getElementById(component).value = "";
                    document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details,
                // and then fill-in the corresponding field on the form.
                for (const component of place.address_components) {
                    const addressType = component.types[0];
                    console.log(addressType,component);

                    if(addressType == "locality" || addressType == "sublocality_level_1"){
                        const val = component['long_name'];
                        document.getElementById('city').value = val;
                        $('#city').attr('readonly', true);
                    }
                    else if(addressType == "administrative_area_level_1"){
                        const val = component['short_name'];
                        document.getElementById('state').value = val;
                        $('#state').attr('readonly', true);
                    }
                    else if(addressType == "street_number"){

                        streetAddress = component['long_name'];
                        document.getElementById('address').value = streetAddress;
                    }
                    else if(addressType == "route"){
                        streetAddress += ' '+component['long_name'];
                        document.getElementById('address').value = streetAddress;
                    }

                }
            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(position => {
                        const geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        const circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocompleteTripper.setBounds(circle.getBounds());
                    });
                }
            }



            var geocoder;  var map;
            function initializePropertyDetailMap() {
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(-34.397, 150.644);
                var mapOptions = {
                    zoom: 8,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                propertyDetailMap();
            }

            function propertyDetailMap() {
                var address = $('.right-text:first h4').text().trim();

                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        var iconMarker = (getPropertyType == 'property') ? privateResidenceMarker : vacationRentalMarker;

                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            icon: iconMarker
                        });
                    } else {
                        console.log('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

        </script>

        @auth('rentaluser')
            @if(isLoggedInUserIsTripper())
                <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
                <script type="application/javascript">

                    // Enable pusher logging - don't include this in production
                    Pusher.logToConsole = true;
                    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}',{
                        encrypted: true,
                        cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
                        authEndpoint: '{{ route('api.pusher.verify.private.tripper.post') }}',
                        auth: {
                            headers: {
                                'Authorization': "Bearer {{ getJwtToken() }}",
                                'X-CSRF-Token': "{{ csrf_token() }}"
                            }
                        }
                    });

                    // Subscribe to the channel we specified in our Laravel Event
                    var pusherChannel = pusher.subscribe('private-App.RentalUser.{{ getLoggedInRentalId() }}');

                    pusherChannel.bind('pusher:subscription_error', function(err) {
                        console.log(err);
                    });

                </script>
            @endif
        @endauth

    @show
</body>
</html>


