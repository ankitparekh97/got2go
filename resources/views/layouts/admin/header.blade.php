<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <meta name="_url" content="{{ URL('') }}">
   <title>GOT2GO | Admin</title>
   <!-- Custom fonts for this template-->
   <link href="{{URL::asset('css/all.min.css')}}" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
   <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;900&display=swap" rel="stylesheet">
   <!-- Custom styles for this template-->
   <link href="{{URL::asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
   <link href="{{URL::asset('css/admin.css')}}" rel="stylesheet">
   <link rel="icon" href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">
   <!-- Bootstrap core JavaScript-->
   <script src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
   <script src="{{URL::asset('js/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
   <!-- Core plugin JavaScript-->
   <script src="{{URL::asset('js/jquery-easing/jquery.easing.min.js')}}"></script>
   <!-- Custom scripts for all pages-->
   <script src="{{URL::asset('js/sb-admin-2.min.js')}}"></script>
   <script src="{{URL::asset('js/jquery.validate.min.js') }}"></script>
   <script type="application/javascript">
      var baseUrl = $('meta[name="_url"]').attr('content');
      var assetBaseUrl = "{{ asset('') }}";
   </script>
   <script src="{{URL::asset('js/lazy-load.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.plugins.min.js')}}"></script>
</head>

