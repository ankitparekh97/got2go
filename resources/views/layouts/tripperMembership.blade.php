<div class="container">
    <span class="display-block text-center strike text"><s>${{ Helper::formatMoney($subscription->strike_through_amount,1,false) }} ANNUAL MEMBERSHIP</s></span>
    <h1 class="text-center">${{$subscription->annual_subscription ? Helper::formatMoney($subscription->annual_subscription,1,false):0}} for {{ carbonGetCurrentYear() }}</h1>
    <div class="flight-image text-center 1">
        <img data-src="{{URL::asset('media/images/tripper-page/flight-image.png') }}" alt="" class="lazy-load d-none d-sm-none d-md-block d-lg-block d-xl-block"/>
        <img data-src="{{URL::asset('media/images/tripper-page/fligh-image-mobile.png')}}" alt="" class="lazy-load d-block d-sm-block d-md-none d-lg-none d-xl-none">
    </div>
    <h3 class="text-center">Sign up today and save on <span>EVERY. SINGLE. STAY.</span></h3>
    <div class="specification-div">
        <div class="row">
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/specification1.png') }}" alt="specification" />
                    </div>
                    <div class="description">
                        <h3>EXCLUSIVE <br>ACCESS</h3>
                        <p>Enjoy all the member benefits of specific stays and expedite your status. Explore stays only available to those with Tripper Status.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/specification2.png') }}" alt="specification" />
                    </div>
                    <div class="description">
                        <h3>FIRST <br>IN LINE</h3>
                        <p>Book your stay before anyone else even has a chance. Trippers have first dibs on newly available properties.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="inner-row">
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/specification3.png') }}" alt="specification" />
                    </div>
                    <div class="description">
                        <h3>TRIPPER <br>DISCOUNTS</h3>
                        <p>Fees? Trippers avoid those at all costs. That’s why we make it easy for you to calculate exactly what your final price will be. We’ll handle the fees. You handle the fun.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
