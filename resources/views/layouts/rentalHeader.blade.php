<nav class="navbar navbar-expand-md navbar-light fixed-top {{(Request::segment(1) == '') ? 'homepage-nav':''}}"  id="main-nav">
    <div class="container-fluid bg-white p-0">
        <button class="navbar-toggler" id="moburger" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        @if(Auth::guard('rentaluser')->user())
            <a href="{{route('rental.home')}}" class="navbar-brand {{(Request::segment(1) == '') ? 'mobile-hide':''}}" id="gradient-logo">
                <img class="lazy-load" data-src="{{URL::asset('media/images/site-logo.png')}}" alt="logo">
            </a>
        @else
            <a href="{{route('rental.home')}}" class="navbar-brand  {{(Request::segment(1) == '') ? 'mobile-hide':''}}" id="gradient-logo">
                <img class="lazy-load" data-src="{{URL::asset('media/images/site-logo.png')}}" alt="logo">
            </a>
        @endif
        <ul id="responsive-sidenav" class="navbar-nav btnlinks">
            @if(!empty(getLoggedInRental()) && !isLoggedInUserIsTripper())
                <a href="{{route('tripper.singUpForm')}}"><img class="lazy-load" data-src="{{URL::asset('media/images/trippersOnly.png')}}" alt="" /></a>
            @elseif(empty(getLoggedInRental()))
                <li class="nav-item">
                    <a href="{{route('tripper.index')}}" class="nav-link btn-tripper"><img class="lazy-load" data-src="{{URL::asset('media/images/btn-tripper.svg')}}" alt="tripper button" /></a>
                </li>
            @endif

            <li class="nav-item dropdown ">
                @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper=='0')
                    <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="btn-user-text" id="ownerLogin">
                            @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                                {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                            @elseif(Session::get('owner')!='')
                                {{Session::get('owner')}}
                            @else
                                Account
                            @endif
                        </span>

                        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo!='')
                            <img class="rental-profile-pic" src="{{URL::asset('uploads/users/'. \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo)}}" alt="user" />
                        @else
                            <img class="lazy-load rental-profile-pic desktop-hide profile-desktop-hide" data-src="{{URL::asset('media/images/user.svg')}}" alt="" />
                            <span class="user-img intial-desktop-show ">{{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name, 0, 1)}} {{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->last_name, 0, 1)}}</span>
                        @endif
                    </a>

                @elseif(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper=='1')
                    <a href="" class="tripper-login-btn" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo!='')
                            <span><img src="{{URL::asset('uploads/users/'. \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo)}}" class="rental-profile-pic" alt="profile pic"></span>
                        @else
                            <img class="lazy-load rental-profile-pic desktop-hide profile-desktop-hide" data-src="{{URL::asset('media/images/user.svg')}}" alt="" />    
                            <span class="text intial-desktop-show">{{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name, 0, 1)}} {{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->last_name, 0, 1)}}</span>
                        @endif
                    </a>
                @else
                    <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="btn-user-text" id="ownerLogin">
                            @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                                {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                            @elseif(Session::get('owner')!='')
                                {{Session::get('owner')}}
                            @else
                                Account
                            @endif
                        </span> <img src="{{URL::asset('media/images/user.svg')}}" alt="user" />
                    </a>
                @endif

                <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="drpresponsivemenu">

                    @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user() != '' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper != '1')
                        <a class="dropdown-item active-item-link" href="{{route('tripper.singUpForm')}}">Become a tripper</a>
                    @endif

                    @php
                    if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!= ''){
                        $tipperClass = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? 'active-item-link' : '';
                        $label = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? 'Dashboard' : 'My Account';
                        $route = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? route('tripper.dashboard') : url('setting');
                    } else {
                        $tipperClass = 'active-item-link';
                        $label = 'Become a tripper';
                        $route = route('tripper.index');
                    }
                    @endphp
                    <a class="dropdown-item {{$tipperClass}}" href="{{$route}}">{{$label}}</a>

                    {{-- @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!= '')
                        <a class="dropdown-item" href="{{route('tripboards')}}">Tripboards</a>
                    @else
                        <a class="dropdown-item login-modal" data-action="{{route('tripboards')}}" href="" data-toggle="modal" data-target="#login_popup">Tripboards</a>
                    @endif --}}

                    {{-- <a class="dropdown-item " href="#">Rent a Stay</a> --}}
                    <a class="dropdown-item extra-bold" href="{{ (Session::get('user')!='') ? ((getOwnerPropertyCount()->count() > 0) ?route('owner.property'): route('property.create') ) : route('owner.list-your-stay') }}">
                        @if(Session::get('user')!='' && getOwnerPropertyCount()->count() > 0)
                            My Listings
                        @else
                            List a Stay
                        @endif
                    </a>
                    <a class="dropdown-item font-light" href="#">Help</a>

                    @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                        {{-- <a class="dropdown-item font-light" style="cursor: pointer" href="{{ url('setting') }}">My Account </a> --}}
                        <a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutRental(`{{route('user.logout')}}`)">Logout </a>
                    @else
                        <a class="dropdown-item font-light" style="cursor: pointer" href="" data-toggle="modal" data-target="#login_popup">Login </a>
                    @endif

                </div>
            </li>
        </ul>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="responsive-block-logo test">
                <div class="res-logo"><img class="lazy-load" data-src="{{URL::asset('media/images/site-logo.png')}}" alt=""></div>
                <div class="res-close"><button type="button" id="closemenu"><img class="lazy-load" data-src="{{URL::asset('media/images/main-navigation/close-pink-icon.svg')}}" alt=""></button></div>

            </div>
            @include('layouts.topNav')

            <ul id="sidenav" class="navbar-nav btnlinks">
                @if(!empty(getLoggedInRental()) && !isLoggedInUserIsTripper())
                    <a href="{{route('tripper.singUpForm')}}"><img src="{{URL::asset('media/images/trippersOnly.png')}}" alt="tripboard" /></a>
                @elseif(empty(getLoggedInRental()))
                    <li class="nav-item">
                        <a href="{{route('tripper.index')}}" class="nav-link btn-tripper"><img src="{{URL::asset('media/images/btn-tripper.svg')}}" alt="tripper" /></a>
                    </li>
                @endif

                <li class="nav-item dropdown ">
                    @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper=='0')
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="btn-user-text" id="ownerLogin">
                                @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                                    {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                                @elseif(Session::get('owner')!='')
                                    {{Session::get('owner')}}
                                @else
                                    Account
                                @endif
                            </span>

                            @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo!='')
                                <img class="rental-profile-pic" src="{{URL::asset('uploads/users/'. \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo)}}" alt="user" />
                            @else
                            <img class="lazy-load rental-profile-pic desktop-hide profile-desktop-hide" data-src="{{URL::asset('media/images/user.svg')}}" alt="" />
                                <span class="user-img  intial-desktop-show ">{{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name, 0, 1)}} {{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->last_name, 0, 1)}}</span>
                            @endif
                        </a>

                    @elseif(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper=='1')
                        <a href="" class="tripper-login-btn" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                            @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo!='')
                                <span><img src="{{URL::asset('uploads/users/'. \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo)}}" class="rental-profile-pic" alt="profile"></span>
                            @else
                            <img class="lazy-load rental-profile-pic desktop-hide profile-desktop-hide" data-src="{{URL::asset('media/images/user.svg')}}" alt="" />
                                <span class="text intial-desktop-show">{{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name, 0, 1)}} {{substr(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->last_name, 0, 1)}}</span>
                            @endif
                        </a>
                    @else
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="btn-user-text" id="ownerLogin">
                                @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                                    {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}}
                                @elseif(Session::get('owner')!='')
                                    {{Session::get('owner')}}
                                @else
                                    Account
                                @endif
                            </span> <img class="lazy-load" data-src="{{URL::asset('media/images/user.svg')}}" alt="user" />
                        </a>
                    @endif

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user() != '' && \Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper != '1')
                            <a class="dropdown-item active-item-link" href="{{route('tripper.singUpForm')}}">Become a tripper</a>
                        @endif

                        @php
                        if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!= ''){
                            $tipperClass = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? 'active-item-link' : '';
                            $label = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? 'Dashboard' : 'My Account';
                            $route = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? route('tripper.dashboard') : url('setting');
                        } else {
                            $tipperClass = 'active-item-link';
                            $label = 'Become a tripper';
                            $route = route('tripper.index');
                        }
                        @endphp
                        <a class="dropdown-item {{$tipperClass}}" href="{{$route}}">{{$label}}</a>

                       {{--
                        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!= '')
                            <a class="dropdown-item" href="{{route('tripboards')}}">Tripboards</a>
                        @else
                            <a class="dropdown-item login-modal" data-action="{{route('tripboards')}}" href="" data-toggle="modal" data-target="#login_popup">Tripboards</a>
                        @endif --}}

                        {{-- <a class="dropdown-item" href="#">Rent a Stay</a> --}}

                        <a class="dropdown-item extra-bold" href="{{ (Session::get('user')!='') ? ((getOwnerPropertyCount()->count() > 0) ?route('owner.property'): route('property.create') ) : route('owner.list-your-stay') }}">
                            @if(Session::get('user')!='' && getOwnerPropertyCount()->count() > 0)
                                My Listings
                            @else
                                List a Stay
                            @endif
                        </a>
                        <a class="dropdown-item font-light" href="#">Help</a>

                        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                            {{-- <a class="dropdown-item font-light" style="cursor: pointer" href="{{ ( Auth::guard('rentaluser')->user()->is_tripper == '1') ? url('dashboard') :url('setting')}}">
                                {{ ( Auth::guard('rentaluser')->user()->is_tripper == '1') ? 'Dashboard' : 'My Account' }}
                            </a> --}}
                            <a class="dropdown-item font-light" style="cursor: pointer" onclick="logoutRental(`{{route('user.logout')}}`)">Logout </a>
                        @else
                            <a class="dropdown-item font-light" style="cursor: pointer" href="" data-toggle="modal" data-target="#login_popup">Login </a>
                        @endif

                    </div>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0);" class="nav-link btn-search rental-search">
                        <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M5.29679 1.13934C2.48741 2.64773 0.734531 5.57825 0.734375 8.76696C0.767337 8.9849 0.794666 9.20457 0.822033 9.42454C0.880181 9.89194 0.938498 10.3607 1.05138 10.817C1.69201 13.4287 3.51583 15.5912 5.98201 16.6633C8.44818 17.7354 11.2736 17.594 13.6204 16.281C13.6891 16.2259 13.7774 16.2013 13.8647 16.213C13.9519 16.2247 14.0306 16.2717 14.0824 16.343C14.5553 16.8137 15.0314 17.2758 15.5076 17.738C15.6809 17.9061 15.8542 18.0743 16.0274 18.243C16.3761 18.5824 16.7245 18.9226 17.073 19.2628C17.9707 20.1391 18.8689 21.0159 19.7744 21.884C20.3043 22.4009 21.0722 22.5912 21.7823 22.3816C22.4923 22.1721 23.0337 21.5952 23.198 20.8734C23.3623 20.1515 23.1238 19.3972 22.5744 18.901C20.723 17.0943 18.864 15.2943 16.9974 13.501C16.9156 13.4469 16.8611 13.3602 16.8479 13.2631C16.8347 13.166 16.864 13.0679 16.9284 12.994C17.535 11.9441 17.9052 10.7746 18.0134 9.56696C18.3079 6.39188 16.8333 3.31192 14.1753 1.5504C11.5173 -0.211122 8.10617 -0.369052 5.29679 1.13934ZM4.23744 8.71664C4.24534 5.85381 6.57154 3.53891 9.43438 3.54496L9.43038 3.54096C12.2954 3.55965 14.6083 5.88691 14.6094 8.75196C14.5978 11.6148 12.2686 13.9267 9.40576 13.9169C6.54293 13.9072 4.22954 11.5795 4.23744 8.71664Z" fill="#939FA4"/>
                        </svg>
                    </a>
                    <div class="header-search-top">
                        <span>
                            <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.29679 1.13934C2.48741 2.64773 0.734531 5.57825 0.734375 8.76696C0.767337 8.9849 0.794666 9.20457 0.822033 9.42454C0.880181 9.89194 0.938498 10.3607 1.05138 10.817C1.69201 13.4287 3.51583 15.5912 5.98201 16.6633C8.44818 17.7354 11.2736 17.594 13.6204 16.281C13.6891 16.2259 13.7774 16.2013 13.8647 16.213C13.9519 16.2247 14.0306 16.2717 14.0824 16.343C14.5553 16.8137 15.0314 17.2758 15.5076 17.738C15.6809 17.9061 15.8542 18.0743 16.0274 18.243C16.3761 18.5824 16.7245 18.9226 17.073 19.2628C17.9707 20.1391 18.8689 21.0159 19.7744 21.884C20.3043 22.4009 21.0722 22.5912 21.7823 22.3816C22.4923 22.1721 23.0337 21.5952 23.198 20.8734C23.3623 20.1515 23.1238 19.3972 22.5744 18.901C20.723 17.0943 18.864 15.2943 16.9974 13.501C16.9156 13.4469 16.8611 13.3602 16.8479 13.2631C16.8347 13.166 16.864 13.0679 16.9284 12.994C17.535 11.9441 17.9052 10.7746 18.0134 9.56696C18.3079 6.39188 16.8333 3.31192 14.1753 1.5504C11.5173 -0.211122 8.10617 -0.369052 5.29679 1.13934ZM4.23744 8.71664C4.24534 5.85381 6.57154 3.53891 9.43438 3.54496L9.43038 3.54096C12.2954 3.55965 14.6083 5.88691 14.6094 8.75196C14.5978 11.6148 12.2686 13.9267 9.40576 13.9169C6.54293 13.9072 4.22954 11.5795 4.23744 8.71664Z" fill="#939FA4"/>
                            </svg>
                        </span>
                        <div>
                        <input type="text" placeholder="Type to search..." class="mainSearchTxt" />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="menu-overlay"></div>


