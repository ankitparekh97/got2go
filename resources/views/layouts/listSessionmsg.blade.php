@if(session('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{session('success')}}
    </div>
@endif
@if(session('warning'))
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        {{session('warning')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        {{session('error')}}
    </div>
@endif
@if(Session::has('success_msg'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{Session::get('success_msg')}}
    </div>
@endif
@if(Session::has('warning_msg'))
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        {{Session::get('warning_msg')}}
    </div>
@endif
@if(Session::has('danger_msg'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        {{Session::get('danger_msg')}}
    </div>
@endif