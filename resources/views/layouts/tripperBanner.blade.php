<div class="tripper-banner" style="background-image: url({{URL::asset('media/images/tripper-page/tripper-banner.jpg') }});">
    <div class="banner-comtent">
        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/trippersonlywhite.png') }}" alt="tripper"/>

        @if(Request::segment(1) == 'tripper')
            <a href="javascript:void(0)">Become a Tripper</a>
        @elseif(Request::segment(1) == 'sign-up')
            <a href="javascript:void(0)">Registration</a>
        @endif
    </div>
</div>
