

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                      <a href="" class="footer-responsive-logo-none"><img class="lazy-load" data-src="{{URL::asset('media/images/footer-logo.svg')}}" alt="logo" /></a>
                      <a href="" class="footer-responsive-logo"><img class="lazy-load" data-src="{{URL::asset('media/images/main-navigation/got2go-mobile-logo.svg')}}" alt="logo" /></a>
                    </div>
                </div>
                <div class="col-md-8 pt-4 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="nav-item {{((Request::segment(1) == '' || Request::segment(1) == 'search') ? 'active' : '') }}">
                                <a href="{{url('/')}}" class="">Home </a>
                                </li>
                                <li class="nav-item {{((Request::segment(1) == 'view-stays' || Request::segment(1) == 'propertydetail') ? 'active' : '') }}">
                                    <a href="{{route('view-stays')}}" class="">View Stays</a>
                                </li>
                                <li class="nav-item">
                                    <a  href="{{route('ownerloginform')}}" class="nav-link ">List your Stay</a>
                                </li>
                                <li class="nav-item {{((Request::segment(1) == 'tripboards'  || Request::segment(1) == 'tripboard-detail' ) ? 'active' : '') }}">
                                    <a href="{{route('tripboards')}}" class="">Tripboards </a>
                                </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 pl-1 pt-6 text-right">
                    <div class="footer-contact">
                        <a href="tel:(844) I-GOT2GO"><img src="{{URL::asset('media/images/tripper-page/contact-img.png')}}" alt="logo" />(844) I-GOT2GO</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="global-footer-bottom-strip">
            <div class="container">

                <ul class="footer-bottom-strip-list social-media-list-icons d-inline-block float-right">
                    <li><a href="" data-toggle="modal" data-target="#login_popup">
                            <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.1519 11.6152V19H1.18671C0.865308 19 0.587174 18.8825 0.352304 18.6475C0.117435 18.4124 0 18.1341 0 17.8125V1.1875C0 0.865885 0.117435 0.587565 0.352304 0.352539C0.587174 0.117513 0.865308 0 1.18671 0H17.8006C18.122 0 18.4002 0.117513 18.635 0.352539C18.8699 0.587565 18.9873 0.865885 18.9873 1.1875V17.8125C18.9873 18.1341 18.8699 18.4124 18.635 18.6475C18.4002 18.8825 18.122 19 17.8006 19H13.0538V11.6152H15.4272L15.8259 8.87842H13.0538V6.72607C13.0538 6.33024 13.1542 6.02718 13.3551 5.81689C13.556 5.60661 13.8511 5.50146 14.2405 5.50146H16.0206V3.03369C15.489 2.9471 14.84 2.90381 14.0736 2.90381C12.8004 2.90381 11.8285 3.22697 11.1578 3.87329C10.4872 4.51961 10.1519 5.39941 10.1519 6.5127V8.87842H7.77851V11.6152H10.1519Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="" data-toggle="modal" data-target="#login_popup">
                            <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.74418 15.5601C8.70272 15.8693 7.60409 16.0239 6.44828 16.0239C4.10577 16.0239 2.11247 15.4951 0.468384 14.4375C0.752699 14.4808 1.06174 14.5024 1.3955 14.5024C2.26081 14.5024 3.10757 14.3354 3.9358 14.0015C4.76402 13.6675 5.52735 13.2191 6.22578 12.6562C5.39137 12.6501 4.63887 12.3918 3.96825 11.8816C3.29764 11.3713 2.8418 10.7266 2.60075 9.94727C2.86035 9.99674 3.1014 10.0215 3.3239 10.0215C3.68239 10.0215 4.02851 9.9751 4.36227 9.88232C3.46606 9.70296 2.72128 9.2561 2.12792 8.54175C1.53457 7.82739 1.23789 7.00016 1.23789 6.06006V6.01367C1.79416 6.31673 2.38133 6.47754 2.99941 6.49609C2.46168 6.13737 2.03676 5.6735 1.72463 5.10449C1.4125 4.53548 1.25644 3.92318 1.25644 3.26758C1.25644 2.63053 1.43258 1.99967 1.78489 1.375C2.76145 2.5625 3.94198 3.50415 5.32647 4.19995C6.71097 4.89575 8.20363 5.28695 9.80445 5.37354C9.74882 5.13232 9.721 4.83236 9.721 4.47363C9.721 3.39746 10.0996 2.48055 10.8567 1.7229C11.6139 0.965251 12.5302 0.586426 13.6056 0.586426C14.712 0.586426 15.6669 0.994629 16.4704 1.81104C17.3543 1.63167 18.1732 1.31624 18.9273 0.864746C18.6306 1.80485 18.0589 2.52539 17.2121 3.02637C17.9847 2.93978 18.7326 2.73258 19.4557 2.40479C18.9365 3.19027 18.2907 3.86442 17.5181 4.42725V4.9375C17.5181 5.80339 17.4083 6.67391 17.1889 7.54907C16.9695 8.42423 16.6543 9.27466 16.2433 10.1003C15.8322 10.926 15.3053 11.6976 14.6625 12.415C14.0197 13.1325 13.2997 13.7587 12.5024 14.2937C11.705 14.8287 10.7856 15.2508 9.74418 15.5601Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="" data-toggle="modal" data-target="#login_popup">
                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.4052 19C17.1716 19 17.8252 18.7294 18.3661 18.1882C18.9069 17.6471 19.1773 16.993 19.1773 16.2261V2.77393C19.1773 2.007 18.9069 1.35295 18.3661 0.811768C17.8252 0.27059 17.1716 0 16.4052 0H2.96202C2.1956 0 1.54199 0.27059 1.00117 0.811768C0.46035 1.35295 0.189941 2.007 0.189941 2.77393V16.2261C0.189941 16.993 0.46035 17.6471 1.00117 18.1882C1.54199 18.7294 2.1956 19 2.96202 19H16.4052ZM2.56336 17.0239H16.8039C16.9151 17.0239 17.0094 16.9853 17.0866 16.908C17.1639 16.8306 17.2025 16.7363 17.2025 16.625V8.3125H15.098C15.1783 8.67741 15.2185 9.07324 15.2185 9.5C15.2185 10.502 14.9713 11.4281 14.4768 12.2786C13.9823 13.129 13.3102 13.8016 12.4603 14.2964C11.6105 14.7912 10.6849 15.0386 9.68362 15.0386C8.68233 15.0386 7.75676 14.7912 6.9069 14.2964C6.05704 13.8016 5.38488 13.129 4.89042 12.2786C4.39596 11.4281 4.14873 10.502 4.14873 9.5C4.14873 9.07324 4.18891 8.67741 4.26926 8.3125H2.1647V16.625C2.1647 16.7363 2.20333 16.8306 2.28059 16.908C2.35785 16.9853 2.45211 17.0239 2.56336 17.0239ZM9.68362 13.0625C8.70087 13.0625 7.86183 12.7146 7.1665 12.0188C6.47116 11.323 6.12349 10.4834 6.12349 9.5C6.12349 8.5166 6.47116 7.677 7.1665 6.9812C7.86183 6.2854 8.70087 5.9375 9.68362 5.9375C10.6664 5.9375 11.5039 6.28695 12.1961 6.98584C12.8945 7.67855 13.2437 8.5166 13.2437 9.5C13.2437 10.4834 12.8961 11.323 12.2007 12.0188C11.5054 12.7146 10.6664 13.0625 9.68362 13.0625ZM14.4305 5.14893H16.8039C16.9151 5.14893 17.0094 5.11027 17.0866 5.03296C17.1639 4.95565 17.2025 4.86133 17.2025 4.75V2.375C17.2025 2.26367 17.1639 2.16935 17.0866 2.09204C17.0094 2.01473 16.9151 1.97607 16.8039 1.97607H14.4305C14.3192 1.97607 14.2249 2.01473 14.1477 2.09204C14.0704 2.16935 14.0318 2.26367 14.0318 2.375V4.75C14.0318 4.86133 14.0704 4.95565 14.1477 5.03296C14.2249 5.11027 14.3192 5.14893 14.4305 5.14893Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="" data-toggle="modal" data-target="#login_popup">
                            <svg width="15" height="19" viewBox="0 0 15 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.3118 18.9954C3.37052 19.0108 3.4246 18.9907 3.47404 18.9351C3.5853 18.8052 3.73981 18.6042 3.93759 18.332C4.13538 18.0599 4.39961 17.627 4.73029 17.0332C5.06096 16.4395 5.29428 15.8859 5.43025 15.3726C5.52914 15.0509 5.78566 14.0552 6.19977 12.3853C6.39755 12.7563 6.74831 13.0718 7.25204 13.3315C7.75578 13.5913 8.29814 13.7212 8.87914 13.7212C10.0226 13.7212 11.0424 13.3965 11.9386 12.7471C12.8348 12.0977 13.5271 11.2055 14.0154 10.0706C14.5036 8.93563 14.7478 7.65999 14.7478 6.24365C14.7478 5.43962 14.5809 4.66032 14.2471 3.90576C13.9134 3.1512 13.4529 2.48478 12.8657 1.90649C12.2785 1.32821 11.5477 0.865885 10.6731 0.519531C9.79852 0.173177 8.85441 0 7.84076 0C6.83947 0 5.90772 0.137614 5.0455 0.412842C4.18328 0.68807 3.45241 1.05298 2.85287 1.50757C2.25334 1.96216 1.74033 2.48633 1.31386 3.08008C0.887385 3.67383 0.572159 4.28768 0.368195 4.92163C0.164229 5.55558 0.0622559 6.18799 0.0622559 6.81885C0.0622559 7.79606 0.250765 8.65885 0.627792 9.40723C1.00482 10.1556 1.55801 10.6844 2.28734 10.9937C2.42332 11.0431 2.54074 11.0431 2.63963 10.9937C2.73852 10.9442 2.80343 10.8483 2.83434 10.7061C2.96413 10.2236 3.0383 9.93294 3.05684 9.83398C3.08774 9.69792 3.09392 9.59587 3.07538 9.52783C3.05684 9.4598 2.99812 9.36393 2.89923 9.24023C2.41713 8.65267 2.17607 7.93213 2.17607 7.07861C2.17607 6.39209 2.30279 5.73494 2.5562 5.10718C2.80961 4.47941 3.16345 3.92432 3.61774 3.44189C4.07202 2.95947 4.64221 2.57601 5.32828 2.2915C6.01434 2.007 6.75912 1.86475 7.56262 1.86475C8.9842 1.86475 10.0937 2.25285 10.891 3.02905C11.6883 3.80526 12.0869 4.81185 12.0869 6.04883C12.0869 7.10026 11.9448 8.0651 11.6605 8.94336C11.3762 9.82162 10.9698 10.5267 10.4413 11.0586C9.91286 11.5905 9.31178 11.8564 8.63808 11.8564C8.05708 11.8564 7.58888 11.6477 7.23349 11.2302C6.8781 10.8127 6.7653 10.3164 6.8951 9.74121C6.97545 9.41341 7.10369 8.96965 7.27985 8.40991C7.456 7.85018 7.59817 7.36003 7.70633 6.93945C7.81449 6.51888 7.86858 6.16325 7.86858 5.87256C7.86858 5.39632 7.74186 5.00513 7.48845 4.69897C7.23504 4.39282 6.87037 4.23975 6.39445 4.23975C5.80728 4.23975 5.31282 4.50724 4.91107 5.04224C4.50932 5.57723 4.30844 6.24984 4.30844 7.06006C4.30844 7.74658 4.41661 8.32487 4.63293 8.79492L3.25153 14.7324C3.18354 15.0107 3.1341 15.3138 3.1032 15.6416C3.07229 15.9694 3.05529 16.2694 3.0522 16.5415C3.04911 16.8136 3.05375 17.0966 3.06612 17.3904C3.07848 17.6842 3.09393 17.9223 3.11247 18.1047C3.13101 18.2872 3.1511 18.4619 3.17273 18.6289C3.19436 18.7959 3.20517 18.8825 3.20517 18.8887C3.21753 18.9443 3.25308 18.9799 3.3118 18.9954Z" fill="" />
                            </svg>

                        </a></li>
                </ul>
                <ul class="footer-bottom-strip-list d-inline-block">
                    <li><a><strong>© 2020 GOT2GO. </strong> All rights reserved</a></li>
                    <li class="mobile-hide">|</li>
                    <li><a href="">Privacy </a></li>
                    <li class="mobile-hide">|</li>
                    <li><a href="">Terms and Conditions</a></li>
                    <li class="mobile-hide">|</li>
                    <li><a href="">About</a></li>
                </ul>
            </div>
        </div>
    </footer>


<script>


    const APP_URL = '{{ URL::to('') }}';

$('#moburger').on('click', function() {
  $(".menu-overlay").show();
});
$(".menu-overlay").click(function(event) {
  $("#moburger").trigger("click");
  $(".menu-overlay").hide();
});
$("#closemenu").click(function(event) {
  $("#moburger").trigger("click");
  $(".menu-overlay").hide();
});


    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    //get user timezone
    $('body form').append('<input type="hidden" id="timezone" name="timezone" value="'+Intl.DateTimeFormat().resolvedOptions().timeZone+'">');

    var isRentalAuth = false;
    @if(Auth::guard('rentaluser')->user())
        isRentalAuth = true;
    @endif

    $(document).ready(function() {
        $('ul#mainnavigatn li.nav-item a').click(function() {
            $('li.nav-item a').removeClass("active-menu-link");
            $(this).addClass("active-menu-link");
        });
    });

    var loaderImage = "{{URL::asset('media/images/loader.gif')}}";
    function showLoader(){
        $.blockUI({
            message: '<img src="'+loaderImage+'" class="rounded-circle" />',
            css: {
                border: 'none',
                backgroundColor: 'transparent !important',
            },
            baseZ: 2000
        });
    }

    function hideLoader(){
        $.unblockUI();
    }

    function logoutRental(url){
        showLoader();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: url,
            type: 'GET',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
            },
            success: function (data) {
                hideLoader();
                if(data.success){
                    // window.onbeforeunload = null;
                    window.location = data.redirect_url;

                }
            }
        });
    }

    function logoutOwner(url){
        showLoader();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: url,
            type: 'GET',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
            },
            success: function (data) {
                hideLoader();
                if(data.success){
                    window.onbeforeunload = null;
                    window.location = data.redirect_url;
                }
            }
        });
    }

    let placeSearch;
    let autocompleteTripper;
    const componentForm = {
    city: "long_name",
    state: "short_name",
    };
    function initialize() {

        var options = {
            types: ['(cities)'],
            componentRestrictions: {country: "us"}
        };

        var input = document.getElementById('location_detail');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        var input1 = document.getElementById('modal_location_detail');
        var autocomplete = new google.maps.places.Autocomplete(input1, options);

        var options = {
            componentRestrictions: {country: "us"},
            types: ["geocode"]
        };

        var billing_address = document.getElementById('address');
        autocompleteTripper = new google.maps.places.Autocomplete(billing_address, options);
        autocompleteTripper.setFields(["address_component"]);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocompleteTripper.addListener("place_changed", fillInAddress);


    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        const place = autocompleteTripper.getPlace();
        let streetAddress = '';

            document.getElementById('address').value = '';
            for (const component in componentForm) {

                // console.log( document.getElementById(component).value );
                document.getElementById(component).value = "";
                document.getElementById(component).disabled = false;
            }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
            for (const component of place.address_components) {
                const addressType = component.types[0];
                console.log(addressType,component);

                if(addressType == "locality" || addressType == "sublocality_level_1"){
                    const val = component['long_name'];
                    document.getElementById('city').value = val;
                    $('#city').attr('readonly', true);
                }
                else if(addressType == "administrative_area_level_1"){
                    const val = component['short_name'];
                    document.getElementById('state').value = val;
                    $('#state').attr('readonly', true);
                }
                else if(addressType == "street_number"){

                    // var street = place.address_components[1];
                    streetAddress = component['long_name'];
                    document.getElementById('address').value = streetAddress;
                }
                else if(addressType == "route"){

                    // var street = place.address_components[1];
                    // const val = component['long_name']+' '+street['long_name'];
                    streetAddress += ' '+component['long_name'];
                    document.getElementById('address').value = streetAddress;
                }

            }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
            const geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            const circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocompleteTripper.setBounds(circle.getBounds());
            });
        }
        }

        const propertyOfferSettingData = {
            status : {
                pending : 'pending',
                counterOffer : 'counter_offer',
                accepted : 'accepted',
                rejected : 'rejected',
                cancel : 'cancel',
            },
            actionBy : {
                owner : 'owner',
                rental : 'rental',
                system : 'system'
            }
        }

    HelperFunction = {
        numberWithCommas : function (num) {
            let fixedToLast2Digit = parseFloat(parseFloat((num)).toFixed(2));
            return fixedToLast2Digit.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        },
        roundNumber : function (num) {
            return Math.round(parseFloat(num));
        },
        calculateTotalPrice : function(startDate,endDate,nightlyPrice) {
            let offerStartDate = moment(startDate);
            let offerEndDate = moment(endDate);
            let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate,offerEndDate);
            if(totalNightsToStay > 0) {
                return (totalNightsToStay * nightlyPrice);
            }
            return nightlyPrice;
        },
        dateDiffInDays : function (startDate,endDate) {
            let offerStartDate = moment(startDate).startOf('day');
            let offerEndDate = moment(endDate).startOf('day');
            return offerEndDate.diff(offerStartDate,'days',true);
        }
    }
</script>
</body>

</html>
