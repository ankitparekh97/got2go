<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @section('headerCss')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="{{URL::asset('css/rental/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/owl.theme.default.min.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800,900' rel='stylesheet'>
    <link rel="stylesheet" href="{{URL::asset('plugins/global/plugins.bundle.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.bundle.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/rental/rental1.css')}}">
    @show

    <link rel="icon" href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">
    <title>GOT2GO TRAVEL</title>

    @section('headerJs')
    @show
    <title>Trippers</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    @include('layouts.tripperHeader')
    <div class="menu-overlay"></div>
    @include('layouts.tripperBanner')

    <div class="page-content-start tripper-page @if(Request::segment(1) == 'sign-up') registration-form no-padding @endif  @if(Request::segment(1) == 'reg-success') registration no-padding @endif @if(Request::segment(1) == 'register') no-padding @endif">

        @if(Request::segment(1) == 'tripper')
            @include('layouts.tripperMembership')
            @include('layouts.tripperSlider')
        @endif

        @yield('content')

        @include('layouts.rentalSubsubribe')
        @include('layouts.rentalFooter')
    </div>
    @include('rentaluser.login')
    @include('rentaluser.signup')
    @include('rentaluser.forgot-password')

    @section('footerJs')
        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&callback=initialize&libraries=places&v=weekly"
                defer
        ></script>

        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHWR2OoJUyZOvAL1GZBkdWH_IOi-0tYTY&callback=initialize&libraries=places&v=weekly"
                defer
        ></script>

        <script src="{{URL::asset('plugins/global/plugins.bundle.js')}}"></script>
        <script src="{{URL::asset('js/scripts.bundle.js')}}"></script>
        <script src="{{URL::asset('js/select2.js') }}"></script>
        <script src="{{URL::asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
        <script src="{{ URL::asset('js/commonHelpers.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js" integrity="sha512-mh+AjlD3nxImTUGisMpHXW03gE6F4WdQyvuFRkjecwuWLwD2yCijw4tKA3NsEFpA1C3neiKhGXPSIGSfCYPMlQ==" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.min.js')}}"></script>
        <script src="{{URL::asset('js/lazy-load.js')}}"></script>
        
        <script>
        var searchPath = "{{route('rental.search')}}";
        $( ".rental-search" ).click(function() {
        $(this).parent().toggleClass('active');
        $('.mainSearchTxt').focus()
        });
        $( document ).ready(function() {
            $('.mainSearchTxt').on("keyup", function(e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    var data = {
                        "property_type":['property','hotels','vacation_rental'],
                        "adults":1,
                        "child":0,
                        "searchText": $(this).val(),
                        "pageno":0,
                        "number_of_guest":1,
                    }
                    if($(this).val().trim() == ''){
                        return false;
                    }
                    window.location.href = searchPath+"?"+$.param(data);
                }
            });

            $('.search-icon-mobile').on('click',function(e){
                var data = {
                        "property_type":['property','hotels','vacation_rental'],
                        "adults":1,
                        "child":0,
                        "searchText": $('.mainSearchTxt').val(),
                        "pageno":0,
                        "number_of_guest":1,
                    }
                window.location.href = searchPath+"?"+$.param(data);
            });
        });

        </script>
        
        <script>

            jQuery(".tripper-banner .banner-comtent a").click(function() {
                jQuery('html,body').animate({
                        scrollTop: jQuery("#tripper-reg").offset().top},
                    1000);
            });

            const APP_URL = '{{ URL::to('') }}';
            var loaderImage = "{{URL::asset('media/images/loader.gif')}}";


            function showLoader(){
                $.blockUI({
                    message: '<img src="'+loaderImage+'" alt="circle" class="rounded-circle" />',
                    css: {
                        border: 'none',
                        backgroundColor: 'transparent !important',
                    },
                    baseZ: 2000
                });
            }

            function hideLoader(){
                $.unblockUI();
            }

            $('#moburger').on('click', function() {
                $(".menu-overlay").show();
            });

            $(".menu-overlay").click(function(event) {
                $("#moburger").trigger("click");
                $(".menu-overlay").hide();
            });

            $("#closemenu").click(function(event) {
                $("#moburger").trigger("click");
                $(".menu-overlay").hide();
            });

            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            //get user timezone
            $('body form').append('<input type="hidden" id="timezone" name="timezone" value="'+Intl.DateTimeFormat().resolvedOptions().timeZone+'">');

            var isRentalAuth = false;
            @if(Auth::guard('rentaluser')->user())
                isRentalAuth = true;
            @endif

            function logoutRental(url){
                showLoader();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    headers: {
                        "Authorization": "Bearer {{session()->get('rental_token')}}",
                    },
                    success: function (data) {
                        hideLoader();
                        if(data.success){
                            window.location = data.redirect_url;
                            // location.reload();
                        }
                    }
                });
            }

            function logoutOwner(url){
                showLoader();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    headers: {
                        "Authorization": "Bearer {{session()->get('owner_token')}}",
                    },
                    success: function (data) {
                        hideLoader();
                        if(data.success){
                            window.location = data.redirect_url;
                            // location.reload();
                        }
                    }
                });
            }

            $(document).ready(function() {
                $('ul#mainnavigatn li.nav-item a').click(function() {
                    $('li.nav-item a').removeClass("active-menu-link");
                    $(this).addClass("active-menu-link");
                });
            });

            let placeSearch;
            let autocompleteTripper;
            const componentForm = {
                city: "long_name",
                state: "short_name",
            };

            function initialize() {

                var options = {
                    types: ['(cities)'],
                    componentRestrictions: {country: "us"}
                };

                var input = document.getElementById('location_detail');
                var autocomplete = new google.maps.places.Autocomplete(input, options);
                var input1 = document.getElementById('modal_location_detail');
                var autocomplete = new google.maps.places.Autocomplete(input1, options);

                var options = {
                    componentRestrictions: {country: "us"},
                    types: ["geocode"]
                };

                var billing_address = document.getElementById('address');
                autocompleteTripper = new google.maps.places.Autocomplete(billing_address, options);
                autocompleteTripper.setFields(["address_component"]);
                // When the user selects an address from the drop-down, populate the
                // address fields in the form.
                autocompleteTripper.addListener("place_changed", fillInAddress);


            }

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                const place = autocompleteTripper.getPlace();
                let streetAddress = '';

                document.getElementById('address').value = '';
                for (const component in componentForm) {
                    document.getElementById(component).value = "";
                    document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details,
                // and then fill-in the corresponding field on the form.
                for (const component of place.address_components) {
                    const addressType = component.types[0];
                    console.log(addressType,component);

                    if(addressType == "locality" || addressType == "sublocality_level_1"){
                        const val = component['long_name'];
                        document.getElementById('city').value = val;
                        $('#city').attr('readonly', true);
                    }
                    else if(addressType == "administrative_area_level_1"){
                        const val = component['short_name'];
                        document.getElementById('state').value = val;
                        $('#state').attr('readonly', true);
                    }
                    else if(addressType == "street_number"){
                        streetAddress = component['long_name'];
                        document.getElementById('address').value = streetAddress;
                    }
                    else if(addressType == "route"){
                        streetAddress += ' '+component['long_name'];
                        document.getElementById('address').value = streetAddress;
                    }

                }
            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(position => {
                        const geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        const circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocompleteTripper.setBounds(circle.getBounds());
                    });
                }
            }
        </script>
    @show
</body>

</html>
