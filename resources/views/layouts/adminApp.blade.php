<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>GOT2GO | Admin</title>

    @section('headerCss')
        <!-- Custom fonts for this template-->
        <link href="{{URL::asset('css/all.min.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;900&display=swap" rel="stylesheet">
        <link href="{{URL::asset('css/webfonts/got2gofont.css')}}" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="{{URL::asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/admin.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/admin2.css')}}" rel="stylesheet">
    @show

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"  href="{{URL::asset('media/images/Gtog.png')}}" type="image/png">

    @section('headerJs')
        <!-- Bootstrap core JavaScript-->
        <script src="{{URL::asset('js/jquery/jquery.min.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{URL::asset('js/jquery-easing/jquery.easing.min.js')}}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{URL::asset('js/sb-admin-2.min.js')}}"></script>
        <!-- Page level plugins -->
        <script src="{{URL::asset('js/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::asset('js/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Page level custom scripts -->
        <script src="{{URL::asset('js/datatables-demo.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script>
        <script src="{{ URL::asset('js/commonHelpers.js') }}"></script>
        <script>

            $(document).ready(function(){
                $('#sidebarToggleTop').click(function(){
                    $('ul.sidebar').toggleClass("toggled");
                });
            });


        </script>
        <script src="{{URL::asset('js/lazy-load.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.lazy.plugins.min.js')}}"></script>
    @show
</head>
<body id="page-top">
    @include('admin.adminHeader')
    <!-- Page Wrapper -->
    <div id="wrapper">
        @include('admin.adminSidebar')
        @yield('content')
        @include('admin.adminFooter')
    </div>
    @section('footerJs')
        <script src="{{ URL::asset('js/admin/login/logout.js') }}"></script>
    @show
</body>
</html>
