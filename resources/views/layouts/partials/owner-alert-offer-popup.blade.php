@php
    $showHideClass = '';
    if(empty($count)){
        $showHideClass = 'd-done';
    } else {
        $routeMyOffers = route('property.booking.list')."#offers";
        $currentUrl = url()->current()."#offers";
        if($routeMyOffers == $currentUrl){
            $showHideClass = 'd-done';
        }
    }
@endphp
<!-- pop up box starts here -->
<div class="alert-popup-box-red @if(empty($count)) d-none @endif" id="alert-bar-offer-redirect">
    <a href="#" id="close-btn1-alert-bar" onclick="PropertyOfferNotification.closeAlertBar()" class="close-btn1" data-currentcount="{{ $count }}">
        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 2L13 13" stroke="white" stroke-width="3.5"></path>
            <path d="M2 13L13 2" stroke="white" stroke-width="3.5"></path>
        </svg>
    </a>
    <a href="{{ route('property.booking.list') }}#offers" target="_blank">
        You have <span id="offerBarCount">{{ $count }}</span> <span id="offerBarCountText"></span>{{ Str::plural('offer', $count) }}</span> pending your response, click here to respond to the Tripper
        <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="0.511719" width="25.4881" height="24" fill="black" fill-opacity="0.01"/>
            <path d="M5.82227 12C5.82227 11.4477 6.29774 11 6.88427 11H19.6283C20.2148 11 20.6903 11.4477 20.6903 12C20.6903 12.5523 20.2148 13 19.6283 13H6.88427C6.29774 13 5.82227 12.5523 5.82227 12Z" fill="white"/>
            <path d="M18.157 11.9701L13.0031 6.6761C12.7466 6.41279 12.6588 6.04064 12.7727 5.69984C12.8866 5.35903 13.185 5.10134 13.5554 5.02384C13.9258 4.94633 14.312 5.06079 14.5685 5.3241L20.4095 11.3241C20.7938 11.7188 20.7798 12.3285 20.3776 12.7071L14.0056 18.7071C13.739 18.9671 13.3441 19.0713 12.9728 18.9798C12.6015 18.8883 12.3115 18.6152 12.2143 18.2656C12.1171 17.916 12.2279 17.5442 12.5039 17.2931L18.157 11.9701Z" fill="white"/>
        </svg>
    </a>
</div>
@section('footerJs')
    @parent
    <script type="text/javascript">
        const readMakeAnOfferAndCounterOfferNotifications = '{{ route('api.owner.readMakeAnOfferAndCounterOfferNotifications') }}'

        PropertyOfferNotification = {
            closeAlertBar : function () {
                showLoader();

                $.ajax({
                    type: "POST",
                    url: readMakeAnOfferAndCounterOfferNotifications,
                    dataType: "JSON",
                    success: function (msg) {
                        hideLoader();
                        $(".alert-popup-box-red").addClass('d-none');
                        let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                        alertBarOfferRedirect.find("#close-btn1-alert-bar").attr('data-currentcount',0);
                        PropertyOfferNotification.showHideAlertBarAtOfferPage();
                    }
                });

            },
            updateOfferCardAlertBar : function(count) {

                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");

                PropertyOfferNotification.hideAlertBar();
                let currentcount = alertBarOfferRedirect.find("#close-btn1-alert-bar").attr('data-currentcount');

                alertBarOfferRedirect.find("#offerBarCount").html('');
                alertBarOfferRedirect.find("#offerBarCount").html(count);

                alertBarOfferRedirect.find("#close-btn1-alert-bar").attr('data-currentcount',count);
                let  getString = HelperFunction.getSingularOrPlural(count,'offer','offers');

                alertBarOfferRedirect.find("#offerBarCountText").html('');
                alertBarOfferRedirect.find("#offerBarCountText").html(getString);

                if(count > 0 && count != currentcount){
                    PropertyOfferNotification.showAlertBar();
                }

            },
            hideAlertBar: function () {
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                alertBarOfferRedirect.addClass('d-none');
            },
            showAlertBar: function () {
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                alertBarOfferRedirect.removeClass('d-none');
            },
            showHideAlertBarAtOfferPage: function () {
                PropertyOfferNotification.hideAlertBar();
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                let currentCount = alertBarOfferRedirect.find('#close-btn1-alert-bar').attr('data-currentcount');
                if(currentCount > 0){
                    PropertyOfferNotification.showAlertBar();
                }
            }
        }

    </script>

    <script>
        pusherChannel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
            if(data.type === PusherEventNotificationTypeConstant.propertyOffer.alertBar){
                let pendingResponseOnOfferByOwnerCount = parseInt(data.pendingResponseOnOfferByOwner);
                PropertyOfferNotification.updateOfferCardAlertBar(pendingResponseOnOfferByOwnerCount);
            }
        });
    </script>
@endsection
<!-- pop up box ends here -->
@php
    unset($showHideClass);
    unset($currentUrl);
    unset($routeMyOffers);
@endphp
