@php
    $showHideClass = 'd-none';
    if(route('web.tripper.myOffers') == url()->current() ){
            $showHideClass = 'd-none';
    } else if(!empty($count)){
        $showHideClass = '';
    }
@endphp

<!-- alert pop up starts here -->
<div class="alert-popup-box {{ $showHideClass }}" id="alert-bar-offer-redirect">
    <a href="javascript:void(0);" id="close-btn1-alert-bar" class="close-popup-btn1" onclick="PropertyOfferNotification.closeAlertBar()" data-currentcount="{{ $count }}">
        <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 2L13 13" stroke="white" stroke-width="3.5"/>
            <path d="M2 13L13 2" stroke="white" stroke-width="3.5"/>
        </svg>
    </a>
    <a href="{{ route('web.tripper.myOffers') }}" >
        Your <span id="offerBarCount">{{ $count }}</span> <span id="offerBarCountText">{{ Str::plural('offer', $count) }}</span> has been accepted! <br>
        Book this listing before it becomes unavailable to take advantage of this limited-time offer.
    </a>
</div>
<!-- alert pop up ends here -->

@section('footerJs')
    @parent
    <script type="text/javascript">
        const readOfferAcceptedNotifications = '{{ route('api.tripper.readOfferAcceptedNotifications') }}'

        PropertyOfferNotification = {
            closeAlertBar : function () {
                showLoader();

                $.ajax({
                    type: "POST",
                    url: readOfferAcceptedNotifications,
                    dataType: "JSON",
                    success: function (msg) {
                        hideLoader();
                        $(".alert-popup-box").addClass('d-none');
                        let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                        alertBarOfferRedirect.find("#offerBarCount").html('0');
                    }
                });
            },
            updateOfferCardAlertBar : function (count) {
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");

                PropertyOfferNotification.hideAlertBar();
                let currentcount = alertBarOfferRedirect.find("#close-btn1-alert-bar").attr('data-currentcount');

                alertBarOfferRedirect.find("#offerBarCount").html();
                alertBarOfferRedirect.find("#offerBarCount").html(count);

                alertBarOfferRedirect.find("#close-btn1-alert-bar").attr('data-currentcount',count);
                let  getString = HelperFunction.getSingularOrPlural(count,'offer','offers');

                alertBarOfferRedirect.find("#offerBarCountText").html(getString);
                if(count > 0 && count != currentcount){
                    PropertyOfferNotification.showAlertBar();
                }

            },
            hideAlertBar: function () {
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                alertBarOfferRedirect.addClass('d-none');
            },
            showAlertBar: function () {
                let alertBarOfferRedirect = $("#alert-bar-offer-redirect");
                alertBarOfferRedirect.removeClass('d-none');
            },
        }

    </script>

    <script type="text/javascript">
        pusherChannel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
            if(data.type === PusherEventNotificationTypeConstant.propertyOffer.alertBar){
                let pendingResponseOnOfferByTripperCount = parseInt(data.pendingResponseOnOfferByTripper);
                PropertyOfferNotification.updateOfferCardAlertBar(pendingResponseOnOfferByTripperCount);
            }
        });
    </script>

@endsection
<!-- pop up box ends here -->
