<div class="tripper-slider-outer" style="background-image: url({{URL::asset('media/images/tripper-page/tripper-slider-bg.png') }});">
    <div class="container">
        <div class="owl-carousel owl-theme owl-centered" id="tripper-page-carousal">
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Fairbanks,</span> Alaska</h4>
                    </div>
                    <div class="image">
                        <img src="{{URL::asset('media/images/tripper-page/tripper-slider-1.jpg') }}" alt="slider1" defer/>
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Jackson,</span> WYOMING</h4>
                    </div>
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-slider-2.jpg') }}" alt="slider2" />
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Portland,</span> Maine</h4>
                    </div>
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-slider-3.jpg') }}" alt="slider3" />
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Fairbanks,</span> Alaska</h4>
                    </div>
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-slider-1.jpg') }}" alt="slider1" />
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Jackson,</span> WYOMING</h4>
                    </div>
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-slider-2.jpg') }}" alt="slider2" />
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="tripper-carousal-div">
                    <div class="title">
                        <h4><span>Portland,</span> Maine</h4>
                    </div>
                    <div class="image">
                        <img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/tripper-slider-3.jpg') }}" alt="slider3" />
                    </div>
                    <div class="extra-info">
                        <h4>Extra Tripper Info</h4>
                        <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                        <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('footerJs')
    @parent
<script>
    jQuery('#tripper-page-carousal').owlCarousel({
            loop:true,
            margin:30,
            center: true,
            nav:true,
            dots: false,
            autoHeight: false,
            responsive:{
                0:{
                    items:1
                },
                640:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

</script>
@endsection