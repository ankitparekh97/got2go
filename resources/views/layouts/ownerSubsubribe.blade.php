</div>
<div class="live-life-travel text-center">
    <div class="container">
        <h3>Live Life Travel</h3>
        <p>Your exclusive deals are waiting.</p>
    </div>
    <div class="subscribe-box-outer">
        <div class="subscribe-box-inner">
            <form>
                <div class="input-box-cstm">
                    <span><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/msg-img.png')}}" alt="message"/></span>
                    <input type="text" placeholder="Signup to get notified on exclusive listings or hot deals!" />
                </div>
                <div class="submit-btn-cstm">
                    <input type="button" name="submit" value="Subscribe" />
                </div>
            </form>
        </div>
    </div>
</div>
