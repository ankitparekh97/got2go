 <!-- search for mobile starts here -->
<div class="search-mobile d-block d-sm-none d-md-none d-lg-none">
    <div class="search-mobile-input">
        <input type="text" placeholder="Search" class="mainSearchTxt"/>
    </div>
    <div class="search-icon-mobile">
        <div class="bg-search_mobile_icon search-mobile-bg-image"></div>
    </div>
</div>
 <!-- search for mobile ends here -->
<ul id="mainnavigatn" class="navbar-nav">
    <li class="nav-item">
        <a href="{{url('/')}}" class="nav-link {{((Request::segment(1) == '' || Request::segment(1) == 'search') ? 'active-menu-link' : '') }}">Home </a>
    </li>
    <li class="nav-item">
        <a href="{{route('view-stays')}}" class="nav-link {{((Request::segment(1) == 'view-stays' || Request::segment(1) == 'propertydetail') ? 'active-menu-link' : '') }}">View Stays</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('owner.list-your-stay') }}" class="nav-link {{((Request::segment(1) == 'list-your-stay') ? 'active-menu-link' : '') }}">
            List your Stay
        </a>
    </li>

    <li class="nav-item">
        <a href="{{route('tripboards')}}" class="nav-link {{((Request::segment(1) == 'tripboards'  || Request::segment(1) == 'tripboard-detail' || Request::segment(1) == 'favorites' ) ? 'active-menu-link' : '') }}">Tripboards </a>
    </li>
    <li class="nav-item mainnav-extention">
        @php
        if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!= ''){
            $label = 'DashBoard';
            $route = (\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->is_tripper == '1') ? route('tripper.dashboard') : '#';
        } else {
            $label = 'Become a tripper';
            $route = route('tripper.index');
        }
        @endphp
        <a class="nav-link" href="{{$route}}">{{$label}}</a>
        {{-- <a href="" class="nav-link">Become A Tripper </a> --}}
    </li>
</ul>
