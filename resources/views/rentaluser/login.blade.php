
    <div class="modal fade reg_popup" id="login_popup" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1> Account <strong>Login</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text"></div>
                        </div>

                        <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 RenterUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text" style="font-weight: bold;"> </div>
                        </div>

                        <form method="POST" id="rentaluserlogin" action="{{route('userlogin')}}">
                            @csrf

                            <div class="d-block text-center login-social-media">
                                <a href="javascript:void(0)" class="socialMedia" id="googleId"><div class="bg-google image"></div></a>
                                <a href="javascript:void(0)" class="socialMedia" id="facebookId"><div class="bg-favebook_login_btn image"></div></a>
                            </div>
                            <div class="forget-password">
                                Or use your phone number <strong class="bolder">or</strong> email to login:
                            </div>
                            <label class="form-errors"></label>
                            <div class="input-group login-custom">

                                <input type="text" id="phoneno" class="form-control" name="phone" placeholder="Phone Number">
                            </div>
                            <div class="input-group login-custom">

                                <input type="email" id="email" class="form-control" name="email" placeholder="Email">
                            </div>

                            <div class="input-group login-custom">
                                <span toggle="#password-field" class="fa fa-fw field-icon rentalPassword toggle-login-password fa-eye show-password"></span>
                                <input type="password" id="password" class="form-control rentalLoginPassword" name="password" placeholder="Password" passwordCheck="passwordCheck">
                            </div>

                            <div class="forget-password">
                                <a href="#" data-toggle="modal" data-target="#forgotpassword_popup">Forgot Password?</a>
                            </div>
                            <button type="submit" class="mb-10 button-default-animate" value="login"> Login <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="fly" class="lazy-load ml-5"></button>
                        </form>

                        @if(Request::segment(1)!='sign-up')
                            <div class="forget-password">
                                New here? Click to   <a href="" data-toggle="modal" data-target="#signup_popup"> Sign up </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('footerJs')
    @parent
    <script>
        var RedirectURL = '';
        var linkUrl = "{{route('owner.home')}}";
        var owner_name = "{{ Session::get('owner') }}";

        $(document).ready(function() {

            var googleUrl = "{{ url('auth/rentaluser/google') }}";
            var faceBookUrl = "{{ url('auth/rentaluser/facebook') }}";
            $('.socialMedia').click(function(e){
                e.preventDefault();
                let id = $(this).attr('id');

                let tripboard_name = escape($('#tripboard_name').val());
                document.cookie = "tripboardName=" + tripboard_name;
                if(id == 'googleId'){
                    window.location = googleUrl;
                } else {
                    window.location = faceBookUrl;
                }

            })
            $(".toggle-login-password").click(function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $(".rentalLoginPassword");

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $('.login-modal').click(function(e){

                e.preventDefault();
                RedirectURL =  $(this).attr('data-action');
                if(!RedirectURL){
                    RedirectURL = '';
                }

                $("#login_popup"). modal('show');

            })

            $('#login_popup').on('show.bs.modal', function(e) {
                $("#signup_popup").modal("hide");
                $("#reg_popup").modal("hide");
                $("#otp_popup").modal("hide");
                $("#activatepopup").modal("hide");

                $("#forgotpassword_popup").modal("hide");

                $('#login_popup').addClass("overflow-y");
                $('body').removeClass("overflow-y");
                $('body').addClass("overflow-hidden");

                $(".alert-light-danger").hide();
            });

            $('#forgotpassword_popup').on('show.bs.modal', function(e) {
                $("#login_popup").modal("hide");
                $("#activatepopup").modal("hide");
                $('#forgotpassword_popup').addClass("overflow-y");
                $('body').removeClass("overflow-y");
                $('body').addClass("overflow-hidden");

                $(".alert-light-danger").hide();
            });

            $('#loginCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-y");
            });
        });


        function popupWindow(url, windowName, win, w, h) {
            const y = win.top.outerHeight / 2 + win.top.screenY - ( h / 2);
            const x = win.top.outerWidth / 2 + win.top.screenX - ( w / 2);
            return win.open(url, windowName, `toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${w}, height=${h}, top=${y}, left=${x}`);
        }

        // submit form on enter key
        $('#email, #password').keyup(function(event) {
            if (event.which === 13) {
                event.preventDefault();
                $('#rentaluserlogin').submit();
            }
        });
        $.validator.addMethod("regx", function(value, element, regexpr) {
                return this.optional(element) || regexpr.test(value);
            }, "Please enter valid phone number.");
        $('#rentaluserlogin').validate({
             rules: {
                email: {
                    email:true
                },
                password:{
                    required:true,
                    minlength:6
                },
                phone:{
                    regx:/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
                }
            },
            messages: {
                email: {
                    email: "Please enter valid email address"
                },
                password: {
                    required:"Password is required",
                    minlength:"The password you entered is incorrect."
                },
                phone:{
                }
            },
        });

        $('#login_popup').on('hidden.bs.modal', function (e) {
            $(this)
                .find("input[type=text], input[type=email], input[type=password]")
                .val('')
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();

            $(".alert-light-danger").hide();

            var $alertas = $('#rentaluserlogin');
            $alertas.validate().resetForm();
            $alertas.find('.error').removeClass('error');
        })

        function recaptchaCallback() {
            $('#hiddenRecaptcha').valid();
        };
        $('#phoneno,#email').on('keyup',function(){
            if($("#phoneno").val() != ''){
                $("#phoneno").removeClass('error');
                $("#email").removeClass('error');
            }
            if( $("#email").val() != ''){
                $("#email").removeClass('error');
                $("#phoneno").removeClass('error');
            }
            $(".form-errors").html('').removeClass('error');
        })
        $('#phoneno').mask('(000) 000-0000',{placeholder: "Phone number"});
        $('#rentaluserlogin').on('submit', function(e) {
            e.preventDefault();
            let $form = $(this);

            let $phoneNo = $('#rentaluserlogin').find("#phoneno");
            let $email = $('#rentaluserlogin').find("#email");
            if($phoneNo.val() == '' && $email.val() == ''){
                $phoneNo.addClass('error');
                $email.addClass('error');
                $(".form-errors").html("Email address and/or phone number is required.").css('display','block').addClass('error');
                return false;
            }
            if(!$form.valid()) return false;
            showLoader();

            let tripboard_name = escape($('#tripboard_name').val());
            document.cookie = "tripboardName=" + tripboard_name;

            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    hideLoader();
                    if($.isEmptyObject(msg.error))
                    {
                        $('.RenterUI').hide();
                        $loginModal = $("#login_popup");
                        $loginModal.modal('hide');
                        Swal.fire({
                            icon: "success",
                            title: "Your login is successful!",
                            showConfirmButton: false,
                            timer: 3000
                        }).then(() => {
                            if(msg.session.tripboard_share_id != null) {
                                var $url = window.location.pathname;
                                var str = window.location.pathname.split('/').pop();
                                $url = $url.replace( new RegExp(str), '' );
                                // window.onbeforeunload = null;
                                window.location = $url+"tripboard-detail/"+msg.session.tripboard_share_id+'?shareid=' + msg.session.tripboard_share_id
                            } else if(msg.is_tripboard_redirect != ''){
                                var url = '{{ route("tripboard.detail", ":id") }}';
                                url = url.replace(':id',msg.is_tripboard_redirect);
                                $('#add_tripboard_popup').modal('hide');
                                window.location.href = url;
                            } else {
                                window.location.reload();
                            }

                        });

                    } else {

                        setTimeout(function() {
                            $(".alert-light-danger").show();
                            $(".alert-text").html('');
                            $.each( msg.error, function( key, value ) {
                                $(".alert-text").append(value);
                            });
                        }, 1000);

                    }
                }
            });
        });

    </script>
    @endsection
