@extends('layouts.rentalApp')

@section('content')

    <section id="slider" class="">
        <div class="">
            <div id="carouselExampleIndicators" class="carousel slide pointer-event" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>

                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="lazy-load d-block w-100" data-src="{{URL::asset('media/images/slider1.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/slider2.jpg')}}" alt="Second slide">
                    </div>

                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-2.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-3.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-4.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-5.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-6.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-7.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/banner-1.jpg')}}" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <form method="GET" id="searchForm" action="{{route('rental.search')}}" >
        <section id="wheretogo" class="container py-5">
            <div class="row">
                <div class="col-md-12 where-to-go">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 paddingtop-30 ">
                            <div class="input-group">
                                <img class="lazy-load" data-src="{{URL::asset('media/images/location.svg')}}" alt="location">
                                <input type="text" class="form-control" name="location_detail" id="location_detail" placeholder="Search Location">
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-6 paddingtop-30 ">
                            <div class="input-group" >
                                <img class="lazy-load" data-src="{{URL::asset('media/images/clock.svg')}}" alt="clock">
                                <input type="text" id="filter_dates" autocomplete="off" name="filter_dates" class="form-control" value="" placeholder="Select date">
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-6 paddingtop-30 ">
                            <div id="achild" class="input-group">
                                <img class="lazy-load" data-src="{{URL::asset('media/images/person.svg')}}" alt="person">
                                <input type="hidden" id="number_of_guest" name="number_of_guest" class="form-control persons" value="1" placeholder="2 Adults 1 Child">
                                <input type="text" class="form-control" id="adultchild" role="button" data-toggle="dropdown" aria-haspopup="true" readonly aria-expanded="false" placeholder="1 Adult 0 Child">
                                <div class="dropdown-menu adultchilddropdown" aria-labelledby="adultchild">
                                    <div class="form-group row">
                                        <label class="col-form-label text-left col-12 col-md-7">Adults
                                        </label>

                                        <div class="col-12 col-md-5 validate">
                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"><span class="input-group-btn input-group-prepend"></span><input id="touchspin_1" type="text" class="form-control" value="1" readonly name="adults" placeholder=""><span class="input-group-btn input-group-append"></span></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label text-left col-12 col-md-7">Children
                                        </label>

                                        <div class="col-12 col-md-5 validate">
                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"><span class="input-group-btn input-group-prepend"></span><input id="touchspin_2" type="text" class="form-control" value="0" readonly name="child" placeholder=""><span class="input-group-btn input-group-append"></span></div>
                                        </div>
                                    </div>
                                    <div class="form-group row float-right pr-4">
                                        <button type="button" name="okbutton" class="btn btn-primary okbutton">Ok</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 text-right">
                            <a href="" class="gobutton" data-toggle="modal" data-target="#filter_popup"><img class="lazy-load" data-src="{{URL::asset('media/images/filter.svg')}}" alt="fillter"></a>
                            <button type="submit" class="gobutton"><img class="lazy-load" data-src="{{URL::asset('media/images/gobutton.svg')}}" alt="go button"></button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="checkboxes-inline">
                <div class="row">
                    <div class="col-md-4 offset-md-1">
                        <label class="custom-checkbox">Vacation clubs
                            <input type="checkbox" value="vacation_rental" checked="checked" name="property_type[]">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-md-3 ">
                        <label class="custom-checkbox">hotels
                            <input type="checkbox" value="hotels" checked="checked" name="property_type[]">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-md-4 ">
                        <label class="custom-checkbox">private residences
                            <input type="checkbox" value="property" checked="checked" name="property_type[]" >
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </form>


    <div class="modal fade" id="filter_popup" tabindex="-1" role="dialog" aria-labelledby="filter_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Refine <span class="lighter-font"> your search</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30"
                             height="30" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149"
                                   xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII=" />
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="login-container align-items-center w-100">
                        <form>
                            <div class="pl-5 pr-5">
                                <div class="d-table mt-0">
                                    <div class="d-table-cell vertical-middle">
                                        <div class="row">
                                            <div class="col-lg-4 col-6">
                                                <div class="input-group" id="">
                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/location.svg')}}" alt="location">
                                                    <input type="text" class="form-control" placeholder="Number of People">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-6">
                                                <div class="input-group" id="kt_daterangepicker_1_validate">
                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/clock.svg')}}" alt="clock">
                                                    <input type="text" class="form-control" placeholder="Select date range">

                                                </div>

                                            </div>
                                            <div class="col-lg-4 col-6">
                                                <div class="input-group" id="">
                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/person.svg')}}" alt="person">
                                                    <input type="text" class="form-control" placeholder="Number of People">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-5 col-12">
                                        <div class="filter-subox mt-8">
                                            <h4 class="filter-subox-header">your budget</h4>

                                            <div class="multi-range-container pt-3">
                                                <div class="multi-range">
                                                    <input class="range" type="range" min="0" max="10" value="0" step="0.1" id="lower">
                                                    <span id="range-color" class="range-color"></span>
                                                    <input class="range" type="range" min="0" max="10" value="10" step="0.1" id="upper">
                                                </div>
                                                <p>$500 - 1200</p>
                                            </div>

                                        </div>
                                        <div class="filter-subox mt-10">
                                            <h4 class="filter-subox-header">Property Room</h4>
                                            <div class="form-group">
                                                <label>Bedroom </label>
                                                <div class="checkbox-inline">
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>1</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>2</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>3</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" checked="checked" name="Checkboxes3_1">
                                                        <span>4</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>5+</span>

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Bathroom </label>
                                                <div class="checkbox-inline">
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>1</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" checked="checked" name="Checkboxes3_1">
                                                        <span>2</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>3</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>4</span>

                                                    </label>
                                                    <label class="checkbox checkbox-lg">
                                                        <input type="checkbox" name="Checkboxes3_1">
                                                        <span>5+</span>

                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-7 col-12">

                                        <div class="filter-box-tabs mt-5">
                                            <ul class="nav nav-tabs nav-tabs-line">
                                                <li class="nav-item mr-5">
                                                    <a class="nav-link active pt-7" data-toggle="tab" href="#kt_tab_pane_1">
                                                        <span class="w-100 d-block pb-2">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="31.355"
                                                                 height="31.354" viewBox="0 0 31.355 31.354">
                                                                <g id="house" transform="translate(0 0)">
                                                                    <path id="Path"
                                                                          d="M143.857,277.173l-14.5-13.9a.979.979,0,0,0-1.356,0l-14.516,13.915A1.656,1.656,0,0,0,113,278.35a1.635,1.635,0,0,0,1.633,1.633h2.286v12.411a1.96,1.96,0,0,0,1.96,1.96h5.552a.98.98,0,0,0,.98-.98v-8.492a.327.327,0,0,1,.326-.327h5.879a.328.328,0,0,1,.327.327v8.492a.98.98,0,0,0,.98.98h5.553a1.96,1.96,0,0,0,1.96-1.96V279.983h2.286a1.635,1.635,0,0,0,1.633-1.633A1.668,1.668,0,0,0,143.857,277.173Z"
                                                                          transform="translate(-113 -263)" />
                                                                </g>
                                                            </svg>
                                                        </span> PRIVATE RESIDENCE</a>
                                                </li>
                                                <li class="nav-item mr-5">
                                                    <a class="nav-link pt-7" data-toggle="tab" href="#kt_tab_pane_2">
                                                        <span class="w-100 d-block pb-2">
                                                            <svg id="Commercial" xmlns="http://www.w3.org/2000/svg"
                                                                 width="27.395" height="31.309"
                                                                 viewBox="0 0 27.395 31.309">
                                                                <path id="Shape"
                                                                      d="M27.4,31.309H0V29.352H1.956V3.914A3.918,3.918,0,0,1,5.87,0H21.524a3.918,3.918,0,0,1,3.914,3.914V29.352H27.4v1.956ZM9,23.485H9v5.871h9.4V23.485Zm6.655-5.871h0v3.911h3.912V17.614Zm-7.831,0h0v3.911h3.912V17.614Zm7.831-5.871h0v3.911h3.912V11.743Zm-7.831,0h0v3.911h3.912V11.743Zm7.831-5.872h0V9.783h3.912V5.871Zm-7.831,0h0V9.783h3.912V5.871Z"
                                                                      transform="translate(0 0)" />
                                                            </svg>
                                                        </span> HOTEL
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link pt-7" data-toggle="tab" href="#kt_tab_pane_3">
                                                        <span class="w-100 d-block pb-2">
                                                            <svg id="Apartment" xmlns="http://www.w3.org/2000/svg"
                                                                 width="31.309" height="31.309"
                                                                 viewBox="0 0 31.309 31.309">
                                                                <path id="Shape"
                                                                      d="M31.309,31.309H0V29.352H1.957V9.784H0V5.87L15.655,0,31.309,5.87V9.784H29.352V29.352h1.957v1.956Zm-13.7-11.744h0v9.783h5.871V19.565H17.614Zm-9.783,0h0v4.3H13.7v-4.3Zm9.783-9.782h0v5.871h5.871V9.783H17.614Zm-9.783,0h0v5.871H13.7V9.783Z"
                                                                      transform="translate(0 0)" />
                                                            </svg>

                                                        </span> VACATION CLUB</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content mt-9" id="myTabContent">
                                                <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_1">
                                                    <div class="w-100">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <ul>
                                                                    <li><label class="custom-checkbox">Luxury homes
                                                                            <input type="checkbox" checked="checked">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>

                                                                    <li><label class="custom-checkbox">getaway homes
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                    <li><label class="custom-checkbox">CONDOS
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                    <li><label class="custom-checkbox">ranch style
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <ul>
                                                                    <li><label class="custom-checkbox">cabins
                                                                            <input type="checkbox" checked="checked">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>

                                                                    <li><label class="custom-checkbox">bungalows
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                    <li><label class="custom-checkbox">townhomes
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                    <li><label class="custom-checkbox">cottages
                                                                            <input type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <ul>
                                                                    <li><label class="custom-checkbox">tiny homes
                                                                            <input type="checkbox" checked="checked">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                                    <div class="w-100">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <ul>
                                                                    <li><label class="custom-checkbox">tiny homes
                                                                            <input type="checkbox" checked="checked">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="kt_tab_pane_3" role="tabpanel" aria-labelledby="kt_tab_pane_3">
                                                    <div class="w-100">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <ul>
                                                                    <li><label class="custom-checkbox">homes
                                                                            <input type="checkbox" checked="checked">
                                                                            <span class="checkmark"></span>
                                                                        </label></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xl-7 col-12">
                                        <div class="filter-subox mt-8 filter-stay-features">
                                            <h4 class="filter-subox-header">stay features</h4>
                                            <div id="carouselstayfilter" class="carousel slide" data-ride="carousel" data-interval="false">
                                                <div class="form-group">
                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active">


                                                            <div class="checkbox-inline">
                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox" checked="checked"
                                                                           name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="32" height="28.95"
                                                                             viewBox="0 0 32 28.95">
                                                                            <g id="Group_131" data-name="Group 131"
                                                                               transform="translate(-9840.5 -2363.666)">
                                                                                <g id="Group_57" data-name="Group 57">
                                                                                    <g id="Group_56"
                                                                                       data-name="Group 56"
                                                                                       transform="translate(5172.105 1052.04)">
                                                                                        <path id="Path_75"
                                                                                              data-name="Path 75"
                                                                                              d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z"
                                                                                              transform="translate(-0.022)"
                                                                                              fill="" />
                                                                                        <path id="Path_76"
                                                                                              data-name="Path 76"
                                                                                              d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z"
                                                                                              transform="translate(0 -469.471)"
                                                                                              fill="" />
                                                                                        <path id="Path_77"
                                                                                              data-name="Path 77"
                                                                                              d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z"
                                                                                              transform="translate(-0.042 -577.79)"
                                                                                              fill="" />
                                                                                    </g>
                                                                                </g>
                                                                                <g id="Group_58" data-name="Group 58">
                                                                                    <g id="Group_56-2"
                                                                                       data-name="Group 56"
                                                                                       transform="translate(5172.105 1052.04)">
                                                                                        <path id="Path_75-2"
                                                                                              data-name="Path 75"
                                                                                              d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z"
                                                                                              transform="translate(-0.022)"
                                                                                              fill="" />
                                                                                        <path id="Path_76-2"
                                                                                              data-name="Path 76"
                                                                                              d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z"
                                                                                              transform="translate(0 -469.471)"
                                                                                              fill="" />
                                                                                        <path id="Path_77-2"
                                                                                              data-name="Path 77"
                                                                                              d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z"
                                                                                              transform="translate(-0.042 -577.79)"
                                                                                              fill="" />
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        POOL
                                                                    </span>

                                                                </label>

                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox" checked="checked"
                                                                           name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="27.275" height="27.256"
                                                                             viewBox="0 0 27.275 27.256">
                                                                            <g id="_3VNxTW.tif" data-name="3VNxTW.tif"
                                                                               transform="translate(-3768.875 -1681.92)">
                                                                                <g id="Group_61" data-name="Group 61"
                                                                                   transform="translate(3768.874 1681.92)">
                                                                                    <path id="Path_78"
                                                                                          data-name="Path 78"
                                                                                          d="M3796.637,1805.232h-27.226v-21.721h27.226Zm-21.159-10.825a7.542,7.542,0,1,0,7.551-7.548A7.538,7.538,0,0,0,3775.478,1794.407Z"
                                                                                          transform="translate(-3769.382 -1777.976)"
                                                                                          fill="" />
                                                                                    <path id="Path_79"
                                                                                          data-name="Path 79"
                                                                                          d="M3796.147,1686.261h-27.254c-.005-.094-.014-.18-.014-.266,0-.562.014-1.124,0-1.685a2.1,2.1,0,0,1,1.221-2.017,3.2,3.2,0,0,1,1.57-.371q10.86,0,21.719,0a3.054,3.054,0,0,1,1.972.653,1.966,1.966,0,0,1,.791,1.623C3796.142,1684.875,3796.147,1685.553,3796.147,1686.261Zm-21.221-1.571h1.593V1683.5h-1.593Zm13.58-.005h1.588v-1.177h-1.588Zm-9.051.008h1.579V1683.5h-1.579Zm4.528-1.188v1.183h1.589V1683.5Z"
                                                                                          transform="translate(-3768.874 -1681.92)"
                                                                                          fill="" />
                                                                                    <path id="Path_80"
                                                                                          data-name="Path 80"
                                                                                          d="M3908.6,1879.017a6.378,6.378,0,1,1,6.369-6.408A6.356,6.356,0,0,1,3908.6,1879.017Zm-4.7-6.4a4.693,4.693,0,1,0,4.652-4.678A4.677,4.677,0,0,0,3903.9,1872.622Z"
                                                                                          transform="translate(-3894.952 -1856.217)"
                                                                                          fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        W/D
                                                                    </span>

                                                                </label>

                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox" name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="49.305" height="20.274"
                                                                             viewBox="0 0 49.305 20.274">
                                                                            <g id="C1aRwJ.tif"
                                                                               transform="translate(-6073.588 -1468.754)">
                                                                                <g id="Group_63" data-name="Group 63"
                                                                                   transform="translate(6073.588 1468.754)">
                                                                                    <path id="Path_81"
                                                                                          data-name="Path 81"
                                                                                          d="M6158.258,1478.9q0-4.267,0-8.533c0-1.118.458-1.581,1.563-1.594q.79-.01,1.58,0c1.128.012,1.6.48,1.605,1.628.009,1.875,0,3.75,0,5.625q0,5.626,0,11.251c0,1.3-.458,1.757-1.737,1.76-.463,0-.927,0-1.39,0-1.146-.008-1.622-.474-1.623-1.6Q6158.255,1483.169,6158.258,1478.9Z"
                                                                                          transform="translate(-6147.528 -1468.769)"
                                                                                          fill="" />
                                                                                    <path id="Path_82"
                                                                                          data-name="Path 82"
                                                                                          d="M6109.15,1478.859q0-4.235,0-8.471c0-1.153.454-1.6,1.6-1.612.527,0,1.054-.005,1.58,0,1.056.008,1.547.49,1.548,1.546q.007,6.321,0,12.642c0,1.5,0,2.992,0,4.488,0,1.09-.5,1.588-1.577,1.591-.548,0-1.1,0-1.644,0a1.347,1.347,0,0,1-1.513-1.522c-.008-2.887,0-5.773,0-8.66Z"
                                                                                          transform="translate(-6104.639 -1468.771)"
                                                                                          fill="" />
                                                                                    <path id="Path_83"
                                                                                          data-name="Path 83"
                                                                                          d="M6345.371,1478.937q0,4.2,0,8.406c0,1.233-.448,1.678-1.671,1.681-.505,0-1.011.007-1.517,0a1.353,1.353,0,0,1-1.54-1.559q-.011-5.973,0-11.946c0-1.728-.007-3.455,0-5.183.007-1.119.46-1.562,1.577-1.577.569-.007,1.138-.01,1.707,0a1.3,1.3,0,0,1,1.442,1.452C6345.377,1473.122,6345.371,1476.03,6345.371,1478.937Z"
                                                                                          transform="translate(-6306.793 -1468.754)"
                                                                                          fill="" />
                                                                                    <path id="Path_84"
                                                                                          data-name="Path 84"
                                                                                          d="M6389.756,1478.9q0-4.267,0-8.534c0-1.149.452-1.591,1.615-1.6.526,0,1.054-.007,1.58,0a1.358,1.358,0,0,1,1.542,1.557c.009,2.4.005,4.8.006,7.207q0,4.868,0,9.736c0,1.326-.451,1.774-1.787,1.77-.443,0-.885,0-1.328,0-1.17-.006-1.63-.47-1.631-1.66q0-4.235,0-8.471Z"
                                                                                          transform="translate(-6349.686 -1468.763)"
                                                                                          fill="" />
                                                                                    <path id="Path_85"
                                                                                          data-name="Path 85"
                                                                                          d="M6207.033,1539.34V1536.3c.242-.016.462-.042.683-.043,4.716,0,9.431.008,14.146-.013.546,0,.749.122.712.7-.05.791-.013,1.588-.013,2.4Z"
                                                                                          transform="translate(-6190.122 -1527.688)"
                                                                                          fill="" />
                                                                                    <path id="Path_86"
                                                                                          data-name="Path 86"
                                                                                          d="M6438.662,1510.6c1.2.122,1.264.192,1.259,1.339,0,.337,0,.674,0,1.011,0,1.259,0,1.259-1.258,1.469,0,.754,0,1.524,0,2.294,0,.875-.156,1.025-1.034,1.023-.861,0-1-.134-1-.981q0-4.265,0-8.529c0-.8.193-.981,1.022-.979.812,0,1.012.2,1.015.994C6438.664,1509.017,6438.662,1509.793,6438.662,1510.6Z"
                                                                                          transform="translate(-6390.617 -1502.369)"
                                                                                          fill="" />
                                                                                    <path id="Path_87"
                                                                                          data-name="Path 87"
                                                                                          d="M6074.844,1514.41c-1.231-.176-1.253-.2-1.255-1.359,0-.358,0-.716,0-1.074,0-1.191.048-1.246,1.255-1.38,0-.787,0-1.582,0-2.378,0-.759.216-.965.978-.974.852-.01,1.07.167,1.072.936q.012,4.326,0,8.652c0,.76-.185.908-1.026.9-.825,0-1.021-.172-1.029-.911C6074.838,1516.028,6074.844,1515.23,6074.844,1514.41Z"
                                                                                          transform="translate(-6073.588 -1502.367)"
                                                                                          fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        GYM
                                                                    </span>

                                                                </label>

                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox" name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="28.238" height="28.228"
                                                                             viewBox="0 0 28.238 28.228">
                                                                            <g id="hihsdn.tif"
                                                                               transform="translate(-4905.098 -480.231)">
                                                                                <g id="Group_66" data-name="Group 66"
                                                                                   transform="translate(4905.098 480.231)">
                                                                                    <path id="Path_88"
                                                                                          data-name="Path 88"
                                                                                          d="M4943.436,580.045h-18.2V598.2h-4.017v-22.17h26.212V598.19h-4Z"
                                                                                          transform="translate(-4920.204 -569.968)"
                                                                                          fill="" />
                                                                                    <path id="Path_89"
                                                                                          data-name="Path 89"
                                                                                          d="M4905.1,485.223v-4.992h28.238v4.992Z"
                                                                                          transform="translate(-4905.098 -480.231)"
                                                                                          fill="" />
                                                                                    <path id="Path_90"
                                                                                          data-name="Path 90"
                                                                                          d="M5036.234,671.473a15.814,15.814,0,0,1,4.517,3.376.422.422,0,0,1,.079.3,4.719,4.719,0,0,1-1.428,2.91c-.439.423-.879.845-1.321,1.263a6.627,6.627,0,0,0-2.3,5.466,6.084,6.084,0,0,0,.7,2.4c.039.075.081.147.118.222a.3.3,0,0,1,.009.084,6.384,6.384,0,0,1-3.146-1.349,5.075,5.075,0,0,1-.132-7.947,26.851,26.851,0,0,0,2.154-2.208,3.881,3.881,0,0,0,1.047-3.041C5036.479,672.468,5036.342,671.992,5036.234,671.473Z"
                                                                                          transform="translate(-5023.488 -659.379)"
                                                                                          fill="" />
                                                                                    <path id="Path_91"
                                                                                          data-name="Path 91"
                                                                                          d="M5128.074,751.885a10.207,10.207,0,0,1,1.57,4.085,6.444,6.444,0,0,1-.038,2.721,5.615,5.615,0,0,1-3.853,3.872c-.266.093-.553.125-.82.216a.541.541,0,0,1-.682-.259,4.77,4.77,0,0,1,.17-6.025,9.413,9.413,0,0,1,.714-.754c.179-.178.375-.337.559-.51A7.851,7.851,0,0,0,5128.074,751.885Z"
                                                                                          transform="translate(-5109.464 -734.705)"
                                                                                          fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        FIREPLACE
                                                                    </span>

                                                                </label>


                                                            </div>



                                                        </div>
                                                        <div class="carousel-item">


                                                            <div class="checkbox-inline">
                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox"
                                                                           name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="32" height="28.95"
                                                                             viewBox="0 0 32 28.95">
                                                                            <g id="Group_131" data-name="Group 131"
                                                                               transform="translate(-9840.5 -2363.666)">
                                                                                <g id="Group_57" data-name="Group 57">
                                                                                    <g id="Group_56"
                                                                                       data-name="Group 56"
                                                                                       transform="translate(5172.105 1052.04)">
                                                                                        <path id="Path_75"
                                                                                              data-name="Path 75"
                                                                                              d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z"
                                                                                              transform="translate(-0.022)"
                                                                                              fill="" />
                                                                                        <path id="Path_76"
                                                                                              data-name="Path 76"
                                                                                              d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z"
                                                                                              transform="translate(0 -469.471)"
                                                                                              fill="" />
                                                                                        <path id="Path_77"
                                                                                              data-name="Path 77"
                                                                                              d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z"
                                                                                              transform="translate(-0.042 -577.79)"
                                                                                              fill="" />
                                                                                    </g>
                                                                                </g>
                                                                                <g id="Group_58" data-name="Group 58">
                                                                                    <g id="Group_56-2"
                                                                                       data-name="Group 56"
                                                                                       transform="translate(5172.105 1052.04)">
                                                                                        <path id="Path_75-2"
                                                                                              data-name="Path 75"
                                                                                              d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z"
                                                                                              transform="translate(-0.022)"
                                                                                              fill="" />
                                                                                        <path id="Path_76-2"
                                                                                              data-name="Path 76"
                                                                                              d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z"
                                                                                              transform="translate(0 -469.471)"
                                                                                              fill="" />
                                                                                        <path id="Path_77-2"
                                                                                              data-name="Path 77"
                                                                                              d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z"
                                                                                              transform="translate(-0.042 -577.79)"
                                                                                              fill="" />
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        POOL
                                                                    </span>

                                                                </label>

                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox"
                                                                           name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="27.275" height="27.256"
                                                                             viewBox="0 0 27.275 27.256">
                                                                            <g id="_3VNxTW.tif" data-name="3VNxTW.tif"
                                                                               transform="translate(-3768.875 -1681.92)">
                                                                                <g id="Group_61" data-name="Group 61"
                                                                                   transform="translate(3768.874 1681.92)">
                                                                                    <path id="Path_78"
                                                                                          data-name="Path 78"
                                                                                          d="M3796.637,1805.232h-27.226v-21.721h27.226Zm-21.159-10.825a7.542,7.542,0,1,0,7.551-7.548A7.538,7.538,0,0,0,3775.478,1794.407Z"
                                                                                          transform="translate(-3769.382 -1777.976)"
                                                                                          fill="" />
                                                                                    <path id="Path_79"
                                                                                          data-name="Path 79"
                                                                                          d="M3796.147,1686.261h-27.254c-.005-.094-.014-.18-.014-.266,0-.562.014-1.124,0-1.685a2.1,2.1,0,0,1,1.221-2.017,3.2,3.2,0,0,1,1.57-.371q10.86,0,21.719,0a3.054,3.054,0,0,1,1.972.653,1.966,1.966,0,0,1,.791,1.623C3796.142,1684.875,3796.147,1685.553,3796.147,1686.261Zm-21.221-1.571h1.593V1683.5h-1.593Zm13.58-.005h1.588v-1.177h-1.588Zm-9.051.008h1.579V1683.5h-1.579Zm4.528-1.188v1.183h1.589V1683.5Z"
                                                                                          transform="translate(-3768.874 -1681.92)"
                                                                                          fill="" />
                                                                                    <path id="Path_80"
                                                                                          data-name="Path 80"
                                                                                          d="M3908.6,1879.017a6.378,6.378,0,1,1,6.369-6.408A6.356,6.356,0,0,1,3908.6,1879.017Zm-4.7-6.4a4.693,4.693,0,1,0,4.652-4.678A4.677,4.677,0,0,0,3903.9,1872.622Z"
                                                                                          transform="translate(-3894.952 -1856.217)"
                                                                                          fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        W/D
                                                                    </span>

                                                                </label>

                                                                <label class="checkbox checkbox-lg">
                                                                    <input type="checkbox" name="Checkboxes3_1">
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="49.305" height="20.274"
                                                                             viewBox="0 0 49.305 20.274">
                                                                            <g id="C1aRwJ.tif"
                                                                               transform="translate(-6073.588 -1468.754)">
                                                                                <g id="Group_63" data-name="Group 63"
                                                                                   transform="translate(6073.588 1468.754)">
                                                                                    <path id="Path_81"
                                                                                          data-name="Path 81"
                                                                                          d="M6158.258,1478.9q0-4.267,0-8.533c0-1.118.458-1.581,1.563-1.594q.79-.01,1.58,0c1.128.012,1.6.48,1.605,1.628.009,1.875,0,3.75,0,5.625q0,5.626,0,11.251c0,1.3-.458,1.757-1.737,1.76-.463,0-.927,0-1.39,0-1.146-.008-1.622-.474-1.623-1.6Q6158.255,1483.169,6158.258,1478.9Z"
                                                                                          transform="translate(-6147.528 -1468.769)"
                                                                                          fill="" />
                                                                                    <path id="Path_82"
                                                                                          data-name="Path 82"
                                                                                          d="M6109.15,1478.859q0-4.235,0-8.471c0-1.153.454-1.6,1.6-1.612.527,0,1.054-.005,1.58,0,1.056.008,1.547.49,1.548,1.546q.007,6.321,0,12.642c0,1.5,0,2.992,0,4.488,0,1.09-.5,1.588-1.577,1.591-.548,0-1.1,0-1.644,0a1.347,1.347,0,0,1-1.513-1.522c-.008-2.887,0-5.773,0-8.66Z"
                                                                                          transform="translate(-6104.639 -1468.771)"
                                                                                          fill="" />
                                                                                    <path id="Path_83"
                                                                                          data-name="Path 83"
                                                                                          d="M6345.371,1478.937q0,4.2,0,8.406c0,1.233-.448,1.678-1.671,1.681-.505,0-1.011.007-1.517,0a1.353,1.353,0,0,1-1.54-1.559q-.011-5.973,0-11.946c0-1.728-.007-3.455,0-5.183.007-1.119.46-1.562,1.577-1.577.569-.007,1.138-.01,1.707,0a1.3,1.3,0,0,1,1.442,1.452C6345.377,1473.122,6345.371,1476.03,6345.371,1478.937Z"
                                                                                          transform="translate(-6306.793 -1468.754)"
                                                                                          fill="" />
                                                                                    <path id="Path_84"
                                                                                          data-name="Path 84"
                                                                                          d="M6389.756,1478.9q0-4.267,0-8.534c0-1.149.452-1.591,1.615-1.6.526,0,1.054-.007,1.58,0a1.358,1.358,0,0,1,1.542,1.557c.009,2.4.005,4.8.006,7.207q0,4.868,0,9.736c0,1.326-.451,1.774-1.787,1.77-.443,0-.885,0-1.328,0-1.17-.006-1.63-.47-1.631-1.66q0-4.235,0-8.471Z"
                                                                                          transform="translate(-6349.686 -1468.763)"
                                                                                          fill="" />
                                                                                    <path id="Path_85"
                                                                                          data-name="Path 85"
                                                                                          d="M6207.033,1539.34V1536.3c.242-.016.462-.042.683-.043,4.716,0,9.431.008,14.146-.013.546,0,.749.122.712.7-.05.791-.013,1.588-.013,2.4Z"
                                                                                          transform="translate(-6190.122 -1527.688)"
                                                                                          fill="" />
                                                                                    <path id="Path_86"
                                                                                          data-name="Path 86"
                                                                                          d="M6438.662,1510.6c1.2.122,1.264.192,1.259,1.339,0,.337,0,.674,0,1.011,0,1.259,0,1.259-1.258,1.469,0,.754,0,1.524,0,2.294,0,.875-.156,1.025-1.034,1.023-.861,0-1-.134-1-.981q0-4.265,0-8.529c0-.8.193-.981,1.022-.979.812,0,1.012.2,1.015.994C6438.664,1509.017,6438.662,1509.793,6438.662,1510.6Z"
                                                                                          transform="translate(-6390.617 -1502.369)"
                                                                                          fill="" />
                                                                                    <path id="Path_87"
                                                                                          data-name="Path 87"
                                                                                          d="M6074.844,1514.41c-1.231-.176-1.253-.2-1.255-1.359,0-.358,0-.716,0-1.074,0-1.191.048-1.246,1.255-1.38,0-.787,0-1.582,0-2.378,0-.759.216-.965.978-.974.852-.01,1.07.167,1.072.936q.012,4.326,0,8.652c0,.76-.185.908-1.026.9-.825,0-1.021-.172-1.029-.911C6074.838,1516.028,6074.844,1515.23,6074.844,1514.41Z"
                                                                                          transform="translate(-6073.588 -1502.367)"
                                                                                          fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        GYM
                                                                    </span>

                                                                </label>


                                                            </div>



                                                        </div>
                                                        <a class="carousel-control-next" href="#carouselstayfilter" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                     height="24" viewBox="0 0 24 24">
                                                                    <g id="Arrow_Icon" data-name="Arrow Icon"
                                                                       transform="translate(-214 -5634)">
                                                                        <rect id="Shape" width="24" height="24"
                                                                              transform="translate(214 5634)"
                                                                              fill="rgba(0,0,0,0)" />
                                                                        <path id="Path_94" data-name="Path 94"
                                                                              d="M227.616,5646.029l-4.853,5.294a1,1,0,1,0,1.474,1.352l5.5-6a1,1,0,0,0-.03-1.383l-6-6a1,1,0,1,0-1.414,1.415Z"
                                                                              fill="#fff" />
                                                                    </g>
                                                                </svg>

                                                            </span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 text-right mt-3 mb-4">
                                        <button type="submit" class="btn btn-fly-go">
                                            REFINE SEARCH
                                            <span class="pl-5">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" width="57.558"
                                                     height="24.346" viewBox="0 0 57.558 24.346">
                                                    <defs>
                                                        <linearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5"
                                                                        gradientUnits="objectBoundingBox">
                                                            <stop offset="0" stop-color="#e40046" />
                                                            <stop offset="0.111" stop-color="#e40444" />
                                                            <stop offset="0.243" stop-color="#e61340" />
                                                            <stop offset="0.386" stop-color="#e82b38" />
                                                            <stop offset="0.536" stop-color="#ec4c2e" />
                                                            <stop offset="0.693" stop-color="#f07721" />
                                                            <stop offset="0.851" stop-color="#f6ab11" />
                                                            <stop offset="0.999" stop-color="#fce300" />
                                                        </linearGradient>
                                                    </defs>
                                                    <path id="Path_1" data-name="Path 1"
                                                          d="M147.455,1310.953c2.63,0,4.986.032,7.341-.009,2.154-.037,4.316-.053,6.457-.262,2.372-.231,4.721-.691,7.086-1.01a46.881,46.881,0,0,0,6.792-1.507c1.133-.329,2.264-.665,3.384-1.034.767-.252.834-.573.248-1.15a12.428,12.428,0,0,0-1.127-.916q-2.925-2.266-5.857-4.523a4.324,4.324,0,0,0-.792-.454,1.1,1.1,0,0,1-.626-1.6c.34-.132.665-.243.978-.381a5.2,5.2,0,0,1,3.441.066c1.257.3,2.5.654,3.751.993,2.019.547,4.033,1.112,6.055,1.65,1.11.3,2.234.544,3.347.832.7.181,1.383.425,2.086.594,1.3.313,2.365-.491,3.541-.77.966-.23,1.928-.5,2.871-.808a36.984,36.984,0,0,0,3.717-1.361,4.383,4.383,0,0,1,4.636,1.067,2.644,2.644,0,0,1-.294,2.653,7.564,7.564,0,0,1-2.018,1.276c-1.539.728-3.107,1.4-4.681,2.05-1.107.46-2.247.839-3.362,1.28a1.7,1.7,0,0,0-.706.475c-2.015,2.6-4.011,5.212-6.012,7.822q-2.077,2.709-4.152,5.419a3.6,3.6,0,0,1-2.7.771c-.569-.239-.419-.711-.326-1.144.225-1.05.44-2.1.7-3.144.3-1.221.664-2.428.973-3.647.187-.735.355-1.477.475-2.225.115-.711-.446-.522-.787-.471-.751.112-1.492.293-2.238.441-1.243.247-2.479.542-3.732.72-1.463.209-2.939.321-4.41.462-1.528.147-3.056.3-4.587.409a19.918,19.918,0,0,1-2.192.018c-1.67-.062-3.342-.112-5.007-.246-2.236-.178-4.479-.345-6.7-.676-1.739-.26-3.443-.756-5.157-1.167C147.76,1311.42,147.681,1311.225,147.455,1310.953Z"
                                                          transform="translate(-147.455 -1297.832)"
                                                          fill="url(#linear-gradient)" />
                                                </svg>

                                            </span>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade " id="activateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" style="background: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="activationClose">×</span>
                    </button>
                    <div class="row m-auto">
                        <div class="col-md-12 m-auto">
                            <div class="row">
                                <div class="col-lg-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="91.541" height="91.541" viewBox="0 0 91.541 91.541">
                                        <defs>
                                            <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                                                <stop offset="0" stop-color="#e40046"/>
                                                <stop offset="1" stop-color="#fce300"/>
                                            </linearGradient>
                                        </defs>
                                        <g id="correct_2_" data-name="correct (2)" style="isolation: isolate">
                                            <g id="Group_410" data-name="Group 410">
                                                <path id="Path_2470" data-name="Path 2470" d="M78.135,13.406A45.771,45.771,0,0,0,13.406,78.135,45.771,45.771,0,1,0,78.135,13.406Zm-10.5,21.472L42.059,60.456a2.682,2.682,0,0,1-3.793,0L23.9,46.093A2.682,2.682,0,1,1,27.7,42.3L40.163,54.767,63.845,31.085a2.682,2.682,0,0,1,3.793,3.793Z" fill="url(#linear-gradient)"/>
                                            </g>
                                        </g>
                                    </svg>

                                </div>
                                <div class="col-lg-6">
                                    <h1 class="congratulation-text">Congratulations!</h1>
                                    <h4 class="activation-message-text">Your Got2Go account is Activated.</h4>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <a class="activation-to-login-btn" href="" data-toggle="modal" data-target="#login_popup">Login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endsection
    @section('footerJs')
    @parent
    <script>
        $(document).ready(function() {
            $('#login_popup').on('show.bs.modal', function() {
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-hidden");
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $('body').addClass("overflow-hidden");
                $('#reg_popup').addClass("overflow-show");
            });

            $('#filter_popup').on('show.bs.modal', function() {
                $('body').addClass("overflow-hidden");
                $('#filter_popup').addClass("overflow-show");
            })
            var date = new Date();
            var year = date.getFullYear();
            $('#filter_dates').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                minDate:moment().format('MM/DD/YYYY'),
                autoUpdateInput: false

            },function(start, end, label) {
                $('#filter_dates').val( start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            });

            $('#searchForm').validate({

                rules: {
                    location_detail: {
                        required:true

                    },
                    filter_dates:{
                        required:true
                    },
                    number_of_guest:{
                        required:true
                    },
                    'property_type[]':{
                        required:true

                    }
                },
                messages: {
                    location_detail: {
                        required: "Please Select Location"

                    },
                    filter_dates:{
                        required: 'Please Slect Dates'
                    },
                    number_of_guest:{
                        required: 'Please Slect Guest Number'
                    },
                    'property_type[]':{
                        required:'Please Select At-least one Prperty Type'

                    }
                }
            });

            $( "#searchForm" ).submit(function( event ) {

                let $form = $(this);

                if(!$form.valid())
                {
                    event.preventDefault();
                    return false;
                }
                else
                {
                    return true;
                }


            });
        });
    </script>
    <script>
        var lowerSlider = document.querySelector('#lower'); //Lower value slider
        var upperSlider = document.querySelector('#upper'); //Upper value slider

        var rangeColor = document.querySelector('#range-color'); //Range color

        //When the upper value slider is moved.
        upperSlider.oninput = function() {
            lowerVal = parseInt(lowerSlider.value); //Get lower slider value
            upperVal = parseInt(upperSlider.value); //Get upper slider value

            //If the upper value slider is LESS THAN the lower value slider plus one.
            if (upperVal < lowerVal + 1) {
                //The lower slider value is set to equal the upper value slider minus one.
                lowerSlider.value = upperVal - 1;
                //If the lower value slider equals its set minimum.
                if (lowerVal == lowerSlider.min) {
                    //Set the upper slider value to equal 1.
                    upperSlider.value = 1;
                }
            }


            //Setting the margin left of the middle range color.
            //Taking the value of the lower value times 10 and then turning it into a percentage.
            rangeColor.style.marginLeft = (lowerSlider.value * 10) + '%';

            //Setting the width of the middle range color.
            //Taking the value of the upper value times 10 and subtracting the lower value times 10 and then turning it into a percentage.
            rangeColor.style.width = (upperSlider.value * 10) - (lowerSlider.value * 10) + '%';


            console.log('upper value: ' + upperSlider.value);
        };

        //When the lower value slider is moved.
        lowerSlider.oninput = function() {
            lowerVal = parseInt(lowerSlider.value); //Get lower slider value
            upperVal = parseInt(upperSlider.value); //Get upper slider value

            //If the lower value slider is GREATER THAN the upper value slider minus one.
            if (lowerVal > upperVal - 1) {
                //The upper slider value is set to equal the lower value slider plus one.
                upperSlider.value = lowerVal + 1;

                //If the upper value slider equals its set maximum.
                if (upperVal == upperSlider.max) {
                    //Set the lower slider value to equal the upper value slider's maximum value minus one.
                    lowerSlider.value = parseInt(upperSlider.max) - 1;
                }

            }

            //Setting the margin left of the middle range color.
            //Taking the value of the lower value times 10 and then turning it into a percentage.
            rangeColor.style.marginLeft = (lowerSlider.value * 10) + '%';

            //Setting the width of the middle range color.
            //Taking the value of the upper value times 10 and subtracting the lower value times 10 and then turning it into a percentage.
            rangeColor.style.width = (upperSlider.value * 10) - (lowerSlider.value * 10) + '%';

            console.log('lower value: ' + lowerSlider.value);
        };
    </script>
    <script>
        $('#touchspin_1').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            min: 1,
            max: 10,
            step: 1,
            decimals: 0,
            boostat: 5,
            maxboostedstep: 10,
        });

        $(' #touchspin_2, #touchspin_3').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            min: 0,
            max: 20,
            step: 1,
            decimals: 0,
            boostat: 5,
            maxboostedstep: 10,
        });

        $('#touchspin_1, #touchspin_2 ').on('change', function(event) {
            var total = parseInt($('#touchspin_1').val()) + parseInt($('#touchspin_2').val());
            $('#number_of_guest').val(total)

            $('input#adultchild').attr("value",$('#touchspin_1').val()+' Adults '+$('#touchspin_2').val()+' Childs')
        });

        $('input#adultchild').on('click', function(event) {
            $(".adultchilddropdown").toggle();
        });
        $('.okbutton').on('click', function(event) {
            $(".adultchilddropdown").hide();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#activateModal").modal('show');

            $('#activateModal').on('show.bs.modal', function() {
                $("#activateModal").modal("hide");
                $("#login_popup").modal("show");
            });

            $('#loginCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $("#reg_popup").modal("hide");
            });

            $('#regCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
            });
        });
    </script>

@endsection
