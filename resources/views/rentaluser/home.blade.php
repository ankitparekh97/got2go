@extends('layouts.rentalApp')

@section('content')

    {{-- @if ($message = Session::get('activation'))
        <div class="modal fade " id="activateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="background: none;">
                        <button type="button" class="activation close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="activationClose">×</span>
                        </button>
                        <div class="row m-auto">
                            <div class="col-md-12 m-auto">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="91.541" height="91.541" viewBox="0 0 91.541 91.541">
                                            <defs>
                                                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                                                    <stop offset="0" stop-color="#e40046"/>
                                                    <stop offset="1" stop-color="#fce300"/>
                                                </linearGradient>
                                            </defs>
                                            <g id="correct_2_" data-name="correct (2)" style="isolation: isolate">
                                                <g id="Group_410" data-name="Group 410">
                                                    <path id="Path_2470" data-name="Path 2470" d="M78.135,13.406A45.771,45.771,0,0,0,13.406,78.135,45.771,45.771,0,1,0,78.135,13.406Zm-10.5,21.472L42.059,60.456a2.682,2.682,0,0,1-3.793,0L23.9,46.093A2.682,2.682,0,1,1,27.7,42.3L40.163,54.767,63.845,31.085a2.682,2.682,0,0,1,3.793,3.793Z" fill="url(#linear-gradient)"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="col-lg-6">
                                        <h1 class="congratulation-text">Congratulations!</h1>
                                        <h4 class="activation-message-text">Your Got2Go account is Activated.</h4>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <a class="activation-to-login-btn" id="activationLogin" href="" data-toggle="modal" data-target="#login_popup">Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif --}}

    @if(Request::segment(1) == '')
    <section id="slider">
        <div class="container-fluid p-0">
            <div id="carouselExampleIndicators" class="test carousel slide pointer-event" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="9"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="10"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="11"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="12"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="13"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="14"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="15"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="16"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="17"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="18"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="19"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="20"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="21"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="22"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="23"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="24"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="lazy-load d-block w-100" data-src="{{URL::asset('media/images/homepage-slider/banner_1.jpg')}}" alt="1">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_3.jpg')}}" alt="2">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_7a.jpg')}}" alt="3">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-business-2.jpg')}}" alt="4">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-event-1.jpg')}}" alt="5">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-event-4.jpg')}}" alt="6">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-party-3.jpg')}}" alt="7">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-couple-2.jpg')}}" alt="8">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-couple-1.jpg')}}" alt="9">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-couple-3.jpg')}}" alt="10">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-event-5.jpg')}}" alt="11">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-party-2.jpg')}}" alt="12">
                    </div>

                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-couple-4.jpg')}}" alt="13">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-destination-3.jpg')}}" alt="14">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-destination-1.jpg')}}" alt="15">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-party-4.jpg')}}" alt="16">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-event-3.jpg')}}" alt="17">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_5.jpg')}}" alt="18">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_6.jpg')}}" alt="19">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-party-1.jpg')}}" alt="20">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-business-1.jpg')}}" alt="21">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_2.jpg')}}" alt="22">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-event-2.jpg')}}" alt="23">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner-destination-2.jpg')}}" alt="24">
                    </div>
                    <div class="carousel-item">
                        <img class="lazy-load d-block w-100" data-src="{{url::asset('media/images/homepage-slider/banner_8.jpg')}}" alt="25">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
        <form method="GET" id="searchForm" autocomplete="off" action="{{route('rental.search')}}" >

            <section id="wheretogo" class="container py-5">
                <div class="responsive-brand-logo-img">
                <img src="{{URL::asset('media/images/main-navigation/got2go-white-logo.svg')}}" alt="logo">
                </div>
                <div class="row">
                    <div class="col-md-12 where-to-go">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 paddingtop-30 ">
                                <div class="input-group pl-3">
                                    <img src="{{URL::asset('media/images/location.svg')}}" alt="location" class="desktop-view">
                                    <img src="{{URL::asset('media/images/main-navigation/location1.svg')}}" alt="location" class="desktop-hide">
                                    <input type="text" class="form-control" autocomplete="off" name="location_detail" id="location_detail" placeholder="Where are you going?">
                                    <input type="hidden" name="address_selected_from_drop" class="address_selected_from_drop" value="">
                                    <input type="hidden" name="from_home" id="from_home" value="1">
                                </div>

                            </div>
                            <div class="col-lg-3 col-md-6 paddingtop-30 ">
                                <div class="input-group" >
                                    <img src="{{URL::asset('media/images/clock.svg')}}" alt="clock" class="desktop-view">
                                    <img src="{{URL::asset('media/images/main-navigation/clock1.svg')}}" alt="clock" class="desktop-hide">
                                    <input type="text" autocomplete="off" readonly  id="filter_dates_picker"  name="filter_dates_picker" value="{{request()->get('filter_dates_picker')}}" class="form-control" placeholder="When are you going?">
                                    <input type="hidden"  id="filter_dates"   name="filter_dates" value="{{request()->get('filter_dates')}}" class="form-control " placeholder="When are you going?">
                                </div>

                            </div>
                            <div class="col-lg-3 col-md-6 paddingtop-30 person-box">
                                <div id="achild" class="input-group">
                                    <img src="{{URL::asset('media/images/person.svg')}}" alt="person" class="desktop-view">
                                    <img src="{{URL::asset('media/images/main-navigation/person1.svg')}}" alt="person" class="desktop-hide">
                                    <input type="hidden" id="number_of_guest" name="number_of_guest" class="form-control persons" value="1" placeholder="1 Adult(s) - 0 Children">
                                    <input type="text" autocomplete="off" readonly class="form-control adultchild" id="adultchild" role="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" placeholder="1 Adult(s) - 0 Children">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 text-right search-box-button">
                                <a href="" class="float-left pt-7 refine_filter_icon" data-toggle="modal" data-target="#filter_popup"><img src="{{URL::asset('media/images/filter.svg')}}" alt="filter"></a>
                                <button type="button" class="gobutton search-button-box">
                                    SEARCH <div class="bg-search_plane image"></div></button>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkboxes-inline res-checkbox-inline">
                    <div class="row">
                        <div class="col-4">
                            <label class="custom-checkbox float-right">Vacation clubs
                                <input type="checkbox" value="vacation_rental" checked="checked" name="property_type[]">
                                <span class="checkmark">VACATION CLUBS</span>
                            </label>
                        </div>
                        <div class="col-4 second-column">
                            <label class="custom-checkbox common-center-x">hotels
                                <input type="checkbox" value="hotels" checked="checked" name="property_type[]">
                                <span class="checkmark">HOTELS</span>
                            </label>
                        </div>
                        <div class="col-4">
                            <label class="custom-checkbox">private residences
                                <input type="checkbox" value="property" checked="checked" name="property_type[]" >
                                <span class="checkmark">PRIVATE RESIDENCES</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="duplicate-button-responsive">

                <button type="button" class="gobutton">SEARCH</button>
                </div>
                </div>
            </section>


       @include('rentaluser.hotdeals')
 <!--Start Trippers Only section -->
 <div class="tripperonly-wrapper">
            <div class="container">
                    <div class="makeiteasy-wrapper">
                        <div class="makeiteasy-inner">
                            <h2>MAKE IT EASY.</h2>
                            <h3> THIS SHOULD BE FUN! </h3>
                            <p>It’s a no brainer. We take the stress out of your trip by making it a one-stop-shop from planning to getting there. You’re on your own for packing that bag. </p>
                            <ul class="list-unstyled">
                                <li>TRIPPER POINTS</li>
                                <li>TRIP BOARDS</li>
                                <li>ITINERARY SCHEDULER</li>
                                <li>EXCLUSIVE TRIPPER DISCOUNTS</li>

                            </ul>
                        </div>
                    </div>
                    <div class="tripperonly-action-wrapper tripperonly-action-mobile-wrp">
                        <div class="tripperonly-action">
                            <img src="../../html/assets/media/images/tripper-page/tripper-only-img.png" alt="image">
                             <div class="text-right"><a href="{{route('tripper.index')}}" class="orangeigradient-btn button-default-animate">SIGN ME UP</a></div>
                        </div>
                    </div>
             </div>

        </div>
    <!-- End Trippers Only section -->
  <!-- Start List you Stay -->
        <div class="listyoustay-wrapper innerpage-section">
            <div class="container">
                <div class="listyour-section">
                    <div class="listyour-block">
                        <div class="listyour-header page-heading">
                            <div class="d-desktop">
                                <h2>LIST</h2>
                                <h3>YOUR STAY <a href="{{route('owner.list-your-stay')}}" class="yellow-link ml-4"><i class="got got-singlarrow-right"></i></a></h3>
                            </div>
                            <h2 class="d-mobile">List Your Stay</h2>
                        </div>
                        <div class="listyoustay-body">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="liststay-label">Advertising opportunities</div>
                                    <p>Promote your stay with our creative in-site ads and create experiences.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Personal dashboard</div>
                                    <p>Receive, track and respond to your guests.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Customer care team</div>
                                    <p>Available when you need immediate assistance for customer care.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Estimator tool</div>
                                    <p>Use our estimator to easily determine the value of your property or rental.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="d-mobile text-center">
                                <a href="#" class="orangeigradient-btn">BECOME A HOST</a>
                            </div>
                    </div>
                </div>
                <div class="d-mobile">
                    <div class="listyour-image ">
                        <img src="../../media/images/tripper-page/list_your_stay-_door_image_mobile.png" alt="mobile door"/>
                    </div>
                </div>
            </div>
        </div>
    <!-- End List you Stay -->
     <!-- Start Blog and Journal -->
      <div class="blog-wrapper innerpage-section">
            <div class="container">
                <div class="page-heading text-center">
                    <h2>TRAVEL THE WORLD</h2>
                    <h3>Through our Blog & Journal</h3>
                </div>
            </div>
            <div class="blog-slider-outer" style="background-image: url(../../media/images/tripper-page/tripper-slider-bg.png);">
                <div class="container">
                    <div class="owl-carousel owl-theme owl-centered" id="blog-page-carousal">
                        <div class="item">
                            <div class="blog-carousal-div">
                                <div class="image">
                                    <img class="lazy-load" data-src="../../html/assets/media/images/tripper-page/property-3.jpg" alt="property" />
                                </div>
                                <div class="extra-bloginfo">
                                    <h4>Why You Shouldn't Ride Elephants in Indonesia Ride Elephants in Indonesia 1</h4>
                                    <p> Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating  Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating</p>
                                <a href="#" class="ext-link">Read More <em class="got got-arrow-right"></em></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-carousal-div">
                                    <div class="image">
                                        <img class="lazy-load" data-src="../../html/assets/media/images/tripper-page/property-1.jpg" alt="property" />
                                    </div>
                                    <div class="extra-bloginfo">
                                        <h4>Best Travel Quotes For Inspiration Travel Blog</h4>
                                        <p> Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating </p>
                                    <a href="#" class="ext-link">Read More <em class="got got-arrow-right"></em></a>
                                    </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-carousal-div">
                                    <div class="image">
                                        <img class="lazy-load" data-src="../../html/assets/media/images/tripper-page/property-2.jpg" alt="property" />
                                    </div>
                                    <div class="extra-bloginfo">
                                        <h4>Hiking Greenland's Arctic Circle Trail 2019</h4>
                                        <p> Tourism is travel for pleasure or  Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating  </p>
                                    <a href="#" class="ext-link">Read More <em class="got got-arrow-right"></em></a>
                                    </div>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
      </div>
    <!-- End Blog and Journal -->

    <!-- Start Inatagram Gallery -->
    <div class="instagramgallery-wrapper innerpage-section">
        <div class="container">
            <div class="instagramgallery-title page-heading">
                <em class="got got-instagram"></em>
                <h2 class="text=uppercase">GOT2GO DESTINATIONS</h2>
                <h3>NO FILTERS REQUIRED</h3>
            </div>

            <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-b9292718-e722-4956-b6ff-72f752ecfbbb"></div>
        </div>
    </div>
    <!-- End Inatagram Gallery -->


       @include('rentaluser.advanceSearch')

    </form>
    @endif
    @endsection

@section('footerJs')
    @parent
    <script>
        $(document).ready(function() {

            $('#tripper-page-carousal').owlCarousel({
                loop:true,
                margin:30,
                center: true,
                nav:true,
                dots: false,
                autoHeight: false,
                responsive:{
                    0:{
                        items:1
                    },
                    640:{
                        items:2
                    },
                    767:{
                        items:3
                    },
                    992:{
                        items:2
                    },
                    1399:{
                        items:3
                    }
                }
            });

            $(window).keydown(function(event){
                if( (event.keyCode == 13) ) {
                event.preventDefault();
                return false;
                }
            });

            $('.gobutton').click(function(){
                $( "#searchForm" ).submit();
            });


            $('#login_popup').on('show.bs.modal', function() {
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-hidden");
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $('body').addClass("overflow-hidden");
                $('#reg_popup').addClass("overflow-show");
            });


            $('#searchForm').validate({

                rules: {
                    location_detail: {
                        address_selection_required : true
                    },
                    modal_location_detail: {
                        address_selection_required:true

                    },
                    number_of_guest:{
                        required:true
                    },
                    'property_type[]':{
                        required:true

                    }
                },
                messages: {
                    location_detail: {
                        required: "Please type in a city."
                    },
                    modal_location_detail: {
                        required: "Please type in a city."


                    },
                    number_of_guest:{
                        required: 'Please select a guest.'
                    },
                    'property_type[]':{
                        required:'Please Select At-least one Prperty Type'
                    }
                }
            });
            jQuery.validator.addMethod("address_selection_required", function(value, element) {
                if(element.value == ''){
                    return false;
                }
                else if(element.value != '' && $(".address_selected_from_drop").val() != element.value){
                    return false;
                }
                return true;
            }, "Please type in a city.");

            $( "#searchForm" ).submit(function( event ) {
                let $form = $(this);

                if(!$form.valid())
                {
                    event.preventDefault();
                    return false;
                }
                else
                {
                    return true;
                }

            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#activateModal").modal('show');

            $('#activateModal').on('show.bs.modal', function() {
                $("#activateModal").modal("hide");
                $("#login_popup").modal("show");
                $('body').addClass("overflow-auto");
            });

            $('#activationLogin').on('click', function() {
                $("#activateModal").modal("hide");
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-auto");
            });

            $('#loginCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-y");
            });

            $('#regCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $('body').addClass("overflow-y");
            });

            $('#signupCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $('body').addClass("overflow-y");
            });

            $('#signup_popup').on('show.bs.modal', function (e) {
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                let createRentalTripperUser = $('#createRentalTripperUser');

                createRentalTripperUser.validate().resetForm();
                createRentalTripperUser.find('.error').removeClass('error');
            })

            $('#reg_popup').on('show.bs.modal', function (e) {
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                let rentaluserReg = $('#rentaluserReg');

                rentaluserReg.validate().resetForm();
                rentaluserReg.find('.error').removeClass('error');

            })

            $('#otp_popup').on('show.bs.modal', function (e) {
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                let otpForm = $('#otpForm');

                otpForm.validate().resetForm();
                otpForm.find('.otpError').hide();
            })

            $('#forgotpassword_popup').on('show.bs.modal', function (e) {
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                let forgotpasswordForm = $('#forgotpassword');

                forgotpasswordForm.validate().resetForm();
                forgotpasswordForm.find('.otpError').hide();
            })

        });
    </script>

<script>
        jQuery('#blog-page-carousal').owlCarousel({
            loop:true,
            margin:0,
            center: true,
            nav:true,
            dots: false,
            autoHeight: false,
            responsive:{
                0:{
                    items:1
                },
                640:{
                    items:1,
                    margin:10
                },
                768:{
                    items:2,
                    margin:30
                },
                1000:{
                    items:3,
                    nav:false,
                    margin:30
                }
            }
        });

</script>
@endsection
