@extends('layouts.rentalApp')
@section('content')

<div class="my-account-dashboard">
@include('rentaluser.sidebar')

<div id="content-wrapper" class="d-flex flex-column pt-20">
    <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
       <i class="fas fa-bars"></i>
    </button>
    <!-- Main Content -->
    <div id="content">
        <div class="container-fluid my-acc-dashbrd notification-page">
            <!-- Page Heading -->
            <h2 class="dashboard-title">MY <strong>settings</strong></h2>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form id="notificationForm" class="notification-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3><strong>Communication Preferences</strong></h3>
                            </div>
                            <div class="col-lg-6">
                                <ul id="notifi-type" class="notification-types">
                                    <li>Notifications</li>
                                    <li>Email</li>
                                    <li>SMS</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row mt-5 mb-15">
                            <div class="col-12">
                                <div class="row mb-5">
                                    <div class="col-lg-6">
                                        <p class="my-notifi-title">
                                            Important updates
                                        </p>
                                        <p class="my-notifi-content">
                                            Receive Messages from Hosts and Guests, Bookings
                                            Reminders, Requests to write a Review, Pricing notice,
                                            and other reminders related to your activities.
                                        </p>
                                    </div>
                                    <div class="col-lg-6 pl-1 pr-1">
                                        <ul class="notification-types mt-10">
                                            <li>
                                                <label class="notify-type-lbl">Notifications</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[important_updates][is_notification]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">Email</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[important_updates][is_email]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">SMS</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[important_updates][is_sms]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-lg-6">
                                        <p class="my-notifi-title">
                                            Promotions and Tips
                                        </p>
                                        <p class="my-notifi-content">
                                            Receive Coupons, Promotions, Surveys, Product Updates
                                        </p>
                                    </div>
                                    <div class="col-lg-6 pl-1 pr-1">
                                        <ul class="notification-types mt-10">
                                            <li>
                                                <label class="notify-type-lbl">Notification</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[promotions_and_tips][is_notification]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">Email</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[promotions_and_tips][is_email]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">SMS</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[promotions_and_tips][is_sms]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-lg-6">
                                        <p class="my-notifi-title">
                                            Policy and Community
                                        </p>
                                        <p class="my-notifi-content">
                                            Receive updates on home sharing regulations and about
                                            advocacy efforts to create fair and responsible home sharing
                                            laws in our community.
                                        </p>
                                    </div>
                                    <div class="col-lg-6 pl-1 pr-1">
                                    <ul class="notification-types mt-10">
                                            <li>
                                                <label class="notify-type-lbl">Notification</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[policy_and_community][is_notification]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">Email</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[policy_and_community][is_email]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">SMS</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[policy_and_community][is_sms]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-lg-6">
                                        <p class="my-notifi-title">
                                            Account Support
                                        </p>
                                        <p class="my-notifi-content">
                                            Receive messages about your account, your trips, legal
                                            notifications, security and privacy matters, and customer
                                            support requests. For your security, you can not disable
                                            email notifications and we may contact you by phone
                                            or other means if needed.
                                        </p>
                                    </div>
                                    <div class="col-lg-6 pl-1 pr-1">
                                        <ul class="notification-types mt-10">
                                            <li>
                                                <label class="notify-type-lbl">Notification</label>
                                                <span class="switch custom_readonly">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[account_support][is_notification]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">Email</label>
                                                <span class="switch custom_readonly">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[account_support][is_email]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                            <li>
                                                <label class="notify-type-lbl">SMS</label>
                                                <span class="switch">
                                                    <label>
                                                        <input type="checkbox" name="prefernces[account_support][is_sms]" />
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="mb-10 form-divide-line"/>
                        <div class="row notifi-unsubscribe">
                            <div class="col-9">
                                <p class="my-notifi-title">
                                    Unsubscribe Marketing Emails
                                </p>
                            </div>
                            <div class="col-3">
                                <ul class="notification-types text-right pr-2">
                                    <li>
                                        <span class="switch">
                                            <label>
                                                <input type="checkbox" name="is_subscribed" />
                                                <span></span>
                                            </label>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <hr class="form-divide-line mb-15"/>
                        <div class="row notifi-dropdown mb-1">
                            <div class="col-lg-6 ">
                                <p class="my-notifi-title pt-9">
                                    Currency
                                </p>
                            </div>
                            <div class="col-lg-6 ">
                                <div class="form-group-custm no-icon">
                                <select class="form-class-cstm setting-lang-dropdown" name="currency">
                                    <option>US Dollar</option>
                                </select>
                            </div>
                        </div>
                        </div>
                            <div class="row notifi-dropdown mb-8" >
                                <div class="col-lg-6 ">
                                    <p class="my-notifi-title pt-9">
                                        Site Language
                                    </p>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group-custm no-icon">
                                        <select class="form-class-cstm setting-lang-dropdown" name="language">
                                            <option>English</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="button-default-animate button-default-custom btn-sunset-gradient mt-5 saveNotifications">
                                Save Changes
                            </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="domMessage" style="display:none;">
    <h1>Your Preferences are saved.</h1>
</div>
@endsection
@section('footerJs')
@parent
<script>
var notificationUrl = "{{route('save-notifications') }}";
var preferences = @JSON($rentalUser->communication_preferences);
var is_subscribed = @JSON($rentalUser);
</script>
<script src="{{ URL::asset('js/myaccount/myaccount.js')}}"></script>
@endsection
