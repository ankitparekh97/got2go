
     <div class="modal fade reg_popup" id="reg_popup" tabindex="-1" role="dialog" aria-labelledby="reg_popup" aria-hidden="true" data-controls-modal="reg_popup" data-keyboard="false">
        <div id="loader" style="display: none;">
            <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
        </div>
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" id="regCloseModal" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body" id="regBody">
                <h1> Create <strong>account</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                         
                        <form method="POST" id="rentaluserReg" action="{{ route('usersignup') }}">
                            @csrf

                            <input type="hidden" name="rentalUserEmailTemp" id="rentalUserEmailTemp" value="">
                            <div class="d-block text-center">
                                <a href="{{ url('auth/rentaluser/google') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/google.png')}}" alt="google"></a>
                                <a href="{{ url('auth/rentaluser/facebook') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/favebook-login-btn.png')}}" alt="facebook"></a>
                            </div>
                            <div class="forget-password">
                                Or use your email account:
                            </div>

                            <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text"></div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="21.732" height="26.92" viewBox="0 0 25.732 26.92">
                                            <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                                                <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                                                <g id="Group_406" data-name="Group 406" transform="translate(0)">
                                                    <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#939FA4"></path>
                                                </g>
                                                </g>
                                                <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                                                <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                                                    <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#939FA4"></circle>
                                                </g>
                                                </g>
                                            </g>
                                            </svg>
                                    </span>
                                </div>
                                <input type="text" class="form-control required" name="first_name" id="first_name" placeholder="First Name">
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="21.732" height="26.92" viewBox="0 0 25.732 26.92">
                                            <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                                                <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                                                <g id="Group_406" data-name="Group 406" transform="translate(0)">
                                                    <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#939FA4"></path>
                                                </g>
                                                </g>
                                                <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                                                <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                                                    <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#939FA4"></circle>
                                                </g>
                                                </g>
                                            </g>
                                            </svg>
                                    </span>
                                </div>
                                <input type="text" class="form-control required" name="last_name" id="last_name" placeholder="Last Name">
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="21.732" height="26.92" viewBox="0 0 25.732 26.92">
                                            <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                                                <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                                                <g id="Group_406" data-name="Group 406" transform="translate(0)">
                                                    <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#939FA4"></path>
                                                </g>
                                                </g>
                                                <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                                                <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                                                    <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#939FA4"></circle>
                                                </g>
                                                </g>
                                            </g>
                                            </svg>
                                    </span>
                                </div>
                                <input type="password" class="form-control required" name="password" id="password" passwordCheck="passwordCheck" placeholder="Password">
                            </div>

                            <button type="submit" value="Signup" class="mt-10 mb-10 button-default-animate">Sign up <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="fly" class="lazy-load ml-5"></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
     </div>

    @section('footerJs')
    @parent
    <script>
        var CaptchaCallback = function() {

                var widgetId1;
                var widgetId2;

                widgetId1 = grecaptcha.render('recaptcha', {'sitekey' : "{{env('RECAPTCHA')}}", 'callback' : recaptchaCallback});
                widgetId2 = grecaptcha.render('regRecaptcha', {'sitekey' : "{{env('RECAPTCHA')}}", 'callback' : regcaptchaCallback});
        };
        var recaptchaCallback = function(response) {
            $("#hiddenRecaptcha").val(response);
        };
        var regcaptchaCallback = function(response) {
            $("#reghiddenRecaptcha").val(response);
        };

        $(document).ready(function() {

            $('ul.navbar-nav.ml-auto li.nav-item a').click(function() {
                $('li.nav-item a').removeClass("active-menu-link");
                $(this).addClass("active-menu-link");
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $('body').addClass("overflow-hidden");
                $('#reg_popup').addClass("overflow-show");
            });

            $('#regCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $('body').addClass("overflow-y");
            });

            $('#reg_popup').on('hidden.bs.modal', function (e) {
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                var $alert = $('#usersignup');
                $alert.validate().resetForm();
                $alert.find('.error').removeClass('error');
            })
        });

        $('#rentaluserReg').validate({
             rules: {
                first_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 30
                },
                last_name: {
                     required: true,
                     minlength: 2,
                     maxlength: 30
                },
                password:{
                     required:true,
                     minlength: 6,
                     maxlength: 20
                },
            },
            messages: {
                name: {
                    required: "First Name is required"
                },
                last_name: {
                    required: "Last Name is required"
                },
                email: {
                    required: "Email is required",
                    email:"Please enter valid email address"
                },
                password: {
                    required:"Password field is required",
                    minlength: "Password should be minimum 6 characters",
                    maxlength: "Password must be maximum 20 characters",
                },
            },
        });

        function regcaptchaCallback() {
            $('#regRecaptcha').valid();
        };

        jQuery.validator.addMethod("passwordCheck",
            function(value, element, param) {
                if (this.optional(element)) {
                    return true;
                } else if (!/[A-Z]/.test(value)) {
                    return false;
                } else if (!/[a-z]/.test(value)) {
                    return false;
                } else if (!/[0-9]/.test(value)) {
                    return false;
                } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                    return false;
                }

                return true;
            },
            "Password should contain combination of uppercase, lowercase, digits & special characters");

        $('#rentaluserReg').on('submit', function(e) {
            e.preventDefault();
            let $form = $(this);
            if(!$form.valid()) return false;
            $("#activateModal").modal("hide");

            KTApp.block('#reg_popup .modal-content', {
                overlayColor: 'red',
                state: 'danger',
                message: 'Please wait...'
            });

            setTimeout(function() {
                KTApp.unblock('#reg_popup .modal-content');
            }, 1000);

            $.ajax({
                type: "POST",
                url: "{{ route('usersignup') }}",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    if($.isEmptyObject(msg.error)){

                        let regSuccMsg = '<p style="text-align: center;font-weight: bold;">Thank you for your registration. Kindly check your registered email to activate your Got2Go rental account.</p>';
                        $('#regBody').empty().append(regSuccMsg);

                        let $modalDialog = $("#reg_popup");
                        $modalDialog.modal('hide');

                        $('#otp_popup').modal('show');

                    } else {

                        KTApp.unblock('#reg_popup .modal-content');
                        setTimeout(function() {
                            $(".alert-light-danger").show();
                            $.each( msg.error, function( key, value ) {
                                $(".alert-text").empty().html(value);
                            });
                        })
                    }

                }
            });
        });
    </script>
    @endsection
