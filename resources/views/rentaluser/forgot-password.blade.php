
     <div class="modal fade" id="forgotpassword_popup" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="forgotpassword_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" id="forgotpasswordCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1> Forgot <strong>Password</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <div class="alert alert-custom alert-notice alert-light-danger forgotpasswordmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text"></div>
                        </div>

                        <div class="alert alert-custom alert-notice alert-light-success fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text" style="font-weight: bold;"> </div>
                        </div>

                        <form method="POST" id="forgotpassword" action="{{ route('forgotRenterTripperPassword') }}" >
                            @csrf
                            <div class="input-group login-custom">
                                <input type="email" id="emailId" class="form-control" name="emailId" placeholder="Email Address">
                            </div>
                            <div class="forget-password">
                                Already have a account ? <a href="#" data-toggle="modal" data-target="#login_popup">Sign In</a>
                            </div>
                            <button type="submit" class="mb-10 button-default-animate" value="login"> Reset <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="fly" class="lazy-load ml-5"></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
     </div>

@section('footerJs')
    @parent
<script>
 $(document).ready(function() {

    $('#forgotpasswordCloseModal').on('click', function() {
        $("#activateModal").modal("hide");
        $('body').addClass("overflow-y");
    });

    $('#forgotpassword_popup').on('show.bs.modal', function (e) {
        $(this)
            .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=text], input[type=radio]")
            .prop("checked", "")
            .end();

        $(".alert-danger").hide();

        let forgotpasswordForm = $('#forgotpassword');

        forgotpasswordForm.validate().resetForm();
        forgotpasswordForm.find('.otpError').hide();
    })

    $('#forgotpassword').validate({
        rules: {
            emailId: {
                required: true,
                email:true,
            },
        },
        messages: {
            emailId: {
                required: "Email is required",
                email:"Please enter valid email address"
            },

        }
    });

     // submit form on enter key
     $('#emailId').keyup(function(event) {
            if (event.which === 13) {
                event.preventDefault();
                $('#forgotpassword').submit();
            }
        });

    // email form
    $('#forgotpassword').on('submit', function(e) {
        e.preventDefault();
        let $form = $(this);
        if(!$form.valid()) return false;

        showLoader();

        $.ajax({
            type: "POST",
            url: "{{ route('forgotRenterTripperPassword') }}",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                hideLoader();
                if($.isEmptyObject(msg.error)){
                    if(msg.success == true){

                        Swal.fire({
                            icon: 'success',
                            text: 'The temporary password has been sent to your registered email.',
                            showConfirmButton: false,
                            timer: 2500
                        })

                        setTimeout(function() {
                            $("#forgotpassword_popup").modal("hide");
                        }, 2500);
                    }

                } else {

                    Swal.fire({
                        icon: 'error',
                        title: 'This email is not registered.',
                        text: 'Kindly enter your registered email to recover your Got2Go account.',
                        showConfirmButton: true
                    })
                }

            }
        });
    });
 });

</script>
@endsection