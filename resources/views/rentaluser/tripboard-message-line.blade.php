
<li class="date-separator">
    <div class="separator-info"><span>{{$date}}</span></div>
</li>
@foreach($messages as $message)
@if($message->from_user == \Auth::guard('rentaluser')->user()->id)
<li class="contact-chat active replies user-chat-white" data-message-id="{{ $message->id }}">
<div class="contact-wrap">
    <div class="chatuser-img">
        @if($message->rentalUser->photo != '')
            <img data-src="{{URL::asset('uploads/users/'.$message->rentalUser->photo)}}" alt="photo" class="lazy-load mCS_img_loaded">
        @else
            <div id="profileImage" class="rental">{{$message->rentalUser->first_name[0].$message->rentalUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="chatuser-info">
        <div class="preview">{!! $message->messages !!}</div>
        <div class="chat-date">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</div>
</li>
@else
<li class="contact-chat active user-chat-blue" data-message-id="{{ $message->id }}">
<div class="contact-wrap">
    <div class="chatuser-img">
         @if($message->rentalUser->photo != '')
            <img data-src="{{URL::asset('uploads/users/'.$message->rentalUser->photo)}}" alt="photo" class="lazy-load mCS_img_loaded">
        @else
            <div id="profileImage" class="rental">{{$message->rentalUser->first_name[0].$message->rentalUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="chatuser-info">
        <div class="preview">{!! $message->messages !!}</div>
        <div class="chat-date">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</div>
</li>
@endif
@endforeach

