
     <div class="modal fade" id="signup_popup" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="signup_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" id="signupCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1> Create <strong>account</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <div class="alert alert-custom alert-notice alert-light-danger signupmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text"></div>
                        </div>

                        <div class="alert alert-custom alert-notice alert-light-success fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text" style="font-weight: bold;"> </div>
                        </div>

                        <form method="POST" id="createRentalTripperUser" action="{{ route('createRentalTripperUser') }}" >
                            @csrf

                            <div class="d-block text-center login-social-media">
                                <a href="{{ url('auth/rentaluser/google') }}"><div class="bg-google image"></div></a>
                                <a href="{{ url('auth/rentaluser/facebook') }}"><div class="bg-favebook_login_btn image"></div></a>
                            </div>
                            <div class="forget-password">
                                Or use your phone number <strong class="bolder">or</strong> email to login:
                            </div>
                            <label class="form-errors"></label>
                            <div class="input-group login-custom">

                                <input type="text" id="registerphoneno" class="form-control registerphoneno" name="registerphoneno" placeholder="Phone Number">
                            </div>
                            <div class="input-group login-custom">
                                <input type="email" id="rentalEmail" class="form-control rentalEmail" name="email" placeholder="Email Address">
                            </div>

                            <button type="submit" class="mb-10 button-default-animate nextbutton" value="login"> Next <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="" class="lazy-load ml-5"></button>
                            <div class="forget-password">
                                Already have an account? Click to <a href="#" data-toggle="modal" data-target="#login_popup">Login</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
     </div>


     <div class="modal fade" style="display:none" id="reg_popup" tabindex="-1" role="dialog" aria-labelledby="reg_popup" aria-hidden="true" data-controls-modal="reg_popup" data-keyboard="false">
        <div id="loader" style="display: none;">
            <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
        </div>
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" id="regCloseModal" class="close" data-dismiss="modal" aria-label="Close">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body" id="regBody">
                <h1> Create <strong>account</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <form method="POST" id="rentaluserReg" action="{{ route('usersignup') }}">
                            @csrf

                            <input type="hidden" name="rentalUserEmailTemp" id="rentalUserEmailTemp" value="">
                            <input type="hidden" name="userPhoneNo" id="userPhoneNo" value="">
                            <input type="hidden" name="userOtp" id="userOtp" value="">

                            <p>
                                <span id="tempEmailDisplay"></span>
                                <span><a href="javascript:void(0)" id="edit_email"> Edit email </a> </span>
                            </p>

                            <div class="alert alert-custom alert-notice alert-light-danger regmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text"></div>
                            </div>

                            <div class="input-group login-custom">
                                <input type="text" class="form-control required" name="first_name" id="first_name" placeholder="First Name">
                            </div>

                            <div class="input-group login-custom">
                                <input type="text" class="form-control required" name="last_name" id="last_name" placeholder="Last Name">
                            </div>

                            <div class="input-group login-custom">
                                <input type="password" class="form-control rentalUserPassword required" name="password" id="passwordRental" passwordCheck="passwordCheck" placeholder="Password">
                                <span toggle="#password-field" class="fa fa-fw field-icon rentalPassword toggle-password show-password fa-eye"></span>
                                <div class="passwordError"></div>
                            </div>

                            <button type="submit" value="Signup" class="mt-10 mb-10 button-default-animate">Sign up <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="" class="lazy-load ml-5"></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
     </div>


     <div class="modal fade " style="display:none" id="otp_popup" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="otp_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div id="loader" class="loader" style="display: none;">
                    <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" id="otpCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 0.810997L4.811 4L8 7.189L7.189 8L4 4.811L0.810997 8L0 7.189L3.189 4L0 0.810997L0.810997 0L4 3.189L7.189 0L8 0.810997Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1> Registration <strong>Code</strong> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <div class="alert alert-custom alert-notice alert-light-danger otpmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text"></div>
                        </div>

                        <div class="alert alert-custom alert-notice alert-light-success otpSuccessmodal fade show mb-5 tripperUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text" style="font-weight: bold;"> </div>
                        </div>

                        <form method="POST" id="otpForm" action="{{ route('authenticateUserOtp') }}" >
                            @csrf

                            <input type="hidden" name="encryptedId" id="encryptedId" value="">

                            <div class="forget-password mt-5 mb-15">
                                Please enter the registration code received:
                            </div>
                            <div class="form-group regis-code rentalOTP">
                                <input type="text" class="reg-code-digit" name="code1" id="code1" maxlength="1" oninput="numberOnly(this.id);" >
                                <input type="text" class="reg-code-digit" name="code2" id="code2" maxlength="1" oninput="numberOnly(this.id);" >
                                <input type="text" class="reg-code-digit" name="code3" id="code3" maxlength="1" oninput="numberOnly(this.id);" >
                                <input type="text" class="reg-code-digit" name="code4" id="code4" maxlength="1" oninput="numberOnly(this.id);" >
                                <input type="text" class="reg-code-digit" name="code5" id="code5" maxlength="1" oninput="numberOnly(this.id);" >
                            </div>

                            <div id="otpError"></div>
                            <div class="some_div"></div>


                            <button type="submit" class="mb-10 button-default-animate otpButton" value="login"> Verify Code <img data-src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="" class="lazy-load ml-5"></button>
                            <div>
                                <span class="forget-password" style="">
                                    Didn’t receive the OTP?
                                  <a href="#" id="resendOtp" class="resendOtpRental">Resend OTP</a>
                                    60sec
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
     </div>


     <div class="modal fade" style="display:none" id="activatepopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="otpCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1 class="px-5"> <strong>ACCOUNT CREATED!</strong></h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">

                        <div class="alert alert-custom alert-notice alert-light-danger signupmodal fade show mb-5" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text"></div>
                        </div>

                        <div class="alert alert-custom alert-notice alert-light-success fade show mb-5 tripperUI" role="alert" style="display: none; padding:0.75rem 1.25rem !important;">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text" style="font-weight: bold;"> </div>
                        </div>

                        {{-- <div class="d-block text-center">
                            <a href="{{ url('auth/rentaluser/google') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/google.png')}}" alt=""></a>
                            <a href="{{ url('auth/rentaluser/facebook') }}"><img class="lazy-load" data-src="{{URL::asset('media/images/tripper-page/favebook-login-btn.png')}}" alt=""></a>
                        </div> --}}

                        <div class="forget-password mt-5 mb-15">
                           Your account is now active. You can now reserve listings, create tripboards, list your property and more!
                        </div>
                         <a class="mb-10 button-default-animate dahsboard-link" href="{{ url('setting')}}" style="    padding: 20px;display: inline-block;width: auto;">Go To Dashbaord<img src="{{URL::asset('media/images/fly-orange-gradient.png')}}" alt="" class="ml-5"></a>

                    </div>
                </div>

            </div>
        </div>
     </div>

    @section('footerJs')
    @parent
    <script>

        var container = document.getElementsByClassName("rentalOTP")[0];
        container.onkeyup = function(e) {
            var target = e.srcElement;
            var maxLength = parseInt(target.attributes["maxlength"].value, 10);
            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() == "input") {
                        next.focus();
                        break;
                    }
                }
            }
        }

        $(document).ready(function() {

             // submit form on enter key
            $('#code5').keyup(function(event) {
                if (event.which === 13) {
                    event.preventDefault();
                    $('#otpForm').submit();
                }
            });

            $('#rentalEmail').keyup(function(event) {
                if (event.which === 13) {
                    event.preventDefault();
                    $('#createRentalTripperUser').submit();
                }
            });

            $('#passwordRental').keyup(function(event) {
                if (event.which === 13) {
                    event.preventDefault();
                    $('#rentaluserReg').submit();
                }
            });

            $('#signup_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $("#otp_popup").modal("hide");
                $("#activatepopup").modal("hide");
                $("#reg_popup").modal("hide");
                $(this)
                    .find("input[type=text], input[type=email], input[name=phone], input[type=password]")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=text], input[type=radio]")
                    .prop("checked", "")
                    .end();

                $(".alert-danger").hide();

                let createRentalTripperUser = $('#createRentalTripperUser');

                createRentalTripperUser.validate().resetForm();
                createRentalTripperUser.find('.error').removeClass('error');
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $("#signup_popup").modal("hide");
                $("#otp_popup").modal("hide");
                $("#activatepopup").modal("hide");
            });

            $('#otp_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $("#signup_popup").modal("hide");
                $("#reg_popup").modal("hide");
                $("#activatepopup").modal("hide");
                $('#code1').trigger('focus');
            });

            $('#activatepopup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $("#signup_popup").modal("hide");
                $("#reg_popup").modal("hide");
                $("#otp_popup").modal("hide");
            });
            $('#regCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $('body').addClass("overflow-y");
            });

            $('#signupCloseModal').on('click', function() {
                $("#activateModal").modal("hide");
                $('body').addClass("overflow-y");
                $('.rentalEmail').val('');
            });

            jQuery.validator.addMethod("passwordCheck",
                function(value, element, param) {
                    if (this.optional(element)) {
                        return true;
                    } else if (!/[A-Z]/.test(value)) {
                        return false;
                    } else if (!/[a-z]/.test(value)) {
                        return false;
                    } else if (!/[0-9]/.test(value)) {
                        return false;
                    } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                        return false;
                    }

                    return true;
                }, "This password needs to have at least 6 characters with 1 uppercase letter, 1 number, and 1 special character."
            );

            $(".toggle-password").click(function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $(".rentalUserPassword");

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $("#edit_email").click(function() {
                $("#reg_popup").modal("hide");
                $("#signup_popup").modal("show");
                var email = $("#rentalUserEmailTemp").val();
                var phone = $("#userPhoneNo").val();
                if(email!=''){
                    $('.rentalEmail').empty().val(email);
                }
                if(phone!=''){
                    $('.registerphoneno').empty().val(phone);
                }
            });

            $('#signupCloseModal','#regCloseModal','#otpCloseModal','#activateCloseModal').on('click', function() {
                $('body').addClass("overflow-y");
            });
            $('#phoneno').mask('(000) 000-0000',{placeholder: "Phone number"});
            $('#registerphoneno').mask('(000) 000-0000',{placeholder: "Phone number"});
            $.validator.addMethod("regx", function(value, element, regexpr) {
                return this.optional(element) || regexpr.test(value);
            }, "Please enter valid phone number.");
            $('#createRentalTripperUser').validate({
                rules: {
                    email: {
                        email:true,
                    },
                    registerphoneno:{
                        regx:/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
                    }
                },
                messages: {
                    email: {
                        email:"Please enter valid email address"
                    },
                    registerphoneno:{
                    }

                },
            });

            $('#registerphoneno,#email').on('keyup',function(){
                if($("#registerphoneno").val() != ''){
                    $("#registerphoneno").removeClass('error');
                    $("#email").removeClass('error');
                }
                if( $("#email").val() != ''){
                    $("#email").removeClass('error');
                    $("#registerphoneno").removeClass('error');
                }
                $(".form-errors").html('').removeClass('error');
            });
            // email form
            $('#createRentalTripperUser').on('submit', function(e) {
                e.preventDefault();
                let $form = $(this);

                $registerphoneno = $("#createRentalTripperUser").find("#registerphoneno");
                $email = $("#createRentalTripperUser").find("#rentalEmail");
                if($registerphoneno .val() == '' && $email.val() == ''){
                    $registerphoneno.addClass('error');
                    $email.addClass('error');
                    $(".form-errors").html("Email address and/or phone number is required.").css('display','block').addClass('error');
                    return false;
                }
                if(!$form.valid()) return false;

                showLoader();
                $(".signupmodal").hide();
                $(".signupmodal .alert-text").empty().html('');

                $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    dataType: "JSON",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(msg) {
                        hideLoader();
                        if($.isEmptyObject(msg.error)){

                            if(msg.rentalUser!= null)
                            {
                                $(".signupmodal").show();
                                $(".signupmodal .alert-text").empty().html('You already have an account. Please <a href="" class="font-light loginPopUp" data-toggle="modal" data-target="#login_popup">log in </a>?');

                            } else {
                                $("#rentaluserReg")[0].reset();
                                $('#rentalUserEmailTemp').empty().val(msg.userEmail);
                                $('#userPhoneNo').empty().val(msg.userPhone);
                                $('#UserOtp').empty().val(msg.userOtp);
                                if(msg.userEmail != null){
                                    $('span#tempEmailDisplay').empty().text(msg.userEmail);
                                    $('#edit_email').empty().text('Edit Email');
                                }else if(msg.userPhone != null){
                                    $('span#tempEmailDisplay').empty().text(msg.userPhone);
                                    $('#edit_email').empty().text('Edit Phone');
                                }

                                $("#signup_popup").modal('hide');
                                $('#code1').val('');
                                $('#code2').val('');
                                $('#code3').val('');
                                $('#code4').val('');
                                $('#code5').val('');
                                $(".some_div").empty();
                                $('#otp_popup').modal('show');
                                clearInterval(id);
                                setCounter();
                            }

                        } else {

                            setTimeout(function() {
                                $(".signupmodal").show();
                                $.each( msg.error, function( key, value ) {
                                    $(".signupmodal .alert-text").empty().html(value);
                                });
                            })
                        }

                    }
                });
            });

            $('#rentaluserReg').validate({
                //  ignore: ".ignore",
                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    },
                    last_name: {
                        required: true,
                        minlength: 2,
                        maxlength: 30
                    },
                    password:{
                        required:true,
                        minlength: 6,
                        maxlength: 20
                    },
                },
                messages: {
                    first_name: {
                        required: "Please enter valid First Name"
                    },
                    last_name: {
                        required: "Please enter valid Last Name"
                    },
                    password: {
                        required:"Please enter in password.",
                        minlength: "Password should be minimum 6 characters",
                        maxlength: "Password must be maximum 20 characters",
                    },
                },
                errorPlacement: function(error,element){
                    var password = $('.passwordError');
                    if (element.attr('id')=='passwordRental') {
                        $(password).append(error)
                    } else  {
                        error.insertAfter(element);
                    }
                }
            });

            // sign up form
            $('#rentaluserReg').on('submit', function(e) {
                e.preventDefault();
                let $form = $(this);

                if(!$form.valid()) return false;

                showLoader();

                $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    dataType: "JSON",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(msg) {
                        hideLoader();
                        if($.isEmptyObject(msg.error)){
                            $("#reg_popup").modal('hide');
                            $('#activatepopup').modal('show');

                            setTimeout(function() {
                                $('#activatepopup').modal('hide');
                            }, 8000);

                            setTimeout(function() {
                                if(msg.session.tripboard_share_id != null) {
                                    $("#tripboardConfirm").modal('show');
                                    $('#tripboard_name_display').text(msg.session.tripboard_name);
                                } else {
                                    location.reload();
                                }
                            }, 8000);

                        } else {

                            setTimeout(function() {
                                $(".regmodal").show();
                                $.each( msg.error, function( key, value ) {
                                    $(".regmodal .alert-text").empty().html(value);
                                });
                            })
                        }

                    }
                });
            });

            $('#otpForm').validate({
                rules: {
                    code1: {
                        required: true,
                    },
                    code2: {
                        required: true,
                    },
                    code3: {
                        required: true,
                    },
                    code4: {
                        required: true,
                    },
                    code5: {
                        required: true,
                    }
                },
                groups: {
                    code: "code1 code2 code3 code4 code5"
                },
                messages: {
                    code1: {
                        required: "Registration code is required",
                    },
                    code2: {
                        required: "Registration code is required",
                    },
                    code3: {
                        required: "Registration code is required",
                    },
                    code4: {
                        required: "Registration code is required",
                    },
                    code5: {
                        required: "Registration code is required",
                    }
                },
                errorPlacement: function(error, element) {
                  var otpError = $('#otpError');
                    $(otpError).append(error)
                }
            });

            // Otp form
            $('#otpForm').on('submit', function(e) {
                e.preventDefault();
                let $form = $(this);

                if(!$form.valid()) return false;

                let code1 = $('#code1').val();
                let code2 = $('#code2').val();
                let code3 = $('#code3').val();
                let code4 = $('#code4').val();
                let code5 = $('#code5').val();

                let otp_code = code1 + code2 + code3 + code4 + code5;

                showLoader();

                $.ajax({
                    type: "POST",
                    url: "{{ route('authenticateUserOtp') }}",
                    dataType: "JSON",
                    data: {
                        otp_code: otp_code
                    },
                    success: function(msg) {
                        hideLoader();
                        if($.isEmptyObject(msg.error)){

                            $('#otp_popup').modal('hide');
                            $('#reg_popup').modal('show');

                        } else {

                            setTimeout(function() {

                                $(".otpmodal").show();
                                $(".otpmodal .alert-text").html('');
                                $.each( msg.error, function( key, value ) {
                                    $(".otpmodal .alert-text").html(value);
                                });
                            }, 2000)
                        }

                    }
                });
            });

            // resend OTP
            $("body").delegate("#resendOtp", "click", function(e){
                e.preventDefault();

                $('#code1').val('');
                $('#code2').val('');
                $('#code3').val('');
                $('#code4').val('');
                $('#code5').val('');

                showLoader();

                $.ajax({
                    type: "POST",
                    url: "{{ route('resendRenterTripperOtp') }}",
                    dataType: "JSON",
                    data: {
                        id : $('#encryptedId').val()
                    },
                    success: function(msg) {
                        hideLoader();
                        if($.isEmptyObject(msg.error)) {
                            $(".otpmodal").hide();

                            if(msg.success == true){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'A new registration code has been sent to your registered email or phone',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(() => {
                                    $(".some_div").empty();
                                    clearInterval(id);
                                    setCounter();
                                });
                            } else {
                                $('.otpSuccessmodal').hide();
                            }

                        }else{
                            if(msg.error!=''){

                                setTimeout(function() {

                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Could not send One Time Password. Please try again after sometime or contact site administrator',
                                        showConfirmButton: true,
                                    })
                                }, 2000);
                            }
                        }
                    }
                });
            })

            $('#activationLogin').on('click', function() {
                $("#activatepopup").modal("hide");
                $('body').addClass("overflow-auto");
            });

        });

        function numberOnly(id) {
            var element = document.getElementById(id);
            var regex = /[^0-9]/gi;
            element.value = element.value.replace(regex, "");
        }

        var id;
        function setCounter() {    // To Load the Popupbox
            var counter = 60;
            id = setInterval(function() {
                counter--;
                if(counter < 0) {
                    clearInterval(id);

                    $(".some_div").html("Time's up! Please click <a href='javascript:;' id='resendOtp' class='resendOtpRental' style='color: red;font-weight: bold;'>Resend</a> to receive a new code").css('color','red');
                } else {
                    $(".some_div").text("Code will expire in - 00:" + (counter.toString() <10?'0':'')+counter.toString()).css('color','red');
                }
            }, 1000);

        }
    </script>
    @endsection
