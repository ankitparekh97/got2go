@php
    $subscribeDate = getLoggedUserSubcribeDate();
    if(!empty($subscribeDate)){
        $subscribeMonthMMM = carbonParseDate($subscribeDate)->format('M');
        $subscribeDateDO = carbonParseDate($subscribeDate)->format('d');
        $subscribeDateDO = ordinal($subscribeDateDO);
        $subscribeDateMonth = carbonParseDate($subscribeDate)->format('m');
        $subscribeDateYear = carbonParseDate($subscribeDate)->format('y');
        $subscribeDateArr = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$subscribeDateDO);
        $subscribeDateDTH = carbonParseDate($subscribeDate)->format('M jS');
    }
@endphp
<!-- new sidebar starts here -->
<div class="navbar-nav left-sidebar accordion pt-20" >
    @if(getLoggedInRental() && isLoggedInUserIsTripper() == 1)
        <div class="tripper-img-mobile">
            <img class="lazy-load" data-src="{{ URL::asset('media/images/my-dashboard/tripper.png') }}" alt="tripboard">
        </div>
        <div class="tripper-since-txt">
            Since
            <strong class="common-fatfrank">
                @if(!empty($subscribeDate)) {{ $subscribeMonthMMM  }} {{ $subscribeDateArr[0] }}
                <sup>{{ $subscribeDateArr[1] }}</sup>@endif
            </strong>
        </div>
    @endif
    <div class="left-sidebar-nav-item user user-image-box tripper-image">
        <div class="left-sidebar-nav-link" href="#">
            <span class="left-sidebar-user-img">
                <img class="lazy-load" data-src="{{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo ? URL::asset('uploads/users/'.\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo):URL::asset('media/images/user-icon-zero-state.png')}}" alt="zero state">
            </span>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#UploadProfileModal">
                <div class="hello-lydia-down-arrow"></div>
            </a>
        </div>
        <div class="user-name-mobile">Hello <span class="user-text">
        @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
            {{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->first_name}} !
        @endif
        </span></div>
    </div>
    @if(getLoggedInRental() && isLoggedInUserIsTripper() == 1)
        <div class="dollars-saved-since commom-open">
            <strong>
            {{ Helper::formatMoney(getTripperAllSavedAmountTill(),1) }}
            </strong>
            saved since {{$subscribeDateDTH}}
        </div>
    @endif
    <div class="responsive-scroll">
        <ul class="dashboar-sidebar">
            <li class="left-sidebar-nav-item {{ (Request::segment(1) == 'dashboard')  ? 'active' : '' }}">
                <a class="left-sidebar-nav-link"  href="{{ url('dashboard')}}">
                    <span class="">
                        <i class="fas fa-home"></i>
                    </span> Dashboard
                </a>
            </li>
            <li class="left-sidebar-nav-item {{ (Request::segment(1) == 'mystay' || Request::segment(1) == 'mystay-details')  ? 'active' : '' }}">
                <a class="left-sidebar-nav-link" href="{{ url('mystay')}}">
                    <span class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26.876" height="26.871" viewBox="0 0 26.876 26.871">
                            <g id="my_stays_svg" transform="translate(-1777.335 -208.62)">
                                <g id="my_stays_svg" data-name="my_stays_svg" transform="translate(1777.335 208.62)">
                                    <path id="Path_2543" data-name="Path 2543" d="M1779.852,268.013c0,.29,0,.533,0,.775a1.68,1.68,0,1,0,3.359,0c0-.243,0-.486,0-.752H1793.3c0,.31-.018.617,0,.921a1.673,1.673,0,0,0,2.519,1.337,1.623,1.623,0,0,0,.828-1.422c.008-.271,0-.542,0-.808a1.9,1.9,0,0,1,2.506,1.737c.038,2.182.011,4.365.011,6.574h-1.662v-3.3h-18.491v.307q0,5.52,0,11.04c0,.383.008.393.4.393l9.318,0h.329v1.676h-.335l-9.4,0a1.853,1.853,0,0,1-1.986-1.973q-.006-7.271,0-14.541a1.845,1.845,0,0,1,1.961-1.966Z" transform="translate(-1777.335 -264.655)" fill="" />
                                    <path id="Path_2544" data-name="Path 2544" d="M2028.789,453.28a6.717,6.717,0,1,1-6.719-6.721A6.736,6.736,0,0,1,2028.789,453.28Zm-1.677-.02a5.038,5.038,0,1,0-5.094,5.055A5.048,5.048,0,0,0,2027.112,453.26Z" transform="translate(-2001.913 -433.121)" fill="" />
                                    <path id="Path_2545" data-name="Path 2545" d="M1840.539,390.777h-3.316V387.46h3.316Z" transform="translate(-1833.841 -377.36)" fill="" />
                                    <path id="Path_2546" data-name="Path 2546" d="M1929.924,390.737h-3.293v-3.318h3.293Z" transform="translate(-1918.199 -377.321)" fill="" />
                                    <path id="Path_2547" data-name="Path 2547" d="M2015.629,390.392v-3.338h.6q1.241,0,2.482,0c.143,0,.254,0,.253.2-.007,1.006,0,2.012-.007,3.018a.6.6,0,0,1-.027.119Z" transform="translate(-2002.171 -376.976)" fill="" />
                                    <path id="Path_2548" data-name="Path 2548" d="M1840.51,476.809v3.3h-3.321v-3.3Z" transform="translate(-1833.81 -461.663)" fill="" />
                                    <path id="Path_2549" data-name="Path 2549" d="M1926.613,476.667h3.3v3.3h-3.3Z" transform="translate(-1918.182 -461.529)" fill="" />
                                    <path id="Path_2550" data-name="Path 2550" d="M1838.51,211.155c0,.535,0,1.071,0,1.606a.832.832,0,1,1-1.661.012q-.011-1.634,0-3.268a.833.833,0,1,1,1.661.016c0,.545,0,1.089,0,1.634Z" transform="translate(-1833.483 -208.62)" fill="" />
                                    <path id="Path_2551" data-name="Path 2551" d="M2076.523,211.14c0,.535,0,1.071,0,1.606a.831.831,0,1,1-1.658.012q-.012-1.62,0-3.24a.836.836,0,1,1,1.666.016c0,.272,0,.545,0,.817S2076.523,210.877,2076.523,211.14Z" transform="translate(-2058.055 -208.62)" fill="" />
                                    <path id="Path_2552" data-name="Path 2552" d="M2070.225,535.489c.581-.586,1.135-1.147,1.692-1.707q.6-.6,1.2-1.2a.839.839,0,1,1,1.19,1.173q-1.7,1.709-3.413,3.411a.861.861,0,0,1-1.377-.01q-1.1-1.095-2.194-2.2a.843.843,0,1,1,1.194-1.173Z" transform="translate(-2050.643 -513.995)" fill="" />
                                </g>
                            </g>
                        </svg>
                    </span> My Stays
                </a>
            </li>
            @if(getLoggedInRental() && isLoggedInUserIsTripper() == 1)
            <li class="left-sidebar-nav-item {{ getActiveClass(route('web.tripper.myOffers')) }}">
                <a class="left-sidebar-nav-link" href="{{ route('web.tripper.myOffers') }}">
                    <span class="">
                        <svg width="35" height="23" viewBox="0 0 35 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.49392 3.30542C9.55327 3.99319 10.7739 4.39236 12.0349 4.46342C11.9579 4.59342 11.9079 4.68542 11.8509 4.77142C11.3989 5.44542 10.9379 6.11342 10.4949 6.79242C10.134 7.28898 9.94477 7.88965 9.95592 8.50342C9.95909 8.9863 10.3173 9.39314 10.7959 9.45742C11.6741 9.56364 12.5625 9.37208 13.3189 8.91342C14.1577 8.4311 14.8825 7.77347 15.4439 6.98542C15.5614 6.81109 15.7428 6.69003 15.9489 6.64842C16.0855 6.62787 16.2185 6.58815 16.3439 6.53042C16.7144 6.36258 17.1462 6.40408 17.4779 6.63942C21.0912 8.5233 24.4932 10.7875 27.6259 13.3934C27.794 13.5313 27.9524 13.6807 28.0999 13.8404C28.5257 14.3049 28.5257 15.0179 28.0999 15.4824C27.9398 15.6645 27.7139 15.7753 27.4719 15.7904C27.2409 15.8054 27.0079 15.8244 26.7769 15.8184C26.6779 15.8078 26.5829 15.7735 26.4999 15.7184C24.9489 14.7984 23.3999 13.8724 21.8489 12.9534C21.6046 12.812 21.3525 12.6845 21.0939 12.5714C20.9429 12.5034 20.7709 12.4424 20.6629 12.6544C20.5549 12.8664 20.6569 12.9804 20.8249 13.0834C21.9656 13.7874 23.1056 14.4918 24.2449 15.1964C24.7569 15.5114 25.2779 15.8114 25.7869 16.1324C25.8742 16.197 25.9327 16.2932 25.9499 16.4004C26.0735 16.8873 25.8808 17.3997 25.4669 17.6844C25.0501 17.9852 24.5028 18.0358 24.0379 17.8164C23.3499 17.4794 22.6889 17.0834 22.0199 16.7084C21.2369 16.2694 20.4579 15.8224 19.6709 15.3884C19.5745 15.334 19.4603 15.3207 19.3539 15.3514C19.0399 15.4924 19.0269 15.7514 19.3309 15.9394C20.3169 16.5344 21.3119 17.1144 22.3009 17.7034C22.6679 17.9224 23.0359 18.1404 23.3889 18.3794C23.6719 18.5714 23.6829 18.6884 23.6009 19.0104C23.5202 19.3796 23.2789 19.6935 22.943 19.8666C22.6071 20.0397 22.2114 20.0539 21.8639 19.9054C21.3883 19.7536 20.9256 19.5644 20.4799 19.3394C19.7689 19.0144 19.0669 18.6684 18.3599 18.3334C18.2599 18.2874 18.1599 18.2504 18.0599 18.2144C17.9825 18.1788 17.8936 18.1777 17.8154 18.2114C17.7372 18.2452 17.677 18.3106 17.6499 18.3914C17.6066 18.4651 17.5991 18.5544 17.6294 18.6343C17.6598 18.7142 17.7247 18.776 17.8059 18.8024C18.6709 19.2414 19.5399 19.6734 20.4059 20.1114C20.6499 20.2344 20.8909 20.3664 21.1289 20.4924C21.1209 20.5374 21.1289 20.5624 21.1139 20.5774C20.7674 21.1427 20.1789 21.5159 19.5199 21.5884C18.372 21.613 17.2316 21.3973 16.1719 20.9554C16.1393 20.9374 16.1085 20.9163 16.0799 20.8924C16.1278 20.7807 16.184 20.6728 16.2479 20.5694C16.6127 19.9913 16.6593 19.2676 16.3715 18.6476C16.0837 18.0276 15.5009 17.5959 14.8239 17.5014L14.4549 17.4394C14.542 16.8513 14.3648 16.2547 13.9709 15.8094C13.5899 15.3311 13.0083 15.0569 12.3969 15.0674C12.3969 14.8834 12.4039 14.7054 12.3969 14.5284C12.3685 13.9521 12.0915 13.4164 11.6376 13.0601C11.1837 12.7038 10.5975 12.562 10.0309 12.6714C9.66544 12.7486 9.34095 12.9571 9.11892 13.2574C8.98792 13.4214 8.86392 13.5904 8.71092 13.7924C8.64592 13.6674 8.59192 13.5634 8.53692 13.4604C8.16999 12.6897 7.34666 12.2449 6.50092 12.3604C6.24418 12.4288 5.99719 12.5296 5.76592 12.6604C5.6428 12.7331 5.52606 12.8161 5.41692 12.9084C5.36192 12.8494 5.31092 12.8004 5.26592 12.7454C4.88392 12.2804 4.50592 11.8134 4.12092 11.3514C4.04482 11.2843 4.03106 11.1708 4.08892 11.0874C5.52825 8.54809 6.96558 6.00742 8.40092 3.46542C8.40792 3.42042 8.43492 3.38742 8.49392 3.30542Z" fill="" />
                            <path d="M18.7466 2.69549C20.1114 2.689 21.4714 2.85907 22.7926 3.20149C23.2716 3.31249 23.7566 3.40149 24.2426 3.47649C24.8123 3.54858 25.3907 3.50401 25.9426 3.34549C26.2916 3.25749 26.2956 3.27149 26.4676 3.57449C27.8343 5.98849 29.2009 8.40216 30.5676 10.8155C30.6117 10.8658 30.6327 10.9323 30.6255 10.9988C30.6183 11.0654 30.5835 11.1258 30.5296 11.1655C29.9843 11.7573 29.3772 12.289 28.7186 12.7515C28.6396 12.8025 28.5506 12.8385 28.4756 12.8955C28.3406 12.9955 28.2416 12.9635 28.1166 12.8615C26.2375 11.3238 24.277 9.88837 22.2436 8.56149C20.6727 7.54819 19.045 6.62568 17.3686 5.79849C17.1097 5.65027 16.7983 5.62462 16.5186 5.72849C16.1236 5.87149 15.7186 5.97249 15.3186 6.11449C15.1866 6.163 15.0724 6.25011 14.9906 6.36449C14.4661 7.15165 13.7717 7.81117 12.9586 8.29449C12.4404 8.60637 11.8502 8.77863 11.2456 8.79449C10.7916 8.80949 10.6676 8.69449 10.7036 8.25849C10.7101 7.99246 10.7811 7.73198 10.9106 7.49949C11.7699 6.21882 12.6486 4.95216 13.5466 3.69949C13.6941 3.52187 13.8879 3.3886 14.1066 3.31449C14.8617 3.05637 15.6481 2.90098 16.4446 2.85249C17.2126 2.77449 17.9806 2.74549 18.7466 2.69549Z" fill="" />
                            <path d="M8.46885 1.7065C8.42485 1.7965 8.39585 1.8625 8.36085 1.9255C6.87152 4.59217 5.38085 7.25883 3.88885 9.9255C3.56585 10.4995 3.20985 11.0545 2.87585 11.6255C2.85146 11.6986 2.79208 11.7547 2.71767 11.7748C2.64327 11.7949 2.56374 11.7764 2.50585 11.7255C1.74385 11.2655 0.975849 10.8165 0.218849 10.3475C-0.0551514 10.1775 -0.0491514 10.1475 0.111849 9.8615C1.84585 6.7615 3.57285 3.6515 5.32385 0.561502C5.74985 -0.190498 5.63585 -0.0734982 6.29485 0.307502C6.97185 0.698502 7.62185 1.1345 8.28085 1.5545C8.34712 1.60054 8.40995 1.65134 8.46885 1.7065Z" fill="" />
                            <path d="M32.05 11.8195C31.9951 11.7619 31.9449 11.7001 31.9 11.6345C31.493 10.9415 31.08 10.2515 30.687 9.55049C29.271 7.02349 27.864 4.49049 26.446 1.96449C26.333 1.76449 26.352 1.63249 26.546 1.51649C27.324 1.04049 28.103 0.566491 28.879 0.0854912C29.041 -0.0145088 29.111 0.0704912 29.179 0.195491C29.846 1.33249 30.534 2.45749 31.179 3.60649C32.353 5.69649 33.504 7.79949 34.661 9.89849C34.813 10.1745 34.813 10.1985 34.549 10.3595C33.803 10.8185 33.049 11.2595 32.296 11.7105C32.216 11.7514 32.1339 11.7877 32.05 11.8195Z" fill="" />
                            <path d="M8.5388 18.6716C7.89859 18.6641 7.35003 18.2117 7.2208 17.5846C7.15873 17.3341 7.20913 17.0689 7.3588 16.8586C8.1258 15.8226 8.8818 14.7776 9.6588 13.7526C9.80886 13.51 10.062 13.3496 10.3455 13.3176C10.6289 13.2856 10.9114 13.3855 11.1118 13.5886C11.3589 13.771 11.5392 14.0297 11.6248 14.3246C11.7344 14.6154 11.6928 14.9416 11.5138 15.1956C10.7558 16.2206 10.0038 17.2496 9.2398 18.2706C9.09478 18.5202 8.82741 18.6731 8.5388 18.6716Z" fill="" />
                            <path d="M13.7606 17.1465C13.7183 17.3118 13.6558 17.4713 13.5746 17.6215C12.9976 18.4355 12.4206 19.2505 11.8116 20.0405C11.6508 20.2646 11.4029 20.4107 11.1289 20.4429C10.8549 20.475 10.58 20.3903 10.3716 20.2095C10.0794 20.0322 9.87658 19.7387 9.81414 19.4027C9.75171 19.0666 9.83554 18.7199 10.0446 18.4495C10.6196 17.6495 11.2336 16.8825 11.8196 16.0935C12.0288 15.7996 12.4131 15.6889 12.7466 15.8265C13.3379 15.996 13.7493 16.5315 13.7606 17.1465Z" fill="" />
                            <path d="M15.8997 19.5236C15.867 19.6692 15.8149 19.8098 15.7447 19.9416C15.3097 20.5416 14.8737 21.1486 14.4087 21.7286C14.1407 22.027 13.6985 22.0933 13.3547 21.8866C12.9967 21.717 12.7213 21.4113 12.5897 21.0376C12.4724 20.7733 12.5016 20.4669 12.6667 20.2296C13.0881 19.6429 13.5144 19.0596 13.9457 18.4796C14.1041 18.2737 14.3586 18.1656 14.6167 18.1946C15.2988 18.2905 15.8279 18.8386 15.8997 19.5236Z" fill="" />
                            <path d="M8.04174 14.3794C8.01247 14.5404 7.96027 14.6963 7.88674 14.8424C7.60874 15.2424 7.31274 15.6234 7.00274 15.9964C6.79158 16.2628 6.42832 16.3549 6.11574 16.2214C5.64166 16.0744 5.27596 15.6947 5.14674 15.2154C5.0633 14.9778 5.09665 14.7147 5.23674 14.5054C5.50674 14.1208 5.78207 13.7398 6.06274 13.3624C6.29309 13.0516 6.70558 12.9378 7.06274 13.0864C7.63238 13.2632 8.02611 13.7832 8.04174 14.3794Z" fill="" />
                        </svg>
                    </span> My Offers
                </a>
            </li>
            @endif
            <li class="left-sidebar-nav-item {{ (Request::segment(1) == 'chat') ? 'active' : '' }}">
                <a class="left-sidebar-nav-link" href="{{ route('rental.chat')}}">
                    <span class="">
                    <svg width="28" height="25" viewBox="0 0 28 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.38672 24.3056C6.07943 24.179 6.76758 23.9574 7.45117 23.6409C8.13477 23.3245 8.76823 22.9809 9.35156 22.6101C9.93489 22.2394 10.4408 21.8777 10.8691 21.5251C11.2975 21.1724 11.5846 20.9057 11.7305 20.7248C12.0404 20.7791 12.3092 20.8107 12.5371 20.8198C12.765 20.8288 13.0065 20.8333 13.2617 20.8333H14C15.9323 20.8333 17.7507 20.5621 19.4551 20.0195C21.1595 19.477 22.6452 18.731 23.9121 17.7816C25.179 16.8321 26.1771 15.729 26.9062 14.4721C27.6354 13.2152 28 11.8634 28 10.4167C28 8.96991 27.6354 7.61809 26.9062 6.36122C26.1771 5.10435 25.179 4.00119 23.9121 3.05176C22.6452 2.10232 21.1595 1.35634 19.4551 0.813802C17.7507 0.271267 15.9323 0 14 0C12.0677 0 10.2493 0.271267 8.54492 0.813802C6.84049 1.35634 5.35482 2.10232 4.08789 3.05176C2.82096 4.00119 1.82292 5.10435 1.09375 6.36122C0.364584 7.61809 0 8.96991 0 10.4167C0 11.3209 0.132162 12.1844 0.396484 13.0073C0.660807 13.8301 1.03906 14.6077 1.53125 15.3402C2.02344 16.0726 2.63411 16.7462 3.36328 17.3611C4.09245 17.976 4.92187 18.5185 5.85156 18.9887C5.85156 19.0068 5.847 19.0204 5.83789 19.0294C5.82878 19.0384 5.82422 19.052 5.82422 19.0701C5.82422 19.8477 5.6556 20.6208 5.31836 21.3894C4.98112 22.158 4.6849 22.7684 4.42969 23.2205C4.41146 23.2567 4.39779 23.3019 4.38867 23.3561C4.37956 23.4104 4.375 23.4646 4.375 23.5189C4.375 23.7359 4.45247 23.9213 4.60742 24.075C4.76237 24.2287 4.94922 24.3056 5.16797 24.3056H5.38672Z" fill="#939FA4"/>
                    </svg>

                    </span> Chat
                </a>
            </li>
            <li class="left-sidebar-nav-item">
                <a class="left-sidebar-nav-link" href="#">
                    <span class="">
                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="22" height="21">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.354 20.849C17.735 20.849 17.893 20.534 17.893 20.192C17.8952 20.1041 17.8909 20.0162 17.88 19.929L16.754 13.361L21.523 8.71C21.7128 8.55061 21.8351 8.32508 21.865 8.079C21.865 7.685 21.445 7.527 21.129 7.479L14.534 6.52L11.574 0.539C11.456 0.289 11.232 0 10.93 0C10.628 0 10.405 0.289 10.286 0.539L7.331 6.516L0.736 7.475C0.408 7.528 0 7.685 0 8.075C0.0302679 8.31782 0.146652 8.54172 0.328 8.706L5.111 13.361L3.981 19.929C3.96583 20.0159 3.95714 20.1038 3.955 20.192C3.955 20.534 4.126 20.849 4.507 20.849C4.69247 20.8411 4.87298 20.7868 5.032 20.691L10.932 17.591L16.832 20.691C16.9894 20.788 17.1693 20.8424 17.354 20.849Z" fill="white"/>
                    </mask>
                    <g mask="url(#mask0)">
                    <path d="M-6.91211 -1.78711H29.6619V33.7749H-6.91211V-1.78711Z" fill="#939FA4"/>
                    </g>
                    </svg>
                    </span>Preferred Cities
                </a>
            </li>
            <li class="left-sidebar-nav-item {{ (request()->is('setting')) ? 'active' : '' }}">
                <a class="left-sidebar-nav-link" href="{{ url('setting')}}">
                    <span class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="28.75" viewBox="0 0 30 28.75">
                            <g id="login_svg" data-name="login_svg" transform="translate(0)">
                                <path id="Path_304" data-name="Path 304" d="M329.063,261h-.313v-1.875a3.125,3.125,0,1,0-6.25,0V261h-.313A2.19,2.19,0,0,0,320,263.187v4.375a2.19,2.19,0,0,0,2.187,2.188h6.875a2.19,2.19,0,0,0,2.187-2.188v-4.375A2.19,2.19,0,0,0,329.063,261Zm-4.687-1.875a1.25,1.25,0,1,1,2.5,0V261h-2.5Zm0,0" transform="translate(-301.25 -241)" fill="" />
                                <path id="Path_305" data-name="Path 305" d="M16.875,263.187a4.069,4.069,0,0,1,2.5-3.75v-.313a4.941,4.941,0,0,1,.489-2.119,5.908,5.908,0,0,0-3.3-1.006H5.937A5.944,5.944,0,0,0,0,261.937v4.375a.938.938,0,0,0,.937.937H16.875Zm0,0" transform="translate(0 -241)" fill="" />
                                <path id="Path_306" data-name="Path 306" d="M97.832,6.25A6.25,6.25,0,1,1,91.582,0,6.25,6.25,0,0,1,97.832,6.25Zm0,0" transform="translate(-80.332)" fill="" />
                            </g>
                        </svg>
                    </span> My account
                </a>
            </li>
            <li class="left-sidebar-nav-item {{ (request()->is('notifications')) ? 'active' : '' }}">
                <a class="left-sidebar-nav-link" href="{{ url('notifications')}}">
                    <span class="">
                        <svg id="alarm-settings_svg" data-name="alarm-settings_svg" xmlns="http://www.w3.org/2000/svg" width="26.209" height="30.088" viewBox="0 0 26.209 30.088">
                            <path id="Shape" d="M33.287,24.648l.3,1.8H35.9l.3-1.8a5.7,5.7,0,0,1-2.912,0Z" transform="translate(-15.63 -11.406)" fill="" />
                            <path id="Shape-2" data-name="Shape" d="M22.793,18.139l1.161,2L25.66,19.5a5.9,5.9,0,0,1-1.454-2.517Z" transform="translate(-10.774 -7.859)" fill="" />
                            <path id="Shape-3" data-name="Shape" d="M31.835,14.67A4.835,4.835,0,1,0,27,9.835,4.835,4.835,0,0,0,31.835,14.67Zm0-7.521a2.686,2.686,0,1,1-2.686,2.686A2.686,2.686,0,0,1,31.835,7.149Z" transform="translate(-12.721 -2.314)" fill="" />
                            <circle id="Oval" cx="1.612" cy="1.612" r="1.612" transform="translate(17.502 5.909)" fill="" />
                            <path id="Shape-4" data-name="Shape" d="M46.738,7.143l-1.161-2-1.705.639A5.9,5.9,0,0,1,45.325,8.3Z" transform="translate(-20.528 -2.38)" fill="" />
                            <path id="Shape-5" data-name="Shape" d="M23.544,5.841a.472.472,0,0,1-.034.06L22.8,7.137l1.411,1.157a5.9,5.9,0,0,1,1.455-2.516l-1.709-.642Z" transform="translate(-10.775 -2.377)" fill="" />
                            <path id="Shape-6" data-name="Shape" d="M36.2,1.8,35.9,0h-2.31l-.3,1.8a5.7,5.7,0,0,1,2.912,0Z" transform="translate(-15.634 0)" fill="" />
                            <path id="Shape-7" data-name="Shape" d="M19.223,2.686c.128,0,.254.009.381.015l.277-.478a1.071,1.071,0,0,1,1.3-.469l1.057.395A3.184,3.184,0,0,0,19.223,0,3.227,3.227,0,0,0,16,3.223V3.25A9.605,9.605,0,0,1,19.223,2.686Z" transform="translate(-7.63 0)" fill="" />
                            <path id="Shape-8" data-name="Shape" d="M43.87,19.5l1.709.645,1.157-2-1.411-1.157A5.9,5.9,0,0,1,43.87,19.5Z" transform="translate(-20.528 -7.862)" fill="" />
                            <path id="Shape-9" data-name="Shape" d="M15.071,50a3.761,3.761,0,0,0,7.445,0Z" transform="translate(-7.201 -23.139)" fill="" />
                            <path id="Shape-10" data-name="Shape" d="M20.609,21.516V19.367H18.38a1.074,1.074,0,0,1-1.062-.9l-.4-2.4a5.977,5.977,0,0,1-.663-.376l-2.279.856a1.046,1.046,0,0,1-.372.068,1.074,1.074,0,0,1-.935-.545l-1.156-1.991a1.088,1.088,0,0,1,.243-1.369l1.89-1.547c-.008-.13-.019-.257-.019-.386s.011-.256.019-.384L11.76,8.845a1.087,1.087,0,0,1-.252-1.366l.265-.456a8.6,8.6,0,0,0-8.354,8.583v5.909A10.569,10.569,0,0,1,.571,28.124a.553.553,0,0,0-.1.6.51.51,0,0,0,.484.311H23.075a.51.51,0,0,0,.484-.311.554.554,0,0,0-.1-.607,10.566,10.566,0,0,1-2.847-6.606ZM5.03,27.962H2.881a.537.537,0,0,1,0-1.074H5.03a.537.537,0,0,1,0,1.074Zm0-11.819a.537.537,0,0,1-.537-.537,7.489,7.489,0,0,1,2.824-5.87.537.537,0,1,1,.672.838,6.422,6.422,0,0,0-2.422,5.032.537.537,0,0,1-.537.537ZM21.146,27.962H7.179a.537.537,0,0,1,0-1.074H21.146a.537.537,0,1,1,0,1.074Z" transform="translate(-0.421 -3.25)" fill="" />
                        </svg>
                    </span> Settings
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- new sidebar ends here -->
<!-- Modal -->
<div class="modal fade uploadprofile_modal" id="UploadProfileModal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header bg-white">
                    <button type="button" class="close cancelOption" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <h1> Upload profile picture</h1>
                    <div class="login-container align-items-center w-100 vertical-middle text-center">
                        <form method="POST" enctype="multipart/form-data" name="profile_pic_form" id="profile_pic_form" action="javascript:void(0)" >
                        @csrf
                        <div class="uploadprofile-wrapper">
                                <div class="userprofile-img">
                                @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo !='')
                                    <img class="lazy-load" id="image_preview_container" data-src="{{URL::asset('uploads/users/'.\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->photo)}}" alt="photo" class="">
                                @else
                                <img class="lazy-load" id="image_preview_container" data-src="{{URL::asset('media/images/user-icon-zero-state.png')}}" alt="zero state">
                                @endif
                                </div>
                                <div class="uploadprofile-action">
                                     <div class="profilebtn-wrap">
                                        <input type="file" name="profilePic" id="profilePic" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple="">
                                        <label for="profilePic"><span>Browse pictures</span></label>
                                        <span id="ProfileErr"></span>
                                    </div>

                                    <button type="submit" id="save_profilepic" class="button-default-animate button-default-custom btn-sunset-gradient mt-5">Save</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-800 btn-link mt-5 cancelOption">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@php
    if(!empty($subscribeDate)){
        unset($subscribeDate);
        unset($subscribeMonth);
        unset($subscribeDate);
        unset($subscribeDateArr);
        unset($subscribeDateMonth);
        unset($subscribeDateYear);
    }
@endphp

 <!-- End Modal -->
 @section('footerJs')
    @parent
<script>

    $.validator.addMethod("dimention", function (value, element) {
        let isInvalid = true;
        isInvalid = Math.round($('#profilePic')[0].files[0].size / (1024 * 1024)) <= 2;

        return isInvalid;
    }, 'This file is too large. Please try again with an image less than 2 MB.');

    $('#profile_pic_form').validate({
        rules: {
            profilePic: {
                required: true,
                dimention: true,
                accept:"image/jpeg,image/jpg,image/png*",
            }
        },
        messages: {
            profilePic: {
                required: "This file is too large. Please try again with an image less than 2 MB.",
                accept: 'Only jpg,png,jpeg,gif,svg format are allowed'
            }
        },
        errorPlacement: function(error, element) {
            var PhotoError = $('#ProfileErr');
            if (element.attr('name')=='profilePic') {
                $(PhotoError).append(error)
            } else  {
                error.insertAfter(element);
            }
        }
    });

    $('#profilePic').change(function(){
        let reader = new FileReader();
        reader.onload = (e) => {
        $('#image_preview_container').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#profile_pic_form').on('submit', function(e){
        e.preventDefault();
        var myForm = $("#profile_pic_form");
        if (!myForm.valid()) return false;
        $.ajax({
            type: "POST",
            url: "{{ route('save-profilepic') }}",
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
            },
            data: new FormData(this),
            cache:false,
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            success: function(data) {
                if(data.success == true)
                {
                    $("#UploadProfileModal").modal('hide');
                    Swal.fire({
                            icon: "success",
                            title: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        }).then(() => {
                            window.location.reload();
                        });
                } else {

                    Swal.fire({
                            icon: "error",
                            title: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                }
            },
            error: function(xhr, status, error) {

                Swal.fire({
                            icon: "error",
                            title: error,
                            showConfirmButton: false,
                            timer: 3000
                        });
            }
        });
    });

    $('.cancelOption').click(function() {
            $('#ProfileErr').text('');
    });

    $('.togl-btn-custom').click(function() {
        $(this).toggleClass("pushed");
    });

    // Upload profile image
    'use strict';
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
           // Upload profile image
</script>
<!-- End Modal -->
@endsection
