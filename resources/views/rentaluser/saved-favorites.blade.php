@extends('layouts.rentalApp')

@section('content')

<section class="tripboard-view">
        <div class="container-fluid p-0">
            <form method="get" name="tripBoardsearchForm" autocomplete="off" id="tripBoardsearchForm">
                <div class="row m-0">
                    <div class="col-xl-3 tripboard-edit min-width-auto">
                        <div class="w-100">
                            <div class="tripboard-left-custom-padding">
                                <p>
                                    <a class="previous-icon mr-5" href="{{ route('tripboards') }}">
                                        <img class="lazy-load" data-src="{{URL::asset('media/images/left-arrow-cir.png')}}" alt="left arrow">
                                    </a><img class="lazy-load single-tripboard" data-src="{{URL::asset('media/images/single-tripboard.svg')}}" alt="tripboard" >
                                </p>
                                <div class="input-group mt-12">
                                    <input type="text" class="input-boards-title" placeholder="Saved Favorites {{((count($similarListing) == 0)?'(0)':'')}} " disabled>
                                </div>
                                <div class="border-separator-s mb-15" style="margin-top: -15px;"></div>
                            </div>
                            @if(count($similarListing) > 0)
                                <div class="mb-2 row h-saved">
                                    <div class="col-6 pr-1">
                                        <label class="col-form-label">Suggested Stays</label>
                                    </div>
                                    <div class="col-6 pl-1 text-right float-right">

                                    </div>

                                </div>

                                <div class="tripboard-edit-right-section padding-0 h-saved">
                                    @foreach($similarListing as $key => $similarListings)
                                        <div class="d-flex flax-wrap mb-5">

                                            <div class="flex-shrink-0 mr-7">
                                                <div class="symbol symbol-140 symbol-lg-140">
                                                    <a href="{{ url('propertydetail/'.$similarListings->id)}}">
                                                        <img class="lazy-load" alt="Pic" data-src="{{URL::asset('uploads/property/'.$similarListings->cover_photo)}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">

                                                <div class="d-flex align-items-center justify-content-between flex-wrap">

                                                    <div class="">
                                                        <a href="{{ url('propertydetail/'.$similarListings->id)}}">
                                                            <p class="name-place">{{$similarListings->title}}
                                                            </p>
                                                        </a>

                                                        <p class="price-of-place">${{number_format($similarListings->price)}}/stay</p>
                                                        <ul class="recent-view-ul">
                                                        <li>{{$similarListings->total_beds}} {{$similarListings->total_beds > 1 ?'beds':'bed'}} </li>
                                                            <li>{{$similarListings->total_bathroom}} {{$similarListings->total_bathroom > 1 ?'baths':'bath'}}</li>
                                                            <li>{{$similarListings->no_of_guest}} {{$similarListings->no_of_guest > 1 ?'guests':'guest'}}</li>
                                                            <li>{{$similarListings->property_type}}</li>
                                                        </ul>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="col-xl-9 tripboard-edit-right-section pb-0">
                            <input type="hidden" name="filter_dates" id="filter_dates" value="">
                            <input type="hidden" name="number_of_guest" id="number_of_guest" value="1">
                            <input type="hidden" name="filter_dates_picker" id="filter_dates_picker" value="">
                            <input type="hidden" class="small" name="where_is_listing_clone" id="where_is_listing_clone">
                            <input type="hidden" class="small" name="city" id="city">
                            <input type="hidden" class="small" name="state" id="state">
                            <input type="hidden" class="small" name="country" id="country">
                            <input type="hidden" class="small" name="zipcode" id="zipcode">
                            <div class="d-block w-100 mb-7">
                                <div class="row m-0">
                                    <div class="col-lg-7 pl-0 pr-0 where-to-go">
                                        <div class="row m-0">
                                            <div class="col-lg-8 col-md-9 p-5 pt-9 pr-2">
                                                <div class="input-group pl-3">
                                                    <img class="lazy-load" data-src="https://got2go.kitelytechdev.com/media/images/location.svg" alt="location">
                                                    <input type="text" class="form-control pac-target-input" autocomplete="off" name="location_detail" id="location_detail" placeholder="Got2Go to">
                                                </div>
                                                <div>
                                                    <label class="error" id="location_error" style="display: none;">Please enter valid address.</label>
                                                </div>
                                                <div class="map iframe-map" style="display: none;">
                                                    <div id="map" style="height:300px;"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-3 text-right pl-0 pt-5 pr-5 pb-5">
                                                <a href="" class="pr-4 pt-3 refine_filter_icon" data-toggle="modal" data-target="#filter_popup"><img src="https://got2go.kitelytechdev.com/media/images/filter.svg" alt="fillter"></a>
                                                <button type="submit" id="tripboardSubmit"><img class="lazy-load" data-src="https://got2go.kitelytechdev.com/media/images/gobutton.svg" alt="go button" style="height: 73px;"></button>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-7 pl-0 pr-0 checkboxes-inline">
                                        <div class="row m-0 w-100">
                                            <div class="col-4 pl-0 pr-2">
                                                <label class="custom-checkbox common-fatfrank">Vacation club
                                                    <input type="checkbox" checked="checked" value="vacation_rental" name="property_type[]">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="col-3 pl-0 pr-4">
                                                <label class="custom-checkbox common-fatfrank float-right">hotels
                                                    <input type="checkbox" checked="checked" value="hotels" name="property_type[]">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="col-5 pl-0">
                                                <label class="custom-checkbox common-fatfrank unchecked float-right">private residences
                                                    <input type="checkbox" value="property" checked="checked" name="property_type[]">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div>
                                                <label class="error" id="property_error" style="display: none;">Please select atleast one property type.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row potential-listing-section overflow-hidden">
                            <div class="col-md-8">
                                <div class="potential-listing-title common-open"><strong>SAVED</strong> FAVORITES</div>
                            </div>
                            @if(count($potentialListing) > 0)
                                <div class="col-md-4 text-right">
                                    <div class="total-stays"><span>{{count($potentialListing)}}</span> {{(count($potentialListing) > 1)?"Stays":"Stay"}}</div>
                                </div>
                                <div class="tripbord-save-property">
                                    <div class="row">
                                    @foreach($potentialListing as $key => $potentialList)
                                        <div class="col-md-4 tripboard-saved-property-outer" id="savedProperty_{{$potentialList->id}}">
                                            <div class="tripboard-saved-prty-box">
                                                <div class="image">
                                                    <a href="{{ url('propertydetail/'.$potentialList->property->id)}}">
                                                        <img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$potentialList->property->cover_photo)}}" alt="{{$potentialList->title}}">
                                                    </a>
                                                    <img data-src="{{URL::asset('media/images/favorite-heart-icon.svg')}}" class="lazy-load favourite" alt="heart icon" data-id="{{$potentialList->id}}">
                                                </div>
                                                <div class="description">
                                                    <h3>{{$potentialList->property->title}}</h3>
                                                    <h6>{{$potentialList->property->city}}, {{$potentialList->property->state}}</h6>
                                                    <p class="price-per-night"><strong>{{ Helper::formatMoney($potentialList->property->price,1) }}</strong>/night</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            @else
                            <div class="col-12 pl-0 pr-0 tripper-dboard-form">
                                <div class="d-block text-center">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                    <div class="zero-state-text common-open">You have no listings favorited yet. Start browsing by city using the search bar above!</div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @include('rentaluser.advanceSearch')
            </form>
        </div>
        </div>
    </section>
    @endsection
    @section('footerJs')
    @parent
    <script>
        var tripboardSearch = "{{route('rental.search')}}";
        var unlikeUrl = "{{route('rental.api.saved.unlike') }}";
        $(document).ready(function() {
            $('#tripBoardsearchForm').validate({
            ignore:":not(:visible)",
               rules: {
                   location_detail: {
                       required:true

                   }
               },
               messages: {
                   location_detail: {
                       required: "Please type in a address."
                   },
               }
           });
           $('#number_of_guest').val('1');
            $('#touchspin_1').val('1');
            $('#modal_adultchild').attr("value", $('#touchspin_1').val() + ' Adult(s) - ' + $('#touchspin_2').val() + ' Children')
            let date = $('#from_date').val() + ' - ' + $('#to_date').val();

            var fromDate =moment();
            var toDate = moment().add(2,'days');
            $('#filter_dates_picker').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
            $('#filter_dates').val(fromDate.format('MM/DD/YYYY') + ' - ' + toDate.format('MM/DD/YYYY'));
            $('#modal_filter_dates').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
            //$('#modal_filter_dates').data('daterangepicker').setStartDate(fromDate);
            //$('#modal_filter_dates').data('daterangepicker').setEndDate(toDate);
            $('#adults').val('1');
           // search
            $('#tripboardSubmit').on('click', function(e) {
                e.preventDefault();

                let $form = $("#tripBoardsearchForm");
                var location_details = $('#location_detail').val();

                if (location_details.indexOf(',') <= 2) {
                    $( '#location_error' ).css("display","block");
                    return false;
                }
                if($('input[name="property_type[]"]:checked').length < 1){
                    $( '#property_error' ).css("display","block");
                    return false;
                }

                if(!$form.valid()) return false;
                showLoader();

                let action = tripboardSearch + '?' + $('#tripBoardsearchForm').serialize();
                $('#tripBoardsearchForm').attr('action', action);
                $('#tripBoardsearchForm').submit();
            });
            $('.btn-fly-go').on('click', function(e) {
                e.preventDefault();

                $('#tripboardSubmit').trigger('click');
            });

            $('#location_detail').on('change', function() {
                $('#modal_location_detail').val($(this).val())
            });

            $('#modal_location_detail').on('change', function() {
                $('#location_detail').val($(this).val())
            });
            $('.refine_filter_icon').click(function(e) {
                e.preventDefault();

                $('#number_of_guest').val('1');
                $('#touchspin_1').val('1');
                $('#modal_adultchild').attr("value", $('#touchspin_1').val() + ' Adult(s) - ' + $('#touchspin_2').val() + ' Children')
                let date = $('#from_date').val() + ' - ' + $('#to_date').val();

                var fromDate =moment();
                var toDate = moment().add(2,'days');
                $('#filter_dates_picker').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
                $('#filter_dates').val(fromDate.format('MM/DD/YYYY') + ' - ' + toDate.format('MM/DD/YYYY'));
                $('#modal_filter_dates').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
                $('#modal_filter_dates').data('daterangepicker').setStartDate(fromDate);
                $('#modal_filter_dates').data('daterangepicker').setEndDate(toDate);
                $('#adults').val(1);
            });
            $('#filter_popup').on('hide.bs.modal', function() {

                $('body').removeClass("overflow-hidden");
                $('body').addClass("overflow-y");
                $('#filter_popup').removeClass("overflow-y");
                $('#location_detail').val($('#modal_location_detail').val());
                var fromDate =  moment($('#modal_filter_dates').data('daterangepicker').startDate).toDate();
                var endDate =  moment($('#modal_filter_dates').data('daterangepicker').endDate).toDate();
                $('#kt_datepicker_3').val(moment(fromDate).format('MM/DD/YYYY'));
                $('#kt_datepicker_4').val(moment(endDate).format('MM/DD/YYYY'));

                var guest =  parseInt(parseInt($('#touchspin_1').val()) + parseInt($('#touchspin_2').val()));
                $('#guests').val(guest);
                $('#guests').trigger('change');
                $('#adults').val($('#touchspin_1').val());
                $('#touchspin_2').val(0);
            });
            $('#filter_popup').on('show.bs.modal', function() {
                $('body').addClass("overflow-hidden");
                $('body').removeClass("overflow-y");
                $('#filter_popup').addClass("overflow-y");
                $('#modal_location_detail').val($('#location_detail').val())
                var fromDate =moment();
                var toDate = moment().add(2,'days');
                $('#filter_dates_picker').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
                $('#filter_dates').val(fromDate.format('MM/DD/YYYY') + ' - ' + toDate.format('MM/DD/YYYY'));

                $('#modal_filter_dates').val($('#filter_dates_picker').val())
                $('#touchspin_1').val('1');
                $('#touchspin_2').val('0');
                $('#modal_adultchild').attr("value", $('#touchspin_1').val() + ' Adult(s) - ' + $('#touchspin_2').val() + ' Children')


                $('#number_of_guest').val($('#guests').val());

                $('#modal_filter_dates').data('daterangepicker').setEndDate(toDate);
                $('#adults').val($('#touchspin_1').val());
                $('#touchspin_2').val(0);
            });
            $('.btn-fly-go').click(function() {
                $('#location_detail').val($('#modal_location_detail').val());
                $('#tripboardSubmit').trigger('click')
            });

            $('.favourite').click(function(e) {
                var id = $(this).attr('data-id');
                var totalstays = $('.total-stays span').text();

                $.ajax({

                    type: "POST",
                    url: unlikeUrl,
                    dataType: "JSON",
                    headers: {
                        "Authorization": "Bearer {{session()->get('rental_token')}}",
                    },
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.success == true) {
                            $('#savedProperty_'+id).remove();
                             if(totalstays == 2){
                                 $('.total-stays').text('1 Stay');
                            }
                            $('.total-stays span').text(totalstays - 1);

                            if($('.tripbord-save-property .col-md-4').length == 0){
                                $('.input-boards-title').attr("placeholder", "Saved Favorites   (0)");
                                $('.h-saved').hide();
                                $html = '<div class="col-12 pl-0 pr-0 tripper-dboard-form"><div class="d-block text-center"><i class="fa fa-suitcase" aria-hidden="true"></i><div class="zero-state-text common-open">You have no listings favorited yet. Search for listings above to save them to your Favorites!</div></div></div>';
                                $('.tripbord-save-property').replaceWith($html);
                                $('.total-stays').css('display','none');
                            }
                        }else{
                            Swal.fire({
                                icon: "error",
                                title: data.data,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }

                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                            icon: "error",
                            title: data.data,
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }


                });

            });
         });




        $(".potential-listing-slider.owl-carousel").owlCarousel({
            dots: false,
            nav: true,
            lazyLoad: true,
            loop: false,
            slideBy: 3,
            fade: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1700: {
                    items: 4
                },
                2100: {
                    items: 5
                }
            }
        });

        jQuery('#tripper-page-carousal').owlCarousel({
            loop: true,
            margin: 30,
            center: true,
            nav: true,
            dots: false,
            autoHeight: false,
            responsive: {
                0: {
                    items: 1
                },
                640: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
        jQuery(".tripper-banner .banner-comtent a").click(function() {
            jQuery('html,body').animate({
                    scrollTop: jQuery("#tripper-reg").offset().top
                },
                1000);
        });
    </script>
    @endsection
