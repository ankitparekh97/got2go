@extends('layouts.rentalApp')
@section('content')
<div class="my-account-dashboard">
    @include('rentaluser.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column pt-20">
        <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Main Content -->
        <div id="content">
            <div class="container-fluid my-acc-dashbrd my-accountsetting-mob">
                <!-- Page Heading -->
                <h2 class="dashboard-title">MY <strong>Account</strong></h2>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="alert alert-success" id="showmsg" style="display:none"></div>
                        <div class="alert alert-danger" id="showcancelerrormsg" style="display:none"></div>
                        <h3><strong>My Personal Info</strong></h3>
                        <form class="dashboard-forms" id="myAccountSetting" name="myAccountSetting" autocomplete="off" action="{{ route('setting-save') }}">
                            @csrf
                            @if(\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()!='')
                            <input type="hidden" id="id" name="id" value="{{\Illuminate\Support\Facades\Auth::guard('rentaluser')->user()->id}}">
                            @endif
                            <div class="row m-0">
                                <div class="col-lg-6 pl-0">
                                    <div class="form-group-custm no-icon payment-icon">
                                        <input class="form-class-cstm" type="text" value="{{ $rentalUser->first_name ? $rentalUser->first_name : '' }}" id="firstname" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" id="lastname" value="{{ $rentalUser->last_name ? $rentalUser->last_name : '' }}" autocomplete="off" name="lastname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0 d-none d-sm-none d-xs-none d-md-block d-lg-block">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" passwordCheck="passwordCheck" type="password" id="updatepassword" name="updatepassword" placeholder="Update Password">
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0 "></div>
                                <div class="col-lg-6 pl-0  d-none d-sm-none d-xs-none d-md-block d-lg-block">

                                    <div class="input-group">
                                        <input class="form-class-cstm" type="password" id="retypepassword" name="retypepassword" placeholder="Retype Password">
                                        <div class="input-group-append error-after">
                                            <span class="input-group-text" style="border-radius: 0 30px 30px 0;">
                                                <i id="refreshpassword" class="la la-refresh"></i>
                                            </span>
                                        </div>
                                        <div class="passwordError"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0 "></div>


                                <div class="col-lg-6 pl-0 ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" value="{{ $rentalUser->phone ? $rentalUser->phone : '' }}" id="phonenumber" name="phonenumber" placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0 ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text"  value="{{ $rentalUser->email ? $rentalUser->email : '' }}" id="email" name="email" placeholder="Email Address" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-0 ">
                                    <div class="custom-border-hidden input-group input-group-solid date" id="kt_datepicker_2" data-target-input="nearest">
                                        <input type="text" name="dateofbirth" id="dateofbirth" value="{{ $rentalUser->birthdate ? date('m/d/Y', strtotime($rentalUser->birthdate)) : '' }}" class="form-class-cstm" placeholder="Birthday (mm/dd/yyyy)" data-target="#kt_datepicker_2" />
                                        <div class="input-group-append" data-target="#kt_datepicker_2" data-toggle="datetimepicker">
                                            <span class="input-group-text">
                                                <i class="ki ki-calendar"></i>
                                            </span>
                                        </div>
                                        <div class="dateError w-100 d-block"></div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="save_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5 btn-wd">Save Changes</button>
                           <!-- linked account starts here -->
                           <div class="link-social-account" id="link-account-screen">
                           <h3 class="pt-20 linked-account"><strong>LINKED ACCOUNTS</strong></h3>
                           <div class="row">
                                <div class="dashbrd-social-login">
                                    
                                    <span {{ $rentalUser->google_id ? 'class=""' : 'class="not-connected"' }}><a href="javascript:;"><img class="lazy-load" src="{{URL::asset('media/images/tripper-page/google.png')}}" alt="google">{{ $rentalUser->google_id ? 'Connected' : 'Connect Account' }}</a></span>
                                    
                                    
                                    <span {{ $rentalUser->facebook_id ? 'class=""' : 'class="not-connected"' }}><a href="javascript:;"><img class="lazy-load" src="{{URL::asset('media/images/tripper-page/favebook-login-btn.png')}}" alt="facebook">{{ $rentalUser->facebook_id ? 'Connected' : 'Connect Account' }}</a> </span>
                                    
                                    
                                </div>
                            </div>
                            </div>
                           <!-- linked account ends here -->
                           <!-- password starts here -->
                           <div class="">
                           <h3 class="pt-20 linked-account"><strong>PASSWORD</strong></h3>
                           <div class="row">
                               <div class="update-password">
                                    <p>Last updated 2 months ago.</p>
                                    <a href="#" class="update-password-button" data-toggle="modal" data-target="#myModal">Update Password</a>
                                </div>
                            </div>
                            <!-- The Modal -->
                            <div class="modal" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                <h4 class="modal-title"></h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body update-password-popup">
                                    <p class="update-password-popup-heading">Update Password</p>
                                    <div>
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" value="" id="" name="" placeholder="Current Password">
                                        </div>
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" value="" id="" name="" placeholder="New Password">
                                        </div>
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" value="" id="" name="" placeholder="Confirm New Password">
                                        </div>
                                    </div>
                                    <div class="password-button">
                                        <a href="#" class="save-password">Save Password</a>
                                        <a href="#" class="cancel-password">Cancel</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            </div>
                           <!-- password ends here -->
                            <h3 class="pt-20"><strong>My Billing</strong></h3>
                            @if(count($paymentDetails) > 0)
                                    @foreach($paymentDetails as $paymentDetail)
                            <div class="row_{{$paymentDetail->id}} mt-2">
                                <div class="col-xl-9">
                                    <div class="row bank-card-detail">
                                        <div class="col-10 pl-1">
                                            <div class="bank-card-type-img">
                                            @if( $paymentDetail->card_type == 'MasterCard' )
                                                <img src="{{URL::asset('media/images/mastercard-img.svg')}}" alt="matercard">
                                            @elseif($paymentDetail->card_type == 'Visa')
                                                <img src="{{URL::asset('media/images/visa.png')}}" alt="visa">
                                            @elseif($paymentDetail->card_type == 'American Express')
                                                <img src="{{URL::asset('media/images/cc-icons/american-express.png')}}" alt="american express">
                                            @elseif($paymentDetail->card_type == 'Discover')
                                                <img src="{{URL::asset('media/images/cc-icons/discover.png')}}" alt="discover">
                                            @elseif($paymentDetail->card_type == 'JCB')
                                                <img src="{{URL::asset('media/images/cc-icons/jcb.png')}}" alt="jcb">
                                            @elseif($paymentDetail->card_type == 'Diners Club')
                                                <img src="{{URL::asset('media/images/cc-icons/diners-club.png')}}" alt="diners club">
                                            @elseif($paymentDetail->card_type == 'Maestro')
                                                <img src="{{URL::asset('media/images/cc-icons/maestro.png')}}" alt="mastro">
                                            @elseif($paymentDetail->card_type == 'UnionPay')
                                                <img src="{{URL::asset('media/images/cc-icons/unionpay.png')}}" alt="unionpay">
                                            @else
                                                <img src="{{URL::asset('media/images/no-image.png')}}" alt="no image">
                                            @endif
                                            </div>
                                            <p><strong>{{$paymentDetail->card_type}}
                                            @php
                                            $last_four ='';
                                            if ($paymentDetail && $paymentDetail->last_four != "") {
                                                $last_four = substr($paymentDetail->last_four,-4);
                                            }
                                            @endphp
                                            <b class="pl-2">******{{@$last_four ? @$last_four: 5139}}
                                            </strong></strong></p>
                                            <p>Exp. {{@$paymentDetail->expiration_date ? @$paymentDetail->expiration_date:'08/2020'}}</p>
                                        </div>
                                        <div class="col-2 float-right pr-0">
                                            <a class="del-circle-icon" href="#2" data-toggle="modal" data-target="#deletepayment" onclick="deletePayment({{$paymentDetail->id}})">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="col-xl-9">
                                    <div class="row bank-card-detail">
                                        <div class="col-10 pl-1">No Payment detail found!</div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <a class="mt-10 button-default-animate btn-add-payment-method" href="javascript:;"  >Add payment method</a>
                        </form>

                        @php
                            /**
                            * @var \App\Models\RentalUserSubscription $subscription
                            */
                            $subscription = getLoggedInUserSubscription();
                        @endphp
                        @if(!empty($subscription))
                            <div class="membership-wrapper">
                                <h3>MY TRIPPER MEMBERSHIP</h3>
                                @if($subscription->status == 'pause')
                                    <p class="pb-2"> Payments paused, enjoy free access as a Tripper till {{ carbonParseDate($subscription->billing_period_end_date)->format('M d, Y')  }}.</p>
                                @else
                                    <div class="row">
                                        <div class="col-xl-6 pl-0 pr-0">
                                            <div class="row m-0">
                                                <div class="col-sm-6 col-6">
                                                    <div class="renewal-tripper-text text-disabled">Renewal Date</div>
                                                    <div class="renewal-tripper-date text-disabled">{{ carbonParseDate($subscription->billing_period_end_date)->format('M d, Y')  }}</div>
                                                </div>
                                                <div class="col-sm-6 col-6">
                                                    <div class="renewal-tripper-text text-disabled">Renewal Cost</div>
                                                    @php
                                                        $renewalCost = !empty($subscription->price) ? ($subscription->price)/100 : 0;
                                                    @endphp
                                                    <div class="renewal-tripper-date text-disabled">{{ \App\Helpers\Helper::formatMoney($renewalCost,2)  }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <a href="#"  id="cancel_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5" data-toggle="modal" data-target="#MembershipModal">Cancel My Tripper Membership</a>
                            </div>
                        @endif
                        <div class="membership-wrapper deactive-account">
                                <h3>DEACTIVATE MY ACCOUNT</h3>

                                    <p class="pb-2"> Deactivating your account will cancel any future stays and you will no longer be able to communicate with other users.  </p>

                                <a href="#"  id="cancel_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5" >Deactivate  My Account</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Content Wrapper -->
</div>


<!-- Modal -->
<div class="modal fade custom-modal membershipModal" id="MembershipModal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-lg cust-modal-lg" role="document">
        <div class="modal-content bg-white">
            <div class="modal-header">
                <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                        <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                    </svg>
                </button>
            </div>
            <div class="modal-body bg-white">
                <h1 class="modal-title"> Are you sure you want to cancel your membership?</h1>
                <div class="make-offer-body align-items-center w-10 vertical-middle ">
                    <p class="pera text-center"> By cancelling your Tripper Membership you will lose all the exclusive deals, access to listings, travel perks, and Tripper benefits from GOT2GO effective immediately.  </p>

                    <div class="modal-footer d-flex flex-column justify-content-center border-0">
                        <button type="submit" id="#" class="button-default-animate button-default-custom btn-sunset-gradient btn-wd mt-2" onclick="cancelSubscription('{{ getLoggedInRentalId() }}')">Cancel My Tripper Status</button>
                        <button type="submit" data-dismiss="modal" id="#" class="btn btn-800 btn-link mt-5">Keep My Tripper Status</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->

<div class="modal fade custom_modal" id="new-payment-method_popup" tabindex="-1" role="dialog" aria-labelledby="reg_popup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                        <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII=" />
                    </svg>
                </button>
            </div>
            <div class="modal-body" id="payment-save">
                <h1 class="modal-title"> Add a new payment method</h1>
                <div class="align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                    <form class="dashboard-forms" name="addPaymentDetails" id="addPaymentDetails" method="post" action="">
                    @csrf
                    <input type="hidden" name="payloadNonce" id="paymentMethodNonce">
                    <input type="hidden" id="card_type" name="card_type">
                    <div class="row m-0">

                        <div class="col-md-6">
                            <div class="form-group-custm no-icon card" id="cardNumber">
                            </div>
                            <span class="card_icon"></span>
                            <label class="error" id="numberError" style="display: none;">
                                Please enter in a valid credit card number.
                            </label>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="row">
                                    <div class="col-7">
                                        <div class="form-group-custm no-icon" id="expirationDate">
                                        </div>
                                        <label class="error" id="expirationDateError" style="display: none;">
                                            Please enter in a valid expiration date.
                                        </label>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group-custm no-icon" id="cvv">
                                        </div>
                                        <label class="error" id="cvvError" style="display: none;">
                                            Please enter in a valid security code.
                                        </label>
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                                <div class="form-group-custm no-icon">
                                <input type="text" class="form-class-cstm formcontrol-custom" name="nameOnCard" id="nameOnCard" placeholder="First Name">
                                        </div>
                                <label class="error" id="cardholderNameError" style="display: none;">
                                Name not valid.
                            </label>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" maxlength="14" name="lastname" id="lastname" placeholder="Last Name" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);">
                                    <div class="phonenumberError d-block w-100"></div>
                                </div>

                            </div>

                            <div class="col-lg-6  ">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" id="address" name="address" placeholder="Address 01">
                                </div>
                            </div>
                            <div class="col-lg-6  ">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" name="address2" id="address2" placeholder="Address 02">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" id="city" type="text" name="city" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" id="city" type="text" name="city" placeholder="Zipcode">
                                        <!-- <input class="form-class-cstm w-25 float-right" type="text" name="zip" id="zip" placeholder="ZIP Code"> -->
                                        </div>
                                    </div>
                                    <div class="d-block w-100">
                                        <div class="cityError d-inline-block w-75 float-left"></div>
                                        <div class="zipError d-inline-block w-25 float-right"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <div class="form-group-custm no-icon">
                                     <select class="form-class-cstm" name="state" id="state">
                                        <option value="">State</option>
                                        @foreach(getStatesWithAbbr() as $abbr => $state)
                                            <option value="{{ $abbr }}">{{ $state }}</option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer d-flex flex-column justify-content-center border-0">
                                    <button type="submit" id="add_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5 btn-wd ">Save Payment Method</button>
                                <button type="button"  class="close btn btn-800 btn-link mt-5" data-dismiss="modal">Cancel</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deletepayment" class="modal fade cmn-popup" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ml-auto">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                        <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII=" />
                    </svg>
                </button>
                </div>
                <div class="modal-body text-center">
                <div class="alert alert-danger" id="showerrormsg" style="display:none"></div>
                <input type="hidden" id="paymentcardid" name="paymentcardid">
                <span id="status_show"></span>
                <h1 class="d-block w-100 text-center popup-heading common-fatfrank"> Are you sure you want to delete this payment method?</h1>

                <div class="d-block mt-5 text-center delete-btns" >
                <button class="red-btn-cmn button-default-animate button-default-custom btn-sunset-gradient mt-5 " data-toggle="modal" onclick = "deletePaymentDetails(this)">Yes</button><br>
                <button id="btnClosePopup" class="button-custm-cancel close-btn-cmn mt-2" type="button" data-dismiss="modal">No</button>
            </div>


                </div>





            </div>
        </div>
</div>
<select style="display:none;" id="monthSelect" name="dwfrm_billing_paymentMethods_creditCard_expiration_month" >
        <option class="select-option" label="Month" value="" selected="selected">Month</option>
        <option class="select-option" label="01" value="01">01</option>
        <option class="select-option" label="02" value="02">02</option>
        <option class="select-option" label="03" value="03">03</option>
        <option class="select-option" label="04" value="04">04</option>
        <option class="select-option" label="05" value="05">05</option>
        <option class="select-option" label="06" value="06">06</option>
        <option class="select-option" label="07" value="07">07</option>
        <option class="select-option" label="08" value="08">08</option>
        <option class="select-option" label="09" value="09">09</option>
        <option class="select-option" label="10" value="10">10</option>
        <option class="select-option" label="11" value="11">11</option>
        <option class="select-option" label="12" value="12">12</option>
    </select>

    <select style="display:none;" class="input-select year" id="yearSelect" aria-required="true" aria-describedby="dwfrm_billing_paymentMethods_creditCard_expiration_year-error" aria-invalid="false">
        <option class="select-option" label="Year" value="" selected="selected">Year</option>
        <option class="select-option" label="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}" value="{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}">{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y') }}</option>
    </select>
@endsection

@section('headerCss')
    @parent
<style>
.card{
    border: none;
}
.card .card_icon {
    background: transparent url(../media/credit_card_sprites.png) no-repeat 30px 0;
    position: absolute;
    left: 262px !important;
    top: 8px;
}
#cardNumber,#expirationDate,#cvv,#nameOnCard  {
    font-size: 13px;
    padding: 15px 20px;
    height: 48px;
    background:#F9F9F9;
    border-radius:25px;
    margin-bottom: 5px;
}
span.card_icon {
    background-repeat: no-repeat;
    background-position: top left;
    float: right;
    width: 35px;
    height: 25px;
    position: absolute;
    right: 35px;
    top: 20px;
    background-size: 100%;
}
.modal-backdrop{
    z-index: 10;
}
#new-payment-method_popup .pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}
#new-payment-method_popup.modal{
    z-index: 20;
}
</style>
@endsection

@section('footerJs')
    @parent
<script src="https://js.stripe.com/v3/"></script>
<script>
    const baseURL = '{{ URL::to('') }}';

    function changeCardImage(ccCategoryName){
                let imagePath = baseURL +'/media/images/cc-icons/'+ccCategoryName+'.png';
                $('span.card_icon').css('backgroundImage','url("'+imagePath+'")');
            }

            function removeCardImage(){
                $('span.card_icon').css('backgroundImage','');
            }
            // Create a Stripe client.
            var stripe = Stripe('{{ env('STRIPE_PUBLIC_KEY') }}');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    fontSize: '13px',
                    padding: '15px 20px',
                    height: '55px',
                    background: '#F9F9F9',
                    borderRadius: '25px',
                    marginBottom: '5px'
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element.
            //var card = elements.create('card', {style: style});
            var cardNumberElm = elements.create('cardNumber');
            var cardExpiryElm = elements.create('cardExpiry');
            var cardCvcElm = elements.create('cardCvc');

            // Add an instance of the card Element into the `card-element` <div>.

            cardNumberElm.mount('#cardNumber');
            cardExpiryElm.mount('#expirationDate');
            cardCvcElm.mount('#cvv');

            // Handle real-time validation errors from the card Element.
            cardNumberElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#numberError").show();
                } else {
                    changeCardImage(event.brand);
                    $("#numberError").hide();
                }
            });

            cardExpiryElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#expirationDateError").show();
                } else {
                    //changeCardImage(event.brand);
                    $("#expirationDateError").hide();
                }
            });

            cardCvcElm.on('change', function(event) {
                //$("#card-errors").html('').hide();
                //removeCardImage();
                if (event.error) {
                    $("#cvvError").show();
                } else {
                    //changeCardImage(event.brand);
                    $("#cvvError").hide();
                }
            });

            $('#addPaymentDetails').validate({
                rules: {
                    nameOnCard:{
                        required:true,
                    },
                    address:{
                        required:true,
                    },
                    city: {
                        required: true,
                    },
                    state: {
                        required: true
                    },
                }
            });
            // Handle form submission.
            var form = document.getElementById('addPaymentDetails');
            form.addEventListener('submit', function(event) {
                // Submit the form
                event.preventDefault();

                stripe.createToken(cardNumberElm,{
                    name : $("#nameOnCard").val(),
                    address_line1 : '7415 N. Fairfield Ave.',
                    address_line2 : '',
                    address_city : 'IL',
                    address_state : 'Chicago'
                }).then(function(result) {
                    if (result.error) {
                        if(result.error.code == "incomplete_number" || result.error.code == "invalid_number"){
                            $("#numberError").show();
                        }
                        if(result.error.code == "incomplete_expiry" || result.error.code == "invalid_expiry_year_past" || result.error.code == "invalid_expiry_month_past"){
                            $("#expirationDateError").show();
                        }
                        if(result.error.code == "incomplete_cvc" || result.error.code == "invalid_cvv"){
                            $("#cvvError").show();
                        }

                        // Inform the user if there was an error.
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

            // Submit the form with the token ID.
            function stripeTokenHandler(token) {
                let formIsInvalid = !jQuery('#addPaymentDetails').valid();

                if(formIsInvalid){
                    return;
                }

                showLoader();
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('addPaymentDetails');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                $.ajax({
                    type: "POST",
                    url: "{{ route('savePaymentDetails') }}",
                    dataType: "JSON",
                    data: new FormData(document.getElementById("addPaymentDetails")),
                    processData: false,
                    contentType: false,
                    success: function(msg) {
                        if (msg.success == true) {
                            $('.alert-light-danger').hide();
                            $('#new-payment-method_popup').modal('hide');
                            $("#paymentinfo").css("display", "none");
                            Swal.fire({
                                icon: "success",
                                title: msg.message,
                                showConfirmButton: false,
                                timer: 3000
                            }).then(() => {
                                // window.onbeforeunload = null;
                                window.location.reload();
                            });
                            hideLoader();
                        } else {
                            hideLoader();
                            Swal.fire({
                                icon: "error",
                                title: msg.message,
                                showConfirmButton: false,
                                allowEscapeKey : true,
                                timer: 5000
                            });
                        }
                    }
                });
            }
</script>
<script src="{{URL::asset('js/cardcheck.js')}}"></script>
<script>
    var addPaymentDetails = jQuery('#addPaymentDetails');

   function deletePayment(id) {
        $('#paymentcardid').val(id);
    }
    function deletePaymentDetails() {
        var paymentId = $('#paymentcardid').val();
        // delete payment details
        showLoader();
        $.ajax({
            type: "POST",
            url: "{{ route('deletePaymentDetails') }}",
            dataType: "JSON",
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        "Authorization": "Bearer {{session()->get('rental_token')}}"
                    },
            data: {
                'id': paymentId
            },
            success: function (data) {
                hideLoader();
                if (data.success == true) {
                    $("#deletepayment").modal('hide');
                    $("#paymentinfo").css("display", "block");
                    $('.row_'+data.id).remove();
                    Swal.fire({
                                icon: "success",
                                title: data.message,
                                showConfirmButton: false,
                                timer: 3000
                            }).then(() => {
                                //window.location.reload();
                            });

                }
            },
            error: function (xhr, status, error) {
                $('#showcancelerrormsg').text(data.error).show().delay(1000).fadeOut();
            }
        });
    }
    $(document).ready(function() {

         $('.btn-add-payment-method').click(function () {
             $('#addPaymentDetails')[0].reset();
            cardNumberElm.clear();
            cardExpiryElm.clear();
            cardCvcElm.clear();
            $('input').removeClass('error');
            $('select').removeClass('error');
            $('label.error').css('display','none');
            $("#new-payment-method_popup").modal('show');
            $('#new-payment-method_popup').on('shown.bs.modal', function (e)
            {

                var options = {
                    componentRestrictions: {country: "us"},
                    types: ["geocode"]
                };

                var billing_address = document.getElementById('address');
                autocompleteTripper = new google.maps.places.Autocomplete(billing_address, options);
                autocompleteTripper.setFields(["address_component"]);
                // When the user selects an address from the drop-down, populate the
                // address fields in the form.
                autocompleteTripper.addListener("place_changed", fillInAddress);

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    const place = autocompleteTripper.getPlace();
                    let streetAddress = '';

                    document.getElementById('address').value = '';
                    for (const component in componentForm) {

                        // console.log( document.getElementById(component).value );
                        document.getElementById(component).value = "";
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details,
                    // and then fill-in the corresponding field on the form.
                    for (const component of place.address_components) {
                        const addressType = component.types[0];

                        if(addressType == "locality" || addressType == "sublocality_level_1"){
                            const val = component['long_name'];
                            document.getElementById('city').value = val;
                            $('#city').attr('readonly', true);
                        }
                        else if(addressType == "administrative_area_level_1"){
                            const val = component['short_name'];
                            document.getElementById('state').value = val;
                            $('#state').attr('readonly', true);
                        }
                        else if(addressType == "street_number"){
                            streetAddress = component['long_name'];
                            document.getElementById('address').value = streetAddress;
                        }
                        else if(addressType == "route"){
                            streetAddress += ' '+component['long_name'];
                            document.getElementById('address').value = streetAddress;
                        }

                    }
                 }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(position => {
                        const geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        const circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocompleteTripper.setBounds(circle.getBounds());
                        });
                    }
                }


            });


         });
        jQuery.validator.addMethod("passwordCheck",
            function(value, element, param) {
                if (this.optional(element)) {
                    return true;
                } else if (!/[A-Z]/.test(value)) {
                    return false;
                } else if (!/[a-z]/.test(value)) {
                    return false;
                } else if (!/[0-9]/.test(value)) {
                    return false;
                } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                    return false;
                }

                return true;
            },
            "Password should contain combination of uppercase, lowercase, digits & special characters");

        // min age validator
        $.validator.addMethod("minAge", function(value, element, min) {
            var today = new Date();
            var birthDate = new Date(value);
            var age = today.getFullYear() - birthDate.getFullYear();

            if (age > min+1) {
                return true;
            }

            var m = today.getMonth() - birthDate.getMonth();

            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            return age >= min;
        }, "You are not old enough!");

        $.validator.addMethod("regx", function(value, element, regexpr) {
               return this.optional(element) || regexpr.test(value);
            }, "Please enter valid phone number.");
        $('#myAccountSetting').validate({
            rules: {
                firstname: {
                    required: true,
                    lettersonly: true,
                    minlength: 3
                },
                lastname: {
                    required: true,
                    lettersonly: true,
                    minlength: 3
                },
                updatepassword: {
                    required: false,
                    minlength: 6,
                    maxlength: 20
                },
                retypepassword: {
                    required: false,
                    equalTo: "#updatepassword",
                    minlength: 6,
                    maxlength: 20
                },
                phonenumber: {
                    required: true,
                    regx:/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
                },
                dateofbirth: {
                    required: true,
                    minAge: 18
                },
            },
            messages: {
                firstname: {
                    required: "First Name is required",
                    minlength: "First Name should be atleast 3 characters",
                    lettersonly: jQuery.format("First name must consist of alphabetical characters only")
                },
                lastname: {
                    required: "Last Name is required",
                    minlength: "Last Name should be atleast 3 characters",
                    lettersonly: jQuery.format("Last name must consist of alphabetical characters only")
                },
                updatepassword: {
                    required: "Password is required",
                    minlength: "Password should be minimum 6 characters",
                    maxlength: "Password must be maximum 20 characters",
                },
                retypepassword: {
                    required: "Both passwords need to match",
                    equalTo: "Both passwords need to match",
                },
                phonenumber: {
                    required: "Phone Number is required"
                },
                dateofbirth: {
                    required: "Birth Date is required",
                    minAge: "You must be at least 18 years old to register as a Tripper."
                }
            },
            errorPlacement: function(error, element) {
                if(element.attr("name") == "retypepassword"){
                    error.insertAfter(".input-group-append.error-after");
                }else{
                    error.insertAfter(element);
                }

            }
        });
        $('.button-custm-cancel').on('click', function() {
            $("#addPaymentDetails")[0].reset();
            cardNumberElm.clear();
            cardExpiryElm.clear();
            cardCvcElm.clear();
            $('div').removeClass('card');
            $('input').removeClass('error');
            $('select').removeClass('error');
            $('.error').text('');
            $('.alert-light-danger').hide();
            $(".error").html('');
            $('#new-payment-method_popup').modal('hide');
        });


        $('.togl-btn-custom').click(function() {
            $(this).toggleClass("pushed");
        });
        // alphabet validator
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z ]+$/i.test(value);
        }, "Letters and spaces only please");

        $('#refreshpassword').on("click", function() {
            $('#retypepassword').val('');
        });

        $('#myAccountSetting').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            if (!$form.valid())
                return false;
            showLoader();
            $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        "Authorization": "Bearer {{session()->get('rental_token')}}"
                    },
                    url: "{{ route('setting-save') }}",
                    dataType: "JSON",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.success == true) {
                            Swal.fire({
                                icon: "success",
                                title: data.message,
                                showConfirmButton: false,
                                timer: 3000
                            }).then(() => {
                                // window.onbeforeunload = null;
                                window.location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: data.error,
                                showConfirmButton: false,
                                timer: 3000
                            }).then(() => {
                                window.location.reload();
                            });
                        }

                    }
                });


        });

        $("#dateofbirth").mask("00/00/0000");
        $("#kt_datepicker_2").datetimepicker({
            format: 'MM/DD/YYYY',
            timepicker: false,
            viewMode: 'years',
        });

        // month restriction to 2 digits
    function check(e,value)
    {
        //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
        if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
    }

    function checkLength()
    {
        var fieldLength = document.getElementById('month').value.length;
        if(fieldLength <= 2){
            return true;
        }
        else
        {
            var str = document.getElementById('month').value;
            str = str.substring(0, str.length - 1);
            document.getElementById('month').value = str;
        }
    }

    // year restriction to 4 digits
    function checkYear(e,value)
    {
        //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
        if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
    }

    function checkYearLength()
    {
        var fieldLength = document.getElementById('year').value.length;
        //Suppose u want 4 number of character
        if(fieldLength <= 4){
            return true;
        }
        else
        {
            var str = document.getElementById('year').value;
            str = str.substring(0, str.length - 1);
            document.getElementById('year').value = str;
        }
    }


    });
    // phone formatting
    const isNumericInput = (event) => {
        const key = event.keyCode;
        return ((key >= 48 && key <= 57) || // Allow number line
            (key >= 96 && key <= 105) // Allow number pad
        );
    };
    const isModifierKey = (event) => {
        const key = event.keyCode;
        return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
            (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
            (key > 36 && key < 41) || // Allow left, up, right, down
            (
                // Allow Ctrl/Command + A,C,V,X,Z
                (event.ctrlKey === true || event.metaKey === true) &&
                (key === 65 || key === 67 || key === 86 || key === 88 || key === 90)
            )
    };
    const enforceFormat = (event) => {
        // Input must be of a valid number format or a modifier key, and not longer than ten digits
        if (!isNumericInput(event) && !isModifierKey(event)) {
            event.preventDefault();
        }
    };
    const formatToPhone = (event) => {
        if (isModifierKey(event)) {
            return;
        }
        // I am lazy and don't like to type things more than once
        const target = event.target;
        const input = event.target.value.replace(/\D/g, '').substring(0, 10); // First ten digits of input only
        const zip = input.substring(0, 3);
        const middle = input.substring(3, 6);
        const last = input.substring(6, 10);

        if (input.length > 6) {
            target.value = `(${zip})-${middle}-${last}`;
        } else if (input.length > 3) {
            target.value = `(${zip}) ${middle}`;
        } else if (input.length > 0) {
            target.value = `(${zip}`;
        }
    };
    const inputElement = document.getElementById('phonenumber');
    inputElement.addEventListener('keydown', enforceFormat);
    inputElement.addEventListener('keyup', formatToPhone);
    // billing phone
    const inputElementPhone = document.getElementById('billingPhone');
    inputElementPhone.addEventListener('keydown', enforceFormat);
    inputElementPhone.addEventListener('keyup', formatToPhone);

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object) {
    phonevalue1 = ParseChar(object.value, zChar);
}

function ParseForNumber2(object) {
    phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object, e) {
    if (e) {
        e = e
    } else {
        e = window.event
    }
    if (e.which) {
        var keycode = e.which
    } else {
        var keycode = e.keyCode
    }

    ParseForNumber1(object)

    if (keycode >= 48) {
        ValidatePhone(object)
    }
}

function backspacerDOWN(object, e) {
    if (e) {
        e = e
    } else {
        e = window.event
    }
    if (e.which) {
        var keycode = e.which
    } else {
        var keycode = e.keyCode
    }
    ParseForNumber2(object)
}

function GetCursorPosition() {

    var t1 = phonevalue1;
    var t2 = phonevalue2;
    var bool = false
    for (i = 0; i < t1.length; i++) {
        if (t1.substring(i, 1) != t2.substring(i, 1)) {
            if (!bool) {
                cursorposition = i
                bool = true
            }
        }
    }
}

function ValidatePhone(object) {

    var srcObj = document.getElementById("billingPhone");

    var p = phonevalue1

    p = p.replace(/[^\d]*/gi, "")

    if (p.length < 3) {
        object.value = p
    } else if (p.length == 3) {
        pp = p;
        d4 = p.indexOf('(')
        d5 = p.indexOf(')')
        if (d4 == -1) {
            pp = "(" + pp;
        }
        if (d5 == -1) {
            pp = pp + ")";
        }
        object.value = pp;
    } else if (p.length > 3 && p.length < 7) {
        p = "(" + p;
        l30 = p.length;
        p30 = p.substring(0, 4);
        p30 = p30 + ")"

        p31 = p.substring(4, l30);
        pp = p30 + p31;

        object.value = pp;

    } else if (p.length >= 7) {
        p = "(" + p;
        l30 = p.length;
        p30 = p.substring(0, 4);
        p30 = p30 + ")"

        p31 = p.substring(4, l30);
        pp = p30 + p31;

        l40 = pp.length;
        p40 = pp.substring(0, 8);
        p40 = p40 + "-"

        p41 = pp.substring(8, l40);
        ppp = p40 + p41;

        object.value = ppp.substring(0, maxphonelength);
    }

    GetCursorPosition()

    if (cursorposition >= 0) {
        if (cursorposition == 0) {
            cursorposition = 2
        } else if (cursorposition <= 2) {
            cursorposition = cursorposition + 1
        } else if (cursorposition <= 5) {
            cursorposition = cursorposition + 2
        } else if (cursorposition == 6) {
            cursorposition = cursorposition + 2
        } else if (cursorposition == 7) {
            cursorposition = cursorposition + 4
            e1 = object.value.indexOf(')')
            e2 = object.value.indexOf('-')
            if (e1 > -1 && e2 > -1) {
                if (e2 - e1 == 4) {
                    cursorposition = cursorposition - 1
                }
            }
        } else if (cursorposition < 11) {
            cursorposition = cursorposition + 3
        } else if (cursorposition == 11) {
            cursorposition = cursorposition + 1
        } else if (cursorposition >= 12) {
            cursorposition = cursorposition
        }

        if (document.createRange) {     // all browsers, except IE before version 9
                var txtRange = document.createRange ();
                txtRange.selectNodeContents (srcObj);
                txtRange.deleteContents ();
            }
            else {      // Internet Explorer before version 9
                var txtRange = document.body.createTextRange ();
                txtRange.moveToElementText (srcObj);
                txtRange.select ();
                txtRange.execCommand ('cut');
            }
    }

}

function ParseChar(sStr, sChar) {
    if (sChar.length == null) {
        zChar = new Array(sChar);
    } else zChar = sChar;

    for (i = 0; i < zChar.length; i++) {
        sNewStr = "";

        var iStart = 0;
        var iEnd = sStr.indexOf(sChar[i]);

        while (iEnd != -1) {
            sNewStr += sStr.substring(iStart, iEnd);
            iStart = iEnd + 1;
            iEnd = sStr.indexOf(sChar[i], iStart);
        }
        sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

        sStr = sNewStr;
    }

    return sNewStr;
}

function cancelSubscription(userId)
{
    showLoader();

    $.ajax({
        type: "POST",
        url: "{{ route('api.tripper.subscription.cancelMySubscription',[ 'tripperId' => getLoggedInRentalId() ]) }}",
        dataType: "JSON",
        success: function (msg) {
            hideLoader();
            if(msg.success == true){
                window.location.reload();
            }else{
                Swal.fire({
                icon: "error",
                title: msg.error,
                showConfirmButton: false,
                timer: 3000
            }).then(() => {
                window.location.reload();
            });
            }
            
        }
    });
}

</script>
<script>
   jQuery( document ).ready(function() {

   var windowSize = $(window).width();

       if (windowSize <= 767) {
           jQuery('.link-social-account').prependTo('#link-account-mobile');
       }
       else{
           jQuery('.link-social-account').prependTo('#link-account-screen');

       }
   });
   jQuery( window ).resize(function() {
   var windowSize = $(window).width();

   if (windowSize <= 767) {
           jQuery('.link-social-account').prependTo('#link-account-mobile');

       }
       else{
           jQuery('.link-social-account').prependTo('#link-account-screen');

       }

   });
</script>
@endsection
