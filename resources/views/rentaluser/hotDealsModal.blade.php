<!-- MOdal popup hot deals slider -->
<div class="modal fade" id="hot_deals" tabindex="-1" role="dialog" aria-labelledby="hot_deals" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!-- <i aria-hidden="true" class="ki ki-close"></i> -->
                    <span class="svg-icon svg-icon-primary svg-icon-3x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo8\dist/../src/media/svg/icons\Navigation\Close.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect x="0" y="7" width="16" height="2" rx="1"/>
                                    <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"/>
                                </g>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                </button>

                <h2 class="text-center">Last Minute <strong>Hot Deals</strong></h2>
                <div class="d-table mt-0">
                    <div class="d-table-cell vertical-middle">
                        <div class="row">
                            <div class="col-lg-4 col-12 col-md-4">
                                <div class="input-group" id="">
                                    <img src="../../media/images/location.svg" alt="location">
                                    <input type="text" class="form-control" name="hotdeal_location" id="hotdeal_location" placeholder="Daytona, Florida">
                                </div>
                            </div>
                            <div class="col-lg-3 col-12 col-md-4">
                                <div class="input-group" id="kt_daterangepicker_1_validate">
                                    <img src="../../media/images/clock.svg" alt="clock">
                                    <input type="text" class="form-control" id="hotdeal_dates" name="hotdeal_dates" placeholder="When are you going?">
                                    <input type="hidden"  id="hotdeal_filter_dates"  name="hotdeal_filter_dates" value="" class="form-control required" placeholder="When are you going?">

                                </div>

                            </div>
                            <div class="col-lg-4 col-12 col-md-4">
                                <div class="input-group" id="">
                                    <img src="../../media/images/person.svg" alt="person">
                                    <input type="hidden" class="form-control"  id="hotdeal_guest"  name="hotdeal_guest">
                                    <input type="text" class="form-control adultchild"  id="hotdeal_adult_child"  id="hotdeal_adult_child" placeholder="1 Adults- 0 children">
                                    <input type="hidden" autocomplete="off" value="1-0" class="form-control" id="hotdeal_adultchildCount"  >


                                </div>
                            </div>
                            <div class="col-lg-1 col-md-6 text-right hot-deals-search-button">
                                
                                <button type="button" id="hotdeal_gobutton" style="border:none;width:50px" class=""> <img class="lazy-load" data-src="{{URL::asset('media/images/gobutton.svg')}}" alt="go button"></button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="owl-carousel owl-theme owl-centered tripper-page-carousalpopup test" id="tripper-page-carousal">
                                           
                </div>
            </div>
            <div class="owl-clone" style="display:none">
                <a class="aLink" href="#">
                    <div class="item ">
                        <div class="tripper-carousal-div">
                            <div class="title">

                            </div>
                            <div class="image">
                                
                            </div>
                            <div class="extra-info">
                                <h4>BOOK WITHIN 6 HOURS FOR</h4>
                                <h2><s class="price">${{rand(550,1000)}}</s>  <span color="blue" class="offer-price">${{number_format(500,0)}}</span><span class="light">/ NIGHT</span></h2>
                                <h4 class="ratings-bottom">
                                    @for ($i = 1; $i <= 5; $i++)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @endfor
                                    {{number_format(5,1)}} ({{5}})
                                </h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div> 
        </div>
    </div>
</div>
