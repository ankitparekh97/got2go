@extends('layouts.rentalApp')
@section('content')
<div class="my-account-dashboard">
    @include('rentaluser.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column pt-20">
            <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
                <i class="fas fa-bars"></i>
            </button>
            <!-- Main Content -->
            <div id="content">

                <div class="container-fluid my-acc-dashbrd">

                    <!-- Page Heading -->
                    <h2 class="dashboard-title mb-12">MY <strong>Dashboard</strong></h2>
                    <form class="dashboard-forms tripper-dboard-form">
                        <div class="row m-0">
                            <div class="col-xl-6 pl-0 pr-3">
                                <div class="card mb-4 triper-dashbrd-card-block">
                                    <div class="card-body p-10 upcoming-stays-block">
                                        <div class="d-block w-100">
                                            <div class="block-1-heading-1 common-fatfrank">STAY HAPPENING IN 3 DAYS!</div>
                                            <div class="block-1-heading-2 common-open"><a href="">All upcoming</a></div>
                                        </div>
                                        <div class="d-block">
                                            <div class="row m-0 mt-7 w-100">
                                                <div class="col-8 pl-0">
                                                    <div class="block-1-heading-3 common-fatfrank common-text-hide">LAKEVIEW PLACE</div>
                                                    <div class="block-1-heading-4 common-open common-text-hide">Mountain View, AK</div>
                                                </div>
                                                <div class="col-4 pl-0 pr-0">
                                                    <div class="block-1-heading-5 common-open"><b class="common-fatfrank">2 </b>Nights</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-img-block-1">
                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tripper-dboard-1.png')}}" alt="dashboard">
                                        </div>
                                        <div class="youare-good-to-go common-open">You Are Good To Go!</div>
                                        <div class="points-heading common-open">500 POINTS</div>
                                        <div class="text-center mt-7 block1-btn">
                                            <button type="submit" id="view-stay_btn" class="button-default-animate button-default-custom btn-purple-gradient">view stay</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 pl-0 pr-3">
                                <div class="card mb-4 triper-dashbrd-card-block">
                                    <div class="card-body p-8 dashboard-my-offer">
                                        <div class="d-block">
                                            <div class="block-comon-title common-open">My <b class="common-fatfrank"> Offers</b></div>
                                            <div class="block-1-heading-2 common-open"><a href="{{ route('web.tripper.myOffers') }}">View All</a></div>
                                        </div>
                                        <div class="d-block mt-16">

                                            <div class="triper-stay-dbrd-list mb-5">
                                                <div class="row m-0 w-100 ">
                                                    <div class="col-3 pl-0 pr-0">
                                                        <div class="trip-img-block-2">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                        </div>

                                                    </div>
                                                    <div class="col-9 pl-3 pr-0">
                                                        <div class="block-2-heading-1 common-fatfrank common-text-hide">THE MAIN ESCAPE</div>
                                                        <div class="block-2-heading-2 common-open common-text-hide">Portland, Maine</div>
                                                        <div class="block-2-heading-3 common-open common-text-hide">8/16/2020 - 8/19/2020</div>
                                                        <div class=""><a href="" class="block-2-heading-4 common-open">View Negotiation</a></div>
                                                    </div>
                                                </div>
                                                <button type="submit" id="go-btn" class="button-default-animate button-default-custom btn-purple-gradient d-none d-xs-none d-sm-block d-md-block d-lg-block">Go!</button>
                                            </div>
                                            <div class="triper-stay-dbrd-list mb-5">
                                                <div class="row m-0 w-100 ">
                                                    <div class="col-3 pl-0 pr-0">
                                                        <div class="trip-img-block-2">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystays">
                                                        </div>

                                                    </div>
                                                    <div class="col-9 pl-3 pr-0">
                                                        <div class="block-2-heading-1 common-fatfrank common-text-hide">THE MAIN ESCAPE</div>
                                                        <div class="block-2-heading-2 common-open common-text-hide">Portland, Maine</div>
                                                        <div class="block-2-heading-3 common-open common-text-hide">8/16/2020 - 8/19/2020</div>
                                                        <div class=""><a href="" class="block-2-heading-4 common-open">View Negotiation</a></div>
                                                    </div>
                                                </div>
                                                <button type="submit" id="go-btn" class="button-default-animate button-default-custom btn-purple-gradient d-none d-xs-none d-sm-block d-md-block d-lg-block">Go!</button>
                                            </div>
                                            <div class="triper-stay-dbrd-list mb-5">
                                                <div class="row m-0 w-100 ">
                                                    <div class="col-3 pl-0 pr-0">
                                                        <div class="trip-img-block-2">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                        </div>

                                                    </div>
                                                    <div class="col-9 pl-3 pr-0">
                                                        <div class="block-2-heading-1 common-fatfrank common-text-hide">THE MAIN ESCAPE</div>
                                                        <div class="block-2-heading-2 common-open common-text-hide">Portland, Maine</div>
                                                        <div class="block-2-heading-3 common-open common-text-hide">8/16/2020 - 8/19/2020</div>
                                                        <div class=""><a href="" class="block-2-heading-4 common-open">View Negotiation</a></div>
                                                    </div>
                                                </div>
                                                <button type="submit" id="go-btn" class="button-default-animate button-default-custom btn-purple-gradient d-none d-xs-none d-sm-block d-md-block d-lg-block">Go!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row m-0">

                            <div class="col-12 pl-0 pr-3">
                                <div class="card mb-4">
                                    <div class="card-body p-8">
                                        <div class="d-block">
                                            <div class="block-comon-title common-open">My <b class="common-fatfrank"> Deals</b></div>
                                            <div class="block-1-heading-2 common-open"><a href="">View All</a></div>
                                        </div>
                                        <div class="d-block mt-8">
                                            <div class="row m-0 my-deals-row">
                                                <div class="col-12 pl-0 pr-0">
                                                <div class="tripper-dboard-slider-deal owl-carousel">

                                                    <div class="deals-slider-box">
                                                        <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">abc JACKSON, </b> Wyoming</div>
                                                        <div class="d-block w-100 img">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                            <div class="circle-with-txt-overly">
                                                                <div class="circle-txt-overly common-open">
                                                                    <b class="common-fatfrank">$370</b>
                                                                    PER NIGHT</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="deals-slider-box">
                                                        <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">abc JACKSON, </b> Wyoming</div>
                                                        <div class="d-block w-100 img">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                            <div class="circle-with-txt-overly">
                                                                <div class="circle-txt-overly common-open">
                                                                    <b class="common-fatfrank">$370</b>
                                                                    PER NIGHT</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="deals-slider-box ">
                                                        <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">abc JACKSON, </b> Wyoming</div>
                                                        <div class="d-block w-100 img">
                                                            <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                            <div class="circle-with-txt-overly">
                                                                <div class="circle-txt-overly common-open">
                                                                    <b class="common-fatfrank">$370</b>
                                                                    PER NIGHT</div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                        <div class="deals-slider-box">
                                                            <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">JACKSON, </b> Wyoming</div>
                                                            <div class="d-block w-100 img">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                                <div class="circle-with-txt-overly">
                                                                    <div class="circle-txt-overly common-open">
                                                                        <b class="common-fatfrank">$370</b>
                                                                        PER NIGHT</div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="deals-slider-box ">
                                                            <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">JACKSON, </b> Wyoming</div>
                                                            <div class="d-block w-100 img">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                                <div class="circle-with-txt-overly">
                                                                    <div class="circle-txt-overly common-open">
                                                                        <b class="common-fatfrank">$370</b>
                                                                        PER NIGHT</div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="deals-slider-box">
                                                            <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">JACKSON, </b> Wyoming</div>
                                                            <div class="d-block w-100 img">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                                <div class="circle-with-txt-overly">
                                                                    <div class="circle-txt-overly common-open">
                                                                        <b class="common-fatfrank">$370</b>
                                                                        PER NIGHT</div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="deals-slider-box">
                                                            <div class="w-100 deal-titl common-open common-text-hide"><b class="common-fatfrank">JACKSON, </b> Wyoming</div>
                                                            <div class="d-block w-100 img">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/mystay.png')}}" alt="mystay">
                                                                <div class="circle-with-txt-overly">
                                                                    <div class="circle-txt-overly common-open">
                                                                        <b class="common-fatfrank">$370</b>
                                                                        PER NIGHT</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0">
                                <div class="col-xl-6 pl-0 pr-3">
                                    <div class="card mb-4 triper-dashbrd-card-block">
                                        <div class="card-body p-8">
                                            <div class="d-block">
                                                <div class="block-comon-title common-open recent-message-dashboard">Recent <b class="common-fatfrank"> messages (1)</b></div>
                                                <div class="block-1-heading-2 common-open recent-message-see-all"><a href="">See All</a></div>
                                            </div>
                                            <div class="d-block mt-8 recent-chat-box-block">
                                                <div class="recent-message-chatbox unread">
                                                    <div class="row m-0 w-100">
                                                        <div class="col-md-9 pl-0 pr-2">
                                                            <div class="d-inline-block float-left pr-1 w-25">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/john-smith.png')}}" alt="user">
                                                            </div>
                                                            <div class="d-inline-block float-left w-75">
                                                                <div class="recent-message-name common-open common-text-hide">
                                                                    John smith
                                                                </div>
                                                                <div class="recent-message-content common-open common-text-hide">
                                                                    Hello, how are…
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 pl-2 pr-0 text-right">
                                                            <div class="recent-message-date common-open">
                                                                07/01/2020
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="recent-message-chatbox">
                                                    <div class="row m-0 w-100">
                                                        <div class="col-md-9 pl-0 pr-2">
                                                            <div class="d-inline-block float-left  pr-1 w-25">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/john-smith.png')}}" alt="user">
                                                            </div>
                                                            <div class="d-inline-block float-left w-75">
                                                                <div class="recent-message-name common-open common-text-hide">
                                                                    John smith
                                                                </div>
                                                                <div class="recent-message-content common-open common-text-hide">
                                                                    Hello, how are…
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 pl-2 pr-0 text-right">
                                                            <div class="recent-message-date common-open">
                                                                07/01/2020
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="recent-message-chatbox">
                                                    <div class="row m-0 w-100">
                                                        <div class="col-md-9 pl-0 pr-2">
                                                            <div class="d-inline-block float-left  pr-1 w-25">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/john-smith.png')}}" alt="user">
                                                            </div>
                                                            <div class="d-inline-block float-left w-75">
                                                                <div class="recent-message-name common-open common-text-hide">
                                                                    John smith
                                                                </div>
                                                                <div class="recent-message-content common-open common-text-hide">
                                                                    Hello, how are…
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 pl-2 pr-0 text-right">
                                                            <div class="recent-message-date common-open">
                                                                07/01/2020
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="recent-message-chatbox">
                                                    <div class="row m-0 w-100">
                                                        <div class="col-md-9 pl-0 pr-2">
                                                            <div class="d-inline-block float-left  pr-1 w-25">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/john-smith.png')}}" alt="user">
                                                            </div>
                                                            <div class="d-inline-block float-left w-75">
                                                                <div class="recent-message-name common-open common-text-hide">
                                                                    John smith
                                                                </div>
                                                                <div class="recent-message-content common-open common-text-hide">
                                                                    Hello, how are…
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 pl-2 pr-0 text-right">
                                                            <div class="recent-message-date common-open">
                                                                07/01/2020
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="recent-message-chatbox">
                                                    <div class="row m-0 w-100">
                                                        <div class="col-md-9 pl-0 pr-2">
                                                            <div class="d-inline-block float-left pr-1 w-25">
                                                                <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/john-smith.png')}}" alt="user">
                                                            </div>
                                                            <div class="d-inline-block float-left w-75">
                                                                <div class="recent-message-name common-open common-text-hide">
                                                                    John smith
                                                                </div>
                                                                <div class="recent-message-content common-open common-text-hide">
                                                                    Hello, how are…
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 pl-2 pr-0 text-right">
                                                            <div class="recent-message-date common-open">
                                                                07/01/2020
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 pl-0 pr-3">
                                    <div class="card mb-4 triper-dashbrd-card-block">
                                        <div class="card-body p-8 my-tripboard-dashboard">
                                            <div class="d-block">
                                                <div class="block-comon-title common-open">my <b class="common-fatfrank"> tripboards</b></div>
                                                <div class="block-1-heading-2 common-open"><a href="">See All</a></div>
                                            </div>
                                            <div class="d-block mt-8">
                                                <div class="row m-0 my-tripboards-row">
                                                    <div class="col-lg-6 col-md-4 pl-0 pr-4 mb-9">
                                                        <div class="tripper-tripboard-box w-100 float-left">
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-1">
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-2">
                                                                    <div class="overlay-yelblue common-fatfrank">
                                                                        +20
                                                                    </div>
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-100 float-left mt-4 block-2-heading-1 common-fatfrank common-text-hide share-edit-tripboard">SAVED FAVORITE</div>
                                                        <div class="w-100 float-left mt-0 block-2-heading-4 common-open share-edit-tripboard">
                                                            <a href="" class="d-inline-block pr-2">Share </a> |
                                                            <a href="" class="d-inline-block pl-2"> edit</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-4 pl-0 pr-4 mb-9">
                                                        <div class="tripper-tripboard-box w-100 float-left">
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-1">
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-2">
                                                                    <div class="overlay-yelblue common-fatfrank">
                                                                        +20
                                                                    </div>
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-100 float-left mt-4 block-2-heading-1 common-fatfrank common-text-hide share-edit-tripboard">MAMA 70’s BIRTHDAY</div>
                                                        <div class="w-100 float-left mt-0 block-2-heading-4 common-open share-edit-tripboard">
                                                            <a href="" class="d-inline-block pr-2">Share </a> |
                                                            <a href="" class="d-inline-block pl-2"> edit</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-4 pl-0 pr-4 mb-9">
                                                        <div class="tripper-tripboard-box w-100 float-left">
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-1">
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                            <div class="w-50 float-left text-center">
                                                                <div class="tile-2">
                                                                    <div class="overlay-yelblue common-fatfrank">
                                                                        +20
                                                                    </div>
                                                                    <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/tile-1.png')}}" alt="image">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-100 float-left mt-4 block-2-heading-1 common-fatfrank common-text-hide share-edit-tripboard">DREAM PLACES</div>
                                                        <div class="w-100 float-left mt-0 block-2-heading-4 common-open share-edit-tripboard">
                                                            <a href="" class="d-inline-block pr-2">Share </a> |
                                                            <a href="" class="d-inline-block pl-2"> edit</a>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-4 pl-0 pr-4 mb-9">
                                                        <div class="tripper-tripboard-box create-new-box w-100 float-left">
                                                            <a class="plus-button common-fatfrank" href="" data-toggle="modal" data-target="#add_tripboard_popup">+</a>
                                                        </div>
                                                        <div class="w-100 float-left mt-4 block-2-heading-1 common-fatfrank common-text-hide text-center">Create new</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>

                </div>

            </div>


        </div>
        <!-- End of Content Wrapper -->
    <!-- End of Content Wrapper -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script>
        $(".tripper-dboard-slider-deal").owlCarousel({
           dots: false,
            nav: true,
            autoWidth: true,
            lazyLoad: true,
            loop: false,
            fade: true,
            responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:2
        },
        1024:{
            items:3
        },
        1200: {
            items:2
        },
        1300: {
            items: 3
        }
        }
        });

        $('.togl-btn-custom').click(function() {
            $(this).toggleClass("pushed");
        });
    </script>
@endsection
