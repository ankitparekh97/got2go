@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Timeshare Booking</h1>
    <div>&nbsp;
    <!-- <a style="margin: 19px;" href="{{ route('properties.create')}}" class="btn btn-primary">New Proeprty</a> -->
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Timeshare name</td>
          <td>property_type</td>
          <td>date</td>
          <td >Action</td>
        
        </tr>
    </thead>
    <tbody>
        @foreach($bookings as $booking)
    <tr id="row_{{$booking->booking_id}}">
            <td>{{$booking->booking_id}}</td>
            <td>{{$booking->timeshare_description}}</td>
            <td>{{$booking->property_type}}</td>
            <td>{{ Carbon\Carbon::parse($booking->check_in_date)->format('F-d')}} To {{ Carbon\Carbon::parse($booking->check_out_date)->format('F-d')}}</td>
            
            <td>
            @if($booking->is_cancelled == '0') 
              @if($booking->owner_status == 'pending' )
                <span  class="text-primary"> Owner Pending</span>
              @elseif($booking->owner_status == 'approved' )
                <span  class="text-success">Owner Approved</span>
              @elseif($booking->owner_status == 'decline' || $booking->owner_status == 'auto_reject')
                <span  class="text-danger"> {{ $booking->owner_status}}</span>
              @endif

              &nbsp;|&nbsp;
              @if($booking->property_status == 'pending')
                <span  class="text-primary">Property Pending</span>
              @elseif($booking->property_status == 'approved')
                <span  class="text-success">Property Approved</span>
              @elseif($booking->property_status == 'decline' || $booking->property_status == 'auto_reject')
                <span  class="text-danger"> {{ $booking->property_status}}</span>
              @endif

              <button id="cancleBtn"  class="btn btn-danger"type="button" onclick="cancleBooking({{$booking->booking_id}}, 0)">Cancle Booking</button>
            @else
              <span  class="text-danger">Canclled</span>
            @endif
            </td>
            <!-- <td>
                <form action="{{ route('properties.destroy', $booking->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$booking->id}},`{{ route('properties.destroy', $booking->id)}}`)">Delete</button>
                </form>
            </td> -->
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>

    $( document ).ready(function() {
        
    });

    function cancleBooking(booking_id,status){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: "{{ route('rental.booking.cancle') }}",
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, 'booking_id': booking_id},
                    dataType: 'JSON',
                    headers: {
                    "Authorization": "Bearer {{session()->get('rental_token')}}",
                },
                    success: function (data, status) {
                        if(data.success)
                          window.location.reload();
                        else
                          alert(data.error);

                          
                    }
                });
            } 
  

        


</script>
@endsection
