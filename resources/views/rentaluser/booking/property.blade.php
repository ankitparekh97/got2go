@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h1 class="display-3">Property Booking</h1>
    <div>&nbsp;
    <!-- <a style="margin: 19px;" href="{{ route('properties.create')}}" class="btn btn-primary">New Proeprty</a> -->
    </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>title</td>
          <td>property_type</td>
          <td>date</td>
          <td >Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($bookings as $booking)
    <tr id="row_{{$booking->booking_id}}">
            <td>{{$booking->booking_id}}</td>
            <td>
                @if($booking->booking_type == 'property'  && isset($booking->property->ownerproperty) )
                   {{ $booking->property->ownerproperty->title }}
                @elseif(isset($booking->property->ownertimeshare))
                  {{ $booking->property->ownertimeshare->timeshare_description }}
                @endif  
            </td>
            <td>{{$booking->property->property_type}}</td>
            <td>{{ Carbon\Carbon::parse($booking->check_in_date)->format('F-d')}} To {{ Carbon\Carbon::parse($booking->check_out_date)->format('F-d')}}</td>
            
            <td>
            @if($booking->is_cancelled == '0') 
              @if($booking->owner_status == 'pending' )
                <span  class="text-primary"> Owner Pending</span>
              @elseif($booking->owner_status == 'instant_booking')
              <span  class="text-primary"> Instant Booking</span>
              @elseif($booking->owner_status == 'approved' )
                <span  class="text-success">Owner Approved</span>
              @elseif($booking->owner_status == 'decline' || $booking->owner_status == 'auto_reject')
                <span  class="text-danger"> {{ $booking->owner_status}}</span>
              @endif

              &nbsp;|&nbsp;
              @if($booking->property_status == 'pending')
                <span  class="text-primary">Property Pending</span>
              @elseif($booking->property_status == 'approved')
                <span  class="text-success">Property Approved</span>
              @elseif($booking->property_status == 'decline' || $booking->property_status == 'auto_reject')
                <span  class="text-danger"> {{ $booking->property_status}}</span>
              @endif
              
              <button id="cancleBtn"  class="btn btn-danger"type="button" onclick="cancleBooking({{$booking->booking_id}}, 0)">Cancle Booking</button>
            @else
              <span  class="text-danger">Canclled</span>
            @endif 
            </td>
            <!-- <td>
                <form action="{{ route('properties.destroy', $booking->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="button" onclick="distroy({{$booking->id}},`{{ route('properties.destroy', $booking->id)}}`)">Delete</button>
                </form>
               
            </td> -->
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<script>
 $( document ).ready(function() {

      
    
 });
 function cancleBooking(booking_id,status){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: "{{ route('rental.booking.cancle') }}",
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, 'booking_id': booking_id},
                    dataType: 'JSON',
                    headers: {
                    "Authorization": "Bearer {{session()->get('rental_token')}}",
                },
                    success: function (data, status) {
                        if(data.success)
                          window.location.reload();
                        else
                          alert(data.error);

                          
                    }
                });
            }
</script>
@endsection
