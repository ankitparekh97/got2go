<script>
    @if (session()->has('message'))
    var popupId = "{{ uniqid() }}";
    if(!sessionStorage.getItem('shown-' + popupId)) {
        Swal.fire({
            position: "top-center",
            icon: "success",
            title: "{{ session('message') }}",
            showConfirmButton: false,
            timer: 1500
        });
    }
    sessionStorage.setItem('shown-' + popupId, '1');    
    @endif
</script>
