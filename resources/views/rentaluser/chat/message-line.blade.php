<li class="date-separator">
    <div class="separator-info"><span>{{$date}}</span></div>
</li>
@foreach($messages as $message)
@if($message->from_user == \Auth::guard('rentaluser')->user()->id && $message->from_user_type =='rental')
<li class="contact-chat active replies" data-message-id="{{ $message->id }}">
<div class="contact-wrap">
    <div class="chatuser-img">
        @if($message->pairUser->rentalUser->photo != '')
            <img data-src="{{URL::asset('uploads/users/'.$message->pairUser->rentalUser->photo)}}" alt="photo" class="lazy-load mCS_img_loaded">
        @else
            <div id="profileImage" class="rental">{{$message->pairUser->rentalUser->first_name[0].$message->pairUser->rentalUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="chatuser-info">
        <div class="preview">{!! $message->messages !!}</div>
        <div class="chat-date">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</div>
</li>
@else
<li class="contact-chat active" data-message-id="{{ $message->id }}">
<div class="contact-wrap">
    <div class="chatuser-img">
        @if($message->from_user_type =='admin')
            <img data-src="{{URL::asset('media/images/owner-fav.png')}}" alt="{{$message->pairUser->adminUser->name}}" class="lazy-load mCS_img_loaded">
        @elseif($message->pairUser->ownerUser->photo != '')
            <img data-src="{{URL::asset('uploads/owners/'.$message->pairUser->ownerUser->photo)}}" alt="photo" class="lazy-load mCS_img_loaded">
        @else
            <div id="profileImage" class="owner">{{$message->pairUser->ownerUser->first_name[0].$message->pairUser->ownerUser->last_name[0]}}</div>
        @endif
    </div>
    <div class="chatuser-info">
        <div class="preview">{!! $message->messages !!}</div>
        <div class="chat-date">{{date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($message->created_at, Session::get('timezone'))))}}</div>
    </div>
</div>
</li>
@endif                
@endforeach