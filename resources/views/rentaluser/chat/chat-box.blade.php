<div class="" id="chat_box" style="display:none">
    <div class="mychatbox-header">
        <div class="chatbox-user">
            <div class="d-block d-xs-block d-sm-block d-md-none d-lg-none chat-back-button"><img class="lazy-load" data-src="{{URL::asset('media/images/chat-back.svg')}}" alt="chat"></div>
            <div class="chatbox-userimg"></div> 
            <div class="chatbox-name">Louis Lit</div>
        </div>
    </div>
    <div class="mychatbox-body">
        <div class="mychatbox-tabs">
            <ul class="nav nav-tabs nav-tabs-line custom w-100">
                <li class="nav-item w-50">
                    <a class="nav-link active d-block text-center" data-toggle="tab" href="#kt_tab_pane_1">Conversation</a>
                </li>
            </ul>
            <div class="dashboard-forms">
                <div class="tab-content " id="myTabContent">
                    <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_1">

                        <div class="mychatlist-wrapper customscroll">
                            <div class="mychat-contactList">
                                <ul></ul>
                            </div>
                        </div>

                        <div class="mychatlist-footer">
                            <div class="mychatlist-footer-wrapper">
                                <div class="chat-textarea"><textarea class="form-control chat_input" id="" rows="1" spellcheck="false" placeholder="Type here...."></textarea></div>
                                <div class="chat-action">
                                    <button class="btn btn-800 btn-round btn-outline-grey btn-chat" data-to-user="" data-id="">Send</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    
<input type="hidden" id="to_user_id" value="" />
     <input type="hidden" id="id" value="" />
                </div>
            </div>
        </div>
    </div>


</div>