@extends('layouts.rentalApp')
@section('content')
<div class="my-account-dashboard">
    @include('rentaluser.sidebar')      

    <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column pt-20">
            <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
                <i class="fas fa-bars"></i>
            </button>
            <!-- Main Content -->
            <div id="content">
                <div class="container-fluid my-chat-dashbrd my-acc-dashbrd1">
                    <!-- Page Heading -->
                    <h2 class="dashboard-title mb-12">MY <strong>Chat</strong></h2>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body p-0">
                            <div class="mychat-wrapper">
                                <div class="mychat-left chat-contact-mobile">
                                    <div class="mychat-search">
                                        <div class="search-chat">
                                            <label for="#"><i class="fa fa-search" aria-hidden="true"></i></label>
                                            <input type="text" placeholder="Search..." name="name" id="search">
                                            <a href="javascript:;" class="searchclose" style="display:none"><i class="fas fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <div class="mychat-contactList customscroll">
                                        
                                    </div>
                                </div>

                                 @include('rentaluser.chat.chat-box')
                                <div class="mychat-right chat-detail-mobile" id="chat-overlay">
                                   <div class="tile height-block mb-0 p-0">
                                        <p class="first-conversation">Click on chat thread to start conversation</p>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>

                </div>

            </div>


        </div>
        <!-- End of Content Wrapper -->
       
    </div>
    <input type="hidden" id="current_user" value="{{ (new \App\Helpers\Helper)->urlsafe_b64encode(\Auth::guard('rentaluser')->user()->id) }}" />
    <input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') }}" />
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}" />
    @endsection

@section('footerJs')
    @parent
    <script src="{{URL::asset('js/bootstrap/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
		var getMessage = "{{route('rental.chat.getmessages')}}";
		var imagePath = "{{URL::asset('uploads/owners/')}}";
		var sendMessage = "{{route('rental.chat.send')}}";
        var chatSearch = "{{route('rental.chatsearch')}}";
        var imagesendPath = "{{URL::asset('uploads/users/')}}";
        var unreadUrl = "{{route('rental.unread.chat')}}";
        var adminLogo = "{{URL::asset('media/images/owner-fav.png')}}";
	</script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script src="{{ asset('js/renter/chat/chat.js') }}"></script>
@endsection