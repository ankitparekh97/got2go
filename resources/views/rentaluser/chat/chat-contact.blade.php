<ul>
     @foreach($users as $user)
    <li class="contact-chat chat-toggle" id="chat_contact_{{(new \App\Helpers\Helper)->urlsafe_b64encode($user->id)}}" data-id="{{ (new \App\Helpers\Helper)->urlsafe_b64encode($user->id) }}" data-user="{{($user->type =='admin')?$user->adminUser->name:$user->ownerUser->first_name.' '.$user->ownerUser->last_name}}" data-image="{{($user->type =='admin')?'Gotgo Admin':$user->ownerUser->photo }}" data-touserid="{{ (new \App\Helpers\Helper)->urlsafe_b64encode($user->owner_id) }}" data-type="{{$user->type}}" style="background-color:{{($user->messages_count > 0)?'#00800024':''}}">
        <div class="contact-wrap">
            <div class="chatuser-img">
                @if($user->type =='admin')
					<img data-src="{{URL::asset('media/images/owner-fav.png')}}" alt="{{$user->adminUser->name}}" class="lazy-load mCS_img_loaded">
				@elseif($user->ownerUser->photo != '')
					<img data-src="{{URL::asset('uploads/owners/'.$user->ownerUser->photo)}}" alt="{{$user->ownerUser->fisrt_name.' '.$user->ownerUser->last_name}}" class="lazy-load mCS_img_loaded">
				@else
					<div id="profileImage" class="owner">{{$user->ownerUser->first_name[0].$user->ownerUser->last_name[0]}}</div>
				@endif
            </div>
            <div class="chatuser-info">
                <div class="name-wrap">
                    <div class="name {{($user->type =='admin')?'admin_name':''}}">{{($user->type =='admin')?$user->adminUser->name:$user->ownerUser->first_name.' '.$user->ownerUser->last_name}}</div>
                    <div class="chat-date">{{date('m/d/Y',strtotime((new \App\Helpers\Helper)->convertToLocal($user->lastMessages->created_at, Session::get('timezone'))))}}</div>
                </div>
                <div class="message-block">
                    <div class="preview">{{$user->lastMessages->messages}}</div>
                    @if($user->messages_count > 0)
                    <span class="message-count">{{$user->messages_count}}</span>
                    @endif
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>