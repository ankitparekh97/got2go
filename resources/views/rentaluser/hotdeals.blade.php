<!-- Hot deals slider -->
<div class="home-hot-deal-slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="hot-deals-description">
                    <h1><strong>Hot</strong> Deals</h1>
                    <p>From wine tasting to hiking to hot air ballooning, select your travel mood and view listings from all your favorite places.</p>
                    <a href="{{route('view-stays')}}" class="orangeigradient-btn mobile-hide">Show me More!</a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="owl-carousel owl-theme owl-centered" id="tripper-page-carousal">
                    @if(count($hotDealsSlider)>0)
                        @foreach($hotDealsSlider as $item)
                            <div class="item">
                                <div class="tripper-carousal-div">
                                    <a href="{{ url('/propertydetail/'.$item->propertyid)}}">
                                        <div class="title">
                                            <h4><span>{{$item->city}},</span> {{$item->state}}</h4>
                                        </div>
                                        <div class="image">
                                            <img src="{{URL::asset('uploads/property/'.$item->cover_photo)}}" alt="photo" defer />
                                        </div>
                                        <div class="extra-info">
                                            <h4>BOOK WITHIN 6 HOURS FOR</h4>
                                            <h2><s>${{rand(550,1000)}}</s> <span color="blue">${{number_format($item->price,0)}}</span><span class="light">/ NIGHT</span></h2>
                                            <h4 class="ratings-bottom">
                                                @for ($i = 1; $i <= number_format($item->avgRating,0); $i++)
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                @endfor
                                                {{number_format($item->avgRating,1)}} ({{$item->totalRating}})
                                            </h4>
                                        </div>
                                    </a>    
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-12 text-center">
            <a href="#" class="orangeigradient-btn mobile-view mb-10">Show me More!</a>
            </div>
        </div>
    </div>
</div>
