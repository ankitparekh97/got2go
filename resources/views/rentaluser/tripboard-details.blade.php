@extends('layouts.rentalApp')
@section('content')
<section class="tripboard-view">
   <div class="container-fluid p-0">
   <div class="row m-0">
      <!-- Add new tripboard mobile box starts here -->
      <div class="add-new-tripboard-mobile">
         <img class="img-fluid" src="{{URL::asset('media/images/tripboard-mobile-heading.svg')}}" alt="">
         <p>make it happen. this is the year.</p>
         <p>
            <a class="btn-add-new-board button-default-animate" href="#" data-toggle="modal" data-target="#add_tripboard_popup"><span class="pr-2 plus">+</span> Add new board</a>
         </p>
      </div>
      <!-- add new tripboard mobile box ends here -->
      <div class="col-xl-3 tripboard-edit">
         <div class="w-100">
            <p class="tripboard-back-mobile">
               <a class="previous-icon mr-5" href="{{ route('tripboards') }}">
               <img src="{{URL::asset('media/images/left-arrow-cir.png')}}" alt="left arrow">
               </a>
               <img class="single-tripboard" src="{{URL::asset('media/images/single-tripboard.svg')}}" alt="ingle tripboard">
            </p>
            <div class="input-group mt-12 tripboard-edit-mobile">
               <input type="text" id="title" name="tripboard_name" autocomplete="off" class="input-boards-title" disabled value="{{$tripboard->tripboard_name }}">
               <input type="hidden" id="tripboard_id" name="tripboard_id" value="{{$tripboard->id }}">
               <button type="button" class="ml-auto pencil-edit board-pencil-icon-grey">
               <img src="{{URL::asset('media/images/board-pencil-icon.svg')}}" alt="pencil icon" class="d-none d-xs-none d-sm-block d-md-block d-lg-block">
               <img src="{{URL::asset('media/images/chat-mobile.svg')}}" alt="" class="d-block d-xs-block d-sm-none d-md-none d-lg-none">
               <i class="fas fa-circle chat-mobile-notification"></i>
               </button>
               <button type="button"  onClick="shareLink({{$tripboard->id }})" class="ml-auto mr-2 sharelink">
               <i class="fas fa-share-alt d-none d-xs-none d-sm-block d-md-block d-lg-block"></i>
               <img src="{{URL::asset('media/images/edit-tripboard-mobile.svg')}}" alt="" class="d-block d-xs-block d-sm-none d-md-none d-lg-none">
               </button>
               <button type="button" data-toggle="modal" data-target="" class="ml-auto sharelink">
               <i class="fa fa-trash d-none d-xs-none d-sm-block d-md-block d-lg-block"></i>
               <img src="{{URL::asset('media/images/share-tripboard-mobile.svg')}}" alt="" class="d-block d-xs-block d-sm-none d-md-none d-lg-none">
               </button>
            </div>
            <div class="border-separator-s mb-15  d-xs-none d-sm-none d-none d-md-block d-lg-block" style="margin-top: -15px;">
            </div>
            <!-- mobile tab starts here -->
            <div class="tripboard-mobile-tab responsive-scroll d-block d-xs-block d-sm-block d-md-none d-lg-none">
               <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link " data-toggle="tab" href="#search">Search</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link active" data-toggle="tab" href="#info-screen-mobile">Info</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" data-toggle="tab" href="#potentialdes">Potential  Destinations</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" data-toggle="tab" href="#similarstays">Similar Stays</a>
                  </li>
               </ul>
            </div>
            <!-- mobile tab ends here -->
            <!-- info section starts here -->
            <div id="info-screen">
               <div class="info-section">
                  <div class="mb-2 row">
                     <div class="col-6 pr-1">
                        <label class="col-form-label proposed-date-mob">Proposed Dates</label>
                     </div>
                     <div class="col-6 pl-1 text-right float-right">
                        <span class="switch d-inline-block">
                        <label>
                        <input type="checkbox" value="0" id="type_of_date" {{( $tripboard->type_of_date == '0') ? 'checked' : '' }} name="type_of_date" />
                        <span></span>
                        </label>
                        </span>
                        <label class="col-form-label text-right d-inline-block" id="type_of_date_label">{{( $tripboard->type_of_date == '0') ? 'Fixed Date' : 'Open Date' }}</label>
                     </div>
                  </div>
                  <div class="row bg-white gadget-tripboard">
                     <div class="col-4 pr-0 gadget-first-col">
                        <div class="text-center">
                           <label class="gadget-lbl">CHECK-IN</label>
                           <div class="input-group">
                              <div class="input-group-prepend">
                                 <span class="input-group-text p-0 pr-1">
                                 <i class="fa fa-calendar" aria-hidden="true"></i>
                                 </span>
                              </div>
                              <input type="text" class="form-control" readonly name="from_date" value="{{ date('m/d/Y', strtotime($tripboard->from_date))}}" id="kt_datepicker_3"/>
                           </div>
                        </div>
                     </div>
                     <div class="col-4 pl-0 gadget-last-col">
                        <div class="text-center">
                           <label class="gadget-lbl">CHECK-OUT</label>
                           <div class="input-group">
                              <div class="input-group-prepend">
                                 <span class="input-group-text p-0 pr-1">
                                 <i class="fa fa-calendar" aria-hidden="true"></i>
                                 </span>
                              </div>
                              <input type="text" class="form-control" name="to_date"  value="{{ date('m/d/Y', strtotime($tripboard->to_date))}}" readonly  id="kt_datepicker_4"/>
                           </div>
                        </div>
                     </div>
                     <div class="col-4 guest-middle-col ">
                        <div class="text-center">
                           <label class="gadget-lbl">GUESTS</label>
                           <div class="display-inline">
                              <div class="plus-minus-sec">
                                 <span class="guest-minus minus transition">-</span>
                                 <input id="guests" name="guests" type="text" max="30" value="{{$tripboard->no_of_guest}}" data-value="1" class="cmn-add" readonly/>
                                 <span class="guest-plus plus transition">+</span>
                              </div>
                              <div class="no_of_guest" style="display:none">{{count($travelbuddies)}}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <p class="pt-15 pb-2 who-is-going">
                     Who's Going
                  </p>
                  <div class="d-block form-group" id="buddyDiv">
                     @if(count($travelbuddies) > 0)
                     @foreach($travelbuddies as $buddy)
                     <div class="input-group add-buddie-group" id="div_{{$buddy->id}}">
                        <input type="text" disabled id="buddy_{{$buddy->id}}" name="buddy_name" data-id="{{$buddy->id}}" class="form-control edit-input" value="{{$buddy->buddy_name}}" placeholder="Add your travel buddy's name">
                        <button type="button" id="edit_{{$buddy->id}}" data-id="{{$buddy->id}}" class="edit-input-pencil"><img src="{{URL::asset('media/images/board-pencil-icon-darkgrey.svg')}}" alt="pencil icon"></button>
                     </div>
                     @endforeach
                     @endif
                     <div class="input-group add-buddie-group" id="cloneDiv" style="display:none">
                        <input type="text" id="buddy_" disabled data-id="new" class="form-control edit-input" value="" placeholder="">
                        <button type="button" id="edit_" data-id="new" class="edit-input-pencil"><img src="{{URL::asset('media/images/board-pencil-icon-darkgrey.svg')}}" alt=""></button>
                     </div>
                  </div>
                  <div class="d-block add-buddie-button">
                     <button type="button" class="button-default-animate btn-add-new-travelbuddy"><span class="pr-2 plus">+</span> Add Travel Buddy</button>
                  </div>
               </div>
            </div>
            <!-- info section ends here -->
         </div>
      </div>
      <div class="col-xl-9 tripboard-edit-right-section ">

         <form method="get" name="searchForm" autocomplete="off" id="searchForm" action="{{route('rental.search')}}" class="tab-content">
        <!-- info secreen starts here -->
            <div id="info-screen-mobile" class="tab-pane active">
            </div>
         <!-- info screen ends here -->
         <!-- search starts here -->
            <div class="row m-0 tab-pane fade search-mob-tab" id="search">
               <div class="col-lg-7 col-md-12  where-to-go" >
                  <input type="hidden" name="filter_dates_picker" id="filter_dates_picker" value="">
                  <input type="hidden"  id="filter_dates"  name="filter_dates" value="{{request()->get('filter_dates')}}" class="form-control required" placeholder="When are you going?">
                  <input type="hidden" id="number_of_guest"  name="number_of_guest"  value="{{request()->get('number_of_guest')}}" class="form-control persons required" placeholder="Number of People">
                  <input type="hidden" id="pageno"  name="pageno" value="0"  >
                  <div class="w-100 mb-7 d-none d-xs-none d-sm-none d-md-block d-lg-block ">
                     <div class="row m-0">
                        <div class="col-lg-12 col-md-12 pl-0 pr-0 where-to-go where-to-go-desktop">
                           <div class="row m-0">
                              <div class="col-lg-8 col-md-9 p-5 pt-9 pr-2">
                                 <div class="input-group pl-3">
                                    <img src="https://got2go.kitelytechdev.com/media/images/location.svg" alt="location">
                                    <input type="text"  id="location_detail" autocomplete="off" value="{{request()->get('location_detail')}}" name="location_detail" class="form-control required location_detail" placeholder="Got2Go to?">
                                    <input type="hidden"  id="hdn_resort_id" value="{{request()->get('resort_id')}}" name="resort_id" class="form-control " >
                                    <input type="hidden"  id="hdn_resort_total" value="0" name="hdn_resort_total" class="" >
                                    <input type="hidden"  id="hdn_search_text" name="searchText"  value="{{request()->get('searchText')}}" >
                                    <input type="hidden" name="address_selected_from_drop" class="address_selected_from_drop" value="{{request()->get('address_selected_from_drop')}}">
                                 </div>
                                 <div>
                                    <label class="error" id="location_error" style="display: none;">Please enter valid address.</label>
                                 </div>
                                 <div class="map iframe-map" style="display: none;">
                                    <div id="map" style="height:300px;"></div>
                                 </div>
                              </div>
                              <div class="col-lg-4 col-md-3 text-right pl-0 pt-5 pr-5 pb-5">
                                 <a href="" class="pr-4 pt-3 refine_filter_icon" data-toggle="modal" data-target="#filter_popup"><img src="https://got2go.kitelytechdev.com/media/images/filter.svg" alt="fillter"></a>
                                 <button type="submit" id="tripboardSubmit"  class="tripboard-search-button"><img src="https://got2go.kitelytechdev.com/media/images/gobutton.svg" alt="go button" style="height: 73px;"></button>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 pl-0 pr-0 checkboxes-inline">
                           <div class="row m-0 w-100">
                              <div class="col-4 pl-0 pr-2">
                                 <label class="custom-checkbox common-fatfrank">Vacation club
                                 <input type="checkbox" checked="checked" value="vacation_rental" name="property_type[]">
                                 <span class="checkmark"></span>
                                 </label>
                              </div>
                              <div class="col-3 pl-0 pr-4">
                                 <label class="custom-checkbox common-fatfrank float-right">hotels
                                 <input type="checkbox" checked="checked" value="hotels" name="property_type[]">
                                 <span class="checkmark"></span>
                                 </label>
                              </div>
                              <div class="col-5 pl-0">
                                 <label class="custom-checkbox common-fatfrank unchecked float-right">private residences
                                 <input type="checkbox" value="property" checked="checked" name="property_type[]">
                                 <span class="checkmark"></span>
                                 </label>
                              </div>
                              <div>
                                 <label class="error" id="property_error" style="display: none;">Please select atleast one property type.</label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row d-block d-xs-block d-sm-block d-md-none d-lg-none">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="input-group pl-3">
                                            <img src="{{URL::asset('media/images/location.svg')}}" alt="location" class="desktop-view">
                                            <img src="{{URL::asset('media/images/main-navigation/location1.svg')}}" alt="location" class="desktop-hide">
                                            <input type="text" class="form-control" autocomplete="off" name="location_detail" id="location_detail" placeholder="Where are you going?">
                                            <input type="hidden" name="address_selected_from_drop" class="address_selected_from_drop" value="">
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-md-6 paddingtop-30 ">
                                        <div class="input-group" >
                                            <img src="{{URL::asset('media/images/clock.svg')}}" alt="clock" class="desktop-view">
                                            <img src="{{URL::asset('media/images/main-navigation/clock1.svg')}}" alt="clock" class="desktop-hide">
                                            <input type="text" autocomplete="off" readonly  id="filter_dates_picker"  name="filter_dates_picker" value="{{request()->get('filter_dates_picker')}}" class="form-control" placeholder="When are you going?">
                                            <input type="hidden"  id="filter_dates"   name="filter_dates" value="{{request()->get('filter_dates')}}" class="form-control " placeholder="When are you going?">
                                        </div>

                                    </div>
                                    <div class="col-lg-3 col-md-6 paddingtop-30 person-box">
                                        <div id="achild" class="input-group">
                                            <img src="{{URL::asset('media/images/person.svg')}}" alt="person" class="desktop-view">
                                            <img src="{{URL::asset('media/images/main-navigation/person1.svg')}}" alt="person" class="desktop-hide">
                                            <input type="hidden" id="number_of_guest" name="number_of_guest" class="form-control persons" value="1" placeholder="1 Adult(s) - 0 Children">
                                            <input type="text" autocomplete="off" readonly class="form-control adultchild" id="adultchild" role="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" placeholder="1 Adult(s) - 0 Children">
                                        </div>
                                    </div>
                                 </div>
                  <div id="loader" class="loader" style="display: none;">
                     <img src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
                  </div>
               </div>
               <div id="wheretogo" class="d-block d-xs-block d-sm-block d-md-none d-lg-none where-to-go-tripboard">
                            <div class="checkboxes-inline res-checkbox-inline">
                                <div id="responsive-proerty-selection" >
                                    <ul class="property-type-list">
                                            <li><label class="custom-checkbox">Vacation Clubs
                                            <input name="property_type[]" value="vacation_rental" type="checkbox">
                                                <span class="checkmark">VACATION CLUBS</span>
                                                </label>
                                                <span class="numbers-of-property vacation_rental"> (0)</span>
                                            </li>
                                            <li><label class="custom-checkbox">Hotels
                                                <input name="property_type[]" value="hotels" type="checkbox" >
                                                <span class="checkmark">Hotels</span>
                                                </label>
                                                <span class="numbers-of-property hotels"> (0)</span>
                                            </li>
                                            <li><label class="custom-checkbox">Private residences
                                                <input name="property_type[]" value="property" type="checkbox">
                                                <span class="checkmark">Private residences</span>
                                                </label>
                                                <span class="numbers-of-property property"> (0)</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                             <!-- mobile search button starts here -->
                             <a href="#" class="tripboard-mobile-search-button d-block d-xs-block d-sm-block d-md-none d-lg-none ">Search</a>
                            <!-- mobile search button ends here -->
            </div>
            <!-- search ends here -->
            <div class="row potential-listing-section overflow-hidden tab-pane fade" id="potentialdes">
               <div class="col-md-8">
                  <div class="potential-listing-title common-open">
                     <strong class="common-fatfrank">Potential</strong> LISTINGS
                     </div>
               </div>
               <div class="col-md-4 text-right">
                  <div class="total-stays"><span>{{count($potentialListing)}}</span> {{(count($potentialListing) > 1)?"Stays":"Stay"}}</div>
               </div>
               <div class="col-12 pl-0 pr-0 tripper-dboard-form">
                  @if(count($potentialListing) > 0 && $potentialListing[0]->title != null)
                  <div class="potential-listing-slider tripper-dboard-slider-deal owl-carousel">
                     @foreach($potentialListing as $key => $potentialList)
                     <div class="deals-slider-box" id="sliderbox_{{$potentialList->property_id}}">
                        @php
                        $check_in_date_past = new \DateTime($potentialList->from_date);
                        $check_out_date_past = new \DateTime($potentialList->to_date);
                        $interval = $check_in_date_past->diff($check_out_date_past);
                        $pastDays = $interval->format('%a');
                        if($pastDays==1){
                        $pastDayLabel = 'day';
                        } else {
                        $pastDayLabel = 'days';
                        }
                        $pastNightsDays = $check_out_date_past->diff($check_in_date_past)->format("%a");
                        if($pastNightsDays == 0) {
                        $pastNightsDays = 1;
                        }
                        if($pastNightsDays==1){
                        $pastNightLabel = 'night';
                        } else {
                        $pastNightLabel = 'nights';
                        }
                        if(Auth::guard('rentaluser')->user()->is_tripper == '1'){
                        $potentialList->price = round($potentialList->price - (($discount / 100) * $potentialList->price));
                        }
                        $totalPrice = ($pastNightsDays * $potentialList->price);
                        $percentage = 30;
                        $before_price = $totalPrice + (30 / 100) * $totalPrice;
                        if($potentialList->votes <= 1){ $votesLabel='Vote' ; } else { $votesLabel='Votes' ; }
                        @endphp
                        <div class="d-block w-100 img">
                           <a href="{{ url('propertydetail/'.$potentialList->property_id)}}">
                           <img src="{{URL::asset('uploads/property/'.$potentialList->cover_photo)}}" alt="{{$potentialList->title}}" alt="">
                           </a>
                           <a href="#" data-toggle="modal" data-target="#deletedeal">
                              <div class="deal-delete" data-listing="{{$potentialList->title}}" data-id="{{$potentialList->property_id}}" data-tripboardId="{{$potentialList->tripboard_id}}">Delete</div>
                           </a>
                        </div>
                        <div id="loaddata" class="d-block w-100 potential-listing-slider-content">
                           <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide redirectPDP" data-redirectId="{{ $potentialList->property_id }}">{{$potentialList->city}}{{$potentialList->state ? ', ':''}} {{$potentialList->state}}</div>
                           <div class="potential-listing-slider-title-2 mt-1 common-text-hide redirectPDP" data-redirectId="{{ $potentialList->property_id }}">{{$potentialList->title}}</div>
                           <div class="potential-listing-slider-title-3 mt-7 common-text-hide potential-listing-price"><strong>{{ Helper::formatMoney($totalPrice,1) }} </strong> It was {{ Helper::formatMoney($before_price,1) }}</div>
                           <div class="potential-listing-slider-title-3 mt-3 common-text-hide potential-listing-vote">
                              <strong id="voted_{{$potentialList->property_id}}">{{$potentialList->totalVote ? $potentialList->totalVote: 0}} {{$votesLabel}}</strong>
                              <div id="votedclass_{{$potentialList->property_id}}" class="d-inline-block float-right  {{$potentialList->votes ? 'voted-text': ''}}">{{$potentialList->votes ? 'Voted!': ''}}</div>
                              <i id="votedtag_{{$potentialList->property_id}}" style="cursor:pointer" data-propertyId="{{$potentialList->property_id}}" data-tripboardId="{{$potentialList->tripboard_id}}" title="Vote" class="float-right fa fa-thumbs-up {{$potentialList->votes ? 'voted': ''}}" onclick="propertyVote(this)"></i>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
                  @else
                  <div class="d-block text-center">
                     <i class="fa fa-suitcase" aria-hidden="true"></i>
                     <div class="zero-state-text common-open">You have no listings saved to this Tripboard yet. Search for listings above to add them to your Tripboard!</div>
                  </div>
                  @endif
               </div>
            </div>
            <!-- simmilar stay starts here -->
            <div id="similar-stay-mobile">

            </div>
            <!-- simmilar stay ends here -->
            <!-- chat starts here -->
            <div id="chat-mobile">

            </div>
            <!-- chat ends here -->

            @include('rentaluser.advanceSearch')
         </form>
         <div class="row page-section">
            @if(count($potentialListing) > 0)
            <div class="col-xl-6 mb-15 tripboard-detail-chat" id="chat-desktop">
               <div class="take-last-min-trip-box pt-0">
                  <h1 class="take-last-min-text d-none d-sm-none d-xs-none d-lg-block d-md-block">
                     chat room
                  </h1>
                  <div class="border-separator"></div>
                  <div class="user-mobile-image d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
                        <img class="img-fluid" src="{{URL::asset('media/images/chat-user-1.png')}}" alt="user-mobile-img">
                  </div>
                  <div class="chat-mobile-date d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
                  Today · Aug 18 · 3:36 · Mama’s 70
                  </div>
               </div>
               <div class="minichat-wrapper" id="chat_box_{{(new \App\Helpers\Helper)->urlsafe_b64encode($tripboard->id)}}" data-id="{{(new \App\Helpers\Helper)->urlsafe_b64encode($tripboard->id)}}">

               <div class="mychat-contactList customscroll user-chat-conversation">
                     <ul></ul>
                  </div>
                  <div class="mychatlist-footer">
                     <div class="mychatlist-footer-wrapper">
                        <div class="chat-plus-icon d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
                           <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M1.1875 5.94335H11.0741" stroke="#E40246" stroke-width="1.23582" stroke-linecap="round" stroke-linejoin="round"/>
                           <path d="M6.13158 1V10.8866" stroke="#E40246" stroke-width="1.23582" stroke-linecap="round" stroke-linejoin="round"/>
                           </svg>
                        </div>
                        <div class="chat-textarea"><textarea class="form-control chat_input" id="" rows="1" spellcheck="false" placeholder="Type here...."></textarea></div>
                        <div class="chat-action">
                           <span class="d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
                           <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.5" d="M19.4587 12.6704L8.84837 13.927L8.84837 20.827L25.1598 12.6704L8.84749 4.51469L8.84837 11.4138L19.4587 12.6704Z" fill="#E40246"/>
                        </svg>
                           </span>
                           <button class="btn btn-800 btn-round btn-outline-grey btn-chat d-none d-xs-none d-sm-none d-md-block d-lg-block d-xl-block" data-to-user="" data-id="{{(new \App\Helpers\Helper)->urlsafe_b64encode($tripboard->id)}}">Send</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-5 tab-pane fade similar-stay-block" id="similarstays">
               <div class="take-last-min-trip-box">
                  <h1 class="take-last-min-text d-none d-xs-none d-sm-none d-md-block d-lg-block">
                     Similar stays
                  </h1>
                  <div class="border-separator d-none d-xs-none d-sm-none d-md-block d-lg-block"></div>
                  <div class="d-block mt-5">
                     @if(count($similarListing) > 0)
                     @foreach($similarListing as $key => $similarListings)
                     <div class="d-block mb-10 w-100 float-left">
                        <div class="d-inline-block float-left w-200px similar-stay-mob-box">
                           <a href="{{ url('propertydetail/'.$similarListings->id)}}">

                           <img alt="Pic" src="{{URL::asset('uploads/property/'.$similarListings->cover_photo)}}" class="similar-stay-row-img">
                           </a>
                        </div>
                        <div class="d-inline-block w-50 similar-stay-mob-detail">
                           <p class="name-place">
                              <a href="{{ url('propertydetail/'.$similarListings->id)}}">{{$similarListings->title}}</a>
                           </p>
                           <p class="price-of-place">${{number_format($similarListings->price)}}/night</p>
                           <ul class="recent-view-ul">
                              <li>{{$similarListings->no_of_guest}} {{$similarListings->no_of_guest > 1 ?'guests':'guest'}} </li>
                              <li>{{$similarListings->total_beds}} {{$similarListings->total_beds > 1 ?'beds':'bed'}} </li>
                              <li>{{$similarListings->total_bathroom}} {{$similarListings->total_bathroom > 1 ?'baths':'bath'}}</li>
                              <li>{{$similarListings->property_type}}</li>
                           </ul>
                        </div>
                     </div>
                     @endforeach
                     @else
                     <div class="d-flex flax-wrap mb-10">
                        <p class="mystay-tripboard-text">No similar stay found</p>
                     </div>
                     @endif
                  </div>
               </div>
               @endif
            </div>
         </div>
      </div>
   </div>
</section>
<div id="sharelinkMDL" class="modal fade" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header ml-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                  <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
               </svg>
            </button>
         </div>
         <div class="modal-body">
            <h1 class="d-block w-100 text-center popup-heading common-fatfrank copy-text-link"> The link to this tripboard has been copied!</h1>
         </div>
      </div>
   </div>
</div>
<div id="deletedeal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header ml-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                  <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
               </svg>
            </button>
         </div>
         <div class="modal-body">
            <input type="hidden" name="property_id" id="property_id">
            <input type="hidden" name="tripboardid" id="tripboardid">
            <h1 class="d-block w-100 text-center popup-heading common-fatfrank"> Are you sure you want to remove this listing from your tripboard?</h1>
            <h2 class="popup-sub-heading">Doing so will remove this <span id="listing"></span> from this tripboard.</h2>
            <div class="d-block w-100 pt-3 mb-5 text-center">
               <a href="#" class="button-default-animate purplewhite-gradient common-fatfrank" onClick="DeletePropertyFromTripBoard(this)">Yes, Remove</a>
            </div>
            <div class="d-block w-100 pt-3 mb-5 text-center">
               <a href="#" class="cancelbtn common-fatfrank" onClick="deleteCancel()">Cancel</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="add_tripboard_popup" tabindex="-1" role="dialog" aria-labelledby="add_tripboard_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-body text-center">
                    <h1 class="title-new-tripboard">NEW <span class="light">TRIPBOARD</span></h1>
                    <div class="add-tripboard-container align-items-center w-100">

                        <div class="vertical-middle">
                            <p class="pt-5">Name your board</p>
                            <input type="text" class="form-control input-board mt-10" name="url" placeholder="Say something like “Conference Weekend” or “Friend’s Bachelorette”">
                            <a class="btn-tripboard-fly mt-15 button-default-animate" href="#">ADD BOARD <svg width="58" height="26" viewBox="0 0 58 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.173828 13.944C2.80383 13.944 5.15983 13.976 7.51483 13.935C9.66883 13.898 11.8308 13.882 13.9718 13.673C16.3438 13.442 18.6928 12.982 21.0578 12.663C23.356 12.3296 25.6263 11.8259 27.8498 11.156C28.9828 10.827 30.1138 10.491 31.2338 10.122C32.0008 9.87005 32.0678 9.54905 31.4818 8.97205C31.1243 8.64501 30.748 8.33915 30.3548 8.05605C28.4048 6.54538 26.4525 5.03771 24.4978 3.53305C24.2506 3.35419 23.9851 3.20201 23.7058 3.07905C23.3867 2.97975 23.13 2.74075 23.0083 2.42951C22.8865 2.11827 22.9128 1.76854 23.0798 1.47905C23.4198 1.34705 23.7448 1.23605 24.0578 1.09805C25.1791 0.728833 26.3926 0.752108 27.4988 1.16405C28.7558 1.46405 29.9988 1.81805 31.2498 2.15705C33.2688 2.70405 35.2828 3.26905 37.3048 3.80705C38.4148 4.10705 39.5388 4.35105 40.6518 4.63905C41.3518 4.82005 42.0348 5.06405 42.7378 5.23305C44.0378 5.54605 45.1028 4.74205 46.2788 4.46305C47.2448 4.23305 48.2068 3.96305 49.1498 3.65505C50.4122 3.26806 51.653 2.81374 52.8668 2.29405C54.4875 1.70718 56.3017 2.12473 57.5028 3.36105C57.8912 4.23357 57.7788 5.24771 57.2088 6.01405C56.608 6.54353 55.9268 6.97423 55.1908 7.29005C53.6518 8.01805 52.0838 8.69005 50.5098 9.34005C49.4028 9.80005 48.2628 10.179 47.1478 10.62C46.876 10.716 46.6331 10.8794 46.4418 11.095C44.4268 13.695 42.4308 16.307 40.4298 18.917C39.0452 20.723 37.6612 22.5294 36.2778 24.336C35.5193 24.9479 34.5451 25.2261 33.5778 25.107C33.0088 24.868 33.1588 24.396 33.2518 23.963C33.4768 22.913 33.6918 21.863 33.9518 20.819C34.2518 19.598 34.6158 18.391 34.9248 17.172C35.1118 16.437 35.2798 15.695 35.3998 14.947C35.5148 14.236 34.9538 14.425 34.6128 14.476C33.8618 14.588 33.1208 14.769 32.3748 14.917C31.1318 15.164 29.8958 15.459 28.6428 15.637C27.1798 15.846 25.7038 15.958 24.2328 16.099C22.7048 16.246 21.1768 16.399 19.6458 16.508C18.916 16.5543 18.1843 16.5603 17.4538 16.526C15.7838 16.464 14.1118 16.414 12.4468 16.28C10.2108 16.102 7.96783 15.935 5.74683 15.604C4.00783 15.344 2.30383 14.848 0.589828 14.437C0.478828 14.411 0.399828 14.216 0.173828 13.944Z" fill="url(#paint0_linear)"/>
                            <defs>
                            <linearGradient id="paint0_linear" x1="0.173828" y1="25.134" x2="57.7313" y2="25.134" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#5495D1"/>
                            <stop offset="0.5" stop-color="#DDDDB6"/>
                            <stop offset="0.632" stop-color="#EEDA8C"/>
                            <stop offset="0.808" stop-color="#E6C469"/>
                            <stop offset="1" stop-color="#F6AB34"/>
                            </linearGradient>
                            </defs>
                            </svg>
                              </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" id="current_user" value="{{ (new \App\Helpers\Helper)->urlsafe_b64encode(\Auth::guard('rentaluser')->user()->id) }}" />
<input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') }}" />
<input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}" />
<input type="hidden" id="id" value="" />

@endsection

@section('footerJs')
    @parent
<script>
   var getMessage = "{{route('tripboard.chat')}}";
   var imagePath = "{{URL::asset('uploads/users/')}}";
   var sendMessage = "{{route('tripboard.chat.send')}}";
   var tripboardSearch = "{{route('rental.search')}}";
   var isTriboardId = false;
   var isTriboardExits = 0;
   var fromDate = "{{$tripboard->from_date}}";
   var toDate = "{{ $tripboard->to_date}}";
   var no_of_guest = "{{ $tripboard->no_of_guest | 1}}";
   var saverecordtripboard = "{{ route('rental.api.save.tripboard') }}";
   var rental_token = "{{session()->get('rental_token')}}";
   var buddyCount = "{{count($travelbuddies)}}";
   var savebuddyUrl = "{{ route('rental.api.save.buddy') }}";
   var deletebuddyUrl = "{{ route('rental.api.delete.buddy') }}";
   var propertyVote_url = "{{ route('rental.api.tripboard.vote') }}";
   var DeletePropertyFromTripBoardUrl = "{{ route('tripboard-remove-listing') }}";
   var shareLinkURL = "{{ url('tripboards') }}";

   $(".potential-listing-slider.owl-carousel").owlCarousel({
      dots: false,
      nav: true,
      lazyLoad: true,
      loop: false,
      slideBy: 3,
      fade: true,
      responsive: {
          0: {
              items: 1,
              slideBy: 1
          },
          600: {
              items: 2
          },
          991: {
              items: 3
          },
          1700: {
              items: 4
          },
          2100: {
              items: 5
          }
      }
   });
</script>
<style>
   .input-group.active .edit-input-pencil,
   .input-group.active .delete-input-icon {
   display: none;
   }
   .input-group.active .edit-input {
   width: 100%;
   border-radius: 0.42rem;
   }
   .daterangepicker td.off.available {
   visibility: hidden;
   }
   .redirectPDP {
   cursor: pointer;
   }
   .plus-minus-sec{
   padding-top: 0;
   }
</style>
<script>
   jQuery( document ).ready(function() {

   var windowSize = $(window).width();

       if (windowSize <= 767) {
           jQuery('.info-section').prependTo('#info-screen-mobile');
           jQuery('.tripboard-detail-chat').prependTo('#chat-mobile');
           jQuery('.similar-stay-block').prependTo('#similar-stay-mobile');
       }
       else{
           jQuery('.info-section').prependTo('#info-screen');
           jQuery('.tripboard-detail-chat').prependTo('#chat-desktop');
           jQuery('.similar-stay-block').prependTo('#similarstays');
       }
   });
   jQuery( window ).resize(function() {
   var windowSize = $(window).width();

   if (windowSize <= 767) {
           jQuery('.info-section').prependTo('#info-screen-mobile');
           jQuery('.tripboard-detail-chat').prependTo('#chat-mobile');
           jQuery('.similar-stay-block').prependTo('#similar-stay-mobile');
       }
       else{
           jQuery('.info-section').prependTo('#info-screen');
           jQuery('.tripboard-detail-chat').prependTo('#chat-desktop');
           jQuery('.similar-stay-block').prependTo('#similarstays');
       }

   });
</script>
<script src="{{ asset('js/renter/tripboard/tripboard.js') }}"></script>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script src="{{ asset('js/renter/tripboard/chat.js') }}"></script>
@endsection
