@extends('layouts.rentalApp')
@section('content')
<div class="my-account-dashboard">
    @include('rentaluser.sidebar')
    <!-- Content Wrapper -->
    <div id="sharelinkMystay" class="modal fade sharelinkMystay" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="sharing-heading">
                        Share it now! The link was copied to your clipboard.
                    </div>
                    <button type="button" class="btnClose ml-auto" data-dismiss="modal" aria-label="Close">
                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.3334 1.74802L7.08141 6.00002L11.3334 10.252L10.2521 11.3334L6.00008 7.08135L1.74808 11.3334L0.666748 10.252L4.91875 6.00002L0.666748 1.74802L1.74808 0.666687L6.00008 4.91869L10.2521 0.666687L11.3334 1.74802Z" fill="white" />
                        </svg>

                    </button>

                </div>

            </div>
        </div>
    </div>
    <div id="content-wrapper" class="d-flex flex-column pt-20">
        <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
            <em class="fas fa-bars"></em>
        </button>
        <!-- Main Content -->
        <div id="content">
            <div class="container-fluid my-acc-dashbrd">
                <!-- Page Heading -->
                <h2 class="dashboard-title mb-12">MY <strong>stays</strong></h2>
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-line custom w-100">
                            <li class="nav-item w-50">
                                <a class="nav-link active d-block text-center" data-toggle="tab" href="#kt_tab_pane_1">Upcoming Stays</a>
                            </li>
                            <li class="nav-item w-50">
                                <a class="nav-link d-block text-center" data-toggle="tab" href="#kt_tab_pane_2">Past stays</a>
                            </li>
                        </ul>
                        <hr class="mt-0" />
                        <form class="dashboard-forms pt-9">
                            <div class="tab-content mt-5" id="myTabContent">
                                <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2">

                                    @if(count($upcomingStays) > 0)
                                    @foreach($upcomingStays as $upcomingStay)
                                    <div class="row m-0 mb-5 my-stay-block">
                                        <div class="col-sm-10 pl-0 pr-1 custom-text-basic-rules">
                                            <div class="row m-0 ">
                                                <div class="col-lg-4 col-md-12 pl-0 pr-5 mb-4">
                                                    <div class="mystays-list-img">
                                                        <a href="{{ url('mystay-details/'.$upcomingStay->propertyid.'/'.$upcomingStay->bookingId)}}"><img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$upcomingStay->cover_photo)}}" alt="{{$upcomingStay->title}}"></a>
                                                    </div>
                                                    <div class="d-inline-block d-block d-xs-block d-sm-none d-md-none d-lg-none my-stay-share">
                                                <a href="{{ url('/propertydetail/'.$upcomingStay->propertyid.'?form_date='.date('m/d/Y',strtotime($upcomingStay->check_in_date)).'&to_date='.date('m/d/Y',strtotime($upcomingStay->check_out_date)).'&guest='.$upcomingStay->adults.'')}}" class="sharelink" data-toggle="modal" data-target="#sharelinkMystay"><img class="lazy-load" alt="share icon" data-src="{{URL::asset('media/images/my-dashboard/share-circle-icon.svg')}}"></a>
                                            </div>
                                                </div>
                                                <div class="col-lg-5 col-md-7 col-sm-8 pl-1 pr-1 cstm-padding-left">
                                                    <p class="mystays-list-title"><a href="{{ url('mystay-details/'.$upcomingStay->propertyid.'/'.$upcomingStay->bookingId)}}">{{$upcomingStay->title}}</a></p>
                                                    <p class="mystays-hotel-name">
                                                        @php
                                                            $address = explode(",",$upcomingStay->location_detail);
                                                            $date_now = date("Y-m-d");
                                                        @endphp

                                                        @if(strtotime($date_now) < strtotime($upcomingStay->check_out_date) && (($upcomingStay->owner_status == 'confirmed' && $upcomingStay->booking_type == 'vacation_rental') || ($upcomingStay->owner_status == 'approved' || $upcomingStay->owner_status == 'instant_booking' && $upcomingStay->booking_type == 'property')))
                                                            {{ $address[0] }} | {{$upcomingStay->city}}, {{$upcomingStay->state}} {{$upcomingStay->zipcode}}
                                                        @else
                                                            {{$upcomingStay->city}}, {{$upcomingStay->state}} {{$upcomingStay->zipcode}}
                                                        @endif
                                                    </p>
                                                    @php

                                                    $days = carbonDateDiffInDays($upcomingStay->check_in_date,$upcomingStay->check_out_date);
                                                   
                                                    $price = ($upcomingStay->actual_price) / $days;
                                                @endphp
                                                <div class="col-lg-3 col-md-5 col-sm-4 pl-1 pr-1 pt-2 d-block d-xs-block d-md-none d-lg-none d-sm-none">
                                                    <div class="left-my-stay-col">
                                                    <p class="mystays-date-dollar">{{date('M d', strtotime($upcomingStay->check_in_date))}} - {{date('M d', strtotime($upcomingStay->check_out_date))}}</p>
                                                    <p class="mystays-time">Check In: {{date('h:m A', strtotime($upcomingStay->check_in))}}</p>
                                                </div>
                                                <div class="right-my-stay-col">
                                                    <p class="mystays-date-dollar">{{Helper::formatMoney($price,0)}}/night</p>
                                                    <p class="mystays-time">Total: {{Helper::formatMoney($price * $days,1)}}</p>
                                                </div>
                                                </div>
                                                    @if($upcomingStay->owner_status == 'auto_reject' || $upcomingStay->owner_status == 'rejected')
                                                    <p class="d-inline-block mystay-status-text rejected">Booking Request Declined</p>
                                                    @elseif($upcomingStay->owner_status == 'pending')
                                                    <p class="d-inline-block mystay-status-text patience">Booking Request Pending</p>
                                                    @elseif(($upcomingStay->owner_status == 'approved' || $upcomingStay->owner_status == 'instant_booking') && ($upcomingStay->booking_type == 'vacation_rental'))
                                                    <p class="d-inline-block mystay-status-text patience">Pending Confirmation</p>
                                                    @elseif($upcomingStay->owner_status == 'cancelled')
                                                        @if(($upcomingStay->payment_type == 'refunded'))
                                                            <p class="d-inline-block mystay-status-text rejected">Cancelled, refund complete</p>
                                                        @else
                                                            <p class="d-inline-block mystay-status-text rejected">Booking Request Cancelled</p>
                                                        @endif

                                                    @else
                                                    <p class="d-inline-block mystay-status-text approved">Booked</p>
                                                    @endif
                                                </div>
                                                @php

                                                    $days = carbonDateDiffInDays($upcomingStay->check_in_date,$upcomingStay->check_out_date);
                                                    
                                                    $price = ($upcomingStay->actual_price) / $days;
                                                @endphp
                                                <div class="col-lg-3 col-md-5 col-sm-4 pl-1 pr-1 pt-2 d-none d-xs-none d-sm-block d-md-block d-lg-block">
                                                    <p class="mystays-date-dollar">{{date('M d', strtotime($upcomingStay->check_in_date))}} - {{date('M d', strtotime($upcomingStay->check_out_date))}}</p>
                                                    <p class="mystays-time">Check In: {{date('h:m A', strtotime($upcomingStay->check_in))}}</p>
                                                    <p class="mystays-date-dollar">{{Helper::formatMoney($price,0)}}/night</p>
                                                    <p class="mystays-time">Total: {{Helper::formatMoney($price * $days,1)}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 pl-1 pr-1 pt-0 mb-4 fix-icon-cstm d-none d-xs-none d-sm-block d-md-block d-lg-block">
                                            <div class="d-inline-block">
                                                <a href="{{ url('/propertydetail/'.$upcomingStay->propertyid.'?form_date='.date('m/d/Y',strtotime($upcomingStay->check_in_date)).'&to_date='.date('m/d/Y',strtotime($upcomingStay->check_out_date)).'&guest='.$upcomingStay->adults.'')}}" class="sharelink" data-toggle="modal" data-target="#sharelinkMystay"><img class="lazy-load" alt="share icon" data-src="{{URL::asset('media/images/my-dashboard/share-circle-icon.svg')}}"></a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row m-0 mb-5 vertical-middle">
                                        <p class="mystay-tripboard-text">You have no upcoming stays</p>
                                    </div>
                                    @endif
                                </div>
                                <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                    @if(count($pastStays) > 0)
                                    @foreach($pastStays as $pastStay)
                                    <div class="row m-0 mb-5 my-stay-block">
                                        <div class="col-sm-10 pl-0 pr-1 custom-text-basic-rules">
                                            <div class="row m-0 ">
                                                <div class="col-lg-4 col-md-12 pl-0 pr-5 mb-4">
                                                    <div class="mystays-list-img">
                                                        <a href="{{ url('mystay-details/'.$pastStay->propertyid.'/'.$pastStay->bookingId)}}"><img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$pastStay->cover_photo)}}" alt="{{$pastStay->title}}"></a>
                                                    </div>

                                                   
                                            <div class="d-inline-block d-block d-xs-block d-sm-none d-md-none d-lg-none my-stay-share">

                                                <a href="{{ url('/propertydetail/'.$pastStay->propertyid.'?form_date='.date('m/d/Y',strtotime($pastStay->check_in_date)).'&to_date='.date('m/d/Y',strtotime($pastStay->check_out_date)).'&guest='.$pastStay->adults.'')}}" class="sharelink" data-toggle="modal" data-target="#sharelinkMystay"><img class="lazy-load" alt="share icon" data-src="{{URL::asset('media/images/my-dashboard/share-circle-icon.svg')}}"></a>
                                            </div>
                                       
                                                </div>
                                                <div class="col-lg-5 col-md-7 col-sm-8 pl-1 pr-1 cstm-padding-left">
                                                    <p class="mystays-list-title"><a href="{{ url('mystay-details/'.$pastStay->propertyid.'/'.$pastStay->bookingId)}}">{{$pastStay->title}}</a></p>
                                                    <p class="mystays-hotel-name">
                                                        @php
                                                            $address = explode(",",$pastStay->location_detail);
                                                            $date_now = date("Y-m-d");
                                                        @endphp

                                                        @if(strtotime($date_now) < strtotime($pastStay->check_out_date) && (($pastStay->owner_status == 'confirmed' && $pastStay->booking_type == 'vacation_rental') || ($pastStay->owner_status == 'approved' || $pastStay->owner_status == 'instant_booking' && $pastStay->booking_type == 'property')))
                                                            {{ $address[0] }} | {{$pastStay->city}}, {{$pastStay->state}} {{$pastStay->zipcode}}
                                                        @else
                                                            {{$pastStay->city}}, {{$pastStay->state}} {{$pastStay->zipcode}}
                                                        @endif
                                                    </p>
                                                    @php
                                                    $days = carbonDateDiffInDays($pastStay->check_in_date,$pastStay->check_out_date);
                                                    
                                                    $price = ($pastStay->actual_price) / $days;
                                                @endphp
                                                <div class="col-lg-3 col-md-5 col-sm-4 pl-1 pr-1 pt-2 order-1 d-block d-xs-block d-md-none d-sm-none d-lg-none">
                                                    <div class="left-my-stay-col">
                                                        <p class="mystays-date-dollar">{{date('M d', strtotime($pastStay->check_in_date))}} - {{date('M d', strtotime($pastStay->check_out_date))}}</p>
                                                        <p class="mystays-time">Check In: {{date('h:m A', strtotime($pastStay->check_in))}}</p>
                                                    </div>
                                                    <div class="right-my-stay-col">
                                                        <p class="mystays-date-dollar">{{Helper::formatMoney($price,0)}}/night</p>
                                                        <p class="mystays-time">Total: {{Helper::formatMoney($price * $days,1)}}</p>
                                                    </div>
                                                </div>
                                                    @if($pastStay->owner_status == 'auto_reject' || $pastStay->owner_status == 'rejected')
                                                    <p class="d-inline-block mystay-status-text rejected">Booking Request Declined</p>
                                                    @elseif($pastStay->owner_status == 'approved' || $pastStay->owner_status == 'confirmed' || $pastStay->owner_status == 'instant_booking')
                                                    <p class="d-inline-block mystay-status-text approved order-12 order-sm-1">Booked</p>
                                                    @else
                                                         @if(($pastStay->payment_type == 'refunded'))
                                                            <p class="d-inline-block mystay-status-text rejected">Cancelled, refund complete</p>
                                                        @else
                                                            <p class="d-inline-block mystay-status-text rejected">Booking Request Cancelled</p>
                                                        @endif
                                                    @endif
                                                </div>
                                                 @php
                                                    $days = carbonDateDiffInDays($pastStay->check_in_date,$pastStay->check_out_date);
                                                    
                                                    $price = ($pastStay->actual_price) / $days;
                                                @endphp
                                                <div class="col-lg-3 col-md-5 col-sm-4 pl-1 pr-1 pt-2 order-1 d-none d-xs-none d-md-block d-sm-block d-lg-block">
                                                    <div class="left-my-stay-col">
                                                        <p class="mystays-date-dollar">{{date('M d', strtotime($pastStay->check_in_date))}} - {{date('M d', strtotime($pastStay->check_out_date))}}</p>
                                                        <p class="mystays-time">Check In: {{date('h:m A', strtotime($pastStay->check_in))}}</p>
                                                    </div>
                                                    <div class="right-my-stay-col">
                                                        <p class="mystays-date-dollar">{{Helper::formatMoney($price,0)}}/night</p>
                                                        <p class="mystays-time">Total: {{Helper::formatMoney($price * $days,1)}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 pl-1 pr-1 pt-0 mb-4  d-none d-xs-none d-sm-block d-md-block d-lg-block">
                                            <div class="d-inline-block">

                                                <a href="{{ url('/propertydetail/'.$pastStay->propertyid.'?form_date='.date('m/d/Y',strtotime($pastStay->check_in_date)).'&to_date='.date('m/d/Y',strtotime($pastStay->check_out_date)).'&guest='.$pastStay->adults.'')}}" class="sharelink" data-toggle="modal" data-target="#sharelinkMystay"><img class="lazy-load" alt="share icon" data-src="{{URL::asset('media/images/my-dashboard/share-circle-icon.svg')}}"></a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row m-0 mb-5 vertical-middle">
                                        <p class="mystay-tripboard-text">You have no past stays</p>
                                    </div>
                                    @endif
                                </div>

                            </div>
                        </form>

                    </div>

                    <div class="mystay-tripboard">
                        <div class="add-tripboard-container align-items-center w-100 text-center">

                            <div class="vertical-middle">
                                <p class="mystay-tripboard-text">Plan the perfect trip with your travel buddies by exploring destinations, adding listing & voting on the perfect place to stay.</p>

                                <a class="btn-tripboard-fly mt-10 button-default-animate rounded" href="{{ url('tripboards')}}">view my tripboard <svg class="ml-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="57.558" height="24.346" viewBox="0 0 57.558 24.346">
                                        <defs>
                                            <linearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                                                <stop offset="0" stop-color="#5495d1" />
                                                <stop offset="0.5" stop-color="#ddddb6" />
                                                <stop offset="0.632" stop-color="#eeda8c" />
                                                <stop offset="0.808" stop-color="#e6c469" />
                                                <stop offset="1" stop-color="#f6ab34" />
                                            </linearGradient>
                                        </defs>
                                        <path id="Path_1" data-name="Path 1" d="M147.455,1310.953c2.63,0,4.986.032,7.341-.009,2.154-.037,4.316-.053,6.457-.262,2.372-.231,4.721-.691,7.086-1.01a46.881,46.881,0,0,0,6.792-1.507c1.133-.329,2.264-.665,3.384-1.034.767-.252.834-.573.248-1.15a12.428,12.428,0,0,0-1.127-.916q-2.925-2.266-5.857-4.523a4.324,4.324,0,0,0-.792-.454,1.1,1.1,0,0,1-.626-1.6c.34-.132.665-.243.978-.381a5.2,5.2,0,0,1,3.441.066c1.257.3,2.5.654,3.751.993,2.019.547,4.033,1.112,6.055,1.65,1.11.3,2.234.544,3.347.832.7.181,1.383.425,2.086.594,1.3.313,2.365-.491,3.541-.77.966-.23,1.928-.5,2.871-.808a36.984,36.984,0,0,0,3.717-1.361,4.383,4.383,0,0,1,4.636,1.067,2.644,2.644,0,0,1-.294,2.653,7.564,7.564,0,0,1-2.018,1.276c-1.539.728-3.107,1.4-4.681,2.05-1.107.46-2.247.839-3.362,1.28a1.7,1.7,0,0,0-.706.475c-2.015,2.6-4.011,5.212-6.012,7.822q-2.077,2.709-4.152,5.419a3.6,3.6,0,0,1-2.7.771c-.569-.239-.419-.711-.326-1.144.225-1.05.44-2.1.7-3.144.3-1.221.664-2.428.973-3.647.187-.735.355-1.477.475-2.225.115-.711-.446-.522-.787-.471-.751.112-1.492.293-2.238.441-1.243.247-2.479.542-3.732.72-1.463.209-2.939.321-4.41.462-1.528.147-3.056.3-4.587.409a19.918,19.918,0,0,1-2.192.018c-1.67-.062-3.342-.112-5.007-.246-2.236-.178-4.479-.345-6.7-.676-1.739-.26-3.443-.756-5.157-1.167C147.76,1311.42,147.681,1311.225,147.455,1310.953Z" transform="translate(-147.455 -1297.832)" fill="url(#linear-gradient)" />
                                    </svg>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
    <!-- End of Content Wrapper -->
</div>
<script>
    //share my stay
        var $temp = $("<input>");
        var $url = $(location).attr('href');
        $('.sharelink').on('click', function() {
            $url = $(this).attr('href');
            $("body").append($temp);
            $temp.val($url).select();
            document.execCommand("copy");
            $temp.remove();
        });
</script>
@endsection
