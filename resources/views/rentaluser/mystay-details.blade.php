@extends('layouts.rentalApp')
@section('content')
<div class="my-account-dashboard">
@include('rentaluser.sidebar')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column pt-20">
            <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
                <em class="fas fa-bars"></em>
            </button>
            <!-- Main Content -->
            <div id="content">
                <div class="container-fluid my-acc-dashbrd">
                    <h2 class="dashboard-title mb-12">My <strong>STays</strong></h2>
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <a class="back-to-all-stays" href="{{ url('/mystay')}}"><i class="fas fa-arrow-left"></i>Back to all stays</a>
                            <div class="d-block">
                                <h3 class="type-apt-title">{{$bookingDetails->title}}</h3>
                                <p class="type-apt-subtitle">
                                @php
                                    $address = explode(",",$bookingDetails->location_detail);
                                @endphp

                                @if($showStAdd == true)
                                    {{ $address[0] }} |
                                @endif

                                {{$bookingDetails->city}}, {{$bookingDetails->state}} @empty(!$bookingDetails->zipcode) {{$bookingDetails->zipcode}} @endempty
                                </p>
                            </div>

                            <form class="dashboard-forms pt-2">
                                <div class="row m-0 mb-10">
                                    <div class="col-xl-6 col-lg-4 pl-1 pr-2 mb-4">
                                        <div class="mystays-list-img-big mb-1">
                                            <a class="w-100 py-5 button-default-animate button-default-custom btn-sunset-gradient mt-5 text-uppercase" href="{{ url('/propertydetail/'.$bookingDetails->propertyid.'?form_date='.$bookingCheckInDate.'&to_date='.$bookingCheckOutDate.'&guest='.$bookingDetails->adults.'')}}">
                                                <img class="lazy-load" data-src="{{URL::asset('uploads/property/'.$bookingDetails->cover_photo)}}" alt="{{$bookingDetails->title}}" alt="Got2go">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-lg-8 pl-2 pr-0">
                                        <div class="row m-0 ">
                                            <p class="standard-rates">standard rates</p>
                                        </div>
                                        <div class="row m-0 pt-2">
                                            <div class="col-6 pl-1 pr-1">
                                                <p class="standard-rates-title">date</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-title text-center">nights</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-title text-center">$/night</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-title text-center">total</p>
                                            </div>
                                        </div>
                                        <div class="row m-0 pt-2">
                                            <div class="col-6 pl-1 pr-1">
                                            @php
                                                            $check_in_date_past = new \DateTime($bookingDetails->check_in_date);
                                                            $check_out_date_past = new \DateTime($bookingDetails->check_out_date);
                                                            $interval = $check_in_date_past->diff($check_out_date_past);
                                                            $pastDays = $interval->format('%a');
                                                            if($pastDays==1){
                                                                $pastDayLabel = 'day';
                                                            } else {
                                                                $pastDayLabel = 'days';
                                                            }
                                                            $pastNightsDays = $check_out_date_past->diff($check_in_date_past)->format("%a");
                                                            if($pastNightsDays==1){
                                                                $pastNightLabel = 'night';
                                                            } else {
                                                                $pastNightLabel = 'nights';
                                                            }
                                                           
                                                            $price = ($bookingDetails->actual_price ) / $pastDays;
                                                        @endphp
                                                <p class="standard-rates-content">{{date('M jS, Y', strtotime($bookingDetails->check_in_date))}} - {{date('M jS, Y', strtotime($bookingDetails->check_out_date))}}</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-content text-center">{{$pastNightsDays}}</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-content text-center">${{number_format($price)}}</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-content text-center">${{number_format($price * $pastNightsDays)}}</p>
                                            </div>
                                        </div>
                                        <div class="row m-0 standard-rates-total">
                                            <div class="col-6 pl-3 pr-1">
                                                <p class="standard-rates-total-text text-uppercase">STANDARD total</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-total-text text-center">{{$pastNightsDays}}</p>
                                            </div>
                                            <div class="col-2 pl-1 pr-1">

                                            </div>
                                            <div class="col-2 pl-1 pr-1">
                                                <p class="standard-rates-total-text text-center">${{number_format($price * $pastNightsDays)}}</p>
                                            </div>
                                        </div>
                                        <div class="row m-0 statndard-rates-status">
                                            <div class="d-block w-100 mt-3">
                                            @if($bookingDetails->owner_status == 'auto_reject' || $bookingDetails->owner_status == 'rejected')
                                                <p class="d-inline-block mystay-status-text rejected">Booking Request Declined</p>
                                                @elseif($bookingDetails->owner_status == 'pending')
                                                <p class="d-inline-block mystay-status-text patience w-100">Booking Request Pending</p>
                                                @elseif($bookingDetails->owner_status == 'cancelled')
                                                 @if(($paymnetStatus == 'refunded'))
                                                    <p class="d-inline-block mystay-status-text rejected w-100">Cancelled, refund complete</p>
                                                 @else
                                                    <p class="d-inline-block mystay-status-text rejected w-100">Booking Request Cancelled</p>
                                                @endif
                                                @elseif(($bookingDetails->owner_status == 'approved' || $bookingDetails->owner_status == 'instant_booking') && ($bookingDetails->booking_type == 'vacation_rental'))
                                                {{-- <div class="d-inline-block mystays-list-status">
                                                    <img class="lazy-load" alt="Just a bit more patience" data-src="{{URL::asset('media/images/my-dashboard/pending.svg')}}">
                                                </div> --}}
                                                <p class="d-inline-block mystay-status-text patience w-100">Pending Confirmation</p>
                                                @else
                                                <p class="d-inline-block mystay-status-text approved w-100">Booked</p>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="row m-0 w-100 d-block">
                                           <a class="w-100 py-5 button-default-animate button-default-custom btn-sunset-gradient mt-5 text-uppercase" href="{{ url('/propertydetail/'.$bookingDetails->propertyid.'?form_date='.$bookingCheckInDate.'&to_date='.$bookingCheckOutDate.'&guest='.$bookingDetails->adults.'')}}">view listing</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="row m-0 mb-5 view-mystays-para">
                                    <p class="title">ABOUT THE PLACE</p>
                                    <p class="second-para">{{$bookingDetails->description}}</p>
                                    <p class="title">THE SPACE</p>
                                    <p>{{$bookingDetails->about_the_space}}</p>
                                    <p class="title">
                                        ENTIRE HOME HOSTED BY
                                    </p>
                                    <div class="entire-home-hosted-by">
                                        <div class="d-block">
                                            <img class="lazy-load" data-src="{{ $bookingDetails->photo ? url('uploads/owners/'.$bookingDetails->photo) : url('/media/users/default.jpg') }}" alt="{{$bookingDetails->first_name}}" alt="host owner">
                                            <h4>{{$bookingDetails->first_name}}</h4>
                                        </div>
                                        <h5 class="d-block">{{ $bookingDetails->no_of_guest > 1 ? $bookingDetails->no_of_guest ." guests" : $bookingDetails->no_of_guest ." guest" }} · {{ $bookingDetails->total_bedroom > 1 ? $bookingDetails->total_bedroom ." bedrooms" : $bookingDetails->total_bedroom. " bedroom" }} · {{ $bookingDetails->total_beds > 1 ? $bookingDetails->total_beds ." beds" : $bookingDetails->total_beds ." bed" }} · {{ $bookingDetails->total_bathroom > 1 ? $bookingDetails->total_bathroom ." baths" : $bookingDetails->total_bathroom ." bath" }}</h5>
                                    </div>

                                    <div class="view-stay-type-residence">
                                        <img class="lazy-load" data-src="{{URL::asset('media/images/my-dashboard/home-gradient-icon.svg')}}" alt="residence">
                                        <p>@if($bookingDetails->type_of_property == 'vacation_rental')
                                        Vacation Rental
                                        @else
                                        Private Residence
                                        @endif</p>
                                    </div>
                                    <div class="view-stay-policy">
                                    <img data-src="{{URL::asset('media/images/policy-icon.png')}}" alt="policy" class="lazy-load d-inline-block"/>
                                    @if(($bookingDetails->owner_status == 'cancelled'))
                                        @if(($paymnetStatus == 'refunded'))
                                            <h5 class="d-inline-block pl-3 pr-5">Refund Processed </h5>
                                         @else
                                          <h5 class="d-inline-block pl-3 pr-5">Cancelled</h5>
                                          @endif
                                    @else
                                        <h5 class="d-inline-block pl-3 pr-5">{{$cancellationText}}<a data-toggle="modal" data-target="#cancellation-policy" class="policy"></a>
                                           </h5>
                                    @endif
                                            <div class="d-inline-block" style="{{($bookingDetails->owner_status == 'cancelled')?'display:none !important':''}}">
                                                <a class="view-past-stay-btn" href="#" data-toggle="modal" data-target="#cancel_popup">Cancel stay</a>
                                            </div>
                                    </div>
                                    <p class="title mt-10">
                                       Amenities

                                    </p>

                               <div class="col-xl-7">
                               <div class="owl-carousel owl-theme" id="view-stay-page-slider">
                               @foreach($propertyAmenities as $amenity)
                                <div class="item">
                                    <span>
                                        @switch( $amenity )
                                        @case($amenity->masteramenity->amenity_name == 'Kid Friendly')
                                        <svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="0 -40 512 512" width="512">
                                            <g>
                                                <path d="m87.628906 186.949219c21.191406 0 40.4375-10.488281 51.933594-27.535157 5.589844 5.332032 13.039062 8.730469 21.257812 9.117188 15.402344 32.707031 48 53.550781 84.503907 53.550781h22.886719c36.5 0 69.113281-20.855469 84.511718-53.574219 8.089844-.496093 15.394532-3.941406 20.855469-9.269531 11.46875 17.164063 30.738281 27.710938 52.066406 27.710938 6.78125 0 12.296875-5.515625 12.296875-12.296875v-84.429688c0-28.703125-23.347656-52.050781-52.050781-52.050781h-18.019531c-3.054688-7.175781-10.175782-12.21875-18.453125-12.21875h-3.621094c-3.632813 0-7.152344 1.011719-10.203125 2.820313-17.019531-17.722657-40.933594-28.773438-67.382812-28.773438h-22.886719c-26.5 0-50.453125 11.089844-67.476563 28.867188-3.082031-1.867188-6.660156-2.914063-10.375-2.914063h-3.617187c-8.277344 0-15.398438 5.042969-18.453125 12.21875h-18.019532c-28.699218 0-52.050781 23.351563-52.050781 52.050781v84.429688c-.003906 6.78125 5.515625 12.296875 12.296875 12.296875zm263.074219-33.542969c-.5 0-1.066406-.050781-1.785156-.152344-3.457031-.507812-6.804688 1.429688-8.109375 4.664063-12.019532 29.769531-40.515625 49.003906-72.601563 49.003906h-22.886719c-32.117187 0-60.625-19.261719-72.625-49.074219-1.171874-2.910156-3.980468-4.75-7.027343-4.75-.445313 0-.898438.039063-1.347657.117188-.703124.128906-1.335937.191406-1.929687.191406-9.898437 0-17.945313-8.050781-17.945313-17.945312v-8.472657c0-5.949219 4.839844-10.789062 10.789063-10.789062h4.226563c4.1875 0 7.582031-3.390625 7.582031-7.582031v-.867188c14.789062-18.765625 37.605469-29.898438 61.523437-29.898438h56.394532c23.921874 0 46.734374 11.132813 61.527343 29.898438v.867188c0 4.1875 3.394531 7.582031 7.582031 7.582031h1.882813c6.820313 0 12.367187 5.546875 12.367187 12.371093v7.214844c.003907 9.714844-7.902343 17.621094-17.617187 17.621094zm72.078125-63.183594v81.480469c-17.617188-1.035156-33.023438-11.585937-40.359375-27.648437.6875-2.644532 1.0625-5.414063 1.0625-8.269532v-7.21875c0-6.09375-2.011719-11.722656-5.378906-16.289062 0 0-.085938-14.117188-.261719-15.855469-.414062-4.164063-4.136719-7.210937-8.296875-6.785156-4.164063.414062-7.203125 4.128906-6.789063 8.296875.125 1.234375.1875 2.496094.1875 3.742187v.289063c-.429687-.113282-.855468-.242188-1.292968-.332032v-8.195312c0-4.871094-.378906-9.65625-1.097656-14.328125 5.410156-3.648437 8.914062-9.828125 8.914062-16.636719v-9.144531h16.421875c20.339844 0 36.890625 16.550781 36.890625 36.894531zm-76.984375-49.109375h3.621094c2.695312 0 4.886719 2.191407 4.886719 4.882813v11.128906c-2.386719-5.636719-5.3125-10.992188-8.707032-16 .066406-.003906.132813-.011719.199219-.011719zm-100.472656-25.953125h22.886719c40.488281 0 73.894531 30.898438 77.878906 70.355469-16.835938-14.523437-38.566406-22.824219-61.125-22.824219h-56.394532c-22.5625 0-44.292968 8.300782-61.128906 22.824219 3.984375-39.457031 37.390625-70.355469 77.882813-70.355469zm-81.472657 25.953125h3.621094c.152344 0 .300782.015625.449219.027344-3.519531 5.191406-6.523437 10.757813-8.953125 16.617187v-11.761718c0-2.691406 2.191406-4.882813 4.882812-4.882813zm-73.359374 49.109375c0-20.34375 16.546874-36.894531 36.890624-36.894531h16.425782v9.144531c0 6.957032 3.59375 13.199219 9.144531 16.804688-.703125 4.621094-1.070313 9.347656-1.070313 14.160156v7.816406c-.523437.066406-1.039062.15625-1.550781.253906.003907-1.191406.0625-2.382812.179688-3.554687.417969-4.164063-2.621094-7.878906-6.785157-8.296875-4.164062-.417969-7.882812 2.621094-8.300781 6.785156-.171875 1.726563-.261719 14.117188-.261719 14.117188-3.671874 4.476562-5.878906 10.199218-5.878906 16.429687v8.472657c0 3.179687.457032 6.253906 1.300782 9.167968-7.449219 15.722656-22.734376 26.046875-40.09375 27.074219zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m231.527344 120.242188c0 6.230468-5.046875 11.277343-11.277344 11.277343-6.226562 0-11.277344-5.046875-11.277344-11.277343 0-6.226563 5.050782-11.277344 11.277344-11.277344 6.230469 0 11.277344 5.050781 11.277344 11.277344zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m304.558594 120.242188c0 6.230468-5.050782 11.277343-11.277344 11.277343-6.230469 0-11.277344-5.046875-11.277344-11.277343 0-6.226563 5.046875-11.277344 11.277344-11.277344 6.226562 0 11.277344 5.050781 11.277344 11.277344zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m290.800781 139.957031h-68.066406c-3.214844 0-6.289063 1.371094-8.4375 3.769531-2.152344 2.394532-3.183594 5.605469-2.832031 8.804688 2.527344 23.136719 22.003906 40.578125 45.300781 40.578125 23.300781 0 42.777344-17.441406 45.304687-40.578125.347657-3.199219-.683593-6.410156-2.832031-8.808594-2.148437-2.394531-5.226562-3.765625-8.4375-3.765625zm-34.035156 37.992188c-14.089844 0-26.085937-9.554688-29.464844-22.832031h58.933594c-3.378906 13.277343-15.375 22.832031-29.46875 22.832031zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m490.164062 312.335938v-8.195313c0-51.523437-41.914062-93.441406-93.4375-93.441406h-22.886718c-51.523438 0-93.441406 41.917969-93.441406 93.441406v7.8125c-12.730469 1.652344-22.597657 12.5625-22.597657 25.738281v8.46875c0 17.730469 14.007813 32.246094 31.539063 33.070313 15.398437 32.707031 47.996094 53.550781 84.5 53.550781h22.886718c36.5 0 69.113282-20.855469 84.511719-53.574219 17.140625-1.046875 30.761719-15.320312 30.761719-32.71875v-7.21875c0-13.226562-9.378906-24.304687-21.835938-26.933593zm-116.324218-86.476563h22.886718c40.488282 0 73.894532 30.902344 77.878907 70.359375-16.835938-14.527344-38.566407-22.828125-61.125-22.828125h-56.394531c-22.558594 0-44.289063 8.300781-61.125 22.828125 3.980468-39.457031 37.386718-70.359375 77.878906-70.359375zm123 120.628906c0 9.714844-7.902344 17.617188-17.621094 17.617188-.5 0-1.066406-.046875-1.785156-.148438-3.457032-.507812-6.800782 1.425781-8.109375 4.664063-12.019531 29.769531-40.515625 49-72.597657 49h-22.886718c-32.121094 0-60.628906-19.261719-72.628906-49.070313-1.171876-2.910156-3.980469-4.753906-7.027344-4.753906-.445313 0-.898438.039063-1.347656.121094-.703126.128906-1.332032.1875-1.929688.1875-9.894531 0-17.945312-8.046875-17.945312-17.945313v-8.46875c0-5.949218 4.839843-10.789062 10.789062-10.789062h4.226562c4.1875 0 7.582032-3.394532 7.582032-7.582032v-.871093c14.792968-18.761719 37.605468-29.894531 61.527344-29.894531h56.390624c23.921876 0 46.734376 11.132812 61.527344 29.894531v.871093c0 4.1875 3.394532 7.582032 7.582032 7.582032h1.882812c6.820312 0 12.371094 5.546875 12.371094 12.367187zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m360.046875 330.945312c0 6.230469-5.050781 11.277344-11.277344 11.277344-6.230469 0-11.28125-5.046875-11.28125-11.277344 0-6.226562 5.050781-11.277343 11.28125-11.277343 6.226563 0 11.277344 5.050781 11.277344 11.277343zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m433.074219 330.945312c0 6.230469-5.046875 11.277344-11.277344 11.277344s-11.277344-5.046875-11.277344-11.277344c0-6.226562 5.046875-11.277343 11.277344-11.277343s11.277344 5.050781 11.277344 11.277343zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m419.316406 350.65625h-68.066406c-3.214844 0-6.289062 1.375-8.4375 3.769531-2.148438 2.398438-3.183594 5.609375-2.832031 8.808594 2.527343 23.132813 22 40.578125 45.300781 40.578125s42.777344-17.445312 45.304688-40.574219c.351562-3.203125-.679688-6.414062-2.828126-8.808593-2.152343-2.398438-5.226562-3.773438-8.441406-3.773438zm-34.035156 37.996094c-14.09375 0-26.089844-9.558594-29.464844-22.832032h58.933594c-3.378906 13.273438-15.375 22.832032-29.46875 22.832032zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m232.367188 312.332031v-8.191406c0-51.523437-41.917969-93.441406-93.441407-93.441406h-22.886719c-51.523437 0-93.441406 41.917969-93.441406 93.441406v7.816406c-12.730468 1.648438-22.597656 12.5625-22.597656 25.734375v8.46875c0 17.730469 14.007812 32.25 31.539062 33.070313 15.398438 32.710937 47.996094 53.554687 84.5 53.554687h22.886719c36.503907 0 69.113281-20.859375 84.511719-53.578125 17.136719-1.046875 30.761719-15.324219 30.761719-32.71875v-7.21875c0-13.226562-9.378907-24.304687-21.832031-26.9375zm6.671874 34.15625c0 9.714844-7.90625 17.617188-17.621093 17.617188-.5 0-1.066407-.046875-1.785157-.148438-3.449218-.507812-6.804687 1.425781-8.109374 4.664063-12.019532 29.769531-40.515626 49-72.601563 49h-22.886719c-32.117187 0-60.625-19.261719-72.625-49.070313-1.171875-2.910156-3.980468-4.753906-7.027344-4.753906-.445312 0-.894531.039063-1.347656.121094-.703125.128906-1.335937.1875-1.929687.1875-9.894531 0-17.945313-8.046875-17.945313-17.945313v-8.46875c0-5.949218 4.839844-10.789062 10.789063-10.789062h4.226562c4.1875 0 7.582031-3.394532 7.582031-7.582032v-.871093c14.792969-18.761719 37.605469-29.894531 61.527344-29.894531h56.394532c5.152343 0 10.300781.5 15.304687 1.492187 4.105469.816406 8.09375-1.855469 8.910156-5.960937.8125-4.105469-1.855469-8.097657-5.964843-8.910157-5.96875-1.183593-12.113282-1.785156-18.25-1.785156h-56.394532c-22.558594 0-44.289062 8.300781-61.128906 22.828125 3.984375-39.457031 37.390625-70.359375 77.882812-70.359375h22.886719c40.476563 0 73.875 30.882813 77.875 70.320313-3.492187-3.023438-7.203125-5.800782-11.136719-8.300782-3.535156-2.242187-8.21875-1.195312-10.464843 2.339844-2.242188 3.535156-1.195313 8.21875 2.339843 10.460938 7.550782 4.792968 14.144532 10.78125 19.664063 17.796874v.84375c0 4.1875 3.394531 7.578126 7.582031 7.578126h1.882813c6.820312 0 12.371093 5.550781 12.371093 12.371093zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m102.246094 330.945312c0 6.230469-5.050782 11.277344-11.277344 11.277344-6.230469 0-11.28125-5.046875-11.28125-11.277344 0-6.226562 5.050781-11.277343 11.28125-11.277343 6.226562 0 11.277344 5.050781 11.277344 11.277343zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m175.273438 330.945312c0 6.230469-5.046876 11.277344-11.277344 11.277344-6.226563 0-11.277344-5.046875-11.277344-11.277344 0-6.226562 5.050781-11.277343 11.277344-11.277343 6.230468 0 11.277344 5.050781 11.277344 11.277343zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                                <path d="m161.515625 350.65625h-68.066406c-3.214844 0-6.289063 1.375-8.4375 3.769531-2.152344 2.398438-3.183594 5.609375-2.832031 8.808594 2.527343 23.132813 22.003906 40.578125 45.300781 40.578125 23.300781 0 42.777343-17.445312 45.304687-40.578125.347656-3.199219-.683594-6.410156-2.832031-8.808594-2.148437-2.394531-5.222656-3.769531-8.4375-3.769531zm-34.035156 37.996094c-14.089844 0-26.085938-9.558594-29.464844-22.832032h58.933594c-3.378907 13.277344-15.375 22.832032-29.46875 22.832032zm0 0" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Iron')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="158.326" height="76.367" viewBox="0 0 158.326 76.367">
                                            <g id="iron_pink" data-name="iron pink" transform="translate(0 -81.818)">
                                                <g id="Group_473" data-name="Group 473" transform="translate(0 81.818)">
                                                    <path id="Path_306" data-name="Path 306" d="M89.481,108.863v-4.587H83.125v8.649C85.245,111.479,87.364,110.138,89.481,108.863Z" transform="translate(-41.491 -93.028)" fill="" />
                                                    <path id="Path_307" data-name="Path 307" d="M169.938,153.779c-2.463-3.533-7.459-5.991-13.253-6.075-.1,0-.2,0-.3,0-5.666,0-10.6,2.294-13.12,5.694a2.652,2.652,0,0,0-.163,3.116,4.574,4.574,0,0,0,3.83,2.016l19.195.275h.074c2.773,0,5.062-2.239,3.983-4.528A3.222,3.222,0,0,0,169.938,153.779Z" transform="translate(-71.212 -114.703)" fill="" />
                                                    <path id="Path_308" data-name="Path 308" d="M138.82,91.816a6.01,6.01,0,0,0-1.081-1.136,5.6,5.6,0,0,0-1.362-.814,6.019,6.019,0,0,0-1.362-.406v-4.5a1.9,1.9,0,0,0-1.425-1.76,106.971,106.971,0,0,0-17.653-1.378A126.379,126.379,0,0,0,52.16,99.2c-.452.267-.907.556-1.362.828s-.907.55-1.362.831q-1.847,1.144-3.7,2.381c-.673.447-1.343.9-2.016,1.362-.638.441-1.275.9-1.912,1.362-13.42,9.666-26.773,23.033-38.861,41.509l-.049.079c.03,0,.063,0,.1,0H120.456a1.6,1.6,0,0,0,1.463-1.438c.621-4.029,2.945-11.139,11.551-13.431a2.053,2.053,0,0,0,1.545-1.779v-28.68a6.317,6.317,0,0,0,1.362-.4,5.765,5.765,0,0,0,1.362-.793,5.871,5.871,0,0,0,.886-.9c12.259,2.231,12.343,19.674,12.967,30.157.314,5.236,8.486,5.266,8.173,0C158.856,115.02,156.857,93.633,138.82,91.816Zm-20.064,22.276c-.036,6.71-6.328,6.841-15.5,7.184a5.239,5.239,0,0,1-.417,3.765,7.224,7.224,0,0,1-6.514,3.6h0l-19.195-.275a7.19,7.19,0,0,1-6.391-3.754,5.41,5.41,0,0,1-.354-1.062c-9.494-.425-16.533-2.626-13.6-7.584,5.732-9.712,15.637-15.708,31.266-18.724C118.876,91.293,118.876,91.293,118.756,114.091Z" transform="translate(-1.447 -81.818)" fill="" />
                                                    <rect id="Rectangle_14" data-name="Rectangle 14" width="126.022" height="6.557" transform="translate(0 69.81)" fill="" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Satellite Cable')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="145.424" height="154.956" viewBox="0 0 145.424 154.956">
                                            <g id="in-unit_washer_dryer_pink" data-name="in-unit washer dryer pink" transform="translate(-1451.941 -432.782)">
                                                <path id="Path_220" data-name="Path 220" d="M1519.812,552.176v72.259h-67.439V552.176Zm-14.442,43.7a19.36,19.36,0,1,0-19.669,19.022A19.418,19.418,0,0,0,1505.37,595.872Zm-39.012-27.052a4.777,4.777,0,1,0-4.515-5.141A4.858,4.858,0,0,0,1466.357,568.821Zm17.242,0a4.754,4.754,0,1,0-.167-9.505,4.585,4.585,0,0,0-4.573,4.635A4.741,4.741,0,0,0,1483.6,568.824Z" transform="translate(-0.133 -36.738)" fill="" />
                                                <path id="Path_221" data-name="Path 221" d="M1631.661,624.488h-67.434V552.217h67.434Zm-33.989-9.564a19.368,19.368,0,1,0-19.063-19.667A19.429,19.429,0,0,0,1597.673,614.924Zm-19.123-55.635a4.775,4.775,0,1,0,4.852,4.814A4.875,4.875,0,0,0,1578.55,559.289ZM1600.3,564a4.759,4.759,0,1,0-4.416,4.8A4.641,4.641,0,0,0,1600.3,564Z" transform="translate(-34.551 -36.75)" fill="" />
                                                <path id="Path_222" data-name="Path 222" d="M1519.628,505.372h-67.505c-.064-1.259-.172-2.374-.172-3.489q-.014-26.622,0-53.243c.007-9.731,6.014-15.807,15.679-15.827,12.447-.026,24.894-.059,37.34.014,8.485.049,14.766,6.137,14.812,14.554.1,18.784.033,37.57.026,56.354C1519.8,504.178,1519.712,504.622,1519.628,505.372Zm-14.392-28.694a19.359,19.359,0,1,0-19.6,19.1A19.419,19.419,0,0,0,1505.236,476.677ZM1471.312,445a4.765,4.765,0,0,0-4.818-4.83,4.566,4.566,0,0,0-4.762,4.5,4.752,4.752,0,0,0,4.607,5.024A4.852,4.852,0,0,0,1471.312,445Zm16.921.014a4.732,4.732,0,0,0-4.777-4.832,4.6,4.6,0,0,0-4.724,4.481,4.755,4.755,0,1,0,9.5.352Z" fill="" />
                                                <path id="Path_223" data-name="Path 223" d="M1631.817,505.379h-67.7c-.062-.914-.16-1.687-.16-2.459q-.019-26.8-.007-53.593c.022-10.879,5.773-16.509,16.729-16.513q17.634-.006,35.268,0c9.82.017,15.84,5.919,15.859,15.628q.052,26.969.01,53.938Zm-33.724-48.313a19.369,19.369,0,1,0,19.158,19.571A19.465,19.465,0,0,0,1598.093,457.066Zm-14.785-12.042a4.831,4.831,0,0,0-4.8-4.859,4.773,4.773,0,1,0,4.8,4.859Zm16.891-.154a4.754,4.754,0,1,0-4.409,4.795A4.609,4.609,0,0,0,1600.2,444.871Z" transform="translate(-34.461 -0.008)" fill="" />
                                                <path id="Path_224" data-name="Path 224" d="M1506.347,610.43a9.692,9.692,0,1,1-9.764-9.62A9.755,9.755,0,0,1,1506.347,610.43Z" transform="translate(-10.776 -51.703)" fill="" />
                                                <path id="Path_225" data-name="Path 225" d="M1608.776,600.837a9.684,9.684,0,1,1-9.786,9.585A9.736,9.736,0,0,1,1608.776,600.837Z" transform="translate(-45.247 -51.711)" fill="" />
                                                <path id="Path_226" data-name="Path 226" d="M1506.347,491.4a9.692,9.692,0,1,1-9.792-9.589A9.756,9.756,0,0,1,1506.347,491.4Z" transform="translate(-10.776 -15.086)" fill="" />
                                                <path id="Path_227" data-name="Path 227" d="M1618.355,491.488a9.682,9.682,0,1,1-9.709-9.65A9.76,9.76,0,0,1,1618.355,491.488Z" transform="translate(-45.248 -15.095)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'In-Unit Washer/Dryer')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="145.424" height="154.956" viewBox="0 0 145.424 154.956">
                                            <g id="in-unit_washer_dryer_pink" data-name="in-unit washer dryer pink" transform="translate(-1451.941 -432.782)">
                                                <path id="Path_220" data-name="Path 220" d="M1519.812,552.176v72.259h-67.439V552.176Zm-14.442,43.7a19.36,19.36,0,1,0-19.669,19.022A19.418,19.418,0,0,0,1505.37,595.872Zm-39.012-27.052a4.777,4.777,0,1,0-4.515-5.141A4.858,4.858,0,0,0,1466.357,568.821Zm17.242,0a4.754,4.754,0,1,0-.167-9.505,4.585,4.585,0,0,0-4.573,4.635A4.741,4.741,0,0,0,1483.6,568.824Z" transform="translate(-0.133 -36.738)" fill="" />
                                                <path id="Path_221" data-name="Path 221" d="M1631.661,624.488h-67.434V552.217h67.434Zm-33.989-9.564a19.368,19.368,0,1,0-19.063-19.667A19.429,19.429,0,0,0,1597.673,614.924Zm-19.123-55.635a4.775,4.775,0,1,0,4.852,4.814A4.875,4.875,0,0,0,1578.55,559.289ZM1600.3,564a4.759,4.759,0,1,0-4.416,4.8A4.641,4.641,0,0,0,1600.3,564Z" transform="translate(-34.551 -36.75)" fill="" />
                                                <path id="Path_222" data-name="Path 222" d="M1519.628,505.372h-67.505c-.064-1.259-.172-2.374-.172-3.489q-.014-26.622,0-53.243c.007-9.731,6.014-15.807,15.679-15.827,12.447-.026,24.894-.059,37.34.014,8.485.049,14.766,6.137,14.812,14.554.1,18.784.033,37.57.026,56.354C1519.8,504.178,1519.712,504.622,1519.628,505.372Zm-14.392-28.694a19.359,19.359,0,1,0-19.6,19.1A19.419,19.419,0,0,0,1505.236,476.677ZM1471.312,445a4.765,4.765,0,0,0-4.818-4.83,4.566,4.566,0,0,0-4.762,4.5,4.752,4.752,0,0,0,4.607,5.024A4.852,4.852,0,0,0,1471.312,445Zm16.921.014a4.732,4.732,0,0,0-4.777-4.832,4.6,4.6,0,0,0-4.724,4.481,4.755,4.755,0,1,0,9.5.352Z" fill="" />
                                                <path id="Path_223" data-name="Path 223" d="M1631.817,505.379h-67.7c-.062-.914-.16-1.687-.16-2.459q-.019-26.8-.007-53.593c.022-10.879,5.773-16.509,16.729-16.513q17.634-.006,35.268,0c9.82.017,15.84,5.919,15.859,15.628q.052,26.969.01,53.938Zm-33.724-48.313a19.369,19.369,0,1,0,19.158,19.571A19.465,19.465,0,0,0,1598.093,457.066Zm-14.785-12.042a4.831,4.831,0,0,0-4.8-4.859,4.773,4.773,0,1,0,4.8,4.859Zm16.891-.154a4.754,4.754,0,1,0-4.409,4.795A4.609,4.609,0,0,0,1600.2,444.871Z" transform="translate(-34.461 -0.008)" fill="" />
                                                <path id="Path_224" data-name="Path 224" d="M1506.347,610.43a9.692,9.692,0,1,1-9.764-9.62A9.755,9.755,0,0,1,1506.347,610.43Z" transform="translate(-10.776 -51.703)" fill="" />
                                                <path id="Path_225" data-name="Path 225" d="M1608.776,600.837a9.684,9.684,0,1,1-9.786,9.585A9.736,9.736,0,0,1,1608.776,600.837Z" transform="translate(-45.247 -51.711)" fill="" />
                                                <path id="Path_226" data-name="Path 226" d="M1506.347,491.4a9.692,9.692,0,1,1-9.792-9.589A9.756,9.756,0,0,1,1506.347,491.4Z" transform="translate(-10.776 -15.086)" fill="" />
                                                <path id="Path_227" data-name="Path 227" d="M1618.355,491.488a9.682,9.682,0,1,1-9.709-9.65A9.76,9.76,0,0,1,1618.355,491.488Z" transform="translate(-45.248 -15.095)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Free Parking')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="139.246" height="101.948" viewBox="0 0 139.246 101.948">
                                            <path id="free_parking_pink" data-name="free parking pink" d="M161.3,191.569h6.244l1.21,4.973H161.3Zm-4.982-49.748a14.911,14.911,0,1,1,14.91-14.91A14.911,14.911,0,0,1,156.318,141.82Zm0-4.973a9.937,9.937,0,1,0-9.937-9.937A9.937,9.937,0,0,0,156.318,136.847Zm4.964-9.937a4.964,4.964,0,1,0-4.964,4.964A4.964,4.964,0,0,0,161.282,126.91Zm-28.232,25.2a13.362,13.362,0,0,0-12.407-7.79H99.137V169.19h41.028Zm-56.5,49.4a14.754,14.754,0,0,1,0,4.973h45.18a14.754,14.754,0,0,1,0-4.973Zm-14.708-7.46A9.946,9.946,0,1,0,71.785,204a9.946,9.946,0,0,0-9.946-9.946Zm74.6,0A9.946,9.946,0,1,0,146.381,204,9.946,9.946,0,0,0,136.435,194.055ZM32,201.515v4.973H47.13a14.754,14.754,0,0,1,0-4.973ZM151.354,204a14.973,14.973,0,0,1-.211,2.487h20.1v-4.973h-20.1a14.972,14.972,0,0,1,.211,2.487Zm4.973-7.46h-6.98a14.905,14.905,0,0,0-25.825,0H74.75a14.9,14.9,0,0,0-25.824,0H33.541l1.048-6.9c0-.022.006-.043.009-.065,1.135-9.077,8.9-15.417,18.873-15.417H146a24.592,24.592,0,0,1,9.383,1.784,19.322,19.322,0,0,1,10.77,10.649h-7.339a2.487,2.487,0,0,0-2.487,2.487Zm-47.244-14.919a2.487,2.487,0,0,0-2.487-2.487h-4.973a2.487,2.487,0,1,0,0,4.973H106.6A2.487,2.487,0,0,0,109.083,181.622ZM94.163,169.19V144.324H79.253c-8.135,0-15.577,4.371-18.96,11.137L53.429,169.19H94.163Zm63.107,2.156c.527.218,1.038.453,1.543.7V146.811H153.84V170.2A28.068,28.068,0,0,1,157.271,171.346Z" transform="translate(-32 -111.999)" fill="" />
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Gated Access')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="159.996" height="137.809" viewBox="0 0 159.996 137.809">
                                            <g id="gated_access_pink" data-name="gated access pink" transform="translate(0 -35.5)">
                                                <g id="Group_422" data-name="Group 422" transform="translate(0 35.5)">
                                                    <g id="Group_421" data-name="Group 421">
                                                        <path id="Path_194" data-name="Path 194" d="M0,35.5V173.309H22.187V93.624A57.471,57.471,0,0,1,31.562,62.1V35.5Z" transform="translate(0 -35.5)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_424" data-name="Group 424" transform="translate(84.686 45.415)">
                                                    <g id="Group_423" data-name="Group 423">
                                                        <path id="Path_195" data-name="Path 195" d="M283.5,118.438V70.154A48.048,48.048,0,0,0,271,67.228V195.123h12.5V141.81a14.05,14.05,0,0,1,0-23.372Z" transform="translate(-271 -67.228)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_426" data-name="Group 426" transform="translate(106.56 53.141)">
                                                    <g id="Group_425" data-name="Group 425" transform="translate(0 0)">
                                                        <path id="Path_196" data-name="Path 196" d="M341,91.953v41.2a14.06,14.06,0,0,1,0,27.947v51.025h21.875V132.436A48.431,48.431,0,0,0,341,91.953Z" transform="translate(-341 -91.953)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_428" data-name="Group 428" transform="translate(100.31 103.623)">
                                                    <g id="Group_427" data-name="Group 427">
                                                        <path id="Path_197" data-name="Path 197" d="M325.687,253.5a4.687,4.687,0,1,0,4.687,4.687A4.693,4.693,0,0,0,325.687,253.5Z" transform="translate(-321 -253.5)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_430" data-name="Group 430" transform="translate(31.562 53.141)">
                                                    <g id="Group_429" data-name="Group 429" transform="translate(0 0)">
                                                        <path id="Path_198" data-name="Path 198" d="M101,132.436v79.686h21.874V91.953A48.431,48.431,0,0,0,101,132.436Z" transform="translate(-101 -91.953)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_432" data-name="Group 432" transform="translate(62.811 45.415)">
                                                    <g id="Group_431" data-name="Group 431" transform="translate(0 0)">
                                                        <path id="Path_199" data-name="Path 199" d="M201,70.154V195.123h12.5V67.228A48.048,48.048,0,0,0,201,70.154Z" transform="translate(-201 -67.228)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_434" data-name="Group 434" transform="translate(128.435 35.5)">
                                                    <g id="Group_433" data-name="Group 433">
                                                        <path id="Path_200" data-name="Path 200" d="M411,35.5V62.1a57.471,57.471,0,0,1,9.375,31.523v79.686h22.187V35.5Z" transform="translate(-411 -35.5)" fill="" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Fire Extinguisher')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="93.678" height="124.511" viewBox="0 0 93.678 124.511">
                                            <g id="fire_extinguisher_pink" data-name="fire extinguisher pink" transform="translate(-53.488)">
                                                <g id="Group_477" data-name="Group 477" transform="translate(53.488)">
                                                    <g id="Group_476" data-name="Group 476">
                                                        <path id="Path_341" data-name="Path 341" d="M119.428,3.468v.051a10.368,10.368,0,0,0-8.038,5.7h-9.006A11.453,11.453,0,0,0,84.975,4.612H54.936v9.223H72.93a41.617,41.617,0,0,0-19.442,21.01l8.527,3.532A32.44,32.44,0,0,1,82.093,19.954a11.548,11.548,0,0,0,5.123,4.436v3.745A23.06,23.06,0,0,0,68.771,50.727v64.561a9.22,9.22,0,0,0,9.223,9.223h27.669a9.22,9.22,0,0,0,9.223-9.223V50.727A23.06,23.06,0,0,0,96.44,28.135V24.39a11.544,11.544,0,0,0,5.944-5.944h9.011a10.368,10.368,0,0,0,8.033,5.7v.06l27.738,3.468V0Zm-27.6,111.82H77.994V50.727H91.828Zm4.893-96.56a6.917,6.917,0,1,1-.083-9.693A6.863,6.863,0,0,1,96.721,18.727Z" transform="translate(-53.488)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_479" data-name="Group 479" transform="translate(88.563 10.574)">
                                                    <g id="Group_478" data-name="Group 478" transform="translate(0)">
                                                        <path id="Path_342" data-name="Path 342" d="M179.98,38.722a1.743,1.743,0,0,0-.406-.314l-4.39-1.72,1.72,4.386a1.9,1.9,0,0,0,.314.41,1.953,1.953,0,0,0,2.762-2.762Z" transform="translate(-175.184 -36.688)" fill="" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Dishwasher')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="126.847" height="147.842" viewBox="0 0 126.847 147.842">
                                            <g id="dishwasher_pink" data-name="dishwasher pink" transform="translate(-1532.107 -49.316)">
                                                <path id="Path_245" data-name="Path 245" d="M1532.179,210.762V105.248h126.727V210.762Zm10.609-95.007v36c8.56-19.664,21.139-28.173,36.771-25.068,15.389,3.056,26.258,15.313,26.659,30.064a31.831,31.831,0,0,1-24.156,32.122,30.993,30.993,0,0,1-24.675-4.013c-7.586-4.718-12.126-11.746-14.553-20.228v35.545h105.44V115.755Zm54.2,42.263a22.568,22.568,0,1,0-22.365,22.587A22.848,22.848,0,0,0,1596.984,158.018Z" transform="translate(-0.017 -13.604)" fill="" />
                                                <path id="Path_246" data-name="Path 246" d="M1532.107,87.276V49.316h126.847v37.96Zm116.3-22.069h-31.286V75.292h31.443c0-2.876.009-5.5-.006-8.132C1648.558,66.558,1648.47,65.955,1648.41,65.207Zm-88.591,5.07a6.852,6.852,0,0,0-6.775-6.671,6.632,6.632,0,1,0,6.775,6.671Zm21.225.067A6.647,6.647,0,1,0,1574.5,76.9,6.906,6.906,0,0,0,1581.044,70.344Zm14.665-6.708a6.63,6.63,0,1,0,6.531,6.913A6.925,6.925,0,0,0,1595.709,63.636Z" transform="translate(0)" fill="" />
                                                <path id="Path_247" data-name="Path 247" d="M1658.02,133.274c0,7.858.4,15.427-.089,22.939-.819,12.7-13.43,21.668-25.528,18.738-2.408-.583-3.247-1.365-2.814-4.067,2.092-13.058-1.886-24.072-11.642-33.041-1.135-1.044-1.829-2.566-3.213-4.569Z" transform="translate(-20.097 -20.421)" fill="" />
                                                <path id="Path_248" data-name="Path 248" d="M1618.046,213.341V209.46h17.538V194.393h4.094v14.912h17.658v4.036Z" transform="translate(-20.903 -35.287)" fill="" />
                                                <path id="Path_249" data-name="Path 249" d="M1582,150.523a18.53,18.53,0,1,1-18.5,18.558A18.443,18.443,0,0,1,1582,150.523Z" transform="translate(-7.635 -24.616)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Covered Parking')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="136.198" height="117.888" viewBox="0 0 136.198 117.888">
                                            <g id="covered_parking_pink" data-name="covered parking pink" transform="translate(0 -19.038)">
                                                <g id="Group_499" data-name="Group 499" transform="translate(0 19.038)">
                                                    <path id="Path_394" data-name="Path 394" d="M10.432,67.486,66.644,33.91,125.9,67.562a6.894,6.894,0,0,0,6.809-11.989L69.96,19.937a6.9,6.9,0,0,0-6.941.076L3.36,55.648a6.895,6.895,0,1,0,7.072,11.838Z" transform="translate(0 -19.038)" fill="" />
                                                    <path id="Path_395" data-name="Path 395" d="M132.777,148.428a20.693,20.693,0,0,0-4.318.366v13.742a12.529,12.529,0,0,0,3.649.366c5.594,0,9-2.8,9-7.541C141.107,150.8,137.943,148.428,132.777,148.428Z" transform="translate(-66.685 -86.206)" fill="" />
                                                    <path id="Path_396" data-name="Path 396" d="M97.569,94.514a40.8,40.8,0,1,0,40.8,40.795A40.8,40.8,0,0,0,97.569,94.514Zm10.277,41.8c-3.039,2.981-7.659,4.439-12.952,4.439a16.521,16.521,0,0,1-3.649-.3v15.509H83.824V115.515a65.84,65.84,0,0,1,11.438-.853c5.467,0,9.424,1.155,12.038,3.343a11.6,11.6,0,0,1,4.013,9.063A12.456,12.456,0,0,1,107.846,136.314Z" transform="translate(-29.471 -58.218)" fill="" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Balcony')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="111.746" height="98.184" viewBox="0 0 111.746 98.184">
                                            <g id="balcony_pink" data-name="balcony pink" transform="translate(0 -31.069)">
                                                <g id="Group_456" data-name="Group 456" transform="translate(26.426 64.32)">
                                                    <g id="Group_455" data-name="Group 455" transform="translate(0 0)">
                                                        <rect id="Rectangle_12" data-name="Rectangle 12" width="24.916" height="19.631" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_458" data-name="Group 458" transform="translate(26.837 31.069)">
                                                    <g id="Group_457" data-name="Group 457">
                                                        <path id="Path_256" data-name="Path 256" d="M122.964,55.259h24.505V31.069A29.519,29.519,0,0,0,122.964,55.259Z" transform="translate(-122.964 -31.069)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_460" data-name="Group 460" transform="translate(60.403 31.069)">
                                                    <g id="Group_459" data-name="Group 459">
                                                        <path id="Path_257" data-name="Path 257" d="M276.757,31.069v24.19h24.505A29.519,29.519,0,0,0,276.757,31.069Z" transform="translate(-276.757 -31.069)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_462" data-name="Group 462" transform="translate(60.403 64.32)">
                                                    <g id="Group_461" data-name="Group 461" transform="translate(0 0)">
                                                        <rect id="Rectangle_13" data-name="Rectangle 13" width="24.916" height="19.631" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_464" data-name="Group 464" transform="translate(0 93.011)">
                                                    <g id="Group_463" data-name="Group 463">
                                                        <path id="Path_258" data-name="Path 258" d="M107.215,314.877H4.53a4.53,4.53,0,1,0,0,9.061H6.04v22.651a4.531,4.531,0,0,0,4.53,4.53h90.6a4.53,4.53,0,0,0,4.53-4.53V323.938h1.51a4.53,4.53,0,1,0,0-9.061ZM24.161,327.335v14.723H15.1V323.938h9.06v3.4Zm18.121,0v14.723h-9.06V323.937h9.06Zm18.121,0v14.723H51.343V323.937H60.4Zm18.121,0v14.723H69.463V323.937h9.061Zm18.121,14.723H87.584V323.937h9.06v18.121Z" transform="translate(0 -314.877)" fill="" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'POOL')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="28.95" viewBox="0 0 32 28.95">
                                            <g id="Group_131" data-name="Group 131" transform="translate(-9840.5 -2363.666)">
                                                <g id="Group_57" data-name="Group 57">
                                                    <g id="Group_56" data-name="Group 56" transform="translate(5172.105 1052.04)">
                                                        <path id="Path_75" data-name="Path 75" d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z" transform="translate(-0.022)" fill="" />
                                                        <path id="Path_76" data-name="Path 76" d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z" transform="translate(0 -469.471)" fill="" />
                                                        <path id="Path_77" data-name="Path 77" d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z" transform="translate(-0.042 -577.79)" fill="" />
                                                    </g>
                                                </g>
                                                <g id="Group_58" data-name="Group 58">
                                                    <g id="Group_56-2" data-name="Group 56" transform="translate(5172.105 1052.04)">
                                                        <path id="Path_75-2" data-name="Path 75" d="M4690.573,1328.565c.316-.164.594-.311.874-.453a4.384,4.384,0,0,1,4.2.02c.436.225.863.469,1.3.692a2.37,2.37,0,0,0,2.225.018c.233-.114.459-.242.692-.357a.349.349,0,0,1,.548.331c.007.456,0,.912,0,1.369a.409.409,0,0,1-.259.394,4.373,4.373,0,0,1-4.087.031c-.472-.231-.928-.5-1.395-.739a2.372,2.372,0,0,0-2.263-.015c-.471.235-.931.491-1.4.727a4.371,4.371,0,0,1-4.162-.018c-.437-.224-.864-.467-1.3-.693a2.382,2.382,0,0,0-2.282-.01c-.47.237-.929.494-1.4.728a4.376,4.376,0,0,1-4.162-.032c-.43-.222-.852-.461-1.281-.684a2.373,2.373,0,0,0-2.263-.023c-.508.254-1,.537-1.513.783a4.392,4.392,0,0,1-3.9.017.5.5,0,0,1-.325-.527c.014-.4,0-.8,0-1.2s.267-.555.619-.372c.162.084.321.174.482.258a2.385,2.385,0,0,0,2.3.01c.487-.248.965-.517,1.457-.755a4.353,4.353,0,0,1,4.049.04c.444.224.879.468,1.319.7.066.035.135.066.242.117v-12.11a5.049,5.049,0,0,1,4.407-5.131,7.486,7.486,0,0,1,1.386-.043.968.968,0,0,1,.928,1.025,1,1,0,0,1-1,.962,8.07,8.07,0,0,0-1.359.093,3.055,3.055,0,0,0-2.369,2.932c-.006.391,0,.783,0,1.187h7.707c0-.108,0-.218,0-.327a8.058,8.058,0,0,1,.153-2.051,5.1,5.1,0,0,1,4.923-3.833c.217,0,.436-.007.653,0a1,1,0,0,1,1,1.008,1.013,1.013,0,0,1-1.026.987,7.577,7.577,0,0,0-1.277.075,3.066,3.066,0,0,0-2.437,3.05q-.007,3.5,0,6.992v4.822Zm-9.68-8.709v3.724h7.679v-3.724Zm7.678,9.259V1325.6h-7.682v3.242c.084-.04.153-.069.22-.1.41-.214.817-.436,1.231-.642a4.374,4.374,0,0,1,4.143.017c.444.226.875.478,1.32.7C4688.039,1328.932,4688.293,1329.008,4688.571,1329.115Z" transform="translate(-0.022)" fill="" />
                                                        <path id="Path_76-2" data-name="Path 76" d="M4700.391,1803.684c0,.218,0,.435,0,.653a.417.417,0,0,1-.255.419,4.389,4.389,0,0,1-4.107.026c-.453-.223-.892-.474-1.337-.712a2.4,2.4,0,0,0-2.356-.01c-.455.236-.9.481-1.363.709a4.376,4.376,0,0,1-4.162-.031c-.43-.222-.852-.46-1.281-.683a2.373,2.373,0,0,0-2.281-.016c-.475.241-.941.5-1.419.737a4.366,4.366,0,0,1-4.105-.015c-.444-.224-.876-.472-1.318-.7a2.382,2.382,0,0,0-2.3-.013c-.487.248-.965.516-1.457.756a4.4,4.4,0,0,1-3.9.045.52.52,0,0,1-.354-.552c.017-.4,0-.8.005-1.2,0-.385.267-.541.609-.366a4.2,4.2,0,0,0,1.232.523,2.42,2.42,0,0,0,1.532-.237c.468-.241.93-.493,1.4-.729a4.375,4.375,0,0,1,4.144,0c.438.222.865.464,1.3.693a2.379,2.379,0,0,0,2.318.016c.468-.24.93-.492,1.4-.726a4.363,4.363,0,0,1,4.1.014c.445.224.877.471,1.318.7a2.372,2.372,0,0,0,2.3.015c.456-.233.906-.478,1.363-.709a4.377,4.377,0,0,1,4.2.018c.43.223.853.458,1.282.682a2.371,2.371,0,0,0,2.281.012c.2-.1.4-.208.6-.31.377-.195.609-.05.611.38C4700.391,1803.276,4700.391,1803.48,4700.391,1803.684Z" transform="translate(0 -469.471)" fill="" />
                                                        <path id="Path_77-2" data-name="Path 77" d="M4700.433,1916.776c0,.2-.011.394,0,.589a.478.478,0,0,1-.317.51,4.4,4.4,0,0,1-4.07-.019c-.458-.229-.9-.487-1.355-.721a2.371,2.371,0,0,0-2.263-.013c-.47.236-.93.491-1.4.727a4.375,4.375,0,0,1-4.162-.018c-.443-.227-.875-.475-1.318-.7a2.361,2.361,0,0,0-2.225-.019c-.478.236-.943.5-1.419.735a4.381,4.381,0,0,1-4.181-.02c-.43-.221-.851-.461-1.28-.684a2.389,2.389,0,0,0-2.3-.008c-.494.251-.977.525-1.476.765a4.388,4.388,0,0,1-3.919.01.477.477,0,0,1-.309-.5c.011-.4,0-.8,0-1.2,0-.428.256-.574.637-.391a7.113,7.113,0,0,0,1.154.491,2.152,2.152,0,0,0,1.59-.216c.46-.239.917-.485,1.38-.72a4.374,4.374,0,0,1,4.18.01c.437.224.865.464,1.3.691a2.369,2.369,0,0,0,2.281.011c.482-.244.954-.508,1.438-.745a4.363,4.363,0,0,1,4.068.019c.445.222.878.47,1.318.7a2.388,2.388,0,0,0,2.337.008c.442-.229.881-.464,1.325-.689a4.381,4.381,0,0,1,4.218.014c.417.216.828.444,1.243.664a2.392,2.392,0,0,0,2.319.024c.219-.111.434-.23.653-.339a.351.351,0,0,1,.547.334c0,.119,0,.239,0,.358Z" transform="translate(-0.042 -577.79)" fill="" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case( $amenity->masteramenity->amenity_name == 'AC' )
                                        <svg xmlns="http://www.w3.org/2000/svg" width="137.733" height="137.733" viewBox="0 0 137.733 137.733">
                                            <g id="air_conditioning_pink" data-name="air conditioning pink" transform="translate(-32 -48)">
                                                <path id="Path_228" data-name="Path 228" d="M191.908,384.682a2.46,2.46,0,0,0,3.408.7l3.32-2.193,2.554,1.476v6.026l-2.554,1.476-3.32-2.193a2.46,2.46,0,0,0-2.71,4.1l3.563,2.353.256,4.262A2.46,2.46,0,0,0,198.877,403c.05,0,.1,0,.15,0a2.46,2.46,0,0,0,2.308-2.6l-.239-3.972,2.554-1.476,5.22,3.013v2.949l-3.56,1.78a2.459,2.459,0,1,0,2.2,4.4l3.819-1.907,3.819,1.909a2.459,2.459,0,1,0,2.2-4.4l-3.56-1.78v-2.949l5.22-3.013,2.554,1.476-.239,3.972a2.46,2.46,0,0,0,2.308,2.6c.05,0,.1,0,.15,0a2.46,2.46,0,0,0,2.452-2.312l.256-4.262,3.563-2.353a2.46,2.46,0,0,0-2.71-4.1l-3.32,2.193-2.554-1.476v-6.028l2.554-1.476,3.32,2.193a2.46,2.46,0,1,0,2.71-4.1l-3.563-2.353-.256-4.262a2.459,2.459,0,1,0-4.91.295l.239,3.972L219.01,380.4l-5.22-3.013V374.44l3.56-1.78a2.459,2.459,0,1,0-2.2-4.4l-3.819,1.907-3.819-1.909a2.459,2.459,0,1,0-2.2,4.4l3.56,1.78v2.949l-5.22,3.013-2.554-1.476.239-3.972a2.459,2.459,0,1,0-4.91-.295l-.256,4.262-3.563,2.353a2.459,2.459,0,0,0-.7,3.41Zm14.2-.021,5.22-3.013,5.22,3.013v6.027l-5.22,3.013-5.22-3.013Z" transform="translate(-110.463 -221.618)" fill="" />
                                                <path id="Path_229" data-name="Path 229" d="M78.62,322.049l-1.461.843-2.337-1.543a2.46,2.46,0,0,0-2.71,4.1l2.58,1.7.184,3.086a2.459,2.459,0,0,0,2.452,2.313c.049,0,.1,0,.15,0a2.459,2.459,0,0,0,2.308-2.6l-.168-2.8,1.461-.844,3.575,2.064v1.687l-2.506,1.253a2.459,2.459,0,1,0,2.2,4.4l2.765-1.384,2.765,1.383a2.46,2.46,0,1,0,2.2-4.4l-2.5-1.251v-1.687l3.575-2.064,1.461.844-.168,2.8a2.459,2.459,0,0,0,2.308,2.6c.05,0,.1,0,.15,0a2.46,2.46,0,0,0,2.452-2.313l.184-3.086,2.58-1.7a2.46,2.46,0,0,0-2.71-4.1l-2.337,1.543-1.461-.843v-4.129l1.461-.843,2.337,1.543a2.46,2.46,0,0,0,2.71-4.1l-2.58-1.7-.184-3.086a2.459,2.459,0,1,0-4.91.295l.168,2.8-1.461.844L89.574,311.6V309.91l2.505-1.253a2.46,2.46,0,1,0-2.2-4.4l-2.765,1.383-2.765-1.383a2.459,2.459,0,1,0-2.2,4.4l2.506,1.253V311.6l-3.575,2.064-1.461-.844.168-2.8a2.459,2.459,0,1,0-4.91-.295l-.184,3.086-2.58,1.7a2.46,2.46,0,1,0,2.71,4.1l2.337-1.543,1.461.843Zm4.919-4.128,3.575-2.064,3.575,2.064v4.128l-3.573,2.064-3.575-2.064Z" transform="translate(-27.015 -177.294)" fill="" />
                                                <path id="Path_230" data-name="Path 230" d="M342.473,315.42a2.459,2.459,0,0,0-2.6,2.308l-.184,3.086-2.58,1.7a2.459,2.459,0,1,0,2.71,4.1l2.337-1.543,1.461.843v4.128l-1.461.843-2.337-1.543a2.46,2.46,0,0,0-2.71,4.1l2.58,1.7.184,3.086a2.46,2.46,0,0,0,2.452,2.313c.05,0,.1,0,.15,0a2.46,2.46,0,0,0,2.308-2.6l-.168-2.8,1.461-.844,3.575,2.064v1.687l-2.506,1.253a2.459,2.459,0,1,0,2.2,4.4l2.765-1.384,2.765,1.383a2.459,2.459,0,1,0,2.2-4.4l-2.505-1.253v-1.687l3.575-2.064,1.461.844-.168,2.8a2.46,2.46,0,0,0,2.306,2.6c.05,0,.1,0,.15,0a2.46,2.46,0,0,0,2.452-2.312l.184-3.086,2.579-1.7a2.459,2.459,0,1,0-2.71-4.1l-2.337,1.543-1.461-.844v-4.128l1.461-.844,2.337,1.543a2.46,2.46,0,0,0,2.71-4.1l-2.579-1.7-.184-3.086a2.459,2.459,0,1,0-4.91.295l.168,2.8-1.461.844-3.575-2.064v-1.689l2.505-1.253a2.459,2.459,0,0,0-2.2-4.4l-2.765,1.384-2.765-1.383a2.459,2.459,0,1,0-2.2,4.4l2.506,1.253V319.6l-3.575,2.064-1.461-.844.168-2.8A2.46,2.46,0,0,0,342.473,315.42Zm6.061,10.5,3.575-2.064,3.575,2.064v4.128l-3.575,2.064-3.575-2.064Z" transform="translate(-210.54 -182.835)" fill="" />
                                                <path id="Path_231" data-name="Path 231" d="M80,128H188.219v4.919H80Z" transform="translate(-33.243 -55.405)" fill="" />
                                                <path id="Path_232" data-name="Path 232" d="M167.273,48H34.46A2.462,2.462,0,0,0,32,50.46V84.893a2.462,2.462,0,0,0,2.46,2.46h7.379V72.6a4.925,4.925,0,0,1,4.919-4.919H154.976a4.925,4.925,0,0,1,4.919,4.919V87.352h7.379a2.462,2.462,0,0,0,2.46-2.46V50.46A2.462,2.462,0,0,0,167.273,48ZM133.58,62.757a2.46,2.46,0,1,1,2.46-2.46A2.46,2.46,0,0,1,133.58,62.757Zm9.1,0a2.46,2.46,0,1,1,2.46-2.46A2.46,2.46,0,0,1,142.678,62.757Zm9.838,0a2.46,2.46,0,1,1,2.46-2.46A2.46,2.46,0,0,1,152.516,62.757Z" fill="" />
                                                <path id="Path_233" data-name="Path 233" d="M80,160H188.219v4.919H80Z" transform="translate(-33.243 -77.567)" fill="" />
                                                <path id="Path_234" data-name="Path 234" d="M250.46,215.379a2.46,2.46,0,0,0,2.46-2.46V210.46a2.46,2.46,0,1,0-4.919,0v2.459A2.46,2.46,0,0,0,250.46,215.379Z" transform="translate(-149.593 -110.81)" fill="" />
                                                <path id="Path_235" data-name="Path 235" d="M248,257.217a2.46,2.46,0,0,0,4.919,0V242.46a2.46,2.46,0,0,0-4.919,0Z" transform="translate(-149.593 -132.972)" fill="" />
                                                <path id="Path_236" data-name="Path 236" d="M171.511,264.738a22.237,22.237,0,0,1-2.527,2.214,2.459,2.459,0,1,0,2.951,3.935,27.214,27.214,0,0,0,3.089-2.705,2.459,2.459,0,0,0-3.513-3.443Z" transform="translate(-94.188 -149.593)" fill="" />
                                                <path id="Path_237" data-name="Path 237" d="M194.8,222.552a2.457,2.457,0,0,0,3.238-1.27,26.893,26.893,0,0,0,2.259-10.822,2.46,2.46,0,1,0-4.919,0,22,22,0,0,1-1.848,8.854A2.46,2.46,0,0,0,194.8,222.552Z" transform="translate(-111.723 -110.81)" fill="" />
                                                <path id="Path_238" data-name="Path 238" d="M317.2,255.87a22.012,22.012,0,0,1-5.889-6.641,2.459,2.459,0,1,0-4.26,2.46,26.9,26.9,0,0,0,7.2,8.116,2.46,2.46,0,1,0,2.951-3.935Z" transform="translate(-190.258 -138.511)" fill="" />
                                                <path id="Path_239" data-name="Path 239" d="M300.919,210.46a2.46,2.46,0,1,0-4.919,0,27.127,27.127,0,0,0,.545,5.4,2.46,2.46,0,1,0,4.821-.978A22.183,22.183,0,0,1,300.919,210.46Z" transform="translate(-182.836 -110.81)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'GYM')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="49.305" height="20.274" viewBox="0 0 49.305 20.274">
                                            <g id="C1aRwJ.tif" transform="translate(-6073.588 -1468.754)">
                                                <g id="Group_63" data-name="Group 63" transform="translate(6073.588 1468.754)">
                                                    <path id="Path_81" data-name="Path 81" d="M6158.258,1478.9q0-4.267,0-8.533c0-1.118.458-1.581,1.563-1.594q.79-.01,1.58,0c1.128.012,1.6.48,1.605,1.628.009,1.875,0,3.75,0,5.625q0,5.626,0,11.251c0,1.3-.458,1.757-1.737,1.76-.463,0-.927,0-1.39,0-1.146-.008-1.622-.474-1.623-1.6Q6158.255,1483.169,6158.258,1478.9Z" transform="translate(-6147.528 -1468.769)" fill="" />
                                                    <path id="Path_82" data-name="Path 82" d="M6109.15,1478.859q0-4.235,0-8.471c0-1.153.454-1.6,1.6-1.612.527,0,1.054-.005,1.58,0,1.056.008,1.547.49,1.548,1.546q.007,6.321,0,12.642c0,1.5,0,2.992,0,4.488,0,1.09-.5,1.588-1.577,1.591-.548,0-1.1,0-1.644,0a1.347,1.347,0,0,1-1.513-1.522c-.008-2.887,0-5.773,0-8.66Z" transform="translate(-6104.639 -1468.771)" fill="" />
                                                    <path id="Path_83" data-name="Path 83" d="M6345.371,1478.937q0,4.2,0,8.406c0,1.233-.448,1.678-1.671,1.681-.505,0-1.011.007-1.517,0a1.353,1.353,0,0,1-1.54-1.559q-.011-5.973,0-11.946c0-1.728-.007-3.455,0-5.183.007-1.119.46-1.562,1.577-1.577.569-.007,1.138-.01,1.707,0a1.3,1.3,0,0,1,1.442,1.452C6345.377,1473.122,6345.371,1476.03,6345.371,1478.937Z" transform="translate(-6306.793 -1468.754)" fill="" />
                                                    <path id="Path_84" data-name="Path 84" d="M6389.756,1478.9q0-4.267,0-8.534c0-1.149.452-1.591,1.615-1.6.526,0,1.054-.007,1.58,0a1.358,1.358,0,0,1,1.542,1.557c.009,2.4.005,4.8.006,7.207q0,4.868,0,9.736c0,1.326-.451,1.774-1.787,1.77-.443,0-.885,0-1.328,0-1.17-.006-1.63-.47-1.631-1.66q0-4.235,0-8.471Z" transform="translate(-6349.686 -1468.763)" fill="" />
                                                    <path id="Path_85" data-name="Path 85" d="M6207.033,1539.34V1536.3c.242-.016.462-.042.683-.043,4.716,0,9.431.008,14.146-.013.546,0,.749.122.712.7-.05.791-.013,1.588-.013,2.4Z" transform="translate(-6190.122 -1527.688)" fill="" />
                                                    <path id="Path_86" data-name="Path 86" d="M6438.662,1510.6c1.2.122,1.264.192,1.259,1.339,0,.337,0,.674,0,1.011,0,1.259,0,1.259-1.258,1.469,0,.754,0,1.524,0,2.294,0,.875-.156,1.025-1.034,1.023-.861,0-1-.134-1-.981q0-4.265,0-8.529c0-.8.193-.981,1.022-.979.812,0,1.012.2,1.015.994C6438.664,1509.017,6438.662,1509.793,6438.662,1510.6Z" transform="translate(-6390.617 -1502.369)" fill="" />
                                                    <path id="Path_87" data-name="Path 87" d="M6074.844,1514.41c-1.231-.176-1.253-.2-1.255-1.359,0-.358,0-.716,0-1.074,0-1.191.048-1.246,1.255-1.38,0-.787,0-1.582,0-2.378,0-.759.216-.965.978-.974.852-.01,1.07.167,1.072.936q.012,4.326,0,8.652c0,.76-.185.908-1.026.9-.825,0-1.021-.172-1.029-.911C6074.838,1516.028,6074.844,1515.23,6074.844,1514.41Z" transform="translate(-6073.588 -1502.367)" fill="" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Fireplace')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="28.238" height="28.228" viewBox="0 0 28.238 28.228">
                                            <g id="hihsdn.tif" transform="translate(-4905.098 -480.231)">
                                                <g id="Group_66" data-name="Group 66" transform="translate(4905.098 480.231)">
                                                    <path id="Path_88" data-name="Path 88" d="M4943.436,580.045h-18.2V598.2h-4.017v-22.17h26.212V598.19h-4Z" transform="translate(-4920.204 -569.968)" fill="" />
                                                    <path id="Path_89" data-name="Path 89" d="M4905.1,485.223v-4.992h28.238v4.992Z" transform="translate(-4905.098 -480.231)" fill="" />
                                                    <path id="Path_90" data-name="Path 90" d="M5036.234,671.473a15.814,15.814,0,0,1,4.517,3.376.422.422,0,0,1,.079.3,4.719,4.719,0,0,1-1.428,2.91c-.439.423-.879.845-1.321,1.263a6.627,6.627,0,0,0-2.3,5.466,6.084,6.084,0,0,0,.7,2.4c.039.075.081.147.118.222a.3.3,0,0,1,.009.084,6.384,6.384,0,0,1-3.146-1.349,5.075,5.075,0,0,1-.132-7.947,26.851,26.851,0,0,0,2.154-2.208,3.881,3.881,0,0,0,1.047-3.041C5036.479,672.468,5036.342,671.992,5036.234,671.473Z" transform="translate(-5023.488 -659.379)" fill="" />
                                                    <path id="Path_91" data-name="Path 91" d="M5128.074,751.885a10.207,10.207,0,0,1,1.57,4.085,6.444,6.444,0,0,1-.038,2.721,5.615,5.615,0,0,1-3.853,3.872c-.266.093-.553.125-.82.216a.541.541,0,0,1-.682-.259,4.77,4.77,0,0,1,.17-6.025,9.413,9.413,0,0,1,.714-.754c.179-.178.375-.337.559-.51A7.851,7.851,0,0,0,5128.074,751.885Z" transform="translate(-5109.464 -734.705)" fill="" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Wifi')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="149.198" height="111.904" viewBox="0 0 149.198 111.904">
                                            <g id="wifi_pink" data-name="wifi pink" transform="translate(0 -63.992)">
                                                <path id="Path_397" data-name="Path 397" d="M195.973,254.143a4.334,4.334,0,0,1,.745,5.122,11.65,11.65,0,0,1-17.069,3.866,39.492,39.492,0,0,0-46.914,0,11.653,11.653,0,0,1-17.069-3.865,4.334,4.334,0,0,1,.745-5.122A55.994,55.994,0,0,1,195.973,254.143Z" transform="translate(-81.591 -122.988)" fill="" />
                                                <path id="Path_398" data-name="Path 398" d="M160.116,185.878a64.647,64.647,0,0,0-90.909,0,4.384,4.384,0,0,1-4.149,1.14,11.329,11.329,0,0,1-8.507-9.879,4.365,4.365,0,0,1,1.3-3.52,81.32,81.32,0,0,1,113.611,0,4.365,4.365,0,0,1,1.3,3.52,11.331,11.331,0,0,1-8.505,9.88A4.383,4.383,0,0,1,160.116,185.878Z" transform="translate(-40.06 -61.291)" fill="" />
                                                <path id="Path_399" data-name="Path 399" d="M145.847,106.507a13.163,13.163,0,0,1-1.174,1.031,4.341,4.341,0,0,1-5.74-.405,90.359,90.359,0,0,0-128.678.018,4.361,4.361,0,0,1-5.856.314q-.424-.347-.821-.739a11.215,11.215,0,0,1,.355-16.743,109.07,109.07,0,0,1,141.337,0,11.217,11.217,0,0,1,.577,16.525Z" transform="translate(0 0)" fill="" />
                                                <circle id="Ellipse_10" data-name="Ellipse 10" cx="11.656" cy="11.656" r="11.656" transform="translate(62.946 152.585)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'TV')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512.003 512.003">
                                            <g>
                                                <path d="M408.042,181.002H50.001c-5.523,0-10,4.477-10,10v212c0,5.523,4.477,10,10,10h163.033c5.523,0,10-4.477,10-10    s-4.477-10-10-10H60.001v-192h338.042v192H280.667c-5.523,0-10,4.477-10,10s4.477,10,10,10h127.375c5.523,0,10-4.477,10-10v-212    C418.042,185.479,413.565,181.002,408.042,181.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>

                                            <g>
                                                <path d="M502.001,136.002H302.988c-1.492-7.019-4.521-13.475-8.718-19l70.482-70.482c2.587,0.956,5.383,1.48,8.298,1.48    c13.233,0,24-10.767,24-24c0-13.233-10.767-24-24-24c-13.233,0-24,10.767-24,24c0,2.959,0.542,5.792,1.525,8.411l-71.479,71.479    c-6.858-3.776-14.729-5.93-23.095-5.93c-8.403,0-16.305,2.174-23.183,5.981l-71.53-71.53c0.984-2.619,1.525-5.453,1.525-8.411    c0-13.233-10.767-24-24-24c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c2.916,0,5.711-0.524,8.298-1.48l70.56,70.56    c-4.166,5.508-7.172,11.936-8.657,18.923H10.001c-5.523,0-10,4.477-10,10v302c0,5.523,4.477,10,10,10h59.373l-14.77,40.58    c-1.116,3.066-0.666,6.483,1.206,9.156c1.871,2.672,4.929,4.264,8.191,4.264h32c4.204,0,7.959-2.629,9.397-6.58l17.26-47.42    h266.687l17.26,47.42c1.438,3.95,5.193,6.58,9.397,6.58h32c3.263,0,6.32-1.592,8.191-4.264c1.872-2.673,2.321-6.09,1.206-9.156    l-14.77-40.58h59.373c5.523,0,10-4.477,10-10v-302C512.001,140.479,507.524,136.002,502.001,136.002z M373.051,20    c2.206,0,4,1.794,4,4s-1.794,4-4,4s-4-1.794-4-4S370.845,20,373.051,20z M138.813,28c-2.206,0-4-1.794-4-4s1.794-4,4-4    s4,1.794,4,4S141.019,28,138.813,28z M256.001,117.96c11.94,0,22.155,7.504,26.192,18.042H229.81    C233.846,125.465,244.061,117.96,256.001,117.96z M88.999,492.002H78.282l12.375-34h10.717L88.999,492.002z M423.003,492.002    l-12.375-34h10.717l12.375,34H423.003z M492.001,438.002h-472v-282h472V438.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M246.001,393.002h-0.333c-5.523,0-10,4.477-10,10s4.477,10,10,10h0.333c5.523,0,10-4.477,10-10    S251.524,393.002,246.001,393.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,210.815c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,221.582,468.255,210.815,455.022,210.815z M455.022,238.815c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,238.815,455.022,238.815z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,273.002c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,283.769,468.255,273.002,455.022,273.002z M455.022,301.002c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,301.002,455.022,301.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,335.188c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,345.955,468.255,335.188,455.022,335.188z M455.022,363.188c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,363.188,455.022,363.188z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M342.766,347.915L225.031,230.181c-3.905-3.905-10.237-3.905-14.143,0c-3.906,3.905-3.905,10.237,0,14.143    l117.735,117.735c1.953,1.952,4.513,2.928,7.072,2.928s5.119-0.976,7.071-2.929C346.671,358.153,346.671,351.821,342.766,347.915z    " data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M190.828,266.334l-32.955-32.955c-3.904-3.905-10.236-3.905-14.142,0c-3.905,3.905-3.905,10.237,0,14.143l32.955,32.955    c1.953,1.953,4.512,2.929,7.071,2.929s5.119-0.976,7.071-2.929C194.733,276.572,194.733,270.24,190.828,266.334z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M214.02,289.527l-0.092-0.093c-3.895-3.917-10.227-3.933-14.142-0.038c-3.916,3.895-3.932,10.227-0.038,14.142    l0.092,0.093c1.955,1.965,4.521,2.948,7.09,2.948c2.55,0,5.102-0.97,7.052-2.91C217.898,299.774,217.915,293.442,214.02,289.527z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M269.999,345.505l-33.329-33.329c-3.906-3.905-10.237-3.905-14.142,0c-3.905,3.905-3.906,10.237,0,14.142l33.329,33.329    c1.953,1.953,4.512,2.929,7.071,2.929c2.559,0,5.119-0.977,7.071-2.929C273.903,355.742,273.904,349.41,269.999,345.505z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'No Smoking')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512.003 512.003">
                                            <g>
                                                <path d="M408.042,181.002H50.001c-5.523,0-10,4.477-10,10v212c0,5.523,4.477,10,10,10h163.033c5.523,0,10-4.477,10-10    s-4.477-10-10-10H60.001v-192h338.042v192H280.667c-5.523,0-10,4.477-10,10s4.477,10,10,10h127.375c5.523,0,10-4.477,10-10v-212    C418.042,185.479,413.565,181.002,408.042,181.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>

                                            <g>
                                                <path d="M502.001,136.002H302.988c-1.492-7.019-4.521-13.475-8.718-19l70.482-70.482c2.587,0.956,5.383,1.48,8.298,1.48    c13.233,0,24-10.767,24-24c0-13.233-10.767-24-24-24c-13.233,0-24,10.767-24,24c0,2.959,0.542,5.792,1.525,8.411l-71.479,71.479    c-6.858-3.776-14.729-5.93-23.095-5.93c-8.403,0-16.305,2.174-23.183,5.981l-71.53-71.53c0.984-2.619,1.525-5.453,1.525-8.411    c0-13.233-10.767-24-24-24c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c2.916,0,5.711-0.524,8.298-1.48l70.56,70.56    c-4.166,5.508-7.172,11.936-8.657,18.923H10.001c-5.523,0-10,4.477-10,10v302c0,5.523,4.477,10,10,10h59.373l-14.77,40.58    c-1.116,3.066-0.666,6.483,1.206,9.156c1.871,2.672,4.929,4.264,8.191,4.264h32c4.204,0,7.959-2.629,9.397-6.58l17.26-47.42    h266.687l17.26,47.42c1.438,3.95,5.193,6.58,9.397,6.58h32c3.263,0,6.32-1.592,8.191-4.264c1.872-2.673,2.321-6.09,1.206-9.156    l-14.77-40.58h59.373c5.523,0,10-4.477,10-10v-302C512.001,140.479,507.524,136.002,502.001,136.002z M373.051,20    c2.206,0,4,1.794,4,4s-1.794,4-4,4s-4-1.794-4-4S370.845,20,373.051,20z M138.813,28c-2.206,0-4-1.794-4-4s1.794-4,4-4    s4,1.794,4,4S141.019,28,138.813,28z M256.001,117.96c11.94,0,22.155,7.504,26.192,18.042H229.81    C233.846,125.465,244.061,117.96,256.001,117.96z M88.999,492.002H78.282l12.375-34h10.717L88.999,492.002z M423.003,492.002    l-12.375-34h10.717l12.375,34H423.003z M492.001,438.002h-472v-282h472V438.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M246.001,393.002h-0.333c-5.523,0-10,4.477-10,10s4.477,10,10,10h0.333c5.523,0,10-4.477,10-10    S251.524,393.002,246.001,393.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,210.815c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,221.582,468.255,210.815,455.022,210.815z M455.022,238.815c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,238.815,455.022,238.815z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,273.002c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,283.769,468.255,273.002,455.022,273.002z M455.022,301.002c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,301.002,455.022,301.002z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M455.022,335.188c-13.233,0-24,10.767-24,24c0,13.233,10.767,24,24,24c13.233,0,24-10.767,24-24    C479.022,345.955,468.255,335.188,455.022,335.188z M455.022,363.188c-2.206,0-4-1.794-4-4s1.794-4,4-4s4,1.794,4,4    S457.228,363.188,455.022,363.188z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M342.766,347.915L225.031,230.181c-3.905-3.905-10.237-3.905-14.143,0c-3.906,3.905-3.905,10.237,0,14.143    l117.735,117.735c1.953,1.952,4.513,2.928,7.072,2.928s5.119-0.976,7.071-2.929C346.671,358.153,346.671,351.821,342.766,347.915z    " data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M190.828,266.334l-32.955-32.955c-3.904-3.905-10.236-3.905-14.142,0c-3.905,3.905-3.905,10.237,0,14.143l32.955,32.955    c1.953,1.953,4.512,2.929,7.071,2.929s5.119-0.976,7.071-2.929C194.733,276.572,194.733,270.24,190.828,266.334z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M214.02,289.527l-0.092-0.093c-3.895-3.917-10.227-3.933-14.142-0.038c-3.916,3.895-3.932,10.227-0.038,14.142    l0.092,0.093c1.955,1.965,4.521,2.948,7.09,2.948c2.55,0,5.102-0.97,7.052-2.91C217.898,299.774,217.915,293.442,214.02,289.527z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                            <g>
                                                <path d="M269.999,345.505l-33.329-33.329c-3.906-3.905-10.237-3.905-14.142,0c-3.905,3.905-3.906,10.237,0,14.142l33.329,33.329    c1.953,1.953,4.512,2.929,7.071,2.929c2.559,0,5.119-0.977,7.071-2.929C273.903,355.742,273.904,349.41,269.999,345.505z" data-original="#000000" class="active-path" fill="" data-old_color="#000000" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Public Transport')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="138.992" height="147.086" viewBox="0 0 138.992 147.086">
                                            <g id="public_transport_pink" data-name="public transport pink" transform="translate(-6.433 -1)">
                                                <g id="Group_450" data-name="Group 450" transform="translate(6.433 1)">
                                                    <path id="Path_209" data-name="Path 209" d="M60.346,11.6A4.351,4.351,0,0,0,56.211,16.2v.459a6.926,6.926,0,0,0,8.73.689C64.941,17.118,65.4,11.374,60.346,11.6Z" transform="translate(66.469 12.748)" fill="" />
                                                    <path id="Path_210" data-name="Path 210" d="M41.47,60.98c6.2-7.352,6.892-7.352,7.352-8.96L49.97,48.8c2.757,34.461,2.3,32.393,2.757,33.082l-4.135,35.38a6.6,6.6,0,1,0,13.1,1.608l4.365-36.3H67.43l4.365,36.3a6.6,6.6,0,1,0,13.1-1.608L80.3,81.656a.844.844,0,0,0,.23-.689l3.446-45.258h0l5.973,1.608a4.432,4.432,0,0,0,5.514-4.595L94.539,17.1a14.426,14.426,0,0,1-4.365.689,13.376,13.376,0,0,1-4.365-.689l.459,10.108-3.676-.689h0c-11.487-7.352-22.974-7.581-33.542.689a4.98,4.98,0,0,0-2.3,2.527l-6.2,18.839-5.743,6.662A4.4,4.4,0,1,0,41.47,60.98Z" transform="translate(37.33 19.888)" fill="" />
                                                    <path id="Path_211" data-name="Path 211" d="M26.8,80.144l3.446-28.717A49.135,49.135,0,0,1,17.377,48.9L13.93,78.536a6.52,6.52,0,0,0,5.743,7.352C22.66,86.347,26.336,83.82,26.8,80.144Z" transform="translate(11.571 61.145)" fill="" />
                                                    <path id="Path_212" data-name="Path 212" d="M22.7,51.557l3.446,28.488A6.6,6.6,0,0,0,33.5,85.788a6.52,6.52,0,0,0,5.743-7.352L35.8,48.8A38.215,38.215,0,0,1,22.7,51.557Z" transform="translate(23.018 61.015)" fill="" />
                                                    <path id="Path_213" data-name="Path 213" d="M15.254,17.233c0-.23.919-6.433-4.365-6.433-5.743,0-4.135,7.122-4.365,7.811A6.543,6.543,0,0,0,15.254,17.233Z" transform="translate(1.976 11.714)" fill="" />
                                                    <circle id="Ellipse_7" data-name="Ellipse 7" cx="11.257" cy="11.257" r="11.257" transform="translate(32.393 14.474)" fill="" />
                                                    <path id="Path_214" data-name="Path 214" d="M128.007,12.027c.689,0,1.378-.23,2.068-.23a6.36,6.36,0,0,1,2.068.23v-6.2h8.271V1H2.8V5.825h9.879v6.2c.689,0,1.378-.23,2.068-.23a6.36,6.36,0,0,1,2.068.23v-6.2H128.237v6.2Z" transform="translate(-2.8 -1)" fill="" />
                                                    <path id="Path_215" data-name="Path 215" d="M14.487,29.444A11.52,11.52,0,0,0,25.974,17.957,11.184,11.184,0,0,0,14.487,6.7,11.474,11.474,0,0,0,3,17.957,11.52,11.52,0,0,0,14.487,29.444ZM5.3,17.957a9.19,9.19,0,1,1,9.19,9.19A9.217,9.217,0,0,1,5.3,17.957Z" transform="translate(-2.541 6.395)" fill="" />
                                                    <path id="Path_216" data-name="Path 216" d="M64.787,6.7A11.474,11.474,0,0,0,53.3,17.957a11.487,11.487,0,1,0,22.974,0A11.628,11.628,0,0,0,64.787,6.7Zm0,20.447a9.19,9.19,0,1,1,9.19-9.19A9.215,9.215,0,0,1,64.787,27.147Z" transform="translate(62.718 6.395)" fill="" />
                                                    <path id="Path_217" data-name="Path 217" d="M30.887,34.281h.919V27.619a.988.988,0,0,0-.919-.919H20.319a.988.988,0,0,0-.919.919V48.066a.988.988,0,0,0,.919.919H30.887a.988.988,0,0,0,.919-.919v-.459h-.919a6.662,6.662,0,0,1,0-13.325Zm-2.068-5.743a1.378,1.378,0,1,1-1.378,1.378A1.482,1.482,0,0,1,28.819,28.538Z" transform="translate(18.737 32.343)" fill="" />
                                                    <path id="Path_218" data-name="Path 218" d="M63.475,57.764A4.389,4.389,0,0,0,67.61,52.02c-11.946-35.38-7.811-23.2-8.73-25.96h0A4.665,4.665,0,0,0,56.123,23.3h0c-9.419-5.054-18.839-5.054-28.488,0h0c-.459,0-1.149.459-12.406,4.135V17.1a13.628,13.628,0,0,1-5.284,1.149A17.734,17.734,0,0,1,6.5,17.789V33.411a4.389,4.389,0,0,0,5.743,4.135c.23,0,15.392-5.284,14.474-4.825V46.736L19.365,84.184c14.014,8.271,30.325,8.73,44.8,0L59.34,59.831H50.61v.689a3.041,3.041,0,0,1-2.987,2.987H37.055a3.041,3.041,0,0,1-2.987-2.987V40.074a3.041,3.041,0,0,1,2.987-2.987H47.623a3.041,3.041,0,0,1,2.987,2.987v6.662h5.973c0,.23.919,2.527.689,2.3H47.393A4.224,4.224,0,0,0,43.028,53.4a4.362,4.362,0,0,0,4.365,4.365Z" transform="translate(2 19.888)" fill="" />
                                                    <path id="Path_219" data-name="Path 219" d="M54.127,8.3A11.03,11.03,0,0,0,43.1,19.327,10.886,10.886,0,0,0,54.127,30.355,11.03,11.03,0,0,0,65.155,19.327,10.886,10.886,0,0,0,54.127,8.3Z" transform="translate(49.485 8.471)" fill="" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Pet Friendly')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="152.18" height="132.621" viewBox="0 0 152.18 132.621">
                                            <g id="pet_friendly_pink" data-name="pet friendly pink" transform="translate(0 -28.79)">
                                                <path id="Path_147" data-name="Path 147" d="M222.594,220.053h-1.177a35.409,35.409,0,0,1-15.756-3.72l-9.44-4.722.09-.179-4.163,4.164a10.849,10.849,0,0,1,4.533,3.864,7.43,7.43,0,0,0,6.19,3.311,2.718,2.718,0,0,1,2.717,2.717v5.435a10.888,10.888,0,0,1-8.153,10.526v7.512a14.877,14.877,0,0,1-2.514,8.294L192,261.639v6.2l6.231-6.231a2.718,2.718,0,0,1,4.639,1.922v18.479l5.435-3.6V258.1a2.718,2.718,0,0,1,1.087-2.174l10.632-7.973a18.123,18.123,0,0,0,4.23-10.763,37.571,37.571,0,0,0-.285-6.841Z" transform="translate(-126.78 -120.601)" fill="" />
                                                <path id="Path_148" data-name="Path 148" d="M219.394,146.7h-.034l4.579,2.289a29.948,29.948,0,0,0,13.326,3.146h.476V135.832a2.718,2.718,0,0,1,2.717-2.717h6.794a4.081,4.081,0,0,0,4.076-4.076v-4.076h-5.435a2.718,2.718,0,0,1-1.922-.8l-5.472-5.472a6.386,6.386,0,0,0-4.546-1.884h-5.883a6.748,6.748,0,0,0-4.763,1.973A24.775,24.775,0,0,0,216,136.417v4.851h3.394a2.044,2.044,0,0,0,2.041-2.042V130.4h5.435v8.828a7.485,7.485,0,0,1-7.476,7.477Zm10.194-24.458h5.435v5.435h-5.435Z" transform="translate(-142.627 -58.12)" fill="" />
                                                <path id="Path_149" data-name="Path 149" d="M256,360.244l5.435-3.6V340.81L256,344.886Z" transform="translate(-169.04 -206.03)" fill="" />
                                                <path id="Path_150" data-name="Path 150" d="M150.414,59.9a51.063,51.063,0,0,0-5.173-12.376A37.751,37.751,0,0,0,82.429,43.9L78.264,49.45a2.718,2.718,0,0,1-4.348,0L69.751,43.9a37.934,37.934,0,0,0-30.2-15.1H38.52A37.746,37.746,0,0,0,6.671,46.282,42.589,42.589,0,0,0,0,69.189v7.857a63.693,63.693,0,0,0,27.424,52.461l2.735-4.1A73.412,73.412,0,0,1,16.63,112.36L14.183,109.3a2.718,2.718,0,0,1,4.244-3.4l2.446,3.059a67.983,67.983,0,0,0,12.3,11.911l7.585-11.38v-7.68q0-.357.012-.713A20.453,20.453,0,0,1,45.7,88.648l3.863-4.543a2.717,2.717,0,0,1,4.706,1.1l2.2,8.81h2.188l9.278-9.278V78.3a30.175,30.175,0,0,1,8.9-21.477,12.184,12.184,0,0,1,8.607-3.564h5.883a11.787,11.787,0,0,1,8.39,3.476l4.675,4.677h7.027a2.718,2.718,0,0,1,2.718,2.718v6.794a9.522,9.522,0,0,1-9.511,9.511h-4.076V93.837l2.028,15.188a43.027,43.027,0,0,1,.326,7.832,23.546,23.546,0,0,1-5.072,13.466v16.691l25.736-17.045A63.746,63.746,0,0,0,152.18,76.7V73.2a50.912,50.912,0,0,0-1.766-13.3ZM11.729,99.313A2.715,2.715,0,0,1,8.293,97.6v0l-.423-1.267a47.367,47.367,0,0,1-2.435-15V80.43a2.718,2.718,0,1,1,5.435,0v.894a41.934,41.934,0,0,0,2.156,13.285l.422,1.267A2.717,2.717,0,0,1,11.729,99.313ZM138,109.3l-2.446,3.059a73.111,73.111,0,0,1-20.9,17.88l-7.316,4.18a2.718,2.718,0,0,1-2.7-4.719l7.316-4.181a67.7,67.7,0,0,0,19.356-16.556l2.446-3.059a2.718,2.718,0,0,1,4.274,3.357l-.03.037Zm8.748-27.979a47.367,47.367,0,0,1-2.435,15l-.423,1.267a2.718,2.718,0,1,1-5.173-1.669l.017-.05.422-1.267a41.933,41.933,0,0,0,2.156-13.284V80.43a2.718,2.718,0,1,1,5.435,0v.894Zm0-11.764a2.718,2.718,0,1,1-5.435,0V66.842a2.718,2.718,0,1,1,5.435,0Z" fill="" />
                                                <path id="Path_151" data-name="Path 151" d="M122.357,260.9l3.38-5.071a9.463,9.463,0,0,0,1.6-5.277v-9.885a2.718,2.718,0,0,1,2.717-2.718,5.441,5.441,0,0,0,5.435-5.435v-3.006a12.855,12.855,0,0,1-7.994-5.441,5.421,5.421,0,0,0-4.522-2.423h-6.507a2.718,2.718,0,0,1-2.636-2.059l-1.436-5.741-.438.515a15,15,0,0,0-3.635,9.118c-.005.174-.008.354-.008.532v8.5a2.718,2.718,0,0,1-.457,1.508L94.07,254.706l1.556,1.019,5.719,3.747,21.842,14.309-1-2a2.716,2.716,0,0,1-.286-1.215v-8.153A2.717,2.717,0,0,1,122.357,260.9Zm-3.174-28.385v-5.435h5.435v5.435Z" transform="translate(-62.116 -122.197)" fill="" />
                                                <path id="Path_152" data-name="Path 152" d="M199.1,387.216a2.718,2.718,0,0,1,.288,1.217v2.28l2.718,1.78v-8.37l-3.034,3.034Z" transform="translate(-131.447 -234.63)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Smoke Alarm')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="88.662" height="107.104" viewBox="0 0 88.662 107.104">
                                            <g id="smoke_alarm_pink" data-name="smoke alarm pink" transform="translate(364.18 -48.275)">
                                                <path id="Path_400" data-name="Path 400" d="M-349.611,190.9l10.736-3.336-1.057-3.027-9.492,2.993a3.655,3.655,0,0,0-.939.325,2.864,2.864,0,0,1-3.58-.286,26.589,26.589,0,0,1-4.157-3.342c-1.992-2.236-1.761-4.731.318-6.917a19.487,19.487,0,0,1,7.234-4.463,70.762,70.762,0,0,1,15.455-3.907,125.642,125.642,0,0,1,21.1-1.377,108.742,108.742,0,0,1,26.24,3.253,35.608,35.608,0,0,1,11.5,4.826,8.252,8.252,0,0,1,3.291,3.8c.761,2.05.024,3.753-1.349,5.25a16.431,16.431,0,0,1-5.409,3.643,1.628,1.628,0,0,1-1.007-.107c-3.61-1.12-7.22-2.241-10.815-3.407-.561-.182-.762-.05-.917.458-.229.752-.5,1.491-.773,2.278L-282.5,190.9l-2.256,4.314c-1.5-.449-2.961-.914-4.439-1.322-1.661-.459-3.336-.872-5.008-1.293-.52-.131-.914-.089-1.077.585a22.077,22.077,0,0,1-.765,2.2l9.8,2.651a6.864,6.864,0,0,1-2.691,2.609,31.59,31.59,0,0,1-10.15,3.611,81.134,81.134,0,0,1-19.392,1.649,68.357,68.357,0,0,1-20.416-3.279,15.131,15.131,0,0,1-6.267-3.581c-.244-.256-.435-.563-.761-.993l9.844-2.665c-.231-.674-.537-1.387-.714-2.13s-.6-.807-1.209-.638c-2.855.79-5.72,1.545-8.561,2.38-.624.184-.892.121-1.17-.48C-348.264,193.375-348.9,192.267-349.611,190.9Zm33.549-9.134a105.2,105.2,0,0,0-18.136,1.647c-.585.1-.613.348-.425.811a16.02,16.02,0,0,1,.614,1.714c.144.519.373.629.918.527a94.538,94.538,0,0,1,15.218-1.533,101.925,101.925,0,0,1,17.446,1.24c.715.107,1.571.789,2.161.056a6.027,6.027,0,0,0,.827-2.2c.04-.148-.287-.551-.49-.589A100.729,100.729,0,0,0-316.061,181.77Zm15.959,9.742a98.522,98.522,0,0,0-31.905,0l1.087,3.017a96.481,96.481,0,0,1,29.751-.009Z" transform="translate(-3.844 -97.448)" fill=""></path>
                                                <path id="Path_401" data-name="Path 401" d="M-275.518,76.683c-4.27-3.773-9.29-5.712-14.455-7.346a85.331,85.331,0,0,0-14.283-3.073c-2.8-.381-5.617-.641-8.436-.768q-5.475-.246-10.963-.179a97.1,97.1,0,0,0-15.4,1.493,77.8,77.8,0,0,0-13.779,3.546c-4.093,1.509-8.007,3.315-11.3,6.487-.02-.373-.049-.63-.044-.885.061-3.411.148-6.822.186-10.233.034-3.024,1.814-5.086,3.851-6.961,4.407-4.058,9.82-6.168,15.482-7.7a75.03,75.03,0,0,1,17.244-2.58c4.258-.138,8.525-.287,12.779-.15a79.493,79.493,0,0,1,22.994,3.788,33.011,33.011,0,0,1,10.753,5.592,34.1,34.1,0,0,1,3.711,3.87,5.789,5.789,0,0,1,1.333,3.7C-275.711,69.084-275.624,72.884-275.518,76.683Z" fill=""></path>
                                                <path id="Path_402" data-name="Path 402" d="M-148.051,458.5c-3.57-2.042-6.11-4.871-6.718-9.073s.793-7.91,2.958-11.382c1.3-2.087,2.769-4.069,4.118-6.127a12.062,12.062,0,0,0,2.02-5.312,8.941,8.941,0,0,0-.934-4.591c-.377-.86-.836-1.684-1.258-2.524l.157-.209a32.616,32.616,0,0,1,2.872,2.02c3.5,3.047,4.007,6.868,2.749,11.119-.831,2.808-2.492,5.175-3.942,7.665a50.274,50.274,0,0,0-3.56,6.837c-1.207,3.083-.535,6.21.608,9.225C-148.682,456.94-148.361,457.72-148.051,458.5Z" transform="translate(-170.976 -303.124)" fill=""></path>
                                                <path id="Path_403" data-name="Path 403" d="M-193.513,441.158c4.337,2.1,5.682,6.162,3.523,10.569a70.9,70.9,0,0,1-3.645,6.186,9.789,9.789,0,0,0-.719,9.3,6.564,6.564,0,0,1,.414,1.36,8.445,8.445,0,0,1-4.521-6.408c-.4-3.3.862-6.11,2.684-8.744.966-1.4,1.947-2.785,2.832-4.232a6.374,6.374,0,0,0,.09-6.734C-193.053,442.094-193.23,441.719-193.513,441.158Z" transform="translate(-135.333 -320.993)" fill=""></path>
                                                <path id="Path_404" data-name="Path 404" d="M-94.718,478.909a6.928,6.928,0,0,1,3.635,9.41,56.615,56.615,0,0,1-3.422,5.876,37.2,37.2,0,0,0-2.014,3.559,9.29,9.29,0,0,0,.01,6.852c.165.511.345,1.016.535,1.571-1.365-.432-3.216-2.523-3.875-4.28a9.529,9.529,0,0,1,.935-8.474c1.161-1.982,2.685-3.749,3.931-5.686a14.137,14.137,0,0,0,1.512-3.261c.5-1.531-.081-2.979-.692-4.381C-94.318,479.741-94.491,479.394-94.718,478.909Z" transform="translate(-215.47 -351.836)" fill=""></path>
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break

                                        @case($amenity->masteramenity->amenity_name == 'Heating')
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="97.057" height="108.759" viewBox="0 0 97.057 108.759">
                                                <g id="heating_pink" data-name="heating pink" transform="translate(-25.718 0)">
                                                    <g id="Group_466" data-name="Group 466" transform="translate(64.607 12.399)">
                                                        <g id="Group_465" data-name="Group 465" transform="translate(0)">
                                                            <path id="Path_259" data-name="Path 259" d="M213.594,68.175a12.424,12.424,0,0,0-2.523-3.164,15.53,15.53,0,0,1-4.581-10.5,12.234,12.234,0,0,0-.893,1.281,11.822,11.822,0,0,0,.233,12.637,1.944,1.944,0,1,1-2.64,2.853,10.579,10.579,0,0,1-4.387-5.008A8.908,8.908,0,1,0,214.467,72.1v-.039A8.826,8.826,0,0,0,213.594,68.175Z" transform="translate(-196.675 -54.509)" fill=""></path>
                                                        </g>
                                                    </g>
                                                    <g id="Group_468" data-name="Group 468" transform="translate(48.022 0)">
                                                        <g id="Group_467" data-name="Group 467" transform="translate(0 0)">
                                                            <path id="Path_260" data-name="Path 260" d="M157.375,1.074a26.205,26.205,0,0,0-32.542,32.542l-.078.078a26.283,26.283,0,0,0,17.917,17.762,26.68,26.68,0,0,0,3.649.757,26.266,26.266,0,0,0,10.812-.757l.757-.252c.466-.155.951-.291,1.417-.466a26.34,26.34,0,0,0,11.006-7.978q.718-.893,1.359-1.825a25.985,25.985,0,0,0,2.8-5.377,20.02,20.02,0,0,0,.641-1.941A26.206,26.206,0,0,0,157.375,1.074Zm3.913,33.144A12.812,12.812,0,1,1,142.633,18.98h-.116a1.768,1.768,0,0,1,1.068-.272,15.665,15.665,0,0,1,8.153-12.579,1.941,1.941,0,0,1,2.8,2.271,11.824,11.824,0,0,0-.5,3.028v.427a11.647,11.647,0,0,0,3.455,8.328,15.857,15.857,0,0,1,3.28,4.232A12.849,12.849,0,0,1,161.288,34.218Z" transform="translate(-123.765 0)" fill=""></path>
                                                        </g>
                                                    </g>
                                                    <g id="Group_470" data-name="Group 470" transform="translate(25.718 34.994)">
                                                        <g id="Group_469" data-name="Group 469">
                                                            <path id="Path_261" data-name="Path 261" d="M120.834,206.246a1.941,1.941,0,0,0-1.941,1.941v3.882a1.941,1.941,0,0,1-1.941,1.941h-5.823V159.659a5.708,5.708,0,0,0-1.708-4.1,5.823,5.823,0,0,0-6.542-1.184c-.1.311-.214.6-.33.893l-.272.738c-.214.543-.447,1.068-.679,1.592l-.175.388c-.311.641-.641,1.281-.99,1.941l-.33.543c-.214.349-.408.7-.621,1.029v13.685a1.941,1.941,0,0,1-3.882,0V166.3l-.33.311a24.729,24.729,0,0,1-1.941,1.708l-.738.6q-.932.718-1.941,1.378l-.66.447a30.189,30.189,0,0,1-2.718,1.495l-.543.233q-1.165.524-2.368.971l-.427.175v9.337a1.941,1.941,0,0,1-3.882,0v-8.328h-.408q-1.009.194-2.038.291h-.718c-.874,0-1.766.136-2.659.136a17.2,17.2,0,0,1-2.64-.136h-.621q-1.087-.116-2.155-.311H68.4v8.347a1.941,1.941,0,0,1-3.882,0v-9.376L64.1,173.4q-1.184-.427-2.329-.951l-.582-.252a30.161,30.161,0,0,1-2.718-1.495l-.679-.447q-.99-.64-1.941-1.359l-.757-.6q-.97-.8-1.941-1.689l-.252-.311v8.891a1.941,1.941,0,0,1-3.882,0v-13.7c-.214-.33-.408-.679-.621-1.029l-.33-.543c-.349-.621-.679-1.262-.99-1.941l-.175-.408c-.233-.524-.466-1.048-.679-1.592l-.252-.68-.349-.932a5.473,5.473,0,0,0-2.427-.524,5.823,5.823,0,0,0-5.823,5.823v54.352H27.659a1.941,1.941,0,1,0,0,3.882h9.706v3.882a5.823,5.823,0,0,0,11.647,0V182.952a1.941,1.941,0,0,1,3.882,0v38.823a5.823,5.823,0,1,0,11.647,0V190.717a1.941,1.941,0,0,1,3.882,0v31.058a5.823,5.823,0,1,0,11.647,0V190.717a1.941,1.941,0,0,1,3.882,0v31.058a5.823,5.823,0,1,0,11.647,0V182.952a1.941,1.941,0,0,1,3.882,0v38.823a5.823,5.823,0,1,0,11.647,0v-3.882h5.823a5.823,5.823,0,0,0,5.823-5.823v-3.882A1.94,1.94,0,0,0,120.834,206.246Z" transform="translate(-25.718 -153.835)" fill=""></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </span>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Gym')
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                            width="49.305" height="20.274"
                                                                            viewBox="0 0 49.305 20.274">
                                                                            <g id="C1aRwJ.tif"
                                                                                transform="translate(-6073.588 -1468.754)">
                                                                                <g id="Group_63" data-name="Group 63"
                                                                                    transform="translate(6073.588 1468.754)">
                                                                                    <path id="Path_81"
                                                                                        data-name="Path 81"
                                                                                        d="M6158.258,1478.9q0-4.267,0-8.533c0-1.118.458-1.581,1.563-1.594q.79-.01,1.58,0c1.128.012,1.6.48,1.605,1.628.009,1.875,0,3.75,0,5.625q0,5.626,0,11.251c0,1.3-.458,1.757-1.737,1.76-.463,0-.927,0-1.39,0-1.146-.008-1.622-.474-1.623-1.6Q6158.255,1483.169,6158.258,1478.9Z"
                                                                                        transform="translate(-6147.528 -1468.769)"
                                                                                        fill="" />
                                                                                    <path id="Path_82"
                                                                                        data-name="Path 82"
                                                                                        d="M6109.15,1478.859q0-4.235,0-8.471c0-1.153.454-1.6,1.6-1.612.527,0,1.054-.005,1.58,0,1.056.008,1.547.49,1.548,1.546q.007,6.321,0,12.642c0,1.5,0,2.992,0,4.488,0,1.09-.5,1.588-1.577,1.591-.548,0-1.1,0-1.644,0a1.347,1.347,0,0,1-1.513-1.522c-.008-2.887,0-5.773,0-8.66Z"
                                                                                        transform="translate(-6104.639 -1468.771)"
                                                                                        fill="" />
                                                                                    <path id="Path_83"
                                                                                        data-name="Path 83"
                                                                                        d="M6345.371,1478.937q0,4.2,0,8.406c0,1.233-.448,1.678-1.671,1.681-.505,0-1.011.007-1.517,0a1.353,1.353,0,0,1-1.54-1.559q-.011-5.973,0-11.946c0-1.728-.007-3.455,0-5.183.007-1.119.46-1.562,1.577-1.577.569-.007,1.138-.01,1.707,0a1.3,1.3,0,0,1,1.442,1.452C6345.377,1473.122,6345.371,1476.03,6345.371,1478.937Z"
                                                                                        transform="translate(-6306.793 -1468.754)"
                                                                                        fill="" />
                                                                                    <path id="Path_84"
                                                                                        data-name="Path 84"
                                                                                        d="M6389.756,1478.9q0-4.267,0-8.534c0-1.149.452-1.591,1.615-1.6.526,0,1.054-.007,1.58,0a1.358,1.358,0,0,1,1.542,1.557c.009,2.4.005,4.8.006,7.207q0,4.868,0,9.736c0,1.326-.451,1.774-1.787,1.77-.443,0-.885,0-1.328,0-1.17-.006-1.63-.47-1.631-1.66q0-4.235,0-8.471Z"
                                                                                        transform="translate(-6349.686 -1468.763)"
                                                                                        fill="" />
                                                                                    <path id="Path_85"
                                                                                        data-name="Path 85"
                                                                                        d="M6207.033,1539.34V1536.3c.242-.016.462-.042.683-.043,4.716,0,9.431.008,14.146-.013.546,0,.749.122.712.7-.05.791-.013,1.588-.013,2.4Z"
                                                                                        transform="translate(-6190.122 -1527.688)"
                                                                                        fill="" />
                                                                                    <path id="Path_86"
                                                                                        data-name="Path 86"
                                                                                        d="M6438.662,1510.6c1.2.122,1.264.192,1.259,1.339,0,.337,0,.674,0,1.011,0,1.259,0,1.259-1.258,1.469,0,.754,0,1.524,0,2.294,0,.875-.156,1.025-1.034,1.023-.861,0-1-.134-1-.981q0-4.265,0-8.529c0-.8.193-.981,1.022-.979.812,0,1.012.2,1.015.994C6438.664,1509.017,6438.662,1509.793,6438.662,1510.6Z"
                                                                                        transform="translate(-6390.617 -1502.369)"
                                                                                        fill="" />
                                                                                    <path id="Path_87"
                                                                                        data-name="Path 87"
                                                                                        d="M6074.844,1514.41c-1.231-.176-1.253-.2-1.255-1.359,0-.358,0-.716,0-1.074,0-1.191.048-1.246,1.255-1.38,0-.787,0-1.582,0-2.378,0-.759.216-.965.978-.974.852-.01,1.07.167,1.072.936q.012,4.326,0,8.652c0,.76-.185.908-1.026.9-.825,0-1.021-.172-1.029-.911C6074.838,1516.028,6074.844,1515.23,6074.844,1514.41Z"
                                                                                        transform="translate(-6073.588 -1502.367)"
                                                                                        fill="" />
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Cable TV')
                                        <svg xmlns="http://www.w3.org/2000/svg" width="125.634" height="125.634" viewBox="0 0 125.634 125.634">
                                                                    <g id="satellite_cable_pink" data-name="satellite cable pink" transform="translate(-4.32 -4.32)">
                                                                        <path id="Path_271" data-name="Path 271" d="M33.4,27.019l-6.735-6.724L39.429,7.524l-3.2-3.2-31.9,31.9,3.2,3.2,12.771-12.76L27.019,33.4A20.858,20.858,0,0,1,33.4,27.019Z" transform="translate(0)" fill="" />
                                                                        <path id="Path_272" data-name="Path 272" d="M40.261,18.149A15.794,15.794,0,0,0,18.149,40.261Z" transform="translate(13.405 13.405)" fill="" />
                                                                        <path id="Path_273" data-name="Path 273" d="M66.307,67.184V50.69a20.1,20.1,0,0,1-9.025,4.693v11.8H46V71.7H77.589V67.184Z" transform="translate(52.365 58.257)" fill="" />
                                                                        <path id="Path_274" data-name="Path 274" d="M54,28h4.513v4.513H54Z" transform="translate(62.416 29.75)" fill="" />
                                                                        <path id="Path_275" data-name="Path 275" d="M54,24h4.513v4.513H54Z" transform="translate(62.416 24.725)" fill="" />
                                                                        <path id="Path_276" data-name="Path 276" d="M54,20h4.513v4.513H54Z" transform="translate(62.416 19.7)" fill="" />
                                                                        <path id="Path_277" data-name="Path 277" d="M54,16h4.513v4.513H54Z" transform="translate(62.416 14.674)" fill="" />
                                                                        <path id="Path_278" data-name="Path 278" d="M54,12h4.513v4.513H54Z" transform="translate(62.416 9.649)" fill="" />
                                                                        <path id="Path_279" data-name="Path 279" d="M54,8h4.513v4.513H54Z" transform="translate(62.416 4.623)" fill="" />
                                                                        <path id="Path_280" data-name="Path 280" d="M42,8h4.513v4.513H42Z" transform="translate(47.339 4.623)" fill="" />
                                                                        <path id="Path_281" data-name="Path 281" d="M38,8h4.513v4.513H38Z" transform="translate(42.314 4.623)" fill="" />
                                                                        <path id="Path_282" data-name="Path 282" d="M50,8h4.513v4.513H50Z" transform="translate(57.39 4.623)" fill="" />
                                                                        <path id="Path_283" data-name="Path 283" d="M46,8h4.513v4.513H46Z" transform="translate(52.365 4.623)" fill="" />
                                                                        <path id="Path_284" data-name="Path 284" d="M34,8h4.513v4.513H34Z" transform="translate(37.289 4.623)" fill="" />
                                                                        <path id="Path_285" data-name="Path 285" d="M30,8h4.513v4.513H30Z" transform="translate(32.263 4.623)" fill="" />
                                                                        <path id="Path_286" data-name="Path 286" d="M26,8h4.513v4.513H26Z" transform="translate(27.238 4.623)" fill="" />
                                                                        <path id="Path_287" data-name="Path 287" d="M8,26h4.513v4.513H8Z" transform="translate(4.623 27.238)" fill="" />
                                                                        <path id="Path_288" data-name="Path 288" d="M8,30h4.513v4.513H8Z" transform="translate(4.623 32.263)" fill="" />
                                                                        <path id="Path_289" data-name="Path 289" d="M8,34h4.513v4.513H8Z" transform="translate(4.623 37.289)" fill="" />
                                                                        <path id="Path_290" data-name="Path 290" d="M8,38h4.513v4.513H8Z" transform="translate(4.623 42.314)" fill="" />
                                                                        <path id="Path_291" data-name="Path 291" d="M8,42h4.513v4.513H8Z" transform="translate(4.623 47.339)" fill="" />
                                                                        <path id="Path_292" data-name="Path 292" d="M8,46h4.513v4.513H8Z" transform="translate(4.623 52.365)" fill="" />
                                                                        <path id="Path_293" data-name="Path 293" d="M8,50h4.513v4.513H8Z" transform="translate(4.623 57.39)" fill="" />
                                                                        <path id="Path_294" data-name="Path 294" d="M8,54h4.513v4.513H8Z" transform="translate(4.623 62.416)" fill="" />
                                                                        <path id="Path_295" data-name="Path 295" d="M12,54h4.513v4.513H12Z" transform="translate(9.649 62.416)" fill="" />
                                                                        <path id="Path_296" data-name="Path 296" d="M16,54h4.513v4.513H16Z" transform="translate(14.674 62.416)" fill="" />
                                                                        <path id="Path_297" data-name="Path 297" d="M20,54h4.513v4.513H20Z" transform="translate(19.7 62.416)" fill="" />
                                                                        <path id="Path_298" data-name="Path 298" d="M24,54h4.513v4.513H24Z" transform="translate(24.725 62.416)" fill="" />
                                                                        <path id="Path_299" data-name="Path 299" d="M28,54h4.513v4.513H28Z" transform="translate(29.75 62.416)" fill="" />
                                                                        <path id="Path_300" data-name="Path 300" d="M32,54h4.513v4.513H32Z" transform="translate(34.776 62.416)" fill="" />
                                                                        <path id="Path_301" data-name="Path 301" d="M36,54h4.513v4.513H36Z" transform="translate(39.801 62.416)" fill="" />
                                                                        <path id="Path_302" data-name="Path 302" d="M46.548,26.243A20.307,20.307,0,0,1,26.241,46.55v4.513a24.806,24.806,0,0,0,24.82-24.82Z" transform="translate(27.541 27.543)" fill="" />
                                                                        <path id="Path_303" data-name="Path 303" d="M37.417,37.416a15.591,15.591,0,0,0,4.619-11.178l-4.513.02a11.255,11.255,0,0,1-11.21,11.264H26.26l-.023,4.513h.077a15.6,15.6,0,0,0,11.1-4.619Z" transform="translate(27.536 27.537)" fill="" />
                                                                        <path id="Path_304" data-name="Path 304" d="M28.018,34.4a4.513,4.513,0,1,0,2.031-7.534l-4.569-4.569-3.19,3.19,4.569,4.569a4.492,4.492,0,0,0,1.16,4.343Z" transform="translate(22.575 22.58)" fill="" />
                                                                        <path id="Path_305" data-name="Path 305" d="M67.707,62.707a15.81,15.81,0,0,0,2.369-19.314,9.928,9.928,0,0,0-.587-.88,2.873,2.873,0,0,0-.293-.429l-.429.429H52.025a4.513,4.513,0,1,0-4.513,4.513V63.768l-.429.429a2.874,2.874,0,0,0,.429.293,9.926,9.926,0,0,0,.88.587,15.81,15.81,0,0,0,19.314-2.369ZM59.735,51.538l-4.513-4.513h9.032Zm-7.71-1.322,4.513,4.513-4.513,4.513Z" transform="translate(48.596 42.314)" fill="" />
                                                                    </g>
                                                                </svg>
                                                                <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @case($amenity->masteramenity->amenity_name == 'Hot tub')
                                        <svg id="hot_tub_pink" data-name="hot tub pink" xmlns="http://www.w3.org/2000/svg" width="119.326" height="123.594" viewBox="0 0 119.326 123.594">
                                            <g id="_038---Jacuzzi" data-name="038---Jacuzzi" transform="translate(0 0)">
                                                <path id="Shape" d="M24.263,38.355a2.131,2.131,0,0,1-1.7-.852,10.756,10.756,0,0,1,0-13.929,6.4,6.4,0,0,0,1.7-4.421,6.262,6.262,0,0,0-1.7-4.375A10.426,10.426,0,0,1,20,7.814,10.422,10.422,0,0,1,22.558.852a2.131,2.131,0,1,1,3.409,2.557,6.356,6.356,0,0,0-1.7,4.4,6.365,6.365,0,0,0,1.7,4.4,10.4,10.4,0,0,1,2.557,6.938A10.5,10.5,0,0,1,26,26.09a6.45,6.45,0,0,0-1.739,4.451,6.356,6.356,0,0,0,1.7,4.4,2.131,2.131,0,0,1-1.7,3.414Z" transform="translate(22.615 0)" fill="" />
                                                <path id="Shape-2" data-name="Shape" d="M30.263,38.355a2.131,2.131,0,0,1-1.7-.852,10.756,10.756,0,0,1,0-13.929,6.4,6.4,0,0,0,1.7-4.421,6.263,6.263,0,0,0-1.7-4.375A10.426,10.426,0,0,1,26,7.814,10.422,10.422,0,0,1,28.558.852a2.131,2.131,0,1,1,3.409,2.557,6.356,6.356,0,0,0-1.7,4.4,6.365,6.365,0,0,0,1.7,4.4,10.4,10.4,0,0,1,2.557,6.938A10.5,10.5,0,0,1,32,26.09a6.45,6.45,0,0,0-1.739,4.451,6.356,6.356,0,0,0,1.7,4.4,2.131,2.131,0,0,1-1.7,3.414Z" transform="translate(29.4 0)" fill="" />
                                                <path id="Shape-3" data-name="Shape" d="M36.263,38.355a2.131,2.131,0,0,1-1.7-.852,10.756,10.756,0,0,1,0-13.929,6.4,6.4,0,0,0,1.7-4.421,6.263,6.263,0,0,0-1.7-4.375A10.426,10.426,0,0,1,32,7.814,10.422,10.422,0,0,1,34.558.852a2.131,2.131,0,0,1,3.409,2.557,6.356,6.356,0,0,0-1.7,4.4,6.365,6.365,0,0,0,1.7,4.4,10.4,10.4,0,0,1,2.557,6.938A10.5,10.5,0,0,1,38,26.09a6.45,6.45,0,0,0-1.739,4.451,6.356,6.356,0,0,0,1.7,4.4,2.131,2.131,0,0,1-1.7,3.414Z" transform="translate(36.185 0)" fill="" />
                                                <path id="Shape-4" data-name="Shape" d="M6,27.775V63.309a71.406,71.406,0,0,0,8.523,2.845v-35.8A88.418,88.418,0,0,1,6,27.775Z" transform="translate(6.785 31.409)" fill="" />
                                                <path id="Shape-5" data-name="Shape" d="M18,30.422V66.4q5.056.616,10.654.94V31.349q-5.461-.3-10.654-.927Z" transform="translate(20.355 34.402)" fill="" />
                                                <path id="Shape-6" data-name="Shape" d="M12,29.443V65.322c2.659.6,5.5,1.134,8.523,1.594V30.964Q16.11,30.317,12,29.443Z" transform="translate(13.57 33.295)" fill="" />
                                                <path id="Shape-7" data-name="Shape" d="M7.392,28.882A37.148,37.148,0,0,1,1,25.281V58.4a14.816,14.816,0,0,0,6.392,5.683Z" transform="translate(1.131 28.588)" fill="" />
                                                <path id="Shape-8" data-name="Shape" d="M112.081,15.175c-9.525-5.157-21.116-7.607-32.09-8.715a14.419,14.419,0,0,1,.98,5.391,16.568,16.568,0,0,1-.32,3.26,2.131,2.131,0,0,1,2.3-1.342c18.346,2.706,30.322,8.2,33.326,15.022a7.671,7.671,0,0,0,3.047-5.327c0-2.941-2.365-5.647-7.245-8.289ZM82.314,17.988a2.131,2.131,0,0,1-1.79-2.408A15.4,15.4,0,0,1,77.6,21.312c-.874,1.172-.895,1.236-.895,1.918s.021.746.852,1.833a6.418,6.418,0,0,1-1.278,8.971,6.337,6.337,0,0,1-3.835,1.278,6.45,6.45,0,0,1-5.114-2.557,20.336,20.336,0,0,1-1.492-2.195,6.393,6.393,0,0,1-11.293,2.195,20.338,20.338,0,0,1-1.492-2.195,6.393,6.393,0,0,1-11.293,2.195,15.009,15.009,0,0,1,0-19.05c.852-1.129.852-1.151.852-1.854s0-.7-.852-1.811a17.626,17.626,0,0,1-2.131-3.431A117.116,117.116,0,0,0,16.62,11.3C6.222,14.664,0,19.224,0,23.464a7.671,7.671,0,0,0,3.047,5.327c3.6-8.1,19.54-13.126,34.6-15.214a2.131,2.131,0,1,1,.6,4.219C17.792,20.651,7.8,26.618,6.669,31.348c10.462,6.158,30.982,10.121,52.994,10.121s42.531-3.963,52.994-10.121c-1.151-4.624-10.739-10.462-30.343-13.36ZM23.439,33.181A2.131,2.131,0,1,1,25.57,31.05,2.131,2.131,0,0,1,23.439,33.181Zm6.392-6.392a2.131,2.131,0,1,1,2.131-2.131A2.131,2.131,0,0,1,29.832,26.788Zm6.392,6.392a2.131,2.131,0,1,1,2.131-2.131A2.131,2.131,0,0,1,36.224,33.181Zm46.878,0a2.131,2.131,0,1,1,2.131-2.131A2.131,2.131,0,0,1,83.1,33.181Zm6.392-6.392a2.131,2.131,0,1,1,2.131-2.131A2.131,2.131,0,0,1,89.495,26.788Zm6.392,6.392a2.131,2.131,0,1,1,2.131-2.131A2.131,2.131,0,0,1,95.887,33.181Z" transform="translate(0 7.305)" fill="" />
                                                <path id="Shape-9" data-name="Shape" d="M109.705,49.349a2.055,2.055,0,0,1-.409.179c-11.368,5.044-29.356,8.2-49.633,8.2S21.4,54.572,10.03,49.528a2.055,2.055,0,0,1-.409-.179A28.421,28.421,0,0,1,.132,42.957,4.475,4.475,0,0,0,0,43.986V57.027C0,62.343,9.233,67.8,24.1,71.276a158.644,158.644,0,0,0,35.568,3.735,158.611,158.611,0,0,0,35.566-3.735c14.865-3.473,24.1-8.932,24.1-14.249V43.986a4.451,4.451,0,0,0-.128-1.023A28.53,28.53,0,0,1,109.705,49.349Zm4.262,6.708c-11.536,6.382-31.835,10.2-54.3,10.2s-42.768-3.814-54.3-10.2a2.131,2.131,0,0,1,2.063-3.729c10.767,5.96,30.786,9.663,52.239,9.663s41.472-3.7,52.239-9.663a2.131,2.131,0,1,1,2.063,3.729Z" transform="translate(0 48.577)" fill="" />
                                                <path id="Shape-10" data-name="Shape" d="M46,30.345V66.153a71.4,71.4,0,0,0,8.523-2.845V27.775A87.837,87.837,0,0,1,46,30.345Z" transform="translate(52.018 31.409)" fill="" />
                                                <path id="Shape-11" data-name="Shape" d="M59.663,32.281c-23.887,0-45.493-4.385-56.39-11.443A25.068,25.068,0,0,1,0,18.354v4.7a6.1,6.1,0,0,0,1.492,3.718,2.328,2.328,0,0,1,.162.213A25.857,25.857,0,0,0,11.413,33.3a1.279,1.279,0,0,1,.156.068A80.056,80.056,0,0,0,24.1,37.3a158.644,158.644,0,0,0,35.559,3.742A158.61,158.61,0,0,0,95.229,37.3a79.707,79.707,0,0,0,12.538-3.936l.153-.066a25.881,25.881,0,0,0,9.763-6.314,1.987,1.987,0,0,1,.16-.213,6.1,6.1,0,0,0,1.483-3.72v-4.7a25.321,25.321,0,0,1-3.277,2.487c-10.893,7.055-32.5,11.44-56.386,11.44Z" transform="translate(0 20.755)" fill="" />
                                                <path id="Shape-12" data-name="Shape" d="M58.392,25.281A37.372,37.372,0,0,1,52,28.882v35.2A14.816,14.816,0,0,0,58.392,58.4Z" transform="translate(58.803 28.588)" fill="" />
                                                <path id="Shape-13" data-name="Shape" d="M40,30.966V66.917c3.024-.46,5.864-1,8.523-1.594V29.442q-4.1.876-8.523,1.524Z" transform="translate(45.233 33.294)" fill="" />
                                                <path id="Shape-14" data-name="Shape" d="M33,31.349V67.336q5.591-.318,10.654-.94V30.422Q38.47,31.036,33,31.349Z" transform="translate(37.317 34.402)" fill="" />
                                                <path id="Shape-15" data-name="Shape" d="M37.785,66.941v-36c-2.114.068-4.242.117-6.392.117s-4.279-.049-6.392-.117v36c2.088.068,4.213.111,6.392.111S35.7,67.009,37.785,66.941Z" transform="translate(28.271 34.993)" fill="" />
                                            </g>
                                        </svg>
                                        <span class="title"> {{$amenity->masteramenity->amenity_name}}</span>
                                        @break
                                        @endswitch
                                    </span>
                                </div>
                                @endforeach
                                </div>
                               </div>

                                </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>


        </div>
        <!-- End of Content Wrapper -->
</div>
<div id="cancel_popup" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ml-auto">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                <h1 class="d-block w-100 text-center popup-heading common-fatfrank">Are you sure you want to cancel your reservation from  {{date('M jS, Y', strtotime($bookingDetails->check_in_date))}} to {{date('M jS, Y', strtotime($bookingDetails->check_out_date))}}? </h1>
                <div class="d-block w-100 text-center mt-3">

                <a class="button-default-animate button-default-custom btn-sunset-gradient mt-5 confirmation-yes">Yes, cancel my stay</a>
                    </div>
                <div class="d-block w-100 text-center">
                    <button class="btn button-custm-cancel" type="button" data-dismiss="modal">No, don't cancel my stay</button>
                </div>
            </div>

            </div>
        </div>
</div>

<div id="confirm_refund_popup" class="modal fade" role="dialog">
<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ml-auto">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                <h1 class="d-block w-100 text-center popup-heading common-fatfrank refund-text">Are you sure you want to cancel your reservation from  {{date('M jS, Y', strtotime($bookingDetails->check_in_date))}} to {{date('M jS, Y', strtotime($bookingDetails->check_out_date))}}? </h1>
                <div class="d-block w-100 text-center mt-3">

                <a class="button-default-animate button-default-custom btn-sunset-gradient mt-5 cancellation-yes">Confirm Cancellation</a>
                    </div>
                <div class="d-block w-100 text-center">
                    <button class="btn button-custm-cancel" type="button" data-dismiss="modal">No</button>
                </div>
            </div>

            </div>
        </div>
</div>
@endsection

@section('footerJs')
    @parent
    <script>

        var cancelBooking = "{{route('cancel.booking')}}";
        var calculateRefund = "{{route('calculate.refund')}}";
        var bookingId = "{{$bookingDetails->bookingid}}";
          jQuery('#view-stay-page-slider').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            dots:false,
            responsive:{
                0:{
                    items:3
                },
                600:{
                    items:3
                },
                1200:{
                    items:2
                },
                1300:{
                    items:3
                }
            }
        });
        $('.togl-btn-custom').click(function() {
            $(this).toggleClass("pushed");
        });

        $(document).ready(function () {
            $('.confirmation-yes').on('click',function(){
                 showLoader();
                $.ajax({
                    type: "POST",
                    url: calculateRefund,
                    dataType: "JSON",
                    headers: {
                        "Authorization": Authorization,
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                    },
                    data: {
                        id: bookingId,
                    },
                    success: function (msg) {
                        hideLoader();
                        $.unblockUI()

                        if ($.isEmptyObject(msg.error)) {
                            $('#cancel_popup').modal('hide');
                            if(msg.penidngRequest == 'yes'){
                                $('.view-stay-policy .view-past-stay-btn').remove();
                                $('.view-stay-policy h5').empty().text('Cancelled');
                            }else{
                                if(msg.refundAmount == 0){
                                var refundText = "According to the cancellation policy for this reservation, you are not eligible for a refund if you cancel. Would you still like to cancel?";
                                }else{
                                    var refundText = "According to the cancellation policy for this reservation, you will receive $"+HelperFunction.numberWithCommas(msg.refundAmount)+" back.";
                                }
                                $('#confirm_refund_popup').find('.refund-text').empty().text(refundText)
                                $('#confirm_refund_popup').modal('show');
                            }
                        }

                    }
                });
            });

            $('.cancellation-yes').on('click',function(){
                 showLoader();
                $.ajax({
                    type: "POST",
                    url: cancelBooking,
                    dataType: "JSON",
                    headers: {
                        "Authorization": Authorization,
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                    },
                    data: {
                        id: bookingId,
                    },
                    success: function (msg) {
                        hideLoader();
                        $.unblockUI()

                        if ($.isEmptyObject(msg.error)) {
                            $('.view-stay-policy .view-past-stay-btn').remove();
                            if(msg.booking_status == 'cancelled'){
                                $('.view-stay-policy h5').empty().text('Cancelled');
                            }else{
                                $('.view-stay-policy h5').empty().text('Refund Processed');
                            }

                            $('#confirm_refund_popup').modal('hide');
                        }else{
                            $('#confirm_refund_popup').modal('hide');
                              Swal.fire({
                                icon: "error",
                                title: msg.error,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            
                        }

                    }
                });
            });

        });

    </script>
@endsection
