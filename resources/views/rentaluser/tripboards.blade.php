@extends('layouts.rentalApp')

@section('content')


<div class="modal fade" id="add_tripboard_popup" tabindex="-1" role="dialog" aria-labelledby="add_tripboard_popup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h1 class="title-new-tripboard">NEW <span class="light">TRIPBOARD</span></h1>
                <div class="add-tripboard-container align-items-center w-100">
                    <div class="vertical-middle">
                        <form method="post" id="addTripBoard" name="addTripBoard">
                            <p class="pt-5">Name your board</p>
                            <input type="text" class="form-control input-board mt-10"  data-msg-required="Please add tripboard name."  name="tripboard_name" id="tripboard_name" placeholder="Say something like “Conference Weekend” or “Friend’s Bachelorette”">
                            <button class="btn-tripboard-fly mt-15 button-default-animate" href="#">ADD BOARD <svg class="ml-8" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="57.558" height="24.346" viewBox="0 0 57.558 24.346">
                                    <defs>
                                        <linearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                                            <stop offset="0" stop-color="#5495d1" />
                                            <stop offset="0.5" stop-color="#ddddb6" />
                                            <stop offset="0.632" stop-color="#eeda8c" />
                                            <stop offset="0.808" stop-color="#e6c469" />
                                            <stop offset="1" stop-color="#f6ab34" />
                                        </linearGradient>
                                    </defs>
                                    <path id="Path_1" data-name="Path 1" d="M147.455,1310.953c2.63,0,4.986.032,7.341-.009,2.154-.037,4.316-.053,6.457-.262,2.372-.231,4.721-.691,7.086-1.01a46.881,46.881,0,0,0,6.792-1.507c1.133-.329,2.264-.665,3.384-1.034.767-.252.834-.573.248-1.15a12.428,12.428,0,0,0-1.127-.916q-2.925-2.266-5.857-4.523a4.324,4.324,0,0,0-.792-.454,1.1,1.1,0,0,1-.626-1.6c.34-.132.665-.243.978-.381a5.2,5.2,0,0,1,3.441.066c1.257.3,2.5.654,3.751.993,2.019.547,4.033,1.112,6.055,1.65,1.11.3,2.234.544,3.347.832.7.181,1.383.425,2.086.594,1.3.313,2.365-.491,3.541-.77.966-.23,1.928-.5,2.871-.808a36.984,36.984,0,0,0,3.717-1.361,4.383,4.383,0,0,1,4.636,1.067,2.644,2.644,0,0,1-.294,2.653,7.564,7.564,0,0,1-2.018,1.276c-1.539.728-3.107,1.4-4.681,2.05-1.107.46-2.247.839-3.362,1.28a1.7,1.7,0,0,0-.706.475c-2.015,2.6-4.011,5.212-6.012,7.822q-2.077,2.709-4.152,5.419a3.6,3.6,0,0,1-2.7.771c-.569-.239-.419-.711-.326-1.144.225-1.05.44-2.1.7-3.144.3-1.221.664-2.428.973-3.647.187-.735.355-1.477.475-2.225.115-.711-.446-.522-.787-.471-.751.112-1.492.293-2.238.441-1.243.247-2.479.542-3.732.72-1.463.209-2.939.321-4.41.462-1.528.147-3.056.3-4.587.409a19.918,19.918,0,0,1-2.192.018c-1.67-.062-3.342-.112-5.007-.246-2.236-.178-4.479-.345-6.7-.676-1.739-.26-3.443-.756-5.157-1.167C147.76,1311.42,147.681,1311.225,147.455,1310.953Z" transform="translate(-147.455 -1297.832)" fill="url(#linear-gradient)" />
                                </svg>
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="tripboards">
    <div class="container">
        <div class="text-center">
            <img class="lazy-load" data-src="{{URL::asset('media/images/Tripboards.svg')}}" alt="tripboard">
        </div>
        <div class="text-center d-block mt-8">
            <p class="tripboard-under-text">make it happen. this is the year. </p>
            {{-- @if(Auth::guard('rentaluser')->user()) --}}
            <a class="float-right btn-add-new-board button-default-animate" href="#" data-toggle="modal" data-target="#add_tripboard_popup"><span class="pr-2 plus">+</span> Add new board</a>
            {{-- @endif --}}
        </div>
        @if(!Auth::guard('rentaluser')->user())
        <div class="text-center d-block mt-8">
            <p class="tripboard-under-text"> Please Login to see your Tripboards.</p>
        </div>
        @endif
    </div>
    <div class="container mt-30">
        <div class="row" id="favTripboard">
            <div id="favTripBlock" class="col-lg-4 position-relative mb-20" style="display: none;">
                <img data-src="{{URL::asset('media/images/favorite-heart-icon.svg')}}" class="lazy-load favorite-heart-icon " style="display: none;" alt="fav icon">
                <div class="pl-8 pr-8">
                    <div class="boards-white">
                        <div class="d-block">
                            <h3 class="boards-title">Saved favorites </h3>
                            <span class="float-right"><a href="#" class="mr-2 editIcon"></a><a href="#" class="editIcon"><img src="{{URL::asset('media/images/board-pencil-icon.svg')}}" alt="pencil icon"></a></span>
                        </div>
                        <div class="border-separator-s"></div>
                        <div class="row m-0">
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link1">
                                    <img data-src="{{URL::asset('uploads/property/slider1.jpg')}}" class="lazy-load boards-thumbnail-img img-property1" alt="slider">
                                </a>
                            </div>
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link2">
                                    <img data-src="{{URL::asset('uploads/property/slider2.jpg')}}" class="lazy-load boards-thumbnail-img img-property2" alt="slider">
                                </a>
                            </div>
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link3">
                                    <img data-src="{{URL::asset('uploads/property/slider3.jpg')}}" class="lazy-load boards-thumbnail-img img-property3" alt="slider">
                                </a>
                            </div>
                        </div>
                        <div class="d-block text-center mt-10 mb-3">
                            <a class="btn-explore-more-tripboards button-default-animate">Explore more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="noFavBoard" class="col-lg-4 position-relative mb-20" style="display: none;">
                <img data-src="{{URL::asset('media/images/favorite-heart-icon.svg')}}" class="lazy-load favorite-heart-icon " alt="">
                <div class="pl-8 pr-8">
                    <div class="boards-white">
                        <div class="d-block">
                            <h3 class="boards-title">Saved Favorites </h3>
                        </div>
                        <div class="border-separator-s"></div>
                        <div class="row m-0">
                            <div class="d-block text-center no-fav-yet w-100">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>
                                <div class="zero-state-text common-open w-100 pt-3 pb-1">You have no saved favorites yet.</div>
                            </div>
                        </div>
                        <div class="d-block text-center mt-8 mb-3">
                            <a class="btn-explore-more-tripboards button-default-animate savedFav" href="javascipt:void(0)">Explore more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="userTripBlock" class="col-lg-4 position-relative mb-20" style="display: none;">
                <div class="pl-8 pr-8">
                    <div class="boards-white">
                        <div class="d-block">
                            <h3 class="boards-title">Saved favorites </h3>
                            <input type="hidden" id="tripboardId" name="tripboardId">
                            <span class="float-right hide_share_icons"><a id="shareIcon" onClick="shareLink(this.id)" class="mr-2 sharelink" style="cursor:pointer"><img class="lazy-load" data-src="{{URL::asset('media/images/edit-button.svg')}}" alt=""></a><a href="#" class="mr-2 editIcon"></a><a href="#" class="editIcon"><img class="lazy-load" data-src="{{URL::asset('media/images/board-pencil-icon.svg')}}" alt="pencil icon"></a></span>
                        </div>

                        <div class="border-separator-s"></div>

                        <div id="images_show" class="row m-0 static_images">
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link1">
                                    <img data-src="{{URL::asset('uploads/property/slider1.jpg')}}" class="lazy-load boards-thumbnail-img img-property1" alt="property">
                                </a>
                            </div>
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link2">
                                    <img data-src="{{URL::asset('uploads/property/slider2.jpg')}}" class="lazy-load boards-thumbnail-img img-property2" alt="property">
                                </a>
                            </div>
                            <div class="col-4 boards-thumbnail">
                                <a href="" class="link3">
                                    <img data-src="{{URL::asset('uploads/property/slider3.jpg')}}" class="lazy-load boards-thumbnail-img img-property3" alt="property">
                                </a>
                            </div>
                        </div>

                        <div class="d-block text-center mt-10 mb-3 nolisting">
                            <a class="btn-explore-more-tripboards button-default-animate">Explore more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="noUserBoard" class="col-lg-4 position-relative mb-20" style="display: none;">
                <div class="pl-8 pr-8">
                    <div class="boards-white">
                        <div class="d-block">
                            <h3 class="boards-title">TripBoard </h3>
                        </div>
                        <div class="border-separator-s"></div>
                        <div class="row m-0">
                        <div class="d-block text-center no-fav-yet w-100">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>
                                <div class="zero-state-text common-open w-100 pt-3 pb-1">You have no listings saved to this Tripboard yet. Search for listings above to add them to your Tripboard!.</div>
                            </div>
                        </div>
                        <div class="d-block text-center mt-10 mb-3">
                            <a class="btn-explore-more-tripboards button-default-animate d-none">Explore more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="sharelink" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ml-auto">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h1 class="d-block w-100 text-center popup-heading common-fatfrank copy-text-link"> The link to this tripboard has been copied!</h1>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($sharedTripBoard) && isset($sharedTripBoard['TripBoard_detail']->tripboard_name))
        <div class="modal fade cmn-popup approve" id="tripboardConfirm" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="approve" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 >Do you want to add <span id="tripboard_name_display">@if(!empty($sharedTripBoard) && isset($sharedTripBoard['TripBoard_detail']->tripboard_name)) {{ $sharedTripBoard['TripBoard_detail']->tripboard_name }} @endif</span> to your Tripboards?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center delete-btns">
                            <button type="button" class="btn btn-default red-btn-cmn approveBooking" onClick="tripboardAddTripboard({{ $sharedTripBoard['TripBoard_detail']->id }})">Yes</button><br>
                            <button type="button" class="btn btn-default close-btn-cmn" onClick="CloseMe()">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>
@endsection
@section('footerJs')
    @parent
<script>
    var tripboard_exits = '<?php echo Session::get('tripboard_exits'); ?>';
    var imageUrl = "{{URL::asset('uploads/property/slider1.jpg')}}";
    var pencilIcon = "{{URL::asset('media/images/board-pencil-icon.svg')}}";
    var propertyDetailURL = '{{ route("propertydetails.index", ":id") }}';
    var favUrl = "{{ route('favorites') }}";

   
   
    $(document).ready(function() {
        @if(!Auth::guard('rentaluser')->user())
            $('#login_popup').modal('show'); 
        @endif
        @if(!empty($sharedTripBoard) && $sharedTripBoard['shared'] == true)
            $("#tripboardConfirm").modal('show');
        @endif
        
            
        $('.savedFav').click(function(e) {
            e.preventDefault();
            if (!isRentalAuth) {
                $("#login_popup").modal('show');
            } else {
                // window.onbeforeunload = null;
                window.location = favUrl;
            }

        });

        function renderGrid(tripBoardJson) {
            var mediaPath = "{{asset('uploads/property/')}}";
            var fav = tripBoardJson.fav;
            var user = tripBoardJson.user;
            for (var x in fav) {
                var div = $("#favTripBlock").clone().prop({
                    id: "fav_" + fav[x].id
                }).appendTo($('#favTripboard'));
                var url = '{{ route("favorites") }}';
                //url = url.replace(':id', fav[x].property_id);
                $(div).find('.btn-explore-more-tripboards').prop({
                    href: url
                });
                $(div).find('.editIcon').prop({
                    href: url
                });
                $(div).find('.sharelink').prop({
                    id: fav[x].id
                });
                $(div).find('.boards-title').html(fav[x].tripboard_name);

                if (x == 0) {
                    $(div).find('.favorite-heart-icon').show()
                    $(div).find('.boards-title').html("Saved Favorites")
                    $(div).find('.boards-thumbnail-img')
                }
                div.show();
            }

            for (var x in user) {
                var div = $("#userTripBlock").clone().prop({
                    id: "fav_" + user[x].id
                }).appendTo($('#favTripboard'));
                div.show();
                var url = '{{ route("tripboard.detail", ":id") }}';
                url = url.replace(':id', user[x].id);
                $(div).find('.boards-title').html(user[x].tripboard_name);
                $(div).find('#tripboardId').val(user[x].id);


                $(div).find('.btn-explore-more-tripboards').prop({
                    href: url
                });
                $(div).find('.editIcon').prop({
                    href: url
                });
                $(div).find('.sharelink').prop({
                    id: user[x].id
                });

                $.ajax({
                    type: "POST",
                    url: "{{ route('view-images') }}",
                    dataType: "JSON",
                    headers: {
                        "Authorization": "Bearer {{session()->get('rental_token')}}",
                    },
                    data: {
                        'tripboardId': user[x].id
                    },
                    success: function(imagedata) {
                        if (imagedata.success == true && imagedata.data != '') {
                            if (imagedata.data != '') {

                                $.each(imagedata.data, function(key, val) {
                                    let propertyUrl = propertyDetailURL;
                                    propertyUrl = propertyUrl.replace(':id', val.property_id);
                                    if (key == 0) {
                                        $("#fav_" + val.tripboard_id + " .link1").attr('href', propertyUrl);
                                        $("#fav_" + val.tripboard_id + " .img-property1").attr('src', mediaPath + "/" + val.cover_photo);
                                    } else if (key == 1) {
                                        $("#fav_" + val.tripboard_id + " .link2").attr('href', propertyUrl);
                                        $("#fav_" + val.tripboard_id + " .img-property2").attr('src', mediaPath + "/" + val.cover_photo);
                                    } else if (key == 2) {
                                        $("#fav_" + val.tripboard_id + " .link3").attr('href', propertyUrl);
                                        $("#fav_" + val.tripboard_id + " .img-property3").attr('src', mediaPath + "/" + val.cover_photo);
                                    }
                                });
                            }
                        } else if(imagedata.data.length == 0 && imagedata.id.id !='') {
                            var url = '{{ route("tripboard.detail", ":id") }}';
                            url = url.replace(':id',imagedata.id.id);
                            $("#fav_"+imagedata.id.id).html(`<div class="pl-8 pr-8">
                                <div class="boards-white">
                                    <div class="d-block">
                                        <h3 class="boards-title">${imagedata.id.tripboard_name}</h3>
                                        <input type="hidden" id="tripboardId" name="tripboardId" value="${imagedata.id.id}">
                                        <span class="float-right hide_share_icons">
                                        <a id="shareIcon" onClick="shareLink(${imagedata.id.id})" class="mr-2 sharelink" style="cursor:pointer">
                                        <i class="fas fa-share-alt" ></i></a>
                                        <a href="${url}" class="editIcon">
                                        <img src="${pencilIcon}" alt=""></a></span>
                                    </div>
                                    <div class="border-separator-s"></div>
                                    <div class="d-block text-center no-fav-yet w-100">
                                        <i class="fa fa-suitcase" aria-hidden="true"></i>
                                        <div class="zero-state-text common-open w-100 pt-1 pb-1">You have no listings saved to this Tripboard yet.</div>
                                    </div>
                                    <div class="d-block text-center mt-5 mb-3 nolisting">
                                        <a class="btn-explore-more-tripboards button-default-animate" href="${url}">Explore more</a>
                                    </div>
                                </div>
                            </div>`);
                        }
                    },
                    error: function(xhr, status, error) {
                        // $('#moreresult').hide()
                        // $('#noReresultSpan').show()
                        // hideloader();
                    }
                });
            }
        }


        $.ajax({
            type: "POST",
            url: "{{ route('rental.api.tripboard') }}",
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
            },
            data: new FormData($('#searchForm')[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.success = 'true') {
                    var total = data.data;
                    if (total.fav.length == 0) {
                        $('#noFavBoard').show()

                    } else {
                        $('#noFavBoard').hide()
                    }
                    renderGrid(data.data);
                }
                // hideloader();
            },
            error: function(xhr, status, error) {
                $('#moreresult').hide()
                $('#noReresultSpan').show()
                hideloader();
            }
        });

        $('.btn-tripboard-fly').click(function(e) {
            e.preventDefault();

            var inputBoard = $('.input-board').val();
            $('.input-board').addClass('required');
            if (!$('#addTripBoard').valid()) {
                return false;
            }

            if (!isRentalAuth) {
                console.log(54);
                $("#login_popup").modal('show');
                return false;
            }

            showLoader();

            $.ajax({
                type: "POST",
                url: "{{ route('rental.api.tripboard.add') }}",
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('rental_token')}}",
                },
                data: new FormData($('#addTripBoard')[0]),
                processData: false,
                contentType: false,
                success: function(data) {
                    hideLoader();

                    if (data.success = 'true') {
                        // window.onbeforeunload = null;
                        var redirect_url = data.data['redirect_url'];
                        window.location.href = redirect_url;
                    }
                },
                error: function(xhr, status, error) {
                    hideLoader();
                    $('#moreresult').hide()
                    $('#noReresultSpan').show()
                    hideloader();
                }
            });
        });
        
    });

    function tripboardAddTripboard(id) {
        showLoader();
        $.ajax({
            type: "POST",
            url: "{{ route('tripboard-add') }}",
            dataType: "JSON",
            data: {id: id},
            success: function(msg) {
                $("#tripboardConfirm").modal('hide');
                Swal.fire({
                    icon: "success",
                    title: msg.message,
                    showConfirmButton: false,
                    timer: 3000
                }).then(() => {
                    window.location.href = "{{ route('tripboards') }}";
                });
                hideLoader();
            }
        });
    }

    function CloseMe(){
        window.location.href = '{{ route('tripboards') }}'
    }
    // share link
    var $temp = $("<input>");
    function shareLink(id) {
        $('#sharelink').modal('show');
        $("body").append($temp);
        str = '{{url('tripboards')}}?id='+id;
        $temp.val(str).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>
@endsection
