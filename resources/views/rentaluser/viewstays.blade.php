@extends('layouts.rentalApp')

@section('content')

    <!-- Hot deals slider -->
    <div class="stays-hot-deal-slider">
        <div class="container">
            <div class="hot-deal-header">
                <h1 class="hot-deals-title-cstm">Last Minute <strong>Hot Deals</strong></h1>
                <p class="text-center pera">View the hottest deals on GOT2GO before time runs out!</p>
            </div>
            <div class="owl-carousel owl-theme owl-centered stays-hot-deal-carousel" id="tripper-page-carousal">
                @if(count($hotDealsSlider)>0)
                    @foreach($hotDealsSlider as $item)
                        <div class="item">
                            <div class="tripper-carousal-div">
                            <a href="{{ url('/propertydetail/'.$item->propertyid)}}">
                                <div class="title">
                                    <h4><span>{{$item->city}},</span> {{$item->state}}</h4>
                                </div>
                                <div class="image">
                                    <img src="{{URL::asset('uploads/property/'.$item->cover_photo)}}" alt="photo" defer/>
                                </div>
                                <div class="extra-info">
                                    <h4>BOOK WITHIN 6 HOURS FOR</h4>
                                    <h2><s>${{rand(550,1000)}}</s>  <span class="blue">${{number_format($item->price,0)}}</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom">
                                    @for ($i = 1; $i <= number_format($item->avgRating,0); $i++)
                                        <i class="got got-star" aria-hidden="true"></i>
                                    @endfor
                                    {{number_format($item->avgRating,1)}} ({{$item->totalRating}})
                                    </h4>
                                </div>
                            </div>
                         </a>
                        </div>
                    @endforeach
                @endif
            </div>


        </div>
    </div>
    @endsection
    @section('footerJs')
    @parent
    <script>
        $(document).ready(function() {

            $('#tripper-page-carousal').owlCarousel({
                loop:true,
                margin:30,
                center: true,
                nav:true,
                dots: false,
                autoHeight: false,
                responsive:{
                    0:{
                        items:1
                    },
                    640:{
                        items:2
                    },
                    767:{
                        items:3
                    },
                    992:{
                        items:2
                    },
                    1399:{
                        items:3
                    }
                }
            });
            $('.btn-fly-go').click(function(){
                $('#filter_popup').modal("hide");
            });

            $('#filter_popup').on('show.bs.modal', function() {
                $('body').addClass("overflow-hidden");
                $('body').removeClass("overflow-y");
                $('#filter_popup').addClass("overflow-y");
                $('#modal_location_detail').val($('#location_detail').val())
                $('#modal_filter_dates').val($('#filter_dates_picker').val())
                $('#modal_adultchild').attr("value",$('#touchspin_1').val()+' Adults - '+$('#touchspin_2').val()+' Children')
            });

            $('#login_popup').on('show.bs.modal', function() {
                $("#reg_popup").modal("hide");
                $('body').addClass("overflow-hidden");
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
                $('body').addClass("overflow-hidden");
                $('#reg_popup').addClass("overflow-show");
            });

            var date = new Date();
            var year = date.getFullYear();
            $('#filter_dates_picker').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                minDate:moment().format('MM/DD/YYYY'),
                autoUpdateInput: false

            },function(start, end, label) {
                $('#filter_dates_picker').val( start.format('MMM DD, YYYY') + ' - ' + end.format('MMM DD, YYYY'));
                $('#filter_dates').val( start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            });




        });
    </script>

@endsection
