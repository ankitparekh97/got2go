@extends('layouts.rentalApp')
@section('content')
<section class="view-stays-banner">
</section>
<div class="row">
    <div class="col-md-4 col-lg-2">
      <button class="btn button-edit-search"><em class="fas fa-search"></em> Edit your search</button>
    </div>
  </div>
<form method="get" name="searchForm" autocomplete="off" id="searchForm">
   <div class="container  edit-search-form">
      <div class="row">
         <div class="col-xl-10">
            <section class="common-search-section">
               <div class="row">
                  <div class="col-lg-3 col-md-6">
                     <div class="input-group">
                        <img class="lazy-load" data-src="{{asset('media/images/location.svg')}}" alt="location">
                        <input type="text"  id="location_detail" autocomplete="off" value="{{request()->get('location_detail')}}" name="location_detail" class="form-control required location_detail" placeholder="Where are you going?">
                        <input type="hidden"  id="hdn_resort_id" value="{{request()->get('resort_id')}}" name="resort_id" class="form-control " >
                        <input type="hidden"  id="hdn_resort_total" value="0" name="hdn_resort_total" class="" >
                        <input type="hidden"  id="hdn_search_text" name="searchText"  value="{{request()->get('searchText')}}" >
                        <input type="hidden" name="address_selected_from_drop" class="address_selected_from_drop" value="{{request()->get('address_selected_from_drop')}}">

                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="input-group" >
                        <img class="lazy-load" data-src="{{asset('media/images/clock.svg')}}" alt="clock">
                        <input type="text"  readonly autocomplete="off" id="filter_dates_picker"  name="filter_dates_picker" value="{{request()->get('filter_dates_picker')}}" class="form-control autoSubmit" placeholder="When are you going?">
                        <input type="hidden"  id="filter_dates"  name="filter_dates" value="{{request()->get('filter_dates')}}" class="form-control required" placeholder="When are you going?">
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6">
                     <div id="achild" class="input-group">
                        <img class="lazy-load" data-src="{{asset('media/images/person.svg')}}" alt="person">
                        <input type="hidden" id="number_of_guest"  name="number_of_guest"  value="{{request()->get('number_of_guest')}}" class="form-control persons required" placeholder="Number of People">
                        <input type="text" autocomplete="off" value="{{request()->get('adults')}} Adult(s) - {{request()->get('child')}} Children" class="form-control adultchild" id="adultchild" role="button" data-toggle="dropdown" aria-haspopup="true" readonly aria-expanded="false" placeholder="{{request()->get('adults')}} Adult(s) - {{request()->get('child')}} Children">
                     </div>
                  </div>
                  <input type="hidden" id="pageno"  name="pageno" value="0"  >
                  <div class="col-lg-2 col-md-6 text-right">
                     <a href="" data-toggle="modal" data-target="#filter_popup" class="float-left"><img class="lazy-load" data-src="{{asset('media/images/filter.svg')}}" alt="filter"></a>
                     <button type="button" id="go_btn_viewstay" style="border:none;width:50px" class="gobutton"> <img class="lazy-load" data-src="{{URL::asset('media/images/gobutton.svg')}}" alt="go button"></button>
                  </div>
               </div>
            </section>
            <section class="search-result-radiobtns" id="wheretogo">
            <div class="checkboxes-inline res-checkbox-inline">
                <div id="responsive-proerty-selection">

                </div>
                </div>
            </section>
            <section class="common-search-section mt-5 mobile-view">

               <div class="row">
               <div class="col-12">
                     <a href="" data-toggle="modal" data-target="#filter_popup" class="float-left"><img class="lazy-load" data-src="{{asset('media/images/filter.svg')}}" alt="filter"> Advanced Filters</a>
                     <button type="button" id="go_btn_viewstay" style="border:none;width:100px;height:45px;background:#fce300;" class="gobutton float-right"><img class="lazy-load" data-src="{{asset('media/images/gobutton.svg')}}" alt="go button" style="max-height:100%;"></button>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   @include('rentaluser.advanceSearch')
   <div id="loader" class="loader" style="display: none;">
      <img class="lazy-load" data-src="{{URL::asset('media/images/loader.gif')}}" alt="Loader">
   </div>
   <div class="container mt-12 thumbnail-section-gridview mb-20 ">
      <div class="row">
         <div class="col-lg-8 col-12">
              <div class="vacation-club-header" style="display: none;">
                 <a href="javascript:void(0)" class="back-arrow-cstm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                 <h2 class="resortTitle" ></h2>
                 <p class="zero_stay"></p>
                 <p class="resortLocation">114 Units | Daytona Beach, Florida (USA)</p>
               </div>
            <div class="row" id="searchGrid" >
            </div>
            <div class="col-12 text-center mt-7">
               <button class="view-all-stays" type="button" style="display: none;" id="moreresult">View more stays</button>
               <span id="noReresultSpan" style="display: none;">No search results found, please try broadening your search.</span>
            </div>
         </div>
         <div class="col-lg-4 searchpage-right-panel pl-15 col-12">
            <div class="take-last-min-trip-box">
               <h1 class="take-last-min-text">
                  TAKING A LAST MINUTE TRIP?
               </h1>
               <div class="border-separator"></div>
               <div class="d-block mt-5">
                  <a data-toggle="modal" data-target="#hot_deals" class="view-hot-deals-btn">VIEW HOT DEALS</a>
               </div>
            </div>
            <div class="take-last-min-trip-box d-none d-xs-none d-sm-block d-md-block d-lg-block">
               <h1 class="take-last-min-text">
                  Property type
               </h1>
               <div class="border-separator"></div>
               <div class="d-block mt-5" id="property-type-desktop">
                  <ul class="property-type-list">
                     <li><label class="custom-checkbox">
                     <input name="property_type[]" {{  (in_array('vacation_rental',request()->get('property_type'))) ? 'checked="checked"' : '' }} value="vacation_rental" type="checkbox">
                        <span class="t-inner">Vacation Clubs</span>
                        <span class="checkmark">VACATION CLUBS</span>
                        </label>
                        <span class="numbers-of-property vacation_rental"> (0)</span>
                     </li>
                     <li><label class="custom-checkbox">
                        <input name="property_type[]" value="hotels" type="checkbox" {{  (in_array('hotels',request()->get('property_type'))) ? 'checked="checked"' : '' }}>
                        <span class="t-inner">Hotels</span>
                        <span class="checkmark">Hotels</span>
                        </label>
                        <span class="numbers-of-property hotels"> (0)</span>
                     </li>
                     <li><label class="custom-checkbox">
                        <input name="property_type[]" value="property" type="checkbox" {{  (in_array('property',request()->get('property_type'))) ? 'checked="checked"' : '' }}>
                        <span class="t-inner">Private residences</span>
                        <span class="checkmark">Private residences</span>
                        </label>
                        <span class="numbers-of-property property"> (0)</span>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="take-last-min-trip-box">
               <h1 class="take-last-min-text">
                  Recently viewed
               </h1>
               <div class="border-separator"></div>
               <div class="d-block mt-5">
                     @if(count(Helper::getRecentlyViewed()) > 0)
                        @foreach(Helper::getRecentlyViewed() as $item)
                            <div class="d-flex flax-wrap mb-4">
                                <a href="{{ url('/propertydetail/'.$item->id)}}" class="tile-outer-link">
                                    <div class="flex-shrink-0 mr-3">
                                        <div class="symbol symbol-50 symbol-lg-100">
                                            <img class="lazy-load" alt="Pic" data-src="{{asset('uploads/property/'.$item->cover_photo)}}">
                                        </div>
                                    </div>
                                    <div class="flex-grow-1">
                                        <div class="d-flex align-items-center justify-content-between flex-wrap">
                                            <div class="">
                                                <p class="name-place"> {{$item->title}}</p>
                                                <p class="price-of-place">${{$item->price}}/night</p>
                                                <ul class="recent-view-ul">
                                                    <li>{{$item->total_beds}} {{($item->total_beds <= 1) ? 'bed' : 'beds'}} </li>
                                                    <li>{{$item->total_bathroom}} {{($item->total_bathroom <= 1) ? 'bath' : 'baths'}} </li>
                                                    <li>{{$item->no_of_guest}} {{($item->no_of_guest <= 1) ? 'guest' : 'guests'}}</li>
                                                    <li>{{(($item->type_of_property == 'property') ? 'Private Residence' : 'Vacation Club')}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        No Recent properties found
                    @endif

               </div>
            </div>
         </div>
      </div>
   </div>
</form>


<!-- two column structure -->

<div class="container" style="display: none;">

    <div class="row"  >
        <div class="col-12"  id="VacationClubDiv" style="margin-bottom: 30px;">
            <div class="property-two-column">
                <div class="top-title">
                    <div class="row align-items-center">
                        <div class="col-md-7">
                            <h2 class="VCTitle">Americano Beach Resort</h2>
                            <p class="VCLocation">Daytona Beach, Florida ( USA )</p>
                        </div>
                        <div class="col-md-5">
                            <button type="button" class="view-all-units">View 14 Units</button>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-6 propertyBlock1"  id="propertyBlock1">
                      <div class="propertyblock-inner">
                       <div id="slider_thumbnail" class="carousel slide pointer-event slider_thumbnail animationcstm" data-ride="carousel" data-interval="false">
                          <div class="dollar-per-stay">
                             <p class="price1"><span class="sup">$</span><span class="priceTxt">650</span>/night</p>
                          </div>
                          <ol class="carousel-indicators">

                          </ol>
                          <div class="carousel-inner">

                          </div>
                          <a class="carousel-control-prev" href="#slider_thumbnail" role="button" data-slide="prev">
                              <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#slider_thumbnail" role="button" data-slide="next">
                              <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                          </a>
                       </div>
                       <!--begin::Top-->
                       <div class="propert-desription-search">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h2 class="resort-text"  >Bay View Suite</h2>
                            </div>

                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <p class="location-city-state-text">Americano Beach Resort</p>
                            </div>
                            <div class="col-md-12">
                                <div class="property-search-icon text-center">
                                    <div class="property-icon">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                                               viewBox="0 0 18.832 11.422">
                                               <path id="beds_amenity"
                                                  d="M262.408,974.707v3h1.508v-4.5H246.592v-6.15a.761.761,0,0,0-.221-.531.715.715,0,0,0-1.067,0,.761.761,0,0,0-.221.531v10.652h1.508v-3Zm-13.443-4.575a1.4,1.4,0,0,0,.993-.4,1.325,1.325,0,0,0,0-1.941,1.4,1.4,0,0,0-.993-.4,1.373,1.373,0,1,0,0,2.745Zm14.952,1.94v-1.83a.9.9,0,0,0-.313-.714,1.515,1.515,0,0,0-.791-.348l-10.924-1.062h-.074a.69.69,0,0,0-.7.7v2.123h-2.8q-.7,0-.7.567t.7.567Z"
                                                  transform="translate(-245.084 -966.287)" fill="#8e97ac" />
                                            </svg>
                                        </span>
                                        <span class="text bedTxt">4 beds</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                                               viewBox="0 0 19.786 12.497">
                                               <path id="bath"
                                                  d="M341.542,977.48l-.977-.977a8.132,8.132,0,0,1-1.323.111h-7.385a8.084,8.084,0,0,1-1.321-.111l-.978.977a.462.462,0,1,1-.653-.652l.593-.592c-1.9-.695-2.9-2.344-2.9-4.878h17.9c0,2.534-1,4.183-2.9,4.878l.592.592a.461.461,0,1,1-.652.652Zm-15.425-6.564a.462.462,0,1,1,0-.923h18.862a.462.462,0,1,1,0,.923Zm16.051-1.462a.241.241,0,0,1,0-.482l1.114-.22-1.238-2.613H339.8a1.22,1.22,0,0,1,1.347-1.022,1.358,1.358,0,0,1,1.273.7l1.561,3.292c.008.017,0,.037,0,.055a.187.187,0,0,1,.02.048c0,.015-.012.025-.014.04a.247.247,0,0,1-.034.082.221.221,0,0,1-.06.066c-.012.008-.014.025-.028.031a.251.251,0,0,1-.1.023Zm-.991-2.117a.12.12,0,0,1-.04-.166l.421-.691a.12.12,0,1,1,.205.124l-.419.693a.124.124,0,0,1-.1.057A.136.136,0,0,1,341.177,967.338Zm-.632,0a.122.122,0,0,1-.04-.166l.421-.691a.119.119,0,0,1,.166-.04.121.121,0,0,1,.04.165l-.421.693a.124.124,0,0,1-.1.057A.138.138,0,0,1,340.545,967.338Zm-.632,0a.12.12,0,0,1-.039-.166l.42-.691a.12.12,0,1,1,.206.124l-.422.693a.12.12,0,0,1-.1.057A.124.124,0,0,1,339.913,967.337Z"
                                                  transform="translate(-325.655 -965.119)" fill="#8e97ac" />
                                            </svg>
                                        </span>
                                        <span class="text bathTxt">3 bath</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                        <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M3.61295 13.7052H4.32575C4.52387 13.7057 4.68418 13.8665 4.68418 14.0646V16.5441C4.68418 16.7426 4.52325 16.9036 4.32473 16.9036H0.738358C0.643025 16.9036 0.551597 16.8657 0.484187 16.7983C0.416777 16.7309 0.378906 16.6395 0.378906 16.5441V14.0646C0.378906 13.9693 0.416777 13.8779 0.484187 13.8104C0.551597 13.743 0.643025 13.7052 0.738358 13.7052H1.45115V13.0799C1.45391 12.7999 1.68025 12.5736 1.96029 12.5708H3.10382C3.38385 12.5736 3.61019 12.7999 3.61295 13.0799V13.7052ZM2.00476 13.7052H3.05766L3.05664 13.1248H2.00476V13.7052Z" fill="#8E97AC"/>
                                            <circle cx="7.6067" cy="1.95191" r="1.34107" fill="#8E97AC"/>
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9165 10.9899H14.2047C14.3079 10.9912 14.4064 11.0337 14.4782 11.1078C14.5501 11.182 14.5894 11.2817 14.5875 11.385V15.8379C14.5875 16.0522 14.4138 16.2259 14.1996 16.2259H13.9114V16.6454C13.9114 16.7916 13.7929 16.9102 13.6466 16.9102C13.5004 16.9102 13.3819 16.7916 13.3819 16.6454V16.2259H12.2404V16.6454C12.2404 16.7916 12.1219 16.9102 11.9756 16.9102C11.8294 16.9102 11.7109 16.7916 11.7109 16.6454V16.2259H11.4227C11.2085 16.2259 11.0348 16.0522 11.0348 15.8379V11.385C11.0348 11.1707 11.2085 10.997 11.4227 10.997H11.7231V9.27818H11.4859C11.4518 9.27818 11.4192 9.26466 11.3951 9.2406C11.3711 9.21654 11.3576 9.18391 11.3576 9.14987V8.8281L9.89429 7.89536C9.70894 7.77737 9.59619 7.57334 9.59492 7.35364L9.5827 5.25802C9.5827 5.18323 9.52206 5.12259 9.44727 5.12259C9.37247 5.12259 9.31183 5.18323 9.31183 5.25802C9.31183 8.02933 9.3146 8.50174 9.3177 9.03021C9.32163 9.70099 9.32609 10.4621 9.32609 16.1291C9.31096 16.547 8.96781 16.878 8.54965 16.878C8.1315 16.878 7.78835 16.547 7.77322 16.1291V9.91766H7.43821V16.1291C7.43821 16.558 7.09058 16.9056 6.66177 16.9056C6.23296 16.9056 5.88533 16.558 5.88533 16.1291V5.24988C5.8795 5.18534 5.8254 5.13591 5.76059 5.13591C5.69579 5.13591 5.64169 5.18534 5.63586 5.24988L5.61244 10.0032C5.61075 10.3591 5.32175 10.6467 4.96583 10.6467C4.79416 10.6459 4.62986 10.5769 4.50914 10.4548C4.38842 10.3328 4.3212 10.1677 4.32228 9.99606L4.3457 5.24274C4.3496 4.4008 5.03117 3.71923 5.87311 3.71533H9.34646C10.1884 3.71923 10.87 4.4008 10.8739 5.24274L10.8841 6.98604L12.2078 7.83222C12.4597 7.99289 12.5688 8.30524 12.4715 8.58778H14.1537C14.2246 8.58778 14.282 8.64523 14.282 8.71609V9.14275C14.282 9.21361 14.2246 9.27105 14.1537 9.27105H13.9165V10.9899ZM12.2472 10.9901H13.3887V9.27124H12.2472V10.9901Z" fill="#8E97AC"/>
                                        </svg>
                                        </span>
                                        <span class="text guestTxt">3 Guests</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                             <path d="M15.0197 5.19078L7.83566 0.0820214C7.67566 -0.0273405 7.48366 -0.0273405 7.32366 0.0820214L3.86766 2.47236V1.33187C3.86766 1.12877 3.70766 0.972539 3.49966 0.972539H2.23566C2.02765 0.972539 1.86765 1.12877 1.86765 1.33187V3.84719L0.187652 5.19078C-0.164348 5.42513 0.0116519 5.97194 0.443653 5.97194H0.715653V13.6116C0.715653 13.9241 0.971653 14.1897 1.30765 14.1897H6.10766C6.26766 14.1897 6.39566 14.0647 6.39566 13.9085V10.6745C6.39566 10.5495 6.49166 10.4558 6.61966 10.4558H8.55567C8.68367 10.4558 8.77967 10.5495 8.77967 10.6745V13.9085C8.77967 14.0647 8.90767 14.1897 9.06767 14.1897H13.8677C14.1877 14.1897 14.4597 13.9397 14.4597 13.6116V5.97194H14.7477C15.1797 5.97194 15.3557 5.44075 15.0197 5.19078ZM4.82766 11.9087C4.82766 12.0337 4.73166 12.1274 4.60366 12.1274H3.01966C2.89166 12.1274 2.79566 12.0337 2.79566 11.9087V10.6745C2.79566 10.5495 2.89166 10.4558 3.01966 10.4558H4.60366C4.73166 10.4558 4.82766 10.5495 4.82766 10.6745V11.9087ZM4.82766 8.20604C4.82766 8.33103 4.73166 8.42477 4.60366 8.42477H3.01966C2.89166 8.42477 2.79566 8.33103 2.79566 8.20604V6.97182C2.79566 6.84683 2.89166 6.75309 3.01966 6.75309H4.60366C4.73166 6.75309 4.82766 6.84683 4.82766 6.97182V8.20604ZM8.60367 8.20604C8.60367 8.33103 8.50767 8.42477 8.37967 8.42477H6.79566C6.66766 8.42477 6.57166 8.33103 6.57166 8.20604V6.97182C6.57166 6.84683 6.66766 6.75309 6.79566 6.75309H8.37967C8.50767 6.75309 8.60367 6.84683 8.60367 6.97182V8.20604ZM12.3797 11.9087C12.3797 12.0337 12.2837 12.1274 12.1557 12.1274H10.5717C10.4437 12.1274 10.3477 12.0337 10.3477 11.9087V10.6745C10.3477 10.5495 10.4437 10.4558 10.5717 10.4558H12.1557C12.2837 10.4558 12.3797 10.5495 12.3797 10.6745V11.9087ZM12.3797 8.20604C12.3797 8.33103 12.2837 8.42477 12.1557 8.42477H10.5717C10.4437 8.42477 10.3477 8.33103 10.3477 8.20604V6.97182C10.3477 6.84683 10.4437 6.75309 10.5717 6.75309H12.1557C12.2837 6.75309 12.3797 6.84683 12.3797 6.97182V8.20604Z" fill="#8E97AC"/>
                                            </svg>
                                        </span>
                                        <span class="text villaTxt">Villa</span>
                                    </div>
                                </div>
                             </div>

                            </div>
                        </div>
                      </div>

                    </div>
                    <div class="col-12 col-md-6 propertyBlock2" id="propertyBlock1">
                      <div class="propertyblock-inner">
                       <div id="slider_thumbnail1" class="carousel slide pointer-event slider_thumbnail animationcstm" data-ride="carousel" data-interval="false">
                          <div class="dollar-per-stay">
                             <p class="price1"><span class="sup">$</span><span class="priceTxt">650</span>/night</p>
                          </div>
                          <ol class="carousel-indicators">

                          </ol>
                          <div class="carousel-inner">

                          </div>
                          <a class="carousel-control-prev" href="#slider_thumbnail1" role="button" data-slide="prev">
                              <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#slider_thumbnail1" role="button" data-slide="next">
                              <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                          </a>
                       </div>
                       <!--begin::Top-->
                       <div class="propert-desription-search">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h2 class="resort-text">Bay View Suite</h2>
                            </div>

                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <p class="location-city-state-text">Americano Beach Resort</p>
                            </div>
                            <div class="col-md-12">
                                <div class="property-search-icon text-center">
                                <div class="property-icon">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                                               viewBox="0 0 18.832 11.422">
                                               <path id="beds_amenity"
                                                  d="M262.408,974.707v3h1.508v-4.5H246.592v-6.15a.761.761,0,0,0-.221-.531.715.715,0,0,0-1.067,0,.761.761,0,0,0-.221.531v10.652h1.508v-3Zm-13.443-4.575a1.4,1.4,0,0,0,.993-.4,1.325,1.325,0,0,0,0-1.941,1.4,1.4,0,0,0-.993-.4,1.373,1.373,0,1,0,0,2.745Zm14.952,1.94v-1.83a.9.9,0,0,0-.313-.714,1.515,1.515,0,0,0-.791-.348l-10.924-1.062h-.074a.69.69,0,0,0-.7.7v2.123h-2.8q-.7,0-.7.567t.7.567Z"
                                                  transform="translate(-245.084 -966.287)" fill="#8e97ac" />
                                            </svg>
                                        </span>
                                        <span class="text bedTxt">4 beds</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                                               viewBox="0 0 19.786 12.497">
                                               <path id="bath"
                                                  d="M341.542,977.48l-.977-.977a8.132,8.132,0,0,1-1.323.111h-7.385a8.084,8.084,0,0,1-1.321-.111l-.978.977a.462.462,0,1,1-.653-.652l.593-.592c-1.9-.695-2.9-2.344-2.9-4.878h17.9c0,2.534-1,4.183-2.9,4.878l.592.592a.461.461,0,1,1-.652.652Zm-15.425-6.564a.462.462,0,1,1,0-.923h18.862a.462.462,0,1,1,0,.923Zm16.051-1.462a.241.241,0,0,1,0-.482l1.114-.22-1.238-2.613H339.8a1.22,1.22,0,0,1,1.347-1.022,1.358,1.358,0,0,1,1.273.7l1.561,3.292c.008.017,0,.037,0,.055a.187.187,0,0,1,.02.048c0,.015-.012.025-.014.04a.247.247,0,0,1-.034.082.221.221,0,0,1-.06.066c-.012.008-.014.025-.028.031a.251.251,0,0,1-.1.023Zm-.991-2.117a.12.12,0,0,1-.04-.166l.421-.691a.12.12,0,1,1,.205.124l-.419.693a.124.124,0,0,1-.1.057A.136.136,0,0,1,341.177,967.338Zm-.632,0a.122.122,0,0,1-.04-.166l.421-.691a.119.119,0,0,1,.166-.04.121.121,0,0,1,.04.165l-.421.693a.124.124,0,0,1-.1.057A.138.138,0,0,1,340.545,967.338Zm-.632,0a.12.12,0,0,1-.039-.166l.42-.691a.12.12,0,1,1,.206.124l-.422.693a.12.12,0,0,1-.1.057A.124.124,0,0,1,339.913,967.337Z"
                                                  transform="translate(-325.655 -965.119)" fill="#8e97ac" />
                                            </svg>
                                        </span>
                                        <span class="text bathTxt">3 bath</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                            <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M3.61295 13.7052H4.32575C4.52387 13.7057 4.68418 13.8665 4.68418 14.0646V16.5441C4.68418 16.7426 4.52325 16.9036 4.32473 16.9036H0.738358C0.643025 16.9036 0.551597 16.8657 0.484187 16.7983C0.416777 16.7309 0.378906 16.6395 0.378906 16.5441V14.0646C0.378906 13.9693 0.416777 13.8779 0.484187 13.8104C0.551597 13.743 0.643025 13.7052 0.738358 13.7052H1.45115V13.0799C1.45391 12.7999 1.68025 12.5736 1.96029 12.5708H3.10382C3.38385 12.5736 3.61019 12.7999 3.61295 13.0799V13.7052ZM2.00476 13.7052H3.05766L3.05664 13.1248H2.00476V13.7052Z" fill="#8E97AC"/>
                                                <circle cx="7.6067" cy="1.95191" r="1.34107" fill="#8E97AC"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9165 10.9899H14.2047C14.3079 10.9912 14.4064 11.0337 14.4782 11.1078C14.5501 11.182 14.5894 11.2817 14.5875 11.385V15.8379C14.5875 16.0522 14.4138 16.2259 14.1996 16.2259H13.9114V16.6454C13.9114 16.7916 13.7929 16.9102 13.6466 16.9102C13.5004 16.9102 13.3819 16.7916 13.3819 16.6454V16.2259H12.2404V16.6454C12.2404 16.7916 12.1219 16.9102 11.9756 16.9102C11.8294 16.9102 11.7109 16.7916 11.7109 16.6454V16.2259H11.4227C11.2085 16.2259 11.0348 16.0522 11.0348 15.8379V11.385C11.0348 11.1707 11.2085 10.997 11.4227 10.997H11.7231V9.27818H11.4859C11.4518 9.27818 11.4192 9.26466 11.3951 9.2406C11.3711 9.21654 11.3576 9.18391 11.3576 9.14987V8.8281L9.89429 7.89536C9.70894 7.77737 9.59619 7.57334 9.59492 7.35364L9.5827 5.25802C9.5827 5.18323 9.52206 5.12259 9.44727 5.12259C9.37247 5.12259 9.31183 5.18323 9.31183 5.25802C9.31183 8.02933 9.3146 8.50174 9.3177 9.03021C9.32163 9.70099 9.32609 10.4621 9.32609 16.1291C9.31096 16.547 8.96781 16.878 8.54965 16.878C8.1315 16.878 7.78835 16.547 7.77322 16.1291V9.91766H7.43821V16.1291C7.43821 16.558 7.09058 16.9056 6.66177 16.9056C6.23296 16.9056 5.88533 16.558 5.88533 16.1291V5.24988C5.8795 5.18534 5.8254 5.13591 5.76059 5.13591C5.69579 5.13591 5.64169 5.18534 5.63586 5.24988L5.61244 10.0032C5.61075 10.3591 5.32175 10.6467 4.96583 10.6467C4.79416 10.6459 4.62986 10.5769 4.50914 10.4548C4.38842 10.3328 4.3212 10.1677 4.32228 9.99606L4.3457 5.24274C4.3496 4.4008 5.03117 3.71923 5.87311 3.71533H9.34646C10.1884 3.71923 10.87 4.4008 10.8739 5.24274L10.8841 6.98604L12.2078 7.83222C12.4597 7.99289 12.5688 8.30524 12.4715 8.58778H14.1537C14.2246 8.58778 14.282 8.64523 14.282 8.71609V9.14275C14.282 9.21361 14.2246 9.27105 14.1537 9.27105H13.9165V10.9899ZM12.2472 10.9901H13.3887V9.27124H12.2472V10.9901Z" fill="#8E97AC"/>
                                            </svg>
                                        </span>
                                        <span class="text guestTxt">3 Guests</span>
                                    </div>
                                    <div class="property-icon">
                                        <span>
                                            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                             <path d="M15.0197 5.19078L7.83566 0.0820214C7.67566 -0.0273405 7.48366 -0.0273405 7.32366 0.0820214L3.86766 2.47236V1.33187C3.86766 1.12877 3.70766 0.972539 3.49966 0.972539H2.23566C2.02765 0.972539 1.86765 1.12877 1.86765 1.33187V3.84719L0.187652 5.19078C-0.164348 5.42513 0.0116519 5.97194 0.443653 5.97194H0.715653V13.6116C0.715653 13.9241 0.971653 14.1897 1.30765 14.1897H6.10766C6.26766 14.1897 6.39566 14.0647 6.39566 13.9085V10.6745C6.39566 10.5495 6.49166 10.4558 6.61966 10.4558H8.55567C8.68367 10.4558 8.77967 10.5495 8.77967 10.6745V13.9085C8.77967 14.0647 8.90767 14.1897 9.06767 14.1897H13.8677C14.1877 14.1897 14.4597 13.9397 14.4597 13.6116V5.97194H14.7477C15.1797 5.97194 15.3557 5.44075 15.0197 5.19078ZM4.82766 11.9087C4.82766 12.0337 4.73166 12.1274 4.60366 12.1274H3.01966C2.89166 12.1274 2.79566 12.0337 2.79566 11.9087V10.6745C2.79566 10.5495 2.89166 10.4558 3.01966 10.4558H4.60366C4.73166 10.4558 4.82766 10.5495 4.82766 10.6745V11.9087ZM4.82766 8.20604C4.82766 8.33103 4.73166 8.42477 4.60366 8.42477H3.01966C2.89166 8.42477 2.79566 8.33103 2.79566 8.20604V6.97182C2.79566 6.84683 2.89166 6.75309 3.01966 6.75309H4.60366C4.73166 6.75309 4.82766 6.84683 4.82766 6.97182V8.20604ZM8.60367 8.20604C8.60367 8.33103 8.50767 8.42477 8.37967 8.42477H6.79566C6.66766 8.42477 6.57166 8.33103 6.57166 8.20604V6.97182C6.57166 6.84683 6.66766 6.75309 6.79566 6.75309H8.37967C8.50767 6.75309 8.60367 6.84683 8.60367 6.97182V8.20604ZM12.3797 11.9087C12.3797 12.0337 12.2837 12.1274 12.1557 12.1274H10.5717C10.4437 12.1274 10.3477 12.0337 10.3477 11.9087V10.6745C10.3477 10.5495 10.4437 10.4558 10.5717 10.4558H12.1557C12.2837 10.4558 12.3797 10.5495 12.3797 10.6745V11.9087ZM12.3797 8.20604C12.3797 8.33103 12.2837 8.42477 12.1557 8.42477H10.5717C10.4437 8.42477 10.3477 8.33103 10.3477 8.20604V6.97182C10.3477 6.84683 10.4437 6.75309 10.5717 6.75309H12.1557C12.2837 6.75309 12.3797 6.84683 12.3797 6.97182V8.20604Z" fill="#8E97AC"/>
                                            </svg>
                                        </span>
                                        <span class="text villaTxt">Villa</span>
                                    </div>
                                </div>
                             </div>

                            </div>
                        </div>
                      </div>

                    </div>


                       <!--end::Bottom-->
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<!-- two column structure END-->


<!-- DIV for cloning  -->
<div class="col-12" id="propertyBlock" style="display:none" >
   <div id="slider_thumbnail" class="carousel slide  pointer-event slider_thumbnail animationcstm" data-ride="carousel" data-interval="false">
      <div class="dollar-per-stay">
         <p class="price1"><span class="sup">$</span><span class="priceTxt">650</span>/night</p>
      </div>
      <ol class="carousel-indicators">
      </ol>
      <div class="carousel-inner">
      </div>
      <a class="carousel-control-prev" href="#slider_thumbnail_58" role="button" data-slide="prev">
          <span class="bg-Left carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#slider_thumbnail_58" role="button" data-slide="next">
          <span class="bg-right carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
      </a>

   </div>
   <!--begin::Top-->
   <div class="propert-desription-search">
    <div class="row align-items-center">
        <div class="col-9">
            <h2 class="resort-text">Americano Beach Resort</h2>
        </div>
        <div class="col-3 text-right">
            @if(Auth::guard('rentaluser')->user() )
                <div class="heart-favourite" data-val="0">
                    <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M11.418 3.54229C12.2666 2.41437 13.4894 1.62587 14.8669 1.31837C18.7761 0.493571 22.28 4.57686 21.3615 8.83631C20.7551 11.2597 19.4189 13.4383 17.5338 15.0773C15.761 16.7911 13.8303 18.3083 11.9465 19.8816C11.7265 20.0498 11.4347 20.0911 11.1767 19.9905C8.07079 17.9083 5.33871 15.3167 3.09565 12.3249C1.61348 10.445 1.09069 7.98231 1.68126 5.66234C2.90319 1.1361 8.03327 -0.264027 11.0952 3.05759C11.2145 3.21109 11.3224 3.37309 11.418 3.54229Z" stroke="" stroke-width="2"/>
                    </svg>
                </div>
            @else
                <div class="heart-favourite" data-toggle="modal" data-target="#login_popup">
                    <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M11.418 3.54229C12.2666 2.41437 13.4894 1.62587 14.8669 1.31837C18.7761 0.493571 22.28 4.57686 21.3615 8.83631C20.7551 11.2597 19.4189 13.4383 17.5338 15.0773C15.761 16.7911 13.8303 18.3083 11.9465 19.8816C11.7265 20.0498 11.4347 20.0911 11.1767 19.9905C8.07079 17.9083 5.33871 15.3167 3.09565 12.3249C1.61348 10.445 1.09069 7.98231 1.68126 5.66234C2.90319 1.1361 8.03327 -0.264027 11.0952 3.05759C11.2145 3.21109 11.3224 3.37309 11.418 3.54229Z" stroke="" stroke-width="2"/>
                    </svg>
                </div>
            @endif

        </div>
    </div>
    <div class="row align-items-center">
        <div class="col-md-5">
            <p class="location-city-state-text">Daytona Beach, Florida ( USA )</p>
        </div>
        <div class="col-md-7">
            <div class="property-search-icon text-right">
            <div class="property-icon">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                        viewBox="0 0 18.832 11.422">
                        <path id="beds_amenity"
                            d="M262.408,974.707v3h1.508v-4.5H246.592v-6.15a.761.761,0,0,0-.221-.531.715.715,0,0,0-1.067,0,.761.761,0,0,0-.221.531v10.652h1.508v-3Zm-13.443-4.575a1.4,1.4,0,0,0,.993-.4,1.325,1.325,0,0,0,0-1.941,1.4,1.4,0,0,0-.993-.4,1.373,1.373,0,1,0,0,2.745Zm14.952,1.94v-1.83a.9.9,0,0,0-.313-.714,1.515,1.515,0,0,0-.791-.348l-10.924-1.062h-.074a.69.69,0,0,0-.7.7v2.123h-2.8q-.7,0-.7.567t.7.567Z"
                            transform="translate(-245.084 -966.287)" fill="#8e97ac" />
                    </svg>
                </span>
                <span class="text bedTxt">4 beds</span>
            </div>
            <div class="property-icon">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29"
                        viewBox="0 0 19.786 12.497">
                        <path id="bath"
                            d="M341.542,977.48l-.977-.977a8.132,8.132,0,0,1-1.323.111h-7.385a8.084,8.084,0,0,1-1.321-.111l-.978.977a.462.462,0,1,1-.653-.652l.593-.592c-1.9-.695-2.9-2.344-2.9-4.878h17.9c0,2.534-1,4.183-2.9,4.878l.592.592a.461.461,0,1,1-.652.652Zm-15.425-6.564a.462.462,0,1,1,0-.923h18.862a.462.462,0,1,1,0,.923Zm16.051-1.462a.241.241,0,0,1,0-.482l1.114-.22-1.238-2.613H339.8a1.22,1.22,0,0,1,1.347-1.022,1.358,1.358,0,0,1,1.273.7l1.561,3.292c.008.017,0,.037,0,.055a.187.187,0,0,1,.02.048c0,.015-.012.025-.014.04a.247.247,0,0,1-.034.082.221.221,0,0,1-.06.066c-.012.008-.014.025-.028.031a.251.251,0,0,1-.1.023Zm-.991-2.117a.12.12,0,0,1-.04-.166l.421-.691a.12.12,0,1,1,.205.124l-.419.693a.124.124,0,0,1-.1.057A.136.136,0,0,1,341.177,967.338Zm-.632,0a.122.122,0,0,1-.04-.166l.421-.691a.119.119,0,0,1,.166-.04.121.121,0,0,1,.04.165l-.421.693a.124.124,0,0,1-.1.057A.138.138,0,0,1,340.545,967.338Zm-.632,0a.12.12,0,0,1-.039-.166l.42-.691a.12.12,0,1,1,.206.124l-.422.693a.12.12,0,0,1-.1.057A.124.124,0,0,1,339.913,967.337Z"
                            transform="translate(-325.655 -965.119)" fill="#8e97ac" />
                    </svg>
                </span>
                <span class="text bathTxt">3 bath</span>
            </div>
            <div class="property-icon">
                <span>
                    <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.61295 13.7052H4.32575C4.52387 13.7057 4.68418 13.8665 4.68418 14.0646V16.5441C4.68418 16.7426 4.52325 16.9036 4.32473 16.9036H0.738358C0.643025 16.9036 0.551597 16.8657 0.484187 16.7983C0.416777 16.7309 0.378906 16.6395 0.378906 16.5441V14.0646C0.378906 13.9693 0.416777 13.8779 0.484187 13.8104C0.551597 13.743 0.643025 13.7052 0.738358 13.7052H1.45115V13.0799C1.45391 12.7999 1.68025 12.5736 1.96029 12.5708H3.10382C3.38385 12.5736 3.61019 12.7999 3.61295 13.0799V13.7052ZM2.00476 13.7052H3.05766L3.05664 13.1248H2.00476V13.7052Z" fill="#8E97AC"/>
                        <circle cx="7.6067" cy="1.95191" r="1.34107" fill="#8E97AC"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9165 10.9899H14.2047C14.3079 10.9912 14.4064 11.0337 14.4782 11.1078C14.5501 11.182 14.5894 11.2817 14.5875 11.385V15.8379C14.5875 16.0522 14.4138 16.2259 14.1996 16.2259H13.9114V16.6454C13.9114 16.7916 13.7929 16.9102 13.6466 16.9102C13.5004 16.9102 13.3819 16.7916 13.3819 16.6454V16.2259H12.2404V16.6454C12.2404 16.7916 12.1219 16.9102 11.9756 16.9102C11.8294 16.9102 11.7109 16.7916 11.7109 16.6454V16.2259H11.4227C11.2085 16.2259 11.0348 16.0522 11.0348 15.8379V11.385C11.0348 11.1707 11.2085 10.997 11.4227 10.997H11.7231V9.27818H11.4859C11.4518 9.27818 11.4192 9.26466 11.3951 9.2406C11.3711 9.21654 11.3576 9.18391 11.3576 9.14987V8.8281L9.89429 7.89536C9.70894 7.77737 9.59619 7.57334 9.59492 7.35364L9.5827 5.25802C9.5827 5.18323 9.52206 5.12259 9.44727 5.12259C9.37247 5.12259 9.31183 5.18323 9.31183 5.25802C9.31183 8.02933 9.3146 8.50174 9.3177 9.03021C9.32163 9.70099 9.32609 10.4621 9.32609 16.1291C9.31096 16.547 8.96781 16.878 8.54965 16.878C8.1315 16.878 7.78835 16.547 7.77322 16.1291V9.91766H7.43821V16.1291C7.43821 16.558 7.09058 16.9056 6.66177 16.9056C6.23296 16.9056 5.88533 16.558 5.88533 16.1291V5.24988C5.8795 5.18534 5.8254 5.13591 5.76059 5.13591C5.69579 5.13591 5.64169 5.18534 5.63586 5.24988L5.61244 10.0032C5.61075 10.3591 5.32175 10.6467 4.96583 10.6467C4.79416 10.6459 4.62986 10.5769 4.50914 10.4548C4.38842 10.3328 4.3212 10.1677 4.32228 9.99606L4.3457 5.24274C4.3496 4.4008 5.03117 3.71923 5.87311 3.71533H9.34646C10.1884 3.71923 10.87 4.4008 10.8739 5.24274L10.8841 6.98604L12.2078 7.83222C12.4597 7.99289 12.5688 8.30524 12.4715 8.58778H14.1537C14.2246 8.58778 14.282 8.64523 14.282 8.71609V9.14275C14.282 9.21361 14.2246 9.27105 14.1537 9.27105H13.9165V10.9899ZM12.2472 10.9901H13.3887V9.27124H12.2472V10.9901Z" fill="#8E97AC"/>
                    </svg>
                </span>
                <span class="text guestTxt">3 Guests</span>
            </div>
            <div class="property-icon">
                <span>
                    <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.0197 5.19078L7.83566 0.0820214C7.67566 -0.0273405 7.48366 -0.0273405 7.32366 0.0820214L3.86766 2.47236V1.33187C3.86766 1.12877 3.70766 0.972539 3.49966 0.972539H2.23566C2.02765 0.972539 1.86765 1.12877 1.86765 1.33187V3.84719L0.187652 5.19078C-0.164348 5.42513 0.0116519 5.97194 0.443653 5.97194H0.715653V13.6116C0.715653 13.9241 0.971653 14.1897 1.30765 14.1897H6.10766C6.26766 14.1897 6.39566 14.0647 6.39566 13.9085V10.6745C6.39566 10.5495 6.49166 10.4558 6.61966 10.4558H8.55567C8.68367 10.4558 8.77967 10.5495 8.77967 10.6745V13.9085C8.77967 14.0647 8.90767 14.1897 9.06767 14.1897H13.8677C14.1877 14.1897 14.4597 13.9397 14.4597 13.6116V5.97194H14.7477C15.1797 5.97194 15.3557 5.44075 15.0197 5.19078ZM4.82766 11.9087C4.82766 12.0337 4.73166 12.1274 4.60366 12.1274H3.01966C2.89166 12.1274 2.79566 12.0337 2.79566 11.9087V10.6745C2.79566 10.5495 2.89166 10.4558 3.01966 10.4558H4.60366C4.73166 10.4558 4.82766 10.5495 4.82766 10.6745V11.9087ZM4.82766 8.20604C4.82766 8.33103 4.73166 8.42477 4.60366 8.42477H3.01966C2.89166 8.42477 2.79566 8.33103 2.79566 8.20604V6.97182C2.79566 6.84683 2.89166 6.75309 3.01966 6.75309H4.60366C4.73166 6.75309 4.82766 6.84683 4.82766 6.97182V8.20604ZM8.60367 8.20604C8.60367 8.33103 8.50767 8.42477 8.37967 8.42477H6.79566C6.66766 8.42477 6.57166 8.33103 6.57166 8.20604V6.97182C6.57166 6.84683 6.66766 6.75309 6.79566 6.75309H8.37967C8.50767 6.75309 8.60367 6.84683 8.60367 6.97182V8.20604ZM12.3797 11.9087C12.3797 12.0337 12.2837 12.1274 12.1557 12.1274H10.5717C10.4437 12.1274 10.3477 12.0337 10.3477 11.9087V10.6745C10.3477 10.5495 10.4437 10.4558 10.5717 10.4558H12.1557C12.2837 10.4558 12.3797 10.5495 12.3797 10.6745V11.9087ZM12.3797 8.20604C12.3797 8.33103 12.2837 8.42477 12.1557 8.42477H10.5717C10.4437 8.42477 10.3477 8.33103 10.3477 8.20604V6.97182C10.3477 6.84683 10.4437 6.75309 10.5717 6.75309H12.1557C12.2837 6.75309 12.3797 6.84683 12.3797 6.97182V8.20604Z" fill="#8E97AC"/>
                    </svg>
                </span>
                <span class="text villaTxt">Villa</span>
            </div>
            </div>
         </div>

        </div>
    </div>
   </div>
   <!--end::Bottom-->
</div>

@include('rentaluser.hotDealsModal')
@endsection
@section('footerJs')
@parent
<script>
    var PropertyTypes = @JSON($propertyType);
    var getDates = "{{request()->get('filter_dates')}}";
    getDates = getDates.split(' - ');
    var fromDate = getDates[0];
    var toDate = getDates[1];
    var adults = "{{request()->get('adults')}}";
    var child = "{{request()->get('child')}}";
    var guest = parseInt(adults) + parseInt(child);
    var autocomplete;
    var mediaPath =  "{{asset('uploads/property/')}}";
    var propertyDetailURL = '{{ route("propertydetails.index", ":id") }}';
    var searchRequestURL = "{{ route('rental.api.search') }}";
    var hotdealsRequestURL = "{{ route('rental.api.hotdeals') }}";
    var searchCountRequestURL = "{{ route('rental.api.searchcount') }}";
    var Authorization = "Bearer {{session()->get('owner_token')}}";
    var likeUrl = "{{route('rental.api.tripboard.like') }}";
    var unlikeUrl = "{{route('rental.api.tripboard.unlike') }}";
</script>
<script src="{{URL::asset('js/search/search.js')}}"></script>
<script>
$(document).ready(function(){
  $(".button-edit-search").click(function(){
    $(".edit-search-form").slideDown("slow");
  });
});

jQuery( document ).ready(function() {

    var windowSize = $(window).width();

        if (windowSize <= 767) {
            jQuery('ul.property-type-list').prependTo('#responsive-proerty-selection');
        }
        else{
            jQuery('ul.property-type-list').prependTo('#property-type-desktop');
        }
});
jQuery( window ).resize(function() {
    var windowSize = $(window).width();

        if (windowSize <= 767) {
            jQuery('ul.property-type-list').prependTo('#responsive-proerty-selection');
        }
        else{
            jQuery('ul.property-type-list').prependTo('#property-type-desktop');
        }

});
</script>
@endsection
