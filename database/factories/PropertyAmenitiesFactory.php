<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PropertyAmenities;
use Faker\Generator as Faker;


$factory->define(PropertyAmenities::class, function (Faker $faker) {

    return [
        'property_type' => 'property',
        'amenity_id' => $faker->randomElement(PropertySeeder::$amenities),//$faker->unique()->numberBetween(1,count($amenities)),
        'amenities_type' => 'unit'
    ];
});
