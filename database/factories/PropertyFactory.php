<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Property;
use Faker\Generator as Faker;
$i=0;
$factory->define(Property::class, function (Faker $faker) use ($i) {

    $location = PropertySeeder::$location[$faker->numberBetween(0,999)];

    if(!isset($location['city']))
    {
        $location['city'] = $location['state'];
    }
    $i++;
    $location_detail = $location['address1'].', '.$location['city'].', '.$location['state'].', USA';
    $propertyType = $faker->randomElement(['property','vacation_rental']);
    $resortId = ($propertyType == 'property') ? 0 : $faker->numberBetween(1,7);
    $property_type_id = ($propertyType == 'property') ? $faker->numberBetween(1,7) : $faker->numberBetween(10,15);

    return [
        'owner_id' => $faker->numberBetween(1,7),
        'resort_id' => $resortId,
        'title' => $faker->streetName,
        'description' =>  $faker->realText(180),
        'about_the_space' =>  $faker->realText(180),
        'property_type_id' => $property_type_id,
        //'location_detail' =>  $faker->streetAddress.', '.$faker->city.', '.$faker->state.', '.$faker->country,
        'location_detail' =>  $location_detail,
        'lat_lang' =>  serialize($location['coordinates']),
        'city' => $location['city'],
        'state' => $location['state'],
        'state_abbrv'=> $location['state'],
        'country' => 'USA',
        'zipcode' => $location['postalCode'],
        'type_of_property' => $propertyType,

        'guest_will_have' => $faker->randomElement(['0', '1']),
        'is_for_guest' => $faker->randomElement(['0', '1']),
        'list_as_company' =>  $faker->randomElement(['0', '1']),
        'no_of_guest' => $faker->numberBetween(1,10),
        'total_bedroom' => $faker->numberBetween(1,10),
        'total_bathroom' => $faker->numberBetween(1,10),
        'total_beds' => $faker->numberBetween(1,10),
        'proof_of_ownership_or_lease' =>  'property_proof_1595922938.jpeg',
        'gov_issue_id' =>  'property_proof_1595922938.jpeg',
        'is_instant_booking_allowed' =>  $faker->randomElement(['0', '1']),


        'cover_photo' =>  'property.jpeg',
        'price' =>  $faker->numberBetween(80,500),
        'is_price_negotiable' =>  $faker->randomElement(['0', '1']),
        'is_partial_payment_allowed'=> $faker->randomElement(['0', '1']),
        'partial_payment_amount' => $faker->numberBetween(80,100),
        'check_in' =>  $faker->time('H:i'),
        'check_out' =>  $faker->time('H:i'),'cancellation_type' => $faker->randomElement(['Flexible', 'Moderate', 'Strict']),
        'publish' => $faker->randomElement(['0', '1']),
        'is_hotdeals' => $faker->randomElement(['0', '1']),
    ];
});
