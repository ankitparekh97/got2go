<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OwnerProperty;
use Faker\Generator as Faker;

$factory->define(OwnerProperty::class, function (Faker $faker) {
    return [
        // $title = ucwords($faker->catchPhrase .' '.$faker->bs);
        'owner_id' => $faker->numberBetween(1,7),
        'guest_will_have' => $faker->randomElement(['private', 'entire']),
        'is_for_guest' => $faker->randomElement(['0', '1']),
        'list_as_company' =>  $faker->randomElement(['0', '1']),
        'no_of_guest' => $faker->numberBetween(1,10),
        'common_room' => $faker->numberBetween(1,8),
        'sofa' => $faker->numberBetween(1,8),
        'common_bathroom' => $faker->numberBetween(1,8),
        'proof_of_ownership_or_lease' =>  'property_proof_1595922938.jpeg',
        'is_instant_booking_allowed' =>  $faker->randomElement(['0', '1']),
        'check_in' =>  $faker->time('H:i'),
        'check_out' =>  $faker->time('H:i'),
        'cover_photo' =>  'property.jpeg',
        'price' =>  $faker->numberBetween(80,500),
        'service_fees' =>  $faker->numberBetween(2,10),
        'occupancy_taxes_and_fees' =>  $faker->numberBetween(10,50),
        'is_partial_payment_allowed'=> $faker->randomElement(['0', '1']),
        'partial_payment_amount' => $faker->numberBetween(80,100),
        'cancellation_type' => $faker->randomElement(['Flexible', 'Moderate', 'Strict']),
        'publish' => $faker->randomElement(['0', '1']),
    ];
});
