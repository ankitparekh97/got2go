<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PropertyImages;
use Faker\Generator as Faker;
$autoIncrement = autoIncrement();

$factory->define(PropertyImages::class, function (Faker $faker) use (&$autoIncrement) {


    if (is_null($autoIncrement->current()))
    {
        $autoIncrement = autoIncrement();
        //$autoIncrement->next();
    }

    $ext = '.jpeg';
    $photo =  'property_photo_'.$autoIncrement->current().$ext;
    $autoIncrement->next();
    return [
        'photo' => $photo
   ];
});

function autoIncrement()
{
    for ($i = 1; $i <= 5; $i++) {
        yield $i;
    }
}
