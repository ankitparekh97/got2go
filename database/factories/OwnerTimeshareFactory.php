<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OwnerTimeshareProperty;
use Faker\Generator as Faker;

$factory->define(OwnerTimeshareProperty::class, function (Faker $faker) {
    $random = rand(1,50);
    $startdate = date('Y-m-d', strtotime('+'.$random.' day', time()));
    
    $erandom = rand(1,10);
    $enddate = date('Y-m-d', strtotime('+'.$erandom.' day', strtotime($startdate)));

    return [
        // $title = ucwords($faker->catchPhrase .' '.$faker->bs);
        'owner_id' => $faker->numberBetween(1,7),
        //'guest_will_have' => $faker->randomElement(['private', 'entire']),
        //'is_for_guest' => $faker->randomElement(['0', '1']),
        //'list_as_company' =>  $faker->randomElement(['0', '1']),
        'no_of_guest' => $faker->numberBetween(1,10),
        'unit_size' => $faker->numberBetween(1,8),
        
        'check_in_date' => $startdate,
        'check_out_date' => $enddate,
        'price' =>  $faker->numberBetween(80,500),
        'service_fees' =>  $faker->numberBetween(2,10),
        'occupancy_taxes_and_fees' =>  $faker->numberBetween(10,50),
        'is_partial_payment_allowed'=> $faker->randomElement(['0', '1']),
        'partial_payment_amount' => $faker->numberBetween(80,100),
        'cancellation_type' => $faker->randomElement(['Flexible', 'Moderate', 'Strict']),
        'publish' => $faker->randomElement(['0', '1']),
        'ltd_time_frame' => $faker->numberBetween(80,100),
        'ltd_value' => $faker->numberBetween(80,100)
    ];
});
