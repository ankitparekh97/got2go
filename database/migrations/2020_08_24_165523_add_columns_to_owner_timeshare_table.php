<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOwnerTimeshareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_timeshare_property', function (Blueprint $table) {
            $table->dropForeign('owner_timeshare_property_hotels_id_foreign');
            $table->dropColumn(['hotels_id']);
            $table->unsignedBigInteger('owner_id')->default(0)->after('id');
            $table->decimal('price',10,2)->after('allow_booking_before');
            $table->decimal('service_fees',10,2)->after('price');
            $table->decimal('occupancy_taxes_and_fees',10,2)->after('service_fees');
            $table->enum('is_partial_payment_allowed',['0','1'])->default('0')->after('occupancy_taxes_and_fees');
            $table->decimal('partial_payment_amount',10,2)->after('is_partial_payment_allowed');
            $table->string('cancellation_type');
            $table->enum('publish',[0,1])->default(0);
            //$table->foreign('owner_id')->references('id')->on('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_timeshare_property', function (Blueprint $table) {
            //
        });
    }
}
