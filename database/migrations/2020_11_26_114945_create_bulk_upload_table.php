<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBulkUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_upload', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->text('property_doc')->nullable();
            $table->text('property_images')->nullable();
            $table->text('verfication_doc')->nullable();
            $table->text('govtid_doc')->nullable();
            $table->enum('status',['pending','in-progress','completed'])->default('pending');
            $table->text('unique_key')->nullable();
            $table->foreign('owner_id')->references('id')->on('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_upload');
    }
}
