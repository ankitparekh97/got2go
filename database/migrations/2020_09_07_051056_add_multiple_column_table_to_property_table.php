<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnTableToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function (Blueprint $table) {
            // about_space
            $table->unsignedBigInteger('owner_id')->after('id');
            $table->unsignedBigInteger('resort_id')->after('owner_id')->default(0);
            $table->unsignedBigInteger('sub_property_type_id')->after('property_type_id');
            $table->enum('is_for_guest',['0','1'])->default(0)->comment('0=no|1=yes')->after('location_detail');
            $table->enum('list_as_company',['0','1'])->default(0)->comment('0=no|1=yes')->after('is_for_guest');
            $table->integer('zipcode')->after('country');
            $table->integer('no_of_guest')->default(1)->after('zipcode');
            $table->integer('total_bedroom')->after('no_of_guest')->default(1)->nullable();
            $table->integer('total_bathroom')->after('total_bedroom')->default(1)->nullable();
            $table->integer('total_beds')->after('total_bathroom')->default(1)->nullable();
            $table->enum('is_instant_booking_allowed',['0','1'])->default(0)->after('lat_lang');
            $table->decimal('price',10,2)->after('is_instant_booking_allowed');
            $table->enum('is_price_negotiable',['0','1'])->default('0')->after('price');
            $table->enum('is_partial_payment_allowed',['0','1'])->default('0')->after('is_price_negotiable');
            $table->decimal('partial_payment_amount',10,2)->after('is_partial_payment_allowed');
            $table->time('check_in')->after('partial_payment_amount');
            $table->time('check_out')->after('check_in');
            $table->date('available_from')->after('check_out');
            $table->date('available_to')->after('available_from');
            $table->date('unavailable_from')->after('available_to');
            $table->date('unavailable_to')->after('unavailable_from');
            $table->string('proof_of_ownership_or_lease')->nullable()->after('unavailable_to');
            $table->string('gov_issue_id')->nullable()->after('proof_of_ownership_or_lease');
            $table->string('cancellation_type')->after('gov_issue_id');
            $table->enum('publish',[0,1])->default(0)->after('cancellation_type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function (Blueprint $table) {
            //
        });
    }
}
