<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameProofOfOwnershipOrLeaseToDocumentNameInPropertyOwnershipDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_ownership_documents', function (Blueprint $table) {
            $table->renameColumn('proof_of_ownership_or_lease', 'document_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_name_in_property_ownership_documents', function (Blueprint $table) {
            //
        });
    }
}
