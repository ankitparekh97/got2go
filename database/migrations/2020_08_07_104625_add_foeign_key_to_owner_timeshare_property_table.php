<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFoeignKeyToOwnerTimesharePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::table('owner_timeshare_property', function (Blueprint $table) {
            $table->dropColumn('hotel_id');
            $table->dropForeign('owner_timeshare_property_hotel_id_foreign');
            
        }); */

        Schema::table('owner_timeshare_property', function($table) {
            $table->dropColumn('hotel_id');
            $table->bigInteger('hotels_id')->unsigned()->nullable()->after('property_id');
            $table->foreign('hotels_id')->references('id')->on('hotels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_timeshare_property', function (Blueprint $table) {
            //
        });
    }
}
