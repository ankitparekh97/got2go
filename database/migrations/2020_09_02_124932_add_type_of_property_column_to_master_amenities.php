<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeOfPropertyColumnToMasterAmenities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_amenities', function (Blueprint $table) {
            $table->enum('property_type',['property','vacation_rental','hotels'])->default('property')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_amenities', function (Blueprint $table) {
            //
        });
    }
}
