<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameGovtIssueIdInPropertyOwnershipDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_ownership_documents', function (Blueprint $table) {
            $table->integer('gov_issue_id')->change()->default(0);
            $table->renameColumn('gov_issue_id', 'is_govt_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_ownership_documents', function (Blueprint $table) {
            //
        });
    }
}
