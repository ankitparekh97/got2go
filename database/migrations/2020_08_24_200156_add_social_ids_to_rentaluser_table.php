<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSocialIdsToRentaluserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rental_user', function (Blueprint $table) {
           // $table->string('google_id')->after('email')->nullable();
            //$table->string('facebook_id')->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rentaluser', function (Blueprint $table) {
            $table->dropColumn(['google_id', 'facebook_id']);
        });
    }
}
