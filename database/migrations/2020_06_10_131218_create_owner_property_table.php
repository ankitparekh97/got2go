<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property', function (Blueprint $table) {
            $table->id();
            $table->integer('owner_id');
            $table->string('name_your_place')->nullable();
            $table->string('property_type')->nullable();
            $table->text('description')->nullable();
            $table->string('guest_will_have')->nullable();
            $table->string('location_detail')->nullable();
            $table->integer('common_room')->nullable();
            $table->integer('sofa')->nullable();
            $table->integer('common_bathroom')->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('bed')->nullable();
            $table->integer('bed_type')->nullable();
            $table->integer('guest_per_room')->nullable();
            $table->string('proof_of_ownership_or_lease')->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('service_fees', 10, 2);
            $table->decimal('occupancy_taxes_and_fees', 10, 2);
            $table->enum('is_instant_booking_allowed',['0','1'])->default(0);
            $table->enum('is_partial_payment_allowed',['0','1'])->default(0);
            $table->decimal('partial_payment_amount', 10, 2)->nullable();
            $table->time('check_in',0);
            $table->time('check_out',0);
            $table->string('cancellation_type')->nullable();
            $table->string('photo')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property');
    }
}
