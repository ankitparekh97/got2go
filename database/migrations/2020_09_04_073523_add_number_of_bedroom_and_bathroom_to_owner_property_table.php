<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNumberOfBedroomAndBathroomToOwnerPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            $table->integer('total_bedroom')->after('no_of_guest')->default(1)->nullable();
            $table->integer('total_bathroom')->after('total_bedroom')->default(1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            //
        });
    }
}
