<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalFieldsInPropertyOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE property_offers MODIFY action_by ENUM('rental','owner','system')");

        Schema::table('property_offers', function (Blueprint $table) {

            $table->date('final_accepted_start_date')->nullable();
            $table->date('final_accepted_end_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE property_offers MODIFY action_by ENUM('rental','owner')");
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dropColumn('final_accepted_start_date');
            $table->dropColumn('final_accepted_end_date');
        });
    }
}
