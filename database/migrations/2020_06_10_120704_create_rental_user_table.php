<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_user', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->nullable();
            $table->string('google_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->text('bio')->nullable();
            $table->string('otp_code')->nullable();
            $table->enum('is_otp_verified',['0','1'])->default(0);
            $table->date('birthdate')->nullable();
            $table->enum('gender',['M','F'])->nullable();
            $table->string('phone')->nullable();
            $table->string('emergency_number')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('site_language')->nullable();
            $table->string('currency')->nullable();
            $table->enum('status', ['pending', 'approved','decline'])->default('pending');
            $table->string('photo')->nullable();
            $table->string('goverment_id_doc')->nullable();
            $table->enum('share_on_fb',['0','1'])->default(0);
            $table->enum('marketing_email',['0','1'])->default(0);
            $table->string('linked_paypal_account')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_user');
    }
}
