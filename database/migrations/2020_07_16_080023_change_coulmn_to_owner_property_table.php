<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCoulmnToOwnerPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            $table->renameColumn('photo', 'cover_photo');
            $table->dropColumn('bathroom');
            $table->dropColumn('bed');
            $table->dropColumn('bed_type');
            $table->dropColumn('guest_per_room');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            $table->renameColumn('cover_photo', 'photo');
        });
    }
}
