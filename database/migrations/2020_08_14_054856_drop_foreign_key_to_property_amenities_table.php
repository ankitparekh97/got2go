<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyToPropertyAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_amenities', function (Blueprint $table) {
            $table->dropForeign('property_amenities_property_id_foreign');
            $table->dropColumn('property_id');
            
        });
        Schema::table('property_amenities', function (Blueprint $table) {
            $table->bigInteger('property_id')->after('property_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_amenities', function (Blueprint $table) {
            //
        });
    }
}
