<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFlexibleDatesToBoolIntoPropertyOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dropColumn('flexible_dates');
        });

        Schema::table('property_offers', function (Blueprint $table) {
            $table->boolean('flexible_dates')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dropColumn('flexible_dates');
        });

        Schema::table('property_offers', function (Blueprint $table) {
            $table->enum('flexible_dates',['0','1'])->default('0');
        });
    }
}
