<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnumValueToMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            DB::statement("ALTER TABLE messages CHANGE COLUMN from_user_type from_user_type ENUM('owner', 'rental', 'admin')");
            DB::statement("ALTER TABLE messages CHANGE COLUMN to_user_type to_user_type ENUM('owner', 'rental', 'admin')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            //
        });
    }
}
