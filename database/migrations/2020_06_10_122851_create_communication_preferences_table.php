<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunicationPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_preferences', function (Blueprint $table) {
            $table->id();
            $table->enum('user_type',['owner','rental']);
            $table->integer('userid');
            $table->string('communication_title');
            $table->enum('is_notification',['0','1'])->default(1);
            $table->enum('is_email',['0','1'])->default(1);
            $table->enum('is_sms',['0','1'])->default(1);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_preferences');
    }
}
