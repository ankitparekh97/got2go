<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBatchFieldIntoPropertyOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('offered_nightly_price');
            $table->dropColumn(['offer_end_date','offer_start_date']);
        });

        Schema::table('property_offers', function (Blueprint $table) {
            $table->enum('status',['pending','accepted','rejected','counter_offer','cancel'])->nullable()->after('action_by');

            $table->decimal('tripper_offer_nightly_price')->nullable()->after('status');
            $table->decimal('owner_offer_nightly_price')->nullable()->after('tripper_offer_nightly_price');

            $table->date('tripper_offer_start_date')->nullable()->after('owner_offer_nightly_price');
            $table->date('tripper_offer_end_date')->nullable()->after('tripper_offer_start_date');

            $table->date('owner_offer_start_date')->nullable()->after('tripper_offer_end_date');
            $table->date('owner_offer_end_date')->nullable()->after('owner_offer_start_date');

            $table->decimal('tripper_offer_proposed_total')->nullable()->after('owner_offer_end_date');
            $table->decimal('owner_offer_proposed_total')->nullable()->after('tripper_offer_proposed_total');

            $table->integer('number_of_guests')->nullable()->after('owner_offer_proposed_total');
            $table->double('final_accepted_nightly_price')->nullable()->after('number_of_guests');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('tripper_offer_nightly_price');
            $table->dropColumn('owner_offer_nightly_price');
            $table->dropColumn('tripper_offer_start_date');
            $table->dropColumn('tripper_offer_end_date');
            $table->dropColumn('owner_offer_start_date');
            $table->dropColumn('owner_offer_end_date');
            $table->dropColumn('tripper_offer_proposed_total');
            $table->dropColumn('owner_offer_proposed_total');
            $table->dropColumn('number_of_guests');
            $table->dropColumn('final_accepted_nightly_price');
            $table->dropSoftDeletes();
        });

        Schema::table('property_offers', function (Blueprint $table) {
            $table->enum('status',['pending','accepted','rejected','cunter_offer'])->default('pending');
            $table->decimal('offered_nightly_price');
            $table->date('offer_start_date');
            $table->date('offer_end_date');
        });
    }
}
