<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tripboard', function (Blueprint $table) {
            $table->id();
            $table->string('tripboard_name');
            $table->integer('user_id');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('no_of_guest');
            $table->integer('type_of_date')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tripboard');
    }
}
