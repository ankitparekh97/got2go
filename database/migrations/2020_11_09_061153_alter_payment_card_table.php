<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPaymentCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_cards', function (Blueprint $table) {
            $table->string('debit')->after('expiration_date')->nullable();
            $table->boolean('default')->after('debit')->nullable();
            $table->string('image_url')->after('default')->nullable();
            $table->string('masked_number')->after('image_url')->nullable();
            $table->string('token')->after('masked_number')->nullable();
            $table->string('unique_identifier')->after('token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_cards', function (Blueprint $table) {
            $table->dropColumn([
                'debit',
                'default',
                'image_url'.
                'masked_number',
                'token',
                'unique_identifier'
            ]);
        });
    }
}
