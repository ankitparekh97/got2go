<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteUpdateFieldsIntoRentalUserSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rental_user_subscription', function (Blueprint $table) {
            $table->dropColumn('number_of_billing_cycles');
            $table->dropColumn('payment_method_token');
            $table->dropColumn('failureCount');
            $table->dropColumn('transaction_id');
            $table->dropColumn('never_expires');
            $table->dropColumn('next_billing_period_amount');
            $table->string('paid_through')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rental_user_subscription', function (Blueprint $table) {
            $table->string('number_of_billing_cycles')->nullable();
            $table->string('payment_method_token')->nullable();
            $table->string('failureCount')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('never_expires')->nullable();
            $table->string('next_billing_period_amount')->nullable();
            $table->dropColumn('paid_through');
        });
    }
}
