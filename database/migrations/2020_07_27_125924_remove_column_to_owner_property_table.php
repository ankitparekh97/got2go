<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnToOwnerPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            $table->dropColumn('owner_id');
            $table->dropForeign('owner_property_property_type_id_foreign');
            $table->dropColumn('property_type_id');
            $table->dropColumn('description');
            $table->dropColumn('guest_will_have');
            $table->dropColumn('location_detail');
            $table->dropColumn('price');
            $table->dropColumn('service_fees');
            $table->dropColumn('occupancy_taxes_and_fees');
            $table->dropColumn('is_partial_payment_allowed');
            $table->dropColumn('partial_payment_amount');
            $table->dropColumn('cancellation_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            //
        });
    }
}
