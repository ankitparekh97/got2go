<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('owner_property_id')->unsignedInteger();
            $table->integer('rating')->default(0);
            $table->text('reviews')->nullable();
            $table->timestamps();
        });

        Schema::table('property_reviews', function($table) {
//            $table->foreign('owner_property_id')
//                ->references('id')
//                ->on('owner_property');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_reviews');
    }
}
