<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignForAminityToPropertyAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_amenities', function (Blueprint $table) {
            $table->bigInteger('amenity_id')->unsigned()->nullable()->after('property_id')->change();
            $table->foreign('amenity_id')->references('id')->on('master_amenities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_amenities', function (Blueprint $table) {
            //
        });
    }
}
