<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnBraintreeCustomerIdToRentalUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rental_user', function (Blueprint $table) {
            $table->renameColumn('braintree_customer_id', 'customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rental_user', function (Blueprint $table) {
            $table->renameColumn('customer_id', 'braintree_customer_id');
        });
    }
}
