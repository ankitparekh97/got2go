<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('property_type_id');
            $table->text('description');
            $table->integer('guest_will_have');
            $table->string('location_detail');
            $table->decimal('price',10,2);
            $table->decimal('service_fees',10,2);
            $table->decimal('occupancy_taxes_and_fees',10,2);
            $table->enum('is_partial_payment_allowed',['0','1']);
            $table->decimal('partial_payment_amount',10,2);
            $table->string('cancellation_type');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->foreign('owner_id')->references('id')->on('owner');
            $table->foreign('property_type_id')->references('id')->on('property_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property');
    }
}
