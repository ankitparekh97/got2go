<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToBulkUploadLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bulk_upload_logs', function (Blueprint $table) {
            $table->json('failed_logs_media')->after('failed_logs')->nullable();
            $table->json('failed_logs_verification')->after('failed_logs_media')->nullable();
            $table->json('failed_logs_govtId')->after('failed_logs_verification')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bulk_upload_logs', function (Blueprint $table) {
            $table->dropColumn('failed_logs_media');
            $table->dropColumn('failed_logs_verification');
            $table->dropColumn('failed_logs_govtId');
        });
    }
}
