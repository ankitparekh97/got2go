<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoulmnToOwnerPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            $table->enum('is_for_guest',['0','1'])->default(0)->comment('0=no|1=yes')->after('location_detail');
            $table->enum('list_as_company',['0','1'])->default(0)->comment('0=no|1=yes')->after('is_for_guest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property', function (Blueprint $table) {
            //
        });
    }
}
