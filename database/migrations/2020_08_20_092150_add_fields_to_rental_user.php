<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToRentalUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rental_user', function (Blueprint $table) {
//            $table->dropColumn('full_name');
//            $table->dropColumn('otp_code');
//            $table->dropColumn('is_otp_verified');
            $table->string('first_name')->after('full_name')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->text('activation_url')->after('linked_paypal_account')->nullable();
            $table->enum('is_verified',['0','1'])->after('activation_url')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rental_user', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('is_verified');
            $table->dropColumn('activation_url');
        });
    }
}
