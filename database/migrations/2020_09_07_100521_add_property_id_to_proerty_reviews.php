<?php

use App\Models\PropertyReviews;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertyIdToProertyReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_reviews', function (Blueprint $table) {
            $table->dropColumn('owner_property_id');
            PropertyReviews::query()->truncate();
            $table->integer('property_id')->unsignedInteger()->after('id');

        });

        // Schema::table('property_reviews', function($table) {
        //     $table->foreign('property_id')
        //         ->references('id')
        //         ->on('property');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proerty_reviews', function (Blueprint $table) {
            //
        });
    }
}
