<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalUserSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_user_subscription', function (Blueprint $table) {
            $table->id();
            $table->foreignId('rental_id')->nullable();
            $table->dateTime('billing_period_start_date')->nullable();
            $table->dateTime('billing_period_end_date')->nullable();
            $table->dateTime('first_billing_date')->nullable();
            $table->string('current_billing_cycle')->nullable();
            $table->string('description')->nullable();
            $table->string('subscription_id')->nullable();
            $table->boolean('never_expires')->nullable();
            $table->dateTime('next_billing_date')->nullable();
            $table->string('next_billing_amount')->nullable();
            $table->string('next_billing_period_amount')->nullable();
            $table->string('number_of_billing_cycles')->nullable();
            $table->dateTime('paid_through_date')->nullable();
            $table->string('payment_method_token')->nullable();
            $table->string('plan_id')->nullable();
            $table->string('price')->nullable();
            $table->string('status')->nullable();
            $table->string('failureCount')->nullable();
            $table->string('transaction_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_user_subscription');
    }
}
