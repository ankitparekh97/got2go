<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_offers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('rental_id');
            $table->enum('action_by',['rental','owner'])->default('rental');
            $table->enum('status',['pending','accepted','rejected','cunter_offer'])->default('pending');
            $table->decimal('offered_nightly_price');
            $table->date('offer_start_date');
            $table->date('offer_end_date');
            $table->enum('flexible_dates',['0','1'])->default('0');
            $table->timestamps();
            $table->foreign('property_id')->references('id')->on('property');
            $table->foreign('owner_id')->references('id')->on('owner');
            $table->foreign('rental_id')->references('id')->on('rental_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_offers');
    }
}
