<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToTripboardTravelBuddiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tripboard_travel_buddies', function (Blueprint $table) {
            $table->integer('user_id')->default(0)->after('buddy_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tripboard_travel_buddies', function (Blueprint $table) {
            //
        });
    }
}
