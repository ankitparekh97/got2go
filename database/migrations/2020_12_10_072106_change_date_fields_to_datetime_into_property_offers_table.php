<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDateFieldsToDatetimeIntoPropertyOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->dateTime('tripper_offer_start_date')->change();
            $table->dateTime('tripper_offer_end_date')->change();
            $table->dateTime('owner_offer_start_date')->change();
            $table->dateTime('owner_offer_end_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_offers', function (Blueprint $table) {
            $table->date('tripper_offer_start_date')->change();
            $table->date('tripper_offer_end_date')->change();
            $table->date('owner_offer_start_date')->change();
            $table->date('owner_offer_end_date')->change();
        });
    }
}
