<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->enum('booking_type',['property','timeshare']);
            $table->integer('rental_user_id');
            $table->integer('property_id');
            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();
            $table->integer('adults')->nullable();
            $table->integer('childrens')->nullable();
            $table->integer('rooms')->nullable();
            $table->decimal('actual_price', 10, 2);
            $table->decimal('offer_amount', 10, 2);
            $table->decimal('tax', 10, 2);
            $table->decimal('occupancy_tax_fees', 10, 2);
            $table->decimal('total', 10, 2);
            $table->enum('status', ['pending', 'approved','decline'])->default('pending');
            $table->enum('is_cancelled', ['0', '1'])->default('0');
            $table->date('cancellation_date')->nullable();
            $table->enum('owner_status', ['pending', 'approved','decline','auto_reject'])->default('pending');
            $table->enum('property_status', ['pending', 'approved','decline','auto_reject'])->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
