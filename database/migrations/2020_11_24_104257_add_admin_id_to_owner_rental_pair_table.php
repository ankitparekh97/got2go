<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminIdToOwnerRentalPairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_rental_pair', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('rental_id')->nullable()->change();
            $table->foreign('admin_id')->references('id')->on('admin');
            $table->unique(array('owner_id','admin_id') ); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_rental_pair', function (Blueprint $table) {
            //
        });
    }
}
