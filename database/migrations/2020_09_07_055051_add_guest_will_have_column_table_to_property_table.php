<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGuestWillHaveColumnTableToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property', function (Blueprint $table) {
            $table->integer('guest_will_have')->default(0)->after('is_for_guest');
            $table->string('cover_photo')->nullable();
            $table->time('check_in')->nullable()->change();
            $table->time('check_out')->nullable()->change();
            $table->date('available_from')->nullable()->change();
            $table->date('available_to')->nullable()->change();
            $table->date('unavailable_from')->nullable()->change();
            $table->date('unavailable_to')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property', function (Blueprint $table) {
            //
        });
    }
}
