<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyBedroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_bedrooms', function (Blueprint $table) {
            $table->id();
            $table->integer('property_id');
            $table->integer('bathroom')->nullable();
            $table->integer('bed')->nullable();
            $table->integer('bed_type')->nullable();
            $table->integer('guest_per_room')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_bedrooms');
    }
}
