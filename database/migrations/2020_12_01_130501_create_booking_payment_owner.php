<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingPaymentOwner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_payment_owner', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id');
            $table->unsignedBigInteger('owner_id');
            $table->enum('payment_type', ['pending', 'completed'])->default('pending');
            $table->string('transaction_number');
            $table->decimal('amount',10,2);
            $table->timestamps();
            $table->foreign('owner_id')->references('id')->on('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_payment_owner');
    }
}
