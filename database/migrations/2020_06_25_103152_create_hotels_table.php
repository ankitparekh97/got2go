<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('email',500);
            $table->string('password',500);
            $table->string('description',1000);
            $table->string('location',255);
            $table->string('city',255);
            $table->string('state',255);
            $table->string('phone',255);
            $table->string('website',255);
            $table->tinyInteger('property_type');
            $table->time('check_in');
            $table->time('check_out');
            $table->integer('private_room_accommodation');
            $table->integer('cottage_room_accommodation');
            $table->integer('delux_room_accommodation');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
