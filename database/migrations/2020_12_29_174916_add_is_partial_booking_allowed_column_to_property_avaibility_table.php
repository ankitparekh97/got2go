<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPartialBookingAllowedColumnToPropertyAvaibilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_avaiblities', function (Blueprint $table) {
            $table->tinyInteger('is_partial_booking_allowed')->default(0);
            $table->integer('minimum_days')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_avaibility', function (Blueprint $table) {
            //
        });
    }
}
