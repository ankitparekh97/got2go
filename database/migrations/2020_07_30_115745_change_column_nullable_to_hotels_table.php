<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNullableToHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels', function (Blueprint $table) {
            $table->string('description',1000)->nullable()->change();
            $table->string('location',255)->nullable()->change();
            $table->string('city',255)->nullable()->change();
            $table->string('state',255)->nullable()->change();
            $table->string('phone',255)->nullable()->change();
            $table->string('website',255)->nullable()->change();
            $table->integer('property_type')->nullable()->change();
            $table->time('check_in')->nullable()->change();
            $table->time('check_out')->nullable()->change();
            $table->integer('private_room_accommodation')->nullable()->change();
            $table->integer('cottage_room_accommodation')->nullable()->change();
            $table->integer('delux_room_accommodation')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function (Blueprint $table) {
            //
        });
    }
}
