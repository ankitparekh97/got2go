<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSavingsAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_savings_accounts', function (Blueprint $table) {
            $table->id();
            $table->enum('user_type',['owner','rental']);
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('routing_number');
            $table->string('institution_name');
            $table->string('account_number');
            $table->string('ssn');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_savings_accounts');
    }
}
