<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnumstatusToBulkUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bulk_upload', function (Blueprint $table) {
            DB::statement("ALTER TABLE `bulk_upload` CHANGE `status` `status` ENUM('pending','in-progress','completed','failed')  NOT NULL DEFAULT 'pending'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bulk_upload', function (Blueprint $table) {
            //
        });
    }
}
