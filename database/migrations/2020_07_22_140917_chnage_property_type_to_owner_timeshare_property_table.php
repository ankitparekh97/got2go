<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChnagePropertyTypeToOwnerTimesharePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_timeshare_property', function (Blueprint $table) {
            $table->dropColumn('property_type');
            $table->unsignedBigInteger('property_type_id')->after('owner_id')->nullable();
            $table->foreign('property_type_id')->references('id')->on('property_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_timeshare_property', function (Blueprint $table) {
            //
        });
    }
}
