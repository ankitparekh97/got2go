<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBulkUploadLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_upload_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bulk_upload_id');
            $table->json('uploaded_logs')->nullable();
            $table->json('failed_logs')->nullable();
            $table->foreign('bulk_upload_id')->references('id')->on('bulk_upload');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_upload_logs');
    }
}
