<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripboardVoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tripboard_vote', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tripboard_id');
            $table->integer('property_id');
            $table->integer('votes')->unsigned();
            $table->integer('user_id')->unsigned();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tripboard_vote');
    }
}
