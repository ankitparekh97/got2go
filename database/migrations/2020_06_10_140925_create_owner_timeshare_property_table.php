<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerTimesharePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_timeshare_property', function (Blueprint $table) {
            $table->id();
            $table->integer('owner_id');
            $table->integer('hotel_id');
            $table->string('property_type')->nullable();
            $table->text('timeshare_description')->nullable();
            $table->text('description')->nullable();
            $table->string('location_detail')->nullable();
            $table->integer('no_of_guest')->nullable();
            $table->integer('unit_size')->nullable();
            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();
            $table->integer('min_days')->nullable();
            $table->integer('allow_booking_before')->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('service_fees', 10, 2);
            $table->decimal('occupancy_taxes_and_fees', 10, 2);
            $table->enum('is_partial_payment_allowed',['0','1'])->default(0);
            $table->decimal('partial_payment_amount', 10, 2)->nullable();
            $table->string('cancellation_type')->nullable();
            $table->string('ltd_time_frame')->nullable();
            $table->decimal('ltd_value', 10, 2);
            $table->string('membership_number')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_timeshare_property');
    }
}
