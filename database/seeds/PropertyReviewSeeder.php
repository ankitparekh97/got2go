<?php

use App\Models\Owner;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PropertyReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ownerProperty = \App\Models\Property::inRandomOrder()->get();
        if(count($ownerProperty)>0){
            foreach ($ownerProperty as $item) {
                $reviews = \App\Models\PropertyReviews::create([
                    'property_id' => $item->id,
                    'rating' => mt_rand(1,5),
                    'reviews' => 'This is great place for vacation. I would recommand this place to stay.'
                ]);
            }
        }
    }
}
