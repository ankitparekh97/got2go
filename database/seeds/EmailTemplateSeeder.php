<?php

use Illuminate\Database\Seeder;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_template')->insert([/* [
            'task' => 'request_hotel',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Request Hotel',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"><tbody><tr><td style="height:75px;    text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"></a></td></tr><tr><td>
<table><tbody><tr><td><p>Dear Got2Go Admin</p>{break}<p>I would like to LIST below timehsare details with Got2Go.</p>{break}<p>Hotel Name : {hotel_name}</p>{break}
<p>Property Type : {property_type}</p>{break}<p>Location : {location}</p>{break}<p>City : {city}</p>{break}<p>State : {state}</p>{break}<p>Let me know if any further information is needed.</p>{break}
<p>Best regards,</p>{break}<p>{owner_name}</p>{break}</tbody></table></td></tr><tr><td style="text-align:center;font-size: 14px;">Copyright © {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table></td></tr><tr><td style="text-align:center;font-size: 14px;">Copyright &copy; {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table><!-- table mian-->',
            'created_at'=>'2020-07-06 11:00:48',
            'updated_at'=>'2020-07-06 11:00:48'


        ],
        [
            'task' => 'request_property',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Request Approval of Property/Timeshare',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"><tbody><tr>			<td style="height:75px;    text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px" /></a></td></tr><tr><td><table><tbody><tr><td><p>Hello</p>{break}<p>{owner_name} requested {property_name} for {name} .</p>{break}

            Please approved!!{break}</td></tr></tbody></table></td></tr><tr><td style="text-align:center;font-size: 14px;">Copyright &copy; {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table><!-- table mian-->',
            'created_at'=>'2020-07-06 11:00:48',
            'updated_at'=>'2020-07-06 11:00:48'


        ],
        [
            'task' => 'send_user_password',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Account Created for timeshare',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"><tbody><tr>			<td style="height:75px;    text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px" /></a></td></tr><tr><td><table><tbody><tr><td><p>Hello {user_name}</p>{break}<p>Your account has been created. Use below login credentials for login in to panel</p>{break}

            Email : {email}{break}Password: {password}{break}</td></tr></tbody></table></td></tr><tr><td style="text-align:center;font-size: 14px;">Copyright &copy; {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table><!-- table mian-->',
            'created_at'=>'2020-07-06 11:00:48',
            'updated_at'=>'2020-07-06 11:00:48'


        ],
        [
            'task' => 'send_hotel_password',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Hotel/Resort Account Created for timeshare',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"><tbody><tr>			<td style="height:75px;    text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px" /></a></td></tr><tr><td><table><tbody><tr><td><p>Hello {user_name}</p>{break}<p>Your account has been created. Use below login credentials for login into panel and update your profile so we can publish your hotels in our portal.</p>{break}

            Email : {email}{break}Password: {password}{break}</td></tr></tbody></table></td></tr><tr><td style="text-align:center;font-size: 14px;">Copyright &copy; {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table><!-- table mian-->',
            'created_at'=>'2020-07-06 11:00:48',
            'updated_at'=>'2020-07-06 11:00:48'


        ],
        [
            'task' => 'owner_forgot_password',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Forgot Password Request',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px">	<tbody>		<tr>			<td style="height:75px;    text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px" /></a></td>		</tr>		<tr>			<td><!--  start cantent part -->			<table>				<tbody>					<tr>						<td>					<p>Hello {owner_name}</p>						<p>This is your new password: {password}</p>{break}
<p>Please use this password to login and change password afterwords from edit profile</p>																	</td>					</tr>				</tbody>			</table>			<!--  end cantent part --></td>		</tr>		<tr>			<td style="text-align:center;font-size: 14px;">Copyright &copy; {footer_year} All rights reserved. <a href="{site_url}">visit Timeshare</a></td>		</tr>	</tbody></table><!-- table mian-->',
            'created_at'=>'2019-09-19 00:00:00',
            'updated_at'=>'2019-09-19 00:00:00'

        ],
        [
            'task' => 'owner_registration',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'New Owner Registration',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"> <tbody> <tr> <td style="height:75px; text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"/></a></td></tr><tr> <td> <table> <tbody> <tr> <td> <p>Hello{user_name}</p>{break}<p>Thanks you for registration with us. Your OPT is :{otp}</p>{break}<p>Use above code or you can access the OTP from your mobile to activate your account. Use can then use below login credentials for login in to panel.</p>{break}Email :{email}{break}</td></tr></tbody> </table> </td></tr><tr> <td style="text-align:center;font-size: 14px;">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
            'created_at'=>'2020-08-05 11:00:48',
            'updated_at'=>'2020-08-05 11:00:48'
         ],[
                'task' => 'rental_registration',
                'from_address' => 'info@timeshare.com',
                'reply_address' => 'no-reply@timeshare.com',
                'subject'=>'New Rental Registration',
                'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"> <tbody> <tr> <td style="height:75px; text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"/></a></td></tr><tr> <td> <table> <tbody> <tr> <td> <p>Hello {user_name},</p><p>Kindly <a target="_blank" href={link}>Click here</a> to complete your registration or copy and paste the following url in your browser.</p><p>{link}</p>{break}</td></tr></tbody> </table> </td></tr><tr> <td style="text-align:center;font-size: 14px;">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
                'created_at'=>'2020-08-21 11:00:48',
                'updated_at'=>'2020-08-21 11:00:48'
            ][
            'task' => 'tripper_registration',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Tripper Registration',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"> <tbody> <tr> <td style="height:75px; text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"/></a></td></tr><tr> <td> <table> <tbody> <tr> <td> <p>Thank you for registering with us. Your registration code is: {otp}</p><p>Use the above code to activate your account.</p>Email :{email}{break}{break}{break}</td></tr></tbody> </table> </td></tr><tr> <td style="text-align:center;font-size: 14px;">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
            'created_at'=>'2020-08-05 11:00:48',
            'updated_at'=>'2020-08-05 11:00:48'
            ]],[
            'task' => 'rental_forgot_password',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Forgot Password Request',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"> <tbody> <tr> <td style="height:75px; text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"/></a></td></tr><tr> <td> <table> <tbody> <tr> <td> <p>Hello {rental_name},</p><p>This is your new password: {password}</p>{break}<p>Please use this password to login and change password afterwords from edit profile</p></td></tr></tbody> </table> </td></tr><tr> <td style="text-align:center;font-size: 14px;">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
            'created_at'=>'2020-09-24 00:00:00',
            'updated_at'=>'2020-09-24 00:00:00'

            ],
            [
            'task' => 'tripper_registration',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Tripper Registration',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"> <tbody> <tr> <td style="height:75px; text-align: center;"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"/></a></td></tr><tr> <td> <table> <tbody> <tr> <td> <p>Thank you for registering with us. Your registration code is: {otp}</p><p>Use the above code to activate your account.</p> <!-- Email :{email} --> {break}{break}{break}</td></tr></tbody> </table> </td></tr><tr> <td style="text-align:center;font-size: 14px;">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
            'created_at'=>'2020-08-05 11:00:48',
            'updated_at'=>'2020-08-05 11:00:48'
            ],*/[
            'task' => 'bulk_upload_notification',
            'from_address' => 'info@timeshare.com',
            'reply_address' => 'no-reply@timeshare.com',
            'subject'=>'Bulk upload Success',
            'message'=>'<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px"><tbody><tr><td style="height:75px;text-align:center"><a href="{site_url}"><img alt="" src="{logo_url}" style="width:180px"></a></td></tr><tr><td><table><tbody><tr><td><p>Hello {owner},</p><p>Your Bulk Listings has been uploaded successfully. </p>{break}</td></tr></tbody></table></td></tr><tr><td style="text-align:center;font-size:14px">Copyright &copy;{footer_year}All rights reserved. <a href="{site_url}">visit Timeshare</a></td></tr></tbody></table>',
            'created_at'=>'2020-12-02 11:00:48',
            'updated_at'=>'2020-12-02 11:00:48'
            ]]);
    }
}
