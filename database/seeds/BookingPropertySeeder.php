<?php

use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookingPropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $properties = App\Models\Property::leftjoin('booking', 'booking.property_id', '=', 'property.id')
                                    ->with(['booking'])
                                    ->where('booking.id', '=', null)
                                    ->select(DB::raw('property.*'))
                                    ->orderByRaw('property.id','desc')->get();

        $status = ['pending','approved','rejected','auto_reject','instant_booking','confirmed','cancelled'];
        $booking = [];

        $users = App\Models\RentalUser::orderByRaw('RAND()')->get();

        foreach($properties as $key=>$property)
        {
            $random = rand(1,50);
            $startdate = date('Y-m-d', strtotime('+'.$random.' day', time()));

            $erandom = rand(1,10);
            $enddate = date('Y-m-d', strtotime('+'.$erandom.' day', strtotime($startdate)));

            foreach($users as $user)
            {
                $booking[] = [

                    'booking_type' => ($property->type_of_property == 'property') ? 'property' : 'vacation_rental',
                    'rental_user_id' => $user->id,
                    'property_id' => $property->id,
                    'check_in_date' => $startdate,
                    'check_out_date' => $enddate,
                    'adults' => rand(1,5),
                    'childrens' => rand(1,5),
                    'rooms' => rand(1,5),
                    'actual_price' => $property->price,
                    'offer_amount' => $property->price,
                    'tax' => $property->service_fees,
                    'occupancy_tax_fees' => $property->occupancy_taxes_and_fees,
                    'total' => $property->price,
                    'status' => 'pending',
                    'is_cancelled' =>'0',
                    'owner_status' => $status[array_rand($status)],
                    'property_status' => 'pending',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];

            }
            break;

        }

        if(count($booking) > 0)
        {
            App\Models\Booking::insert($booking);
        }
    }
}
