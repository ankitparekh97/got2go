<?php

use Illuminate\Database\Seeder;

class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add 'Townhouse' in property type table
        $addNewPropertyType = \App\Models\PropertyType::where('property_type','Townhome')->first();
        if($addNewPropertyType==null){
            $addNewPropertyType = new \App\Models\PropertyType();
            $addNewPropertyType->property_type = 'Townhome';
            $addNewPropertyType->save();
        }
    }
}
