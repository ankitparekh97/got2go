<?php

use App\Services\PaymentServices\PaymentServiceInterface;
use Illuminate\Database\Seeder;
use App\Services\SubscriptionPlanContract;
use App\Http\DTO\Api\SubscriptionPlanDTO;

class SubscriptionPlanSeeder extends Seeder
{
    public $subscriptionPlanService;
    public $paymentService;
    public function __construct(
        SubscriptionPlanContract $subscriptionPlanService,
        PaymentServiceInterface $paymentService
    )
    {
        $this->subscriptionPlanService = $subscriptionPlanService;
        $this->paymentService = $paymentService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amount = 3.50;
        $planId = generateRandomAlphaNumeric('Plan_');
        $stripeSubscriptionPlanDTO = new \App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO;
        $stripeSubscriptionPlanDTO->id = $planId;
        $stripeSubscriptionPlanDTO->name = 'Tripper Subscription Plan 3.26';
        $stripeSubscriptionPlanDTO->amount = $amount;
        $stripeSubscriptionPlanDTO->currency = 'USD';
        $stripeSubscriptionPlanDTO->interval = 'day';

        /**
         * @var \App\Services\PaymentServices\Stripe\Plan\PlanResponseDTO $subscriptionPlan
         */
        $subscriptionPlan = $this->paymentService->createPlan($stripeSubscriptionPlanDTO);


        $subscriptionPlanDTO = new SubscriptionPlanDTO;
        $subscriptionPlanDTO->id = $subscriptionPlan->id;
        $subscriptionPlanDTO->name = $subscriptionPlan->name;
        $subscriptionPlanDTO->amount = $amount;
        $subscriptionPlanDTO->currency = $subscriptionPlan->currency;
        $subscriptionPlanDTO->interval = $subscriptionPlan->interval;
        $subscriptionPlanDTO->statementDescriptor = $subscriptionPlan->statementDescriptor;

        $this->subscriptionPlanService->createSubscriptionPlan($subscriptionPlanDTO);
    }
}
