<?php

use Illuminate\Database\Seeder;

class MasterAmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amenities = ['kitchen','washer_dryer','heating','family friendly','wifi','hairdryer','TV','bedroom_lock','iron','doorman','fireplace','smokedetector','selfcheckin','airconditioning','buzzer_intercom','hangers','owner_validated'];

        foreach($amenities as $amenity)
        {
            $flight = \App\Models\MasterAmenities::updateOrCreate(
                ['amenity_name' => $amenity],
                ['amenity_name' => ucwords(str_replace('_',' ', $amenity)), 'type' => 'unit']
            );
        }

    }
}
