<?php

use Illuminate\Database\Seeder;

class UpdatePropertyImages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dir = \File::files(public_path().'/sliderImages');
        foreach($dir as $path) {
            $file = pathinfo($path);
            $files[] = $file['basename'] ;
        }

        $copy = \File::copyDirectory(public_path().'/sliderImages', public_path().'/uploads/property');

         $propertyImage = \App\Models\Property::all();
         if(count($propertyImage)>0){
             $cnt = 0;
             foreach ($propertyImage as $property){
                $photos = $files;
                $property->cover_photo = $photos[shuffle($photos)];
                $property->is_hotdeals = ($cnt%2 == 0 ? '1' : '0');
                $property->save();

                $cnt++;
             }
         }
    }
}
