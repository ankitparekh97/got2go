<?php

use App\Models\PropertyType;
use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static $location;
    //protected $amenities;
    public static $amenities;
    protected $propertyArr = [];
    public static $photoArr = [];
    public static $PropertyType = 'property';
    public function run()
    {
        // for($i=1;$i<=5;$i++)
        // {
        //     $ext = '.jpeg';
        //     self::$photoArr[] = 'property_photo_'.$i.$ext;
        // }
        $path = public_path('addresses-us-1000.min.json') ; // ie: /var/www/laravel/app/storage/json/filename.json

        $json = json_decode(file_get_contents($path), true);
        self::$location=  $json['addresses'];


        self::$amenities = App\Models\MasterAmenities::select('id')->get();
        factory(App\Models\Property::class, 100)->create()->each(function ($property) {

            if($property->type_of_property  == 'property')
            {
                $this->propertyArr[] = $property->id;
            }
            $property->propertyamenities()->saveMany(factory(App\Models\PropertyAmenities::class,mt_rand(8,count(self::$amenities)))->make());
            $property->propertyimages()->saveMany(factory(App\Models\PropertyImages::class,5)->make());

        });

        if( count($this->propertyArr) > 0)
        {
            App\Models\Property::whereIn('id', $this->propertyArr)->where('type_of_property','property')
            ->update(['resort_id' => DB::raw( 'id' )]);
        }
    }
}
