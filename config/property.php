<?php

return [
    'structure_type'=>[
        '1'=>'Private Room',
        '2'=>'Resort/Cottage',
        '3'=>'Hotel/Motel',
    ],
    'cleaning_fee'=>20,
    'occupancy_taxt_and_fee'=>12,
    'cancellation_type'=>[
        '1'=>['title'=>'Flexible','description'=>"Guests can request a full refund within a limited period.  Cancellation requests received at least 48 hours before check-in time will be given a full refund."],
        '2'=>['title'=>'Moderate','description'=>"Guests can request a refund.  If a guest cancels within 5 days of the check-in date, then the first night of the booking will not be refunded and only 50% of the accomodation fees for the rest of the booking will then be refunded."],
        '3'=>['title'=>'Strict','description'=>"Guests may receive full refund only if they cancel within 48 hours of booking and at least 14 full days prior to the listing's local check-in time.  After 48 hours guests are only entitled to a 50% refund regardless of how far away the check-in date is."]
    ],
    'bed_types' =>[
        '1' => 'King',
        '2' => 'Queen',
        '3' => 'Double',
        '4' => 'Single',
        '5' => 'BunkBed',
        '6' => 'Futon',
        '7' => 'Couch',
        '8' => 'AirMattress',
    ]
];
