/*
 *  Customized Jquery for Site.
 */


var currentTab = 0; // Current tab is set to be the first tab (0)

var oldProgressTab = 0;
function showTab(n,isPrev) {

  var form = jQuery("#owner-panel-form");

  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("outer-table");
  var progressBar = parseInt(oldProgressTab);

  jQuery('.nav.nav-tabs.custom-tab').removeClass('counter'+progressBar);
  x[currentTab].style.display = "none"
  x[n].style.display = "block";

  	// if (form.valid() === false){
	// 	// jQuery('#nextBtn').addClass('disabled')
	// }
	// else{
	// 	jQuery('#nextBtn').removeClass('disabled')
	// }
  //... and fix the Previous/Next buttons:
  currentTab = n;

  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {

	var progressBar = parseInt(currentTab)+parseInt(1);

	jQuery('.nav.nav-tabs.custom-tab').addClass('counter'+progressBar);
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Save & Continue";
  }
  oldProgressTab = progressBar;
  //... and run a function that will display the correct step indicator:
  //fixStepIndicator(n)
}
var count = 2;
function nextPrev(n) {
	var form = jQuery("#owner-panel-form");

			if (form.valid() === true)
			{
				var jQueryqty=jQuery(this).closest('div').find('.cmn-add');
		        var currentVal = parseInt(jQueryqty.val());
		        if (!isNaN(currentVal)) {
		            jQueryqty.val(currentVal + 1);
				}

			  // This function will figure out which tab to display
			  var x = document.getElementsByClassName("outer-table");
			  // Exit the function if any field in the current tab is invalid:
			  //if (n == 1 && !validateForm()) return false;
			  // Hide the current tab:
			  x[currentTab].style.display = "none";
			  // Increase or decrease the current tab by 1:
			  currentTab = currentTab + n;
			  // if you have reached the end of the form...
			  if (currentTab >= x.length) {

			    // ... the form gets submitted:
			    //document.getElementById("regForm").submit();
			    return false;
			  }
			  // Otherwise, display the correct tab:
			  showTab(currentTab,false);

			  var count = currentTab;
		        if (count > 3) {
				jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
				jQuery('ul#ListingTab li:first-child').addClass('success');
			}
			if (count > 8) {
				jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
			}
			if (count > 9) {
				jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
			}
			if (count > 11) {
				jQuery('ul#ListingTab li:nth-child(4)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(5) a').removeClass('disabled');
			}
			}
}
function nextPrev1(n) {
		var jQueryqty=jQuery(this).closest('div').find('.cmn-add');
        var currentVal = parseInt(jQueryqty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
            jQueryqty.val(currentVal - 1);
        }
        var count = currentTab;

			if (count > 4) {
				jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
				jQuery('ul#ListingTab li:first-child').addClass('success');
			}
			if (count > 9) {
				jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
			}
			if (count > 10) {
				jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
			}
			if (count > 12) {
				jQuery('ul#ListingTab li:nth-child(4)').addClass('success');
				jQuery('ul#ListingTab li:nth-child(5) a').removeClass('disabled');
			}
	  // This function will figure out which tab to display
	  var x = document.getElementsByClassName("outer-table");
	  // Exit the function if any field in the current tab is invalid:
	  //if (n == 1 && !validateForm()) return false;
	  // Hide the current tab:
	  x[currentTab].style.display = "none";

	  // Increase or decrease the current tab by 1:
	  currentTab = currentTab + n;
	  // if you have reached the end of the form...
	  if (currentTab >= x.length) {
	  	alert('done');
	    // ... the form gets submitted:
	    //document.getElementById("regForm").submit();
	    return false;
	  }

	  // Otherwise, display the correct tab:
	  showTab(currentTab,true);
	}





jQuery(function () {
	jQuery("#form-submited-modal").modal({
		show:false,
		backdrop:'static'
	});


	var form = jQuery("#owner-panel-form");
    form.validate({
        rules: {
            name_your_listing: {
                required: true,
            },
            describe_listing : {
                required: true,
            },
            about_space : {
                required: true,
            },
            where_is_listing:{
                required: true,
            },
            private_residence_this:{
                required: true,
            },
            vacation_club_is_this:{
                required: true,
            },
            what_kind_cabin:{
                required: true,
            },
            how_many_guests_allowed:{
                required: true,
            },
            how_many_bedrooms_there:{
                required: true,
            },
            how_many_beds_there:{
                required: true,
            },
            how_many_bathrooms_available:{
                required: true,
            },
            guest_check_in:{
                required: true,
            },
            guest_checkout_date:{
                required: true,
            },
            property_available_when:{
                required: true,
            },
            property_available_when_to:{
                required: true,
            },
            property_unavailable:{
                required: true,
            },
            property_unavailable_to:{
                required: true,
            },
            listing:{
                required: true,
            },
            guest_have:{
                required: true,
            },
            is_guest_only:{
                required: true,
            },
            'amenity_id[]':{
                required: true,
            },
            cancellation_policy:{
                required: true,
            }
        },
        messages: {
            name_your_listing: {
                required: "This field is required",
            },
            describe_listing : {
                required: "This field is required",
            },
            about_space : {
                required: "This field is required",
            },
            listing:{
                required: "This field is required",
            },
            where_is_listing:{
                required: "This field is required",
            },
            private_residence_this:{
                required: "This field is required",
            },
            vacation_club_is_this:{
                required: "This field is required",
            },
            what_kind_cabin:{
                required: "This field is required",
            },
            how_many_guests_allowed:{
                required: "This field is required",
            },
            how_many_bedrooms_there:{
                required: "This field is required",
            },
            how_many_beds_there:{
                required: "This field is required",
            },
            how_many_bathrooms_available:{
                required: "This field is required",
            },
            guest_check_in:{
                required: "This field is required",
            },
            guest_checkout_date:{
                required: "This field is required",
            },
            property_available_when:{
                required: "This field is required",
            },
            property_available_when_to:{
                required: "This field is required",
            },
            property_unavailable:{
                required: "This field is required",
            },
            property_unavailable_to:{
                required: "This field is required",
            },
            guest_have:{
                required: "This field is required",
            },
            is_guest_only:{
                required: "This field is required",
            },
            'amenity_id[]':{
                required: "This field is required",
            },
            cancellation_policy:{
                required: "This field is required",
            }
        },
        errorPlacement: function(error, element) {


            var listing = $('#listingErrorLabel');
            var guest_have = $('#guest_haveErrorLabel');
            var is_guest_only = $('#is_guest_onlyErrorLabel');
            var amenityErrorLabel = $('#AmenityErrorLabel');
            var cancellationPolicyErrorLabel = $('#cancellation_policyErrorLabel');

            if (element.attr('name')=='listing') {
                $(listing).append(error)
            } else if (element.attr('name')=='guest_have') {
                $(guest_have).append(error)
            } else if (element.attr('name')=='is_guest_only') {
                $(is_guest_only).append(error)
            } else if (element.attr('name')=='amenity_id[]') {
                $(amenityErrorLabel).append(error)
            } else if (element.attr('name')=='cancellation_policy') {
                $(cancellationPolicyErrorLabel).append(error)
            } else  {
                error.insertAfter(element);
            }
        }
    });
	showTab(currentTab,false); // Display the current tab


                jQuery('#datetimepicker12, #datetimepicker13').datetimepicker({
                	format: 'LT',
                	icons: {
				        up: "fa fa-chevron-circle-up",
				        down: "fa fa-chevron-circle-down",
				        next: 'fa fa-chevron-circle-right',
				        previous: 'fa fa-chevron-circle-left'
				    }
                });
                jQuery('#datetimepicker14, #datetimepicker15, #datetimepicker16,#datetimepicker17').datetimepicker({
                	format: 'L',
                	icons: {
				        up: "fa fa-chevron-circle-up",
				        down: "fa fa-chevron-circle-down",
				        next: 'fa fa-chevron-circle-right',
				        previous: 'fa fa-chevron-circle-left'
				    }
                });
            });
( function(jQuery) {

	jQuery( document ).ready( function() {
		jQuery('.photos').imageUploader();
		jQuery('.photos1').imageUploader();
		jQuery('.photos2').imageUploader();



		jQuery( ".header-user-info .responsive-menu" ).on( "click", function() {
			jQuery('.responsive-menu-show').addClass('active');
			jQuery('.responsive-menu-layer').addClass('show');
		});
		jQuery( ".responsive-menu-layer" ).on( "click", function() {
			jQuery('.responsive-menu-show').removeClass('active');
			jQuery('.responsive-menu-layer').removeClass('show');
		});
		jQuery( ".listing-footer-btns button#nextBtn" ).on( "click", function() {

			   jQuery(".center-container").mCustomScrollbar('scrollTo','top', {
			   	timeout:0,
                scrollInertia:0,
			   });

		});

	} );


} )( jQuery );
