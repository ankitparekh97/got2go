<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
	<link rel="icon" href="IMAGES/favicon.png" />
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" href="css/datepicker.css" />
	<link rel="stylesheet" href="css/image-uploader.css" />
	<link href="css/style.css" rel="stylesheet">
	<link href="css/style2.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/jquery-min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css" />
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<title>Owner Panel</title>
</head>

<body>
	<div class="responsive-menu-layer"></div>
	<header>
		<div class="custom-container">
			<div class="row align-items-center">
				<div class="col-5 col-md-7 col-lg-1">
					<a href="#" class="svg-logo"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="609.147" height="425.649" viewBox="0 0 609.147 425.649">
							<defs>
								<linearGradient id="linear-gradient" x1="-0.643" y1="2.209" x2="1.712" y2="-0.245" gradientUnits="objectBoundingBox">
									<stop offset="0" stop-color="#f40041" />
									<stop offset="0.328" stop-color="#f64d2a" />
									<stop offset="0.792" stop-color="#fab60c" />
									<stop offset="0.999" stop-color="#fce000" />
								</linearGradient>
								<linearGradient id="linear-gradient-2" x1="0.289" y1="1.118" x2="3.375" y2="-1.294" xlink:href="#linear-gradient" />
								<linearGradient id="linear-gradient-3" x1="0.374" y1="2.18" x2="3.056" y2="-0.237" xlink:href="#linear-gradient" />
								<linearGradient id="linear-gradient-4" x1="-2.115" y1="2.278" x2="0.847" y2="-0.243" xlink:href="#linear-gradient" />
								<linearGradient id="linear-gradient-5" x1="-0.701" y1="1.116" x2="1.981" y2="-1.301" xlink:href="#linear-gradient" />
								<linearGradient id="linear-gradient-6" x1="-1.621" y1="1.103" x2="0.779" y2="-1.296" xlink:href="#linear-gradient" />
							</defs>
							<g id="Group_6" data-name="Group 6" transform="translate(-96.048 -89)">
								<g id="Group_4" data-name="Group 4">
									<path id="Path_13" data-name="Path 13" d="M404.99,90.824C347.005,90.824,300,135.929,300,191.569S347.005,292.314,404.99,292.314s104.991-45.1,104.991-100.745S462.975,90.824,404.99,90.824ZM359.054,257.171a252.576,252.576,0,0,1,47.386-6.127c3.446-28.658,1.307-56.722-8.807-84.035l-.517-.064c-.538,2.042-1.167,4.064-1.586,6.13s-.637,4.193-.954,6.376l-1.923-.372c.379.527.536.946.834,1.121,1.143.67,1.061,1.629.892,2.741-.412,2.7-.726,5.409-1.094,8.236l-2.2-.759c-.066.08-.13.161-.2.241a21.028,21.028,0,0,1,1.408,1.774,1.987,1.987,0,0,1,.4,1.29c-.877,4.03-1.819,8.046-2.749,12.064-.052.222-.15.434-.3.859-.528-.571-1.021-1.023-1.418-1.547a24.569,24.569,0,0,1-4.91-16.575c.359-7.642,3.331-14.374,7.26-20.771.3-.5.611-.989.915-1.483l-.428-.316-9.05,11.136c-.319-.658-.7-1.442-1.079-2.227l-.434.128c.184.9.4,1.8.541,2.712a2.371,2.371,0,0,1-.048,1.417c-1.76,2.938-3.591,5.834-5.512,8.922-.63-.932-1.169-1.731-1.71-2.53l-.468.144a14.167,14.167,0,0,0,.433,2.873,3.1,3.1,0,0,1-.085,2.5c-1.358,3.093-2.642,6.22-3.96,9.33-.138.324-.327.625-.584,1.111a24.922,24.922,0,0,1-3.243-8.978c-1.625-10.055,2.457-17.686,10.255-23.673a52.568,52.568,0,0,1,12.523-6.866,2.96,2.96,0,0,0,1.33-.85c-1.272.165-2.55.3-3.816.5s-2.557.45-3.819.744c-1.3.3-2.585.687-3.961,1.06-.117-.478-.22-.9-.351-1.435-.233.187-.482.288-.557.463-.751,1.736-2.314,2.213-3.94,2.675-1.86.528-3.691,1.161-5.6,1.768-.248-.793-.462-1.478-.689-2.2-.175.126-.292.165-.315.234-.785,2.382-2.9,3.2-4.879,4.193-2.652,1.33-5.282,2.7-7.926,4.052a4.587,4.587,0,0,1-1.871.546,45.544,45.544,0,0,1,.962-4.781,15.439,15.439,0,0,1,6.705-8.781c8.15-5.016,16.926-6.867,26.376-4.565a23.708,23.708,0,0,0,3.014.744c-.86-.5-1.7-1.048-2.589-1.5-.946-.486-1.923-.919-2.911-1.317s-2-.748-3.011-1.073-2.028-.586-3.148-.906c.349-.419.626-.751,1.139-1.369a7.286,7.286,0,0,0-1.9.277c-1.487.735-2.954.451-4.466.153-2.126-.417-4.268-.753-6.564-1.151l1-1.452-.141-.259c-.773.337-1.54.688-2.321,1a3.067,3.067,0,0,1-1.277.384c-3.456-.3-6.909-.669-10.564-1.035.359-.358.669-.709,1.02-1.013,9.264-8,21.231-8.576,31.721-1.4,2.244,1.537,4.237,3.445,6.342,5.186.339.28.658.584,1,.888.258-2.907.347-5.728.786-8.492,1-6.3,3.311-11.988,8.44-16.13,4.877-3.94,10.6-4.934,17.012-4.69-1,1.119-1.782,2.006-2.576,2.882-.685.756-1.391,1.493-2.076,2.248-1.569,1.729-2.939,3.727-5.687,3.85.034.113.066.226.1.339l1.879.642c-.545.706-1.007,1.317-1.483,1.917q-1.987,2.507-3.987,5.005c-1.228,1.527-2.206,3.337-4.39,4.01l1.674.639a27.688,27.688,0,0,0-3.6,9.636c6.922-7.843,14.748-13,25.415-11.642,6.034.768,15.118,5.842,17.907,9.86-2.228.231-4.372.45-6.516.676-2.212.233-4.427.465-6.639.71a1.621,1.621,0,0,1-1.853-.711,8.742,8.742,0,0,0-2.366-1.726l-.25.253.84,2.241c-3.417.557-6.7,1.106-9.994,1.606a1.543,1.543,0,0,1-.977-.351c-1.139-.823-2.372-.8-4-.568.944.658,1.636,1.139,2.494,1.738-3.573.858-7.092,1.282-10,3.463,19.082-2.975,31.8,5.372,40.05,22.355a11.417,11.417,0,0,1-1.25-.21c-3.949-1.192-7.89-2.41-11.844-3.583a1.836,1.836,0,0,1-1.369-1.353c-.379-1.165-.925-2.277-1.4-3.411l-.382.064c-.118,1.156-.236,2.312-.376,3.68l-5.62-2.265c-.3-.122-.592-.271-.9-.364-2.077-.624-3.884-1.45-3.636-4.146.039-.4-.294-.839-.608-1.658-.45,1.189-.764,2.015-1.131,2.982-3.557-2.452-7.126-4.632-11.509-5.569,13.043,27.205,20.774,55.486,21.72,85.51a144.762,144.762,0,0,1,35.633,5.988Z" fill="#fbfbfb" />
									<path id="Path_14" data-name="Path 14" d="M235.968,445.065q19.808-17.508,28.709-34.163t8.9-41.341a57.929,57.929,0,0,0-9.186-31.867q-9.192-14.35-26.413-22.679t-41.053-8.326a149.3,149.3,0,0,0-39.9,5.311q-19.237,5.316-32.441,14.5V390.8a99.731,99.731,0,0,1,25.838-14.354A77.553,77.553,0,0,1,177.977,371q13.489,0,19.808,5.167A16.326,16.326,0,0,1,204.1,389.37q0,9.764-9.33,19.953T164.2,437.6q-11.485,8.9-21.245,18.086l-24.114,21.531v34.451H279.03V460.28H218.743Z" fill="#fbfbfb" />
									<path id="Path_15" data-name="Path 15" d="M200.444,215.35h21.609v18.522a42.679,42.679,0,0,1-14.594,2.245q-23.3,0-33.675-11.085T163.4,192.338q0-19.923,12.767-30.73t34.379-10.8q20.205,0,35.5,3.928a77.811,77.811,0,0,1,27.361,12.909v-62.3A110.5,110.5,0,0,0,242.82,93.555q-16.28-3.644-38.167-3.648-33.117,0-57.669,12.348a87.952,87.952,0,0,0-37.747,35.359q-13.192,23.015-13.189,54.724,0,31.433,13.189,54.583a86.837,86.837,0,0,0,37.747,35.36q24.552,12.207,57.669,12.208,46.863,0,75.772-16.559V177.184H200.444Z" fill="#fbfbfb" />
									<path id="Path_16" data-name="Path 16" d="M568.413,285.162h66.509V144.846h50.235V89H518.18v55.846h50.234Z" fill="#fbfbfb" />
									<path id="Path_17" data-name="Path 17" d="M398.709,433h21.608v18.522a42.643,42.643,0,0,1-14.592,2.245q-23.3,0-33.677-11.085T361.664,409.99q0-19.923,12.77-30.73t34.377-10.8q20.207,0,35.5,3.928a77.8,77.8,0,0,1,27.362,12.91v-62.3a110.469,110.469,0,0,0-30.589-11.786q-16.278-3.645-38.167-3.649-33.115,0-57.668,12.348a87.935,87.935,0,0,0-37.746,35.36q-13.2,23.015-13.191,54.724,0,31.433,13.191,54.582a86.812,86.812,0,0,0,37.746,35.36q24.552,12.207,57.668,12.208,46.866,0,75.771-16.558V394.835H398.709Z" fill="#fbfbfb" />
									<path id="Path_18" data-name="Path 18" d="M602.156,308.57A103.039,103.039,0,1,0,705.2,411.61,103.041,103.041,0,0,0,602.156,308.57Zm68.764,93.514c-4.163,1.942-8.4,3.73-12.653,5.47-2.993,1.224-6.073,2.234-9.087,3.408a4.576,4.576,0,0,0-1.91,1.273c-5.47,6.982-10.887,14.006-16.318,21.02q-5.637,7.279-11.271,14.562c-1.029,1.331-5.749,2.7-7.282,2.044s-1.12-1.923-.864-3.089c.62-2.829,1.215-5.665,1.928-8.471.836-3.289,1.821-6.539,2.672-9.824.513-1.979.976-3.978,1.31-5.993.318-1.918-1.2-1.415-2.116-1.282-2.028.292-4.029.771-6.043,1.16-3.355.649-6.694,1.428-10.075,1.893-3.947.543-7.931.827-11.9,1.189-4.123.376-8.247.762-12.377,1.043a53.975,53.975,0,0,1-5.912.021c-4.5-.191-9.013-.347-13.5-.727-6.029-.51-12.077-.99-18.05-1.912-4.687-.723-9.276-2.083-13.895-3.213-.319-.078-.529-.6-1.135-1.341,7.094.034,13.448.15,19.8.07,5.811-.073,11.644-.088,17.421-.622,6.4-.592,12.744-1.8,19.126-2.634a126.332,126.332,0,0,0,18.341-3.977c3.058-.874,6.113-1.765,9.139-2.745,2.073-.671,2.258-1.535.684-3.1a33.261,33.261,0,0,0-3.027-2.484q-7.86-6.152-15.741-12.276a11.47,11.47,0,0,0-2.13-1.235c-2-.953-2.476-2.434-1.668-4.326.92-.351,1.8-.648,2.643-1.014,3.132-1.353,6.221-.53,9.282.224,3.386.834,6.741,1.8,10.1,2.726,5.438,1.5,10.865,3.052,16.31,4.531,2.992.812,6.02,1.494,9.018,2.287,1.885.5,3.726,1.164,5.618,1.631,3.491.86,6.387-1.293,9.562-2.032,2.609-.608,5.205-1.312,7.752-2.143a99.8,99.8,0,0,0,10.044-3.621c3.692-1.66,10.1-.279,12.492,2.938.93,1.25.893,5.517-.828,7.152A20.393,20.393,0,0,1,670.92,402.084Z" fill="#fbfbfb" />
								</g>
								<g id="Group_5" data-name="Group 5">
									<path id="Path_19" data-name="Path 19" d="M404.99,90.824C347.005,90.824,300,135.929,300,191.569S347.005,292.314,404.99,292.314s104.991-45.1,104.991-100.745S462.975,90.824,404.99,90.824ZM359.054,257.171a252.576,252.576,0,0,1,47.386-6.127c3.446-28.658,1.307-56.722-8.807-84.035l-.517-.064c-.538,2.042-1.167,4.064-1.586,6.13s-.637,4.193-.954,6.376l-1.923-.372c.379.527.536.946.834,1.121,1.143.67,1.061,1.629.892,2.741-.412,2.7-.726,5.409-1.094,8.236l-2.2-.759c-.066.08-.13.161-.2.241a21.028,21.028,0,0,1,1.408,1.774,1.987,1.987,0,0,1,.4,1.29c-.877,4.03-1.819,8.046-2.749,12.064-.052.222-.15.434-.3.859-.528-.571-1.021-1.023-1.418-1.547a24.569,24.569,0,0,1-4.91-16.575c.359-7.642,3.331-14.374,7.26-20.771.3-.5.611-.989.915-1.483l-.428-.316-9.05,11.136c-.319-.658-.7-1.442-1.079-2.227l-.434.128c.184.9.4,1.8.541,2.712a2.371,2.371,0,0,1-.048,1.417c-1.76,2.938-3.591,5.834-5.512,8.922-.63-.932-1.169-1.731-1.71-2.53l-.468.144a14.167,14.167,0,0,0,.433,2.873,3.1,3.1,0,0,1-.085,2.5c-1.358,3.093-2.642,6.22-3.96,9.33-.138.324-.327.625-.584,1.111a24.922,24.922,0,0,1-3.243-8.978c-1.625-10.055,2.457-17.686,10.255-23.673a52.568,52.568,0,0,1,12.523-6.866,2.96,2.96,0,0,0,1.33-.85c-1.272.165-2.55.3-3.816.5s-2.557.45-3.819.744c-1.3.3-2.585.687-3.961,1.06-.117-.478-.22-.9-.351-1.435-.233.187-.482.288-.557.463-.751,1.736-2.314,2.213-3.94,2.675-1.86.528-3.691,1.161-5.6,1.768-.248-.793-.462-1.478-.689-2.2-.175.126-.292.165-.315.234-.785,2.382-2.9,3.2-4.879,4.193-2.652,1.33-5.282,2.7-7.926,4.052a4.587,4.587,0,0,1-1.871.546,45.544,45.544,0,0,1,.962-4.781,15.439,15.439,0,0,1,6.705-8.781c8.15-5.016,16.926-6.867,26.376-4.565a23.708,23.708,0,0,0,3.014.744c-.86-.5-1.7-1.048-2.589-1.5-.946-.486-1.923-.919-2.911-1.317s-2-.748-3.011-1.073-2.028-.586-3.148-.906c.349-.419.626-.751,1.139-1.369a7.286,7.286,0,0,0-1.9.277c-1.487.735-2.954.451-4.466.153-2.126-.417-4.268-.753-6.564-1.151l1-1.452-.141-.259c-.773.337-1.54.688-2.321,1a3.067,3.067,0,0,1-1.277.384c-3.456-.3-6.909-.669-10.564-1.035.359-.358.669-.709,1.02-1.013,9.264-8,21.231-8.576,31.721-1.4,2.244,1.537,4.237,3.445,6.342,5.186.339.28.658.584,1,.888.258-2.907.347-5.728.786-8.492,1-6.3,3.311-11.988,8.44-16.13,4.877-3.94,10.6-4.934,17.012-4.69-1,1.119-1.782,2.006-2.576,2.882-.685.756-1.391,1.493-2.076,2.248-1.569,1.729-2.939,3.727-5.687,3.85.034.113.066.226.1.339l1.879.642c-.545.706-1.007,1.317-1.483,1.917q-1.987,2.507-3.987,5.005c-1.228,1.527-2.206,3.337-4.39,4.01l1.674.639a27.688,27.688,0,0,0-3.6,9.636c6.922-7.843,14.748-13,25.415-11.642,6.034.768,15.118,5.842,17.907,9.86-2.228.231-4.372.45-6.516.676-2.212.233-4.427.465-6.639.71a1.621,1.621,0,0,1-1.853-.711,8.742,8.742,0,0,0-2.366-1.726l-.25.253.84,2.241c-3.417.557-6.7,1.106-9.994,1.606a1.543,1.543,0,0,1-.977-.351c-1.139-.823-2.372-.8-4-.568.944.658,1.636,1.139,2.494,1.738-3.573.858-7.092,1.282-10,3.463,19.082-2.975,31.8,5.372,40.05,22.355a11.417,11.417,0,0,1-1.25-.21c-3.949-1.192-7.89-2.41-11.844-3.583a1.836,1.836,0,0,1-1.369-1.353c-.379-1.165-.925-2.277-1.4-3.411l-.382.064c-.118,1.156-.236,2.312-.376,3.68l-5.62-2.265c-.3-.122-.592-.271-.9-.364-2.077-.624-3.884-1.45-3.636-4.146.039-.4-.294-.839-.608-1.658-.45,1.189-.764,2.015-1.131,2.982-3.557-2.452-7.126-4.632-11.509-5.569,13.043,27.205,20.774,55.486,21.72,85.51a144.762,144.762,0,0,1,35.633,5.988Z" fill="url(#linear-gradient)" />
									<path id="Path_20" data-name="Path 20" d="M235.968,445.065q19.808-17.508,28.709-34.163t8.9-41.341a57.929,57.929,0,0,0-9.186-31.867q-9.192-14.35-26.413-22.679t-41.053-8.326a149.3,149.3,0,0,0-39.9,5.311q-19.237,5.316-32.441,14.5V390.8a99.731,99.731,0,0,1,25.838-14.354A77.553,77.553,0,0,1,177.977,371q13.489,0,19.808,5.167A16.326,16.326,0,0,1,204.1,389.37q0,9.764-9.33,19.953T164.2,437.6q-11.485,8.9-21.245,18.086l-24.114,21.531v34.451H279.03V460.28H218.743Z" fill="url(#linear-gradient-2)" />
									<path id="Path_21" data-name="Path 21" d="M200.444,215.35h21.609v18.522a42.679,42.679,0,0,1-14.594,2.245q-23.3,0-33.675-11.085T163.4,192.338q0-19.923,12.767-30.73t34.379-10.8q20.205,0,35.5,3.928a77.811,77.811,0,0,1,27.361,12.909v-62.3A110.5,110.5,0,0,0,242.82,93.555q-16.28-3.644-38.167-3.648-33.117,0-57.669,12.348a87.952,87.952,0,0,0-37.747,35.359q-13.192,23.015-13.189,54.724,0,31.433,13.189,54.583a86.837,86.837,0,0,0,37.747,35.36q24.552,12.207,57.669,12.208,46.863,0,75.772-16.559V177.184H200.444Z" fill="url(#linear-gradient-3)" />
									<path id="Path_22" data-name="Path 22" d="M568.413,285.162h66.509V144.846h50.235V89H518.18v55.846h50.234Z" fill="url(#linear-gradient-4)" />
									<path id="Path_23" data-name="Path 23" d="M398.709,433h21.608v18.522a42.643,42.643,0,0,1-14.592,2.245q-23.3,0-33.677-11.085T361.664,409.99q0-19.923,12.77-30.73t34.377-10.8q20.207,0,35.5,3.928a77.8,77.8,0,0,1,27.362,12.91v-62.3a110.469,110.469,0,0,0-30.589-11.786q-16.278-3.645-38.167-3.649-33.115,0-57.668,12.348a87.935,87.935,0,0,0-37.746,35.36q-13.2,23.015-13.191,54.724,0,31.433,13.191,54.582a86.812,86.812,0,0,0,37.746,35.36q24.552,12.207,57.668,12.208,46.866,0,75.771-16.558V394.835H398.709Z" fill="url(#linear-gradient-5)" />
									<path id="Path_24" data-name="Path 24" d="M602.156,308.57A103.039,103.039,0,1,0,705.2,411.61,103.041,103.041,0,0,0,602.156,308.57Zm68.764,93.514c-4.163,1.942-8.4,3.73-12.653,5.47-2.993,1.224-6.073,2.234-9.087,3.408a4.576,4.576,0,0,0-1.91,1.273c-5.47,6.982-10.887,14.006-16.318,21.02q-5.637,7.279-11.271,14.562c-1.029,1.331-5.749,2.7-7.282,2.044s-1.12-1.923-.864-3.089c.62-2.829,1.215-5.665,1.928-8.471.836-3.289,1.821-6.539,2.672-9.824.513-1.979.976-3.978,1.31-5.993.318-1.918-1.2-1.415-2.116-1.282-2.028.292-4.029.771-6.043,1.16-3.355.649-6.694,1.428-10.075,1.893-3.947.543-7.931.827-11.9,1.189-4.123.376-8.247.762-12.377,1.043a53.975,53.975,0,0,1-5.912.021c-4.5-.191-9.013-.347-13.5-.727-6.029-.51-12.077-.99-18.05-1.912-4.687-.723-9.276-2.083-13.895-3.213-.319-.078-.529-.6-1.135-1.341,7.094.034,13.448.15,19.8.07,5.811-.073,11.644-.088,17.421-.622,6.4-.592,12.744-1.8,19.126-2.634a126.332,126.332,0,0,0,18.341-3.977c3.058-.874,6.113-1.765,9.139-2.745,2.073-.671,2.258-1.535.684-3.1a33.261,33.261,0,0,0-3.027-2.484q-7.86-6.152-15.741-12.276a11.47,11.47,0,0,0-2.13-1.235c-2-.953-2.476-2.434-1.668-4.326.92-.351,1.8-.648,2.643-1.014,3.132-1.353,6.221-.53,9.282.224,3.386.834,6.741,1.8,10.1,2.726,5.438,1.5,10.865,3.052,16.31,4.531,2.992.812,6.02,1.494,9.018,2.287,1.885.5,3.726,1.164,5.618,1.631,3.491.86,6.387-1.293,9.562-2.032,2.609-.608,5.205-1.312,7.752-2.143a99.8,99.8,0,0,0,10.044-3.621c3.692-1.66,10.1-.279,12.492,2.938.93,1.25.893,5.517-.828,7.152A20.393,20.393,0,0,1,670.92,402.084Z" fill="url(#linear-gradient-6)" />
								</g>
							</g>
						</svg>
					</a>
				</div>
				<div class="col-1 col-lg-10 text-center responsive-menu-show transition">
					<ul class="desktop-menu">
						<li><a href="#">DASHBOARD</a></li>
						<li><a href="#">LISTINGS</a></li>
						<li><a href="#">BOOKINGS</a></li>
						<li><a href="#">PAYMENT</a></li>
						<li class="active"><a href="#">CHAT</a></li>
					</ul>
				</div>
				<div class="col-7 col-md-5 col-lg-1 text-right">
					<div class="header-user-info">
						<div class="user-image display-inline">
							<img src="images/user-image.png" alt="user" />
						</div>
						<div class="responsive-menu">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="content-area">
		<div class="custom-container">
			<h1>Chat</h1>
			<div class="chat-wrapper">
				<div class="row">
					<div class="col-lg-4 chat-left">
						<div class="tile height-block mb-0 p-0">
							<div class="sidechatpanel">
								<div class="search-chat">
									<label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
									<input type="text" placeholder="Search contacts...">
								</div>
							</div>
							<div class="chatcontact-list mCustomScrollbar"  data-mcs-theme="dark">
								<ul>
									<li class="chatcontact active">
										<div class="wrap">
											<div class="meta-left">
												<!-- <span class="contact-status online"></span> -->
												<img src="https://homepages.cae.wisc.edu/~ece533/images/girl.png" alt="">
												
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Louis Litt 
														<!-- <span> Property Name </span> -->
													</div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">Sunny Robin submitted a new offer for Cozy 3 B….</div>
											</div>
										</div>
									</li>
									<li class="chatcontact ">
										<div class="wrap">
											<div class="meta-left">
												<img src="http://emilcarlsson.se/assets/rachelzane.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Rachel Zane </div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">Sunny Robin submitted a new offer for Cozy 3 B</div>
											</div>
										</div>
									</li>
									<li class="chatcontact">
										<div class="wrap">
											<div class="meta-left">

												<img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Louis Litt</div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">You just got LITT up, Mike.</div>
											</div>
										</div>
									</li>
									<li class="chatcontact ">
										<div class="wrap">
											<div class="meta-left">
												<img src="http://emilcarlsson.se/assets/rachelzane.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Rachel Zane</div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">Sunny Robin submitted a new offer for Cozy 3 B</div>
											</div>
										</div>
									</li>
									<li class="chatcontact">
										<div class="wrap">
											<div class="meta-left">

												<img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Louis Litt </div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">You just got LITT up, Mike.</div>
											</div>
										</div>
									</li>
									<li class="chatcontact ">
										<div class="wrap">
											<div class="meta-left">
												<img src="http://emilcarlsson.se/assets/rachelzane.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Rachel Zane</div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">Sunny Robin submitted a new offer for Cozy 3 B</div>
											</div>
										</div>
									</li>
									<li class="chatcontact">
										<div class="wrap">
											<div class="meta-left">

												<img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Louis Litt </div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">You just got LITT up, Mike.</div>
											</div>
										</div>
									</li>
									<li class="chatcontact ">
										<div class="wrap">
											<div class="meta-left">
												<img src="http://emilcarlsson.se/assets/rachelzane.png" alt="">
											</div>
											<div class="meta">
												<div class="name-wrap">
													<div class="name">Rachel Zane</div>
													<div class="chat-date">10/10/2020</div>
												</div>
												<div class="preview">Sunny Robin submitted a new offer for Cozy 3 B</div>
											</div>
										</div>
									</li>

								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-8 chat-right">
						<div class="tile height-block mb-0 p-0">
							<div class="chatcontent-header">
								<div class="content-user">
									
									<div class="content-userimg"><img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt=""></div>
									<div class="name-wrap">
										<div class="name">Louis Litt 
											<!-- <span> Property Name </span> -->
										</div>
									</div>
								</div>
								<div class="content-link">
									<ul>
										<li class="active"> <a href="javascript:void(0)">Conversation </a> </li>
										<li> <a href="javascript:void(0)">Offers</a> </li>
									</ul>
								</div>
							</div>
							<div class="chatcontent-body mCustomScrollbar"  data-mcs-theme="dark">
								<div class="message-list">
									<ul>
										<li class="sent">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> They are trained and very well-behaved!</p>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="replies">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are allowed at the listing. It’s an additional $25 per night per animal.</p>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="sent">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p><strong> Teh are trained </strong> and very well-behaved!</p>
												</div>
												<div class="message-info-wrap mesginfo-yellow">
													<div class="message-info-header" >
														<div class="message-title">Mei’s counter offer:</div>
														<div class="message-status">Pending On You</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="offer-text mt-2">Cancel This Offer</div>
													<div class="message-info-action">
														<a href="javascript:void(0)" class="btn btn-bold btn-round btn-font-fatfrank btn-primary-go">Accept Offer for $1250 </a>
														<a href="javascript:void(0)" class="btn btn-bold btn-round btn-outline-grey">Make Counter Offer</a>
														<a href="javascript:void(0)" class="btn btn-bold btn-link">Reject This Offer </a>
													</div>
												</div>

												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="replies">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-red">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status"> Offer Countered</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="sent">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-green">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status">Offer Accepted</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="replies">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-red">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status"> Offer Countered</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="sent">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-green">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status">Offer Accepted</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="replies">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-red">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status"> Offer Countered</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
										<li class="sent">
											<div class="img"><img src="http://emilcarlsson.se/assets/mikeross.png" alt=""></div>
											<div class="message-wrapper">
												<div class="message-box">
													<p> Yes, up to 3 pets are all.</p>
												</div>
												<div class="message-info-wrap mesginfo-green">
													<div class="message-info-header" >
														<div class="message-title">Your counter offer:</div>
														<div class="message-status">Offer Accepted</div>
													</div>
													<div class="message-info-body">
														<p>The California Fresh - 3 Nights</p>
														<p>Standard Rate: $1400</p>
														<p>Mei’s Offer: $1290</p>
													</div>
													<div class="message-info-action"></div>
												</div>
												<div class="time">9:08 PM</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="chatcontent-footer">
								<div class="chatcontent-footer-wrapper">
									<div class="chat-textarea"><textarea class="form-control" id="" rows="2">Type your message here....</textarea></div>
									<div class="chat-action">
										<button class="btn btn-pink btn-pill btn-round">Send</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="form-submited-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content" style="background-image: url('images/plane-bg.png')">
					<div class="description text-center">
						<h4>Congratulations! </h4>
						<h5>Your property has been listed and is available to rent.</h5>
						<button type="button" class="btn btn-default dashboard-btn" data-dismiss="modal">Go to My Listings</button>
					</div>
				</div>
			</div>
		</div>

		<script src="js/movment.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.mCustomScrollbar.js"></script>
		<script src="js/jquery.validate.js"></script>
		<script src="js/image-uploader.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous"></script>
		<script src="js/custom.js"></script>
		<script type="text/javascript">
			var PropertyTypes = '';

			(function($) {
				PropertyTypes = JSON.parse('{"vacation_rental":{"0":"Resort","1":"Hotel","2":"Villa","3":"Holiday Cottage","4":"Condo","5":"Lodge"},"Hotels":{"0":"Suite","Resorts":["Ski Resort","Beach Resort","Water Park Resort"],"1":"Bed & Breakfast"},"property":{"4":"House","0":"Apartment","1":"Studio Apartment","5":"Cottage","2":"Bunglow","3":"Estate"}}')
				var subPropertyType = {}
				subPropertyType['House'] = JSON.parse('["Town House","Tiny Home","Lakehouse","Bunglow","Country House","Ranch House","Duplex","None of the above"]');
				subPropertyType['Cottage'] = JSON.parse('["Cabin","Lodge"]');
				subPropertyType['Lodge'] = JSON.parse('["Cabin","Cottage"]');

				$('.plus').on('click', function() {

					var $qty = $(this).closest('div').find('.cmn-add');
					var tempId = $qty.attr('id');

					var currentVal = parseInt($qty.val());
					if (!isNaN(currentVal)) {
						$qty.val(currentVal + 1);
						if (tempId == 'how-many-bedrooms-there') {
							cloneBedrooms(currentVal + 1);
						}
					}



				});

				$('.minus').on('click', function() {
					var $qty = $(this).closest('div').find('.cmn-add');
					var tempId = $qty.attr('id');
					var currentVal = parseInt($qty.val());
					if (!isNaN(currentVal) && currentVal > 0) {
						$qty.val(currentVal - 1);
						if (tempId == 'how-many-bedrooms-there') {
							removeBedrooms(currentVal);
						}
					}


				});

				function setPropertyInput() {
					$('#what-kind-of-residence').show();
					$('#what-kind-of-cabin').hide();
					$('#what-kind-of-vacation').hide();
					$('#what-is-resort-name').hide();

					$('#what-kind-of-vacation').hide();

				}

				function setVacationInput() {
					$('#what-kind-of-residence').hide();
					$('#what-kind-of-cabin').hide();
					$('#what-kind-of-vacation').hide();
					$('#save-new-resort').hide()
					$('#what-is-resort-name').show();
					$('#what-kind-of-vacation').show();
				}

				var type = 'property'
				$("#property_type").empty();
				$("#property_type").append('<option value="">Select One</option>');
				for (x in PropertyTypes[type]) {
					console.log(PropertyTypes[type])
					$("#property_type").append($("<option></option>").val(PropertyTypes[type][x]).text(PropertyTypes[type][x].trim()));
				}

				var type = 'vacation_rental'
				$("#vacation_club").empty();
				$("#vacation_club").append('<option value="">Select One</option>');
				for (x in PropertyTypes[type]) {
					$("#vacation_club").append($("<option></option>").val(PropertyTypes[type][x]).text(PropertyTypes[type][x].trim()));
				}


				function renderSelect(type) {



				}



				$('#what-kind-of-residence').hide();
				$('#what-kind-of-cabin').hide();
				$('#what-kind-of-vacation').hide();
				$('#what-is-resort-name').hide();


				setPropertyInput();
				$('.type_of_property').on('change', function() {
					if ($(this).val() == 'property') {
						setPropertyInput();
					} else if ($(this).val() == 'vacation_rental') {
						setVacationInput();
					}
				});

				$('#property_type, #vacation_club').on('change', function() {
					var check = '';
					$('input[name="type_of_property"]:checked').each(function() {
						var check = this.value;
						console.log(check)
					});
					var type = this.value;
					if (subPropertyType[type]) {

						$("#subType").empty();
						$("#subType").append('<option value="">Select One</option>');
						for (x in subPropertyType[type]) {
							$("#subType").append($("<option></option>").val(subPropertyType[type][x]).text(subPropertyType[type][x].trim()));
						}
						$('#what-kind-of-cabin').show();
					} else {
						$('#what-kind-of-cabin').hide();
					}

				});


				//Auto SUggestions
				var availableTags = ["Marriott Suites", "Blue Lagoon", "Taj Hotels", "Hayat", "Disney World Resort", "Cancun Rentals", "Mandarin Oriental"];

				$("#txt-resort").autocomplete({
					source: function(request, response) {
						console.log(request)
						console.log(response);
						var data = {};
						var i = 0;
						response($.map(availableTags, function(item) {

							if (item.toLowerCase().search(request.term.toLowerCase()) !== -1) {
								i++;
								return {
									label: item,
									value: item
								}

							}
						}))

						if (i === 0 && request.term.length > 3) {
							$('#save-new-resort').show();
							$('#empty-resorts').show();
							$('#txt-resort').parents('.input-button').removeClass('green');
							$('#txt-resort').parents('.input-button').addClass('red');
						} else {
							$('#empty-resorts').hide();
							$('#save-new-resort').hide();
							$('#txt-resort').parents('.input-button').removeClass('green');
						}

					},

					response: function(event, ui) {
						console.log('close-fn', ui.item)
					}

				});

				$('#save-new-resort').click(function() {
					$('#txt-resort').parents('.input-button').addClass('green');
					$('#txt-resort').attr('disabled', true)
				})

				$("#txt-resort").on("autocompletechange", function(event, ui) {
					console.log('bind', ui.item)
				});
				$("#txt-resort").on("autocompleteresponse", function(event, ui) {
					console.log('bind-close', ui.item)
				});


				function cloneBedrooms(currentVal) {

					var div = $("#bedroomTypes").clone(true, true).prop({
						id: "bedroom_" + currentVal
					}).appendTo($('.cloneBedroom'));
					console.log(div)

					div.find('.delBedroom').attr('data-id', currentVal);
					div.show()
					renderBedroomLabel()
				}

				function renderBedroomLabel() {
					$('.lblBedroom').each(function(index) {

						$(this).text('Bedroom ' + index)
					})
				}

				$('.delBedroom').on('click', function() {

					var id = $(this).attr('data-id');
					$('#bedroom_' + id).remove();
					var val = $('#how-many-bedrooms-there').val()
					$('#how-many-bedrooms-there').val(val - 1);

					renderBedroomLabel();
				})

				function removeBedrooms(currentVal) {

					$('.lblBedroom').each(function(index) {

						if (currentVal == index) {
							$(this).parents('.col-md-6').remove();
						}

					});


				}



			})(jQuery);
		</script>
</body>

</html>