<?php include('header.php'); ?>
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <h2 class="f-w-700 text-uppercase text-dark d-block w-100 align-items-center">
                  View Listing                         
               </h2>
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid property-edit" id="kt_content">
            <div class="row" >
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="property-images">
                           <div class="row">
                              <div class="col-md-12">
                                 <img src="assets/media/images/list-property.jpg" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-desc">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h2 class="f-w-700 mb-5">Studio Apartment, Los Angeles</h2>
                        <h6>Luxurious Studio Apartment in the prime area of Los Angeles.<br>Welcoming guests with LA style stay.</h6>
                        <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h5 class="f-w-700 mb-5">Unit Amenities</h5>
                        <div class="amenities-list-dsply">
                           <div class="row">
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                              <div class="col-6 col-sm-6">
                                 <h6>Kitchen</h6>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-spacing-h6">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="check-in-out-time border-bottom mb-5">
                           <div class="row">
                              <div class="col-md-4">
                                 <h6><strong>Check-In</strong> 11.00AM</h6>
                              </div>
                              <div class="col-md-8">
                                 <h6><strong>Check-Out:</strong> 11.00AM</h6>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Common Rooms:</strong> 1</h6>
                           </div>
                           <div class="col-md-8">
                              <h6>Sofa:</strong> 1</h6>
                           </div>
                        </div>
                        <h6><strong>Bathrooms:</strong> 1</h6>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Property type:</strong> Studio Apartment</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Room type:</strong> Entire Place</h6>
                           </div>
                        </div>
                        <h6><strong>Accommodates:</strong> 3 Guests</h6>
                        <h2 class="mb-5 border-bottom"><strong>Bedroom 1</strong></h2>
                        <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Bedrooms:</strong> 1.5</h6>
                           </div>
                           <div class="col-md-3">
                              <h6><strong>Beds:</strong> 2</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Bed type:</strong> Regular Bed</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Price:</strong> $200 (Per Night)</h6>
                           </div>
                        </div>

                        <h2 class="mb-5 border-bottom"><strong>Bedroom 2</strong></h2>
                        <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Bedrooms:</strong> 1.5</h6>
                           </div>
                           <div class="col-md-3">
                              <h6><strong>Beds:</strong> 2</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Bed type:</strong> Regular Bed</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Price:</strong> $200 (Per Night)</h6>
                           </div>
                        </div>

                        <h2 class="mb-5 border-bottom"><strong>Bedroom 3</strong></h2>
                        <div class="row">
                           <div class="col-md-4">
                              <h6><strong>Bedrooms:</strong> 1.5</h6>
                           </div>
                           <div class="col-md-3">
                              <h6><strong>Beds:</strong> 2</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Bed type:</strong> Regular Bed</h6>
                           </div>
                           <div class="col-md-5">
                              <h6><strong>Price:</strong> $200 (Per Night)</h6>
                           </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </div>
            </div>
            <div class="delete-listing mb-3">
               <a href="#" class="btn btn-hover-bg-danger btn-text-danger btn-hover-text-white border-0 font-weight-bold ">Delete Listing</a>
            </div>
            <div class="posted-updated-date">
               <h6><span class="mr-5">Posted date: 05/10/2019 07:55 PM</span><span>Updated date: 03/02/2020 05:05 PM</span></h6>
            </div>
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<?php include('footer.php'); ?>
