<?php include('header.php'); ?>
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <h2 class="f-w-700 text-uppercase text-dark d-block w-100 align-items-center">
                  View Timeshare                      
               </h2>
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid property-edit" id="kt_content">
            <div class="row" >
               <div class="col-md-6">
                  <div class="card mb-10">
                     <div class="card-body">
                        <div class="property-images">
                           <div class="row">
                              <div class="col-md-12">
                                 <img src="assets/media/images/list-property.jpg" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                              <div class="col-6 col-md-3">
                                 <img src="assets/media/images/property.PNG" alt="" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card" style="height: calc(100% - 30px);">
                     <div class="card-body list-desc">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <h2 class="f-w-700 mb-5">Studio Apartment, Los Angeles</h2>
                        <h6>Luxurious Studio Apartment in the prime area of Los Angeles.<br>Welcoming guests with LA style stay.</h6>
                        <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="card mb-10" style="height: calc(100%);">
                     <div class="card-body">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="d-flex align-items-center" style="justify-content: center;flex-direction: column;height: 100%;">
                           <h6><strong>Price:</strong> $500 per Night</h6>
                           <h6><strong>Tax:</strong> $2</h6>
                           <h6><strong>cancellation:</strong> Flexible</h6>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="card">
                     <div class="card-body list-spacing-h6">
                        <div class="edit-list"><a href="#"><i class="far fa-edit"></i></a></div>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Property type:</strong> Studio Apartment</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Room type:</strong> Entire Place</h6>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <h6><strong>Accommodates:</strong> 2 Guests</h6>
                           </div>
                           <div class="col-md-6">
                              <h6><strong>Unit size:</strong> 1</h6>
                           </div>
                        </div>
                        <div class="form-group">
                           <label>Date Range</label>
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="input-group date">
                                    <input type="text" class="form-control" id="kt_datepicker_2" readonly  placeholder="Select date"/>
                                    <div class="input-group-append">
                                       <span class="input-group-text">
                                          <i class="la la-calendar-check-o"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-1 pt-2 pb-2 text-center">
                                 <label style="margin: 0;">To</label>
                              </div>
                              <div class="col-md-5">
                                 <div class="input-group date">
                                    <input type="text" class="form-control" id="kt_datepicker_3" readonly  placeholder="Select date"/>
                                    <div class="input-group-append">
                                       <span class="input-group-text">
                                          <i class="la la-calendar-check-o"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <h6><strong>Minimum Days</strong>5</h6>
                        <h6>Unit Amenities</h6>
                        <div class="row">
                           <div class="col-6 col-sm-6">
                              <h6>Kitchen</h6>
                           </div>
                           <div class="col-6 col-sm-6">
                              <h6>Kitchen</h6>
                           </div>
                           <div class="col-6 col-sm-6">
                              <h6>Kitchen</h6>
                           </div>
                           <div class="col-6 col-sm-6">
                              <h6>Kitchen</h6>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="delete-listing mb-3">
               <a href="#" class="btn btn-hover-bg-danger btn-text-danger btn-hover-text-white border-0 font-weight-bold ">Delete Listing</a>
            </div>
            <div class="posted-updated-date">
               <h6><span class="mr-5">Posted date: 05/10/2019 07:55 PM</span><span>Updated date: 03/02/2020 05:05 PM</span></h6>
            </div>
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<?php include('footer.php'); ?>
