<?php include('header.php'); ?>
<!--begin::Container-->
<div class="d-flex flex-row flex-column-fluid  container ">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">

        <div class="content flex-column-fluid d-lg-flex" id="kt_content">
            <!--begin::Wizard 6-->
            <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
                <!--begin::Container-->
                <div class="wizard-content d-flex flex-column mx-auto w-100">
                    <!--begin::Nav-->

                    <!--end::Nav-->
                    <!--begin::Form-->
                    <form class="px-10 pt-10" novalidate="novalidate" id="kt_wizard_form">
                        <!--begin: Wizard Step 1-->
                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                            <h5 class="f-w-700"> New Resort/Hotel Request</h5>
                            <div class="steps-container">
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property type</label>
                                            <select class="form-control" id="exampleSelect1">
                                                <option>Resort</option>
                                                <option>Hotel</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter location details</label>
                                            <input type="text" name="location" class="form-control" required="">
                                        </div>
                                        <div class="pt-5 pb-5 text-center">
                                            <h5>OR</h5>
                                        </div>
                                        <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label>
                                        <div class="map">
                                            <div id="kt_leaflet_5" style="height:300px;"></div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Resort/Hotel Name</label>
                                            <select class="form-control" id="exampleSelect1">
                                                <option>Hotel Name 1</option>
                                                <option>Hotel Name 2</option>
                                                <option>Hotel Name 3</option>
                                                <option>Hotel Name 4</option>
                                                <option>Hotel Name 5</option>
                                                <option>Hotel Name 6</option>
                                            </select>
                                        </div>
                                        <!-- <div class="pt-2 pb-5 text-center">
                                    <a href="#">Resort/Hotel not listed here? Submit a Request.</a>
                                 </div> -->
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" name="location" class="form-control" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>State</label>
                                            <input type="text" name="location" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <!--end::Form Group-->
                            </div>
                        </div>
                        <!--end: Wizard Step 1-->

                        <!--begin: Wizard Actions-->



                        <div class="d-flex justify-content-between pt-7 float-right">

                            <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-8 py-4 my-3" data-wizard-type="submit">
                                Submit

                            </button>

                        </div>
                        <!--end: Wizard Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Wizard 6-->
        </div>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
</div>
<!--end::Container-->

<?php include('footer.php'); ?>
<script>
    $(function() {
        $(".repeat").on('click', function(e) {
            var c = $('.base-group').clone();
            c.removeClass('base-group').css('display', 'block').addClass("child-group");
            $(".main-group").append(c);
        });
    });
</script>
<script>
    // Class definition
    var KTLeaflet = function() {

        // Private functions
        var demo5 = function() {
            // Define Map Location
            var leaflet = L.map('kt_leaflet_5', {
                center: [40.725, -73.985],
                zoom: 13
            });

            // Init Leaflet Map
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(leaflet);

            // Set Geocoding
            var geocodeService;
            if (typeof L.esri.Geocoding === 'undefined') {
                geocodeService = L.esri.geocodeService();
            } else {
                geocodeService = L.esri.Geocoding.geocodeService();
            }

            // Define Marker Layer
            var markerLayer = L.layerGroup().addTo(leaflet);

            // Set Custom SVG icon marker
            var leafletIcon = L.divIcon({
                html: `<span class="svg-icon svg-icon-danger svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
                bgPos: [10, 10],
                iconAnchor: [20, 37],
                popupAnchor: [0, -37],
                className: 'leaflet-marker'
            });

            // Map onClick Action
            leaflet.on('click', function(e) {
                geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
                    if (error) {
                        return;
                    }
                    markerLayer.clearLayers(); // remove this line to allow multi-markers on click
                    L.marker(result.latlng, {
                        icon: leafletIcon
                    }).addTo(markerLayer).bindPopup(result.address.Match_addr, {
                        closeButton: false
                    }).openPopup();
                    alert(`${result.address.Match_addr}`);
                });
            });
        }

        return {
            // public functions
            init: function() {
                // default charts
                demo5();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTLeaflet.init();
    });
</script>