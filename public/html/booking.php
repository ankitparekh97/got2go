<?php include('header.php'); ?>
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-6 " id="kt_subheader">
            <div class=" w-100  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <!--begin::Info-->
               <div class="d-flex align-items-center flex-wrap mr-1">
                  <!--begin::Page Heading-->
                  <div class="d-flex align-items-baseline flex-wrap mr-5">
                     <!--begin::Page Title-->
                     <h5 class="f-w-700 text-dark d-block w-100 align-items-center">
                        New Booking / Booking Request                         
                     </h5>
                     <!--end::Page Title-->
                  </div>
                  <!--end::Page Heading-->
               </div>
               <!--end::Info-->
               <!--begin::Toolbar-->
               <div class="d-flex align-items-center">
                  <!--begin::Daterange-->
                  <a href="#" class="btn btn-light-primary btn-sm font-weight-bold mr-2" id="kt_dashboard_daterangepicker">
                  <span class="opacity-60 font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title">Today:</span>
                  <span class="font-weight-bold" id="kt_dashboard_daterangepicker_date">Aug 23</span>
                  </a>
                  <!--end::Daterange-->
                 
               </div>
               <!--end::Toolbar-->
            </div>
         </div>
         <!--end::Subheader-->
         <div class="content flex-column-fluid property-edit" id="kt_content">
            <div class="bookin-top-search">
               <form>
                <div class="search-box cmn-field">
                  <div class="form-group">
                    <i class="fas fa-search"></i>
                    <input type="text" class="form-control" />
                  </div>
                </div>
                <div class="search-box cmn-field label">
                  <div class="form-group">
                      <label>Status:</label>
                      
                      <select class="form-control">
                        <option>All</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                        <option>Option 4</option>
                        <option>Option 5</option>
                      </select>   
                    </div>
                  </div>
                  <div class="search-box cmn-field label">
                    <div class="form-group">
                      <label>Type:</label>
                      
                      <select class="form-control">
                        <option>All</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                        <option>Option 4</option>
                        <option>Option 5</option>
                      </select>   
                    </div>
                  </div>
                  <div class="search-box cmn-field submit-box">
                    <div class="form-group">
                      <input type="submit" class="btn btn-danger font-weight-bold btn-square">
                    </div>
                  </div>
               
               </form>
            </div>
            <div class="booking-list-outer">
              <div class="booking-container-outer border-bottom">
                  <div class="d-flex align-items-center flex-wrap booking-row ">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <a href="javascript:void(0)" class="user-extra-info">
                          <span class="mr-4">
                              <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">
                                  <img alt="Pic" src="assets/media/users/300_19.jpg">
                              </div>
                          </span>
                          <div class="d-flex flex-column text-dark-75">
                              <span class="f-w-700">John Mathews</span>
                          </div>
                        </a>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                       <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-calendar-alt"></i> Feb 14th to Feb16th,20</span>
                           <span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 2 Nights, 1 Guest</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="icon-list"><a href="tel:"><i class="fas fa-phone-alt"></i></a></span>
                        <span class="icon-list"><a href="tel:"><i class="flaticon-chat"></i></i></a></span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fas fa-map-marker-alt"></i>Studio Apartment, Los Angeles</span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-clock"></i> 12:36 Hrs</span>
                           <span class="extra-space text-dark-75 font-weight-bolder font-size-sm">Remaining</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill my-1 opacity">
                        <div class="action-buttons">
                          <a href="#" class="btn btn-success font-weight-bold btn-square">Approved</a>
                          <a href="#" class="btn btn-danger font-weight-bold btn-square">Rejected</a>
                        </div>
                    </div>
                    <!--end: Item-->
                  </div>
                  <div class="extra-info">
                    <div class="description">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-In:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">3rd February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-Out:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">4th February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Stay Details:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 1 Night, 1 Huest(s)</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Property:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Studio Apartment, Los Angeles</span></div>
                        </div>
                        <div class="col-md-7">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="tel:+1(111)123-2345"><i class="fas fa-phone-alt"></i>+1 (111) 123-2345<a/></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="mailto:contactme@gmail.com"><i class="fas fa-envelope"></i>contactme@gmail.com</a></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><i class="flaticon-chat"></i>Send Chat Message</div>
                        </div>
                      </div>
                      <div class="text-dark-75 font-weight-bolder font-size-sm mt-5" style="font-weight: 700 !important;">Payment details</div>
                      <div class="row">  
                        <div class="col-md-4">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Amount:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">$1000</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Payment Mode:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Credit Card</span></div>
                        </div>
                        <div class="col-md-8">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label large">Transaction Status:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Successful</span></div>
                        </div>
                      </div>

                    </div>
                  </div>
              </div>
              <div class="booking-container-outer border-bottom">
                <div class="row-mute d-flex align-items-center flex-wrap booking-row border-bottom">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <a href="javascript:void(0)" class="user-extra-info">
                        <span class="mr-4">
                            <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">
                                <img alt="Pic" src="assets/media/users/300_19.jpg">
                            </div>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="f-w-700">John Mathews</span>
                        </div>
                      </a>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                       <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-calendar-alt"></i> Feb 14th to Feb16th,20</span>
                           <span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 2 Nights, 1 Guest</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="icon-list"><a href="tel:"><i class="fas fa-phone-alt"></i></a></span>
                        <span class="icon-list"><a href="tel:"><i class="flaticon-chat"></i></i></a></span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fas fa-map-marker-alt"></i>Studio Apartment, Los Angeles</span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-clock"></i> 12:36 Hrs</span>
                           <span class="extra-space text-dark-75 font-weight-bolder font-size-sm">Remaining</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill my-1 opacity">
                        <div class="action-buttons">
                          <a href="#" class="btn btn-success font-weight-bold btn-square">Approved</a>
                          <a href="#" class="btn btn-danger font-weight-bold btn-square">Rejected</a>
                        </div>
                    </div>
                    <!--end: Item-->
                </div>
                <div class="extra-info">
                    <div class="description">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-In:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">3rd February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-Out:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">4th February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Stay Details:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 1 Night, 1 Huest(s)</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Property:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Studio Apartment, Los Angeles</span></div>
                        </div>
                        <div class="col-md-7">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="tel:+1(111)123-2345"><i class="fas fa-phone-alt"></i>+1 (111) 123-2345<a/></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="mailto:contactme@gmail.com"><i class="fas fa-envelope"></i>contactme@gmail.com</a></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><i class="flaticon-chat"></i>Send Chat Message</div>
                        </div>
                      </div>
                      <div class="text-dark-75 font-weight-bolder font-size-sm mt-5" style="font-weight: 700 !important;">Payment details</div>
                      <div class="row">  
                        <div class="col-md-4">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Amount:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">$1000</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Payment Mode:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Credit Card</span></div>
                        </div>
                        <div class="col-md-8">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label large">Transaction Status:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Successful</span></div>
                        </div>
                      </div>

                    </div>
                  </div>
              </div>
              <div class="booking-container-outer border-bottom">
                  <div class="d-flex align-items-center flex-wrap booking-row border-bottom">
                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <a href="javascript:void(0)" class="user-extra-info">
                          <span class="mr-4">
                              <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">
                                  <img alt="Pic" src="assets/media/users/300_19.jpg">
                              </div>
                          </span>
                          <div class="d-flex flex-column text-dark-75">
                              <span class="f-w-700">John Mathews</span>
                          </div>
                        </a>
                      </div>
                      <!--end: Item-->

                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                         <div class="d-flex flex-column flex-lg-fill">
                            <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-calendar-alt"></i> Feb 14th to Feb16th,20</span>
                             <span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 2 Nights, 1 Guest</span>
                         </div>
                      </div>
                      <!--end: Item-->

                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                          <span class="icon-list"><a href="tel:"><i class="fas fa-phone-alt"></i></a></span>
                          <span class="icon-list"><a href="tel:"><i class="flaticon-chat"></i></i></a></span>
                      </div>
                      <!--end: Item-->

                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fas fa-map-marker-alt"></i>Studio Apartment, Los Angeles</span>
                      </div>
                      <!--end: Item-->

                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                          <div class="d-flex flex-column flex-lg-fill">
                            <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-clock"></i> 12:36 Hrs</span>
                             <span class="extra-space text-dark-75 font-weight-bolder font-size-sm">Remaining</span>
                         </div>
                      </div>
                      <!--end: Item-->

                      <!--begin: Item-->
                      <div class="d-flex align-items-center flex-lg-fill my-1 opacity">
                          <div class="action-buttons">
                          <a href="#" class="btn btn-success font-weight-bold btn-square">Approved</a>
                          <a href="#" class="btn btn-danger font-weight-bold btn-square">Rejected</a>
                        </div>
                      </div>
                      <!--end: Item-->
                  </div>
                  <div class="extra-info">
                    <div class="description">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-In:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">3rd February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-Out:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">4th February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Stay Details:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 1 Night, 1 Huest(s)</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Property:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Studio Apartment, Los Angeles</span></div>
                        </div>
                        <div class="col-md-7">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="tel:+1(111)123-2345"><i class="fas fa-phone-alt"></i>+1 (111) 123-2345<a/></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="mailto:contactme@gmail.com"><i class="fas fa-envelope"></i>contactme@gmail.com</a></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><i class="flaticon-chat"></i>Send Chat Message</div>
                        </div>
                      </div>
                      <div class="text-dark-75 font-weight-bolder font-size-sm mt-5" style="font-weight: 700 !important;">Payment details</div>
                      <div class="row">  
                        <div class="col-md-4">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Amount:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">$1000</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Payment Mode:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Credit Card</span></div>
                        </div>
                        <div class="col-md-8">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label large">Transaction Status:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Successful</span></div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              <div class="booking-container-outer border-bottom">
                <div class="d-flex align-items-center flex-wrap booking-row border-bottom">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <a href="javascript:void(0)" class="user-extra-info">
                        <span class="mr-4">
                            <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">
                                <img alt="Pic" src="assets/media/users/300_19.jpg">
                            </div>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="f-w-700">John Mathews</span>
                        </div>
                      </a>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                       <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-calendar-alt"></i> Feb 14th to Feb16th,20</span>
                           <span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 2 Nights, 1 Guest</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="icon-list"><a href="tel:"><i class="fas fa-phone-alt"></i></a></span>
                        <span class="icon-list"><a href="tel:"><i class="flaticon-chat"></i></i></a></span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fas fa-map-marker-alt"></i>Studio Apartment, Los Angeles</span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-clock"></i> 12:36 Hrs</span>
                           <span class="extra-space text-dark-75 font-weight-bolder font-size-sm">Remaining</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill my-1 opacity">
                        <div class="action-buttons">
                          <a href="#" class="btn btn-success font-weight-bold btn-square">Approved</a>
                          <a href="#" class="btn btn-danger font-weight-bold btn-square">Rejected</a>
                        </div>
                    </div>
                    <!--end: Item-->
                </div>
                <div class="extra-info">
                    <div class="description">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-In:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">3rd February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-Out:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">4th February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Stay Details:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 1 Night, 1 Huest(s)</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Property:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Studio Apartment, Los Angeles</span></div>
                        </div>
                        <div class="col-md-7">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="tel:+1(111)123-2345"><i class="fas fa-phone-alt"></i>+1 (111) 123-2345<a/></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="mailto:contactme@gmail.com"><i class="fas fa-envelope"></i>contactme@gmail.com</a></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><i class="flaticon-chat"></i>Send Chat Message</div>
                        </div>
                      </div>
                      <div class="text-dark-75 font-weight-bolder font-size-sm mt-5" style="font-weight: 700 !important;">Payment details</div>
                      <div class="row">  
                        <div class="col-md-4">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Amount:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">$1000</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Payment Mode:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Credit Card</span></div>
                        </div>
                        <div class="col-md-8">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label large">Transaction Status:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Successful</span></div>
                        </div>
                      </div>

                    </div>
                  </div>
              </div>
              <div class="booking-container-outer border-bottom">
                <div class="d-flex align-items-center flex-wrap booking-row border-bottom">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <a href="javascript:void(0)" class="user-extra-info">
                        <span class="mr-4">
                            <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">
                                <img alt="Pic" src="assets/media/users/300_19.jpg">
                            </div>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="f-w-700">John Mathews</span>
                        </div>
                      </a>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                       <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-calendar-alt"></i> Feb 14th to Feb16th,20</span>
                           <span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 2 Nights, 1 Guest</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="icon-list"><a href="tel:"><i class="fas fa-phone-alt"></i></a></span>
                        <span class="icon-list"><a href="tel:"><i class="flaticon-chat"></i></i></a></span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fas fa-map-marker-alt"></i>Studio Apartment, Los Angeles</span>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1 opacity">
                        <div class="d-flex flex-column flex-lg-fill">
                          <span class="text-dark-75 font-weight-bolder font-size-sm"><i class="far fa-clock"></i> 12:36 Hrs</span>
                           <span class="extra-space text-dark-75 font-weight-bolder font-size-sm">Remaining</span>
                       </div>
                    </div>
                    <!--end: Item-->

                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill my-1 opacity">
                        <div class="action-buttons">
                          <a href="#" class="btn btn-success font-weight-bold btn-square">Approved</a>
                          <a href="#" class="btn btn-danger font-weight-bold btn-square">Rejected</a>
                        </div>
                    </div>
                    <!--end: Item-->
                </div>
                <div class="extra-info">
                    <div class="description">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-In:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">3rd February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Check-Out:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">4th February 2020 8:00 PM</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Stay Details:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">2 Days, 1 Night, 1 Huest(s)</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Property:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Studio Apartment, Los Angeles</span></div>
                        </div>
                        <div class="col-md-7">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="tel:+1(111)123-2345"><i class="fas fa-phone-alt"></i>+1 (111) 123-2345<a/></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><a href="mailto:contactme@gmail.com"><i class="fas fa-envelope"></i>contactme@gmail.com</a></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><i class="flaticon-chat"></i>Send Chat Message</div>
                        </div>
                      </div>
                      <div class="text-dark-75 font-weight-bolder font-size-sm mt-5" style="font-weight: 700 !important;">Payment details</div>
                      <div class="row">  
                        <div class="col-md-4">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Amount:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">$1000</span></div>
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label">Payment Mode:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Credit Card</span></div>
                        </div>
                        <div class="col-md-8">
                          <div class="text-dark-75 font-weight-bold font-size-sm"><span class="inner-label large">Transaction Status:</span><span class="extra-space light text-dark-75 font-weight-bolder font-size-sm">Successful</span></div>
                        </div>
                      </div>

                    </div>
                  </div>
              </div>

              <div class="view-all-listing text-center">
                <a href="#" class="btn btn-danger font-weight-bold btn-square">View All</a>
              </div>
            </div>



         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->
<script>
$("a.user-extra-info").click(function(){
  $(this).parent().parent().parent().toggleClass('show');
});
</script>
<?php include('footer.php'); ?>
