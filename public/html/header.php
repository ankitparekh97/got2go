<!DOCTYPE html>
<!--
   Template Name: Metronic - Bootstrap 4 HTML, React, Angular 9 & VueJS Admin Dashboard Theme
   Author: KeenThemes
   Website: http://www.keenthemes.com/
   Contact: support@keenthemes.com
   Follow: www.twitter.com/keenthemes
   Dribbble: www.dribbble.com/keenthemes
   Like: www.facebook.com/keenthemes
   Purchase: https://1.envato.market/EA4JP
   Renew Support: https://1.envato.market/EA4JP
   License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
   -->
<html lang="en" >
   <!--begin::Head-->
   <head>
      <base href="">
      <meta charset="utf-8"/>
      <title>Metronic | Reports</title>
      <meta name="description" content="Updates and statistics"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
      <!--begin::Fonts-->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
      <!--end::Fonts-->
      <!--begin::Page Vendors Styles(used by this page)-->
      <link href="../html/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css"/>
      <!--end::Page Vendors Styles-->
      <!--begin::Global Theme Styles(used by all pages)-->

      <link href="../html/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
      <link href="../html/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/>
      <link href="../html/assets/css/pages/wizard/wizard-6.css" rel="stylesheet" type="text/css"/>
      <link href="../html/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
      <link href="../html/assets/plugins/custom/leaflet/leaflet.bundle.css" rel="stylesheet" type="text/css"/>

      <link type="text/css" rel="stylesheet" href="../html/assets/css/image-uploader.css">

      <link href="../html/assets/css/style.css" rel="stylesheet" type="text/css"/>
      <link href="../html/assets/css/style1.css" rel="stylesheet" type="text/css"/>
      <!--end::Global Theme Styles-->
      <!--begin::Layout Themes(used by all pages)-->
      <!--end::Layout Themes-->
      <link rel="shortcut icon" href="../html/assets/media/logos/favicon.ico"/>
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
   </head>
   <!--end::Head-->
   <!--begin::Body-->
   <body  id="kt_body"  class="header-fixed subheader-enabled page-loading"  >
      <!--begin::Main-->
      <div class="d-flex flex-column flex-root">
         <!--begin::Page-->
         <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
               <!--begin::Header Mobile-->
               <div id="kt_header_mobile" class="header-mobile " >
                  <!--begin::Logo-->
                  <a href="index.html">
                  <img alt="Logo" src="../html/assets/media/logos/logo-default.png" class="max-h-30px"/>
                  </a>
                  <!--end::Logo-->
                  <!--begin::Toolbar-->
                  <div class="d-flex align-items-center">
                     <button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
                     <span></span>
                     </button>
                     <button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
                        <span class="svg-icon svg-icon-xl">
                           <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                 <polygon points="0 0 24 0 24 24 0 24"/>
                                 <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                 <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                              </g>
                           </svg>
                           <!--end::Svg Icon-->
                        </span>
                     </button>
                  </div>
                  <!--end::Toolbar-->
               </div>
               <!--end::Header Mobile-->
               <!--begin::Header-->
               <div id="header-static" class="header" >
                  <!--begin::Container-->
                  <div class=" container ">
                     <!--begin::Left-->
                     <div class="d-none d-lg-flex align-items-center mr-3">
                        <!--begin::Logo-->
                        <a href="index.html" class="mr-20 pt-10 pb-10">
                        <img alt="Logo" src="../html/assets/media/logos/Logo.png" class="site-logo"/>
                        </a>
                        <!--end::Logo-->
                     </div>
                     <!--end::Left-->
                     <!--begin::Topbar-->
                     <div class="topbar  topbar-minimize ">
                        <!--begin::Search-->
                        <div class="topbar-item mr-3 w-100 w-lg-auto justify-content-start">
                           <div class="quick-search quick-search-inline w-auto w-lg-200px" id="kt_quick_search_inline">
                              <!--begin::Form-->
                              <form method="get" class="quick-search-form">
                                 <div class="input-group rounded bg-light">
                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <span class="svg-icon svg-icon-lg">
                                             <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                   <rect x="0" y="0" width="24" height="24"/>
                                                   <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                   <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
                                                </g>
                                             </svg>
                                             <!--end::Svg Icon-->
                                          </span>
                                       </span>
                                    </div>
                                    <input type="text" class="form-control h-40px" placeholder="Search..."/>
                                    <div class="input-group-append">
                                       <span class="input-group-text">
                                       <i class="quick-search-close ki ki-close icon-sm"></i>
                                       </span>
                                    </div>
                                 </div>
                              </form>
                              <!--end::Form-->
                              <!--begin::Search Toggle-->
                              <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="0px,1px"></div>
                              <!--end::Search Toggle-->
                           </div>
                        </div>
                        <!--end::Search-->
                        <!--begin::User-->
                        <div class="dropdown">
                           <!--begin::Toggle-->
                           <div class="topbar-item" data-toggle="dropdown" data-offset="0px,0px">
                              <div class="btn btn-icon btn-clean h-40px w-40px btn-dropdown pink-bg">
                                 <span class="svg-icon svg-icon-lg">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <polygon points="0 0 24 0 24 24 0 24"/>
                                          <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                          <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                                       </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                 </span>
                              </div>
                           </div>
                           <!--end::Toggle-->
                           <!--begin::Dropdown-->
                           <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg p-0">
                              <!--begin::Header-->
                              <div class="d-flex align-items-center p-8 rounded-top">
                                 <!--begin::Symbol-->
                                 <div class="symbol symbol-md bg-light-primary mr-3 flex-shrink-0">
                                    <img src="assets/media/users/300_21.jpg" alt=""/>
                                 </div>
                                 <!--end::Symbol-->
                                 <!--begin::Text-->
                                 <div class="text-dark m-0 flex-grow-1 mr-3 font-size-h5">Sean Stone</div>
                                 <span class="label label-light-success label-lg font-weight-bold label-inline">3 messages</span>
                                 <!--end::Text-->
                              </div>
                              <div class="separator separator-solid"></div>
                              <!--end::Header-->
                              <!--begin::Nav-->
                              <div class="navi navi-spacer-x-0 pt-5">
                                 <!--begin::Item-->
                                 <a href="custom/apps/user/profile-1/personal-information.html" class="navi-item px-8">
                                    <div class="navi-link">
                                       <div class="navi-icon mr-2">
                                          <i class="flaticon2-calendar-3 text-success"></i>
                                       </div>
                                       <div class="navi-text">
                                          <div class="font-weight-bold">
                                             My Profile
                                          </div>
                                          <div class="text-muted">
                                             Account settings and more
                                             <span class="label label-light-danger label-inline font-weight-bold">update</span>
                                          </div>
                                       </div>
                                    </div>
                                 </a>
                                 <!--end::Item-->
                                 <!--begin::Item-->
                                 <a href="custom/apps/user/profile-3.html"  class="navi-item px-8">
                                    <div class="navi-link">
                                       <div class="navi-icon mr-2">
                                          <i class="flaticon2-mail text-warning"></i>
                                       </div>
                                       <div class="navi-text">
                                          <div class="font-weight-bold">
                                             My Messages
                                          </div>
                                          <div class="text-muted">
                                             Inbox and tasks
                                          </div>
                                       </div>
                                    </div>
                                 </a>
                                 <!--end::Item-->
                                 <!--begin::Item-->
                                 <a href="custom/apps/user/profile-2.html"  class="navi-item px-8">
                                    <div class="navi-link">
                                       <div class="navi-icon mr-2">
                                          <i class="flaticon2-rocket-1 text-danger"></i>
                                       </div>
                                       <div class="navi-text">
                                          <div class="font-weight-bold">
                                             My Activities
                                          </div>
                                          <div class="text-muted">
                                             Logs and notifications
                                          </div>
                                       </div>
                                    </div>
                                 </a>
                                 <!--end::Item-->
                                 <!--begin::Item-->
                                 <a href="custom/apps/userprofile-1/overview.html" class="navi-item px-8">
                                    <div class="navi-link">
                                       <div class="navi-icon mr-2">
                                          <i class="flaticon2-hourglass text-primary"></i>
                                       </div>
                                       <div class="navi-text">
                                          <div class="font-weight-bold">
                                             My Tasks
                                          </div>
                                          <div class="text-muted">
                                             latest tasks and projects
                                          </div>
                                       </div>
                                    </div>
                                 </a>
                                 <!--end::Item-->
                                 <!--begin::Footer-->
                                 <div class="navi-separator mt-3"></div>
                                 <div class="navi-footer  px-8 py-5">
                                    <a href="custom/user/login-v2.html" target="_blank" class="btn btn-light-primary font-weight-bold">Sign Out</a>
                                    <a href="custom/user/login-v2.html" target="_blank" class="btn btn-clean font-weight-bold">Upgrade Plan</a>
                                 </div>
                                 <!--end::Footer-->
                              </div>
                              <!--end::Nav-->
                           </div>
                           <!--end::Dropdown-->
                        </div>
                        <!--end::User-->
                     </div>
                     <!--end::Topbar-->
                  </div>
                  <!--end::Container-->
               </div>
               <!--end::Header-->
               <!--begin::Header Menu Wrapper-->
               <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                  <div class=" container ">
                     <!--begin::Header Menu-->
                     <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default header-menu-root-arrow " >
                        <!--begin::Header Nav-->
                        <ul class="menu-nav ">
                           <li class="menu-item  menu-item-active">
                              <a  href="javascript:;" class="menu-link menu-toggle"><span class="menu-text">Dashboard</span></a>
                           </li>
                           <li class="menu-item">
                              <a  href="#" class="menu-link menu-toggle"><span class="menu-text">Calendar</span></span></a>
                           </li>
                           <li class="menu-item">
                              <a  href="listing.php" class="menu-link menu-toggle"><span class="menu-text">Listing</span></span></a>
                           </li>
                           <li class="menu-item">
                              <a  href="#" class="menu-link menu-toggle"><span class="menu-text">Booking</span></span></a>
                           </li>
                           <li class="menu-item">
                              <a  href="#" class="menu-link menu-toggle"><span class="menu-text">Payment</span></span></a>
                           </li>
                           <li class="menu-item">
                              <a  href="#" class="menu-link menu-toggle"><span class="menu-text">Profile</span></span></a>
                           </li>
                        </ul>
                        <!--end::Header Nav-->
                     </div>
                     <!--end::Header Menu-->
                  </div>
               </div>
               <!--end::Header Menu Wrapper-->
