<?php include('header.php'); ?>
<!--begin::Container-->
<div class="d-flex flex-row flex-column-fluid  container ">
    <!--begin::Content Wrapper-->
    <div class="main d-flex flex-column flex-row-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 pb-0" id="kt_subheader">
            <ul class="nav nav-tabs nav-tabs-line justify-content-center w-100">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1">Basic Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2">Verification Documents</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_3">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_4">Payment Settings</a>
                </li>
            </ul>

        </div>
        <!--end::Subheader-->
        <form novalidate="novalidate" id="kt_wizard_form">
            <div class="content flex-column-fluid d-lg-flex" id="kt_content">

                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2">


                        <!--begin: Wizard Step 1-->
                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                            <div class="steps-container mt-0">
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input id="first_name" type="text" class="form-control" value="" name="" placeholder="First name" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input id="last_name" type="text" class="form-control" value="" name="" placeholder="Last name" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>UserName</label>
                                            <input id="username" type="text" class="form-control" value="" name="" placeholder="Username" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Bio</label>
                                            <textarea id="bio" class="form-control" rows="2" placeholder="Write about yourself here"></textarea>
                                            <!-- <input id="bio" type="text" class="form-control" value="" name="" placeholder="Write about yourself here"/>                                           -->
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="kt_datepicker_2" readonly="readonly" placeholder="Select date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email ID</label>
                                            <input type="email" name="email_id" class="form-control" required="">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Phone No</label>
                                            <input type="number" class="form-control" name="" placeholder="Phone Number" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input id="city" type="text" class="form-control" value="" name="" placeholder="City" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>State</label>
                                            <input id="state" type="text" class="form-control" value="" name="" placeholder="State" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input id="address" type="address" class="form-control" value="" name="" placeholder="your address here" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Languages known</label>
                                            <input id="languages" type="text" class="form-control" value="" name="" placeholder="English, Spanish" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Form Group-->
                            </div>
                        </div>
                        <div class="d-flex justify-content-between pt-7 float-right">

                            <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">
                                Next
                                <span class="svg-icon svg-icon-md ml-2">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24" />
                                            <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1" />
                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) " />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </button>

                        </div>


                    </div>
                    <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">

                        <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
                            <!--begin::Container-->
                            <div class="wizard-content d-flex flex-column mx-auto w-100">
                                <!--begin::Nav-->
                                <div class="d-flex flex-column-auto flex-column px-10 header-wizard">
                                    <!--begin: Wizard Nav-->
                                    <div class="wizard-nav d-flex flex-column  align-items-center align-items-md-start">
                                        <!--begin::Wizard Steps-->
                                        <div class="wizard-steps d-flex flex-column flex-md-row w-md-700px mx-auto w-100">
                                            <!--begin::Wizard Step 1 Nav-->
                                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                                                <div class="wizard-wrapper pr-lg-7 pr-5">
                                                    <div class="wizard-icon">
                                                        <i class="wizard-check ki ki-check"></i>
                                                        <span class="wizard-number">
                                                            1
                                                        </span>
                                                    </div>
                                                    <div class="wizard-label mr-3">
                                                        <h3 class="wizard-title">
                                                            Document Upload
                                                        </h3>

                                                    </div>
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) " />
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                            <!--end::Wizard Step 1 Nav-->
                                            <!--begin::Wizard Step 2 Nav-->
                                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                                <div class="wizard-wrapper pr-lg-7 pr-5">
                                                    <div class="wizard-icon">
                                                        <i class="wizard-check ki ki-check"></i>
                                                        <span class="wizard-number">
                                                            2
                                                        </span>
                                                    </div>
                                                    <div class="wizard-label mr-3">
                                                        <h3 class="wizard-title">
                                                            Under Verification
                                                        </h3>
                                                    </div>
                                                    <span class="svg-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) " />
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                            <!--end::Wizard Step 2 Nav-->
                                            <!--begin::Wizard Step 3 Nav-->
                                            <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                                <div class="wizard-wrapper">
                                                    <div class="wizard-icon">
                                                        <i class="wizard-check ki ki-check"></i>
                                                        <span class="wizard-number">
                                                            3
                                                        </span>
                                                    </div>
                                                    <div class="wizard-label">
                                                        <h3 class="wizard-title">
                                                            Verified
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Wizard Step 3 Nav-->
                                        </div>
                                        <!--end::Wizard Steps-->
                                    </div>
                                    <!--end: Wizard Nav-->
                                </div>
                                <!--end::Nav-->


                                <!--begin: Wizard Step 1-->
                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                                    <div class="steps-container mt-10">
                                        <!--begin::Form Group-->
                                        <div class="row">
                                            <div class="col-md-4 offset-md-2">

                                                <div class="form-group">
                                                    <label></label>
                                                    <div class="radio-list">
                                                        <label class="radio">
                                                            <input type="radio" name="radios1">
                                                            <span></span>Upload Government ID from system</label>
                                                        <label class="radio">
                                                            <input type="radio" checked="checked" name="radios1">
                                                            <span></span>Take photo from browser</label>
                                                        <!-- <label class="radio">
															<input type="radio" checked="checked" name="radios1">
															<span></span>Checked</label> -->
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <label>Upload</label>

                                                    <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
                                                        <div class="dropzone-msg dz-message needsclick">
                                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                                            <span class="dropzone-msg-desc">This is just a demo dropzone. Selected files are
                                                                <strong>not</strong>actually uploaded.</span>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!--end::Form Group-->
                                    </div>
                                </div>
                                <!--end: Wizard Step 1-->
                                <div class="pb-5" data-wizard-type="step-content">

                                    <div class="steps-container mt-10">
                                        <!--begin::Form Group-->
                                        <div class="row">
                                            <div class="col-md-6 offset-md-3">
                                                <div class="form-group">

                                                    <label>Upload</label>

                                                    <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_2">
                                                        <div class="dropzone-msg dz-message needsclick">
                                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                                            <span class="dropzone-msg-desc">This is just a demo dropzone. Selected files are
                                                                <strong>not</strong>actually uploaded.</span>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!--end::Form Group-->
                                    </div>
                                </div>
                                <div class="pb-5" data-wizard-type="step-content">

                                    <div class="steps-container mt-10">
                                        <!--begin::Form Group-->
                                        <div class="row">
                                            <div class="col-md-6 offset-md-3">
                                                <div class="form-group">

                                                    <label>Uploaded Government ID</label>

                                                    <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_3">
                                                        <div class="dropzone-msg dz-message needsclick">
                                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                                            <span class="dropzone-msg-desc">This is just a demo dropzone. Selected files are
                                                                <strong>not</strong>actually uploaded.</span>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 offset-md-3">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-4 pr-4 mr-3">Upload</button>
                                                    <button type="button" class="btn btn-danger font-weight-bolder font-size-h6 pl-4 pr-4 mr-3">Remove</button>

                                                    <button type="button" class="btn btn-success font-weight-bolder font-size-h6 pl-4 pr-4 ">Add ID</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Form Group-->
                                </div>
                            </div>
                            <!--end: Wizard Actions-->

                        </div>
                        <!--end::Container-->
                    </div>

                    <div class="tab-pane fade" id="kt_tab_pane_3" role="tabpanel" aria-labelledby="kt_tab_pane_3">
                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                            <div class="steps-container mt-0">
                                <h5 class="f-w-700 mt-5 mb-10">Change Password</h5>
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Current Password</label>
                                            <input id="old_pass" type="password" class="form-control" value="" name="" placeholder="Current Password" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input id="new_pass" type="password" class="form-control" value="" name="" placeholder="New Password" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Re-enter New Password</label>
                                            <input id="renter_new_pass" type="password" class="form-control" value="" name="" placeholder="Re-enter New Password" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-8 py-4 my-3" data-wizard-type="action-update">
                                            Update</button>
                                    </div>
                                </div>

                                <hr class="mt-10">
                                <h5 class="f-w-700 mt-10 mb-10">Currency</h5>
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label>Preferred Currency</label>
                                            <select class="form-control" id="pref_currency">
                                                <option>$ US Dollar</option>
                                                <option>Rs INR</option>

                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <hr class="mt-5">
                                <h5 class="f-w-700 mt-10 mb-10">Communiation Preferences</h5>
                                <!--begin::Form Group-->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">

                                        <div class="form-group">
                                            <label>Booking notifications, messages from renters, and trip reminders</label>
                                            <div class="checkbox-inline">
                                                <label class="checkbox">
                                                    <input type="checkbox" checked="checked" name="chk_select_all">
                                                    <span></span>Select All</label>
                                                <label class="checkbox">
                                                    <input type="checkbox" checked="checked" name="chk_notification">
                                                    <span></span>Notification </label>
                                                <label class="checkbox">
                                                    <input type="checkbox" checked="checked" name="chk_email">
                                                    <span></span>Email </label>
                                                <label class="checkbox">
                                                    <input type="checkbox" checked="checked" name="chk_sms">
                                                    <span></span>SMS </label>
                                            </div>
                                            <!-- <span class="form-text text-muted">Some help text goes here</span> -->
                                        </div>
                                    </div>

                                </div>


                                <!--end::Form Group-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <!--end::Content-->
    </div>
    <!--begin::Content Wrapper-->
</div>
<!--end::Container-->
<?php include('footer.php'); ?>