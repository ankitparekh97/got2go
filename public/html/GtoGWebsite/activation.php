<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../assets/css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../../plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <title>GtoG Home</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="#mainnavigatn" class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item margin-right-menu-200">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="" class="nav-link btn-tripper"><img src="../../html/assets/media/images/btn-tripper.svg" alt="" /></a>
                    </li>
                    <li class="nav-item dropdown ">
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-user-text">Account</span> <img src="../../html/assets/media/images/user.svg" alt="" /></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active-item-link" href="#">Become a tripper</a>
                            <a class="dropdown-item" href="#">Tripboards</a>
                            <a class="dropdown-item" href="#">Rent a Stay</a>
                            <a class="dropdown-item" href="#">List a Stay</a>
                            <!-- <div class="dropdown-divider"></div> -->
                            <!-- <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#login_popup">
                                Modal - lg
                            </button> -->
                            <a class="dropdown-item font-light" href="" data-toggle="modal" data-target="#login_popup">Login</a>
                            <a class="dropdown-item font-light" href="#">Help</a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="activation-banner">
        <div class="container">
            <div class="row m-auto">
                <div class="col-md-8 m-auto">
                    <div class="mt-40 activation-box">
                        <div class="row">
                            <div class="col-lg-2">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="91.541" height="91.541" viewBox="0 0 91.541 91.541">
                                    <defs>
                                      <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                                        <stop offset="0" stop-color="#e40046"/>
                                        <stop offset="1" stop-color="#fce300"/>
                                      </linearGradient>
                                    </defs>
                                    <g id="correct_2_" data-name="correct (2)" style="isolation: isolate">
                                      <g id="Group_410" data-name="Group 410">
                                        <path id="Path_2470" data-name="Path 2470" d="M78.135,13.406A45.771,45.771,0,0,0,13.406,78.135,45.771,45.771,0,1,0,78.135,13.406Zm-10.5,21.472L42.059,60.456a2.682,2.682,0,0,1-3.793,0L23.9,46.093A2.682,2.682,0,1,1,27.7,42.3L40.163,54.767,63.845,31.085a2.682,2.682,0,0,1,3.793,3.793Z" fill="url(#linear-gradient)"/>
                                      </g>
                                    </g>
                                  </svg>

                            </div>
                            <div class="col-lg-6">
                                <h1 class="congratulation-text">Congratulations!</h1>
                                <h4 class="activation-message-text">Your Got2Go account is Activated.</h4>
                            </div>
                            <div class="col-lg-4 text-right">
                                <button class="activation-to-login-btn">Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="../../plugins/global/plugins.bundle.js"></script>
    <script src="../../js/scripts.bundle.js"></script>
    <script src="../../js/pages/crud/forms/widgets/select2.js"></script>

</body>

</html>