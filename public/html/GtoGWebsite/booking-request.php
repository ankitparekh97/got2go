<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <link rel="stylesheet" href="../../css/webfonts/got2gofont.css">

    <link rel="stylesheet" href="../assets/plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../assets/css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <link rel="stylesheet" href="../../css/rental/rental2.css">
    <link rel="stylesheet" href="../../css/rental/dashboard.css">
    <link rel="icon" href="../../media/images/Gtog.png" type="image/png">
    <title>Booking Request</title>

</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <style>
    </style>
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Booking request Wrapper -->
    <div class="booking-request-wrapper">
        <div class="container-custom pt-20">
            <div class="container">
                <div class="mainpage-title-wrap">
                    <div class="mainpage-title-left">
                        <a href="javascript:void(0)" class="back-link mb-2"><i class="got got-arrow-left-solid"> </i>Back</a>
                        <h2 class="mainpage-title"><b>LET’S GO!</b></h2>
                    </div>
                </div>
                <div class="bookingrequt-content">
                    <div class="row">
                        <div class="col-xl-6 order-12 order-sm-1">
                            <!-- Payment Method -->
                            <div class="bookingrequt-method-wrapper">
                                <div class="label-control mb-3">Payment Method</div>
                                <div class="btn-group payment-dropdown">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="payment-img"><img src="../../html/assets/media/images/mastercard.png" /></span><span class="paycard-number"> Mastercard ******5139 </span></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item active"><a href="#"> <span class="payment-img"><img src="../../html/assets/media/images/mastercard.png" /> </span> Mastercard ******5139 </a></li>

                                        <li class="dropdown-item"><a href="#"> <span class="payment-img"><img src="../../html/assets/media/images/visa.png" /></span> Visa ******5139 </a></li>
                                        <li class="dropdown-item"><a href="#"> <span class="payment-img"></span> Add Debit/Credit Card </a></li>

                                    </ul>
                                </div>
                                <div class="payment-form">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 mb-2">
                                            <input type="text" class="form-control formcontrol-custom" id="" aria-describedby="emailHelp" placeholder="Card Number">
                                        </div>
                                        <div class=" col-6 col-md-3">
                                            <input type="text" class="form-control formcontrol-custom" id="" aria-describedby="emailHelp" placeholder="MM/YY">
                                        </div>
                                        <div class="col-6 col-md-3">
                                            <input type="text" class="form-control formcontrol-custom" id="" aria-describedby="emailHelp" placeholder="CVV">
                                        </div>
                                    </div>
                                    <div class="payment-action">
                                        <button type="submit" class="btn  btn-700 btn-outline-orange btn-round"> Save Card</button>
                                    </div>
                                </div>
                            </div>
                            <!-- End Payment Method -->

                            <!-- Message Owner Method -->
                            <div class="bookingrequt-method-wrapper">
                                <div class="label-control mb-3">INCLUDE A MESSAGE FOR THE OWNER</div>
                                <div class="message-owner-info">
                                    <div class="owner-content mb-3">
                                        <div class="owner-img">
                                            <img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
                                        </div>
                                        <div class="owner-name"> ANDREA</div>
                                    </div>
                                    <div class="payment-form">
                                        <textarea class="form-control" id="#" rows="5" placeholder="Message"></textarea>
                                    </div>

                                </div>
                            </div>
                            <!-- End Message Owner Method -->

                            <!-- Message Owner Method -->
                            <div class="bookingrequt-method-wrapper">
                                <div class="label-control mb-3">CANCELLATION POLICY</div>
                                <div class="message-owner-info">
                                    <p class="pera">Free cancellation until 07/03/2020. After this you will be charged 50% of the total upon cancellation.</p>
                                </div>
                            </div>
                             <!-- DISCLAIMER POLICY -->
                         <div class="bookingrequt-method-wrapper">
                            <div class="label-control mb-3">DISCLAIMER</div>
                            <div class="message-owner-info">
                                <p class="pera">
                                Although GOT2GO seeks to verify all hosts' submitted information for accuracy in content, GOT2GO encourages all travelers and Trippers to use due diligence and discretion when booking with property owners through GOT2GO.
                                </p>
                            </div>
                        </div>
                        <!-- DISCLAIMER POLICY -->
                            <!-- End Message Owner Method -->
                            <div class="bookingrequt-action text-center">
                                <a href="javascript:void(0)" class="button-default-animate button-default-custom btn-sunset-gradient" data-toggle="modal" data-target="#BookingConfirm">SUBMIT BOOKING REQUEST</a>
                            </div>

                        </div>

                        <div class="col-xl-6">
                            <div class="booking-detail-wrapper"> <!-- payment-danger -->
                                <div class="bookingdetail-innerblock">
                                    <div class="bookingdetail-header">
                                        <div class="bookingdetail-img">
                                            <img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
                                        </div>
                                        <div class="bookingdetail-info">
                                            <div class="booking-subtitle">Private Residence</div>
                                            <h4 class="booking-title">CALIFORNIA FRESH</h4>
                                            <div class="booking-address">
                                                1420 Torrey Pines <span>Daytona, Florida 12345</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="booking-checkin-info">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-sm-4 text-center ">
                                                <div class="display-inline">
                                                    <span>CHECK-IN</span>
                                                    <h5>07/17/2020</h5>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-4 text-center">
                                                <div class="display-inline">
                                                    <span>CHECK-OUT</span>
                                                    <h5>07/20/2020</h5>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-4 text-center">
                                                <div class="display-inline">
                                                    <span>GUESTS</span>
                                                    <h5>2</h5>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="pricing-table-breakdown">
                                        <h5>REGULAR PRICE</h5>
                                        <ul class="list-unstyled mb-0">
                                            <li class="row">
                                                <div class="col-8">
                                                    <span>$450 x 3 nights</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>$1,350</span>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-8">
                                                    <span>Cleaning Fee</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>$95</span>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-8">
                                                    <span>Service Fee</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>$37</span>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-8">
                                                    <span>Occupancy taxes/fees</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>$42</span>
                                                </div>
                                            </li>
                                            <li class="row text-danger">
                                                <div class="col-8">
                                                    <span>Coupon Applied</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>-$42</span>
                                                </div>
                                            </li>
                                            <li class="row">
                                                <div class="col-12">
                                                <a href="#" class="comm-link text-pink " data-toggle="modal" data-target="#applycouponModal">Enter a coupon</a>
                                                </div>
                                            </li>

                                            <li class="row price-total">
                                                <div class="col-8">
                                                    <span>TOTAL</span>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <span>$1,524</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- <div class="mt-4">
                                            <a href="#" class="comm-link" data-toggle="modal" data-target="#applycouponModal">Enter a coupon</a>
                                        </div> -->
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Booking request Wrapper -->


    <div class="live-life-travel text-center">
        <div class="container">
            <h3>Live Life Travel</h3>
            <p>Your exclusive details are waiting</p>
        </div>
        <div class="subscribe-box-outer">
            <div class="subscribe-box-inner">
                <form>
                    <div class="input-box-cstm">
                        <span><img src="../assets/media/images/tripper-page/msg-img.png" /></span>
                        <input type="text" placeholder="Type Your Email" />
                    </div>
                    <div class="submit-btn-cstm">
                        <input type="submit" name="submit" value="Subscribe" />
                    </div>
                </form>
            </div>
        </div>
    </div>


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../assets/media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">View Stays</a></li>
                            <li><a href="#">List your Stay</a></li>
                            <li><a href="#">Tripboards</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../assets/media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <!-- Booking success confirmation Modal -->
    <div class="modal fade custom-modal" id="BookingConfirm" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <div class="modal-container bookingbonfirm-wrapper align-items-center vertical-middle text-center">
                        <div class="bookingbonfirm-bg">
                            <h1> Success!</h1>
                            <div class="bookingconft-center-block">
                                <p class="pera-modal mb-0">
                                    You’ve successfully booked “The California Fresh” for 07/17/2020 to 07/20/2020. We will send you a confirmation email shortly.
                                </p>
                                <!-- <p class="pera-modal mb-0">
                                We’ve sent your booking request to the owner.  They have 24 hours to approve your request.  Upon approval you will recieve a booking and payment confirmation email.  If approval is denied we will notify you and provide suggestions for similar stays.
                                </p> -->
                            </div>
                            <div class="modal-action">
                                <button type="submit" id="" class="button-default-animate btn-round btn-opensan btn-sunset-gradient  mt-5">View My Stays</button>
                                <!-- <button type="submit" id="save_btn" class="btn btn-800 btn-link mt-5">Cancel</button> -->
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>    
        <!-- End  Booking success confirmation Modal -->


        <div class="modal fade" id="cancellation-policy" tabindex="-1" role="dialog" aria-labelledby="cancellation-policy" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <!-- <i aria-hidden="true" class="ki ki-close"></i> -->
                            <span class="svg-icon svg-icon-primary svg-icon-3x">
                                <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo8\dist/../src/media/svg/icons\Navigation\Close.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                            <rect x="0" y="7" width="16" height="2" rx="1" />
                                            <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1" />
                                        </g>
                                    </g>
                                </svg>
                                <!--end::Svg Icon--></span>
                        </button>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="cancellation-sec">
                                    <div class="form-group">
                                        <div class="radio-list">
                                            <label class="radio">
                                                <input type="radio" checked="checked" name="cancellation">
                                                <span></span>
                                                <div class="w-100"><span class="f-w-700">Flexible: </span>Guests can request a full refund within a limited period. Cancellation requests received at least 24 hours before the check-in time will be given full refund.</div>
                                            </label>
                                        </div>
                                    </div>
                                    <?php /*
                               <div class="form-group">
                                   <div class="radio-list">
                                      <label class="radio">
                                         <input type="radio" name="cancellation">
                                         <span></span>
                                         <div class="w-100"><span class="f-w-700">Moderate: </span>Guests can request a refund.If a guest cancels within 5 days of the check-in date, then the first night of the booking will not be refunded and only 50% of the accommodation fees for the rest of the booking will then be refunded. If the guest decides to cancel after check-in then 50% of the accommodation fees for any unspent nights are refundable. However, any nights spent will not be refunded.</div>
                                     </label>
                                  </div>
                               </div>
                               <div class="form-group">
                                   <div class="radio-list">
                                      <label class="radio">
                                         <input type="radio" name="cancellation">
                                         <span></span>
                                         <div class="w-100"><span class="f-w-700">Strict: </span>Guests may receive a full refund if they cancel within 48 hours of booking and at least 14 full days prior to the listing’s local check-in time. After 48 hours guests are only entitled to a 50% refund regardless of how far the check-in date is.</div>
                                     </label>
                                  </div>
                               </div>
                               */ ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
 <!-- Add Coupon Modal -->
 <div class="modal fade custom-modal" id="applycouponModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-white">
                <div class="modal-header">
                    <button type="button" class="close" id="loginCloseModal" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="1" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="></image>
                        </svg>
                    </button>
                </div>
                <div class="modal-body bg-white">
                    <div class="modal-container coupon-wrapper align-items-center vertical-middle">
                        <form id="myCouponSetting">
                            <h1> Apply a Coupon </h1>
                              <div class="form-group-custm no-icon">
                                    <!-- <input class="form-class-cstm error" type="text" name="firstname" placeholder="Enter Coupon Code"> -->
                                    <input class="form-class-cstm" type="text" name="firstname" placeholder="Enter Coupon Code">
                                    <div class="error-block"> Please enter a valid coupon code </div>
                                </div>
                            <div class="modal-action text-center">
                                <button type="submit" id="" class="button-default-animate btn-round btn-opensan btn-sunset-gradient mt-5  ">Apply</button>
                                <!-- Pelase Add "disabled" class in button -->
                                
                            </div>
                           </form> 

                    </div>

                </div>
            </div>
        </div>
        <!-- End Add Coupon Modal -->


        <script src="js/jquery.validate.js"></script>
        <script src="../assets/plugins/global/plugins.bundle.js"></script>
        <script src="../assets/js/scripts.bundle.js"></script>
        <script src="../assets/js/pages/crud/forms/widgets/select2.js"></script>
        <script src="../assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script>
            jQuery('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            jQuery('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                vertical: true,
                dots: false,
                prevArrow: '<button class="thumbnail-custom-arrows prev"><span class="prev" >‹</button>',
                nextArrow: '<button class="thumbnail-custom-arrows next"><span class="next" >›</button>',
                focusOnSelect: true
            });

            jQuery('#propert-detail-page-slider').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                dots: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        </script>
        <script>
            $(" .payment-dropdown .dropdown-menu li a").click(function() {
                var selText = $(this).text();
                $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + '<span class="caret"></span>');
            });
        </script>
         
        

</body>

</html>