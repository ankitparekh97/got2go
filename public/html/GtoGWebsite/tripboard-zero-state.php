<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../../css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../../plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <link rel="stylesheet" href="../../css/rental/rental2.css">
    <link rel="stylesheet" href="../../css/rental/dashboard.css">
    <title>trippers Only</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="tripboard-view">
        <div class="container-fluid p-0">
            <form class="form">
                <div class="row m-0">
                    <div class="col-xl-3 tripboard-edit min-width-auto">
                        <div class="w-100">
                            <p>
                                <button class="previous-icon mr-5" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g id="Arrow_Icon_Prev" data-name="Arrow Icon prev" transform="translate(-147 -1381)">
                                            <rect id="Shape" width="24" height="24" transform="translate(147 1381)" fill="rgba(0,0,0,0)"></rect>
                                            <path id="Path_94" data-name="Path 94" d="M157.384,1393.03l4.853,5.295a1,1,0,0,1-1.474,1.351l-5.5-6a1,1,0,0,1,.03-1.383l6-6a1,1,0,0,1,1.414,1.414Z" fill="#939FA4"></path>
                                        </g>
                                    </svg>
                                </button><img class="single-tripboard" src="../../media/images/single-tripboard.svg">
                            </p>
                            <div class="input-group mt-12">
                                <input type="text" class="input-boards-title" placeholder="Saved Favorites " disabled>
                            </div>
                            <div class="border-separator-s mb-15" style="margin-top: -15px;"></div>
                            <div class="mb-2 row">
                                <div class="col-6 pr-1">
                                    <label class="col-form-label">Suggested Stays</label>
                                </div>
                                <div class="col-6 pl-1 text-right float-right">
                                    
                                </div>

                            </div>

                            <div class="tripboard-edit-right-section padding-0">
                                <div class="d-flex flax-wrap mb-10">

                                    <div class="flex-shrink-0 mr-7">
                                        <div class="symbol symbol-140 symbol-lg-140">
                                            <img alt="Pic" src="../../media/images/recent-view-2.png">
                                        </div>
                                    </div>
                                    <div class="flex-grow-1">

                                        <div class="d-flex align-items-center justify-content-between flex-wrap">

                                            <div class="">
                                                <p class="name-place"> Bay Club of Sandestein
                                                </p>
                                                <p class="price-of-place">$650/stay</p>
                                                <ul class="recent-view-ul">
                                                    <li>1 bed </li>
                                                    <li>3 baths</li>
                                                    <li>7 guests Villa</li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="d-flex flax-wrap mb-10">

                                    <div class="flex-shrink-0 mr-7">
                                        <div class="symbol symbol-140 symbol-lg-140">
                                            <img alt="Pic" src="../../media/images/recent-view-2.png">
                                        </div>
                                    </div>
                                    <div class="flex-grow-1">

                                        <div class="d-flex align-items-center justify-content-between flex-wrap">

                                            <div class="">
                                                <p class="name-place"> Bay Club of Sandestein
                                                </p>
                                                <p class="price-of-place">$650/stay</p>
                                                <ul class="recent-view-ul">
                                                    <li>1 bed </li>
                                                    <li>3 baths</li>
                                                    <li>7 guests Villa</li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="d-flex flax-wrap mb-10">

                                    <div class="flex-shrink-0 mr-7">
                                        <div class="symbol symbol-140 symbol-lg-140">
                                            <img alt="Pic" src="../../media/images/recent-view-2.png">
                                        </div>
                                    </div>
                                    <div class="flex-grow-1">

                                        <div class="d-flex align-items-center justify-content-between flex-wrap">

                                            <div class="">
                                                <p class="name-place"> Bay Club of Sandestein
                                                </p>
                                                <p class="price-of-place">$650/stay</p>
                                                <ul class="recent-view-ul">
                                                    <li>1 bed </li>
                                                    <li>3 baths</li>
                                                    <li>7 guests Villa</li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xl-9 tripboard-edit-right-section pb-0">
                        <div class="d-block w-100 mb-7">
                            <div class="row m-0">
                                <div class="col-lg-7 pl-0 pr-0 where-to-go">
                                    <div class="row m-0">
                                        <div class="col-lg-8 col-md-9 p-5 pt-9 pr-2">
                                            <div class="input-group pl-3">
                                                <img src="https://got2go.kitelytechdev.com/media/images/location.svg" alt="">
                                                <input type="text" class="form-control pac-target-input" autocomplete="off" name="location_detail" id="location_detail" placeholder="Got2Go to">
                                            </div>

                                        </div>

                                        <div class="col-lg-4 col-md-3 text-right pl-0 pt-5 pr-5 pb-5">
                                            <a href="" class="pr-4 pt-3 refine_filter_icon" data-toggle="modal" data-target="#filter_popup"><img src="https://got2go.kitelytechdev.com/media/images/filter.svg" alt=""></a>
                                            <button type="button"><img src="https://got2go.kitelytechdev.com/media/images/gobutton.svg" alt="" style="height: 73px;"></button>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-7 pl-0 pr-0 checkboxes-inline">
                                    <div class="row m-0 w-100">
                                        <div class="col-4 pl-0 pr-2">
                                            <label class="custom-checkbox common-fatfrank">Vacation club
                                                <input type="checkbox" checked="checked">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-3 pl-0 pr-4">
                                            <label class="custom-checkbox common-fatfrank float-right">hotels
                                                <input type="checkbox" checked="checked">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-5 pl-0">
                                            <label class="custom-checkbox common-fatfrank unchecked float-right">private residences
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row potential-listing-section overflow-hidden">
                            <div class="potential-listing-title common-open"><b>SAVED</b> FAVORITES</div>
                            <div class="col-12 pl-0 pr-0 tripper-dboard-form">
                                <div class="d-block text-center">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                    <div class="zero-state-text common-open">You have no listings favorited yet. Search for listings above to save them to your Favorites!</div>
                                </div>
                                

                            </div>
                            <div class="tripbord-save-property">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="tripboard-saved-prty-box">
                                            <div class="image">
                                                <img src="../../media/images/saved-property.jpg" alt="">
                                                <img src="../../media/images/favorite-heart-icon.svg" class="favourite" alt="">
                                            </div>
                                            <div class="description">
                                                <h3>The California Fresh</h3>
                                                <h6>Malibu, California</h6>
                                                <p class="price-per-night"><b>$589</b>/night</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="tripboard-saved-prty-box">
                                            <div class="image">
                                                <img src="../../media/images/saved-property.jpg" alt="">
                                                <img src="../../media/images/favorite-heart-icon.svg" class="favourite" alt="">
                                            </div>
                                            <div class="description">
                                                <h3>The California Fresh</h3>
                                                <h6>Malibu, California</h6>
                                                <p class="price-per-night"><b>$589</b>/night</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="tripboard-saved-prty-box">
                                            <div class="image">
                                                <img src="../../media/images/saved-property.jpg" alt="">
                                                <img src="../../media/images/favorite-heart-icon.svg" class="favourite" alt="">
                                            </div>
                                            <div class="description">
                                                <h3>The California Fresh</h3>
                                                <h6>Malibu, California</h6>
                                                <p class="price-per-night"><b>$589</b>/night</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="tripboard-saved-prty-box">
                                            <div class="image">
                                                <img src="../../media/images/saved-property.jpg" alt="">
                                                <img src="../../media/images/favorite-heart-icon.svg" class="favourite" alt="">
                                            </div>
                                            <div class="description">
                                                <h3>The California Fresh</h3>
                                                <h6>Malibu, California</h6>
                                                <p class="price-per-night"><b>$589</b>/night</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="tripboard-saved-prty-box">
                                            <div class="image">
                                                <img src="../../media/images/saved-property.jpg" alt="">
                                                <img src="../../media/images/favorite-heart-icon.svg" class="favourite" alt="">
                                            </div>
                                            <div class="description">
                                                <h3>The California Fresh</h3>
                                                <h6>Malibu, California</h6>
                                                <p class="price-per-night"><b>$589</b>/night</p>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <div class="live-life-travel text-center">
            <div class="container">
                <h2>Live Life Travel</h2>
                <p>Your exclusive details are waiting</p>
            </div>
            <div class="subscribe-box-outer">
                <div class="subscribe-box-inner">
                    <form>
                        <div class="input-box-cstm">
                            <span><img src="../../media/images/tripper-page/msg-img.png" /></span>
                            <input type="text" placeholder="Type Your Email" />
                        </div>
                        <div class="submit-btn-cstm">
                            <input type="submit" name="submit" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <footer>


        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../../media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../../media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <script src="../../plugins/global/plugins.bundle.js"></script>
    <script src="../../js/scripts.bundle.js"></script>
    <script src="../../js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../../js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

    <script>
        $(".potential-listing-slider.owl-carousel").owlCarousel({
            dots: false,
            nav: true,
            lazyLoad: true,
            loop: false,
            slideBy: 3,
            fade: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1700: {
                    items: 4
                },
                2100: {
                    items: 5
                }
            }
        });

        jQuery('#tripper-page-carousal').owlCarousel({
            loop: true,
            margin: 30,
            center: true,
            nav: true,
            dots: false,
            autoHeight: false,
            responsive: {
                0: {
                    items: 1
                },
                640: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
        jQuery(".tripper-banner .banner-comtent a").click(function() {
            jQuery('html,body').animate({
                    scrollTop: jQuery("#tripper-reg").offset().top
                },
                1000);
        });
    </script>

</body>

</html>