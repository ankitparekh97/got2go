<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../assets/css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../assets/plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../assets/css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <title>trippers Only</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
<style>
</style>
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>
                   
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="tripper-banner" style="background-image: url(../assets/media/images/tripper-page/tripper-banner.jpg);">
        <div class="banner-comtent">
            <img src="../assets/media/images/tripper-page/trippersonlywhite.png" />
            <a href="javascript:void(0)">Registration</a>
        </div>
    </div>

    <div class="page-content-start tripper-page registration-form no-padding">
        
        <div class="tripper-registration" id="tripper-reg">
            <div class="tripper-container">
                <div class="image-title">
                    <h1>TRIPPER <b>REGISTRATION</b></h1>
                    <span><img src="../../html/assets/media/images/tripper-page/flight-img-center.png" /></span>
                </div>
                <div class="image-below-desc">
                    <p class="text-left">Trippers Only®, the only rewards program with exclusive access to premium listings, first in<br>line offers on availability, and no point expiration.</p>
                    <p class="text-left">Already a Tripper? Log in <a href="#">here.</a></p>
                </div>
                <div class="tripper-registration-box">
                    <p class="required text-left">*Required</p>
                    <h3>My <b>Credentials</b></h3>
                    <form id="tripper-registration" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" name="firstname" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" name="lastname" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="password" name="createpassword" placeholder="Create Password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="password" name="conformpassword" placeholder="Re-type Password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group-custm no-icon">
                                    <input class="form-class-cstm" type="text" name="phonenumber" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="input-group input-group-solid date" id="kt_datetimepicker_3" data-target-input="nearest">
                                    <input type="text" class="form-control form-control-solid datetimepicker-input" placeholder="Birthday" data-target="#kt_datetimepicker_3"/>
                                    <div class="input-group-append" data-target="#kt_datetimepicker_3" data-toggle="datetimepicker">
                                        <span class="input-group-text">
                                            <i class="ki ki-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                    
                <form>
                    <div class="tripper-registration-box">
                        <p class="required text-left">*Required</p>
                        <h3>My <b>Billing</b></h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="cardnumber" placeholder="Card Number">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="month" placeholder="MM">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="year" placeholder="YYYY">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="seccode" placeholder="Sec. Code">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="password" name="nameoncard" placeholder="Name on card">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="password" name="billingstreetaddress" placeholder="Billing Street Address Line 1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="password" name="emailaddress" placeholder="Billing Street Address Line 2 (optional)">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="password" name="city" placeholder="City">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group-custm no-icon">
                                        <select class="form-class-cstm">
                                            <option>State</option>
                                            <option>State</option>
                                            <option>State</option>
                                            <option>State</option>
                                            <option>State</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="password" name="phonenumber" placeholder="Billing Phone Number">
                                    </div>
                                </div>
                            </div>
                            <h4>Your card will not be billed until you select “Create Account” at the bottom of the screen. </h4>
                        
                    </div>
                    <div class="text-center tripper-create-account">
                        <input type="submit" value="CREATE ACCOUNT">
                    </div>
                </form>
            </div>
        </div>

        
    </div>

        <div class="live-life-travel text-center">
            <div class="container">
                <h3>Live Life Travel</h3>
                <p>Your exclusive details are waiting</p>
            </div>
            <div class="subscribe-box-outer">
                <div class="subscribe-box-inner">
                    <form>
                        <div class="input-box-cstm">
                            <span><img src="../assets/media/images/tripper-page/msg-img.png" /></span>
                            <input type="text" placeholder="Type Your Email" />
                        </div>
                        <div class="submit-btn-cstm">
                            <input type="submit" name="submit" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

    
    <footer>
        

        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../assets/media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../assets/media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

   

    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

<script>
jQuery('#tripper-page-carousal').owlCarousel({
    loop:true,
    margin:30,
    center: true,
    nav:true,
    dots: false,
    autoHeight: false,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
jQuery(".tripper-banner .banner-comtent a").click(function() {
    jQuery('html,body').animate({
        scrollTop: jQuery("#tripper-reg").offset().top},
        1000);
});

jQuery('#kt_datetimepicker_3').datetimepicker({
    format: 'L'
});

 jQuery('#tripper-registration').validate({
             ignore: ".ignore",
             rules: {
                firstname: {
                    required: true,
                    maxlength: 30
                },
                lastname: {
                     required: true,
                     maxlength: 30
                },
                email: {
                    required: true,
                    email:true,
                    minlength:3
                },
                phonenumber: {
                     required: true,
                     // minlength: 10,
                     // maxlength: 11,
                     phoneUS: true
                 },
                 createpassword:{
                     required:true,
                     minlength: 6,
                     maxlength: 20
                 },
                 conformpassword:{
                     required:true,
                     equalTo: "#password",
                     minlength: 6,
                     maxlength: 20
                 },
                 cardnumber:{
                    required:true,
                     minlength: 10,
                     maxlength: 16
                },
                nameoncard:{
                    required:true,
                     minlength: 3
                },
                billingstreetaddress:{
                    required:true,
                     minlength: 3
                },
                city:{
                    required:true,
                     minlength: 3
                },
                city:{
                    required:true,
                     minlength: 3
                },
                 regRecaptcha: {
                     required: function () {
                         if (grecaptcha.getResponse() == '') {
                             return true;
                         } else {
                             return false;
                         }
                     }
                 }
            },
            messages: {
                firstname: {
                    required: "First Name is required"
                },
                lastname: {
                    required: "Last Name is required"
                },
                email: {
                    required: "Email is required",
                    email:"Please enter valid email address"
                },
                phonenumber: {
                    required: "Phone number is required",
                    // minlength: "Phone should be minimum 10 digits",
                    // maxlength: "Phone should be maximum 11 digits",
                    phoneUS: "Phone should be in US format.",
                },
                createpassword: {
                    required:"Password field is required",
                    minlength: "Password should be minimum 6 characters",
                    maxlength: "Password must be maximum 20 characters",
                },
                conformpassword: {
                    required:"Confirm Password field is required",
                    equalTo: "Password and Confirm Password field should be same",
                    //minlength: "Confirm Password should be minimum 8 characters",
                },
                terms: {
                    required: "Please agree to Terms & Conditions"
                },
            },
            errorPlacement: function(error,element){
                if(element.attr('id')== "terms"){
                    var parentDiv = $(element).parent();
                    error.insertAfter(parentDiv);
                } else {
                    error.insertAfter(element);
                }
            }
        });
</script> 
    
</body>

</html>