<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../../css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../../plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <link rel="stylesheet" href="../../css/custom.css">
    <link rel="stylesheet" href="../../css/rental/dashboard.css">
    <link rel="icon" href="../../media/images/Gtog.png" type="image/png">
    <title>GOT2GO TRAVEL</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">Tripboards</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../media/images/trippers-only.svg" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="modal fade" id="new-payment-method_popup" tabindex="-1" role="dialog" aria-labelledby="reg_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII=" />
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h3>Add a new payment method</h3>
                    <!-- <h1> Registeration <b>code</b> </h1> -->
                    <div class="align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                        <form class="dashboard-forms" id="my-account-setting-popup">
                            <div class="row m-0">
                                <div class="col-lg-6">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="credit" placeholder="Credit Card Number">
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group-custm no-icon">
                                        <label class="w-25 float-left pt-4">Expiration</label>
                                        <input class="form-class-cstm w-35 float-left" type="text" name="mm/yyyy" placeholder="MM/YYYY">
                                        <input class="form-class-cstm w-35 float-right" type="text" name="cvv" placeholder="CVV">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="lastname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-lg-6  ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="add1" placeholder="Address 01">
                                    </div>
                                </div>
                                <div class="col-lg-6  ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm" type="text" name="add2" placeholder="Address 02">
                                    </div>
                                </div>


                                <div class="col-lg-6 ">
                                    <div class="form-group-custm no-icon">
                                        <input class="form-class-cstm w-70 float-left" type="text" name="city" placeholder="City">
                                        <input class="form-class-cstm w-25 float-right" type="text" name="phonenumber" placeholder="ZIP Code">
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group-custm no-icon">
                                        <select class="form-class-cstm">
                                            <option>State</option>
                                            <option>State</option>
                                            <option>Country</option>
                                            <option>Country</option>
                                            <option>Country</option>
                                        </select>

                                    </div>
                                </div>

                            </div>

                            <div class="d-block mt-10">
                                <button type="submit" id="add_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5">ADD</button>
                            </div>
                            <div class="d-block mt-10">
                                <button class="button-custm-cancel" type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                            </div>
                        </form>



                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="my-account-dashboard">

        <!-- Sidebar -->
        <ul class="navbar-nav left-sidebar accordion pt-20" id="accordionSidebar">

            <!-- Divider -->
            <li class="left-sidebar-nav-item user">
                <a class="left-sidebar-nav-link" href="chat.php">
                    <span class="left-sidebar-user-img">
                        <img src="../../media/images/hello-lydia.png" alt="">
                    </span>
                    <div class="hello-lydia-down-arrow"></div>

                </a>
                Hello <span class="user-text">Lydia!</span>
            </li>

            <hr class="hello-lydia-separator">

            <!-- Nav Item - Dashboard -->
            <li class="left-sidebar-nav-item">
                <a class="left-sidebar-nav-link" href="#">
                    <span class="">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22.82" height="19.967" viewBox="0 0 22.82 19.967">
                            <defs>
                                <clipPath id="clip-path-chat-1">
                                    <path id="Path_2578" data-name="Path 2578" d="M4.39-68.033a6.948,6.948,0,0,0,1.683-.546,13.271,13.271,0,0,0,1.549-.847,12.515,12.515,0,0,0,1.237-.891,5.439,5.439,0,0,0,.7-.657,5.01,5.01,0,0,0,.657.078q.279.011.591.011h.6a14.468,14.468,0,0,0,4.446-.669,12.105,12.105,0,0,0,3.632-1.839,9.079,9.079,0,0,0,2.44-2.719,6.568,6.568,0,0,0,.891-3.332,6.568,6.568,0,0,0-.891-3.332,9.079,9.079,0,0,0-2.44-2.719,12.105,12.105,0,0,0-3.632-1.839A14.468,14.468,0,0,0,11.41-88a14.468,14.468,0,0,0-4.446.669,12.105,12.105,0,0,0-3.632,1.839,9.079,9.079,0,0,0-2.44,2.719A6.568,6.568,0,0,0,0-79.443a6.967,6.967,0,0,0,.323,2.128A7.6,7.6,0,0,0,1.248-75.4a8.019,8.019,0,0,0,1.493,1.66A10.426,10.426,0,0,0,4.769-72.4a.055.055,0,0,1,0,.025l-.006.009a.045.045,0,0,0-.011.033,4.737,4.737,0,0,1-.412,1.905q-.412.947-.724,1.5a.4.4,0,0,0-.033.111.812.812,0,0,0-.011.134.623.623,0,0,0,.189.457.623.623,0,0,0,.457.189Z" transform="translate(0 88)" fill="" />
                                </clipPath>
                                <clipPath id="clip-path-chat-11">
                                    <path id="Path_2577" data-name="Path 2577" d="M-186,929.282H197.374V-316H-186Z" transform="translate(186 316)" fill="" />
                                </clipPath>
                            </defs>
                            <g id="Group_678" data-name="Group 678" clip-path="url(#clip-path-chat-1)">
                                <g id="Group_677" data-name="Group 677" transform="translate(-42.445 -52.029)" clip-path="url(#clip-path-chat-11)">
                                    <path id="Path_2576" data-name="Path 2576" d="M-5-93H20.1v22.249H-5Z" transform="translate(46.304 143.888)" fill="" />
                                </g>
                            </g>
                        </svg>



                    </span> Chat
                </a>
            </li>
            <li class="left-sidebar-nav-item">
                <a class="left-sidebar-nav-link" href="my-dashboard-mystays.php">
                    <span class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26.876" height="26.871" viewBox="0 0 26.876 26.871">
                            <g id="my_stays_svg" transform="translate(-1777.335 -208.62)">
                                <g id="my_stays_svg" data-name="my_stays_svg" transform="translate(1777.335 208.62)">
                                    <path id="Path_2543" data-name="Path 2543" d="M1779.852,268.013c0,.29,0,.533,0,.775a1.68,1.68,0,1,0,3.359,0c0-.243,0-.486,0-.752H1793.3c0,.31-.018.617,0,.921a1.673,1.673,0,0,0,2.519,1.337,1.623,1.623,0,0,0,.828-1.422c.008-.271,0-.542,0-.808a1.9,1.9,0,0,1,2.506,1.737c.038,2.182.011,4.365.011,6.574h-1.662v-3.3h-18.491v.307q0,5.52,0,11.04c0,.383.008.393.4.393l9.318,0h.329v1.676h-.335l-9.4,0a1.853,1.853,0,0,1-1.986-1.973q-.006-7.271,0-14.541a1.845,1.845,0,0,1,1.961-1.966Z" transform="translate(-1777.335 -264.655)" fill="" />
                                    <path id="Path_2544" data-name="Path 2544" d="M2028.789,453.28a6.717,6.717,0,1,1-6.719-6.721A6.736,6.736,0,0,1,2028.789,453.28Zm-1.677-.02a5.038,5.038,0,1,0-5.094,5.055A5.048,5.048,0,0,0,2027.112,453.26Z" transform="translate(-2001.913 -433.121)" fill="" />
                                    <path id="Path_2545" data-name="Path 2545" d="M1840.539,390.777h-3.316V387.46h3.316Z" transform="translate(-1833.841 -377.36)" fill="" />
                                    <path id="Path_2546" data-name="Path 2546" d="M1929.924,390.737h-3.293v-3.318h3.293Z" transform="translate(-1918.199 -377.321)" fill="" />
                                    <path id="Path_2547" data-name="Path 2547" d="M2015.629,390.392v-3.338h.6q1.241,0,2.482,0c.143,0,.254,0,.253.2-.007,1.006,0,2.012-.007,3.018a.6.6,0,0,1-.027.119Z" transform="translate(-2002.171 -376.976)" fill="" />
                                    <path id="Path_2548" data-name="Path 2548" d="M1840.51,476.809v3.3h-3.321v-3.3Z" transform="translate(-1833.81 -461.663)" fill="" />
                                    <path id="Path_2549" data-name="Path 2549" d="M1926.613,476.667h3.3v3.3h-3.3Z" transform="translate(-1918.182 -461.529)" fill="" />
                                    <path id="Path_2550" data-name="Path 2550" d="M1838.51,211.155c0,.535,0,1.071,0,1.606a.832.832,0,1,1-1.661.012q-.011-1.634,0-3.268a.833.833,0,1,1,1.661.016c0,.545,0,1.089,0,1.634Z" transform="translate(-1833.483 -208.62)" fill="" />
                                    <path id="Path_2551" data-name="Path 2551" d="M2076.523,211.14c0,.535,0,1.071,0,1.606a.831.831,0,1,1-1.658.012q-.012-1.62,0-3.24a.836.836,0,1,1,1.666.016c0,.272,0,.545,0,.817S2076.523,210.877,2076.523,211.14Z" transform="translate(-2058.055 -208.62)" fill="" />
                                    <path id="Path_2552" data-name="Path 2552" d="M2070.225,535.489c.581-.586,1.135-1.147,1.692-1.707q.6-.6,1.2-1.2a.839.839,0,1,1,1.19,1.173q-1.7,1.709-3.413,3.411a.861.861,0,0,1-1.377-.01q-1.1-1.095-2.194-2.2a.843.843,0,1,1,1.194-1.173Z" transform="translate(-2050.643 -513.995)" fill="" />
                                </g>
                            </g>
                        </svg>
                    </span> My Stays
                </a>
            </li>

            <li class="left-sidebar-nav-item active">
                <a class="left-sidebar-nav-link" href="my-dashboard-setting.php">
                    <span class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="28.75" viewBox="0 0 30 28.75">
                            <g id="login_svg" data-name="login_svg" transform="translate(0)">
                                <path id="Path_304" data-name="Path 304" d="M329.063,261h-.313v-1.875a3.125,3.125,0,1,0-6.25,0V261h-.313A2.19,2.19,0,0,0,320,263.187v4.375a2.19,2.19,0,0,0,2.187,2.188h6.875a2.19,2.19,0,0,0,2.187-2.188v-4.375A2.19,2.19,0,0,0,329.063,261Zm-4.687-1.875a1.25,1.25,0,1,1,2.5,0V261h-2.5Zm0,0" transform="translate(-301.25 -241)" fill="" />
                                <path id="Path_305" data-name="Path 305" d="M16.875,263.187a4.069,4.069,0,0,1,2.5-3.75v-.313a4.941,4.941,0,0,1,.489-2.119,5.908,5.908,0,0,0-3.3-1.006H5.937A5.944,5.944,0,0,0,0,261.937v4.375a.938.938,0,0,0,.937.937H16.875Zm0,0" transform="translate(0 -241)" fill="" />
                                <path id="Path_306" data-name="Path 306" d="M97.832,6.25A6.25,6.25,0,1,1,91.582,0,6.25,6.25,0,0,1,97.832,6.25Zm0,0" transform="translate(-80.332)" fill="" />
                            </g>
                        </svg>

                    </span> Settings
                </a>
            </li>
            <li class="left-sidebar-nav-item">
                <a class="left-sidebar-nav-link" href="my-dashboard-notification.php">
                    <span class="">
                        <svg id="alarm-settings_svg" data-name="alarm-settings_svg" xmlns="http://www.w3.org/2000/svg" width="26.209" height="30.088" viewBox="0 0 26.209 30.088">
                            <path id="Shape" d="M33.287,24.648l.3,1.8H35.9l.3-1.8a5.7,5.7,0,0,1-2.912,0Z" transform="translate(-15.63 -11.406)" fill="" />
                            <path id="Shape-2" data-name="Shape" d="M22.793,18.139l1.161,2L25.66,19.5a5.9,5.9,0,0,1-1.454-2.517Z" transform="translate(-10.774 -7.859)" fill="" />
                            <path id="Shape-3" data-name="Shape" d="M31.835,14.67A4.835,4.835,0,1,0,27,9.835,4.835,4.835,0,0,0,31.835,14.67Zm0-7.521a2.686,2.686,0,1,1-2.686,2.686A2.686,2.686,0,0,1,31.835,7.149Z" transform="translate(-12.721 -2.314)" fill="" />
                            <circle id="Oval" cx="1.612" cy="1.612" r="1.612" transform="translate(17.502 5.909)" fill="" />
                            <path id="Shape-4" data-name="Shape" d="M46.738,7.143l-1.161-2-1.705.639A5.9,5.9,0,0,1,45.325,8.3Z" transform="translate(-20.528 -2.38)" fill="" />
                            <path id="Shape-5" data-name="Shape" d="M23.544,5.841a.472.472,0,0,1-.034.06L22.8,7.137l1.411,1.157a5.9,5.9,0,0,1,1.455-2.516l-1.709-.642Z" transform="translate(-10.775 -2.377)" fill="" />
                            <path id="Shape-6" data-name="Shape" d="M36.2,1.8,35.9,0h-2.31l-.3,1.8a5.7,5.7,0,0,1,2.912,0Z" transform="translate(-15.634 0)" fill="" />
                            <path id="Shape-7" data-name="Shape" d="M19.223,2.686c.128,0,.254.009.381.015l.277-.478a1.071,1.071,0,0,1,1.3-.469l1.057.395A3.184,3.184,0,0,0,19.223,0,3.227,3.227,0,0,0,16,3.223V3.25A9.605,9.605,0,0,1,19.223,2.686Z" transform="translate(-7.63 0)" fill="" />
                            <path id="Shape-8" data-name="Shape" d="M43.87,19.5l1.709.645,1.157-2-1.411-1.157A5.9,5.9,0,0,1,43.87,19.5Z" transform="translate(-20.528 -7.862)" fill="" />
                            <path id="Shape-9" data-name="Shape" d="M15.071,50a3.761,3.761,0,0,0,7.445,0Z" transform="translate(-7.201 -23.139)" fill="" />
                            <path id="Shape-10" data-name="Shape" d="M20.609,21.516V19.367H18.38a1.074,1.074,0,0,1-1.062-.9l-.4-2.4a5.977,5.977,0,0,1-.663-.376l-2.279.856a1.046,1.046,0,0,1-.372.068,1.074,1.074,0,0,1-.935-.545l-1.156-1.991a1.088,1.088,0,0,1,.243-1.369l1.89-1.547c-.008-.13-.019-.257-.019-.386s.011-.256.019-.384L11.76,8.845a1.087,1.087,0,0,1-.252-1.366l.265-.456a8.6,8.6,0,0,0-8.354,8.583v5.909A10.569,10.569,0,0,1,.571,28.124a.553.553,0,0,0-.1.6.51.51,0,0,0,.484.311H23.075a.51.51,0,0,0,.484-.311.554.554,0,0,0-.1-.607,10.566,10.566,0,0,1-2.847-6.606ZM5.03,27.962H2.881a.537.537,0,0,1,0-1.074H5.03a.537.537,0,0,1,0,1.074Zm0-11.819a.537.537,0,0,1-.537-.537,7.489,7.489,0,0,1,2.824-5.87.537.537,0,1,1,.672.838,6.422,6.422,0,0,0-2.422,5.032.537.537,0,0,1-.537.537ZM21.146,27.962H7.179a.537.537,0,0,1,0-1.074H21.146a.537.537,0,1,1,0,1.074Z" transform="translate(-0.421 -3.25)" fill="" />
                        </svg>

                    </span> Notifications
                </a>
            </li>
            <li class="left-sidebar-nav-item">
                <a class="left-sidebar-nav-link" href="#">
                    <span class="">


                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21.86" height="20.848" viewBox="0 0 21.86 20.848">
                            <defs>
                                <clipPath id="clip-path-star-1">
                                    <path id="Path_2585" data-name="Path 2585" d="M17.8,6.42c.381,0,.539-.315.539-.657a1.761,1.761,0,0,0-.013-.263L17.2-1.068l4.769-4.651a.978.978,0,0,0,.342-.631c0-.394-.42-.552-.736-.6l-6.595-.959L12.02-13.89c-.118-.25-.342-.539-.644-.539s-.525.289-.644.539L7.777-7.913l-6.595.959c-.328.053-.736.21-.736.6a1.022,1.022,0,0,0,.328.631L5.557-1.068,4.427,5.5a1.78,1.78,0,0,0-.026.263c0,.342.171.657.552.657a1.109,1.109,0,0,0,.525-.158l5.9-3.1,5.9,3.1A1.067,1.067,0,0,0,17.8,6.42Z" transform="translate(-0.446 14.429)" fill="" />
                                </clipPath>
                                <clipPath id="clip-path-star-11">
                                    <path id="Path_2584" data-name="Path 2584" d="M-34,103.173H138.147V-119H-34Z" transform="translate(34 119)" fill="" />
                                </clipPath>
                            </defs>
                            <g id="Group_701" data-name="Group 701" transform="translate(-0.446 14.429)">
                                <g id="Group_700" data-name="Group 700" transform="translate(0.446 -14.429)" clip-path="url(#clip-path-star-1)">
                                    <g id="Group_699" data-name="Group 699" transform="translate(-50.683 -153.861)" clip-path="url(#clip-path-star-11)">
                                        <path id="Path_2583" data-name="Path 2583" d="M-4.554-19.429H32.02V16.133H-4.554Z" transform="translate(47.879 165.932)" fill="" />
                                    </g>
                                </g>
                            </g>
                        </svg>




                    </span> Preferred Cities
                </a>
            </li>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column pt-20">
        <button class="togl-btn-custom" data-toggle="collapse" data-target="#accordionSidebar" aria-expanded="true">
               <i class="fas fa-bars"></i>
            </button>
            <!-- Main Content -->
            <div id="content">

                <div class="container-fluid my-acc-dashbrd">

                    <!-- Page Heading -->
                    <h2 class="dashboard-title">MY <b>Account</b></h2>
                    <p class="mb-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    </p>
                    <p class="mb-6">View our <u><a href="">Privacy Policy</a></u> for more information..</p>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <!-- <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                    </div> -->
                        <div class="card-body">
                            <h3><b>My Credentials</b></h3>
                            <form class="dashboard-forms" id="my-account-setting-form">
                                <div class="row m-0">
                                    <div class="col-lg-6 pl-0">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" name="firname" placeholder="First Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 pl-0">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" name="lasname" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 pl-0 ">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="password" name="updatepassword" placeholder="Update Password">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 pl-0 "></div>
                                    <div class="col-lg-6 pl-0 ">

                                        <div class="input-group">
                                            <input class="form-class-cstm" type="password" name="retypepassword" placeholder="Retype Password">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-refresh"></i>
                                                </span>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-lg-6 pl-0 "></div>
                                    <div class="col-lg-6 pl-0 ">
                                        <div class="form-group-custm no-icon">
                                            <input class="form-class-cstm" type="text" name="phonenumber" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 pl-0 ">
                                        <div class="input-group input-group-solid date" id="kt_datepicker_2" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-solid" placeholder="Birthday" data-target="#kt_datepicker_2">
                                            <div class="input-group-append" data-target="#kt_datepicker_2" data-toggle="datepicker">
                                                <span class="input-group-text">
                                                    <i class="ki ki-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="dashbrd-social-login">
                                        <span><a href="#"><img src="../../media/images/tripper-page/google.png">Connected</a></span>
                                        <span class="not-connected"><a href="#"><img src="../../media/images/tripper-page/favebook-login-btn.png">Connect Account</a> </span>
                                    </div>
                                </div>
                                <button type="submit" id="save_btn" class="button-default-animate button-default-custom btn-sunset-gradient mt-5">Save</button>
                                <!-- <a href="#" class="button-default-animate button-default-custom btn-sunset-gradient mt-5">
                                    Save
                                </a> -->
                                <h3 class="pt-20"><b>My Billing</b></h3>
                                <div class="row">
                                    <div class="col-xl-9">
                                        <div class="row bank-card-detail">
                                            <div class="col-10 pl-1">
                                                <div class="bank-card-type-img">
                                                    <img src="../../media/images/mastercard-img.svg" alt="">
                                                </div>
                                                <p><b>Mastercard <b class="pl-2">******5139</b></b></p>
                                                <p>Exp. 9/2021</p>
                                            </div>
                                            <div class="col-2 float-right pr-0">
                                                <a class="del-circle-icon" href="">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="mt-10 button-default-animate btn-add-payment-method" href="" data-toggle="modal" data-target="#new-payment-method_popup">Add payment method</a>
                            </form>
                        </div>
                    </div>

                </div>

            </div>


        </div>
        <!-- End of Content Wrapper -->

    </div>


    <footer class="my-dashboard-footer">


        <div class="container">
            <div class="row">

                <div class="col-md-9">

                    <div class="row">
                        <div class="col-md-3">
                            <h5 class="footer-list-title">site map</h5>
                            <ul class="footer-list">
                                <li class="active"><a href="#">View Stays</a></li>
                                <li><a href="#">List your stays</a></li>
                                <li><a href="#">Tripboard</a></li>

                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="footer-list-title">site map</h5>
                            <ul class="footer-list">
                                <li class="active"><a href="#">View Stays</a></li>
                                <li><a href="#">List your stays</a></li>
                                <li><a href="#">Tripboard</a></li>

                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="footer-list-title">site map</h5>
                            <ul class="footer-list">
                                <li class="active"><a href="#">View Stays</a></li>
                                <li><a href="#">List your stays</a></li>
                                <li><a href="#">Tripboard</a></li>

                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="footer-list-title">site map</h5>
                            <ul class="footer-list">
                                <li class="active"><a href="#">View Stays</a></li>
                                <li><a href="#">List your stays</a></li>
                                <li><a href="#">Tripboard</a></li>

                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-logo text-right mt-10">
                    <a href=""> <img src="../../media/images/footer-logo.svg" alt="logo" /></a>   
                    </div>
                </div>

            </div>
        </div>

        <diV class="global-footer-bottom-strip">
            <div class="container">
                <ul class="footer-bottom-strip-list d-inline-block">
                    <li><a><b>© 2020 GOT2GO.</b></a></li>
                    <li><a href=""> All rights reserved</a></li>
                    <li>|</li>
                    <li><a href="">Privacy </a></li>
                    <li>|</li>
                    <li><a href="">Terms and Conditions</a></li>
                    <li>|</li>
                    <li><a href="">About</a></li>
                </ul>
                <ul class="footer-bottom-strip-list social-media-list-icons d-inline-block float-right">
                    <li><a href="#">
                            <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.1519 11.6152V19H1.18671C0.865308 19 0.587174 18.8825 0.352304 18.6475C0.117435 18.4124 0 18.1341 0 17.8125V1.1875C0 0.865885 0.117435 0.587565 0.352304 0.352539C0.587174 0.117513 0.865308 0 1.18671 0H17.8006C18.122 0 18.4002 0.117513 18.635 0.352539C18.8699 0.587565 18.9873 0.865885 18.9873 1.1875V17.8125C18.9873 18.1341 18.8699 18.4124 18.635 18.6475C18.4002 18.8825 18.122 19 17.8006 19H13.0538V11.6152H15.4272L15.8259 8.87842H13.0538V6.72607C13.0538 6.33024 13.1542 6.02718 13.3551 5.81689C13.556 5.60661 13.8511 5.50146 14.2405 5.50146H16.0206V3.03369C15.489 2.9471 14.84 2.90381 14.0736 2.90381C12.8004 2.90381 11.8285 3.22697 11.1578 3.87329C10.4872 4.51961 10.1519 5.39941 10.1519 6.5127V8.87842H7.77851V11.6152H10.1519Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="#">
                            <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.74418 15.5601C8.70272 15.8693 7.60409 16.0239 6.44828 16.0239C4.10577 16.0239 2.11247 15.4951 0.468384 14.4375C0.752699 14.4808 1.06174 14.5024 1.3955 14.5024C2.26081 14.5024 3.10757 14.3354 3.9358 14.0015C4.76402 13.6675 5.52735 13.2191 6.22578 12.6562C5.39137 12.6501 4.63887 12.3918 3.96825 11.8816C3.29764 11.3713 2.8418 10.7266 2.60075 9.94727C2.86035 9.99674 3.1014 10.0215 3.3239 10.0215C3.68239 10.0215 4.02851 9.9751 4.36227 9.88232C3.46606 9.70296 2.72128 9.2561 2.12792 8.54175C1.53457 7.82739 1.23789 7.00016 1.23789 6.06006V6.01367C1.79416 6.31673 2.38133 6.47754 2.99941 6.49609C2.46168 6.13737 2.03676 5.6735 1.72463 5.10449C1.4125 4.53548 1.25644 3.92318 1.25644 3.26758C1.25644 2.63053 1.43258 1.99967 1.78489 1.375C2.76145 2.5625 3.94198 3.50415 5.32647 4.19995C6.71097 4.89575 8.20363 5.28695 9.80445 5.37354C9.74882 5.13232 9.721 4.83236 9.721 4.47363C9.721 3.39746 10.0996 2.48055 10.8567 1.7229C11.6139 0.965251 12.5302 0.586426 13.6056 0.586426C14.712 0.586426 15.6669 0.994629 16.4704 1.81104C17.3543 1.63167 18.1732 1.31624 18.9273 0.864746C18.6306 1.80485 18.0589 2.52539 17.2121 3.02637C17.9847 2.93978 18.7326 2.73258 19.4557 2.40479C18.9365 3.19027 18.2907 3.86442 17.5181 4.42725V4.9375C17.5181 5.80339 17.4083 6.67391 17.1889 7.54907C16.9695 8.42423 16.6543 9.27466 16.2433 10.1003C15.8322 10.926 15.3053 11.6976 14.6625 12.415C14.0197 13.1325 13.2997 13.7587 12.5024 14.2937C11.705 14.8287 10.7856 15.2508 9.74418 15.5601Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="#">
                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.4052 19C17.1716 19 17.8252 18.7294 18.3661 18.1882C18.9069 17.6471 19.1773 16.993 19.1773 16.2261V2.77393C19.1773 2.007 18.9069 1.35295 18.3661 0.811768C17.8252 0.27059 17.1716 0 16.4052 0H2.96202C2.1956 0 1.54199 0.27059 1.00117 0.811768C0.46035 1.35295 0.189941 2.007 0.189941 2.77393V16.2261C0.189941 16.993 0.46035 17.6471 1.00117 18.1882C1.54199 18.7294 2.1956 19 2.96202 19H16.4052ZM2.56336 17.0239H16.8039C16.9151 17.0239 17.0094 16.9853 17.0866 16.908C17.1639 16.8306 17.2025 16.7363 17.2025 16.625V8.3125H15.098C15.1783 8.67741 15.2185 9.07324 15.2185 9.5C15.2185 10.502 14.9713 11.4281 14.4768 12.2786C13.9823 13.129 13.3102 13.8016 12.4603 14.2964C11.6105 14.7912 10.6849 15.0386 9.68362 15.0386C8.68233 15.0386 7.75676 14.7912 6.9069 14.2964C6.05704 13.8016 5.38488 13.129 4.89042 12.2786C4.39596 11.4281 4.14873 10.502 4.14873 9.5C4.14873 9.07324 4.18891 8.67741 4.26926 8.3125H2.1647V16.625C2.1647 16.7363 2.20333 16.8306 2.28059 16.908C2.35785 16.9853 2.45211 17.0239 2.56336 17.0239ZM9.68362 13.0625C8.70087 13.0625 7.86183 12.7146 7.1665 12.0188C6.47116 11.323 6.12349 10.4834 6.12349 9.5C6.12349 8.5166 6.47116 7.677 7.1665 6.9812C7.86183 6.2854 8.70087 5.9375 9.68362 5.9375C10.6664 5.9375 11.5039 6.28695 12.1961 6.98584C12.8945 7.67855 13.2437 8.5166 13.2437 9.5C13.2437 10.4834 12.8961 11.323 12.2007 12.0188C11.5054 12.7146 10.6664 13.0625 9.68362 13.0625ZM14.4305 5.14893H16.8039C16.9151 5.14893 17.0094 5.11027 17.0866 5.03296C17.1639 4.95565 17.2025 4.86133 17.2025 4.75V2.375C17.2025 2.26367 17.1639 2.16935 17.0866 2.09204C17.0094 2.01473 16.9151 1.97607 16.8039 1.97607H14.4305C14.3192 1.97607 14.2249 2.01473 14.1477 2.09204C14.0704 2.16935 14.0318 2.26367 14.0318 2.375V4.75C14.0318 4.86133 14.0704 4.95565 14.1477 5.03296C14.2249 5.11027 14.3192 5.14893 14.4305 5.14893Z" fill="" />
                            </svg>

                        </a></li>
                    <li><a href="#">
                            <svg width="15" height="19" viewBox="0 0 15 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.3118 18.9954C3.37052 19.0108 3.4246 18.9907 3.47404 18.9351C3.5853 18.8052 3.73981 18.6042 3.93759 18.332C4.13538 18.0599 4.39961 17.627 4.73029 17.0332C5.06096 16.4395 5.29428 15.8859 5.43025 15.3726C5.52914 15.0509 5.78566 14.0552 6.19977 12.3853C6.39755 12.7563 6.74831 13.0718 7.25204 13.3315C7.75578 13.5913 8.29814 13.7212 8.87914 13.7212C10.0226 13.7212 11.0424 13.3965 11.9386 12.7471C12.8348 12.0977 13.5271 11.2055 14.0154 10.0706C14.5036 8.93563 14.7478 7.65999 14.7478 6.24365C14.7478 5.43962 14.5809 4.66032 14.2471 3.90576C13.9134 3.1512 13.4529 2.48478 12.8657 1.90649C12.2785 1.32821 11.5477 0.865885 10.6731 0.519531C9.79852 0.173177 8.85441 0 7.84076 0C6.83947 0 5.90772 0.137614 5.0455 0.412842C4.18328 0.68807 3.45241 1.05298 2.85287 1.50757C2.25334 1.96216 1.74033 2.48633 1.31386 3.08008C0.887385 3.67383 0.572159 4.28768 0.368195 4.92163C0.164229 5.55558 0.0622559 6.18799 0.0622559 6.81885C0.0622559 7.79606 0.250765 8.65885 0.627792 9.40723C1.00482 10.1556 1.55801 10.6844 2.28734 10.9937C2.42332 11.0431 2.54074 11.0431 2.63963 10.9937C2.73852 10.9442 2.80343 10.8483 2.83434 10.7061C2.96413 10.2236 3.0383 9.93294 3.05684 9.83398C3.08774 9.69792 3.09392 9.59587 3.07538 9.52783C3.05684 9.4598 2.99812 9.36393 2.89923 9.24023C2.41713 8.65267 2.17607 7.93213 2.17607 7.07861C2.17607 6.39209 2.30279 5.73494 2.5562 5.10718C2.80961 4.47941 3.16345 3.92432 3.61774 3.44189C4.07202 2.95947 4.64221 2.57601 5.32828 2.2915C6.01434 2.007 6.75912 1.86475 7.56262 1.86475C8.9842 1.86475 10.0937 2.25285 10.891 3.02905C11.6883 3.80526 12.0869 4.81185 12.0869 6.04883C12.0869 7.10026 11.9448 8.0651 11.6605 8.94336C11.3762 9.82162 10.9698 10.5267 10.4413 11.0586C9.91286 11.5905 9.31178 11.8564 8.63808 11.8564C8.05708 11.8564 7.58888 11.6477 7.23349 11.2302C6.8781 10.8127 6.7653 10.3164 6.8951 9.74121C6.97545 9.41341 7.10369 8.96965 7.27985 8.40991C7.456 7.85018 7.59817 7.36003 7.70633 6.93945C7.81449 6.51888 7.86858 6.16325 7.86858 5.87256C7.86858 5.39632 7.74186 5.00513 7.48845 4.69897C7.23504 4.39282 6.87037 4.23975 6.39445 4.23975C5.80728 4.23975 5.31282 4.50724 4.91107 5.04224C4.50932 5.57723 4.30844 6.24984 4.30844 7.06006C4.30844 7.74658 4.41661 8.32487 4.63293 8.79492L3.25153 14.7324C3.18354 15.0107 3.1341 15.3138 3.1032 15.6416C3.07229 15.9694 3.05529 16.2694 3.0522 16.5415C3.04911 16.8136 3.05375 17.0966 3.06612 17.3904C3.07848 17.6842 3.09393 17.9223 3.11247 18.1047C3.13101 18.2872 3.1511 18.4619 3.17273 18.6289C3.19436 18.7959 3.20517 18.8825 3.20517 18.8887C3.21753 18.9443 3.25308 18.9799 3.3118 18.9954Z" fill="" />
                            </svg>

                        </a></li>
                </ul>
            </div>
        </diV>
    </footer>


    <script src="../../plugins/global/plugins.bundle.js"></script>
    <script src="../../js/scripts.bundle.js"></script>
    <script src="../../js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../../js/pages/crud/forms/widgets/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script src="../../js/jquery.validate.min.js"></script>
    <script>
    
    

        $('#save_btn').on('click', function() {
            var form = jQuery("#my-account-setting-form");
            form.validate({
                rules: {
                    firname: {
                        required: true
                    },
                    updatepassword: {
                        required: true
                    }
                },
                messages: {
                    firname: {
                        required: "Your First Name is Required"
                    },
                    updatepassword: {
                        required: "New Password is Required"
                    }


                }
            });
        });
        $('#add_btn').on('click', function() {
            var form = jQuery("#my-account-setting-popup");
            form.validate({
                rules: {
                    credit: {
                        required: true
                    },
                    firstname: {
                        required: true
                    },
                    add1: {
                        required: true
                    },
                    city: {
                        required: true
                    }
                },
                messages: {
                    credit: {
                        required: "Credit Card No. is Required",
                    },
                    firstname: {
                        required: "First Name is Required"
                    },
                    add1: {
                        required: "Address is Required"
                    },
                    city: {
                        required: "City is Required"
                    }

                }
            });
        });
        $('.togl-btn-custom').click(function() {
            $(this).toggleClass("pushed");
        });
    </script>

</body>

</html>