<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../../css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../../plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <title>trippers Only</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>
                   
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="modal fade" id="add_tripboard_popup" tabindex="-1" role="dialog" aria-labelledby="add_tripboard_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-body text-center">
                    <h1 class="title-new-tripboard">NEW <span class="light">TRIPBOARD</span></h1>
                    <div class="add-tripboard-container align-items-center w-100">

                        <div class="vertical-middle">
                            <p class="pt-5">Name your board</p>
                            <input type="text" class="form-control input-board mt-10" name="url" placeholder="Say something like “Conference Weekend” or “Friend’s Bachelorette”">
                            <a class="btn-tripboard-fly mt-15 button-default-animate" href="#">ADD BOARD <svg class="ml-10" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="57.558" height="24.346" viewBox="0 0 57.558 24.346">
                                <defs>
                                  <linearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                                    <stop offset="0" stop-color="#5495d1"/>
                                    <stop offset="0.5" stop-color="#ddddb6"/>
                                    <stop offset="0.632" stop-color="#eeda8c"/>
                                    <stop offset="0.808" stop-color="#e6c469"/>
                                    <stop offset="1" stop-color="#f6ab34"/>
                                  </linearGradient>
                                </defs>
                                <path id="Path_1" data-name="Path 1" d="M147.455,1310.953c2.63,0,4.986.032,7.341-.009,2.154-.037,4.316-.053,6.457-.262,2.372-.231,4.721-.691,7.086-1.01a46.881,46.881,0,0,0,6.792-1.507c1.133-.329,2.264-.665,3.384-1.034.767-.252.834-.573.248-1.15a12.428,12.428,0,0,0-1.127-.916q-2.925-2.266-5.857-4.523a4.324,4.324,0,0,0-.792-.454,1.1,1.1,0,0,1-.626-1.6c.34-.132.665-.243.978-.381a5.2,5.2,0,0,1,3.441.066c1.257.3,2.5.654,3.751.993,2.019.547,4.033,1.112,6.055,1.65,1.11.3,2.234.544,3.347.832.7.181,1.383.425,2.086.594,1.3.313,2.365-.491,3.541-.77.966-.23,1.928-.5,2.871-.808a36.984,36.984,0,0,0,3.717-1.361,4.383,4.383,0,0,1,4.636,1.067,2.644,2.644,0,0,1-.294,2.653,7.564,7.564,0,0,1-2.018,1.276c-1.539.728-3.107,1.4-4.681,2.05-1.107.46-2.247.839-3.362,1.28a1.7,1.7,0,0,0-.706.475c-2.015,2.6-4.011,5.212-6.012,7.822q-2.077,2.709-4.152,5.419a3.6,3.6,0,0,1-2.7.771c-.569-.239-.419-.711-.326-1.144.225-1.05.44-2.1.7-3.144.3-1.221.664-2.428.973-3.647.187-.735.355-1.477.475-2.225.115-.711-.446-.522-.787-.471-.751.112-1.492.293-2.238.441-1.243.247-2.479.542-3.732.72-1.463.209-2.939.321-4.41.462-1.528.147-3.056.3-4.587.409a19.918,19.918,0,0,1-2.192.018c-1.67-.062-3.342-.112-5.007-.246-2.236-.178-4.479-.345-6.7-.676-1.739-.26-3.443-.756-5.157-1.167C147.76,1311.42,147.681,1311.225,147.455,1310.953Z" transform="translate(-147.455 -1297.832)" fill="url(#linear-gradient)"/>
                              </svg>
                              </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="tripboards">
        <div class="container">
            <div class="text-center">
                <img src="../../media/images/Tripboards.svg" alt="">
            </div>
            <div class="text-center d-block mt-8">
                <p class="tripboard-under-text">make it happen. this is the year. </p>
                <a class="button-default-animate float-right btn-add-new-board" href="#" data-toggle="modal" data-target="#add_tripboard_popup"><span class="pr-2 plus">+</span> Add new board</a>
            </div>
        </div>
        <div class="container mt-30">
            <div class="row">
                <div class="col-lg-4 position-relative mb-20">
                    <img src="../../media/images/favorite-heart-icon.svg" class="favorite-heart-icon" alt="">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#" class="board-pencil-icon-grey"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="button-default-animate btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 position-relative mb-20">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 position-relative mb-20">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 position-relative mb-20">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 position-relative mb-20">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 position-relative mb-20">
                    <div class="pl-2 pr-2">
                        <div class="boards-white">
                            <h3 class="boards-title">Saved favorites <span class="float-right"><a href="#"><img src="../../media/images/board-pencil-icon.svg"></a></span></h3>
                            <div class="border-separator"></div>
                            <div class="row m-0">
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-1.png" class="boards-thumbnail-img" alt="">
                                </div>
                                <div class="col-4 boards-thumbnail">
                                    <img src="../../media/images/recent-view-2.png" class="boards-thumbnail-img" alt=""></div>
                                <div class="col-4 boards-thumbnail"><img src="../../media/images/recent-view-3.png" class="boards-thumbnail-img" alt=""></div>
                            </div>
                            <div class="d-block text-center mt-10 mb-3">
                                <a class="btn-explore-more-tripboards">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="live-life-travel text-center" style="padding: 50px 0 0 0;">
            <div class="container">
                <h2>Live Life Travel</h2>
                <p>Your exclusive details are waiting</p>
            </div>
            <div class="subscribe-box-outer" style="bottom: -85px">
                <div class="subscribe-box-inner">
                    <form>
                        <div class="input-box-cstm">
                            <span><img src="../../media/images/tripper-page/msg-img.png" /></span>
                            <input type="text" placeholder="Type Your Email" />
                        </div>
                        <div class="submit-btn-cstm">
                            <input type="submit" name="submit" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <footer>


        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../../media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../../media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
   

    <script src="../../plugins/global/plugins.bundle.js"></script>
    <script src="../../js/scripts.bundle.js"></script>
    <script src="../../js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../../js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

<script>
jQuery('#tripper-page-carousal').owlCarousel({
    loop:true,
    margin:30,
    center: true,
    nav:true,
    dots: false,
    autoHeight: false,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
jQuery(".tripper-banner .banner-comtent a").click(function() {
    jQuery('html,body').animate({
        scrollTop: jQuery("#tripper-reg").offset().top},
        1000);
});
</script> 
    
</body>

</html>