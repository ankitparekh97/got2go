<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../../css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../../plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <link rel="stylesheet" href="../../css/rental/rental2.css">
    <link rel="stylesheet" href="../../css/rental/dashboard.css">
    <title>trippers Only</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="tripboard-view">
        <div class="container-fluid p-0">
            <form class="form">
                <div class="row m-0">
                    <div class="col-xl-3 tripboard-edit">
                        <div class="w-100">
                            <p>
                                <button class="previous-icon mr-5" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g id="Arrow_Icon_Prev" data-name="Arrow Icon prev" transform="translate(-147 -1381)">
                                            <rect id="Shape" width="24" height="24" transform="translate(147 1381)" fill="rgba(0,0,0,0)"></rect>
                                            <path id="Path_94" data-name="Path 94" d="M157.384,1393.03l4.853,5.295a1,1,0,0,1-1.474,1.351l-5.5-6a1,1,0,0,1,.03-1.383l6-6a1,1,0,0,1,1.414,1.414Z" fill="#939FA4"></path>
                                        </g>
                                    </svg>
                                </button><img class="single-tripboard" src="../../media/images/single-tripboard.svg"></p>
                            <div class="input-group mt-12">
                                <input type="text" class="input-boards-title" placeholder="Mama's 70th Birthday">
                                <button type="button" class="ml-auto pencil-edit board-pencil-icon-grey mr-3"><img src="../../media/images/board-pencil-icon-grey.svg"></button>
                                <button type="button" data-toggle="modal" data-target="#sharelink" class="sharelink"><img src="../../media/images/share-icon.svg"></button>
                            </div>
                            <div class="border-separator-s mb-15" style="margin-top: -15px;"></div>
                            <!-- <h3 class="boards-title mt-12">Mama's 70<sup>th</sup> Birthday <span class="float-right"><button type="button"><img src="../../media/images/board-pencil-icon-grey.svg"></button></span></h3> -->


                            <div class="mb-2 row">
                                <div class="col-6 pr-1">
                                    <label class="col-form-label">Proposed Dates</label>
                                </div>
                                <div class="col-6 pl-1 text-right float-right">
                                    <span class="switch d-inline-block">
                                        <label>
                                            <input type="checkbox" checked="checked" name="select" />
                                            <span></span>
                                        </label>
                                    </span>
                                    <label class="col-form-label text-right d-inline-block">Fixed Date</label>
                                </div>

                            </div>
                            <div class="row bg-white gadget-tripboard">
                                <div class="col-4 pr-0 gadget-first-col">
                                    <div class="text-center">
                                        <label class="gadget-lbl">CHECK-IN</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text p-0 pr-1">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="from_date_label" readonly placeholder="07/17/2020" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 pl-0 gadget-last-col">
                                    <div class="text-center">
                                        <label class="gadget-lbl">CHECK-OUT</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text p-0 pr-1">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="to_date_label" readonly placeholder="07/17/2020" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 guest-middle-col">
                                    <div class="text-center">
                                        <label class="gadget-lbl">GUESTS</label>
                                        <div class="plus-minus-sec pt-0">
                                            <span class="guest-minus minus transition">-</span>
                                            <div class="no_of_guest">4</div>
                                            <span class="guest-plus plus transition">+</span>
                                        </div>

                                    </div>
                                </div>
                                <!-- <div class="cal-down-arrow-cir">
                                <button type="button" class="previous-icon" id="datepicker" value="11/13/2020 - 12/22/2020">
                                    <img src="../../media/images/Down-arrow-round.png" alt="">
                                </button>
                            </div> -->

                            </div>
                            <p class="pt-15 pb-2 who-is-going">
                                Who's Going
                            </p>

                            <div class="d-block form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control edit-input" placeholder="Mara Jenkins">
                                    <button type="button" class="edit-input-pencil"><img src="../../media/images/board-pencil-icon-darkgrey.svg" alt=""></button>
                                    <button type="button" class="delete-input-icon"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control edit-input" placeholder="Mara Jenkins">
                                    <button type="button" class="edit-input-pencil"><img src="../../media/images/board-pencil-icon-darkgrey.svg" alt=""></button>
                                    <button type="button" class="delete-input-icon"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control edit-input" placeholder="Mara Jenkins">
                                    <button type="button" class="edit-input-pencil"><img src="../../media/images/board-pencil-icon-darkgrey.svg" alt=""></button>
                                    <button type="button" class="delete-input-icon"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control edit-input" placeholder="Mara Jenkins">
                                    <button type="button" class="edit-input-pencil"><img src="../../media/images/board-pencil-icon-darkgrey.svg" alt=""></button>
                                    <button type="button" class="delete-input-icon"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div class="d-block">
                                <button type="button" class="button-default-animate btn-add-new-travelbuddy"><span class="pr-2 plus">+</span> Add Travel buddy</button>
                            </div>

                        </div>

                    </div>
                    <div class="col-xl-9 tripboard-edit-right-section">
                        <div class="d-block w-100 mb-7">
                            <div class="row m-0">
                                <div class="col-lg-7 pl-0 pr-0 where-to-go">
                                    <div class="row m-0">
                                        <div class="col-lg-8 col-md-9 p-5 pt-9 pr-2">
                                            <div class="input-group pl-3">
                                                <img src="https://got2go.kitelytechdev.com/media/images/location.svg" alt="">
                                                <input type="text" class="form-control pac-target-input" autocomplete="off" name="location_detail" id="location_detail" placeholder="Got2Go to">
                                            </div>

                                        </div>

                                        <div class="col-lg-4 col-md-3 text-right pl-0 pt-5 pr-5 pb-5">
                                            <a href="" class="pr-4 pt-3 refine_filter_icon" data-toggle="modal" data-target="#filter_popup"><img src="https://got2go.kitelytechdev.com/media/images/filter.svg" alt=""></a>
                                            <button type="button"><img src="https://got2go.kitelytechdev.com/media/images/gobutton.svg" alt="" style="height: 73px;"></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 pl-0 pr-0 checkboxes-inline">
                                    <div class="row m-0 w-100">
                                        <div class="col-4 pl-0 pr-2">
                                            <label class="custom-checkbox common-fatfrank">Vacation club
                                                <input type="checkbox" checked="checked">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-3 pl-0 pr-4">
                                            <label class="custom-checkbox common-fatfrank float-right">hotels
                                                <input type="checkbox" checked="checked">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-5 pl-0">
                                            <label class="custom-checkbox common-fatfrank unchecked float-right">private residences
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row potential-listing-section overflow-hidden">
                            <div class="col-md-8">
                                <div class="potential-listing-title common-open"><b class="common-fatfrank">Potential</b> LISTINGS</div>
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="total-stays"><span>4</span> Stays</div>
                            </div>
                            <div class="col-12 pl-0 pr-0 tripper-dboard-form">
                                <div class="d-block text-center">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                    <div class="zero-state-text common-open">You have no listings saved to this Tripboard yet. Search for listings above to add them to your Tripboard!</div>
                                </div>
                                <!-- <div class="potential-listing-slider tripper-dboard-slider-deal owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>
                                                    <i class="float-right fa fa-thumbs-up "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>
                                                    <div class="d-inline-block float-right voted-text">voted!</div>
                                                    <i class="float-right fa fa-thumbs-up voted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>
                                                    <i class="float-right fa fa-thumbs-up "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>
                                                    <i class="float-right fa fa-thumbs-up "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>
                                                    <i class="float-right fa fa-thumbs-up "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="deals-slider-box">

                                            <div class="d-block w-100 img">
                                                <img src="../../media/images/my-dashboard/tripboard-ptentiallist-1.png" alt="">

                                            </div>
                                            <div class="d-block w-100 potential-listing-slider-content">
                                                <div class="potential-listing-slider-title-1 common-fatfrank common-text-hide">Paris, France</div>
                                                <div class="potential-listing-slider-title-2 mt-1 common-text-hide">Panorama City</div>
                                                <div class="potential-listing-slider-title-3 mt-7 common-text-hide"><b>$1889 </b> It was $2449</div>
                                                <div class="potential-listing-slider-title-3 mt-3 common-text-hide"><b>3 Votes</b>

                                                    <i class="float-right fa fa-thumbs-up"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div> -->

                            </div>
                        </div>
                        <div class="row m-0 mt-5">
                            <div class="col-xl-6 pr-10">
                                <div class="take-last-min-trip-box">
                                    <h1 class="take-last-min-text">
                                        Chat room
                                    </h1>
                                    <div class="border-separator"></div>
                                    <div class="d-block mt-5 chat-box">
                                        <div class="chat-lists">
                                            <div class="recent-message-chatbox">
                                                <div class="row m-0 w-100 chat-scroll">
                                                    <div class="col-md-9 pl-0 pr-0">
                                                        <div class="d-inline-block float-left mr-4">
                                                            <img src="../../media/images/my-dashboard/john-smith.png" alt="user">
                                                        </div>
                                                        <div class="d-inline-block float-left">
                                                            <div class="recent-message-name common-open common-text-hide">
                                                                John smith
                                                            </div>
                                                            <div class="recent-message-content common-open common-text-hide">
                                                                Hello, how are…
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pl-2 pr-0 text-right">
                                                        <div class="recent-message-date common-open">
                                                            9:08pm
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="recent-message-chatbox">
                                                <div class="row m-0 w-100 chat-scroll">
                                                    <div class="col-md-9 pl-0 pr-0">
                                                        <div class="d-inline-block float-left mr-4">
                                                            <img src="../../media/images/my-dashboard/john-smith.png" alt="user">
                                                        </div>
                                                        <div class="d-inline-block float-left">
                                                            <div class="recent-message-name common-open common-text-hide">
                                                                John smith
                                                            </div>
                                                            <div class="recent-message-content common-open common-text-hide">
                                                                Hello, how are…
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pl-2 pr-0 text-right">
                                                        <div class="recent-message-date common-open">
                                                            9:08pm
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="recent-message-chatbox">
                                                <div class="row m-0 w-100 chat-scroll">
                                                    <div class="col-md-9 pl-0 pr-0">
                                                        <div class="d-inline-block float-left mr-4">
                                                            <img src="../../media/images/my-dashboard/john-smith.png" alt="user">
                                                        </div>
                                                        <div class="d-inline-block float-left">
                                                            <div class="recent-message-name common-open common-text-hide">
                                                                John smith
                                                            </div>
                                                            <div class="recent-message-content common-open common-text-hide">
                                                                Hello, how are…
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pl-2 pr-0 text-right">
                                                        <div class="recent-message-date common-open">
                                                            9:08pm
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="recent-message-chatbox">
                                                <div class="row m-0 w-100 chat-scroll">
                                                    <div class="col-md-9 pl-0 pr-0">
                                                        <div class="d-inline-block float-left mr-4">
                                                            <img src="../../media/images/my-dashboard/john-smith.png" alt="user">
                                                        </div>
                                                        <div class="d-inline-block float-left">
                                                            <div class="recent-message-name common-open common-text-hide">
                                                                John smith
                                                            </div>
                                                            <div class="recent-message-content common-open common-text-hide">
                                                                Hello, how are…
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pl-2 pr-0 text-right">
                                                        <div class="recent-message-date common-open">
                                                            9:08pm
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="recent-message-chatbox">
                                                <div class="row m-0 w-100 chat-scroll">
                                                    <div class="col-md-9 pl-0 pr-0">
                                                        <div class="d-inline-block float-left mr-4">
                                                            <img src="../../media/images/my-dashboard/john-smith.png" alt="user">
                                                        </div>
                                                        <div class="d-inline-block float-left">
                                                            <div class="recent-message-name common-open common-text-hide">
                                                                John smith
                                                            </div>
                                                            <div class="recent-message-content common-open common-text-hide">
                                                                Hello, how are…
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pl-2 pr-0 text-right">
                                                        <div class="recent-message-date common-open">
                                                            9:08pm
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat-type-section">
                                            <div class="row m-0 w-100">
                                                <div class="col-md-9 pl-0 pr-0">
                                                    <input type="text" class="float-left border-0 w-100" placeholder="Type here">
                                                </div>
                                                <div class="col-md-3 pr-0 pl-2 text-right">
                                                    <a class="view-past-stay-btn ml-auto w-auto" href="">Send</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-5">
                                <div class="take-last-min-trip-box">
                                    <h1 class="take-last-min-text">
                                        Similar stays
                                    </h1>
                                    <div class="border-separator"></div>
                                    <div class="d-block mt-5">


                                        <div class="d-flex flax-wrap mb-10">

                                            <div class="flex-shrink-0 mr-7">
                                                <div class="symbol symbol-140 symbol-lg-140">
                                                    <img alt="Pic" src="../../media/images/recent-view-2.png">
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">

                                                <div class="d-flex align-items-center justify-content-between flex-wrap">

                                                    <div class="">
                                                        <p class="name-place"> Bay Club of Sandestin
                                                        </p>
                                                        <p class="price-of-place">$650/stay</p>
                                                        <ul class="recent-view-ul">
                                                            <li>1 bed </li>
                                                            <li>1 bath</li>
                                                            <li>2360 Sq Ft</li>
                                                            <li>Vacation Club</li>
                                                        </ul>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="d-flex flax-wrap mb-10">

                                            <div class="flex-shrink-0 mr-7">
                                                <div class="symbol symbol-140 symbol-lg-140">
                                                    <img alt="Pic" src="../../media/images/recent-view-3.png">
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">

                                                <div class="d-flex align-items-center justify-content-between flex-wrap">

                                                    <div class="">
                                                        <p class="name-place"> Bay Club of Sandestin
                                                        </p>
                                                        <p class="price-of-place">$650/stay</p>
                                                        <ul class="recent-view-ul">
                                                            <li>1 bed </li>
                                                            <li>1 bath</li>
                                                            <li>2360 Sq Ft</li>
                                                            <li>Vacation Club</li>
                                                        </ul>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <div class="live-life-travel text-center">
            <div class="container">
                <h2>Live Life Travel</h2>
                <p>Your exclusive details are waiting</p>
            </div>
            <div class="subscribe-box-outer">
                <div class="subscribe-box-inner">
                    <form>
                        <div class="input-box-cstm">
                            <span><img src="../../media/images/tripper-page/msg-img.png" /></span>
                            <input type="text" placeholder="Type Your Email" />
                        </div>
                        <div class="submit-btn-cstm">
                            <input type="submit" name="submit" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <footer>


        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../../media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../../media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <script src="../../plugins/global/plugins.bundle.js"></script>
    <script src="../../js/scripts.bundle.js"></script>
    <script src="../../js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../../js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

    <script>
        $(".potential-listing-slider.owl-carousel").owlCarousel({
            dots: false,
            nav: true,
            lazyLoad: true,
            loop: false,
            slideBy: 3,
            fade: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1700: {
                    items: 4
                },
                2100: {
                    items: 5
                }
            }
        });

        jQuery('#tripper-page-carousal').owlCarousel({
            loop: true,
            margin: 30,
            center: true,
            nav: true,
            dots: false,
            autoHeight: false,
            responsive: {
                0: {
                    items: 1
                },
                640: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
        jQuery(".tripper-banner .banner-comtent a").click(function() {
            jQuery('html,body').animate({
                    scrollTop: jQuery("#tripper-reg").offset().top
                },
                1000);
        });
    </script>

</body>

</html>