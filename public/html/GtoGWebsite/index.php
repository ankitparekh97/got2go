<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;900&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />

    <!-- <link rel="stylesheet" href="../assets/css/rental/bootstrap.min.css"> -->
    <link rel="stylesheet" href="../../css/webfonts/got2gofont.css">
    <link rel="stylesheet" href="../../html/assets/plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../../html/assets/css/style.bundle.min.css">

    

    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <link rel="stylesheet" href="../../css/rental/rental2.css">
    <link rel="stylesheet" href="../../css/custom.css">
    <link rel="stylesheet" href="../../css/rental/dashboard.css">

    <title>GtoG Home</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
    <!-- <nav class="navbar navbar-expand-md navbar-light homepage-nav fixed-top" id="main-nav"> -->
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
           
            <!-- <button class="navbar-toggler" id="homemoburger" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <button class="navbar-toggler collapsed" id="moburger" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button> 
            <a href="" class="navbar-brand" id="gradient-logo">
                <img src="../../media/images/logo.svg" alt="">
            </a>
       
        <ul id="responsive-sidenav" class="navbar-nav btnlinks">
            <li class="nav-item">
                        <a href="" class="nav-link btn-tripper"><img src="../../html/assets/media/images/btn-tripper.svg" alt="" /></a>
                    </li>
                    <li class="nav-item dropdown ">
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img
                                src="../../html/assets/media/images/user.svg" alt="" /></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active-item-link" href="#">Become a tripper</a>
                            <a class="dropdown-item" href="#">Tripboards</a>
                            <a class="dropdown-item" href="#">Rent a Stay</a>
                            <a class="dropdown-item" href="#">List a Stay</a>
                    
                            <a class="dropdown-item font-light" href="" data-toggle="modal" data-target="#login_popup">Login</a>
                            <a class="dropdown-item font-light" href="#">Help</a>
                        </div>
                    </li>
            </ul>
           
            <!-- <a href="" class="navbar-brand responsive-brand-logo-img" id="homelogo">
                <img src="../../media/images/main-navigation/got2go-white-logo.svg" alt="">
               
            </a> -->

            <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="responsive-block-logo">
                <div class="res-logo"><img src="../../media/images/main-navigation/got2go-mobile-logo.svg" alt=""></div>
                <div class="res-close"><button type="button" id="closemenu"><img src="../../media/images/main-navigation/close-pink-icon.svg" alt=""></button></div>
                <div class="input-group mt-10">
                        <input type="text" class="form-control common-inputwithappend" placeholder="Search" aria-label="Search" aria-describedby="Search">
                        <div class="input-group-append common-append">
                            <span class="input-group-text" id=""><button type="button"><i class="fa fa-search"></i></button></span>
                        </div>
                    </div>
            </div>
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="" class="nav-link btn-tripper"><img src="../../html/assets/media/images/btn-tripper.svg" alt="" /></a>
                    </li>
                    <li class="nav-item dropdown ">
                        <a href="" class="nav-link btn-user" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-user-text">Account</span> <img
                                src="../../html/assets/media/images/user.svg" alt="" /></a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active-item-link" href="#">Become a tripper</a>
                            <a class="dropdown-item" href="#">Tripboards</a>
                            <a class="dropdown-item" href="#">Rent a Stay</a>
                            <a class="dropdown-item" href="#">List a Stay</a>
                            <!-- <div class="dropdown-divider"></div> -->
                            <!-- <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#login_popup">
                                Modal - lg
                            </button> -->
                            <a class="dropdown-item font-light" href="" data-toggle="modal" data-target="#login_popup">Login</a>
                            <a class="dropdown-item font-light" href="#">Help</a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="menu-overlay"></div>
    <section id="slider" class="">
        <div class="">
            <div id="carouselExampleIndicators" class="carousel slide pointer-event" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="../../html/assets/media/images/slider1.jpg" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>...</h5>
                            <p>...</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="../../html/assets/media/images/slider2.jpg" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </section>

    <!-- NEWSLETTER -->
    <section id="wheretogo" class="container py-5">
        <div class="row">
            <div class="col-md-12 where-to-go">
                <div class="row">
                    <div class="col-lg-4 col-md-6 paddingtop-30 ">
                        <img src="../../media/images/location.svg" alt="">
                        <select class="form-control" id="kt_select2_1" name="param">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                            <option value="AZ">Arizona</option>
                            <option value="CO">Colorado</option>
                            <option value="ID">Idaho</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NM">New Mexico</option>
                            <option value="ND">North Dakota</option>
                            <option value="UT">Utah</option>
                            <option value="WY">Wyoming</option>
                            <option value="AL">Alabama</option>
                            <option value="AR">Arkansas</option>
                            <option value="IL">Illinois</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="OK">Oklahoma</option>
                            <option value="SD">South Dakota</option>
                            <option value="TX">Texas</option>
                            <option value="TN">Tennessee</option>
                            <option value="WI">Wisconsin</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="IN">Indiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="OH">Ohio</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WV">West Virginia</option>
                        </select>

                    </div>
                    <div class="col-lg-3 col-md-6 paddingtop-30 ">
                        <div class="input-group" id="kt_daterangepicker_1_validate">
                            <img src="../../html/assets/media/images/clock.svg" alt="">
                            <input type="text" class="form-control" placeholder="Select date range">

                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6 paddingtop-30 ">
                        <img src="../../html/assets/media/images/person.svg" alt="">
                        <input type="text" class="form-control persons" placeholder="Number of People">

                    </div>

                    <div class="col-lg-2 col-md-6 text-right paddingtop-20">
                        <a href="" class="gobutton"><img src="../../html/assets/media/images/filter.svg" alt=""></a>
                        <a href="" class="gobutton"><img src="../../html/assets/media/images/gobutton.svg" alt=""></a>

                    </div>
                </div>
            </div>
        </div>
        <div class="checkboxes-inline res-checkbox-inline">
            <div class="row">
                <div class="col-4">
                    <label class="custom-checkbox float-right">Vacation clubs
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">VACATION CLUBS</span>
                    </label>
                </div>
                <div class="col-4">
                    <label class="custom-checkbox common-center-x">hotels
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">HOTELS</span>
                    </label>
                </div>
                <div class="col-4">
                    <label class="custom-checkbox">private residences
                        <input type="checkbox">
                        <span class="checkmark">PRIVATE RESIDENCES</span>
                    </label>
                </div>
            </div>
        </div>
        </div>
    </section>


    <!-- Hot deals slider -->
    <div class="home-hot-deal-slider">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="hot-deals-description">
                        <h1><b>Hot</b> Deals</h1>
                        <p>From wine tasting to hiking to hot air ballooning, select your travel mood and view listing from all your favourite places.</p>
                        <a href="#" class="orangeigradient-btn">Show me More!</a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="owl-carousel owl-theme owl-centered" id="tripper-page-carousal">
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Jackson,</span> WYOMING</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-1.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Portland,</span> Maine</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-2.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Fairbanks,</span> Alaska</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-3.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Jackson,</span> WYOMING</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-1.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Portland,</span> Maine</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-2.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="tripper-carousal-div">
                                <div class="title">
                                    <h4><span>Fairbanks,</span> Alaska</h4>
                                </div>
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/tripper-slider-3.jpg" />
                                </div>
                                <div class="extra-info">
                                    <h4>Extra Tripper Info</h4>
                                    <h2><s>$205</s>  <span color="blue">$150</span><span class="light">/ NIGHT</span></h2>
                                    <h4 class="ratings-bottom"><i class="fa fa-star" aria-hidden="true"></i>4.2 (10)</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Start Trippers Only section -->
        <div class="tripperonly-wrapper">
            <div class="container">
                    <div class="makeiteasy-wrapper">
                        <div class="makeiteasy-inner">
                            <h2>MAKE IT EASY.</h2>
                            <h3> THIS SHOULD BE FUN! </h3>
                            <p>It’s a no brainer. We take the stress out of your trip by making it a one-stop-shop from planning to getting there. You’re on your own for packing that bag. </p>
                            <ul class="list-unstyled">
                                <li>TRIPPER POINTS</li>
                                <li>TRIP BOARDS</li>
                                <li>ITINERARY SCHEDULER</li>
                                <li>EXCLUSIVE TRIPPER DISCOUNTS</li>
                          
                            </ul>
                        </div>
                    </div>
                    <div class="tripperonly-action-wrapper  tripperonly-action-mobile-wrp">
                        <div class="tripperonly-action">
                            <img src="../../html/assets/media/images/tripper-page/tripper-only-img.png" alt="image" class="d-desktop">
                            <!-- <img src="../../html/assets/media/images/tripper-page/tripper-only-img.png" alt="image" class="d-desktop"> -->
                            <!-- <img src="../../html/assets/media/images/tripper-page/Trippers-Only-white.png" alt="image" class="d-mobile"> -->
                            <div class="text-right"><a href="#" class="orangeigradient-btn button-default-animate">SIGN ME UP</a></div>
                        </div>
                    </div>
             </div>
             <!-- <div class="d-mobile">
                <div class="makeiteasy-image">
                    <img src="../../media/images/tripper-page/trippers_homepage_image_mobile.png" />
                </div>
            </div> -->
        </div>
    <!-- End Trippers Only section -->
  <!-- Start List you Stay -->
        <div class="listyoustay-wrapper innerpage-section">
            <div class="container">
                <div class="listyour-section">
                    <div class="listyour-block">
                        <div class="listyour-header page-heading">
                            <div class="d-desktop">
                                <h2>LIST</h2>
                                <h3>YOUR STAY <a href="#" class="yellow-link ml-4"><i class="got got-singlarrow-right"></i></a></h3>
                            </div>
                            <h2 class="d-mobile">List Your Stay</h2>
                        </div>
                        <div class="listyoustay-body">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="liststay-label">Advertising opportunities</div>
                                    <p>Promote your stay with our creative in-site ads and create experiences.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Personal dashboard</div>
                                    <p>Receive, track and respond to your guests.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Customer care team</div>
                                    <p>Available when you need immediate assistance for customer care.</p>
                                </li>
                                <li>
                                    <div class="liststay-label">Estimator tool</div>
                                    <p>Use our estimator to easily determine the value of your property or rental.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="d-mobile text-center">
                                <a href="#" class="orangeigradient-btn">BECOME A HOST</a>
                            </div>
                    </div>
                </div>
                <div class="d-mobile">
                    <div class="listyour-image ">
                        <img src="../../media/images/tripper-page/list_your_stay-_door_image_mobile.png" />
                    </div>
                </div>
            </div>
        </div>
    <!-- End List you Stay -->
     <!-- Start Blog and Journal -->
      <div class="blog-wrapper innerpage-section">
            <div class="container">
                <div class="page-heading text-center">
                    <h2>TRAVEL THE WORLD</h2>
                    <h3>Through our Blog & Journal</h3>
                </div>
            </div>
            <div class="blog-slider-outer" style="background-image: url(../assets/media/images/tripper-page/tripper-slider-bg.png);">
                <div class="container">
                    <div class="owl-carousel owl-theme owl-centered" id="blog-page-carousal">
                        <div class="item">
                            <div class="blog-carousal-div">
                                <div class="image">
                                    <img src="../assets/media/images/tripper-page/property_photo_1.jpg" />
                                </div>
                                <div class="extra-bloginfo">
                                    <h4>Why You Shouldn't Ride Elephants in Indonesia Ride Elephants in Indonesia 1</h4>
                                    <p> Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating  Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating</p>
                                <a href="#" class="ext-link">Read More <i class="got got-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-carousal-div">
                                    <div class="image">
                                        <img src="../assets/media/images/tripper-page/property_photo_3.jpg" />
                                    </div>
                                    <div class="extra-bloginfo">
                                        <h4>Best Travel Quotes For Inspiration Travel Blog</h4>
                                        <p> Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating </p>
                                    <a href="#" class="ext-link">Read More <i class="got got-arrow-right"></i></a>
                                    </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="blog-carousal-div">
                                    <div class="image">
                                        <img src="../assets/media/images/tripper-page/property_photo_2.jpg" />
                                    </div>
                                    <div class="extra-bloginfo">
                                        <h4>Hiking Greenland's Arctic Circle Trail 2019</h4>
                                        <p> Tourism is travel for pleasure or  Tourism is travel for pleasure or business; also the theory and practice of touring, the business of attracting, accommodating  </p>
                                    <a href="#" class="ext-link">Read More <i class="got got-arrow-right"></i></a>
                                    </div>
                                </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!-- <div class="container">
                <div class="text-center blog-action ">
                    <a href="#" class="orangeigradient-btn">Read More</a>
                </div>
            </div> -->
      </div>  
    <!-- End Blog and Journal -->

    <!-- Start Inatagram Gallery -->
    <div class="instagramgallery-wrapper innerpage-section">
        <div class="container">
            <div class="instagramgallery-title page-heading">
                <i class="got got-instagram"></i>
                <h2 class="text=uppercase">GOT2GO DESTINATIONS</h2>
                <h3>NO FILTERS REQUIRED</h3>
            </div>
            <div class="instagramgallery-list">
                <ul class="list-unstyled">
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-3.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-2.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-1.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-2.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-1.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-2.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-1.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-3.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-2.jpg" alt="image" >
                        </div>
                    </li>
                    <li>
                        <div class="insta-image">
                            <img src="../../html/assets/media/images/tripper-page/property-1.jpg" alt="image" >
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Inatagram Gallery -->
    
    <div class="live-life-travel text-center">
        <div class="container">
            <h3>Live Life Travel</h3>
            <p>Your exclusive details are waiting</p>
        </div>
        <div class="subscribe-box-outer">
            <div class="subscribe-box-inner">
                <form>
                    <div class="input-box-cstm">
                        <span><img src="../assets/media/images/tripper-page/msg-img.png" /></span>
                        <input type="text" placeholder="Type Your Email" />
                    </div>
                    <div class="submit-btn-cstm">
                        <input type="submit" name="submit" value="Subscribe" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../assets/media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../assets/media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    

    <!-- <div class="modal fade" id="login_popup" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">LOGIN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                          </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="login-container align-items-center w-100">
                        <div class="d-table">
                            <div class="d-table-cell vertical-middle">
                                <form>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                      
                                          <svg xmlns="http://www.w3.org/2000/svg" width="24.83" height="17.692" viewBox="0 0 24.83 17.692">
                                            <g id="mail" transform="translate(0 -33.085)">
                                              <path id="Path_2462" data-name="Path 2462" d="M167.488,48.89c0-.028.02-.056.018-.084l-7.577,7.3,7.568,7.068c0-.05-.009-.1-.009-.153Z" transform="translate(-142.677 -14.025)" fill="#f80041"/>
                                              <path id="Path_2463" data-name="Path 2463" d="M25.918,126.9l-3.093,2.976a.809.809,0,0,1-1.113.009l-3.085-2.875-7.618,7.342a1.755,1.755,0,0,0,.592.118H32.926a1.754,1.754,0,0,0,.851-.235Z" transform="translate(-9.822 -83.696)" fill="#f80041"/>
                                              <path id="Path_2464" data-name="Path 2464" d="M17.9,44.484,29.465,33.345a1.752,1.752,0,0,0-.893-.26H7.249A1.757,1.757,0,0,0,6.13,33.5Z" transform="translate(-5.469)" fill="#f80041"/>
                                              <path id="Path_2465" data-name="Path 2465" d="M0,52.1V65.96a1.759,1.759,0,0,0,.076.459L7.6,59.175Z" transform="translate(0 -16.964)" fill="#f80041"/>
                                            </g>
                                          </svg>
                                       </span>
                                        </div>
                                        <input type="email" class="form-control" name="url" placeholder="Email Address">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                         
                                          <svg xmlns="http://www.w3.org/2000/svg" width="19.803" height="25.998" viewBox="0 0 19.803 25.998">
                                            <g id="lock" transform="translate(-61)">
                                              <g id="Group_401" data-name="Group 401" transform="translate(69.378 15.284)">
                                                <g id="Group_400" data-name="Group 400">
                                                  <path id="Path_2466" data-name="Path 2466" d="M228.4,303.769a1.523,1.523,0,1,0-1.752,0,.762.762,0,0,1,.295.828l-.7,2.5H228.8l-.7-2.5A.762.762,0,0,1,228.4,303.769Z" transform="translate(-226 -301)" fill="#f80041"/>
                                                </g>
                                              </g>
                                              <g id="Group_403" data-name="Group 403" transform="translate(61 10.714)">
                                                <g id="Group_402" data-name="Group 402">
                                                  <path id="Path_2467" data-name="Path 2467" d="M78.518,211H63.285A2.287,2.287,0,0,0,61,213.285v10.663a2.331,2.331,0,0,0,2.285,2.336H78.518a2.331,2.331,0,0,0,2.285-2.336V213.285A2.287,2.287,0,0,0,78.518,211Zm-4.6,11.22a.761.761,0,0,1-.733.967h-4.57a.761.761,0,0,1-.733-.967l.84-3a3.047,3.047,0,1,1,4.357,0Z" transform="translate(-61 -211)" fill="#f80041"/>
                                                </g>
                                              </g>
                                              <g id="Group_405" data-name="Group 405" transform="translate(64.047)">
                                                <g id="Group_404" data-name="Group 404">
                                                  <path id="Path_2468" data-name="Path 2468" d="M127.855,0A6.855,6.855,0,0,0,121,6.855V9.191h3.047V6.855a3.808,3.808,0,0,1,7.617,0V9.191h3.047V6.855A6.855,6.855,0,0,0,127.855,0Z" transform="translate(-121)" fill="#f80041"/>
                                                </g>
                                              </g>
                                            </g>
                                          </svg>

                                       </span>
                                        </div>
                                        <input type="password" class="form-control" name="" placeholder="Password">
                                    </div>
                                  
                                    <div class="forget-password text-right">
                                        <p><a href="" data-toggle="modal" data-target="#reg_popup">New User? Sign up</a></p>
                                        <p><a href="#">Forget Password?</a></p>
                                    </div>
                                    <input type="submit" value="login">
                                </form>
                               
                                <div class="pt-6">
                                    <button type="button" class="btn login-with-google-btn my-3 mr-5">
                                  <span class="svg-icon svg-icon-md">
                                  
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="61" height="38" viewBox="0 0 61 38">
                                        <image id="google-plus_1_" data-name="google-plus (1)" width="61" height="38" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAmCAYAAAB3c5OxAAAABHNCSVQICAgIfAhkiAAAA3tJREFUaEPVmlmoTlEUx383QzI8mCLKLA8SnkjmMVOmB6IronjzwgtCIi+UB6Uo8SKKJ7OIzCRTXkzlhmueZcy9+mur0/Gdc9b+zjnf9931etZae/3PWXvttf/rVNXX11OENAeGAMOAPkAXoCXQBPgKPAUeAleBM8DrItbIzaTKE/QYYDEwHWhmjKoOuAjsBPYDP412ualZQQ8FNgODUkbyBFgL7AaKSrGU6/81TwKtlN0KLMpisYCPC8AC4FHGfk3u4kD3Ag67PWty5qn0AZgIXPG0S60eBbo/cApol3qFaAd3gOHAxxzXKOi6EOjewKWcAT8DBrsqX2rM/+1p7eHrnimtglQD1KpGAO2BbkDjCDSf3Be+nSHafkDPUM14E+U//KX3APONwZwFdgAngHchmxbASGA2MMed31L5BUwBThrXsKqp2C4LKI8CFF9ieo8GThtWeQAscU2HQR0VxO3AWFex9WKzlqJAKy1vAAMSojkEzAW+eEYt//ry6s7ykKJATwCOJ0RzBJgG/M4j6pQ+iwJ9EJgZs7CaiIHA55TB5WXuDVoV+y3QNCaice7czivotH69QU92nVfUwufdEZM2sDztvUFvAFbFRLTQXRAsQR8FOlkUDTqT3NkvVV1fdUOLks6hZkrbMarY1uqcTtrPHYGXhiCl8hjoatRNUuvu/ElPp8rNJAPj8xqBvuxawkI2r4AORmcNCvRdoG8EMLWKSWd30LTBfOlKBa0MU6ZJGgGtYjJuE7A08HwqoDt7Iamr5PRW/y6+zSLe1bsSC9l7oI0FrdPxBl2OI0up2iMGlIqr2FareIMuR3OyEVgZg2gLsNyK2PF4XlfLUrehIhnuAa1jQKkxOZYnaPlO2tdZXjj2OXIhCpMYD3V1Ihys4p3eclyqq6XaXdWQOBG/vsKK1unNAEYEbLa5CUtBN//oolKQCOuANQlgvrsC99wTtJd6kCPLiy4Su6o3P94QmQrcaoNeKpW8iEEN+DTc0xRjVoAYjAtW3Jv49m+pEBmMw6CzoIDbOjpWraNVfrhLzy2rQRq9cpH9wZg11RRVfCANEB/bco51FKcAazioKWbJpJwDPE065iVQVbm8iHKNatVbV1fiqDb4lrMcyq8HdrnUzuVLJjlN+tJh+2J+v9CA75zbt3sb0u8XYfBRP9poUikW8gVwH7hWiT/a/AHkwz0oFJOASAAAAABJRU5ErkJggg=="/>
                                      </svg>

                                   
                                 </span>
                                 Login with Google
                              </button>
                                    <button type="button" class="btn login-with-fb-btn my-3">
                                  <span class="svg-icon svg-icon-md">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="41" height="41" viewBox="0 0 41 41">
                                        <image id="facebook" width="41" height="41" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAYAAACoYAD2AAAABHNCSVQICAgIfAhkiAAAAtxJREFUWEe9mcurTVEcxz8HJUkplAw8LiUG8ogMDChlKnN55FGuwkzKsyvMkEdCHvcPMKWklIFwu0gm8kxS7g1FF7kdfW97a9vttdfav7OWPTmns9bv9/usddb6vXar3W5jfLqA1cAKYD4wE5gIjAaGgS/AG+A5cA+4Dbyy2Go1hBwHbAI2AksNBh8CV4ErwFCofCjkGKAb2AtMDVVeM+8jcBw4C/z26QuBXAhcAxb4lBnGnwIbgMd1sj7ILcAZYKwBIFTkJ7ATuOQSqIM8DBwItRRh3hHgYJUeF+T/BszZKkGrIPUXX4ywM1YVW8t/fRlSl+R+4jPog9cZXV68TEVIuZm+BLf4M3ALeALou3xtC5gCrAXmVVDr1i/J3VMRchdw0rfMhuMngB7gm0NOjl0uqOrZDZzSQA6p1SlkxXDUuUEt+rRnUXWQcvgKvUM55I7M+zfcKOf0u8DKAGV1kBJXlDuXQz4wxmIXxzrgRgRIxfplgtSWvgxQ2GSKsqGvJQFdEJ35uYXfJwPjPYpnC3IbcKEJgWeuLsmEijk3gTUGO9sFeR1YbxB2ibzNcsvy+AtgjsFOryAfZT7JIF8p4oJUAjzDYKRPkAPAJINw0520Qg4KUkmnUn7rozD2oyD8zhG1FEWml4zo7I7yGB4WpLnIyZQrYzpkXKECyCyfbIydtEIqV1Cdo8/aJ8aZtEKqunztAwQGYtxuK+Qq4E4AZH8MP2mF3AxcDoAc8ZOxI05sP9mdInbHhuxKkQXFhOwHFqfIJ2NCKs89nyIzjwWpcK0I9Tcz1yWLVePEgtyT11wpqsUYkM+ARVXVonYzRt3dKeQvlQxZCTziRlN0MDqFVAflHyefohfUCeQxYF85CqXoqlkh1UTYXxUmU/Qnm0J21J/MF9W009sEMkqnNwdt0jMPgfwEHI3ZMy8ek5C3D3WQg1nvsxf4HpCmOV1QqKzrPc57R909DfgQqrw47w+afCGAjkrxfgAAAABJRU5ErkJggg=="/>
                                      </svg>
                                  
                                 </span>
                                 Login with Facebook
                              </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div> -->


      <div class="modal fade" id="login_popup" tabindex="-1" role="dialog" aria-labelledby="login_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                          </svg>
                    </button>
                </div>
                <div class="modal-body">
                <h1> Account <b>Login</b> </h1>
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                      
                          
                                <form>
                                    <div class="d-block text-center">
                                        <a href="#"><img src="../../media/images/tripper-page/google.png" alt=""></a>
                                        <a href="#"><img src="../../media/images/tripper-page/favebook-login-btn.png" alt=""></a>
                                    </div>
                                    <div class="forget-password">
                                    Or use your email account:  
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                      
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24.83" height="17.692" viewBox="0 0 24.83 17.692">
                                            <g id="mail" transform="translate(0 -33.085)">
                                              <path id="Path_2462" data-name="Path 2462" d="M167.488,48.89c0-.028.02-.056.018-.084l-7.577,7.3,7.568,7.068c0-.05-.009-.1-.009-.153Z" transform="translate(-142.677 -14.025)" fill="#939FA4"/>
                                              <path id="Path_2463" data-name="Path 2463" d="M25.918,126.9l-3.093,2.976a.809.809,0,0,1-1.113.009l-3.085-2.875-7.618,7.342a1.755,1.755,0,0,0,.592.118H32.926a1.754,1.754,0,0,0,.851-.235Z" transform="translate(-9.822 -83.696)" fill="#939FA4"/>
                                              <path id="Path_2464" data-name="Path 2464" d="M17.9,44.484,29.465,33.345a1.752,1.752,0,0,0-.893-.26H7.249A1.757,1.757,0,0,0,6.13,33.5Z" transform="translate(-5.469)" fill="#939FA4"/>
                                              <path id="Path_2465" data-name="Path 2465" d="M0,52.1V65.96a1.759,1.759,0,0,0,.076.459L7.6,59.175Z" transform="translate(0 -16.964)" fill="#939FA4"/>
                                            </g>
                                          </svg>
                                       </span>
                                        </div>
                                        <input type="email" class="form-control" name="url" placeholder="Email Address">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                         
                                            <svg xmlns="http://www.w3.org/2000/svg" width="19.803" height="25.998" viewBox="0 0 19.803 25.998">
                                            <g id="lock" transform="translate(-61)">
                                              <g id="Group_401" data-name="Group 401" transform="translate(69.378 15.284)">
                                                <g id="Group_400" data-name="Group 400">
                                                  <path id="Path_2466" data-name="Path 2466" d="M228.4,303.769a1.523,1.523,0,1,0-1.752,0,.762.762,0,0,1,.295.828l-.7,2.5H228.8l-.7-2.5A.762.762,0,0,1,228.4,303.769Z" transform="translate(-226 -301)" fill="#939FA4"/>
                                                </g>
                                              </g>
                                              <g id="Group_403" data-name="Group 403" transform="translate(61 10.714)">
                                                <g id="Group_402" data-name="Group 402">
                                                  <path id="Path_2467" data-name="Path 2467" d="M78.518,211H63.285A2.287,2.287,0,0,0,61,213.285v10.663a2.331,2.331,0,0,0,2.285,2.336H78.518a2.331,2.331,0,0,0,2.285-2.336V213.285A2.287,2.287,0,0,0,78.518,211Zm-4.6,11.22a.761.761,0,0,1-.733.967h-4.57a.761.761,0,0,1-.733-.967l.84-3a3.047,3.047,0,1,1,4.357,0Z" transform="translate(-61 -211)" fill="#939FA4"/>
                                                </g>
                                              </g>
                                              <g id="Group_405" data-name="Group 405" transform="translate(64.047)">
                                                <g id="Group_404" data-name="Group 404">
                                                  <path id="Path_2468" data-name="Path 2468" d="M127.855,0A6.855,6.855,0,0,0,121,6.855V9.191h3.047V6.855a3.808,3.808,0,0,1,7.617,0V9.191h3.047V6.855A6.855,6.855,0,0,0,127.855,0Z" transform="translate(-121)" fill="#939FA4"/>
                                                </g>
                                              </g>
                                            </g>
                                          </svg>

                                       </span>
                                        </div>
                                        <input type="password" class="form-control" name="" placeholder="Password">
                                    </div>
                                    <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="21.732" height="26.92" viewBox="0 0 25.732 26.92">
                                                        <g id="user_1_" data-name="user (1)" transform="translate(-11.328)">
                                                          <g id="Group_407" data-name="Group 407" transform="translate(11.328 13.495)">
                                                            <g id="Group_406" data-name="Group 406" transform="translate(0)">
                                                              <path id="Path_2469" data-name="Path 2469" d="M24.194,257.323a12.881,12.881,0,0,0-12.866,12.866.56.56,0,0,0,.559.559H36.5a.56.56,0,0,0,.559-.559A12.881,12.881,0,0,0,24.194,257.323Z" transform="translate(-11.328 -257.323)" fill="#939FA4"></path>
                                                            </g>
                                                          </g>
                                                          <g id="Group_409" data-name="Group 409" transform="translate(18.041 0)">
                                                            <g id="Group_408" data-name="Group 408" transform="translate(0 0)">
                                                              <circle id="Ellipse_28" data-name="Ellipse 28" cx="6.153" cy="6.153" r="6.153" fill="#939FA4"></circle>
                                                            </g>
                                                          </g>
                                                        </g>
                                                      </svg>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control required" name="name" id="name" placeholder="First Name">

                                        </div>
                                      
                                    <div class="forget-password">
                                        <a href="#">Forgot Password?</a>
                                    </div>
                                    <button type="submit" value="login" class="mb-10 button-default-animate">Login <img src="../../media/images/fly-orange-gradient.png" alt="" class="ml-5"></button>
                                   
                                    <div class="forget-password">
                                    New here? Click to    <a href="#" data-toggle="modal" data-target="#reg_popup">Sign Up</a>
                                    </div>
                                </form>
                               
                        
                            
                    </div>
                </div>
              
            </div>
        </div>
    </div>

    <div class="modal fade" id="reg_popup" tabindex="-1" role="dialog" aria-labelledby="reg_popup" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
                <div class="modal-header">
               
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 30 30">
                            <image id="close_1_" data-name="close (1)" width="30" height="30" opacity="0.149" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAATlJREFUSEu1109qAjEUx/Gvde0dSo8kopRKbyGl64LgIdwIVnApHsZNae8hFHkyQgj5896bTJaZPx+SSX4vMwI+gXdgBZwZts2BDbAfAT/AC3AFZsBpIPsN2AFPwJ/AC+C76xgKD9F/4FVgaeGF1niMyme9T/WjDYEnUQFDuPXIs2gKboUX0RzcF6+iJdiLq9AabMXVqAbW4iZUC9dwM2qBc/gkiEFJpHs4aCI33se1Z+KQGXdRa0KtI04lnPSZ0VawK9v7TLWMVJqUOTNugVOrV+BHjTXhWri0ZVxVTQNr9qkZr8Ea1FXPS7AFNeM52IOa8BTcB1XjpaOPK5GCzC0uuNxhry9aHXnqeNsKLeICy2/FwVtlauWsux5P+1TgX+DZW2WUcFzPLwJ/AUvgAzgaXuS5VWZ3DWxvtjyfdH3BZ74AAAAASUVORK5CYII="/>
                          </svg>
                    </button>
                </div>
                <div class="modal-body">
                <h1> Create <b>account</b> </h1>
                <!-- <h1> Registeration <b>code</b> </h1> -->
                    <div class="login-container align-items-center w-100 pt-4 pb-4 vertical-middle text-center">
                      
                          
                                <form>
                                    <div class="d-block text-center">
                                        <a href="#"><img src="../../media/images/tripper-page/google.png" alt=""></a>
                                        <a href="#"><img src="../../media/images/tripper-page/favebook-login-btn.png" alt=""></a>
                                    </div>
                                    <div class="forget-password">
                                    Or use your email for registeration:  
                                    </div>
                                    <div class="form-group">
                                        
                                        <input type="number" class="form-control" name="url" placeholder="Registration Code">
                                    </div>
                                    <div class="forget-password mt-5 mb-15">
                                    Enter Registeration code:  
                                    </div>
                                    <div class="form-group regis-code">
                                        
                                        <input type="number" class="reg-code-digit" name="digit" min="0" max="9" maxlength="1">
                                        <input type="number" class="reg-code-digit" name="digit" min="0" max="9" maxlength="1">
                                        <input type="number" class="reg-code-digit" name="digit"  min="0" max="9" maxlength="1">
                                        <input type="number" class="reg-code-digit" name="digit"  min="0" max="9" maxlength="1">
                                    </div>
                                   
                                    <button type="submit" value="Signup" class="mt-10 mb-10 button-default-animate">Sign up <img src="../../media/images/fly-orange-gradient.png" alt="" class="ml-5"></button>
                                
                                   
                                 
                                </form>
                               
                        
                            
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    <!-- <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->


    <script src="../../html/assets/plugins/global/plugins.bundle.js"></script>
    <script src="../../html/assets/js/scripts.bundle.js"></script>
    <script src="../../html/assets/js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../../html/assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

    <script>
/********Top navigation */
$('#homemoburger, #moburger').on('click', function() {
  $(".menu-overlay").fadeIn(10);
});
$(".menu-overlay").click(function(event) {
  $("#homemoburger").trigger("click");
  $("#moburger").trigger("click");
  $(".menu-overlay").fadeOut(10);
});
$("#closemenu").click(function(event) {
  $("#homemoburger").trigger("click");
  $("#moburger").trigger("click");
  $(".menu-overlay").fadeOut(10);
});
    jQuery('#tripper-page-carousal').owlCarousel({
        loop:true,
        margin:30,
        center: true,
        nav:true,
        dots: false,
        autoHeight: false,
        responsive:{
            0:{
                items:1
            },
            640:{
                items:1
            },
            767:{
                items:3
            },
            992:{
                items:2
            },
            1399:{
                items:3
            }
        }
    });
    </script> 
    <script>
        $(document).ready(function() {
            $('ul.navbar-nav.ml-auto li.nav-item a').click(function() {
                $('li.nav-item a').removeClass("active-menu-link");
                $(this).addClass("active-menu-link");
            });
        });
        $(document).ready(function() {
            $('#login_popup').on('show.bs.modal', function() {
                $("#reg_popup").modal("hide");
            });

            $('#reg_popup').on('show.bs.modal', function() {
                $("#login_popup").modal("hide");
            });
        });
    </script>

<script>
        jQuery('#blog-page-carousal').owlCarousel({
            loop:true,
            margin:0,
            center: true,
            nav:true,
            dots: false,
            autoHeight: false,
            responsive:{
                0:{
                    items:1
                },
                640:{
                    items:1,
                    margin:10
                },
                768:{
                    items:2,
                    margin:30
                },
                1000:{
                    items:3,
                    nav:false,
                    margin:30
                }
            }
        });
   
</script> 
    <!-- <script>
        // Get the current year for the copyright
        $('#year').text(new Date().getFullYear());

        // Init Scrollspy
        $('body').scrollspy({
            target: '#main-nav'
        });

        // Smooth Scrolling
        $("#main-nav a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();

                const hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function() {

                    window.location.hash = hash;
                });
            }
        });
    </script> -->
</body>

</html>
