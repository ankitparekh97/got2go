<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/ffx5zmj.css" rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet'> -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,600,800,900' rel='stylesheet'>

    <!-- <link rel="stylesheet" href="../assets/css/rental/bootstrap.min.css"> -->

    <link rel="stylesheet" href="../assets/plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="../assets/css/style.bundle.min.css">
    <link rel="stylesheet" href="../../css/rental/rental.css">
    <link rel="stylesheet" href="../../css/rental/rental1.css">
    <title>trippers Only</title>
</head>

<body id="home" data-spy="scroll" data-target="#main-nav">
<style>
</style>
    <nav class="navbar navbar-expand-md navbar-light fixed-top" id="main-nav">
        <div class="container-fluid bg-white bd-8">
            <a href="" class="navbar-brand">
                <img src="../../html/assets/media/images/logo.svg" alt="">
                <!-- <h3 class="d-inline align-middle"></h3> -->
            </a>
            <button class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul id="mainnavigatn" class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active-menu-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">View Stays</a>
                    </li>
                    <li class="nav-item ">
                        <a href="#" class="nav-link">List your Stay</a>
                    </li>
                </ul>
                <ul id="sidenav" class="navbar-nav btnlinks">
                    <li class="nav-item">
                        <a href="#">
                            <div class="trippers-header-btn">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn.png" alt="">
                                <img src="../../html/assets/media/images/tripper-page/tripper-only-header-btn-user.png" class="user-mg" alt="">
                            </div>

                        </a>
                    </li>
                   
                    <li class="nav-item ">
                        <a href="" class="nav-link btn-search"> <img src="../../html/assets/media/images/search.svg" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="tripper-banner" style="background-image: url(../assets/media/images/tripper-page/tripper-banner.jpg);">
        <div class="banner-comtent">
            <img src="../assets/media/images/tripper-page/trippersonlywhite.png" />
            <a href="javascript:void(0)">Registration</a>
        </div>
    </div>

    <div class="page-content-start tripper-page registration no-padding">
        
        <div class="tripper-registration" id="tripper-reg">
            <div class="container">
                <h2>TRIPPER <span>REGISTRATION</span></h2>
                <p>Please enter in the confirmation code you received to the inbox of the email address provided.</p>
                <form>
                    <div class="form-group-custm no-icon">
                        <input class="form-class-cstm" type="text" name="emailaddress" placeholder="Registartion Code" />
                    </div>
                    <div class="text-center">
                        
                        <div class="form-group-custm">
                            <input class="form-class-cstm" type="submit" value="NEXT" />
                        </div>
                       
                    </div>
                    
                </form>
            </div>
        </div>

        
    </div>

        <div class="live-life-travel text-center">
            <div class="container">
                <h3>Live Life Travel</h3>
                <p>Your exclusive details are waiting</p>
            </div>
            <div class="subscribe-box-outer">
                <div class="subscribe-box-inner">
                    <form>
                        <div class="input-box-cstm">
                            <span><img src="../assets/media/images/tripper-page/msg-img.png" /></span>
                            <input type="text" placeholder="Type Your Email" />
                        </div>
                        <div class="submit-btn-cstm">
                            <input type="submit" name="submit" value="Subscribe" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

    
    <footer>
        

        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="footer-logo">
                        <img src="../assets/media/images/tripper-page/footer-logo.png" alt="logo" />
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <div class="footer-menu">
                        <ul>
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Tours</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-contact">
                        <a href="tel:(111) 111-1111"><img src="../assets/media/images/tripper-page/contact-img.png" alt="logo" />(111) 111-1111</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

   

    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/pages/crud/forms/widgets/select2.js"></script>
    <script src="../assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

<script>
jQuery('#tripper-page-carousal').owlCarousel({
    loop:true,
    margin:30,
    center: true,
    nav:true,
    dots: false,
    autoHeight: false,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
jQuery(".tripper-banner .banner-comtent a").click(function() {
    jQuery('html,body').animate({
        scrollTop: jQuery("#tripper-reg").offset().top},
        1000);
});
</script> 
    
</body>

</html>