<?php include('header.php'); ?>
   <!--begin::Container-->
   <div class="d-flex flex-row flex-column-fluid  container ">
      <!--begin::Content Wrapper-->
      <div class="main d-flex flex-column flex-row-fluid">

         <div class="content flex-column-fluid d-lg-flex" id="kt_content">
            <!--begin::Wizard 6-->
            <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
               <!--begin::Container-->
               <div class="wizard-content d-flex flex-column mx-auto w-100">
                  <!--begin::Nav-->
                  <div class="d-flex flex-column-auto flex-column px-10 header-wizard">
                     <!--begin: Wizard Nav-->
                     <div class="wizard-nav d-flex flex-column  align-items-center align-items-md-start">
                        <!--begin::Wizard Steps-->
                        <div class="wizard-steps d-flex flex-column flex-md-row w-md-700px mx-auto w-100">
                           <!--begin::Wizard Step 1 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                              <div class="wizard-wrapper pr-lg-7 pr-5">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">1</span>
                                 </div>
                                 <div class="wizard-label mr-3">
                                    <h3 class="wizard-title">
                                       Basic
                                    </h3>
                                    
                                 </div>
                                 <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <polygon points="0 0 24 0 24 24 0 24"/>
                                          <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                          <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                       </g>
                                    </svg>
                                 </span>
                              </div>
                           </div>
                           <!--end::Wizard Step 1 Nav-->
                           <!--begin::Wizard Step 2 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                              <div class="wizard-wrapper pr-lg-7 pr-5">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">2</span>
                                 </div>
                                 <div class="wizard-label mr-3">
                                    <h3 class="wizard-title">
                                       Property details
                                    </h3>
                                 </div>
                                 <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                       <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <polygon points="0 0 24 0 24 24 0 24"/>
                                          <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                          <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                       </g>
                                    </svg>
                                 </span>
                              </div>
                           </div>
                           <!--end::Wizard Step 2 Nav-->
                           <!--begin::Wizard Step 3 Nav-->
                           <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                              <div class="wizard-wrapper">
                                 <div class="wizard-icon">
                                    <i class="wizard-check ki ki-check"></i>
                                    <span class="wizard-number">3</span>
                                 </div>
                                 <div class="wizard-label">
                                    <h3 class="wizard-title">
                                       Amenities and Media
                                    </h3>
                                 </div>
                              </div>
                           </div>
                           <!--end::Wizard Step 3 Nav-->
                        </div>
                        <!--end::Wizard Steps-->
                     </div>
                     <!--end: Wizard Nav-->
                  </div>
                  <!--end::Nav-->
                  <!--begin::Form-->
                  <form class="px-10 pt-10" novalidate="novalidate" id="kt_wizard_form">
                     <!--begin: Wizard Step 1-->
                     <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                        <h5 class="f-w-700">Basic</h5>
                        <div class="steps-container">
                           <!--begin::Form Group-->
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Name your place</label>
                                    <input type="email" name="firstname" class="form-control" placeholder="" required>
                                 </div>
                                 <div class="form-group">
                                    <label>Property type</label>
                                    <select class="form-control" id="exampleSelect1">
                                       <option>Studio Appartment</option>
                                       <option>Townhome</option>
                                       <option>Condo</option>
                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label>Guest will have</label>
                                    <div class="radio-list">
                                      <label class="radio">
                                          <input type="radio" checked="checked" name="radios4">
                                          <span></span>
                                          <div class="w-100">Entire Place<br>(Guest have the whole place to themselves. This usually includes a bedroom, a bathroom, and a kitchen.)</div>
                                      </label>
                                      <label class="radio">
                                          <input type="radio" name="radios4">
                                          <span></span>
                                          <div class="w-100">Private Room<br>(Guest have their own private room for sleeping. Other areas could be shared.)</div>
                                      </label>
                                  </div>
                                 </div>
                                  <div class="form-group">
                                    <label>Is this set up as a dedicated guest space?</label>
                                    <div class="radio-list">
                                      <label class="radio">
                                          <input type="radio" checked="checked" name="radio">
                                          <span></span>
                                          <div class="w-100">Yes, It is setup for Guest only</div>
                                      </label>
                                      <label class="radio">
                                          <input type="radio" name="radio">
                                          <span></span>
                                          <div class="w-100">No, There are some of my belongings</div>
                                      </label>
                                  </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Listing as a company?</label>
                                    <div class="radio-list">
                                      <label class="radio">
                                          <input type="radio" checked="checked" name="radio1">
                                          <span></span>
                                          <div class="w-100">Yes, I am posting as an organization</div>
                                      </label>
                                      <label class="radio">
                                          <input type="radio" name="radio1">
                                          <span></span>
                                          <div class="w-100">No, I am an individual</div>
                                      </label>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Describe</label>
                                    <textarea class="form-control" rows="3"></textarea>
                                 </div>
                                 <div class="form-group">
                                    <label>Enter location details</label>
                                    <input type="email" class="form-control" required>
                                 </div>
                                 <div class="pt-5 pb-5 text-center">
                                    <h5>OR</h5>
                                 </div>
                                 <label class="mark-place"><i class="fas fa-map-marker"></i>Mark place on map</label>
                                 <div class="map">
                                    <div id="kt_leaflet_5" style="height:300px;"></div>
                                 </div>
                              </div>
                           </div>
                           <!--end::Form Group-->
                        </div>
                     </div>
                     <!--end: Wizard Step 1-->
                     <!--begin: Wizard Step 2-->
                     <div class="pb-5" data-wizard-type="step-content">
                        <!--begin::Title-->
                        <h5 class="f-w-700">Property details</h5>
                        <!--begin::Title-->
                        <div class="steps-container">
                           <div class="row">
                              <div class="col-md-7">
                                 <h6 class="f-w-700 pb-4">Structure & Furniture details:</h6>
                                 <div class="border-bottom mb-5">
                                    <div class="row">
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <label>Common Room</label>
                                             <input id="common_room" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>                                          
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <label>Sofa</label>
                                             <input id="sofa" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <label>Common Bath</label>
                                             <input id="common_bath" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div class="bed-room-container base-group" style="display: none;">
                                    <div class="close-button"><i class="icon-xl fas fa-window-close"></i></div>
                                    <h6 class="f-w-700 pb-4">Bedroom</h6>
                                    <div class="border-bottom mb-5">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Bathrooms</label>
                                                <input id="bathroom" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>                                          
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Beds</label>
                                                <input id="beds" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Bed Type</label>
                                                <input type="email" class="form-control" required="">
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Number of guests?</label>
                                                <input id="guest" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>              
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row align-items-center">
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Price</label>
                                                <input id="guest" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>                                          
                                                <label>Per Night</label>
                                             </div>
                                          </div>
                                          <div class="col-sm-8">
                                             <label><span class="f-w-700">Service Fees:$2</span>(Auto calculated based on membership payment)</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="bed-room-container main-group">
                                    <h6 class="f-w-700 pb-4">Bedroom</h6>
                                    <div class="border-bottom mb-5">
                                       <div class="row">
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Bathrooms</label>
                                                <input id="bathroom" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>                                          
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Beds</label>
                                                <input id="beds" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Bed Type</label>
                                                <input type="email" class="form-control" required="">
                                             </div>
                                          </div>
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Number of guests?</label>
                                                <input id="guest" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>              
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row align-items-center">
                                          <div class="col-sm-4">
                                             <div class="form-group">
                                                <label>Price</label>
                                                <input id="guest" type="text" class="form-control" value="0" name="demo0" placeholder="Select time"/>                                          
                                                <label>Per Night</label>
                                             </div>
                                          </div>
                                          <div class="col-sm-8">
                                             <label><span class="f-w-700">Service Fees:$2</span>(Auto calculated based on membership payment)</label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="add-new-room mb-5">
                                    <a href="#" class="btn btn-success btn-sm mr-3 repeat"><i class="icon-xl fas fa-plus-circle"></i> Add Bedroom</a>
                                 </div>
                                 <div class="form-group">
                                    <label>Proof of ownership or lease</label>
                                    <input type="file" class="form-control" />                                   
                                 </div>
                                 <div class="form-group">
                                    <label><span class="f-w-700">occupancy Taxes and fees:$12</span>(Auto calculated as per state norms)</label>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <div class="radio-list">
                                             <label class="radio">
                                                <input type="radio" checked="checked" name="radios4">
                                                <span></span>
                                                <div class="w-100">Allow instant Booking</div>
                                            </label>
                                         </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <div class="radio-list">
                                             <label class="radio">
                                                <input type="radio" checked="checked" name="radios4">
                                                <span></span>
                                                <div class="w-100">Allow partial Booking</div>
                                            </label>
                                         </div>
                                      </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label>Booking Amount</label>
                                    <input type="text" class="form-control" required>
                                 </div>

                              </div>
                              <div class="col-md-5 property-top-space">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Check-In</label>
                                          <input class="form-control" id="kt_timepicker_1" readonly placeholder="Select time" type="text"/>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Check-Out</label>
                                          <input class="form-control" id="kt_timepicker_2" readonly placeholder="Select time" type="text"/>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cancellation-sec">
                                    <div class="form-group">
                                       <label>Cancellation</label>
                                       <div class="radio-list">
                                          <label class="radio">
                                             <input type="radio" checked="checked" name="cancellation">
                                             <span></span>
                                             <div class="w-100"><span class="f-w-700">Flexible</span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</div>
                                         </label>
                                      </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="radio-list">
                                          <label class="radio">
                                             <input type="radio" name="cancellation">
                                             <span></span>
                                             <div class="w-100"><span class="f-w-700">Moderate</span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</div>
                                         </label>
                                      </div>
                                   </div>
                                   <div class="form-group">
                                       <div class="radio-list">
                                          <label class="radio">
                                             <input type="radio" name="cancellation">
                                             <span></span>
                                             <div class="w-100"><span class="f-w-700">Moderate</span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</div>
                                         </label>
                                      </div>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--end: Wizard Step 2-->
                     <!--begin: Wizard Step 3-->
                     <div class="pb-5" data-wizard-type="step-content">
                        <!--begin::Title-->
                        
                        <h5 class="f-w-700">Amenities and Media</h5>
                        <div class="steps-container">
                           <div class="row">
                              <div class="col-md-6 border-right">
                                 <div class="form-group">
                                 <label class="w-100">Amenities</label>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Kitchen
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Shampoo
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Heating
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Air Conditioning
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Washer
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Dryer
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Wireless Internet
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Breakfast
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Family/Kid friendly
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Indoor fireplace
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Buzzer/Wireless intercom
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Doorman
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Hangers
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Iron
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Hair dryer
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Laptop friendly workspace
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Self Check-in
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  TV
                                              </label>
                                           </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="checkbox">
                                                  <input type="checkbox" checked="checked" name="Checkboxes3">
                                                  <span></span>
                                                  Smoke detector
                                              </label>
                                           </div>
                                        </div>
                                        
                                    </div>
                                 
                                 <!-- Add new amenities -->
                                 <?php /*
                                 <div class="form-group">
                                    <label>Add New Amenities</label>
                                    
                                     <input id="kt_tagify_1" class="form-control tagify" name='tags' placeholder='type...' value='' autofocus data-blacklist='.NET,PHP'/>

                                     <div class="mt-3">
                                         <a href="javascript:;" id="kt_tagify_1_remove" class="btn btn-sm btn-light-primary font-weight-bold">Remove Amenities</a>
                                     </div>
                                </div>
                                */ ?>
                              </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <div class="field" align="left">
                                       <label>Add photos of listing</label>
                                       <div class="input-images-1" style="padding-top: .5rem;"></div>
                                    </div>
                                 </div>
                                 <script>
                                 jQuery( document ).ready(function() {
                                     jQuery('.input-images-1').imageUploader();
                                 });

                                 </script>

                                 <div class="form-group">
                                    <label>Add Video</label> 
                                    <input type="file" class="form-control">                                   
                                 </div>
                              </div>
                           </div>
                           
                        </div>
                        
                        <!--end::Title-->
                        <!--begin::Section-->
                        
                        <!--end::Section-->
                     </div>
                     <!--end: Wizard Step 3-->
                     <!--begin: Wizard Actions-->
                     <div class="d-flex justify-content-between pt-7">
                        <div class="mr-2">
                           <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                              <span class="svg-icon svg-icon-md mr-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/>
                                       <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                              Previous
                           </button>
                        </div>
                        <div>
                           <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">
                              Submit
                              <span class="svg-icon svg-icon-md ml-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                       <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                           </button>
                           <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">
                              Next
                              <span class="svg-icon svg-icon-md ml-2">
                                 <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <polygon points="0 0 24 0 24 24 0 24"/>
                                       <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                       <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                    </g>
                                 </svg>
                                 <!--end::Svg Icon-->
                              </span>
                           </button>
                        </div>
                     </div>
                     <!--end: Wizard Actions-->
                  </form>
                  <!--end::Form-->
               </div>
               <!--end::Container-->
            </div>
            <!--end::Wizard 6-->
         </div>
         <!--end::Content-->
      </div>
      <!--begin::Content Wrapper-->
   </div>
   <!--end::Container-->

<?php include('footer.php'); ?>
<script>
$(function() {
  $(".repeat").on('click', function(e) {
      var c = $('.base-group').clone();
    c.removeClass('base-group').css('display','block').addClass("child-group");
    $(".main-group").append(c);
  });
});
</script>
<script>
// Class definition
var KTLeaflet = function () {

   // Private functions
   var demo5 = function () {
      // Define Map Location
      var leaflet = L.map('kt_leaflet_5', {
         center: [40.725, -73.985],
         zoom: 13
      });

      // Init Leaflet Map
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
         attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(leaflet);

      // Set Geocoding
      var geocodeService;
      if (typeof L.esri.Geocoding === 'undefined') {
         geocodeService = L.esri.geocodeService();
      } else {
         geocodeService = L.esri.Geocoding.geocodeService();
      }

      // Define Marker Layer
      var markerLayer = L.layerGroup().addTo(leaflet);

      // Set Custom SVG icon marker
      var leafletIcon = L.divIcon({
         html: `<span class="svg-icon svg-icon-danger svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
         bgPos: [10, 10],
         iconAnchor: [20, 37],
         popupAnchor: [0, -37],
         className: 'leaflet-marker'
      });

      // Map onClick Action
      leaflet.on('click', function (e) {
         geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
            if (error) {
               return;
            }
            markerLayer.clearLayers(); // remove this line to allow multi-markers on click
            L.marker(result.latlng, { icon: leafletIcon }).addTo(markerLayer).bindPopup(result.address.Match_addr, { closeButton: false }).openPopup();
            alert(`${result.address.Match_addr}`);
         });
      });
   }

   return {
      // public functions
      init: function () {
         // default charts
         demo5();
      }
   };
}();

jQuery(document).ready(function () {
   KTLeaflet.init();
});
</script>

