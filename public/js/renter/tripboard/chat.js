$(function () {
    $(document).ready(function () {
        let pusher = new Pusher($("#pusher_app_key").val(), {
            cluster: $("#pusher_cluster").val(),
            encrypted: true
        });

        let channel = pusher.subscribe('group-chat');
        let chatBox = $(".minichat-wrapper");
        let id = chatBox.attr('data-id');
        loadLatestMessages(chatBox, id);

        // on change chat input text toggle the chat btn disabled state
        $(".chat_input").on("change keyup", function (e) {
            if ($(this).val() != "") {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", false);
            } else {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", true);
            }
        });
        $('.chat_input').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                var button = $(this).parent().next().children();
                send(button.attr('data-id'), $("#chat_box_" + button.attr('data-id')).find(".chat_input").val());
            }
        });
        // on click the btn send the message
        $(".btn-chat").on("click", function (e) {
            send($(this).attr('data-id'), $("#chat_box_" + $(this).attr('data-id')).find(".chat_input").val());
        });
        // listen for the send event, this event will be triggered on click the send btn
        channel.bind('send', function (data) {
            displayMessage(data.data);
        });

        let lastScrollTop = 0;
        $(".chat-area").on("scroll", function (e) {
            let st = $(this).scrollTop();
            if (st < lastScrollTop) {
                fetchOldMessages($(this).parents(".chat-opened").find("#to_user_id").val(), $(this).find(".msg_container:first-child").attr("data-message-id"));
            }
            lastScrollTop = st;
        });
        // listen for the oldMsgs event, this event will be triggered on scroll top
        channel.bind('oldMsgs', function (data) {
            displayOldMessages(data);
        });
    });

});


/**
 * loaderHtml
 *
 * @returns {string}
 */
function loaderHtml() {
    return '<i class="glyphicon glyphicon-refresh loader"></i>';
}

/**
 * getMessageSenderHtml
 *
 * this is the message template for the sender
 *
 * @param message
 * @returns {string}
 */
function getMessageSenderHtml(message) {
    if (message.rentalphoto != null) {
        var imageHtml = '<img src="' + imagePath + '/' + message.rentalphoto + '" alt=""></img>';
    } else {
        var matches = message.rentalUserName.match(/\b(\w)/g);
        var acronym = matches.join('');
        var imageHtml = '<div id="profileImage" class="rental">' + acronym + '</div>';
    }

    var separatorHtml = '';
    if ($(".mychat-contactList ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }
    return `${separatorHtml}
            <li class="contact-chat active replies user-chat-white" data-message-id="${message.id}">
            <div class="contact-wrap">
                <div class="chatuser-img">
                    <!-- <span class="contact-status online"></span> -->
                    ${imageHtml}
                </div>
                <div class="chatuser-info">
                    <div class="preview">${message.messages}</div>
                    <div class="chat-date">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
                </div>
            </div>
            </li>`;
}

/**
 * getMessageReceiverHtml
 *
 * this is the message template for the receiver
 *
 * @param message
 * @returns {string}
 */
function getMessageReceiverHtml(message) {
    if (message.rentalphoto != null) {
        var imageHtml = '<img src="' + imagePath + '/' + message.rentalphoto + '" alt=""></img>';
    } else {
        var matches = message.rentalUserName.match(/\b(\w)/g);
        var acronym = matches.join('');
        var imageHtml = '<div id="profileImage" class="rental">' + acronym + '</div>';
    }

    var separatorHtml = '';
    if ($(".mychat-contactList ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }
    return `${separatorHtml}
            <li class="contact-chat active user-chat-white" data-message-id="${message.id}">
            <div class="contact-wrap">
                <div class="chatuser-img">
                    <!-- <span class="contact-status online"></span> -->
                    ${imageHtml}
                </div>
                <div class="chatuser-info">
                    <div class="preview">${message.messages}</div>
                    <div class="chat-date">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
                </div>
            </div>
            </li>`;
}


/**
 * loadLatestMessages
 *
 * this function called on load to fetch the latest messages
 *
 * @param container
 * @param user_id
 */
function loadLatestMessages(container, id) {
    let chat_area = container.find(".mychat-contactList ul");

    chat_area.html("");

    $.ajax({
        url: getMessage,
        data: { user_id: id, _token: $("meta[name='csrf-token']").attr("content") },
        headers: {
            "Authorization": "Bearer " + rental_token,
            "Timezone": timezone
        },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
            if (response.state == 1) {
                if (response.messages.length != 0) {
                    response.messages.map(function (val, index) {
                        $(val).appendTo(chat_area);

                    });
                }
                else {
                    $("#chat_box_" + id + " .mychat-contactList ul").append("<li class='no-user'>Let's start conversation with your buddies.</li>")
                }
            }
        },
        complete: function () {
            $("#chat_box_" + id + " .mychat-contactList").scrollTop($("#chat_box_" + id + " .mychat-contactList ul").outerHeight());
            //hideLoader();
        }
    });
}
/**
 * send
 *
 * this function is the main function of chat as it send the message
 *
 * @param to_user
 * @param message
 */
function send(id, message) {
    let chat_box = $("#chat_box_" + id);
    let chat_area = chat_box.find(".mychat-contactList ul");
    $.ajax({
        url: sendMessage,
        data: { id: id, message: message, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
        },
        complete: function () {
            //hideLoader();
            chat_box.find(".mychat-contactList ul .no-user").remove();
            // /chat_box.find(".btn-chat").prop("disabled", true);
            chat_box.find(".chat_input").val("");
            $("#chat_box_" + id + " .mychat-contactList").animate({ scrollTop: $("#chat_box_" + id + " .mychat-contactList ul").outerHeight() }, 800, 'swing');

        }
    });
}
/**
 * This function called by the send event triggered from pusher to display the message
 *
 * @param message
 */
function displayMessage(message) {
    let alert_sound = document.getElementById("chat-alert-sound");
    let chatBox = $("#chat_box_" + message.pair_user_id);

    if ($("#current_user").val() == message.from_user_id) {
        let messageLine = getMessageSenderHtml(message);
        $("#chat_box_" + message.pair_user_id).find(".mychat-contactList ul").append(messageLine);

    } else {
        //alert_sound.play();
        let messageLine = getMessageReceiverHtml(message);
        // append the message for the receiver user
        $("#chat_box_" + message.pair_user_id).find(".mychat-contactList ul").append(messageLine);

    }
    $("#chat_box_" + message.pair_user_id + " .mychat-contactList").animate({ scrollTop: $("#chat_box_" + message.pair_user_id + " .mychat-contactList ul").outerHeight() }, 800, 'swing');
}

/**
 * fetchOldMessages
 *
 * this function load the old messages if scroll up triggerd
 *
 * @param to_user
 * @param old_message_id
 */
function fetchOldMessages(to_user, old_message_id) {
    let chat_box = $("#chat_box_" + to_user);
    let chat_area = chat_box.find(".chat-area");
    $.ajax({
        url: base_url + "/fetch-old-messages",
        data: { to_user: to_user, old_message_id: old_message_id, _token: $("meta[name='csrf-token']").attr("content") },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            if (chat_area.find(".loader").length == 0) {
                chat_area.prepend(loaderHtml());
            }
        },
        success: function (response) {
        },
        complete: function () {
            chat_area.find(".loader").remove();
        }
    });
}
function displayOldMessages(data) {
    if (data.data.length > 0) {
        data.data.map(function (val, index) {
            $("#chat_box_" + data.to_user).find(".chat-area").prepend(val);
        });
    }
}

function searchForData(value, isLoadMoreMode) {

    /*  if (isLoadMoreMode !== true) {
         clearResult();
     }
  */
    $.ajax({
        url: chatSearch,
        data: { name: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            if (response.users.length != 0) {
                $('.mychat-left .mychat-contactList').html(response.html);
            } else {
                $('.mychat-left .mychat-contactList ul').css('display', 'none');
                $('.mychat-left .mychat-contactList').find('.no-user').remove();
                $('.mychat-left .mychat-contactList').prepend('<span class="no-user">No users found</span>');
            }

        }
    });
}
function converToLocalTimeZone(datetime, pattern) {
    var localTime = moment.utc(datetime).toDate();
    if (pattern == 'time') {
        localTime = moment(localTime).format('hh:mm A');
    } else if (pattern == 'date') {
        localTime = moment(localTime).format('MM/DD/YYYY');
    }
    return localTime;
}
function unreadFlag(value) {
    $.ajax({
        url: unreadUrl,
        data: { id: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            $('#chat_contact_' + value).find('.message-count').remove();
        }
    });
}