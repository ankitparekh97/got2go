var arrows;
if (KTUtil.isRTL()) {
    arrows = {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    }
} else {
    arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
}

var buddyParams = {};
function getBuddyParam(isNew) {
    buddyParams = {
        'buddy_id': (isNew) ? 0 : '',
        'tripboard_id': $('#tripboard_id').val(),
        'buddy_name': (isNew) ? '' : '',
        'isNew': isNew

    }
    return buddyParams;
}


$('.btn-add-new-travelbuddy').click(function(e) {
    e.preventDefault();
    var editable = false;
    $('.edit-input').each(function() {
        if ($(this).val().trim() == '' && this.id != 'buddy_') {
            editable = true;
            $(this).attr('disabled', false);
            $(this).focus()
            $(this).parent('.add-buddie-group').attr('style', 'border:1px solid red !important')

        } else {
            $(this).attr('disabled', true);
            if($(this).parent('.add-buddie-group').attr('id')!= "cloneDiv")
            $(this).parent('.add-buddie-group').attr('style', 'border:none')
        }
    });
    if (!editable) {
        cloneDivs('new');
        $(this).attr('disabled',true)
        var buddyParams = getBuddyParam(true)
        buddyCount++;
    } else {
        return false;
    }
});


    
function cloneDivs(buddyCount) {
    var div = $("#cloneDiv").clone(true, true).prop({
        id: "div_" + buddyCount
    }).appendTo($('#buddyDiv'));
    $(div).find('.edit-input').prop({
        id: "buddy_" + buddyCount,
        name: 'buddy_name',
        value: '',
        placeholder: "Add your travel buddy's name"
    });
    $(div).find('.edit-input').attr('data-id', buddyCount);
    $(div).find('.edit-input').attr('disabled', false);

    $(div).find('.edit-input-pencil').prop({
        id: "edit_" + buddyCount
    });
    $(div).find('.edit-input-pencil').attr('data-id', buddyCount);
    $(div).find('.delete-input-icon').prop({
        id: "delete_" + buddyCount
    });
    $(div).find('.delete-input-icon').attr('data-id', buddyCount);
    div.show('slow');
    $(div).addClass('active');
    $(div).find('.edit-input').focus();
}


function changeIds(buddyCount) {
    var div = $("#div_new");
    div.prop({
        id: "div_" + buddyCount
    });
    $(div).find('.edit-input').prop({
        id: "buddy_" + buddyCount,
        name: 'buddy_name',
    });
    $(div).find('.edit-input').attr('data-id', buddyCount);

    $(div).find('.edit-input-pencil').prop({
        id: "edit_" + buddyCount
    });
    $(div).find('.edit-input-pencil').attr('data-id', buddyCount);
    $(div).find('.delete-input-icon').prop({
        id: "delete_" + buddyCount
    });
    $(div).find('.delete-input-icon').attr('data-id', buddyCount);
}


$('.edit-input-pencil').click(function() {
    var buddyid = $(this).attr('data-id');
    $('#buddy_' + buddyid).attr('disabled', false);
    $('#buddy_' + buddyid).focus();
    if(buddyid != 'new' && $('#buddy_'+buddyid).val().trim() == '') {

        $('#div_new').animate({
            left: '-100px',
            opacity: 'hide', // animate fadeOut
            //width: 'hide'  // animate slideUp
        }, 'slow', 'swing', function() {
            $(this).remove();
        });

    }
});


$('.delete-input-icon').click(function() {
    var buddyid = $(this).attr('data-id');
    if (buddyid == 'new') {
        $('#div_' + buddyid).animate({
            left: '-100px',
            opacity: 'hide', // animate fadeOut
            //width: 'hide'  // animate slideUp
        }, 'slow', 'swing', function() {
            $(this).remove();
        });
    } else {
        deleteBuddy(buddyid)
    }

});


$('.edit-input').bind("change keyup input", function(e) {
    if (e.keyCode === 13) {
        $(this).attr('disabled', true);
        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        if (isSafari) {
            $(this).trigger('focusout');
        }
        $('.btn-add-new-travelbuddy').trigger('click');
    }
    if (this.value.trim() == '') {
        $(this).parent('.add-buddie-group').attr('style', 'border:1px solid red !important')
    } else {
        $(this).parent('.add-buddie-group').attr('style', 'border:none')
    }
});



$('.edit-input').focus(function() {
    var buddyid = $(this).attr('data-id');
    $('#div_' + buddyid).addClass('active');
});


$('.edit-input').focusout(function(e) {
    e.preventDefault();
    var buddyid = $(this).attr('data-id');
    if(buddyid!='new')
    $('#buddy_' + buddyid).attr('disabled', true);
    if (buddyid == 'new' && $('#buddy_' + buddyid).val().trim() != '') {
        buddyParams = getBuddyParam(true);
        buddyParams.buddy_id = buddyid
        buddyParams.buddy_name = $('#buddy_' + buddyid).val()
        var newid = saveBuddy(true);
    } else if (buddyid != '' && $('#buddy_' + buddyid).val().trim() != '') {
        $('#div_' + buddyid).removeClass('active');
        buddyParams = getBuddyParam(false);
        buddyParams.buddy_id = buddyid
        buddyParams.buddy_name = $('#buddy_' + buddyid).val()
        saveBuddy(false);
    }
    else if (buddyid != 'new' && $('#buddy_' + buddyid).val().trim() == '') {
        deleteBuddy(buddyid)
    }
    else if (buddyid == 'new' && $('#buddy_' + buddyid).val().trim() == '') {
        $('#div_' + buddyid).animate({
            left: '-100px',
            opacity: 'hide', // animate fadeOut
        }, 'slow', 'swing', function() {
            $(this).remove();
        });
    }
    $('.btn-add-new-travelbuddy').attr('disabled',false)
    
});

jQuery('#kt_datepicker_3').datepicker({
    minDate: new Date(),
    autoclose: true,
    format: 'mm/dd/yyyy',
    timepicker: false,
    autoclose: true,
    todayHighlight: false,
}).on('changeDate', function(e) {
    var fDate = $('#kt_datepicker_3').val()
    var tDate = $('#kt_datepicker_4').val()
    if (moment(fDate).isSameOrAfter(moment(tDate)) ) {
        var date2 = $('#kt_datepicker_3').datepicker('getDate', '+1d');
        date2.setDate(date2.getDate()+1);
        $('#kt_datepicker_4').datepicker('setDate', date2);
        $('#kt_datepicker_4').datepicker('setStartDate', date2);
    }
    else{
        var date2 = $('#kt_datepicker_3').datepicker('getDate', '+1d');
        date2.setDate(date2.getDate()+1);
        $('#kt_datepicker_4').datepicker('setStartDate', date2);
    }
    var fromDate = moment($('#kt_datepicker_3').val());
    var toDate = moment($('#kt_datepicker_4').val());
    $('#filter_dates_picker').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
    saveRecord()
});

$('#kt_datepicker_4').datepicker({
    autoApply: true,
    format: 'mm/dd/yyyy',
    minDate: new Date(),
    autoclose: true,
}).on('changeDate', function(start) {
    $('.error-span').remove();
    $('.availabelDtsErr').hide();
    $('.guest-info').removeClass('error');
    if(!moment($('#kt_datepicker_3').val()).isSame($('#kt_datepicker_4').val())){
        var minDate = start.format();
        $(this).datepicker('hide');
        var form_date =  new Date($('#kt_datepicker_3').val());
        var to_date = new Date($('#kt_datepicker_4').val());
        var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
        var diffDays = Math.round((to_date-form_date)/oneDay);
    }
    var fromDate = moment($('#kt_datepicker_3').val());
    var toDate = moment($('#kt_datepicker_4').val());
    $('#filter_dates_picker').val(fromDate.format('MMM DD, YYYY') + ' - ' + toDate.format('MMM DD, YYYY'));
    saveRecord();
});

$('#datepicker').daterangepicker({
    buttonClasses: ' btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',
    dateFormat: "mm/dd/YYYY",
    minDate: moment().format('MM/DD/YYYY'),
    showOtherMonths: false,
    autoUpdateInput: false,
    maxDate: moment('12/31/2020').format('MM/DD/YYYY'),

}, function(start, end, label) {

    $('#from_date').val(start.format('MM/DD/YYYY'));
    $('#to_date').val(end.format('MM/DD/YYYY'));

    $('#from_date_label').val(start.format('MM/DD/YYYY'));
    $('#to_date_label').val(end.format('MM/DD/YYYY'));

    saveRecord();
});

if(fromDate == ''){
    var fDate = moment().format('MM/DD/YYYY');
    var tDate = moment().add(2,'days').format('MM/DD/YYYY');
}
else{
    var fDate = moment(fromDate).format('MM/DD/YYYY');
    var tDate = moment(toDate).format('MM/DD/YYYY');
}

$('#kt_datepicker_4').datepicker('setDate',tDate);
$('#kt_datepicker_3').datepicker('setDate',fDate);
$('#kt_datepicker_3').datepicker('setStartDate',fDate);


$('.pencil-edit').click(function() {
    $('#title').attr('disabled', false);
    $('#title').attr('style', 'background-color: #ffffff !important; padding:5px; caret-color:#E40046 !important;');
    $('#title').focus()
    $('.pencil-edit').hide();
});


$('#type_of_date').on('change', function() {
    var label = ($('#type_of_date').is(':checked')) ? 'Fixed Dates' : 'Open Dates';
    $('#type_of_date_label').text(label)
    saveRecord();
});


$('#title').bind("change keyup input", function() {
    if (this.value.trim() == '') {
        $(this).attr('style', 'background-color: #ffffff !important; caret-color:#E40046 !important; padding:5px; border:1px solid red !important');
        $(this).focus();
        $(this).attr('placeholder', 'Enter tripboard name.')
    } else {
        $(this).attr('style', 'background-color: #ffffff !important; caret-color:#E40046 !important; padding:5px;');
    }
});
$('#filter_popup').on('hide.bs.modal', function() {
    var guest =  parseInt(parseInt($('#touchspin_1').val()) + parseInt($('#touchspin_2').val()));
    $('#guests').val(guest);
    $('#number_of_guest').val(guest);
    $('#guests').trigger('change');
    $('#touchspin_2').val(0);
   
});
$('#title').focusout(function() {
    if (this.value.trim() == '') {
        $(this).attr('style', 'background-color: #ffffff !important; caret-color:#E40046 !important; padding:5px; border:1px solid red !important');
        $(this).focus()
    } else {
        $('#title').attr('style', 'background-color: transparent !important');
        $('#title').css({
            'padding': '0px'
        });
        $('#title').attr('disabled', true);
        $('.pencil-edit').show();
        saveRecord();
    }
});

$('#guests').on('change',function(){
    $('#touchspin_1').val($(this).val());
    $('#touchspin_2').val('0');
    $('#number_of_guest').val($(this).val());
    saveRecord()
});

function changeCount(isInc) {
    var count = parseInt($('.no_of_guest').text());
    if (isInc) {
        count++;
    } else {
        count--;
    }
    count = (count < 0) ? 0 : count;
    $('.no_of_guest').text(count)
}

var params = {}

function getparam() {
    params = {
        'tripboard_id': $('#tripboard_id').val(),
        'tripboard_name': $('#title').val(),
        'from_date': $('#kt_datepicker_3').val(),
        'to_date': $('#kt_datepicker_4').val(),
        'type_of_date': ($('#type_of_date').is(':checked')) ? 0 : 1,
        'no_of_guest': $('#guests').val()
    }
    return params;
}


function saveRecord() {
    $.ajax({
        type: "POST",
        url: saverecordtripboard,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer "+rental_token,
        },
        data: getparam(),
        success: function(data) {
            if (data.success = 'true') {
            }
        },
        error: function(xhr, status, error) {
            $('#moreresult').hide()
            $('#noReresultSpan').show()
        }
    });
}


function saveBuddy(isNew) {
    $.ajax({
        type: "POST",
        url: savebuddyUrl,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer "+rental_token,
        },
        data: buddyParams,
        success: function(data) {
            if (data.success = 'true') {
                if (isNew) {
                    $('#div_new').removeClass('active');
                    changeIds(data.data.insertId);
                    changeCount(true);
                } 
            }
        },
        error: function(xhr, status, error) {
            $('#moreresult').hide()
            $('#noReresultSpan').show()
        }
    });
}


function deleteBuddy(buddy_id) {
    $.ajax({
        type: "POST",
        url: deletebuddyUrl,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer "+rental_token,
        },
        data: {
            buddy_id: buddy_id
        },
        success: function(data) {
            if (data.success = 'true') {
                $('#div_' + buddy_id).animate({
                    left: '-100px',
                    opacity: 'hide', // animate fadeOut
                }, 'slow', 'swing', function() {
                    $(this).remove();
                });
                changeCount(false);
            }
        },
        error: function(xhr, status, error) {
            $('#moreresult').hide()
            $('#noReresultSpan').show()
        }
    });
}


$('.guest-middle-col').find('.plus').on('click', function () {
    let $qty = $(this).prev('input');
    let currentVal = parseInt($qty.val());

    let min = 1;
    let max = 30;
    let increment = 1;
    console.log('y');
    if (!isNaN(currentVal) && currentVal < max) {
        $qty.val(currentVal + increment);
    }
    $qty.trigger('change')

    let divMinus = $(this).closest('div').find('.minus');
    let divPlus = $(this);
    if ($qty.val() >= max) {
        divPlus.css({
            'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
        });
    } else if ($qty.val() < max) {
        divPlus.css({
            'cssText': ''
        });
    }

    if ($qty.val() <= min) {
        divMinus.css({
            'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
        });
    } else if ($qty.val() > min) {
        divMinus.css({
            'cssText': ''
        });
    }

});


$('.guest-middle-col').find('.minus').on('click', function () {
    let $qty = $('.minus').next('input');
    let currentVal = parseInt($qty.val());
    let min = 1;
    let max =30;
    let increment = 1;
    if (!isNaN(currentVal) && currentVal > min) {
        $qty.val(currentVal - increment);
    }
    $qty.trigger('change')
    let divPlus = $(this).closest('div').find('.plus');
    let divMinus = $(this);
    if ($qty.val() >= max) {
        divPlus.css({
            'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
        });
    } else if ($qty.val() < max) {
        divPlus.css({
            'cssText': ''
        });
    }
    if ($qty.val() <= min) {
        divMinus.css({
            'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
        });
    } else if ($qty.val() > min) {
        divMinus.css({
            'cssText': ''
        });
    }
});

$(".potential-listing-slider.owl-carousel").owlCarousel({
    dots: false,
    nav: true,
    lazyLoad: true,
    loop: false,
    slideBy: 3,
    fade: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        991: {
            items: 3
        },
        1700: {
            items: 4
        },
        2100: {
            items: 5
        }
    }
});

function propertyVote(obj) {
    var propertyId = $(obj).attr('data-propertyId');
    var tripboardId = $(obj).attr('data-tripboardid');
    $.ajax({
        type: "POST",
        url: propertyVote_url,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer "+rental_token,
        },
        data: {
            property_id: propertyId,
            tripboard_id: tripboardId
        },
        success: function(data) {
            if (data.success == true) {
                var textVote = ' Vote'
                if (data.voteData !== null) {
                    if (data.voteData.votes == 1) {
                        textVote = ' Vote';
                        $("#votedclass_" + data.voteData.property_id).addClass("voted-text");
                        $("#votedtag_" + data.voteData.property_id).addClass("voted");
                        $('#votedclass_' + data.voteData.property_id).text('Voted!');
                    }
                }
                $('#voted_' + +propertyId).text(data.totalVote.totalVotes + textVote);
                if (data.voteData == null) {
                    $("#votedclass_" + propertyId).removeClass("voted-text");
                    $("#votedtag_" + propertyId).removeClass("voted");
                    $('#votedclass_' + propertyId).text('');
                }
            }
        },
        error: function(xhr, status, error) {
            Swal.fire({
                icon: "error",
                title: data.message,
                showConfirmButton: false,
                timer: 3000
            }).then(() => {
                //window.location.reload();
            });
        }


    });
}

$('.deal-delete').click(function() {
    var listing = $(this).attr('data-listing');
    var  property_id = $(this).attr('data-id');
    var  tripboardId = $(this).attr('data-tripboardId');
    $('#listing').text('"' + listing + '"');
    $('#property_id').val(property_id);
    $('#tripboardid').val(tripboardId);
});

function deleteCancel() {
    $("#deletedeal").modal('hide');
}

function DeletePropertyFromTripBoard(obj) {
    var propertyId = $('#property_id').val();
    var tripboardId = $('#tripboardid').val();
    $.ajax({
        type: "POST",
        url: DeletePropertyFromTripBoardUrl,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer "+rental_token,
        },
        data: {
            property_id: propertyId,
            tripboard_id: tripboardId
        },
        success: function(data) {
            $("#deletedeal").modal('hide');
            $("#sliderbox_"+propertyId).remove();
            if (data.success == true) {
                    Swal.fire({
                    icon: "success",
                    title: data.message,
                    showConfirmButton: false,
                    timer: 3000
                }).then(() => {
                    // window.onbeforeunload = null;
                    window.location.reload();
                });
            }
        },
        error: function(xhr, status, error) {
            Swal.fire({
                icon: "error",
                title: data.message,
                showConfirmButton: false,
                timer: 3000
            }).then(() => {
                //window.location.reload();
            });
        }


    });
}
$(document).ready(function() {
    $('#pageno').val(0)
    $('.gobutton').click(function(){
       $( "#searchForm" ).submit();
    });
});


$('#searchForm').validate({
    rules: {
        location_detail: {
            address_selection_required : true,
            required:function(element) {
                if(element.value == '' && $("#hdn_search_text").val() != ''){
                    return false;
                }
                else if(element.value == '' && $("#hdn_search_text").val() == ''){
                    return true;
                }
                else if(element.value == ''){
                    return true;
                }
                    return false;
            }
        },
        modal_location_detail:{                   
            address_selection_required : true,
            required:function(element) {
                if(element.value == '' && $("#hdn_search_text").val() != ''){
                    return false;
                }
                else if(element.value == '' && $("#hdn_search_text").val() == ''){
                    return true;
                }
                else if(element.value == ''){
                    return true;
                }
                return false;
            }
        },
        number_of_guest:{
            required:true
        }
    },
    messages: {
        location_detail: {
         address_selection_required: "Please type in a city.",
         required:"Please type in a city.",
        },
        modal_location_detail: {
         address_selection_required: "Please type in a city.",
         required:"Please type in a city.",
        },
        number_of_guest:{
            required: 'Please select a guest.'
        },
    }
});
jQuery.validator.addMethod("address_selection_required", function(value, element) {
     if(element.value == '' && $("#hdn_search_text").val() != ''){
         return true;
     }
     else if(element.value == '' && $("#hdn_search_text").val() == ''){
         return false;
     }else if(element.value != '' && $(".address_selected_from_drop").val() != element.value){
         return false;
     }
     else if(element.value == ''){
         return false;
     }
     return true;
 }, "");
 var $temp = $("<input>");
 function shareLink(id) {
    $('#sharelinkMDL').modal('show');
    $("body").append($temp);
    str = shareLinkURL+'?id='+id;
    $temp.val(str).select();
    document.execCommand("copy");
    $temp.remove();
}