$(function () {
    $(document).ready(function () {
        let pusher = new Pusher($("#pusher_app_key").val(), {
            cluster: $("#pusher_cluster").val(),
            encrypted: true
        });

        let channel = pusher.subscribe('chat');
        searchForData();
        $(document).on('click', '.chat-back-button', function (e) {
            $('.chat-contact-mobile').css('display', 'block');
            $('.chat-detail-mobile').css('display', 'none');
        });
        // on click on any chat btn render the chat box

        $(document).on('click', '.chat-toggle', function (e) {
            checkScreenSize();
            e.preventDefault();

            let ele = $(this);

            let id = ele.attr("data-id");

            let username = ele.attr("data-user");

            let userimage = ele.attr("data-image");

            let toUserId = ele.attr('data-touserid');

            let userType = ele.attr('data-type');

            cloneChatBox(id, username, userimage, toUserId, userType, function () {
                $('.contact-chat').removeClass('active');
                unreadFlag(id);
                ele.css('background-color', '');
                ele.addClass('active');
                let chatBox = $("#chat_box_" + id);
                loadLatestMessages(chatBox, id);
            });
        });

        // on close chat close the chat box but don't remove it from the dom
        $(".close-chat").on("click", function (e) {

            $(this).parents("div.chat-opened").removeClass("chat-opened").slideUp("fast");
        });

        // on change chat input text toggle the chat btn disabled state
        $(".chat_input").on("change keyup", function (e) {
            if ($(this).val() != "") {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", false);
            } else {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", true);
            }
        });
        $('.chat_input').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                var button = $(this).parent().next().children();
                send(button.attr('data-id'), button.attr('data-to-user'), $("#chat_box_" + button.attr('data-id')).find(".chat_input").val());
            }
        });
        // on click the btn send the message
        $(".btn-chat").on("click", function (e) {
            send($(this).attr('data-id'), $(this).attr('data-to-user'), $("#chat_box_" + $(this).attr('data-id')).find(".chat_input").val());
        });
        // listen for the send event, this event will be triggered on click the send btn
        channel.bind('send', function (data) {
            displayMessage(data.data);
        });

        let lastScrollTop = 0;
        $(".chat-area").on("scroll", function (e) {
            let st = $(this).scrollTop();
            if (st < lastScrollTop) {
                fetchOldMessages($(this).parents(".chat-opened").find("#to_user_id").val(), $(this).find(".msg_container:first-child").attr("data-message-id"));
            }
            lastScrollTop = st;
        });
        // listen for the oldMsgs event, this event will be triggered on scroll top
        channel.bind('oldMsgs', function (data) {
            displayOldMessages(data);
        });

        //On click search show conatct list
        $("#search").keyup(function (e) {
            if (e.keyCode == 13) {
                /*  alert(2); */
                e.preventDefault();
                return false;
            }
            $('.searchclose').css('display', 'inline-block');
            var val = this.value;
            val = val.replace(/^\s|\s $/, "");

            if (val !== "") {
                searchForData(val);
            } else {
                // clearResult();
            }
        });

        //close search data
        $(document).on('click', '.searchclose', function (e) {
            e.preventDefault();
            searchForData();
            $('.searchclose').css('display', 'none');
            $("#search").val('');
        });
    });

});


/**
 * loaderHtml
 *
 * @returns {string}
 */
function loaderHtml() {
    return '<i class="glyphicon glyphicon-refresh loader"></i>';
}

/**
 * getMessageSenderHtml
 *
 * this is the message template for the sender
 *
 * @param message
 * @returns {string}
 */
function getMessageSenderHtml(message) {
    if (message.rentalphoto != null) {
        var imageHtml = '<img src="' + imagesendPath + '/' + message.rentalphoto + '" alt=""></img>';
    } else {
        var matches = message.rentalUserName.match(/\b(\w)/g);
        var acronym = matches.join('');
        var imageHtml = '<div id="profileImage" class="rental">' + acronym + '</div>';
    }

    var separatorHtml = '';
    if ($(".mychat-contactList ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }
    return `${separatorHtml}
            <li class="contact-chat active replies" data-message-id="${message.id}">
            <div class="contact-wrap">
                <div class="chatuser-img">
                    <!-- <span class="contact-status online"></span> -->
                    ${imageHtml}
                </div>
                <div class="chatuser-info">
                    <div class="preview">${message.messages}</div>
                    <div class="chat-date">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
                </div>
            </div>
            </li>`;
}

/**
 * getMessageReceiverHtml
 *
 * this is the message template for the receiver
 *
 * @param message
 * @returns {string}
 */
function getMessageReceiverHtml(message) {

    if (message.type == 'admin') {
        var imageHtml = '<img src="' + adminLogo + '" alt=""></img>';
    } else {
        if (message.ownerphoto != null) {
            var imageHtml = '<img src="' + imagePath + '/' + message.ownerphoto + '" alt=""></img>';
        } else {
            var matches = message.ownerUserName.match(/\b(\w)/g);
            var acronym = matches.join('');
            var imageHtml = '<div id="profileImage" class="owner">' + acronym + '</div>';
        }
    }

    var separatorHtml = '';
    if ($(".mychat-contactList ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }
    return `${separatorHtml}
            <li class="contact-chat active" data-message-id="${message.id}">
            <div class="contact-wrap">
                <div class="chatuser-img">
                    <!-- <span class="contact-status online"></span> -->
                    ${imageHtml}
                </div>
                <div class="chatuser-info">
                    <div class="preview">${message.messages}</div>
                    <div class="chat-date">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
                </div>
            </div>
            </li>`;
}


/**
 * cloneChatBox
 *
 * this helper function make a copy of the html chat box depending on receiver user
 * then append it to 'chat-overlay' div
 *
 * @param user_id
 * @param username
 * @param callback
 */
function cloneChatBox(id, username, userimage, toUserId, userType, callback) {

    if ($("#chat_box_" + id).length == 0) {
        //$("#chat-overlay").html("");
        let cloned = $("#chat_box").clone(true);

        // change cloned box id
        cloned.attr("id", "chat_box_" + id);

        cloned.find(".mychatbox-header .chatbox-user .chatbox-name").text(username);

        cloned.find(".chat-action .btn-chat").attr("data-to-user", toUserId);

        cloned.find(".chat-action .btn-chat").attr("data-id", id);

        cloned.find("#id").val(id);
        cloned.find("#to_user_id").val(toUserId);

        if (userType == 'admin') {
            var imageHtmL = '<img src="' + adminLogo + '" alt="">';
            cloned.find(".mychatbox-header .chatbox-user .chatbox-name").addClass('admin_name');
            cloned.find('.chatcontent-header .content-link').hide();
            cloned.find('.mychatlist-footer').hide();
        }
        else {
            if (userimage != '') {
                var imageHtmL = '<img src="' + imagePath + '/' + userimage + '" alt="">';
            } else {
                var matches = username.match(/\b(\w)/g);
                var acronym = matches.join('');
                var imageHtmL = '<div id="profileImage" class="owner">' + acronym + '</div>';
            }
        }

        cloned.find('.mychatbox-header .chatbox-userimg ').append(imageHtmL);
        cloned.css({ 'display': 'block' });

        $("#chat-overlay").html("");
        $("#chat-overlay").append(cloned);
    }

    callback();
}

/**
 * loadLatestMessages
 *
 * this function called on load to fetch the latest messages
 *
 * @param container
 * @param user_id
 */
function loadLatestMessages(container, id) {
    let chat_area = container.find(".mychatbox-body #kt_tab_pane_1 .mychat-contactList ul");

    chat_area.html("");

    $.ajax({
        url: getMessage,
        data: { user_id: id, _token: $("meta[name='csrf-token']").attr("content") },
        headers: {
            "Authorization": "Bearer {{session()->get('rental_token')}}",
            "Timezone": timezone
        },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            showLoader();
        },
        success: function (response) {
            if (response.state == 1) {
                response.messages.map(function (val, index) {
                    $(val).appendTo(chat_area);

                });
            } else {
                Swal.fire({
                    icon: "error",
                    title: response.error,
                    showConfirmButton: false,
                    width: 500,
                    timer: 3000
                });
            }
        },
        complete: function () {
            $("#chat_box_" + id + " .mychatlist-wrapper").scrollTop($("#chat_box_" + id + " .mychatlist-wrapper .mychat-contactList").outerHeight());
            hideLoader();
        }
    });
}
/**
 * send
 *
 * this function is the main function of chat as it send the message
 *
 * @param to_user
 * @param message
 */
function send(id, toUser, message) {
    let chat_box = $("#chat_box_" + id);
    let chat_area = chat_box.find(".mychatbox-body .mychat-contactList ul");
    $.ajax({
        url: sendMessage,
        data: { id: id, to_user: toUser, message: message, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (response) {
            if (response.success == false) {
                Swal.fire({
                    icon: "error",
                    title: response.error,
                    showConfirmButton: false,
                    width: 500,
                    timer: 3000
                });
            }
        },
        complete: function () {
            chat_box.find(".chat_input").val("");
            $("#chat_box_" + id + " .mychatlist-wrapper").animate({ scrollTop: $("#chat_box_" + id + " .mychatlist-wrapper .mychat-contactList").outerHeight() }, 800, 'swing');

        }
    });
}
/**
 * This function called by the send event triggered from pusher to display the message
 *
 * @param message
 */
function displayMessage(message) {
    let alert_sound = document.getElementById("chat-alert-sound");
    let chatBox = $("#chat_box_" + message.pair_user_id);
    if ($('#chat_contact_' + message.pair_user_id).length == 1) {
        $('#chat_contact_' + message.pair_user_id).find('.preview').text(message.messages);
        $('#chat_contact_' + message.pair_user_id).find('.chat-date').text(converToLocalTimeZone(message.dateTimeStr, 'date'));
    }
    if ($("#current_user").val() == message.from_user_id && message.from_user_type == 'rental') {
        $('#chat_contact_' + message.pair_user_id).prependTo($('#chat_contact_' + message.pair_user_id).parent());
        let messageLine = getMessageSenderHtml(message);
        $("#chat_box_" + message.pair_user_id).find(".mychatbox-body .mychat-contactList ul").append(messageLine);

    } else if ($("#current_user").val() == message.to_user_id && message.to_user_type == 'rental') {
        //alert_sound.play();
        if (message.type == 'admin') {
            message.ownerUserName = message.adminUserName;
        }
        if ($("#chat_contact_" + message.pair_user_id).length == 0) {
            let chatCloned = $(".mychat-contactList ul li:first").clone(true);
            chatCloned.attr('id', "#chat_contact_" + message.pair_user_id);
            chatCloned.attr('data-id', message.pair_user_id);
            chatCloned.attr('data-user', message.ownerUserName);
            chatCloned.attr('data-image', message.ownerphoto);
            chatCloned.attr('data-touserid', message.from_user_id);
            chatCloned.attr('data-type', message.type);
            chatCloned.find('.name').text(message.ownerUserName);
            chatCloned.find('.preview').text(message.messages);
            chatCloned.find('.message-block .message-count').remove();
            chatCloned.find('.message-block').append('<span class="message-count">' + message.message_count + '</span>');
            chatCloned.find('.chat-date').text(converToLocalTimeZone(message.dateTimeStr, 'date'));
            chatCloned.css("background-color", "#00800024");
            if (message.type == 'admin') {
                var imageHtml = '<img src="' + adminLogo + '" alt=""></img>';
            } else {
                if (message.ownerphoto != null) {
                    var imageHtml = '<img src="' + imagePath + '/' + message.ownerphoto + '" alt=""></img>';
                } else {
                    var matches = message.ownerUserName.match(/\b(\w)/g);
                    var acronym = matches.join('');
                    var imageHtml = '<div id="profileImage" class="owner">' + acronym + '</div>';
                }
            }
            chatCloned.find('.meta-left').html(imageHtml);
            $(".mychat-contactList ul").prepend(chatCloned);

        } else {
            if ($('#chat_contact_' + message.pair_user_id).hasClass('active')) {
                unreadFlag(message.pair_user_id);
            } else {
                $('#chat_contact_' + message.pair_user_id).find('.message-block .message-count').remove();
                $('#chat_contact_' + message.pair_user_id).css("background-color", "#00800024");
                $('#chat_contact_' + message.pair_user_id).find('.message-block').append('<span class="message-count">' + message.message_count + '</span>');
            }
            $('#chat_contact_' + message.pair_user_id).prependTo($('#chat_contact_' + message.pair_user_id).parent());
            let messageLine = getMessageReceiverHtml(message);
            // append the message for the receiver user
            $("#chat_box_" + message.pair_user_id).find(".mychatbox-body .mychat-contactList ul").append(messageLine);

        }
        $("#chat_box_" + message.pair_user_id + " .mychatlist-wrapper").animate({ scrollTop: $("#chat_box_" + message.pair_user_id + " .mychatlist-wrapper .mychat-contactList").outerHeight() }, 800, 'swing');
    }
}
/**
 * fetchOldMessages
 *
 * this function load the old messages if scroll up triggerd
 *
 * @param to_user
 * @param old_message_id
 */
function fetchOldMessages(to_user, old_message_id) {
    let chat_box = $("#chat_box_" + to_user);
    let chat_area = chat_box.find(".chat-area");
    $.ajax({
        url: base_url + "/fetch-old-messages",
        data: { to_user: to_user, old_message_id: old_message_id, _token: $("meta[name='csrf-token']").attr("content") },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            if (chat_area.find(".loader").length == 0) {
                chat_area.prepend(loaderHtml());
            }
        },
        success: function (response) {
        },
        complete: function () {
            chat_area.find(".loader").remove();
        }
    });
}
function displayOldMessages(data) {
    if (data.data.length > 0) {
        data.data.map(function (val, index) {
            $("#chat_box_" + data.to_user).find(".chat-area").prepend(val);
        });
    }
}

function searchForData(value, isLoadMoreMode) {

    $.ajax({
        url: chatSearch,
        data: { name: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            if (response.users.length != 0) {
                $('.mychat-left .mychat-contactList').html(response.html);

                let ele = $('.mychat-left .mychat-contactList ul li:first');

                let id = ele.attr("data-id");

                let username = ele.attr("data-user");

                let userimage = ele.attr("data-image");

                let toUserId = ele.attr('data-touserid');

                let userType = ele.attr('data-type');

                cloneChatBox(id, username, userimage, toUserId, userType, function () {
                    $('.contact-chat').removeClass('active');
                    unreadFlag(id);
                    ele.css('background-color', '');
                    ele.addClass('active');
                    let chatBox = $("#chat_box_" + id);
                    loadLatestMessages(chatBox, id);
                });
            } else {
                $('.mychat-left .mychat-contactList ul').css('display', 'none');
                $('.mychat-left .mychat-contactList').find('.no-user').remove();
                $('.mychat-left .mychat-contactList').prepend('<span class="no-user">No users found</span>');
            }

        }
    });
}
function converToLocalTimeZone(datetime, pattern) {
    var localTime = moment.utc(datetime).toDate();
    if (pattern == 'time') {
        localTime = moment(localTime).format('hh:mm A');
    } else if (pattern == 'date') {
        localTime = moment(localTime).format('MM/DD/YYYY');
    }
    return localTime;
}
function unreadFlag(value) {
    $.ajax({
        url: unreadUrl,
        data: { id: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            if (response.success == true) {
                $('#chat_contact_' + value).find('.message-count').remove();
            } else {
                Swal.fire({
                    icon: "error",
                    title: response.error,
                    showConfirmButton: false,
                    width: 500,
                    timer: 3000
                });
            }

        }
    });
}
$(window).on("resize", function (e) {
    checkReSize();
});
function checkScreenSize() {
    var newWindowWidth = $(window).width();
    if (newWindowWidth < 767) {
        $('.chat-contact-mobile').css('display', 'none');
        $('.chat-detail-mobile').css('display', 'block');
    }
}
function checkReSize() {
    var newWindowWidth = $(window).width();
    if (newWindowWidth > 767) {
        $('.chat-contact-mobile').css('display', 'block');
        $('.chat-detail-mobile').css('display', 'block');
    }
}
