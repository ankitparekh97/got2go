// Start Revenue from Reservations
$( "#reservation" ).change(function() {
    var filterOptions = $('#reservation').val();
    revenueReservations(filterOptions);
});
var revenueReservationChart = $('#revenueReservationId').get(0).getContext('2d');
var revenueReservations = (filterLevel) => new Chart(revenueReservationChart, {
    type: 'line',
    data: {
    labels: xAxisData,
    datasets: [
        {
            label: filterLevel? filterLevel.toUpperCase(): 'Monthly'.toUpperCase(),            
            data: yAxisData,
            lineTension: 0,
            fill: false,
            borderColor: 'orange',           
            border: [5, 5],
            pointBorderColor: 'orange',            
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2,
            pointStyle: 'rectRounded',
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            backgroundColor: "#FFE100",
            pointBackgroundColor: '#E60A4C'
                        
        }]
    },
    options: { 
        legend: { 
            display: false 
        }
      }
});
revenueReservations(); // default call
// End Revenue from Reservations
