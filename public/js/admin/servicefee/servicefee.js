//service fee formatting
const isNumericInput = (event) => {
    const key = event.keyCode;
    return ((key >= 48 && key <= 57) || // Allow number line
        (key >= 96 && key <= 105) || // Allow number pad
        (key === 110 || key === 190) // Allow dot
    );
};
const isModifierKey = (event) => {
    const key = event.keyCode;
    return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
        (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
        (key > 36 && key < 41) || // Allow left, up, right, down
        (
            // Allow Ctrl/Command + A,C,V,X,Z
            (event.ctrlKey === true || event.metaKey === true) &&
            (key === 65 || key === 67 || key === 86 || key === 88 || key === 90 || key === 110 || key === 190)
        )
};
const enforceFormat = (event) => {
    // Input must be of a valid number format or a modifier key, and not longer than ten digits
    if (!isNumericInput(event) && !isModifierKey(event)) {
        event.preventDefault();
    }
};
const formatToServiceFee = (event) => {
    if (isModifierKey(event)) { return; }
    // I am lazy and don't like to type things more than once
    const target = event.target;
    const input = event.target.value.replace(/\D/g, '').substring(0, 6); // First five digits of input only
    var chkbox = $("input[name='ratio']:checked").val();
    if (chkbox == 1) {
        const zip = input.substring(0, 3);
        const middle = input.substring(3, 6);
        if (input.length > 5) {
            target.value = `${zip}.${middle}`;
        } else if (input.length > 3) {
            target.value = `${zip}.${middle}`;
        } else if (input.length > 0) {
            target.value = `${zip}`;
        }
    }

};
const inputElement = document.getElementById('servicefee');
inputElement.addEventListener('keydown', enforceFormat);
inputElement.addEventListener('keyup', formatToServiceFee);
function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)e
    if (number.length > 1 && charCode == 46) {
        return false;
    }
    //get the carat positione
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");

    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
        return false;
    }
    return true;
}
function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}
function hidedollor() {
    $('#defaultdolor').hide();
    $('#defaultper').hide();
    $('#showdollorid').hide();
    $('#hidepercentage').show();
    var serviceFeePervals = $('#serviceFeePercentageValues').val();
    if (serviceFeePervals == 2) {
        var service_vals = $('#serviceFeeValues').val();
        $('#servicefee').val(service_vals);
    } else {
        $('#servicefee').val('');
    }
}
function showdollor() {
    $('#hidepercentage').hide();
    $('#defaultper').hide();
    $('#showdollorid').show();
    $('#hidepercentage').hide();
    var serviceFeePervals = $('#serviceFeePercentageValues').val();
    if (serviceFeePervals == 1) {
        var service_vals = $('#serviceFeeValues').val();
        $('#servicefee').val(service_vals);
    } else {
        $('#servicefee').val('');
    }
}
function saveServeiceFee(obj) {
    $('#showerrormsg').text('');
    $('#showmsg').text('');

    $("#showdollorid").val();
    $("#hidepercentage").val();
    var servicefee = $("#servicefee").val();
    var amount_charges = $("input[name='ratio']:checked").val();
    var id = $("#service_id").val();
    var numberWithTdecimal = /^\d{0,6}(\.\d{1,2})?$/;
    var decimalOnly = /^\d{0,2}(\.\d{1,2})?$/;

    var serviceFeeWithdecimal = Number.parseFloat(servicefee).toFixed(2);

    if (servicefee == '') {
        $('#error').show().delay(3000).fadeOut();
        $('#showerrormsg').text('Please enter service fee').show().delay(3000).fadeOut();
        $("#saveFee").modal('hide');
    } else if (amount_charges == 1 && !numberWithTdecimal.test(serviceFeeWithdecimal)) {
        $('#discountPercentageError').css("display", "none");
        $("#servicefee").val('');
        $("#saveFee").modal('hide');
    } else if (amount_charges == 2 && servicefee > 99.99) {
        $('#discountPercentageError').css("display", "block");
        $("#saveFee").modal('hide');
    } else {
        $('#error').hide();
        $('#showerrormsg').hide();
        $('#discountPercentageError').css("display", "none");
        // save service fee
        $.ajax({
            type: "POST",
            url: serviceFeeSave,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('admin_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                'servicefee': serviceFeeWithdecimal + ", " + amount_charges,
                'id': id
            },
            success: function (data) {
                if (data.success = 'true') {
                    $('#serviceFeeValues').val(data.servicefee);
                    $('#serviceFeePercentageValues').val(data.serviceId);
                    $('#servicefee').val(data.servicefee);
                    $('#service_id').val(data.autoid);
                    $('#success').show().delay(3000).fadeOut();
                    $('#showmsg').html(data.message).show().delay(3000).fadeOut();
                    $("#saveFee").modal('hide');
                }
            },
            error: function (xhr, status, error) {
                $('#success').hide();
                $('#error').show().delay(3000).fadeOut();
                $('#showmsg').html(data.message).show().delay(3000).fadeOut();
                $("#saveFee").modal('hide');
            }
        });
    }
}
function saveservicefee(obj) {
    $('#showmsg').text('');
    $('#showerrormsg').text('');
    $('#showerrormsg').hide();
    $('#showmsg').hide();
    $("#showdollorid").val();
    $("#hidepercentage").val();
    $("#servicefee").val();
    $("input[name='ratio']:checked").val();
    $("#service_id").val();
}
