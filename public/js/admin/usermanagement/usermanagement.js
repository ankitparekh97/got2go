function deactivate(id, propertyId) {
    $("#user_id").val(id);
    $("#propertyId").val(propertyId);
}

function cancel(id) {
    $("#rental_user_id").val(id);
}


$("#btnClosePopup").click(function () {
    $('#status_show').text('');
});


function active(id) {
    $("#rental_user_id").val(id);
    $("#subscription").val('active');
    $('#status_show').text('active');
}

function deactivateOwner() {
    var user_id = $("#user_id").val();
    var propertyid = $("#propertyId").val();
    $.ajax({
        type: "POST",
        url: userDeactivate,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': user_id,
            'propertyid': propertyid,
            'status': 'decline'
        },
        success: function (data) {
            if (data.success = 'true') {
                $('#showmsg').text(data.message).show().delay(1000).fadeOut();
                $("#deactivate").modal('hide');
                $("#userslist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showerrormsg').text(data.error).show().delay(1000).fadeOut();
        }
    });
}

function cancelSubscription() {
    var rental_user_id = $("#rental_user_id").val();
    // cancel subscription
    $.ajax({
        type: "POST",
        url: subscriptionCancel,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'id': rental_user_id
        },
        success: function (data) {
            if (data.success = 'true') {
                $('#showmsg').text(data.message).show().delay(1000).fadeOut();
                $("#cancel").modal('hide');
                $("#tripperslist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showcancelerrormsg').text(data.error).show().delay(1000).fadeOut();
        }
    });

}
function activate(user_id,property_id){
    $("#active_user_id").val(user_id);
    $("#active_propertyId").val(property_id);
}
function activateUser(obj) {
    var user_id = $("#active_user_id").val();
    var propertyid = $("#active_propertyId").val();
    $.ajax({
        type: "POST",
        url: userDeactivate,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': user_id,
            'propertyid': propertyid,
            'status': 'approved'
        },
        success: function (data) {
            if (data.success = 'true') {
                $('#showmsg').text(data.message).show().delay(1000).fadeOut();
                $("#activate").modal('hide');
                $("#userslist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showerrormsg').text(data.error).show().delay(1000).fadeOut();
        }
    });
    
}

function format(details) {
    var birthdate = new Date(details.birthdate);
    var imgUrl = defaultUserImage;
    if (details.photo) {
        imgUrl = userImage + '/' + details.photo;
    }
    if (details.property > 0) {
        productlist(details.id);
    }
    var phone = details.phone;
    var phoneUS = '';
    if (phone) {
        phoneUS = ' | ' + phone.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1) $2-$3");
    }
    return `<div class="details card shadow mb-4"><div class="card-body">
        <div class="row ml-0 mr-0 w-100">
                    <div class="col-xl-4 property-user-wrapper">
                            <div class="property-user-img mb-3">
                                <img src="${imgUrl}" alt=""/>
                            </div>
                            <div class="property-user-info">
                                <h5>${details.first_name.toUpperCase() + ' ' + (details.last_name ? details.last_name.toUpperCase() : "")}</h5>
                                <div class="property-user-email"><a href="mailto:${details.email ? details.email : ""}">${details.email ? details.email : ""}</a><span class="detail-phone">${details.phone ? phoneUS : ""}</span></div>                                
                                <div class="property-label">${details.birthdate ? 'Member since ' + birthdate.getFullYear() : ""} </div>
                                <div class="property-label">${details.city ? details.city : ""}${details.state ? ', ' + details.state : ""}</div>
                            </div>
                        </div>
                        <div class="col-xl-8 pl-1 pr-1">                                 
                        <h5 class="mb-4 text-uppercase"><b>${details.property > 1 ? details.property + " Properties" : details.property + " Property"} </b></h5>
                            <div class="row ml-0 mr-0 w-100" id="propertyListShow"></div>
                        </div>
                    </div>
                </div>
            </div>`;
}
userslist(); // call default list
function userslist() {
    $('#userslist').dataTable(
        {
            serverSide: false,
            bLengthChange: true,
            bProcessing: false,
            destroy: true,
            iDisplayLength: 10,
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'All']
            ],
            aaSorting: [[2, 'desc']],
            bInfo: false,
            dom: 'lftip',
            "columnDefs": [{
                "targets": 3,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (cellData == 'Activated') {
                        $(td).addClass('custom-completed-text');
                    } else if (cellData == 'Deactivated') {
                        $(td).addClass('custom-refunded-text');
                    } else {
                        $(td).addClass('custom-awaiting-text')
                    }
                }
            }],
            "language": {
                emptyTable: "No data available in Users",
                searchPlaceholder: "Search owner",
                search: "",
                lengthMenu: "Number per page _MENU_",
                paginate: {
                    next: '<i class="fas fa-angle-right"></i>',
                    previous: '<i class="fas fa-angle-left"></i>'
                }
            },
            ajax: {
                url: userListing,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#loader_spin').show();
                },
                complete: function () {
                    $('#loader_spin').hide();
                },
                error: function () {
                    console.log('Error:');
                }
            },
            aoColumns: [
                {
                    "mData": "name",
                    "sWidth": "15rem",
                    "orderable": true
                },
                {
                    "mData": "email",
                    "sWidth": "20rem",
                    "orderable": true
                },
                {
                    "mData": "property",
                    "sWidth": "15rem",
                    "orderable": true,
                    "className": "text-center",
                },
                {
                    "mData": "status",
                    "sWidth": "20rem",
                    "orderable": true
                },
                {
                    "mData": "action",
                    "sWidth": "12rem",
                    "orderable": false,
                    "className": "text-center",
                }
            ],
        });
}
$("#searchbox").keyup(function () {
    $('#userslist').dataTable().fnFilter(this.value);
});

var table = $('#userslist').DataTable();
// Array to track the ids of the details displayed rows
var detailRows = [];
$('#userslist tbody').on('click', 'tr td #view_profile', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    var idx = $.inArray(tr.attr('id'), detailRows);
    if (row.child.isShown()) {
        tr.removeClass('details');
        row.child.hide();

        // Remove from the 'open' array
        detailRows.splice(idx, 1);
    } else {
        tr.addClass('details');
        row.child(format(row.data())).show();

        // Add to the 'open' array
        if (idx === -1) {
            detailRows.push(tr.attr('id'));
        }
    }
});
// On each draw, loop over the `detailRows` array and show any child rows
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function productlist(id) {
    $.ajax({
        type: "POST",
        url: propertyList,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': id
        },
        success: function (data) {
            if (data.success = 'true') {
                var datashow = [];
                data.propertylist.forEach((dataDetails) =>
                    datashow.push(`<div class="col-lg-3 col-md-6 mb-3 property-list-wrapper pl-1 pr-1">
                                            <div class="propertylist-image">
                                                <img src="${propertyImage}/${dataDetails.cover_photo}" alt="image">
                                            </div>
                                            <div class="propertylist-info">
                                                <div class="property-title">${dataDetails.title}</div>
                                                <div class="property-link"><a target="_blank" href="${propertyUrl}/${dataDetails.id}">View Details <i class="got got-arrow-right"></i> </a></div>
                                            </div>
                                        </div>`)
                );
                $('#propertyListShow').html(datashow).show();
            }
        },
        error: function (xhr, status, error) {
            $('#showerrormsgsub').text(data.message).delay(1000).fadeOut();
        }
    });
}