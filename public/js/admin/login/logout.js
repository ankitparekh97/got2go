function adminLogout(url) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: url,
        type: 'GET',
        data: { _token: CSRF_TOKEN },
        dataType: 'JSON',
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
        },
        success: function (data) {
            if (data.success) {
                window.location = data.redirect_url;
            }
        }
    });
}