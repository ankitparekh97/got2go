$(document).ready(function () {
    $('#forgotpassword').validate({
        ignore: ".ignore",
        rules: {
        email: {
            required: true,
            email:true
        }        
    }
});
$('#forgotpassword').on('submit', function(e) {
    e.preventDefault();
    let $form = $(this);
    if(!$form.valid()) return false;
    $.ajax({
        type: "POST",
        url: forgotpassword,
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(msg) {
            if(msg.success == true) {                   
                $(".alert-light-danger").hide();                   
                setTimeout(function() {
                    $(".alert-success").show();
                    $('#showlogin').show();
                    $(".alert-text").html('');
                    $.each( msg.message, function( key, value ) {
                        $(".alert-text").append(value);
                    });
                }, 1000);                
            } else {  
                $(".alert-success").hide();              
                setTimeout(function() {
                    $(".alert-light-danger").show();                    
                    $(".alert-text").html('');
                    $.each( msg.error, function( key, value ) {
                        $(".alert-text").append(value);
                    });
                }, 1000);
            }
        }
    });
});
});