$(document).ready(function () {
    $('#adminlogin').validate({
            ignore: ".ignore",
            rules: {
            email: {
                required: true,
                email:true
            },
            password:{
                required:true,
                minlength:6
            }        
        }
    });
    $('#adminlogin').on('submit', function(e) {        
        e.preventDefault();
        let $form = $(this);
        if(!$form.valid()) return false;
        $.ajax({
        type: "POST",
        url: adminlogin,
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function(msg) {
            if($.isEmptyObject(msg.error))                    {
                window.location.href = transactionList;
            } else {
                setTimeout(function() {
                    $(".alert-light-danger").show();
                    $(".alert-text").html('');
                    $.each( msg.error, function( key, value ) {
                        $(".alert-text").append(value);
                    });
                }, 1000);
            }
            }
        });
    });
});