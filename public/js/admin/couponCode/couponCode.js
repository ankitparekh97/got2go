$('#couponcodeList').dataTable({
    serverSide: false,
    bLengthChange: true,
    bProcessing: false,
    destroy: true,
    iDisplayLength: 10,
    lengthMenu: [
        [10, 25, 50, 100, -1],
        ['10', '25', '50', '100', 'All']
    ],
    aaSorting: [[2, 'desc']],
    bInfo: false,
    dom: 'lftip',
    "columnDefs": [{
        "targets": 3,
        "createdCell": function (td, cellData, rowData, row, col) {
            if (cellData == 'Activated') {
                $(td).addClass('custom-completed-text');
            } else if (cellData == 'Deactivated') {
                $(td).addClass('custom-refunded-text');
            } else {
                $(td).addClass('custom-awaiting-text')
            }
        }
    }],
    "language": {
        emptyTable: "No data available in coupon code",
        searchPlaceholder: "Search Coupon Codes",
        search: "",
        lengthMenu: "Number per page _MENU_",
        paginate: {
            next: '<i class="fas fa-angle-right"></i>',
            previous: '<i class="fas fa-angle-left"></i>'
        }
    },
    ajax: {
        url: getAllCouponCodesRoute,
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        beforeSend: function () {
            $('#loader_spin').show();
        },
        complete: function () {
            $('#loader_spin').hide();
        },
        error: function () {
            console.log('Error:');
        }
    },
    aoColumns: [
        {
            "mData": "name",
            "sWidth": "15rem",
            "orderable": true
        },
        {
            "mData": "discount_percentage",
            "orderable": true
        },
        {
            "mData": "created_at",
            "orderable": true,
        },
        {
            "mData": "number_of_time_used",
            "orderable": true,
        },
        {
            "mData": "is_single_use",
            "orderable": true
        },
        {
            "mData": "action",
            "sWidth": "12rem",
            "orderable": false,
            "className": "text-center",
        }
    ],
});



var formCouponCode = $("#formCouponCode");

var formCouponCodeValidate = formCouponCode.validate({
    rules: {
        coupon_code: {
            required: true,
            minlength: 5,
            maxlength: 20,
        },
        discount_percentage: {
            required: true,
            number: true,
            integer: true,
            range: [1, 99]
        }
    },
    messages: {
        coupon_code: {
            required: 'Please enter coupon code.',
            maxlength: 'Max character limit reached',
            minlength: 'Min characters is not entered'
        },
        discount_percentage: {
            required: 'Please enter discount percentage.',
            number: 'Discount percentage must be numeric.',
            integer: 'A positive non-decimal number please.',
            range: 'Percentage should be between 1 to 99'
        }
    }
});

jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-z,""]+$/i.test(value);
}, "Letters only please");


$("#saveCouponCode").click(function (e) {
    e.preventDefault();

    let isValid = formCouponCode.valid();

    if (!isValid) {
        return true;
    }

    let coupon_code = $('#coupon_code').val();
    let discount_percentage = $('#discount_percentage').val();
    let is_single_use = $('#is_single_use').is(":checked");

    let submitButton = $("#saveCouponCode");
    submitButton.attr('disabled', true);
    submitButton.text('Saving...');

    $("#formCouponCode input").attr('readonly', true);

    $.ajax({
        type: "POST",
        url: saveCouponCodeRoute,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'name': coupon_code,
            'discountPercentage': discount_percentage,
            'isSingleUse': is_single_use
        },
        success: function (data) {

            let $showerrormsgsub = $("#showerrormsgsub");
            $showerrormsgsub.html('');
            $showerrormsgsub.hide();

            let $showsuccessmsgsub = $("#showsuccessmsgsub");
            $showsuccessmsgsub.html('');
            $showsuccessmsgsub.hide();
            if (data.success === false) {
                $showerrormsgsub.html(data.message);
                $showerrormsgsub.show();
            } else {
                $showsuccessmsgsub.html(data.message);
                $showsuccessmsgsub.fadeOut(3000);
                formCouponCodeValidate.resetForm();
                $('#formCouponCode')[0].reset();
                $("#couponcodeList").DataTable().ajax.reload(null, false);
            }

            submitButton.removeAttr('disabled');
            submitButton.text('Save');
            $("#formCouponCode input").removeAttr('readonly');

        },
        error: function (xhr, status, error) {
            submitButton.removeAttr('disabled');
            submitButton.text('Save');
            $("#formCouponCode input").removeAttr('readonly');
        }
    });

});

function openActivatePopup(couponId) {
    $("#activate").modal('show');
    $("#activate").find('#coupon_code_id').val(couponId);
}

function openDeActivatePopup(couponId) {
    $("#deActivate").modal('show');
    $("#deActivate").find('#coupon_code_id').val(couponId);
}

function activateCouponCode() {

    let $deActivate = $("#activate");
    let $showerrormsgsub = $deActivate.find("#showerrormsg");
    $showerrormsgsub.html('');
    $showerrormsgsub.hide();

    let couponCodeId = $deActivate.find('#coupon_code_id').val();
    let activateCouponCodeRouteURL = activateCouponCodeRoute;
    activateCouponCodeRouteURL = activateCouponCodeRouteURL.replace('#couponCodeId', couponCodeId);

    $.ajax({
        type: "GET",
        url: activateCouponCodeRouteURL,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        success: function (data) {

            if (data.success === false) {
                $showerrormsgsub.html(data.message);
                $showerrormsgsub.show();
            } else {
                $deActivate.find('#coupon_code_id').val('');
                $("#couponcodeList").DataTable().ajax.reload(null, false);
                $deActivate.modal('hide');
            }

        },
        error: function (xhr, status, error) {
            $deActivate.modal('hide');
        }
    });

}

function deActivateCouponCode() {

    let $activate = $("#deActivate");
    let $showerrormsgsub = $activate.find("#showerrormsg");
    $showerrormsgsub.html('');
    $showerrormsgsub.hide();

    let couponCodeId = $activate.find('#coupon_code_id').val();
    let deActivateCouponCodeRouteURL = deActivateCouponCodeRoute;
    deActivateCouponCodeRouteURL = deActivateCouponCodeRouteURL.replace('#couponCodeId', couponCodeId);

    $.ajax({
        type: "GET",
        url: deActivateCouponCodeRouteURL,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        success: function (data) {

            if (data.success === false) {
                $showerrormsgsub.html(data.message);
                $showerrormsgsub.show();
            } else {
                $activate.find('#coupon_code_id').val('');
                $activate.modal('hide');
                $("#couponcodeList").DataTable().ajax.reload(null, false);
            }

        },
        error: function (xhr, status, error) {
            $activate.modal('hide');
        }
    });

}
