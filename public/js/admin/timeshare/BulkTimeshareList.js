function approveproperty_all(id, owner, listing_no) {
    $("#bulkupload_uniquekey").val(id);
    $("#approve_owner_name").text(owner);
    $("#approve_list_no").text(listing_no);
}
function denyproperty_all(id, owner, listing_no) {
    $("#bulkupload_uniquekey").val(id);
    $("#deny_owner_name").text(owner);
    $("#deny_list_no").text(listing_no);
}

function review_all_individually(id, owner, listing_no) {
    $("#bulkupload_uniquekey").val(id);
    $("#individual_owner_name").text(owner);
    $("#individual_list_no").text(listing_no);
}
function propertyapprove_all(obj) {
    var bulkupload_uniquekey = $("#bulkupload_uniquekey").val();
    $.ajax({
        type: "POST",
        url: propertyApprove_all,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'bulkupload_uniquekey': bulkupload_uniquekey,
            'action': 'approved'
        },
        success: function (data) {
            if (data.success = 'true') {
                $('#showmsg_all').text(data.message).show().delay(3000).fadeOut();
                $("#BulkTimeshareList").DataTable().ajax.reload(null, false);
                $("#timesharelist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showmsg_all').text(data.message).show().delay(3000).fadeOut();
        }
    });
    $("#approve_all").modal('hide');
}

function propertydenyMessage_all(obj) {
    var bulkupload_uniquekey = $("#bulkupload_uniquekey").val();
    $("#deny_all").modal('hide');
    $("#reasonDecline_all").modal('show');
    $("#deny_message_all").val('');

}

function propertyreviewindividually_all(obj) {
    var bulkupload_uniquekey = $("#bulkupload_uniquekey").val();
    $.ajax({
        type: "POST",
        url: propertyApprove_all,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'bulkupload_uniquekey': bulkupload_uniquekey,
            'action': 'review_individually'
        },
        success: function (data) {
            if (data.success = 'true') {
                $('#showmsg_all').text(data.message).show().delay(3000).fadeOut();
                $("#BulkTimeshareList").DataTable().ajax.reload(null, false);
                $("#timesharelist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showmsg_all').text(data.message).show().delay(3000).fadeOut();
        }
    });
    $("#individual").modal('hide');
}

function propertydeny_all(obj) {
    var bulkupload_uniquekey = $("#bulkupload_uniquekey").val();
    var deny_message = $("#deny_message_all").val();
    if (deny_message == '') {
        $('#discountPercentageError_all').css("display", "block");
    } else {
        $('#discountPercentageError_all').css("display", "none");
        // deny property
        $.ajax({
            type: "POST",
            url: propertyDeny_all,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('admin_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'bulkupload_uniquekey': bulkupload_uniquekey,
                'action': 'rejected',
                'reason_message': deny_message
            },
            success: function (data) {
                if (data.success = 'true') {
                    $('#showmsg_all').text(data.message).show().delay(3000).fadeOut();
                    $("#deny_message_all").val('');
                    $("#BulkTimeshareList").DataTable().ajax.reload(null, false);
                    $("#timesharelist").DataTable().ajax.reload(null, false);
                }
            },
            error: function (xhr, status, error) {
                $('#showmsg_all').text(data.error).show().delay(3000).fadeOut();
            }
        });
        $("#reasonDecline_all").modal('hide');
    }
}



BulkTimeshareList();
var dataTable;
function BulkTimeshareList() {
    $('#BulkTimeshareList').dataTable(
        {
            serverSide: true,
            bLengthChange: true,
            bProcessing: false,
            destroy: true,
            iDisplayLength: 10,
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'All']
            ],
            aaSorting: [],
            "bInfo": false,
            "dom": 'lftip',
            scrollX: true,
            scrollY: true,
            scrollCollapse: true,
            "columnDefs": [{
                "targets": 3,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (rowData.status == 'Denied') {
                        $(td).addClass('custom-refunded-text');
                    } else if (cellData == 'Pending') {
                        $(td).addClass('custom-pending-text');
                    } else {
                        $(td).addClass('custom-completed-text')
                    }
                }
            }],
            "language": {
                "emptyTable": "No data available in approve time share listing",
                searchPlaceholder: "Search",
                search: "",
                lengthMenu: "Number per page _MENU_",
                paginate: {
                    next: '<i class="fas fa-angle-right"></i>',
                    previous: '<i class="fas fa-angle-left"></i>'
                }
            },
            ajax: {
                url: BulkTimeshareList_url,
                type: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#loader_spin').show();
                },
                complete: function () {
                    $('#loader_spin').hide();
                },
                error: function () {
                    console.log('Error:');
                }
            },
            aoColumns: [
                {
                    "mData": "owner",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData": "email",
                    "sWidth": "18rem",
                    "orderable": true
                },
                {
                    "mData": "govID",
                    "sWidth": "15rem",
                    "orderable": true
                },
                {
                    "mData": "status",
                    "sWidth": "10rem",
                    "orderable": true
                },
                {
                    "mData": "listings",
                    "sWidth": "10rem",
                    "orderable": true
                },
                {
                    "mData": "action",
                    "sWidth": "25rem",
                    "orderable": false,
                    "className": "text-center",
                }
            ],
        });
}