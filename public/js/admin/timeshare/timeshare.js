function approveproperty(id) {   
    $("#property_id").val(id);
  }
  function denyproperty(id) {   
    $("#property_id").val(id);
  }
  
  function propertyapprove(obj) {
      var property_id = $("#property_id").val();
      // approve property
      $.ajax({
          type: "POST",
          url: propertyApprove,            
          dataType: "JSON",
          headers: {
          "Authorization": "Bearer {{session()->get('admin_token')}}",
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          },
          data: {               
              'id' : property_id,
              'action' : 'approved'           
          },
          success: function(data) {
          if(data.success = 'true')
          {
              $( '#showmsg' ).text( data.message ).show().delay(3000).fadeOut();
              $("#timesharelist").DataTable().ajax.reload(null, false );   
              $("#BulkTimeshareList").DataTable().ajax.reload(null, false );             
          }            
          },
          error: function(xhr, status, error) {
              $( '#showmsg' ).text( data.message ).show().delay(3000).fadeOut();
          }
      });
      $("#approve").modal('hide');
  }
  
  function propertydenyMessage(obj) {
    var property_id = $("#property_id").val();
    $("#deny").modal('hide');
    $("#reasonDecline").modal('show');
    $("#deny_message").val('');

  }

  function propertydeny(obj) {
      var property_id = $("#property_id").val();
      var deny_message = $("#deny_message").val();
      if(deny_message == '') {
        $( '#discountPercentageError' ).css("display","block");
      } else {        
        $( '#discountPercentageError' ).css("display","none");
      // deny property
     $.ajax({
          type: "POST",
          url: propertyDeny,            
          dataType: "JSON",
          headers: {
          "Authorization": "Bearer {{session()->get('admin_token')}}",
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {               
              'id' : property_id,
              'action' : 'rejected',
              'reason_message': deny_message          
          },
          success: function(data) {
          if(data.success = 'true')
          {
              $( '#showmsg' ).text( data.message ).show().delay(3000).fadeOut();
              $("#deny_message").val('');
              $("#timesharelist").DataTable().ajax.reload(null, false );        
              $("#BulkTimeshareList").DataTable().ajax.reload(null, false );        
          }            
          },
          error: function(xhr,status, error) {
              $( '#showmsg' ).text( data.error ).show().delay(3000).fadeOut();
          }
      });
      $("#reasonDecline").modal('hide');
    }  
  }
     
timesharelist();
var dataTable;
function timesharelist() {						
    $('#timesharelist').dataTable(							
        {
            serverSide : true,
            bLengthChange : true,
            bProcessing : false,								
            destroy: true,								
            iDisplayLength : 10,
            lengthMenu: [
                [ 10, 25, 50, 100, -1 ],
                [ '10', '25', '50', '100', 'All' ]
            ],
            aaSorting : [],
            "bInfo" : false,
            "dom": 'lftip',
            scrollX: true,
            scrollY: true,
            scrollCollapse: true,
            "columnDefs": [ {
                "targets": 5,
                "createdCell": function (td, cellData, rowData, row, col) {                                              
                    if ( rowData.status == 'Denied' ) {
                        $(td).addClass('custom-refunded-text');
                    } else if(cellData == 'Pending') {
                        $(td).addClass('custom-pending-text');
                    } else {
                        $(td).addClass('custom-completed-text') 
                    }
                }
            }],
            "language": {
                    "emptyTable": "No data available in approve time share listing",
                    searchPlaceholder: "Search",
                    search: "",
                    lengthMenu: "Number per page _MENU_",
                    paginate: {
                        next: '<i class="fas fa-angle-right"></i>', 
                        previous: '<i class="fas fa-angle-left"></i>' 
                      }
                },            
                ajax : {
                    url : timeshareListing,
                    type : 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType : 'json',										
                    beforeSend: function(){
                        $('#loader_spin').show();                            
                    },
                    complete: function(){
                        $('#loader_spin').hide();
                    },
                    error : function() {
                        console.log('Error:');
                    }
                },                
                aoColumns : [
                {
                    "mData" : "owner",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData" : "email",
                    "sWidth": "18rem",
                    "orderable": true
                },                                                
                {
                    "mData" : "property_name",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData" : "timeshare_member_ID",
                    "sWidth": "16rem",
                    "orderable": true
                },
                {
                    "mData" : "ownershipDoc",
                    "sWidth": "15rem",
                    "orderable": true
                },
                {
                    "mData" : "status",
                    "sWidth": "10rem",
                    "orderable": true
                },
                {
                    "mData" : "action",
                    "sWidth": "14rem",
                    "orderable": false,
                    "className": "text-center",
                }									
            ],
        });
}
$("#searchbox").keyup(function() {
    $('#timesharelist').dataTable().fnFilter(this.value);
    
});

function view_document(type,id){
    $.ajax({
        url:view_document_url+'/'+type+'/'+id,
        method:'GET',
        dataType:'html',
        success:function(res){
            $("#MyModal").html(res);
            $("#MyModal").modal();
        }
    })
}