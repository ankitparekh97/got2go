//service fee formatting
const isNumericInput = (event) => {
    const key = event.keyCode;
    return ((key >= 48 && key <= 57) || // Allow number line
        (key >= 96 && key <= 105) || // Allow number pad
        (key === 110 || key === 190) // Allow dot
    );
};
const isModifierKey = (event) => {
    const key = event.keyCode;
    return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
        (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
        (key > 36 && key < 41) || // Allow left, up, right, down
        (
            // Allow Ctrl/Command + A,C,V,X,Z
            (event.ctrlKey === true || event.metaKey === true) &&
            (key === 65 || key === 67 || key === 86 || key === 88 || key === 90 || key === 110 || key === 190)
        )
};
const enforceFormat = (event) => {
    // Input must be of a valid number format or a modifier key, and not longer than ten digits
    if (!isNumericInput(event) && !isModifierKey(event)) {
        event.preventDefault();
    }
};
const formatToServiceFee = (event) => {
    if (isModifierKey(event)) { return; }
    var chkbox = $("input[name='ratio']:checked").val();
    if (chkbox == 2) {
        const target = event.target;
        const input = event.target.value.replace(/\D/g, '').substring(0, 5); // First five digits of input only
        const zip = input.substring(0, 2);
        const middle = input.substring(2, 4);
        if (input.length > 4) {
            target.value = `${zip}.${middle}`;
        } else if (input.length > 2) {
            target.value = `${zip}.${middle}`;
        } else if (input.length > 0) {
            target.value = `${zip}`;
        }
    }

};
const inputElement = document.getElementById('servicefee');
inputElement.addEventListener('keydown', enforceFormat);
inputElement.addEventListener('keyup', formatToServiceFee);
function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot (thanks ddlab)
    if (number.length > 1 && charCode == 46) {
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function hidedollor() {
    $('#defaultdolor').hide();
    $('#defaultper').hide();
    $('#showdollorid').hide();
    $('#hidepercentage').show();
    $('#servicefee').val('');

}
function showdollor() {
    $('#hidepercentage').hide();
    $('#defaultper').hide();
    $('#showdollorid').show();
    $('#hidepercentage').hide();

}
function refundamount(obj) {
    var renterName = $(obj).attr('data-renterName');
    var reservationDate = $(obj).attr('data-reservationDate');
    var transactionId = $(obj).attr('data-id');
    $('#transaction_id').val(transactionId);
    $('#renter_name').text(renterName);
    $('#reservation_dates').text(reservationDate);
}

function paymentrefunded(obj) {
    var transactionId = $('#transaction_id').val();
    $('#transaction_id').val(transactionId);
    $("#refund").modal('hide');
    $("#servicefee").val('');
}
function paymentrefundConfirm(obj) {
    $('#showerrormsg').text('');
    $('#showmsg').text('');
    var transactionId = $('#transaction_id').val();
    var servicefee = $("#servicefee").val();
    var amount_charges = $("input[name='ratio']:checked").val();
    var serviceFeeWithdecimal = Number.parseFloat(servicefee).toFixed(2);

    if (servicefee == '' && amount_charges == 1) {
        $('#error').show().delay(3000).fadeOut();
        $('#showerrormsg').text('Please enter refund amount').show().delay(3000).fadeOut();
        $("#saveFee").modal('hide');
    } else if (servicefee == '' && amount_charges == 2) {
        $('#error').show().delay(3000).fadeOut();
        $('#showerrormsg').text('Please enter percentage amount').show().delay(3000).fadeOut();
        $("#saveFee").modal('hide');
    } else {
        $('#error').hide();
        $('#showerrormsg').hide();
        // ajax call
        $.ajax({
            type: "POST",
            url: refundAmount,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('admin_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                'amount': servicefee,
                'id': transactionId,
                'chk_fixed': amount_charges
            },
            success: function (data) {
                if (data.success == true) {
                    console.log(4);
                    $('#serviceFeeValues').val(data.servicefee);
                    $('#serviceFeePercentageValues').val(data.serviceId);
                    $('#servicefee').val(data.servicefee);
                    $('#service_id').val(data.autoid);
                    $('#success').show().delay(3000).fadeOut();
                    $('#showmsg').html(data.message).show().delay(3000).fadeOut();
                    $("#paymentrefund").modal('hide');
                    $("#transactionLogs").DataTable().ajax.reload(null, false);
                } else {
                    $('#error').show().delay(3000).fadeOut();
                    $('#showerrormsg').text(data.message).show().delay(7000).fadeOut();
                }
            },
            error: function (xhr, status, error) {
                $('#success').hide();
                $('#error').show();
                $("#paymentrefund").modal('hide');
            }
        });
    }
}

transactionLogs();
var dataTable;
function transactionLogs() {
    $('#transactionLogs').dataTable(
        {
            serverSide: false,
            bLengthChange: true,
            bProcessing: false,
            destroy: true,
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'All']
            ],
            iDisplayLength: 10,
            aaSorting: [[0, 'desc']],
            "bInfo": false,
            scrollX: true,
            scrollCollapse: true,
            "dom": 'lftip',
            "columnDefs": [{
                "targets": 2,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (rowData.status == 'Refunded') {
                        $(td).addClass('custom-refunded-text');
                    } else {
                        $(td).addClass('custom-completed-text')
                    }
                }
            },
            {
                "targets": 7,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (cellData == 'Refunded') {
                        $(td).addClass('custom-refunded-text');
                    } else if (cellData == 'Pending') {
                        $(td).addClass('custom-pending-text');
                    } else {
                        $(td).addClass('custom-completed-text')
                    }
                }
            },
            ],
            "language": {
                "emptyTable": "No data available in transaction log",
                searchPlaceholder: "Search transaction",
                search: "",
                lengthMenu: "Number per page _MENU_",
                paginate: {
                    next: '<i class="fas fa-angle-right"></i>',
                    previous: '<i class="fas fa-angle-left"></i>'
                }
            },
            ajax: {
                url: transactionList,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#loader_spin').show();
                },
                complete: function () {
                    $('#loader_spin').hide();
                },
                error: function () {
                    console.log('Error:');
                }
            },
            aoColumns: [
                {
                    "mData": "id",
                    "sWidth": "13rem",
                    "orderable": true,
                    "className": "text-center",
                },
                {
                    "mData": "date",
                    "sWidth": "18rem",
                    "orderable": true
                },
                {
                    "mData": "total",
                    "sWidth": "17rem",
                    "orderable": true,
                    "className": "text-center",
                },
                {
                    "mData": "property_name",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData": "location",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData": "rental_user_id",
                    "sWidth": "12rem",
                    "orderable": true
                },
                {
                    "mData": "owner",
                    "sWidth": "13rem",
                    "orderable": true
                },
                {
                    "mData": "status",
                    "sWidth": "10rem",
                    "orderable": true
                },
                {
                    "mData": "action",
                    "sWidth": "8rem",
                    "orderable": false,
                    "className": "text-center",
                }
            ],
        });
}

$("#searchbox").keyup(function () {
    $('#transactionLogs').dataTable().fnFilter(this.value);
});
