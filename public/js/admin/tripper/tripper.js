
let $discount_percentage = $("#discount_percentage");
let $discount_percentage_val = $discount_percentage.val();
$discount_percentage.mask('00.00', { placeholder: '00.00' });
if ($discount_percentage_val > 0) {
    $discount_percentage.val($discount_percentage.masked($discount_percentage_val));
}

$.validator.addMethod("greaterThan", function (value, element, param) {
    var $otherElement = $(param);
    return parseInt(value, 10) > parseInt($otherElement.val(), 10);
})


$("#formSubscriptionTripper").validate({
    rules: {
        annual_subscription: {
            required: true,
            number: true,
            integer: true,
            min: 1
        },
        annual_strike_subscription_amount: {
            required: true,
            number: true,
            integer: true,
            greaterThan: "#annual_subscription"
        },
        discount_percentage: {
            required: true,
            number: true,
            max: 80
        },
    },
    messages: {
        annual_subscription: {
            required: "Please enter annual membership fee",
            number: 'Annual membership fee is invalid',
            integer: 'A positive non-decimal number please'
        },
        annual_strike_subscription_amount: {
            required: "Please enter strikethrough price",
            number: 'Strikethrough price is invalid',
            integer: 'A positive non-decimal number please',
            greaterThan: 'Strikethrough price must be greater than the Annual Membership Fee'
        },
        discount_percentage: {
            required: "Please enter discount percentage.",
            number: "Discount percentage price is invalid",
            integer: "A positive non-decimal number please",
            max: "Discount percentage cannot be greater than 80%"
        }
    },
    submitHandler: function () {

        hideSubscriptionTransactionFee();

        let subscription_id = $('#subscription_id').val();
        let $annual_subscription = $('#annual_subscription');
        let $discount_percentage = $('#discount_percentage');
        let $annual_strike_subscription_amount = $('#annual_strike_subscription_amount');
        var annual_subscription = $annual_subscription.val();
        var discount_percentage = $discount_percentage.val();
        var annual_strike_subscription_amount = $annual_strike_subscription_amount.val();

        let submitButton = $("#savesubscription");
        submitButton.attr('disabled', true);
        submitButton.text('Saving...');

        $annual_subscription.attr('readonly', true);
        $discount_percentage.attr('readonly', true);
        $annual_strike_subscription_amount.attr('readonly', true);

        $.ajax({
            type: "POST",
            url: subscriptionSave,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('admin_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                'annual_subscription': annual_subscription,
                'discount_percentage': discount_percentage,
                'annual_strike_subscription_amount': annual_strike_subscription_amount,
                'id': subscription_id
            },
            success: function (data) {
                submitButton.removeAttr('disabled');
                submitButton.text('Save');
                $annual_subscription.removeAttr('readonly');
                $discount_percentage.removeAttr('readonly');
                $annual_strike_subscription_amount.removeAttr('readonly');
            },
            error: function (xhr, status, error) {
                submitButton.removeAttr('disabled');
                submitButton.text('Save');
                $annual_subscription.removeAttr('readonly');
                $annual_subscription.removeAttr('readonly');
                $annual_strike_subscription_amount.removeAttr('readonly');
            }
        });
    }
});

$("#annual_subscription").focusout(function () {
    var subcriptionFee = $(this).val();
    if (subcriptionFee == '' || subcriptionFee == 0) {
        hideSubscriptionTransactionFee();
    } else {
        let subscriptionTransactionFee = HelperFunction.getSubscriptionTransactionFee(subcriptionFee);

        showSubscriptionTransactionFee(subscriptionTransactionFee);
    }

});

$("#annual_subscription").keyup(function () {
    var subcriptionFee = $(this).val();
    if (subcriptionFee == '' || subcriptionFee == 0) {
        hideSubscriptionTransactionFee();
    } else {
        let subscriptionTransactionFee = HelperFunction.getSubscriptionTransactionFee(subcriptionFee);

        showSubscriptionTransactionFee(subscriptionTransactionFee);
    }
});


function showSubscriptionTransactionFee(subscriptionTransactionFee) {
    let tripperSubscriptionFee = $("#tripperSubscriptionFee");
    tripperSubscriptionFee.find('#spanTransactionFee').html(subscriptionTransactionFee);
    tripperSubscriptionFee.removeClass('d-none');
    tripperSubscriptionFee.find('.admin-error-cstm').show();
}

function hideSubscriptionTransactionFee() {
    let tripperSubscriptionFee = $("#tripperSubscriptionFee");
    tripperSubscriptionFee.find('#spanTransactionFee').html('tripperSubscriptionFee');
    tripperSubscriptionFee.addClass('d-none');
}

$("#btnClosePopup").click(function () {
    $('#status_show').text('');
});
function paused(id) {
    $("#rental_user_id").val(id);
    $("#subscription").val('paused');
    $('#status_show').text('pause');
}
function cancel(id) {
    $("#rental_user_id").val(id);
}

function active(id) {
    $("#rental_user_id").val(id);
    $("#subscription").val('active');
    $('#status_show').text('activate');
}

function subscriptionApprove() {
    var rental_user_id = $("#rental_user_id").val();
    var subscription = $("#subscription").val();
    $.ajax({
        type: "POST",
        url: subscriptionApproveRoute,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': rental_user_id,
            'subscription_action': subscription
        },
        success: function (data) {
            if (data.success == true) {
                $('#showmsg').text(data.message).show().delay(3000).fadeOut();
                $("#paused").modal('hide');
                $("#tripperslist").DataTable().ajax.reload(null, false);
            } else {
                $("#paused").modal('hide');
                $('#showerrormsgsub').text(data.error).show().delay(3000).fadeOut();
            }
        },
        error: function (xhr, status, error) {

        }
    });
}

function cancelSubscription() {
    var rental_user_id = $("#rental_user_id").val();
    // cancel subscription
    $.ajax({
        type: "POST",
        url: subscriptionCancel,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': rental_user_id
        },
        success: function (data) {
            if (data.success == 'true') {
                $('#showmsg').text(data.message).show().delay(3000).fadeOut();
                $("#cancel").modal('hide');
                $("#tripperslist").DataTable().ajax.reload(null, false);
            }
        },
        error: function (xhr, status, error) {
            $('#showcancelerrormsg').text(data.error).show().delay(3000).fadeOut();
        }
    });
}

tripperList();
function tripperList() {
    $('#tripperslist').dataTable(
        {
            serverSide: false,
            bLengthChange: true,
            bProcessing: false,
            destroy: true,
            iDisplayLength: 10,
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'All']
            ],
            aaSorting: [[3, 'desc']],
            "bInfo": false,
            "dom": 'lftip',
            "columnDefs": [{
                "targets": 4,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (cellData == 'Active') {
                        $(td).addClass('custom-active-text');
                    } else {
                        $(td).addClass('custom-paused-text')
                    }
                }
            }],
            "language": {
                "emptyTable": "No data available in tripper dashboard",
                searchPlaceholder: "Search",
                search: "",
                lengthMenu: "Number per page _MENU_",
                paginate: {
                    next: '<i class="fas fa-angle-right"></i>',
                    previous: '<i class="fas fa-angle-left"></i>'
                }
            },
            ajax: {
                url: trippersListing,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#loader_spin').show();
                },
                complete: function () {
                    $('#loader_spin').hide();
                },
                error: function () {
                    console.log('Error:');
                }
            },
            aoColumns: [
                {
                    "mData": "name",
                    "sWidth": "20rem",
                    "orderable": true
                },
                {
                    "mData": "age",
                    "sWidth": "10rem",
                    "orderable": true,
                    "className": "text-center",
                },
                {
                    "mData": "email",
                    "sWidth": "18rem",
                    "orderable": true
                },
                {
                    "mData": "booking",
                    "sWidth": "10rem",
                    "orderable": true,
                    "className": "text-center",
                },
                {
                    "mData": "status",
                    "sWidth": "8rem",
                    "orderable": true
                },
                {
                    "mData": "action",
                    "sWidth": "10rem",
                    "orderable": false,
                    "className": "text-center",
                }
            ],
        });
}

$("#searchbox").keyup(function () {
    $('#tripperslist').dataTable().fnFilter(this.value);
});
