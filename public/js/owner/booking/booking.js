$(document).ready(function () {
    propertyBookings();
    confirmedPropertyBooking();

    var dataTable;
    function propertyBookings() {

        dataTable = $('.propertyBooking').DataTable({
            processing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: bookingData,
                method: 'POST'
            },
            columns: [
                {
                    'className': 'detail-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                { "data": "photo", "name": "photo" },
                { "data": "propertyTitle", "name": "propertyTitle" },
                { "data": "duration", "name": "duration" },
                { "data": "total", "name": "total" },
                { "data": "statuses", "name": "statuses" },
                { "data": "actions", "name": "actions" },
            ],
            language: {
                processing: "<img src='" + processingImg + "'>"
            },
            bLengthChange: false,
            pageLength: 10,
        });

    }

    var confirmedPropertyBookingdataTable;
    function confirmedPropertyBooking() {

        confirmedPropertyBookingdataTable = $('.confirmedPropertyBooking').DataTable({
            processing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: confirmedbookingData,
                method: 'POST'
            },
            columns: [
                {
                    'className': 'details-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                { "data": "photo", "name": "photo" },
                { "data": "propertyTitle", "name": "propertyTitle" },
                { "data": "duration", "name": "duration" },
                { "data": "total", "name": "total" },
                { "data": "statuses", "name": "statuses" },
                { "data": "actions", "name": "actions" },
            ],
            bLengthChange: false,
            pageLength: 10,
        });

    }


    $("#searchbox").keyup(function () {
        $('#propertyBooking').dataTable().fnFilter(this.value);
    });

    $('#filter').click(function () {
        var type = $('#type').val();
        var owner_status = $('#owner_status').val();
        var booking_type = $('#booking_type').val();

        $('.propertyBooking').DataTable().destroy();
        propertyBookings(type, owner_status, booking_type);
    });

    // Add event listener for opening and closing details
    $('.propertyBooking tbody').on('click', 'tr td.detail-control', function () {
        var tr = $(this).closest('tr');
        var row = dataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    // Add event listener for opening and closing details for confirmed
    $('.confirmedPropertyBooking tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = confirmedPropertyBookingdataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(confirmedFormat(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {

        return '<div class="table-responsive inner"><table class="inner" id="extraInfo" >' +
                '<tr>' +
                '<td></td>' +
                '<td>' + d.adults + '</td>' +
                '<td>' + d.location_detail + '</td>' +
                '<td><span>Check-In: ' + d.check_in + ' ' + d.check_in_date + ' <br> Check-Out: ' + d.check_out + ' ' + d.check_out_date + '</span></td>' +
                '<td>' + d.price + '</td>' +
                '<td>' + d.time + '</td>' +
                '<td></td>' +
                '</tr>' +
                '</table>' +
                '</div>';
    }

    function confirmedFormat(d) {

        return '<div class="table-responsive inner"><table class="inner" id="extraInfo" cellpadding="0" cellspacing="0" border="0" >' +
                '<tr>' +
                '<td></td>' +
                '<td>' + d.adults + '</td>' +
                '<td>' + d.location_detail + '</td>' +
                '<td><span>Check-In: ' + d.check_in + ' ' + d.check_in_date + ' <br> Check-Out: ' + d.check_out + ' ' + d.check_out_date + '</span></td>' +
                '<td>' + d.price + '</td>' +
                '<td>' + d.time + '</td>' +
                '<td></td>' +
                '</tr>' +
                '</table>' +
                '</div>';
    }

    $('#confirmation').validate({
        rules: {
            confirmation_id: {
                required: true,
            }
        },
        messages: {
            confirmation_id: {
                required: "Please enter valid Confirmation ID",
            }
        },
        errorPlacement: function (error, element) {
            var confirmationIdErr = $('#confirmationIdErr');
            if (element.attr('name') == 'confirmation_id') {
                $(confirmationIdErr).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('.propertyBooking, .confirmedPropertyBooking').on('click', '.bookingUpdate', function (e) {
        e.preventDefault();

        let content = '';
        let pid = $(this).data("id");
        let type = $(this).attr('data-type');
        let renter = $(this).attr('data-renter');
        let action = $(this).attr('data-action');
        var checkin = $(this).attr('data-checkin');
        var checkout = $(this).attr('data-checkout');

        $(".approveBooking").attr('data-id', pid);
        $(".approveBooking").attr('data-action', action);

        let timeshareContent = 'Are you sure you want to approve ' + renter + '\'s booking request? Following approval, you will need to submit a confirmation ID provided by your vacation club.';
        let propertyContent = 'Are you sure you want to approve ' + renter + '\'s booking request? ';
        let rejected = 'Are you sure you want to reject ' + renter + '\'s booking request? ';
        //let cancelled = 'Are you sure you want to cancel ' + renter + '\'s booking request? ';
        $('#approve .modal-header h4').css('display', 'block');
        if (action == 'approved') {
            content = (type == 'property') ? propertyContent : timeshareContent;
            $(".approveBooking").empty().append('Approve Request');
        } else if (action == 'rejected') {
            content = rejected;
            $(".approveBooking").empty().append('Reject');
        } else if (action == 'cancelled') {

            $.ajax({
                type: "POST",
                url: calculateownerRefund,
                dataType: "JSON",
                headers: {
                    "Authorization": Authorization,
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                },
                data: {
                    id: pid,
                },
                success: function (msg) {
                    hideLoader();
                    $.unblockUI()

                    if ($.isEmptyObject(msg.error)) {
                        if (msg.refundAmount == 0) {
                            var cancelled = "It is now within 48 hours to check-in time and too late for you to cancel this reservation. If there is an issue, please contact GOT2GO via email at <a href='mailto: admin@got2go.com'>admin@got2go.com</a> or dial (844)-I-GOT2GO";
                            $(".delete-btns").empty().append('<button type="button" class="btn btn-default red-btn-cmn" data-dismiss="modal">Okay</button>');
                        } else {
                            var cancelled = "Are you sure you want to cancel this reservation from " + checkin + " to " + checkout + "?";
                            $(".red-btn-cmn").empty().append('Yes');
                            $(".red-btn-cmn").addClass('confirm-cancel');
                            $(".red-btn-cmn").removeClass('approveBooking');
                            $(".close-btn-cmn").empty().append('No');
                        }
                        $('#approve .modal-header h4').css('display', 'none');
                        content = cancelled;
                        $('#approve').find('.content').empty().html(content);
                        $('#approve').modal('show');


                    }

                }, error:  function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR,textStatus, errorThrown);
                  }
            });

        } else if (action == 'confirmed' && type == 'vacation_rental') {
            $(".approveBooking").empty().append('Submit Confirmation ID');
            $('#confirmBox').modal('show');
            return false;
        }
        $('#approve').find('.content').empty().html(content);
        $('#approve').modal('show');
    });

    $('#confirmBox').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input[type=text], input[type=number], input[type=email], input[type=password]")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();

        var $alertas = $('#confirmation');
        $alertas.validate().resetForm();
        $alertas.find('.error').removeClass('error');
    });

    $('#approve').on('click', '.confirm-cancel', function (e) {

        showLoader();
        $.ajax({
            type: "POST",
            url: calculateownerRefund,
            dataType: "JSON",
            headers: {
                "Authorization": Authorization,
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                id: $(this).attr('data-id'),
            },
            success: function (msg) {
                hideLoader();
                $.unblockUI()

                if ($.isEmptyObject(msg.error)) {
                    if (msg.refundAmount == 0) {
                        var cancelled = "It is now within 48 hours to check-in time and too late for you to cancel this reservation. If there is an issue, please contact GOT2GO via email at <a href='mailto: admin@got2go.com'>admin@got2go.com</a> or dial (844)-I-GOT2GO";
                        $(".delete-btns").empty().append('<button type="button" class="btn btn-default red-btn-cmn" data-dismiss="modal">Okay</button>');
                    } else {
                        var cancelled = "According to the cancellation policy for this reservation, guest will receive $" + HelperFunction.numberWithCommas(HelperFunction.roundNumber(msg.refundAmount)) + " back?";
                        $(".red-btn-cmn").empty().append('Confirm');
                        $(".red-btn-cmn").removeClass('confirm-cancel');
                        $(".red-btn-cmn").addClass('approveBooking');
                        $(".close-btn-cmn").empty().append('Cancel');
                    }
                    $('#approve .modal-header h4').css('display', 'none');
                    content = cancelled;
                    $('#approve').find('.content').empty().html(content);
                    $('#approve').modal('show');


                }

            }
        });

    });

    $('#approve, #confirmBox').on('click', '.approveBooking', function (e) {
        e.preventDefault();

        let $form = $('#confirmation');
        if (!$form.valid()) return false;

        showLoader();

        $.ajax({
            type: "POST",
            url: approve,
            dataType: "JSON",
            data: {
                id: $(this).attr('data-id'),
                action: $(this).attr('data-action'),
                confirmation_id: $('#confirmation_id').val(),
            },
            success: function (msg) {
                hideLoader();
                $.unblockUI()

                if ($.isEmptyObject(msg.error)) {
                    console.log(2);
                    $('#approve').modal('hide');
                    $('#confirmBox').modal('hide');

                    $('.propertyBooking').DataTable().ajax.reload();
                    $('.confirmedPropertyBooking').DataTable().ajax.reload();

                    if (msg.owner_status != 'approved') {
                        $(".confirmedBookings").empty().append('Ok');
                    }

                    let checkInDate = new Date(msg.booking.check_in_date);
                    /* let check_in_dt = checkInDate.toLocaleString('UTC', { month: 'short' }) + ' ' + checkInDate.getDate() + ', ' + checkInDate.getFullYear(); */
                    let check_in_dt = moment.utc(msg.booking.check_in_date).format('MMM DD, YYYY');

                    let checkOutDate = new Date(msg.booking.check_out_date);
                    /* let check_out_dt = checkOutDate.toLocaleString('UTC', { month: 'short' }) + ' ' + checkOutDate.getDate() + ', ' + checkOutDate.getFullYear(); */
                    let check_out_dt = moment.utc(msg.booking.check_out_date).format('MMM DD, YYYY');

                    let pendingTxt = (msg.owner_status == 'pending') ? 'Your earnings will be deposited into your default bank account in 5-7 business days.' : '';
                    let content = 'You have ' + msg.owner_status + ' ' + msg.booking.rentaluser.first_name + ' ' + msg.booking.rentaluser.last_name + '\'s request for ' + check_in_dt + ' - ' + check_out_dt + '. ' + pendingTxt;

                    $('.propertyBooking').DataTable().ajax.reload();
                    $('.confirmedPropertyBooking').DataTable().ajax.reload();

                    $('#congrats').find('.content').empty().html(content);
                    $('#congrats').modal('show');
                    $('html, body').animate({ scrollTop: 50 }, 'slow');
                } else {
                    $('.payment-error p').empty().html(msg.error);
                    $('.payment-error').removeClass('d-none');
                    $('.payment-error').show();
                    $('#approve').modal('hide');
                    $('#confirmBox').modal('hide');
                    $('html, body').animate({ scrollTop: 50 }, 'slow');
                }

            }
        });



    });

    $('#congrats').on('click', '.confirmedBookings', function (e) {
        $('#congrats').modal('hide');

        $('.confirmedPropertyBooking').DataTable().ajax.reload();
        $("#listingtab li:eq(1) a").tab('show');
    });

    setInterval(function () {
        $('.propertyBooking').DataTable().ajax.reload();
        $('.confirmedPropertyBooking').DataTable().ajax.reload();
    }, 60000);


    // Offer Template

    $(".offer-list").on('click', '.offer-box .contentTitleH3', function (e) {
        let propertyId = $(this).attr('data-propertyId');
        let propertyWebURL = getPropertyWebURL;
        propertyWebURL = propertyWebURL.replace('#propertyId', propertyId);
        window.open(propertyWebURL);
    });

    let globalMakeOffer = $("#makeoffer");
    let $globalRangeDateWithFlexible = globalMakeOffer.find('.counterOffer > .rangeDatesWithFlexible');
    let $globalOwnerRangeStartDate = $globalRangeDateWithFlexible.find("#ownerRangeStartDate");
    let $globalOwnerRangeEndDate = $globalRangeDateWithFlexible.find("#ownerRangeEndDate");

    $globalOwnerRangeStartDate.datepicker({
        startDate: '+0d',
        dateFormat: 'mm-dd-yy'
    }).on("changeDate", function (e) {
        $globalOwnerRangeEndDate.val('');
        let dateEndDateSet = e.date;
        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
        $globalOwnerRangeEndDate.datepicker('setStartDate', dateEndDateSet);
    });

    $globalOwnerRangeEndDate.datepicker({
        startDate: '+0d',
        dateFormat: 'mm-dd-yy'
    }).on("changeDate", function (e) {
        let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
        let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));
        if (offerStartDate && offerEndDate) {
            let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);
            globalMakeOffer.find('.counterOffer > .totalNight').text(totalNightsToStay);
            let propertyId = globalMakeOffer.find('#makeAnOfferPropertyId').val();
            if (propertyId) {
                OwnerOffer.validation.validateBookingSlotAvailable(propertyId, offerStartDate, offerEndDate);
            }
        }
    });

    let $makeACounterOfferFormValidate = $("#makeACounterOfferForm").validate({
        rules: {
            ownerRangeStartDate: {
                required: true,
            },
            ownerRangeEndDate: {
                required: true
            },
            totalNightlyStandard: {
                required: true,
                number: true,
                integer: true,
            },
        },
        messages: {
            ownerRangeStartDate: {
                required: "Start date is required"
            },
            ownerRangeEndDate: {
                required: "End date is required"
            },
            totalNightlyStandard: {
                required: "Proposed total price cannot be blank.",
                integer: "Price should be in an integer only"
            }
        },
        errorPlacement: function (error, element) {
            let makeOffer = $("#makeoffer");
            makeOffer.find('.error-label').html(error).removeClass('hide');
        }
    });

    globalMakeOffer.find('.counterOffer > .totalNightlyStandard  #inputTotalNightlyStandard').keyup(function () {
        globalMakeOffer.find('.approveBooking').removeAttr('disabled');
        OwnerOffer.validation.addRangeRulesToMakeAnOffer();
        if (!$("#makeACounterOfferForm").valid()) {
            globalMakeOffer.find('.approveBooking').attr('disabled', true);
        }

    });

    OwnerOffer = ({
        validation: {
            validateCounterOfferStandardOffer: function (proposedTotalPrice) {

                let globalMakeOffer = $("#makeoffer");
                let $globalRangeDateWithFlexible = globalMakeOffer.find('.counterOffer > .rangeDatesWithFlexible');
                let $globalOwnerRangeStartDate = $globalRangeDateWithFlexible.find("#ownerRangeStartDate");
                let $globalOwnerRangeEndDate = $globalRangeDateWithFlexible.find("#ownerRangeEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                proposedTotalPrice = HelperFunction.roundNumber(proposedTotalPrice);

                let standardNightlyPrice = globalMakeOffer.find('#makeAnOfferStandardPrice').val();
                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                totalStandardNightlyPrice = HelperFunction.roundNumber(totalStandardNightlyPrice);

                globalMakeOffer.find('.error-label').text('').addClass('hide');
                globalMakeOffer.find('.approveBooking').removeAttr('disabled');
                if (proposedTotalPrice < 1) {
                    globalMakeOffer.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                    globalMakeOffer.find('.error-label').text('Proposed total price cannot be less than 1').removeClass('hide');
                } else if (proposedTotalPrice >= totalStandardNightlyPrice) {
                    globalMakeOffer.find('.approveBooking').attr('disabled', true);
                    globalMakeOffer.find('.error-label').text('Proposed total price $(' + proposedTotalPrice + ') cannot be greater than Standard total price $(' + totalStandardNightlyPrice + ')').removeClass('hide');
                }
            },
            validateBookingSlotAvailable: function (propertyId, checkInDate, checkOutDate) {

                showLoader();
                let globalMakeOffer = $("#makeoffer");
                globalMakeOffer.find('.error-label').text('').addClass('hide');
                globalMakeOffer.find('.approveBooking').removeAttr('disabled');

                let propertyAvailabilityCheckURL = getPropertyAvailabilityCheckURL;
                checkInDate = moment.utc(checkInDate);
                checkOutDate = moment.utc(checkOutDate);
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#propertyId', propertyId);
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#checkInDate', checkInDate.format('Y-M-D'));
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#checkOutDate', checkOutDate.format('Y-M-D'));

                OwnerOffer.validation.addRangeRulesToMakeAnOffer();

                $.ajax({
                    type: "GET",
                    url: propertyAvailabilityCheckURL,
                    dataType: "JSON",
                    success: function (msg) {
                        if (!msg.success) {
                            globalMakeOffer.find('.approveBooking').attr('disabled', true);
                            globalMakeOffer.find('.error-label').text('The dates you selected are not available.').removeClass('hide');
                        }
                        hideLoader();
                    }
                });
            },
            getStandardPriceWhileCounterOfferSubmit: function () {
                let globalMakeOffer = $("#makeoffer");
                let $globalRangeDateWithFlexible = globalMakeOffer.find('.counterOffer > .rangeDatesWithFlexible');
                let $globalOwnerRangeStartDate = $globalRangeDateWithFlexible.find("#ownerRangeStartDate");
                let $globalOwnerRangeEndDate = $globalRangeDateWithFlexible.find("#ownerRangeEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                let standardNightlyPrice = globalMakeOffer.find('#makeAnOfferStandardPrice').val();
                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                return HelperFunction.roundNumber(totalStandardNightlyPrice);
            },
            addRangeRulesToMakeAnOffer: function () {

                let globalMakeOffer = $("#makeoffer");

                globalMakeOffer.find('#inputTotalNightlyStandard').rules("remove", "range");

                let $globalRangeDateWithFlexible = globalMakeOffer.find('.counterOffer > .rangeDatesWithFlexible');
                let $globalOwnerRangeStartDate = $globalRangeDateWithFlexible.find("#ownerRangeStartDate");
                let $globalOwnerRangeEndDate = $globalRangeDateWithFlexible.find("#ownerRangeEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                let standardNightlyPrice = globalMakeOffer.find('#makeAnOfferStandardPrice').val();
                let minimumOfferedNightlyPrice = globalMakeOffer.find('#makeAnOfferPropertyNightlyPriceByTripper').val();

                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);

                let totalStandardMinimumPrice = 1;
                if (minimumOfferedNightlyPrice) {
                    totalStandardMinimumPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, minimumOfferedNightlyPrice);
                }

                totalStandardNightlyPrice = HelperFunction.roundNumber(totalStandardNightlyPrice);
                totalStandardMinimumPrice = HelperFunction.roundNumber(totalStandardMinimumPrice);

                globalMakeOffer.find('#inputTotalNightlyStandard').rules("add", {
                    min: (totalStandardMinimumPrice + 10),
                    max: (totalStandardNightlyPrice - 10),
                    messages: {
                        min: 'The proposed total price cannot be less than $' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardMinimumPrice) + 10),
                        max: 'The proposed total price cannot be greater than $' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightlyPrice) - 10),
                    }
                });
            },
            resetMakeAnOfferForm: function () {
                let makeOffer = $("#makeoffer");
                $makeACounterOfferFormValidate.resetForm();
                makeOffer.find('#makeACounterOfferForm')[0].reset();
                makeOffer.find('.approveBooking').removeAttr('disabled');
            }
        },
        offerCardTemplate: function (offerCard) {
            return `<div class="offer-box">
                <div class="image">
                    <img src="`+ OwnerOffer.getFirstPropertyImage(offerCard.property.images) + `" alt="" />
                </div>
                <div class="content">
                    <h3 class="contentTitleH3" data-propertyId="`+ offerCard.property.id + `">` + offerCard.property.title + `<span><img src="` + APP_URL + `/media/images/arrow-right-offer.png" alt="" /></span></h3>
                    `+ OwnerOffer.getRequestedOrOfferedDate(offerCard) + `
                    <div class="row">
                        <div class="col-7">
                            <h4>Standard Rate</h4>
                        </div>
                        <div class="col-5 text-right">
                            <h4>$`+ HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.property.tripperPrice)) + `/night</h4>
                        </div>
                    </div>
                    <div class="row">
                        `+ OwnerOffer.getWhomHasOffered(offerCard) + `
                    </div>
                    `+ OwnerOffer.getStatus(offerCard) + `
                    `+ OwnerOffer.getActionButton(offerCard) + `
                    `+ OwnerOffer.getHiddenPart(offerCard) + `
                </div>
            </div>`;
        },
        getRequestedOrOfferedDate: function (offerCard) {

            let labelH4 = 'Exact Dates';
            if (offerCard.flexibleDates) {
                labelH4 = 'Flexible Dates';
            }

            let tripperOfferStartDate = moment.utc(offerCard.tripperOfferStartDate);
            let tripperOfferEndDate = moment.utc(offerCard.tripperOfferEndDate);
            let ownerOfferStartDate = moment.utc(offerCard.ownerOfferStartDate);
            let ownerOfferEndDate = moment.utc(offerCard.ownerOfferEndDate);
            let mdyOfferShowStartDate = tripperOfferStartDate.format('MMM D');
            let mdyOfferShowEndDate = tripperOfferEndDate.format('MMM D');

            if (
                offerCard.flexibleDates &&
                offerCard.status === propertyOfferSettingData.status.counterOffer &&
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner
            ) {

                if (
                    tripperOfferStartDate.format('YYYY MM DD') !== ownerOfferStartDate.format('YYYY MM DD') ||
                    tripperOfferEndDate.format('YYYY MM DD') !== ownerOfferEndDate.format('YYYY MM DD')
                ) {
                    mdyOfferShowStartDate = ownerOfferStartDate.format('MMM D');
                    mdyOfferShowEndDate = ownerOfferEndDate.format('MMM D');
                }

            }

            return `<div class="row">
                <div class="col-7">
                    <h4>`+ labelH4 + `</h4>
                </div>
                <div class="col-5 text-right">
                    <h4>
                        <span>`+ mdyOfferShowStartDate + `</span>
                        <spa>-</spa>
                        <span>`+ mdyOfferShowEndDate + `</span>
                    </h4>
                </div>
            </div>`;


        },
        getFirstPropertyImage: function (propertyImage) {
            return propertyImage[0].photo;
        },
        createOfferCard: function (offerCard) {

            const offTemp = OwnerOffer.offerCardTemplate(offerCard);

            const offerCardHtml = `
                <li id="offerId`+ offerCard.ownerId + `_` + offerCard.rentalId + `_` + offerCard.propertyId + `_` + offerCard.id + `">
                    `+ offTemp + `
                </li>
            `;

            $(".offer-list ul").append(offerCardHtml);
        },
        createOfferCards: function (offerCards) {
            let offerCardHtml = '';
            $.each(offerCards, function (key, offerCard) {
                OwnerOffer.createOfferCard(offerCard);
            })
        },
        updateOfferCard: function (offerCard) {
            const offTemp = OwnerOffer.offerCardTemplate(offerCard);

            let offerCardDiv = "#offerId" + offerCard.ownerId + "_" + offerCard.rentalId + "_" + offerCard.propertyId + "_" + offerCard.id;
            if ($(offerCardDiv).length === 0) {
                OwnerOffer.createOfferCard(offerCard);
            } else {
                $(offerCardDiv).html(offTemp);
            }

        },
        deleteOfferCard: function (offerCard) {
            const offTemp = OwnerOffer.offerCardTemplate(offerCard);
            $("#offerId" + offerCard.ownerId + "_" + offerCard.rentalId + "_" + offerCard.propertyId + "_" + offerCard.id).remove();
        },
        getStatus: function (offerCard) {
            if (offerCard.status === propertyOfferSettingData.status.pending && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return '<label class="red-label">Tripper Is Waiting For Your Response</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return '<label class="red-label">Tripper Is Waiting For Your Response</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return '<label class="orange-label">Tripper Has Declined Your Offer</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.accepted && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return '<label class="green-label">Your Offer Was Accepted By Tripper!</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.accepted && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return '<label class="green-label">You\'ve Accepted Tripper\'s Offer</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return '<label class="red-label">Your Counter Offer Is Pending On Tripper</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return '<label class="orange-label">You\'ve Cancelled Tripper\'s Offer</label>';
            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.system) {
                return '<label class="orange-label">Booking Requested By Another Tripper</label>';
            }
            return '';
        },
        getWhomHasOffered: function (offerCard) {

            let offeredLabel = 'Tripper\'s Offer';
            let offeredPrice = offerCard.tripperOfferNightlyPrice;

            if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                offeredLabel = 'You Offered';
                offeredPrice = offerCard.ownerOfferNightlyPrice;
            }

            return `
                <div class="col-7">
                    <h4><b>`+ offeredLabel + `</b></h4>
                </div>
                <div class="col-5 text-right">
                    <h4><b>$`+ HelperFunction.numberWithCommas(HelperFunction.roundNumber(offeredPrice)) + `/night</b></h4>
                </div>
            `;

        },
        getActionButton: function (offerCard) {
            if (
                (
                    offerCard.status === propertyOfferSettingData.status.pending ||
                    offerCard.status === propertyOfferSettingData.status.counterOffer ||
                    offerCard.status === propertyOfferSettingData.status.cancel
                ) && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return `<a href="javascript:void(0);" onclick="OwnerOffer.acceptOffer(` + offerCard.ownerId + `,` + offerCard.propertyId + `,` + offerCard.rentalId + `,` + offerCard.id + `)" class="red-btn">Accept Offer for $` + HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.tripperOfferNightlyPrice)) + `</a>`;
            } else if (
                (
                    offerCard.status === propertyOfferSettingData.status.counterOffer ||
                    offerCard.status === propertyOfferSettingData.status.cancel
                ) && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<a href="javascript:void(0);" onclick="OwnerOffer.acceptOffer(` + offerCard.ownerId + `,` + offerCard.propertyId + `,` + offerCard.rentalId + `,` + offerCard.id + `)" class="red-btn">Accept Offer for $` + HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.tripperOfferNightlyPrice)) + `</a>`;
            } else if (offerCard.status === propertyOfferSettingData.status.accepted && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<div class="offer-accepted">
                    <div class="row">
                        <div class="col-5">
                            <h4><b>Offer Accepted</b></h4>
                            <h5>Now awaiting booking request</h5>
                        </div>
                        <div class="col-7">
                            <label class="red-label">Pending on Tripper</label>
                            <div class="text-center">
                                <a class="cancel text-right" href="javascript:void(0);" onclick="OwnerOffer.openCancelOfferPopup(`+ offerCard.ownerId + `,` + offerCard.propertyId + `,` + offerCard.rentalId + `,` + offerCard.id + `)">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>`;
            } else if (
                offerCard.status === propertyOfferSettingData.status.accepted &&
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental
            ) {

                let bookingRedirectAnchor = '';
                if (offerCard.booking) {
                    if (
                        offerCard.booking.ownerStatus === 'auto_reject' ||
                        offerCard.booking.ownerStatus === 'rejected'
                    ) {
                        bookingRedirectAnchor = `<a  href="javascript:void(0);" onclick="$('#pending-tab').trigger('click');" class="red-btn">Go to Cancelled Booking</a>`;
                    } else if (offerCard.booking.ownerStatus === 'pending') {
                        bookingRedirectAnchor = `<a  href="javascript:void(0);" onclick="$('#pending-tab').trigger('click');" class="red-btn">Go to Booking Request</a>`;
                    } else if (
                        offerCard.booking.ownerStatus === 'approved' ||
                        offerCard.booking.ownerStatus === 'instant_booking'
                    ) {
                        bookingRedirectAnchor = `<a  href="javascript:void(0);" onclick="$('#confirmed-tab').trigger('click');" class="red-btn">Go to Accepted Booking</a>`;
                    } else if (
                        offerCard.booking.ownerStatus === 'confirmed'
                    ) {
                        bookingRedirectAnchor = `<a  href="javascript:void(0);" onclick="$('#confirmed-tab').trigger('click');" class="red-btn">Go to Confirmed Booking</a>`;
                    } else if (offerCard.booking.ownerStatus === 'cancelled') {
                        bookingRedirectAnchor = `<a  href="javascript:void(0);" onclick="$('#confirmed-tab').trigger('click');" class="red-btn">Go to Cancelled Request</a>`;
                    }
                }

                return bookingRedirectAnchor;

            }
            return '';
        },
        getHiddenPart: function (offerCard) {
            if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental &&
                offerCard.status !== propertyOfferSettingData.status.accepted
            ) {
                return `<div class="hidden-part">
                    <a href="javascript:void(0);" onclick="OwnerOffer.openMakeAnOfferCounterOfferPopup(`+ offerCard.id + `)" class="grey-btn">Make Counter Offer</a>
                    <a href="javascript:void(0);" onclick="OwnerOffer.openDeclineOfferPopup(`+ offerCard.ownerId + `,` + offerCard.propertyId + `,` + offerCard.rentalId + `,` + offerCard.id + `)" class="grey-btn">Decline This Offer</a>
                </div>`;
            }
            return '';
        },
        openMakeAnOfferCounterOfferPopup: function (propertyOfferId) {

            showLoader();
            let makeOffer = $("#makeoffer");

            OwnerOffer.validation.resetMakeAnOfferForm();

            let propertyOfferByIdURL = getPropertyOfferByIdURL;
            propertyOfferByIdURL = propertyOfferByIdURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "GET",
                url: propertyOfferByIdURL,
                dataType: "JSON",
                success: function (msg) {
                    if ($.isEmptyObject(msg.error)) {

                        let offerStartDate = moment.utc(msg.data.tripperOfferStartDate);
                        let offerEndDate = moment.utc(msg.data.tripperOfferEndDate);
                        let rangeDates = offerStartDate.format('MM/DD/YYYY') + ' to ' + offerEndDate.format('MM/DD/YYYY');
                        let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);
                        let tripperOfferNightlyPrice = msg.data.tripperOfferNightlyPrice;
                        let totalOfferedNightsToStayPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, tripperOfferNightlyPrice);

                        let standardNightlyPrice = msg.data.property.tripperPrice;
                        let totalStandardNightsToStayPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);

                        makeOffer.find('.tripperData > .rangeDates').text(rangeDates);
                        makeOffer.find('.tripperData > .totalNight').text(totalNightsToStay);
                        makeOffer.find('.tripperData > .singleNightlyStandard').text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(tripperOfferNightlyPrice)));
                        makeOffer.find('.tripperData > .totalNightlyStandard').text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalOfferedNightsToStayPrice)));

                        makeOffer.find('.standardData > .rangeDates').text(rangeDates)
                        makeOffer.find('.standardData > .totalNight').text(totalNightsToStay);
                        makeOffer.find('.standardData > .singleNightlyStandard').text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(standardNightlyPrice)));
                        makeOffer.find('.standardData > .totalNightlyStandard').text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightsToStayPrice)));

                        let $rangeDateWithFlexible = makeOffer.find('.counterOffer > .rangeDatesWithFlexible');
                        $rangeDateWithFlexible.find("#ownerRangeStartDate").datepicker('setDate', HelperFunction.convertToDateObject(offerStartDate));
                        $rangeDateWithFlexible.find("#ownerRangeEndDate").datepicker('setDate', HelperFunction.convertToDateObject(offerEndDate));
                        let dateEndDateSet = HelperFunction.convertToDateObject(offerStartDate);
                        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
                        $rangeDateWithFlexible.find("#ownerRangeEndDate").datepicker('setStartDate', dateEndDateSet);
                        if (msg.data.flexibleDates) {
                            $rangeDateWithFlexible.find(".datepic-inputgroup").removeClass('disabled');
                            $rangeDateWithFlexible.find("#ownerRangeStartDate").attr('disabled', false);
                            $rangeDateWithFlexible.find("#ownerRangeEndDate").attr('disabled', false);
                        } else {
                            $rangeDateWithFlexible.find(".datepic-inputgroup").addClass('disabled');
                            $rangeDateWithFlexible.find("#ownerRangeStartDate").attr('disabled', true);
                            $rangeDateWithFlexible.find("#ownerRangeEndDate").attr('disabled', true);
                        }

                        makeOffer.find('.counterOffer > .totalNight').text(totalNightsToStay);
                        makeOffer.find('.counterOffer > .singleNightlyStandard').text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(standardNightlyPrice)));
                        makeOffer.find('.counterOffer > .totalNightlyStandard  #inputTotalNightlyStandard').val('');

                        makeOffer.find('#makeAnOfferOwnerId').val(msg.data.ownerId);
                        makeOffer.find('#makeAnOfferRentalId').val(msg.data.rentalId);
                        makeOffer.find('#makeAnOfferPropertyId').val(msg.data.propertyId);
                        makeOffer.find('#makeAnOfferPropertyOfferId').val(msg.data.id);

                        makeOffer.find('#makeAnOfferPropertyOfferStartDateByTripper').val(msg.data.tripperOfferStartDate);
                        makeOffer.find('#makeAnOfferPropertyOfferEndDateByTripper').val(msg.data.tripperOfferEndDate);

                        makeOffer.find('#makeAnOfferPropertyOfferStartDateByOwner').val(msg.data.ownerOfferStartDate);
                        makeOffer.find('#makeAnOfferPropertyOfferEndDateByOwner').val(msg.data.ownerOfferEndDate);

                        makeOffer.find('#makeAnOfferPropertyNumberOfGuests').val(msg.data.numberOfGuests);
                        makeOffer.find('#makeAnOfferPropertyNightlyPriceByTripper').val(msg.data.tripperOfferNightlyPrice);
                        makeOffer.find('#makeAnOfferPropertyIsFlexibleDate').val(msg.data.flexibleDates);
                        makeOffer.find('#makeAnOfferStandardPrice').val(msg.data.property.tripperPrice);
                        makeOffer.find('#makeAnOfferPropertyMinimumOffer').val(msg.data.property.offerPrice);
                        makeOffer.find('#makeAnOfferPropertyActualNightlyPrice').val(msg.data.property.price);

                        makeOffer.modal('show');
                        hideLoader();
                    }
                }
            });

        },
        openCancelOfferPopup: function (ownerId, propertyId, rentalId, propertyOfferId) {
            let cancelAcceptance = $("#cancelAcceptance");
            cancelAcceptance.find('#cancelOfferPopupOwnerId').val(ownerId);
            cancelAcceptance.find('#cancelOfferRentalId').val(rentalId);
            cancelAcceptance.find('#cancelOfferPropertyId').val(propertyId);
            cancelAcceptance.find('#cancelOfferPopupPropertyOfferId').val(propertyOfferId);
            cancelAcceptance.modal('show');
        },
        openDeclineOfferPopup: function (ownerId, propertyId, rentalId, propertyOfferId) {
            let declineOfferPop = $("#declineOfferPop");
            declineOfferPop.find('#declineOfferPopupOwnerId').val(ownerId);
            declineOfferPop.find('#declineOfferRentalId').val(rentalId);
            declineOfferPop.find('#declineOfferPropertyId').val(propertyId);
            declineOfferPop.find('#declineOfferPopupPropertyOfferId').val(propertyOfferId);
            declineOfferPop.modal('show');
        },
        acceptOffer: function (ownerId, propertyId, rentalId, propertyOfferId) {

            showLoader();
            let ownerAcceptPropertyOfferURL = getOwnerAcceptPropertyOfferURL;
            ownerAcceptPropertyOfferURL = ownerAcceptPropertyOfferURL.replace('#propertyId', propertyId);
            ownerAcceptPropertyOfferURL = ownerAcceptPropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: ownerAcceptPropertyOfferURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        OwnerOffer.updateOfferCard(msg.data);
                        hideLoader();
                    }
                }
            });
        },
        declineOffer: function () {

            showLoader();
            let declineOfferPop = $("#declineOfferPop");
            let ownerId = declineOfferPop.find('#declineOfferPopupOwnerId').val();
            let rentalId = declineOfferPop.find('#declineOfferRentalId').val();
            let propertyId = declineOfferPop.find('#declineOfferPropertyId').val();
            let propertyOfferId = declineOfferPop.find('#declineOfferPopupPropertyOfferId').val();

            let ownerDeclinePropertyOfferURL = getOwnerDeclinePropertyOfferURL;
            ownerDeclinePropertyOfferURL = ownerDeclinePropertyOfferURL.replace('#propertyId', propertyId);
            ownerDeclinePropertyOfferURL = ownerDeclinePropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: ownerDeclinePropertyOfferURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        OwnerOffer.deleteOfferCard(msg.data);
                        hideLoader();
                    }
                    declineOfferPop.modal('hide');
                }
            });
        },
        cancelOffer: function () {

            showLoader();
            let cancelAcceptance = $("#cancelAcceptance");
            let ownerId = cancelAcceptance.find('#cancelOfferPopupOwnerId').val();
            let rentalId = cancelAcceptance.find('#cancelOfferRentalId').val();
            let propertyId = cancelAcceptance.find('#cancelOfferPropertyId').val();
            let propertyOfferId = cancelAcceptance.find('#cancelOfferPopupPropertyOfferId').val();

            let ownerCancelPropertyOfferURL = getOwnerCancelPropertyOfferURL;
            ownerCancelPropertyOfferURL = ownerCancelPropertyOfferURL.replace('#propertyId', propertyId);
            ownerCancelPropertyOfferURL = ownerCancelPropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: ownerCancelPropertyOfferURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        OwnerOffer.updateOfferCard(msg.data);
                        hideLoader();
                    }
                    cancelAcceptance.modal('hide');
                }
            });
        },
        createCounterOffer: function () {

            let makeOffer = $("#makeoffer");
            let makeACounterOfferForm = makeOffer.find("#makeACounterOfferForm");
            let proposedTotal = makeOffer.find('#inputTotalNightlyStandard').val();

            OwnerOffer.validation.addRangeRulesToMakeAnOffer();

            if (!makeACounterOfferForm.valid()) {
                return false;
            }

            let ownerId = makeOffer.find('#makeAnOfferOwnerId').val();
            let rentalId = makeOffer.find('#makeAnOfferRentalId').val();
            let propertyId = makeOffer.find('#makeAnOfferPropertyId').val();
            let propertyOfferId = makeOffer.find('#makeAnOfferPropertyOfferId').val();
            let isPropertyOfferDateFlexible = makeOffer.find('#makeAnOfferPropertyIsFlexibleDate').val();
            let propertyTripperOfferStartDate = makeOffer.find('#makeAnOfferPropertyOfferStartDateByTripper').val();
            let propertyTripperOfferEndDate = makeOffer.find('#makeAnOfferPropertyOfferEndDateByTripper').val();
            let numberOfGuests = makeOffer.find('#makeAnOfferPropertyNumberOfGuests').val();
            let propertyNightlyPriceByTripper = makeOffer.find('#makeAnOfferPropertyNightlyPriceByTripper').val();
            let timezone = makeOffer.find('#timezone').val();
            let propertyOwnerOfferStartDate = '';
            let propertyOwnerOfferEndDate = '';

            if (isPropertyOfferDateFlexible != '') {
                propertyOwnerOfferStartDate = $globalOwnerRangeStartDate.val();
                propertyOwnerOfferEndDate = $globalOwnerRangeEndDate.val();
            }

            showLoader();
            let localCounterOfferPropertyURL = counterOfferPropertyURL;
            localCounterOfferPropertyURL = localCounterOfferPropertyURL.replace('#propertyId', propertyId);
            localCounterOfferPropertyURL = localCounterOfferPropertyURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: localCounterOfferPropertyURL,
                dataType: "JSON",
                data: {
                    'rentalId': rentalId,
                    'ownerOfferStartDate': moment.utc(propertyOwnerOfferStartDate).format('Y-M-D'),
                    'ownerOfferEndDate': moment.utc(propertyOwnerOfferEndDate).format('Y-M-D'),
                    'tripperOfferStartDate': moment.utc(propertyTripperOfferStartDate).format('Y-M-D'),
                    'tripperOfferEndDate': moment.utc(propertyTripperOfferEndDate).format('Y-M-D'),
                    'tripperOfferNightlyPrice': propertyNightlyPriceByTripper,
                    'ownerOfferProposedTotal': proposedTotal,
                    'numberOfGuests': numberOfGuests,
                    'timezone': timezone
                },
                success: function (msg) {
                    if (msg.success == 'false' || msg.success === false) {
                        makeOffer.find('.error-label').text(msg.message).removeClass('hide');
                    } else {
                        OwnerOffer.updateOfferCard(msg.data);
                        makeOffer.modal('hide');
                    }
                    hideLoader();
                }
            });
        },
        getOffers: function () {

            showLoader();
            $.ajax({
                type: "GET",
                url: getOwnerPropertyOffersURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        OwnerOffer.createOfferCards(msg.data);
                    }
                }
            });
        },
        getPropertyOfferById: function (propertyOfferId) {
            showLoader();

            let propertyOfferByIdURL = getPropertyOfferByIdURL;
            propertyOfferByIdURL = propertyOfferByIdURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "GET",
                url: propertyOfferByIdURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        OwnerOffer.createOfferCards(msg.data);
                    }
                }
            });
        }
    });

    OwnerOffer.getOffers();

    $("ul#listingtab").on('click', '#offers-tab', function (e) {
        //closeAlertBar();
        //OwnerOffer.getOffers();
    });

    $("ul#listingtab").on('click', 'li a', function (e) {
        window.location.hash = $(this).attr('href');

        if ($(this).attr('href') === '#offers') {
            PropertyOfferNotification.hideAlertBar();
        } else {
            PropertyOfferNotification.showHideAlertBarAtOfferPage();
        }
    });

    if (window.location.hash === '#offers') {
        $('#offers-tab').trigger('click');
    } else if (window.location.hash === '#confirmed') {
        $('#confirmed-tab').trigger('click');
    } else {
        $('#pending-tab').trigger('click');
    }

    pusherChannel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function (data) {
        if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.new) {
            OwnerOffer.createOfferCard(data.propertyOffer);
        } else if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.newOnExisting) {
            OwnerOffer.updateOfferCard(data.propertyOffer);
        } else if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.counterOffer) {
            OwnerOffer.updateOfferCard(data.propertyOffer);
        } else if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.deleted) {
            OwnerOffer.deleteOfferCard(data.propertyOffer);
        } else if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.cancelled) {
            OwnerOffer.updateOfferCard(data.propertyOffer);
        } else if (data.type === PusherEventNotificationTypeConstant.propertyOffer.tripper.accepted) {
            OwnerOffer.updateOfferCard(data.propertyOffer);
        }
    });
});
