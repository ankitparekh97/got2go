$(document).ready(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: timesharelist,
        type: 'POST',
        data: { _token: CSRF_TOKEN },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (data) {
            console.log(data);
        }
    });

    if ($('.timeshare-approval-container').next(".list-container").length == 0) {
        $('.timeshare-approval-container').css('display', 'none');
    }
    if ($('.timeshare-pending-container').next(".list-container").length == 0) {
        $('.timeshare-pending-container').css('display', 'none');
    }


});

function destroy(id) {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: propertydestroy,
        type: 'DELETE',
        data: {
            _token: CSRF_TOKEN,
            id: id
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.success)
                $('#delete-listing').modal('hide');
                $('table tr[data-row='+ id +']').remove();
                $("html, body").animate({ scrollTop: 200 }, "slow");

                let type = (data.property.type_of_property == 'property') ? 'Private Residence' : 'Vacation Rental';
                Swal.fire({
                    icon: "success",
                    title: type+" deleted successfully!",
                    showConfirmButton: false,
                    timer: 3000
                });
        }
    });
}

function publish(id) {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    showLoader();
    $.ajax({
        url: propertypublish,
        type: 'POST',
        data: {
            id: id,
            _token: CSRF_TOKEN
        },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (data) {
            hideLoader();
            if (data.success) {
                if (data.property.publish == 1) {
                    $('table tr[data-row='+ id +']').find('#publish-listing').text('Unpublish');
                }
                else {
                    $('table tr[data-row='+ id +']').find('#publish-listing').text('Publish');
                }
                $('table tr[data-row='+ id +']').find('#publish-listing').attr('data-publish', data.property.publish);
                $("#publish").modal('hide');
            }
        }
    });
}
