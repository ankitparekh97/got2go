// Class definition

var KTWizard6 = function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizard;
    var _validations = [];

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        _wizard = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: true  // allow step clicking
        });

        // Validation before going to next page
        _wizard.on('beforeNext', function (wizard) {
            // Don't go to the next step yet
            _wizard.stop();

            // Validate form
            var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    _wizard.goNext();
                    KTUtil.scrollTop();

                } else {
                    Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        });

        // Change event
        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }

    var initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    propertyTypeId: {
                        validators: {
                            notEmpty: {
                                message: 'Property Type is required'
                            }
                        }
                    },
                    hotel: {
                        validators: {
                            notEmpty: {
                                message: 'Hotel is required'
                            }
                        }
                    },
                    location: {
                        validators: {
                            // notEmpty: {
                            // 	message: 'Location is required'
                            // },
                            callback: {
                                callback: function (input) {
                                    if ($.trim(input.value)) {
                                        return {
                                            valid: true,       // or true
                                            message: ''
                                        };
                                    }
                                    return {
                                        valid: false,       // or true
                                        message: 'Location is required'
                                    };

                                }
                            }
                        }
                    },
                    description: {
                        validators: {
							/* notEmpty: {
								message: 'Description is required'
                     }, */
                            callback: {
                                callback: function (input) {
                                    if ($.trim(input.value)) {
                                        return {
                                            valid: true,       // or true
                                            message: ''
                                        };
                                    }
                                    return {
                                        valid: false,       // or true
                                        message: 'Description is required'
                                    };

                                }
                            }
                        },

                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                }
            }
        ));

        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    structureType: {
                        validators: {
                            notEmpty: {
                                message: 'Structure Type is required'
                            }
                        }
                    },
                    noOfGuest: {
                        validators: {
                            notEmpty: {
                                message: 'No of Guest is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                            }
                        }
                    },
                    unitSize: {
                        validators: {
                            notEmpty: {
                                message: 'Unit Size is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                            }
                        }
                    },
                    checkinDate: {
                        validators: {
                            notEmpty: {
                                message: 'Date is required'
                            },
                            callback: {
                                callback: function (input) {

                                    var today = moment().format('MM/DD/YYYY');
                                    if (moment(input.value).isBefore(today)) {
                                        return {
                                            valid: false,       // or true
                                            message: 'Please select valid check-in date'
                                        };
                                    }
                                    return {
                                        valid: true,       // or true
                                        message: ''
                                    };
                                }
                            },
                        }
                    },
                    checkoutDate: {
                        validators: {
                            notEmpty: {
                                message: 'Date is required'
                            },
                            callback: {
                                callback: function (input) {
                                    console.log(input.value);
                                    console.log(moment().format('MM/DD/YYYY'));
                                    startDate = _formEl.querySelector('[name="checkinDate"]').value;
                                    if (startDate == '') {
                                        return {
                                            valid: false,       // or true
                                            message: 'Please select check-in date first'
                                        };
                                    }
                                    else if (moment(input.value).isBefore(startDate)) {
                                        return {
                                            valid: false,       // or true
                                            message: 'Checkout Date should be greater than check-in date'
                                        };
                                    }
                                    return {
                                        valid: true,       // or true
                                        message: ''
                                    };
                                }
                            },
                        }
                    },
                    minDays: {
                        validators: {
                            notEmpty: {
                                message: 'Minimum Days is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                            },
                            greaterThan: {
                                message: 'The value must be greater than or equal to 1',
                                min: 1,
                            },
                        }
                    },
                    bookingBeforeDays: {
                        validators: {
                            notEmpty: {
                                message: 'Booking Before Days is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                            },
                            greaterThan: {
                                message: 'The value must be greater than or equal to 1',
                                min: 1,
                            },
                        }
                    },
                    timeshareDescripion: {
                        validators: {
							/* notEmpty: {
								message: 'Timeshare Description is required'
                     }, */
                            callback: {
                                callback: function (input) {
                                    if ($.trim(input.value)) {
                                        return {
                                            valid: true,       // or true
                                            message: ''
                                        };
                                    }
                                    return {
                                        valid: false,       // or true
                                        message: 'Timeshare Description is required'
                                    };

                                }
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                }
            }
        ));

        // Step 3
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    price: {
                        validators: {
                            notEmpty: {
                                message: 'Price is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                                // The default separators
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            },
                            greaterThan: {
                                message: 'The value must be greater than or equal to 1',
                                min: 1,
                            }
                        }
                    },
                    partialPyamentAmount: {
                        validators: {
							/* notEmpty: {
								message: 'Partial Payment is required'
							}, */
                            numeric: {
                                message: 'The value is not a number',
                                // The default separators
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            },
                            /* greaterThan: {
                                  message: 'The value must be greater than or equal to 1',
                                  min: 1,
                            }, */
                            callback: {
                                message: 'Booking amount should be less than actual booking price.',
                                callback: function (input) {
                                    const price = _formEl.querySelector('[name="price"]').value;
                                    return input.value <= price;
                                }
                            },
                            callback: {
                                callback: function (input) {
                                    const partial = _formEl.querySelector('[name="partialPyament"]').value;
                                    if (partial == '1' && input.value == '') {
                                        return {
                                            valid: false,       // or true
                                            message: 'Partial Payment is required'
                                        };
                                    }
                                    else if (partial == '1' && input.value < 1) {
                                        return {
                                            valid: false,       // or true
                                            message: 'The value must be greater than or equal to 1'
                                        };
                                    }
                                    return {
                                        valid: true,       // or true
                                        message: ''
                                    };

                                }
                            },
                            different: {
                                compare: function () {
                                    return _formEl.querySelector('[name="price"]').value;
                                },
                                message: 'Booking amount and price cannot be the same as each other.'
                            }
                        }
                    },
                    ltdTimeFrame: {
                        validators: {
                            // notEmpty: {
                            // 	message: 'Time Frame is required'
                            // },
                            numeric: {
                                message: 'The value is not a number',
                            }
                        }
                    },
                    ltdTimeValue: {
                        validators: {
                            // notEmpty: {
                            // 	message: 'Discount Value is required'
                            // },
                            numeric: {
                                message: 'The value is not a number',
                                // The default separators
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            },
                            greaterThan: {
                                message: 'The value must be greater than or equal to 1',
                                min: 1,
                            },
                        }
                    },
                    membershipNumber: {
                        validators: {
                            notEmpty: {
                                message: 'Membership is required'
                            },
                            numeric: {
                                message: 'The value is not a number',
                            }
                        }
                    },
                    cancellationType: {
                        validators: {
                            notEmpty: {
                                message: 'Cancellation Type is required'
                            }
                        }
                    },
                    document: {
                        validators: {
                            notEmpty: {
                                message: 'Document is required'
                            },
                            file: {
                                extension: 'jpeg,jpg,png,doc,docx,rtf,pdf',
                                type: 'image/jpg,image/jpeg,image/png,application/msword,application/pdf,application/rtf',
                                message: 'The selected document is not valid (Only jpeg,jpg,png,doc,docx,pdf,rtf are allowed)',
                                // maxSize: 1048576,   // 2048 * 1024
                                //message: 'The selected document is not valid'
                            },

                        },
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        ).on('core.form.valid', function () {
            $('#loader').show();
            var wizardForm = document.getElementById('kt_wizard_form');
            $.ajax({
                type: "POST",
                url: "{{ route('timeshare.store') }}",
                dataType: "JSON",
                headers: {
                    "Authorization": Authorization,
                },
                data: new FormData(wizardForm),
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#loader').hide();
                    if ($.isEmptyObject(data.error)) {

                        window.location.href = data.redirect_url;
                    } else {
                        $(".alert-danger").find("ul").html('');
                        $(".alert-danger").css('display', 'block');
                        $.each(data.error, function (key, value) {
                            $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                        });

                        $('html, body').animate({
                            scrollTop: $(".alert-danger").offset().top
                        }, 2000);
                    }

                },
            });
        }));
    }

    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('kt_wizard');
            _formEl = KTUtil.getById('kt_wizard_form');


            initWizard();
            initValidation();
            $('#kt_datepicker_2').on('change', function (e) {

                var validator = _validations[1]; // get validator for currnt step

                validator.revalidateField('checkinDate');
                validator.revalidateField('checkoutDate');

                var checkin = $(this).val()

                $('#kt_datepicker_3').datepicker('setStartDate', checkin);

            });

            $('#kt_datepicker_3').on('change', function (e) {

                var validator = _validations[1]; // get validator for currnt step

                validator.revalidateField('checkinDate');
                validator.revalidateField('checkoutDate');

            });
        }
    };
}();


var geocoder;
var map_g;
var address = "New York";
var geocoder;
function initMap() {
    map_g = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        mapTypeId: "roadmap",
        center: { lat: -34.397, lng: 150.644 },
        disableDefaultUI: true
    });
    geocoder = new google.maps.Geocoder();
    codeAddress(geocoder, map_g, address);
}

function codeAddress(geocoder, map_g, address) {
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status === 'OK') {
            map_g.setCenter(results[0].geometry.location);


            var marker = new google.maps.Marker({
                map: map_g,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}


// TouchSpin
var KTKBootstrapTouchspin = function () {

    // Private functions
    var demos = function () {
        $('#price').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            min: 1,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });

        $('#number_guest,#unit_size,#min_days,#bookingBeforeDays').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            min: 1,
            max: 50,
            stepinterval: 50,
            maxboostedstep: 10000000,
        });

        $('#partial_payment_amount').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            min: 0,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
    }

    var validationStateDemos = function () {
        // validation state demos
        $('#kt_touchspin_1_validate').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });

        // vertical buttons with custom icons:
        $('#kt_touchspin_2_validate').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',

            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
        });

        $('#kt_touchspin_3_validate').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            verticalbuttons: true,
            verticalupclass: 'ki ki-plus',
            verticaldownclass: 'ki ki-minus'
        });
    }

    return {
        // public functions
        init: function () {
            demos();
            validationStateDemos();
        }
    };
}();



$(document).ready(function () {

    KTWizard6.init();

    KTKBootstrapTouchspin.init();
    //Get Hotel LIst
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: ownerhotellist,
        type: 'POST',
        data: { _token: CSRF_TOKEN },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (result) {
            $("#hotels").empty();
            var html = '<option value="" selected>Select Hotel</option>';
            $.each(result.data, function (key, row) {
                html += '<option value="' + row.id + '">' + row.name + '</option>';
            });
            $("#hotels").html(html);
        }
    });
    $.ajax({
        url: ownerpropertytypelist,
        type: 'POST',
        data: { _token: CSRF_TOKEN },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (result) {
            $("#property_type").empty();
            var html = '<option value="" selected>Select Property Type</option>';
            $.each(result.data, function (key, row) {
                html += '<option value="' + row.id + '">' + row.property_type + '</option>';
            });
            $("#property_type").html(html);
        }
    });

    $('#hotels').on('change', function () {
        $.ajax({
            url: ownerhotelbyid,
            type: 'POST',
            data: { id: this.value },
            dataType: 'JSON',
            headers: {
                "Authorization": Authorization,
            },
            success: function (result) {
                $('#location_detail').val(result.data.location);
                $('#description').val(result.data.description);
                $(".resort-amenities").empty();
                var html = '';
                $.each(result.data.propertyamenities, function (key, row) {
                    html += '<div class="col-md-6"><div class="form-group"><label class="checkbox"><input type="checkbox" checked="checked" name="resort_aminities[]" value="' + row.masteramenity.id + '" class="invisible"><span></span>' + row.masteramenity.amenity_name + '</label></div></div>';
                    //html += '<div class="col-md-6"><div class="form-group"><label class="checkbox">'+row.masteramenity.amenity_name+'</label></div></div>';
                });
                $(".resort-amenities").html(html);
                if (result.data.location) {
                    codeAddress(geocoder, map_g, result.data.location);
                }
                $('#location_detail').prop('readonly', true);
                $('#description').prop('readonly', true);
                //   /$("input[name='resort_aminities']").attr("disabled", true);

            }
        });
    });

    $.ajax({
        url: owneramenitieslist,
        type: 'POST',
        data: { _token: CSRF_TOKEN },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (result) {
            $(".unit-amenities").empty();
            var html = '';
            $.each(result.data, function (key, row) {
                html += '<div class="col-md-6"><div class="form-group"><label class="checkbox"><input type="checkbox" name="unit_aminities[]" value="' + row.id + '" checked="checked"><span></span>' + row.amenity_name + '</label></div></div>';
            });
            $(".unit-amenities").html(html);
        }
    });

    $('#partial_payment').on('change', function () {
        this.value = this.checked ? 1 : 0;
        // alert(this.value);
        if (this.value == 1) {
            $('.partial-payment-amount').show();
        } else {
            $('.partial-payment-amount').hide();
        }
    }).change();

    $(".repeat").on('click', function (e) {
        var c = $('.base-group').clone();
        c.removeClass('base-group').css('display', 'block').addClass("child-group");
        $(".main-group").append(c);
    });


    $('#kt_wizard_form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        // /if(!$form.valid()) return false;
        $.ajax({
            type: "POST",
            url: timesharestore,
            dataType: "JSON",
            headers: {
                "Authorization": Authorization,
            },
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                if ($.isEmptyObject(data.error)) {

                    window.location.href = data.redirect_url;
                } else {
                    $(".alert-danger").find("ul").html('');
                    $(".alert-danger").css('display', 'block');
                    $.each(data.error, function (key, value) {
                        $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                    });

                    $('html, body').animate({
                        scrollTop: $(".alert-danger").offset().top
                    }, 2000);
                }

            },
        });
    });
    $('#hotelrequest').validate({
        ignore: ".ignore",
        rules: {
            property_type: {
                required: true,

            },
            location: {
                required: true,
            },
            hotelname: {
                required: true,
            },
            state: {
                required: true,
            },
            country: {
                required: true,
            }
        },
        messages: {
            property_type: {
                required: 'Please select Property Type.',

            },
            location: {
                required: 'Please enter Location.',
            },
            hotelname: {
                required: 'Please enter Hotel Name.',
            },
            state: {
                required: 'Please enter State.',
            },
            country: {
                required: 'Please enter Country.'
            }
        }
    });
});

