function destroy(timeshare_id) {

    if (confirm('Are you sure you want to delete Timeshare?')) {
        var timesharedestroy = timesharedestroy
        timesharedestroy = timesharedestroy.replace(':id', timeshare_id);

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: timesharedestroy,
            type: 'DELETE',
            data: { _token: CSRF_TOKEN },
            dataType: 'JSON',
            headers: {
                "Authorization": Authorization,
            },
            success: function (data) {
                if (data.success) {
                    console.log(data);
                    window.location = data.redirect_url;
                }

            }
        });
    }
}