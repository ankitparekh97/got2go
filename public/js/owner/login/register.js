$(document).ready(function () {
    $('#ownersignup').validate({
        ignore: ".ignore",
        rules: {
            name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                minlength: 10,
                maxlength: 11
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            confirm_password: {
                required: true,
                equalTo: "#password",
                minlength: 6,
                maxlength: 20
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            name: {
                required: "First Name is required"
            },
            last_name: {
                required: "Last Name is required"
            },
            email: {
                required: "Email is required",
                email: "Please enter valid email address"
            },
            phone: {
                required: "Phone number is required",
                minlength: "Phone should be minimum 11 digits",
                maxlength: "Phone should be maximum 11 digits",
            },
            password: {
                required: "Password field is required",
                equalTo: "Password and Confirm Password field should be same",
                minlength: "Password should be minimum 6 characters",
                maxlength: "Password must be maximum 20 characters",
            },
            confirm_password: {
                required: "Confirm Password field is required",
                equalTo: "Password and Confirm Password field should be same",
                //minlength: "Confirm Password should be minimum 8 characters",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr('id') == "terms") {
                var parentDiv = $(element).parent();
                error.insertAfter(parentDiv);
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery.validator.addMethod("passwordCheck",
        function (value, element, param) {
            if (this.optional(element)) {
                return true;
            } else if (!/[A-Z]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            } else if (!/[0-9]/.test(value)) {
                return false;
            } else if (!/[!@#\$%\^\&*\)\(+=._-]/.test(value)) {
                return false;
            }

            return true;
        },
        "Password should contain combination of uppercase, lowercase, digits & special characters");

    function recaptchaCallback() {
        $('#hiddenRecaptcha').valid();
    };

    $('#ownersignup').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        if (!$form.valid()) return false;
        $('#loader').show();
        $.ajax({
            type: "POST",
            url: ownersignup,
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (msg) {
                $('#loader').hide();
                if ($.isEmptyObject(msg.error)) {
                    window.location.href = msg.validateOtpUrl;
                } else {

                    $(".alert-danger").find("ul").html('');
                    $(".alert-danger").css('display', 'block');
                    $.each(msg.error, function (key, value) {
                        $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                    });
                }
            }
        });
    });
});