$('#profile_update').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: profilebasic,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            "Authorization": Authorization,
        },
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function (msg) {

        }
    });
});



$('#profile_change_password').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: profilechangepassword,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            "Authorization": Authorization
        },
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function (msg) {

        }
    });
});