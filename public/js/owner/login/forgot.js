$(document).ready(function () {
    $('#ownerforgot').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Please enter valid email address"
            },
        }
    });

    $('#ownerforgot').on('submit', function (e) {
        //console.log(5);
        e.preventDefault();
        var $form = $(this);
        //console.log($("#ownerlogin").valid());
        if (!$form.valid()) return false;
        $('#loader').show();
        $.ajax({
            type: "POST",
            url: ownerforgotpassword,
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (msg) {
                $('#loader').hide();
                if ($.isEmptyObject(msg.error)) {

                    window.location.href = ownerforgot;
                } else {

                    $(".alert-danger").find("ul").html('');
                    $(".alert-danger").css('display', 'block');
                    $.each(msg.error, function (key, value) {
                        $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                    });
                }

            }
        });
    });
});