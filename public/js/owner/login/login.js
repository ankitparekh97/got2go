$(document).ready(function () {
    $('#ownerlogin').validate({
        ignore: ".ignore",
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Please neter valid email address"
            },
            password: {
                required: "Password is Required",
                //max:8
            },
        }
    });

    function recaptchaCallback() {
        $('#hiddenRecaptcha').valid();
    };

    $('#ownerlogin').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        // /console.log($("#ownerlogin").valid());
        if (!$form.valid()) return false;
        $('#loader').show();
        $.ajax({
            type: "POST",
            url: ownerlogin,
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (msg) {
                $('#loader').hide();
                if ($.isEmptyObject(msg.error)) {
                    if (msg.isOtpVerified == true) {
                        window.location.href = ownerhome;
                    } else {
                        window.location.href = msg.validateOtpUrl;
                    }

                } else {

                    $(".alert-danger").find("ul").html('');
                    $(".alert-danger").css('display', 'block');
                    $.each(msg.error, function (key, value) {
                        $(".alert-danger").find("ul").append('<li>' + value + '</li>');
                    });
                }

            }
        });
    });
});