$(document).ready(function() {
    var bulkUploadDataTable;
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    bulkUploadDataTable = $('.bulkupload').DataTable({
            processing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: bulkUploadReview,
                method: 'POST',
                data:{
                    timezone : timezone
                }
            },
            columns: [
                { "data": "id", "name": "id"},
                { "data": "listings", "name": "listings"},
                { "data": "errors", "name": "errors" },
                { "data": "created_at", "name": "created_at"},
                { "data": "status",  "name": "status"},
                { "data": "results",  "name": "results"},
            ],
            language: {
                processing: "<img src='"+processingImg+"'>"
            },
            bLengthChange: false,
            pageLength: 10,
        });

        setInterval(function() {
            $('.bulkupload').DataTable().ajax.reload();
        }, 60000);

});
