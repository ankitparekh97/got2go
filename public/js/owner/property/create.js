

var newResort = '';
var isValidResort = false;
var availDatecount = 0;
var unavailDatecount = 1;
var isMatching = false;

    $(document).ready(function(){


        var type = 'property'
        $("#property_type").empty();
        $("#property_type").append('<option value="">Select One</option>');
        for(x in PropertyTypes[type]){
            $("#property_type").append($("<option></option>").val(x).text(PropertyTypes[type][x].trim() ));
        }

        var type = 'vacation_rental'
        $("#vacation_club").empty();
        $("#vacation_club").append('<option value="">Select One</option>');
        for(x in PropertyTypes[type]){
            $("#vacation_club").append($("<option></option>").val(x).text(PropertyTypes[type][x].trim() ));
        }

        $('#what-kind-of-residence').hide();
        $('#what-kind-of-cabin').hide();
        $('#what-kind-of-vacation').hide();
        $('#what-is-resort-name').hide();

        setPropertyInput();

        $('#property_type, #subType, #vacation_club, #check-in-date, #sel-in-time, #check-out-date, #sel-out-time').select2({
            minimumResultsForSearch: -1
        });

        $("#subType").on('change',function(){
            $(this).valid()
        });

        function validateAvailableDates(){
            var isValid = true;
            $("input[name='property_available_when[]']:visible").each(function(index){

                if($(this).val() == '' )
                {
                    $(this).addClass('error');
                    isValid = false;

                }else{
                    $(this).removeClass('error')
                }

            })

            $("input[name='property_available_when_to[]']:visible").each(function(index){

                if($(this).val() == '')
                {
                    $(this).addClass('error');
                    isValid = false;

                }else{
                    //$(this).removeClass('error')
                }

            })
            $('#availableError label').text('Invalid Dates.');

            $('#availableError label').hide();
            if(!isValid){
                $('#availableError label').show();
            }
            var isValidM = validateOverlapping();
            return isValid && isValidM;
        }

        function validateOverlapping(){

            var isValidM = overlayValidation();
            return  isValidM
        }

        // show extra beds
        $('.click-btn').on('click',function(){
            $(this).parent().find('.select-dropdown-inner').toggleClass('show');
        });

        $('#nextBtn').on('click',function(){
            var form = jQuery("#owner-panel-form");
            var isValid = validateAvailableDates();


            if (form.valid() === false || !isValid){
               return false;
            }
            else{
                form.submit();
                return true;
            }
        })

        $('#owner-panel-form').on('submit', function (e) {
            e.preventDefault();
            if(!$(this).valid()){
                return false;
            }

            showLoader();
            $.ajax({
                type: "POST",
                url: propertystore,
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('owner_token')}}",
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(msg) {
                    hideLoader();
                    if($.isEmptyObject(msg.error)){
                        if(msg.success == true){


                            if(!isPreview)
                            {
                                nextPrev(1);
                            } else if(isPreview && $('#nextBtn').text()=='Submit'){
                                $('#form-submited-modal').modal('show');
                            }else{
                                showTab(12);

                            }

                            window.history.pushState(null,null, msg.redirect_url);
                            Property_DB = msg.property;
                            $('#hdn_property_id').val(Property_DB['id'])




                        }
                    }else{
                        $(".alert-danger").show();
                        $(".alert-danger span").html('');

                        if(msg.validation){
                            showTab(0);
                            $('#owner-panel-form').valid();
                        }

                        $.each( msg.error, function( key, value ) {

                            $(".alert-danger span").html(value);
                        });
                    }
                }
            });
        })
        $('#how-many-guests-allowed').closest('div').find('.minus').css({'border-color': '#f80e41', 'color':'#f80e41'})

        $('.plus').on('click',function(){
            var $qty=$(this).closest('div').find('.cmn-add');
            var tempId = $qty.attr('id');
            var tempName = $qty.attr('name');

            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal < 30 &&
            (tempId == 'how-many-bedrooms-there' || tempId == 'how-many-guests-allowed'  || tempId == 'how_many_bathrooms_available' || tempId == 'how_many_beds_there')) {
                $qty.val(currentVal + 1);
                if(tempId == 'how-many-bedrooms-there'){
                    cloneBedrooms(currentVal+1);
                }
            }

            else if(!isNaN(currentVal) && currentVal < 10
             && tempName.indexOf('bedType') > -1 ){
                var dataindex = $qty.attr('data-index');
                var cnt=0;
                $('input[data-index='+dataindex+']').each(function(i){

                    cnt += parseInt($(this).val())

                });
                if(cnt <10){
                    $qty.val(currentVal + 1);
                }
            }

            if($qty.val() > 0){
                var div = $(this).closest('div').find('.minus');
                div.css({'border-color': '#f80e41', 'color':'#f80e41'})
                div.addClass('active');
            }
            var form = jQuery("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else{
                jQuery('#nextBtn').removeClass('disabled')
            }
        });

        $('.minus').on('click',function(){
            var $qty=$(this).closest('div').find('.cmn-add');
            var tempId = $qty.attr('id');
            var currentVal = parseInt($qty.val());

            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
                if(tempId == 'how-many-bedrooms-there'){
                    removeBedrooms(currentVal);
                }
            }
            if($qty.val() <= 0){
                var div = $(this);
                div.css({'border-color': '#ebedf7', 'color':'#ebedf7'});
                div.removeClass('active');
            }
            var form = jQuery("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else{
                jQuery('#nextBtn').removeClass('disabled')
            }

        });

        function setPropertyInput(){
            $('#what-kind-of-residence').show();
            $('#what-kind-of-cabin').hide();
            $('#what-kind-of-vacation').hide();
            $('#what-is-resort-name').hide();

            $('#what-kind-of-vacation').hide();

        }

        function setVacationInput(){
            $('#what-kind-of-residence').hide();
            $('#what-kind-of-cabin').hide();
            $('#what-kind-of-vacation').hide();
            $('#save-new-resort').hide()
            $('#what-is-resort-name').show();
            $('#what-kind-of-vacation').show();
        }



        $('.add-more-availibility').on('click',function(){

            cloneavaliblityDates();
            var form = jQuery("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else{
                jQuery('#nextBtn').removeClass('disabled')
            }
        });

        function cloneavaliblityDates(){

            var avaiblityDatesBlock = $(".avaiblityDatesBlock").clone(true, true).prop({ id: "avaiblityDates_"+availDatecount}).appendTo($('.avaiblityDates'));
            avaiblityDatesBlock.removeClass('avaiblityDatesBlock');
            avaiblityDatesBlock.find('.frmdatetimepicker').attr('name','property_available_when[]');
            avaiblityDatesBlock.find('.frmdatetimepicker').attr('id','frmdate'+availDatecount);
            avaiblityDatesBlock.find('.frmdatetimepicker').attr('data-id', availDatecount);
            avaiblityDatesBlock.find('.todatetimepicker').attr('data-id', availDatecount);

            avaiblityDatesBlock.find('.todatetimepicker').attr('name','property_available_when_to[]');
            avaiblityDatesBlock.find('.todatetimepicker').attr('id','todate'+availDatecount);
            avaiblityDatesBlock.show();
            avaiblityDatesBlock.find('.delete-availabiities').attr('data-id', availDatecount);
            avaiblityDatesBlock.find('.partial-availabiities').attr('data-id', availDatecount);
            avaiblityDatesBlock.find('.partial-availabiities').find('.pa-radio').attr('name', 'is_partial_booking_date'+availDatecount);
            var radio = avaiblityDatesBlock.find('.partial-availabiities').find('.pa-radio');
            $("input[name='is_partial_booking_date"+availDatecount+"'][value='0']").prop('checked',true);
            avaiblityDatesBlock.find('.min_night-availabiities').attr('data-id', availDatecount);
            avaiblityDatesBlock.find('.min_night-availabiities').find('.min_night').attr('id', 'min_night'+availDatecount);
            avaiblityDatesBlock.find('.min_night-availabiities').find('.min_night').attr('name', 'min_night[]');

            avaiblityDatesBlock.find('.min_night-availabiities').hide();
            $(radio).on('change',function(){
                if($(this).val() == 1){
                    console.log( $(this).parent('.partial-availabiities').find('.min_night-availabiities').show())
                    $(this).parent('.radio-btn').parent('.partial-availabiities').find('.min_night-availabiities').show()

                }else{
                    $(this).parent('.radio-btn').parent('.partial-availabiities').find('.min_night-availabiities').hide()
                }
            });

            initdatepicker(availDatecount)

            availDatecount = availDatecount+1;
        }


        function initdatepicker(pickerId){


            $('#frmdate'+pickerId).datetimepicker({
                format: 'L',
                minDate :moment(new Date()).format('MM/DD/YYYY'),
                icons: {
                    up: "fa fa-chevron-circle-up",
                    down: "fa fa-chevron-circle-down",
                    next: 'fa fa-chevron-circle-right',
                    previous: 'fa fa-chevron-circle-left'
                }
            })
            $('#todate'+pickerId).datetimepicker({
                format: 'L',
                minDate :moment(new Date()).format('MM/DD/YYYY'),
                icons: {
                    up: "fa fa-chevron-circle-up",
                    down: "fa fa-chevron-circle-down",
                    next: 'fa fa-chevron-circle-right',
                    previous: 'fa fa-chevron-circle-left'
                }
            });
            $('#frmdate'+pickerId).on('dp.change', function(e){
                var date = new Date(e.date.valueOf());
                var pickerId = $(this).attr('data-id');
                if(e.date.valueOf() != ''){
                    $('#todate'+pickerId).data("DateTimePicker").minDate(moment($('#frmdate'+pickerId).val()).add(1, 'days'));
                }
                else{
                    $('#todate'+pickerId).val('')
                    $('#todate'+pickerId).data("DateTimePicker").minDate( moment(new Date()).format('MM/DD/YYYY').add(1, 'days'));
                }

                validateAvailableDates();
            });
            $('#todate'+pickerId).on('dp.change', function(e){
                validateAvailableDates();
            });

        }
        initdatepicker(availDatecount)

        function deleteDate(obj){
            if(availDatecount == 1){
                $('#availableError label').text('At least one availability period is required.');
                $('#availableError label.custom-error').show()

            }
            else{
                var index = $(obj).attr('data-id');
                console.log(index)
                $('#avaiblityDates_'+index).remove()
                availDatecount += -1

                 $("input[name='property_available_when[]']").each(function(index){
                    $(this).prop({ id: "frmdate"+index});
                    $(this).attr('data-id', index);
                 });

                 $("input[name='property_available_when_to[]']").each(function(index){
                    $(this).prop({ id: "todate"+index});
                    $(this).attr('data-id', index);
                 });
                var form = jQuery("#owner-panel-form");
                if (form.valid() === false){
                    // jQuery('#nextBtn').addClass('disabled')
                }
                else{
                    jQuery('#nextBtn').removeClass('disabled')
                }
            }


        }
        $('.delete-availabiities').on('click',function(){
                deleteDate(this)
        });
        $('.add-more-unavailibility').on('click',function(){
            cloneunavaliblityDates();
        });

        function cloneunavaliblityDates(){
            unavailDatecount = unavailDatecount+1;
            var avaiblityDatesBlock = $(".unavaiblityDatesBlock").clone().prop({ id: "unavaiblityDates"}).appendTo($('.unavaiblityDates'));
            avaiblityDatesBlock.removeClass('unavaiblityDatesBlock');
            avaiblityDatesBlock.find('.frmunavaiblitydatetimepicker ').attr('id','frmunavaiblitydate'+unavailDatecount);
            avaiblityDatesBlock.find('.tounavaiblitydatetimepicker ').attr('id','tounavaiblitydate'+unavailDatecount);
            avaiblityDatesBlock.show()

            initunavaliblityDatesdatepicker(unavailDatecount)
        }

        $('.type_of_property').on('change',function(){
            newResort = '';
            $('#txt-resort').val('');
            $('#txt-resort').parents('.input-button').removeClass('green');
            $('#txt-resort').attr('readonly',false);
            $('#save-new-resort').html('Save New Resort');
            $("#subType").empty();
            if($(this).val() == 'property'){
                $('#vacation_club').val('');
                $('#vacation_club').trigger('change');
                setPropertyInput();
                $("#msg_available_property").hide();
            }
            else if($(this).val() == 'vacation_rental'){
                $('#property_type').val('')
                $('#property_type').trigger('change');
                $('#vacation_club').val('')
                $('#vacation_club').trigger('change');
                var lblTxt = 'What’s the name of the vacation club?';
                $("#lblclubtype").text(lblTxt);
                setVacationInput();
                $("#msg_available_property").show();
            }
        });

        $('#property_type, #vacation_club').on('change',function(){

            var check = $('input[name="type_of_property"]:checked').val();
            var type = this.value;
            var getName = $(this).find("option:selected").text();

            var lblTxt = 'What’s the name of the vacation club?';


            if(subPropertyType[type]){

                $("#subType").empty();
                var lblTxt = 'What kind of {cabin} is this?';
                $("#lblsubtype").text(lblTxt.replace('{cabin}',PropertyTypes[check][type]))
                $("#subType").append('<option value="">Select One</option>');
                for(x in subPropertyType[type]){
                    $("#subType").append($("<option></option>").val(x).text(subPropertyType[type][x].trim() ));
                }
                if(Property_DB['sub_property_type_id']){
                    $("#subType").val(Property_DB['sub_property_type_id'])
                }
                $('#what-kind-of-cabin').show();
            }
            else{
                $("#subType").empty();
                $('#what-kind-of-cabin').hide();
            }
            $(this).valid();
        });

        $('#check-in-date, #sel-in-time').on('change', function(){
            $('#guest_check_in').val($('#check-in-date').val()+' '+$('#sel-in-time').val())
        });

        $('#check-out-date,#sel-out-time').on('change', function(){
            $('#guest_checkout_date').val($('#check-out-date').val()+' '+$('#sel-out-time').val())
        });

        //Auto SUggestions
        function autocomplete(inp, arr) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value.trim();
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                isMatching = false;
                // for (i = 0; i < arr.length; i++) {
                for (var i in arr) {
                  /*check if the item starts with the same letters as the text field value:*/
                  if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    isMatching = true;
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                  }

                }

                if (!isMatching && val.length >= 3) {
                    isValidResort = true;
                    $('#save-new-resort').show();
                    $('#empty-resorts').show();
                    $('#txt-resort').parents('.input-button').removeClass('green');
                    $('#txt-resort').parents('.input-button').addClass('red');
                }else if(val.length == 0){
                    isValidResort = false;
                    $('#save-new-resort').show();
                    $('#empty-resorts').show();
                    $('#txt-resort').parents('.input-button').removeClass('green');
                    $('#txt-resort').parents('.input-button').addClass('red');
                }
                else{
                    isValidResort = false;
                    $('#empty-resorts').hide();
                    $('#save-new-resort').hide();
                    $('#txt-resort').parents('.input-button').removeClass('green');
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                  /*If the arrow DOWN key is pressed,
                  increase the currentFocus variable:*/
                  currentFocus++;
                  /*and and make the current item more visible:*/
                  addActive(x);
                } else if (e.keyCode == 38) { //up
                  /*If the arrow UP key is pressed,
                  decrease the currentFocus variable:*/
                  currentFocus--;
                  /*and and make the current item more visible:*/
                  addActive(x);
                } else if (e.keyCode == 13) {
                  /*If the ENTER key is pressed, prevent the form from being submitted,*/
                  e.preventDefault();
                  if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                  }
                }
            });
            function addActive(x) {
              /*a function to classify an item as "active":*/
              if (!x) return false;
              /*start by removing the "active" class on all items:*/
              removeActive(x);
              if (currentFocus >= x.length) currentFocus = 0;
              if (currentFocus < 0) currentFocus = (x.length - 1);
              /*add class "autocomplete-active":*/
              x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
              /*a function to remove the "active" class from all autocomplete items:*/
              for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
              }
            }
            function closeAllLists(elmnt) {
                isMatching = false;
              /*close all autocomplete lists in the document,
              except the one passed as an argument:*/
              var x = document.getElementsByClassName("autocomplete-items");
              for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
              }

              var form = jQuery("#owner-panel-form");
              if (form.valid() === false){
                  // jQuery('#nextBtn').addClass('disabled')
              }
              else{
                  jQuery('#nextBtn').removeClass('disabled')
              }
            }
          }
          /*execute a function when someone clicks in the document:*/
          document.addEventListener("click", function (e) {
              closeAllLists(e.target);

              if (!$(e.target).is('.select-custom-dropdown *, .select-dropdown-inner *')) {
                $('.select-dropdown-inner').removeClass('show');
              }



          });
          }
          autocomplete(document.getElementById("txt-resort"), availableTags);

        $('#save-new-resort').click(function(){
            newResort = $('#txt-resort').val();
            $('#empty-resorts').hide();

            $('#txt-resort').parents('.input-button').addClass('green');
            $('#save-new-resort').html('Resort Saved!');
            $('#txt-resort').attr('readonly','readonly');
            var form = jQuery("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else{
                jQuery('#nextBtn').removeClass('disabled')
            }
        })


        $("#owner-panel-form input[name='is_negotiable']").on('change',function(){
            var is_negotiable = $(this).val();
            if(is_negotiable == 1){
                $('.offerPrice').show();
            } else {
                $('.offerPrice').hide();
                $('#offer_price').val('');
            }
        });

        function cloneBedrooms(currentVal){
            var div = $("#bedroomTypes").clone(true,true).prop({ id: "bedroom_"+currentVal}).appendTo($('.cloneBedroom'));
            div.find('.delBedroom').attr('data-id',currentVal);
            div.find('input').each(function(){
                $(this).attr('data-index',currentVal);
            })
            div.show()
            renderBedroomLabel()
        }

        function renderBedroomLabel(){
            $('.lblBedroom').each(function(index){
                $(this).text('Bedroom '+index);

            })
            $('.bedroom-outer-container').each(function(index){
                $(this).find('input').attr('data-index',index);
            })
        }

        $('.delBedroom').on('click',function(){
            var id = $(this).attr('data-id');
            $('#bedroom_'+id).remove();
            var val = $('#how-many-bedrooms-there').val()
            $('#how-many-bedrooms-there').val(val -1);

            renderBedroomLabel();
        })

        function removeBedrooms(currentVal){
            $('.lblBedroom').each(function(index){
                if(currentVal == index)
                {
                    $(this).parents('.col-md-6').remove();
                }

            });


        }

        function setFields(){
            $('#hdn_property_id').val(Property_DB['id'])
            $('#name_your_listing').val(Property_DB['title'])
            $('#describe_listing').val(Property_DB['description'])
            $('#about_space').val(Property_DB['about_the_space'])
            $('#last_tab_id').val(Property_DB['last_tab_id'])

            $('input[name="listing"]').each(function () {
                if(this.value == Property_DB['list_as_company'])
                this.checked = true;

            });

            $('input[name="listing"]').each(function () {
                if(this.value == Property_DB['list_as_company'])
                this.checked = true;
            });
            $('#where_is_listing').val(Property_DB['location_detail'])
            $('#where_is_listing_clone').val(Property_DB['location_detail'])
            $('#city').val(Property_DB['city'])
            $('#state').val(Property_DB['state'])
            $('#country').val(Property_DB['country'])
            $('#zipcode').val(Property_DB['zipcode'])

            $('input[name="type_of_property"]').each(function () {
                if(this.value == Property_DB['type_of_property'])
                    this.checked = true;
            });
            setPropertyInput();
            if(Property_DB['type_of_property'] == 'property'){
                $('#property_type').val(Property_DB['property_type_id']);
                $('#property_type').trigger('change');

            }else if(Property_DB['type_of_property'] == 'vacation_rental'){
                $('#vacation_club').val(Property_DB['property_type_id']);
                $('#vacation_club').trigger('change');
                setVacationInput();
            }

            if(Property_DB['sub_property_type_id']){
                $('#what-kind-of-cabin').show();
                $('#subType').val(Property_DB['sub_property_type_id']);
            }

            $('#txt-resort').val(availableTags[Property_DB['resort_id']]);
            $('#txt-resort').attr('readonly',false);
            $('#save-new-resort').hide();

            $('input[name="guest_have"]').each(function () {
                if (this.value == Property_DB['guest_will_have']) {
                  this.checked = true;
                }

            });

            $('input[name="is_guest_only"]').each(function () {
                if (this.value == Property_DB['is_for_guest']) {
                  this.checked = true;
                }
            });

            if(Property_DB['no_of_guest'])
            $('#how-many-guests-allowed').val( Property_DB['no_of_guest']);

            for(var i = 0 ; i < Property_DB['total_bedroom']; i++){
                $('#how-many-bedrooms-there').closest('div').find('.plus').trigger('click');
            }

            for(var i = 0 ; i < Property_DB['extra_beds']; i++){
                $('#how_many_beds_there').closest('div').find('.plus').trigger('click')
            }

            for(var i = 1 ; i <= Property_DB['total_beds']; i++){

                for(var x in Property_DB["propertybedrooms"]){
                    if(Property_DB["propertybedrooms"][x].bedroom_no == i){
                        var bedType = bedTypes[Property_DB["propertybedrooms"][x].bed_type];

                        for(var j = 0; j<Property_DB["propertybedrooms"][x].bed;j++)
                        {
                            $('#bedroom_'+i).find('.'+bedType).closest('div').find('.plus').trigger('click');
                        }

                    }
                }

            }

            for(var i = 0 ; i < Property_DB['total_bathroom']; i++){
                $('#how_many_bathrooms_available').val(Property_DB['total_bathroom'])
            }
            for (var x in Property_DB['propertyamenities']) {

                $('input:checkbox[name="amenity_id[]"][value="' + Property_DB['propertyamenities'][x].amenity_id + '"]')
                    .attr('checked', 'checked');
            }

            $('#cleaning_fee').val(Property_DB['cleaning_fee']);
            $('#price').val(parseInt(Property_DB['price']));
            $('#offer_price').val(parseInt(Property_DB['offer_price']));
            $('input[name="is_negotiable"]').each(function() {
                if (this.value == Property_DB['is_price_negotiable']) {
                    this.checked = true;
                }

                if (Property_DB['is_price_negotiable'] == '1') {
                    $('#offerPricetxt').empty().text($('#offer_price').val());
                    $('#offerprice').show();
                } else {
                    $('#offerprice').hide();
                    $('.offerPrice').hide();
                }
            });

            $('input[name="is_instant_booking"]').each(function () {
                if (this.value == Property_DB['is_instant_booking_allowed']) {
                    this.checked = true;
                }

            });

            $('input[name="is_partial_booking"]').each(function () {
                if (this.value == Property_DB['is_partial_payment_allowed']) {
                    this.checked = true;
                }
            });

            $('input[name="cancellation_policy"]').each(function () {
                if (this.value == Property_DB['cancellation_type']) {
                    this.checked = true;
                }
            });

            if(Property_DB['check_in']){
                var check_in   = Property_DB['check_in'].split(" ");
                var check_out = Property_DB['check_out'].split(" ");

                $('#check-in-date').val(check_in[0]).trigger('change');
                $('#sel-in-time').val(check_in[1]).trigger('change');

                $('#check-out-date').val(check_out[0]).trigger('change');
                $('#sel-out-time').val(check_out[1]).trigger('change');
            }

            for(var i in Property_DB["propertyavailiblity"]){

                var fromDate = moment(Property_DB["propertyavailiblity"][i].available_from).format('YYYY-MM-DD');
                var toDate = moment(Property_DB["propertyavailiblity"][i].available_to).format('YYYY-MM-DD');

                var currentDate = moment().format('YYYY-MM-DD');
                fromDate = moment(fromDate).format('YYYY-MM-DD');
                toDate = moment(toDate).format('YYYY-MM-DD');;

                cloneavaliblityDates();
                if(Property_DB["propertyavailiblity"][i]){
                    if(fromDate < currentDate){
                        $('#frmdate'+i).val(moment(Property_DB["propertyavailiblity"][i].available_from).format('MM/DD/YYYY'));
                        $('#frmdate'+i).attr('readonly','readonly');
                        $('#frmdate'+i).attr('disabled',true);
                    } else {
                        $('#frmdate'+i).val(moment(Property_DB["propertyavailiblity"][i].available_from).format('MM/DD/YYYY')).change();
                    }

                    if(toDate < currentDate){
                        $('#todate'+i).val(moment(Property_DB["propertyavailiblity"][i].available_to).format('MM/DD/YYYY'));
                        $('#todate'+i).attr('readonly','readonly');
                        $('#todate'+i).attr('disabled',true);
                    } else {
                        $('#todate'+i).val(moment(Property_DB["propertyavailiblity"][i].available_to).format('MM/DD/YYYY')).change();
                    }

                    if(Property_DB["propertyavailiblity"][i]['minimum_days'] != 0)
                    {
                        $('#min_night'+i).val(Property_DB["propertyavailiblity"][i]['minimum_days']);

                    }else{

                        $('#min_night'+i).val(Property_DB["propertyavailiblity"][i]['minimum_days']);
                    }

                    if(Property_DB["propertyavailiblity"][i]['is_partial_booking_allowed'] != 0)
                    {
                        $("input[name='is_partial_booking_date"+i+"'][value='1']").prop('checked',true);
                        $('#min_night'+i).closest('.min_night-availabiities').show()
                    }else{
                        $("input[name='is_partial_booking_date"+i+"'][value='0']").prop('checked',true);
                        $('#min_night'+i).closest('.min_night-availabiities').hide()
                    }
                }
            }



            if(!Property_DB["propertyavailiblity"] || Property_DB.length == 0 || Property_DB['propertyavailiblity'].length == 0){
                cloneavaliblityDates();
            }
        }
        setFields();

        var tab = 0;
        if(Property_DB['status']=='draft'){
            tab = Property_DB['last_tab_id'];
        } else if(Property_DB['status']){
            tab = 12;
        }
        showTab(tab)
    })


