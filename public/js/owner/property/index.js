$(document).ready(function () {


    pendinglistings = $('.pendinglistings').DataTable();
    $('#pendingSearch').keyup(function(){
        pendinglistings.search($(this).val()).draw() ;
    })

    currentSearch = $('.currentlistings').DataTable();
    $('#currentSearch').keyup(function(){
        currentSearch.search($(this).val()).draw() ;
    })

    $("ul#listingtab").on('click', 'li a', function (e) {
        window.location.hash = $(this).attr('href');

        var tabId = $(this).attr('id');
        if(tabId == 'current-tab'){
            $('#currentSearch').show();
            $('#pendingSearch').hide();
        } else if(tabId == 'pending-tab'){
            $('#currentSearch').hide();
            $('#pendingSearch').show();
        } else {
            $('#pendingSearch').hide();
            $('#currentSearch').hide();
        }
    })
});

function destroy(id) {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: propertydestroy,
        type: 'DELETE',
        data: {
            _token: CSRF_TOKEN,
            id: id
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.success)
                $('#delete-listing').modal('hide');
                $('table tr[data-row='+ id +']').remove();
                $("html, body").animate({ scrollTop: 200 }, "slow");

                let type = (data.property.type_of_property == 'property') ? 'Private Residence' : 'Vacation Rental';
                Swal.fire({
                    icon: "success",
                    title: type+" deleted successfully!",
                    showConfirmButton: false,
                    width: 500,
                    timer: 3000
                });
        }
    });
}

function publish(id) {

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    showLoader();
    $.ajax({
        url: propertypublish,
        type: 'POST',
        data: {
            id: id,
            _token: CSRF_TOKEN
        },
        dataType: 'JSON',
        headers: {
            "Authorization": Authorization,
        },
        success: function (data) {
            hideLoader();
            if (data.success) {
                if (data.property.publish == 1) {
                    $('#publish_'+id+' span').empty().text(data.updated_dt);
                    $('table tr[data-row='+ id +']').find('#publish-listing').text('Unpublish');
                }
                else {
                    $('table tr[data-row='+ id +']').find('#publish-listing').text('Publish');
                }
                $('table tr[data-row='+ id +']').find('#publish-listing').attr('data-publish', data.property.publish);
                $('table tr[data-row='+ id +']').find('#publish-listing').attr('onclick', 'showPublishModal('+id+','+data.property.publish+')');
                $("#publish").modal('hide');
            }
        }
    });
}


function propertyListings() {
    var dataTable;
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    dataTable = $('.pendinglistings').DataTable({
        processing: true,
        serverSide: true,
        deferRender: true,
        order: [],
        ajax: {
            url: pendingListings,
            method: 'POST',
            data: {
                timezone: timezone
            }
        },
        columns: [
            { "data": "cover_photo", "name": "cover_photo" },
            { "data": "title", "name": "title" },
            { "data": "status", "name": "status" },
            { "data": "actions", "name": "actions" }
        ],
        language: {
            processing: "<img src='" + processingImg + "'>",
            search: '',
            searchPlaceholder: "Search Listings",
            sEmptyTable: "No Listings found"
        },
        fnCreatedRow: function( nRow, aData, iDataIndex ) {
            $(nRow).attr('data-row', aData.id);
        },
        dom:"lrtip",
        bLengthChange: false,
        pageLength: 10,
    });

}

function currentlistings() {
    var currentlistingsDataTable;
    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    currentlistingsDataTable = $('.currentlistings').DataTable({
        processing: true,
        serverSide: true,
        deferRender: true,
        order: [],
        ajax: {
            url: currentListings,
            method: 'POST',
            data: {
                timezone: timezone
            }
        },
        columns: [
            { "data": "cover_photo", "name": "cover_photo" },
            { "data": "title", "name": "title" },
            { "data": "status", "name": "status" },
            { "data": "actions", "name": "actions" }
        ],
        language: {
            processing: "<img src='" + processingImg + "'>",
            search: '',
            searchPlaceholder: "Search Listings",
            sEmptyTable: "No Listings found"
        },
        fnCreatedRow: function( nRow, aData, iDataIndex ) {
            $(nRow).attr('data-row', aData.id);
        },
        dom:"lrtip",
        bLengthChange: false,
        pageLength: 10,
    });

}
