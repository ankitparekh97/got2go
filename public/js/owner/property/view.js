function destroy(property_id) {

    if (confirm('Are you sure you want to delete Property?')) {

        propertydestroy = propertydestroy.replace(':id', property_id);

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: propertydestroy,
            type: 'DELETE',
            data: { _token: CSRF_TOKEN },
            dataType: 'JSON',
            headers: {
                "Authorization": Authorization,
            },
            success: function (data) {
                if (data.success) {
                    console.log(data.redirect_url);
                    window.location = data.redirect_url;
                }
            }
        });
    }
}