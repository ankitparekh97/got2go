// If JavaScript is enabled, hide fallback select field
$('.no-js').removeClass('no-js').addClass('js');

// When the user focuses on the credit card input field, hide the status
$('.card input').bind('focus', function () {
    $('.card .status').hide();
});

// When the user tabs or clicks away from the credit card input field, show the status
$('.card input').bind('blur', function () {
    $('.card .status').show();
});

// Run jQuery.cardcheck on the input
$('.card input').cardcheck({
    callback: function (result) {

        var status = (result.validLen && result.validLuhn) ? 'valid' : 'invalid',
            message = '',
            types = '';

        // Get the names of all accepted card types to use in the status message.
        for (i in result.opts.types) {
            types += result.opts.types[i].name + ", ";
        }
        types = types.substring(0, types.length - 2);

        // Set status message
        if (result.len < 1) {
            message = 'Please provide a credit card number.';
        } else if (!result.cardClass) {
            message = 'We accept the following types of cards: ' + types + '.';
        } else if (!result.validLen) {
            message = result.cardName;
        } else if (!result.validLuhn) {
            message = result.cardName;
        } else {
            message = result.cardName;
        }
        $('#card_type').val(result.cardName);
        // Show credit card icon
        $('.card .card_icon').removeClass().addClass('card_icon ' + result.cardClass);

        // Show status message
        $('.card .status').removeClass('invalid valid').addClass(status).children('.status_message').text(message);
    }
});

$(document).ready(function () {



    var dataTable;
    propertyBookings();
    $('#confirmed-tab').on('click', function () {
        dataTable.destroy();
        paymentMethodBank();
        paymentCredit();
    });
    $('#history-tab').on('click', function () {
        dataTable1.destroy();
        dataTable2.destroy();
        propertyBookings();
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    function propertyBookings() {
        dataTable = $('#historylist').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            order: [],
            headers: {
                "Authorization": "Bearer {{session()->get('owner_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            ajax: {
                url: historyListing,
                method: 'POST',
            },
            columns: [
                {
                    'className': 'detail-control',
                    'orderable': false,
                    'data': null,
                    'defaultContent': ''
                },
                { "data": "id", "name": "Reservation#" },
                { "data": "paymentdate", "name": "Payment Date" },
                { "data": "status", "name": "status" },
                { "data": "amount", "name": "amount" },
                { "data": "name", "name": "name" },
                { "data": "property", "name": "property" },
                { "data": "actions", "name": "actions" },
            ],
            language: {
                processing: "<img src='" + processingImg + "'>"
            },
            bLengthChange: false,
            pageLength: 10,
        });

    }

    function paymentMethodBank() {
        dataTable1 = $('#paymentMethodsBank').DataTable({
            processing: true,
            serverSide: false,
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            destroy: true,
            "language": {
                "emptyTable": "No data available"
            },
            order: [],
            ajax: {
                url: paymentMethods,
                method: 'POST'
            },
            columns: [
                { "data": "institution_name", "name": "institution_name" },
                { "data": "institutionType", "name": "institutionType" },
                { "data": "account_number", "name": "account_number" },
                { "data": "routing_number", "name": "routing_number" },
                { "data": "actions", "name": "actions" },
            ],
            bLengthChange: false,
            pageLength: 10,
        });

    }

    function paymentCredit() {
        dataTable2 = $('#paymentMethodsCredit').DataTable({
            processing: true,
            serverSide: false,
            destroy: true,
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            "language": {
                "emptyTable": "No data available"
            },
            order: [],
            ajax: {
                url: paymentMethodCredit,
                method: 'POST'
            },
            columns: [
                { "data": "cardname", "name": "cardname" },
                { "data": "card_type", "name": "card_type" },
                { "data": "last_four", "name": "last_four" },
                { "data": "expiration_date", "name": "expiration_date" },
                { "data": "actions", "name": "actions" },
            ],
            bLengthChange: false,
            pageLength: 10,
        });

    }




    $("#searchbox").keyup(function () {
        $('#propertyBooking').dataTable().fnFilter(this.value);
    });



    // Add event listener for opening and closing details
    $('.propertyBooking tbody').on('click', 'tr td.detail-control', function () {
        var tr = $(this).closest('tr');
        var row = dataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    // Add event listener for opening and closing details for confirmed
    $('.confirmedPropertyBooking tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = confirmedPropertyBookingdataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(confirmedFormat(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {
        // `d` is the original data object for the row
        let phone = (d.phone == null) ? '' : d.phone;
        let email = (d.email == null) ? '' : d.email;


        return '<div class="table-responsive inner"><table class="inner" id="extraInfo" >' +
            '<tr>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td>' + d.duration + '<br>' + d.price + '</td>' +
            '<td>' + d.renter + '</td>' +
            '<td>' + d.location_detail + '</td>' +
            '<td></td>' +
            '</tr>' +
            '</table>' +
            '</div>';
    }

    function confirmedFormat(d) {
        // `d` is the original data object for the row
        let phone = (d.phone == null) ? '' : d.phone;
        let email = (d.email == null) ? '' : d.email;


        return '<div class="table-responsive inner"><table class="inner" id="extraInfo" cellpadding="0" cellspacing="0" border="0" >' +
            '<tr>' +
            '<td></td>' +
            '<td>' + d.adults + '</td>' +
            '<td>' + d.location_detail + '</td>' +
            '<td><span>Check-In: ' + d.check_in + ' ' + d.check_in_date + ' <br> Check-out:' + d.check_out + ' ' + d.check_out_date + '</span></td>' +
            '<td>' + d.price + '</td>' +
            '<td>' + d.time + '</td>' +
            '<td></td>' +
            '</tr>' +
            '</table>' +
            '</div>';
    }



    $('.propertyBooking, .confirmedPropertyBooking').on('click', '.cancelBooking', function (e) {
        e.preventDefault();

        let content = '';
        let bookingid = $(this).data("id");
        let bookingAmount = $(this).attr('data-amount');
        $('#reservationId').empty().text(bookingid);
        $('#refundamount').empty().text(bookingAmount);

        $('#cancelBooking').modal('show');
    });

    $('#confirmBox').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input[type=text], input[type=number], input[type=email], input[type=password]")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();

        var $alertas = $('#confirmation');
        $alertas.validate().resetForm();
        $alertas.find('.error').removeClass('error');
    });


    $('#addCardModal').click(function(){
        let event = $(this).data('event');
        if(event!=null && event=='add'){

            $('#last_four').removeAttr('readonly');
            $('#cvv').removeAttr('readonly');

            $('#addCard').modal('show');
            $('#addCard .event').empty().text('Add Card');
            $('#creditCardSave #id').val('');
        }
    });

    $('#addBankHistoryModal').click(function(){
        let event = $(this).data('event');

        if(event!=null && event=='add'){
            $('#routingNumber').removeAttr('readonly');
            $('#accountNumber').removeAttr('readonly');
            $('#addBankAccount').modal('show');
            $('#addBankAccount .bankEvent').empty().text('Add Bank Account');
            $('#bankDetailsSave #id').val('');
        }
    });

    // alphabet validator
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Letters and spaces only please");

    $('#approve, #confirmBox').on('click', '.approveBooking', function (e) {
        e.preventDefault();
        console.log(1);
        let $form = $('#confirmation');
        if (!$form.valid()) return false;

        showLoader();

        $.ajax({
            type: "POST",
            url: approve,
            dataType: "JSON",
            async: true,
            data: {
                id: $(this).attr('data-id'),
                action: $(this).attr('data-action'),
                confirmation_id: $('#confirmation_id').val(),
            },
            // processData: false,
            // contentType: false,
            success: function (msg) {
                hideLoader();
                $.unblockUI()

                if ($.isEmptyObject(msg.error)) {
                    $('#approve').modal('hide');
                    $('#confirmBox').modal('hide');

                    $('.propertyBooking').DataTable().ajax.reload();
                    $('.confirmedPropertyBooking').DataTable().ajax.reload();

                    if (msg.owner_status != 'approved') {
                        $(".confirmedBookings").empty().append('Ok');
                    }

                    let checkInDate = new Date(msg.booking.check_in_date);
                    /* let check_in_dt = checkInDate.toLocaleString('default', { month: 'short' }) + ' ' + checkInDate.getDate() + ', ' + checkInDate.getFullYear(); */
                    let check_in_dt = moment(msg.booking.check_in_date).format('MMM DD, YYYY');

                    let checkOutDate = new Date(msg.booking.check_out_date);
                    /* let check_out_dt = checkOutDate.toLocaleString('default', { month: 'short' }) + ' ' + checkOutDate.getDate() + ', ' + checkOutDate.getFullYear(); */
                    let check_out_dt = moment(msg.booking.check_out_date).format('MMM DD, YYYY');

                    let pendingTxt = (msg.owner_status == 'pending') ? 'Your earnings will be deposited into your default bank account in 5-7 business days.' : '';
                    let content = 'You have ' + msg.owner_status + ' ' + msg.booking.rentaluser.first_name + ' ' + msg.booking.rentaluser.last_name + '\'s request for ' + check_in_dt + ' - ' + check_out_dt + '. ' + pendingTxt;

                    $('#historylist').DataTable().ajax.reload();

                    $('#congrats').find('.content').empty().html(content);
                    $('#congrats').modal('show');
                }

            }
        });
    });

    $('.saveCardinfo').on('click', function () {
        var form = jQuery("#creditCardSave");
        form.validate({
            ignore: ':hidden, [readonly=readonly]',
            rules: {
                name_on_card: {
                    required: true,
                    lettersonly: true,
                    minlength: 3
                },
                last_four: {
                    required: true,
                    creditcard: true,
                    maxlength: 19,
                },
                expiration_date: {
                    required: true,
                    CcExpDate: true,
                },
                cvv: {
                    required: true,
                    digits: true,
                    minlength: 3,
                    maxlength: 4,
                    CVV: true
                },
                address: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                billingPhone: {
                    required: true,
                },
            },
            messages: {
                name_on_card: {
                    required: "Please enter name.",
                    lettersonly: jQuery.format("Name must consist of alphabetical characters only"),
                    minlength: "Name should be atleast 3 characters",
                },
                last_four: {
                    required: "Please enter in a valid credit card number",
                    creditcard: "Please enter in a valid credit card number",
                    maxlength: "credit card number cannot be more than 16 digits"
                },
                expiration_date: {
                    required: "Please enter in a valid expiration date",
                    CcExpDate: "Please enter in a valid expiration date"
                },
                cvv: {
                    required: "Please enter in a valid security code",
                    minlength: "CVV cannot be less than 3 digits",
                    maxlength: "CVV cannot be more than 4 digits"
                },
                address: {
                    required: "Please enter in a address"
                },
                city: {
                    required: "Please enter in a city"
                },
                state: {
                    required: "Please select state"
                },
                billingPhone: {
                    required: "Please enter in a phone number",
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    });


    // Credit Card Expiry Date validator
    $.validator.addMethod('CCExp', function (value, element, params) {
        var minMonth = new Date().getMonth() + 1;
        var minYear = new Date().getFullYear();
        var month = parseInt($(params.month).val(), 10);
        var year = parseInt($(params.year).val(), 10);

        return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
    }, 'Credit Card Expiration date is invalid.');


    $.validator.addMethod(
        "CcExpDate",
        function (value, element) {
            var today = new Date();
            var thisYear = today.getFullYear();

            const expDate = value.split('/');
            var expMonth = expDate[0].trim();
            if (expMonth.length == 1) return false;

            if (!expDate[1]) return false;

            var expYear = parseInt(expDate[1].trim());

            return (expMonth >= 1 && expMonth <= 12
                && (expYear >= thisYear && expYear < thisYear + 20)
                && (expYear == thisYear ? expMonth >= (today.getMonth() + 1) : true))
        },
        "Must be a valid Expiry Date"
    );

    // CVV validator
    $.validator.addMethod("CVV", function (value, element, min) {
        var cvv = value.length;
        var card = $('.status_message').text();

        if (card == "American Express" && cvv < 4) {
            return !cvv;
        } else if (card != "American Express" && cvv == 4) {
            return !cvv;
        } else {
            return true;
        }

    }, "Please enter in a valid security code");

    // Credit Card Expiry Date validator
    $.validator.addMethod('CCExp', function (value, element, params) {
        var minMonth = new Date().getMonth() + 1;
        var minYear = new Date().getFullYear();
        var month = parseInt($(params.month).val(), 10);
        var year = parseInt($(params.year).val(), 10);

        return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
    }, 'Credit Card Expiration date is invalid.');


    $.validator.addMethod(
        "CcExpDate",
        function (value, element) {
            var today = new Date();
            var thisYear = today.getFullYear();

            const expDate = value.split('/');
            var expMonth = expDate[0].trim();
            if (expMonth.length == 1) return false;

            if (!expDate[1]) return false;

            var expYear = parseInt(expDate[1].trim());

            return (expMonth >= 1 && expMonth <= 12
                && (expYear >= thisYear && expYear < thisYear + 20)
                && (expYear == thisYear ? expMonth >= (today.getMonth() + 1) : true))
        },
        "Must be a valid Expiry Date"
    );

    // CVV validator
    $.validator.addMethod("CVV", function (value, element, min) {
        var cvv = value.length;
        var card = $('.status_message').text();
        if (card == "American Express" && cvv < 4) {
            return !cvv;
        } else if (card != "American Express" && cvv == 4) {
            return !cvv;
        } else {
            return true;
        }

    }, "Please enter in a valid security code");

    $('#last_four').on('keypress change', function () {
        $(this).val(function (index, value) {
            return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        });
    });

    $('#creditCardSave').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        if (!$form.valid())
            return false;
        showLoader();
        $.ajax({
            type: "POST",
            url: saveCard,
            dataType: "JSON",
            data: new FormData(document.getElementById("creditCardSave")),
            processData: false,
            contentType: false,
            success: function (msg) {
                if (msg.success == true) {
                    $("#addCard").modal('hide');
                    document.getElementById("creditCardSave").reset();
                    $("#creditCardSave")[0].reset();
                    $("html, body").animate({ scrollTop: 200 }, "slow");
                    $(".modal-backdrop.fade.show").css("display", "none");
                    $('.card_icon').removeClass('');
                    Swal.fire({
                        icon: "success",
                        title: msg.message,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    if (card_type == 1) {
                        $("#paymentMethodsBank").DataTable().ajax.reload(null, false);
                    } else {
                        $("#paymentMethodsCredit").DataTable().ajax.reload(null, false);
                    }
                    hideLoader();
                } else {
                    hideLoader();
                    Swal.fire({
                        icon: "error",
                        title: msg.message,
                        showConfirmButton: false,
                        timer: 5000
                    });
                }
            }
        });

    });

    $('.cancelEditCardDetails').on('click', function () {
        $("#editCreditCardSave")[0].reset();
        $('div').removeClass('card');
        $('input').removeClass('error');
        $('select').removeClass('error');
        $('.error').text('');
        $('.alert-light-danger').hide();
        $(".error").html('');
        document.getElementById("editCreditCardSave").reset();
        $("#editCreditCardSave")[0].reset();
        $("html, body").animate({ scrollTop: 200 }, "slow");
        $(".modal-backdrop.fade.show").css("display", "none");
        $('#editCard').modal('hide');
    });

    $('.cancelEditBankDetails').on('click', function () {
        $("#bankDetailsEdit")[0].reset();
        $('div').removeClass('card');
        $('input').removeClass('error');
        $('select').removeClass('error');
        $('.error').text('');
        $('.alert-light-danger').hide();
        $(".error").html('');
        document.getElementById("bankDetailsEdit").reset();
        $("#bankDetailsEdit")[0].reset();
        $("html, body").animate({ scrollTop: 200 }, "slow");
        $(".modal-backdrop.fade.show").css("display", "none");
        $('#EditBankAccount').modal('hide');
    });

    $('.cancelBankInfo').on('click', function () {
        $("#bankDetailsSave")[0].reset();
        $('div').removeClass('card');
        $('input').removeClass('error');
        $('select').removeClass('error');
        $('.error').text('');
        $('.alert-light-danger').hide();
        $(".error").html('');
        document.getElementById("bankDetailsSave").reset();
        $("#bankDetailsSave")[0].reset();
        $("html, body").animate({ scrollTop: 200 }, "slow");
        $(".modal-backdrop.fade.show").css("display", "none");
        $('#addBankAccount').modal('hide');
    });

    $('.cancelCardInfo').on('click', function () {
        $("#creditCardSave")[0].reset();
        $('div').removeClass('card');
        $('input').removeClass('error');
        $('select').removeClass('error');
        $('.error').text('');
        $('.alert-light-danger').hide();
        $(".error").html('');
        document.getElementById("creditCardSave").reset();
        $("#creditCardSave")[0].reset();
        $("html, body").animate({ scrollTop: 200 }, "slow");
        $(".modal-backdrop.fade.show").css("display", "none");
        $('#addCard').modal('hide');
    });

    $('.savebank').on('click', function () {
        var form = jQuery("#bankDetailsSave");
        form.validate({
            ignore: ':hidden, [readonly=readonly]',
            rules: {
                accountHolderName: {
                    required: true,
                    lettersonly: true,
                    minlength: 3
                },
                routingNumber: {
                    required: true,
                    routing_Number: true
                },
                accountNumber: {
                    required: true,
                    minlength: 8,
                    maxlength: 16,
                    digits: true,
                    accountNumber: true
                },
                accountHolderType: {
                    required: true
                }
            },
            messages: {
                accountHolderName: {
                    required: "Please enter account holder name",
                    lettersonly: jQuery.format("Account holder name must consist of alphabetical characters only"),
                    minlength: "Account holder name should be atleast 3 characters",
                },
                routingNumber: {
                    required: "Please enter in a valid routing number"
                },
                accountNumber: {
                    required: "Please enter in a valid account number",
                    accountNumber: jQuery.format("Please enter valid account number not more than 16 digit"),
                    maxlength: "Please enter in a valid account number"
                },
                accountHolderType: {
                    required: "Please enter account type"
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        });
    });

    $.validator.addMethod('routing_Number', function (value, element) {
        var routing_number = value.length;
        if (routing_number !== 9) {
            return false;
        }

        var checksumTotal = (7 * (parseInt(value.charAt(0), 10) + parseInt(value.charAt(3), 10) + parseInt(value.charAt(6), 10))) +
            (3 * (parseInt(value.charAt(1), 10) + parseInt(value.charAt(4), 10) + parseInt(value.charAt(7), 10))) +
            (9 * (parseInt(value.charAt(2), 10) + parseInt(value.charAt(5), 10) + parseInt(value.charAt(8), 10)));

        var checksumMod = checksumTotal % 10;
        if (checksumMod !== 0) {
            return false;
        } else {
            return true;
        }

    }, 'Please enter a valid routing number.');
    $.validator.addMethod('accountNumber', function (value) {
        return !(/^0{9}$/.test(value));
    });

    $.validator.addMethod("accountHolderName", function (value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");
    $('#bankDetailsSave').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        if (!$form.valid())
            return false;
        showLoader();
        $.ajax({
            type: "POST",
            url: saveBankDetails,
            dataType: "JSON",
            data: new FormData(document.getElementById("bankDetailsSave")),
            processData: false,
            contentType: false,
            success: function (msg) {
                if (msg.success == true) {
                    $("#addBankAccount").modal('hide');
                    document.getElementById("bankDetailsSave").reset();
                    $("#bankDetailsSave")[0].reset();
                    $("html, body").animate({ scrollTop: 200 }, "slow");
                    $(".modal-backdrop.fade.show").css("display", "none");
                    Swal.fire({
                        icon: "success",
                        title: msg.message,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    $("#paymentMethodsBank").DataTable().ajax.reload(null, false);
                    hideLoader();
                } else {
                    hideLoader();
                    Swal.fire({
                        icon: "error",
                        title: msg.message,
                        showConfirmButton: false,
                        timer: 5000
                    });
                }

            },
            error: function (xhr, status, error) {

            }
        });

    });


    $('#deleteCredit').on('click', function () {
        var id = $('#card_id').val();
        var card_type = $('#card_type').val();
        showLoader();
        $.ajax({
            type: "POST",
            url: deletePayment,
            dataType: "JSON",
            data: { _token: CSRF_TOKEN, 'id': id, 'card_type': card_type },
            success: function (data) {
                if (data.success == true) {
                    $("#delete-credit").modal('hide');
                    $("html, body").animate({ scrollTop: 200 }, "slow");
                    $(".modal-backdrop.fade.show").css("display", "none");
                    Swal.fire({
                        icon: "success",
                        title: data.message,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    if (card_type == 1) {
                        $("#paymentMethodsBank").DataTable().ajax.reload(null, false);
                    } else {
                        $("#paymentMethodsCredit").DataTable().ajax.reload(null, false);
                    }
                    hideLoader();
                } else {
                    Swal.fire({
                        icon: "error",
                        title: data.error,
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            },
            error: function (xhr, status, error) {
                $('#showerrormsg').text(data.error).show().delay(1000).fadeOut();
            }
        });
    });

});
function deleteCard(id, cardtype) {
    $('#card_id').val(id);
    $('#card_type').val(cardtype);
}

function editBankDetails(id) {
    $.ajax({
        type: "POST",
        url: editBankPayment,
        dataType: "JSON",
        data: { _token: CSRF_TOKEN, 'id': id },
        success: function (data) {
            if (data.success == true) {

                $('#id').val(data.bankDetails[0].id);
                $('#accountHolderName').val(data.bankDetails[0].first_name);
                $('#accountNumber').val('• • • •   • • • •   • • • •  ' + data.bankDetails[0].account_number.slice(data.bankDetails[0].account_number.length - 4));
                $('#routingNumber').val('• • • • •  ' + data.bankDetails[0].routing_number.slice(data.bankDetails[0].routing_number.length - 4));
                if (data.bankDetails[0].default == 0) {
                    $('input[name=default_option]').attr('checked', false);
                }
                if (data.bankDetails[0].default == 1) {
                    $('input[name=default_option]').attr('checked', true);
                }

            }
        },
        error: function (xhr, status, error) {
            $('#showerrormsg').text(data.error).show().delay(1000).fadeOut();
        }
    });

    $('#routingNumber').attr('readonly','readonly');
    $('#accountNumber').attr('readonly','readonly');

    $('#addBankAccount .bankEvent').empty().text('Edit Bank Account');
    $('#addBankAccount').modal('show');
}

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

function editCard(id) {
    $('#card_id').val(id);

    $.ajax({
        type: "POST",
        url: editPayment,
        dataType: "JSON",
        data: { _token: CSRF_TOKEN, 'id': id },
        success: function (data) {
            if (data.success == true) {
                $('#creditCardSave #id').val(data.paymentCards.id);
                $('#name_on_card').val(data.paymentCards.name_on_card);
                $('#last_four').val('• • • •   • • • •   • • • •  ' + data.paymentCards.last_four);
                $('#expiration_date').val(data.paymentCards.expiration_date);
                $('#cvv').val(data.paymentCards.cvv);
                if (data.paymentCards.default == 0) {
                    $('input[name=is_default_card]').attr('checked', false);
                }
                if (data.paymentCards.default == 1) {
                    $('input[name=is_default_card]').attr('checked', true);
                }
            }
        },
        error: function (xhr, status, error) {
            $('#showerrormsg').text(data.error).show().delay(1000).fadeOut();
        }
    });

    $('#addCard .event').empty().text('Edit Card');
    $('#addCard #last_four').removeAttr('maxlength');
    $('#addCard #last_four').attr('readonly','readonly');
    $('#addCard #cvv').attr('readonly','readonly');
    $('#addCard').modal('show');

}

$('.editBank_details').on('click', function () {
    var form = jQuery("#bankDetailsEdit");
    form.validate({
        rules: {
            account_holder_name: {
                required: true,
                lettersonly: true,
                minlength: 3
            },
        },
        messages: {
            account_holder_name: {
                required: "Please enter account holder name.",
                lettersonly: jQuery.format("Account holder name must consist of alphabetical characters only"),
                minlength: "Account holder name should be atleast 3 characters",
            },

        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        }
    });
});

$('#bankDetailsEdit').on('submit', function (e) {
    e.preventDefault();
    var $form = $(this);
    if (!$form.valid())
        return false;
    showLoader();
    $.ajax({
        type: "POST",
        url: saveBankDetails,
        dataType: "JSON",
        data: new FormData(document.getElementById("bankDetailsEdit")),
        processData: false,
        contentType: false,
        success: function (msg) {
            $("#EditBankAccount").modal('hide');
            document.getElementById("bankDetailsEdit").reset();
            $("#bankDetailsEdit")[0].reset();
            $("html, body").animate({ scrollTop: 200 }, "slow");
            $(".modal-backdrop.fade.show").css("display", "none");
            Swal.fire({
                icon: "success",
                title: msg.message,
                showConfirmButton: false,
                timer: 3000
            });
            $("#paymentMethodsBank").DataTable().ajax.reload(null, false);
            hideLoader();
        }
    });

});

$('.edit_card_details').on('click', function () {
    var form = jQuery("#editCreditCardSave");
    form.validate({
        rules: {
            name_on_card: {
                required: true,
                lettersonly: true,
                minlength: 3
            },
            expiration_date_edit: {
                required: true,
                CcExpDate: true,
            },
        },
        messages: {
            name_on_card: {
                required: "Please enter name.",
                lettersonly: jQuery.format("Name must consist of alphabetical characters only"),
                minlength: "Name should be atleast 3 characters",
            },

            expiration_date_edit: {
                required: "Please enter in a valid expiration date",
                CcExpDate: "Please enter in a valid expiration date"
            },
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        }
    });
});

$('#editCreditCardSave').on('submit', function (e) {
    e.preventDefault();
    var $form = $(this);
    if (!$form.valid())
        return false;

    showLoader();
    $.ajax({
        type: "POST",
        url: saveCard,
        dataType: "JSON",
        data: new FormData(document.getElementById("editCreditCardSave")),
        processData: false,
        contentType: false,
        success: function (msg) {
            $("#editCard").modal('hide');
            document.getElementById("editCreditCardSave").reset();
            $("#editCreditCardSave")[0].reset();
            $("html, body").animate({ scrollTop: 200 }, "slow");
            $(".modal-backdrop.fade.show").css("display", "none");
            Swal.fire({
                icon: "success",
                title: msg.message,
                showConfirmButton: false,
                timer: 3000
            });
            $("#paymentMethodsCredit").DataTable().ajax.reload(null, false);
            hideLoader();
        }
    });

});

