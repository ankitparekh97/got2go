$(document).ready(function () {
  $('.lazy-load').Lazy({
    scrollDirection: 'vertical',
    effect: 'fadeIn',
    visibleOnly: false,
    onError: function (element) {
      console.log('error loading ' + element.data('src'));
    }
  });
  $("#responsive-menu").click(function () {
    $(".responsive-menu-cstm").toggle();
  });
});