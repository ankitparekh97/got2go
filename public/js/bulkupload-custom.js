/*
 *  Customized Jquery for Site.
 */


var currentTab = 0; var count = 2;
if (typeof reUpload !== 'undefined') {
    currentTab = (reUpload!='') ? 1: 0; // Current tab is set to be the first tab (0)
    count = 3;
    jQuery('.listing-footer-btns button#cancel-upload').hide();
} else {
    currentTab = 0;
    count = 2;
}

showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("outer-table");
  x[n].style.display = "block";
  currentTab = n;
  //... and fix the Previous/Next buttons:

  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (currentTab == 1) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Upload Files";
    // saveChanges()
  }
  //... and run a function that will display the correct step indicator:
  //fixStepIndicator(n)
}

function nextPrev(n) {
    var form = jQuery("#bulk-upload-form");
    var jQueryqty=jQuery(this).closest('div').find('.cmn-add');
    var currentVal = parseInt(jQueryqty.val());
    if (!isNaN(currentVal)) {
        jQueryqty.val(currentVal + 1);
    }

    if (count > 1) {
        jQuery('.listing-footer-btns button#cancel-upload').hide();
        jQuery('ul#ListingTab li:first-child').addClass('success');
        jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
    }
    if (count > 2) {
        jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
    }
    if (count > 3) {
        jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
    }

    jQuery('.nav.nav-tabs.custom-tab').addClass('counter'+count);
    count++;

    // This function will figure out which tab to display
    var x = document.getElementsByClassName("outer-table");
    // Exit the function if any field in the current tab is invalid:

    //if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";

    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;

    // if you have reached the end of the form...
    if (currentTab >= x.length) {

        // ... the form gets submitted:
        //document.getElementById("regForm").submit();
        // return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function nextPrev1(n) {
    var jQueryqty=jQuery(this).closest('div').find('.cmn-add');
    var currentVal = parseInt(jQueryqty.val());

    if (!isNaN(currentVal) && currentVal > 0) {
        jQueryqty.val(currentVal - 1);
    }

    count--;
    jQuery('.nav.nav-tabs.custom-tab').removeClass('counter'+count);
    if (count > 2) {
        jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
        jQuery('.listing-footer-btns button#cancel-upload').show();
    } else {
        jQuery('ul#ListingTab li:first-child').removeClass('success');
        jQuery('ul#ListingTab li:nth-child(2) a').addClass('disabled');
        jQuery('.listing-footer-btns button#cancel-upload').show();
    }
    if (count > 3) {
        jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
    } else {
        jQuery('ul#ListingTab li:nth-child(2)').removeClass('success');
        jQuery('ul#ListingTab li:nth-child(3) a').addClass('disabled');
    }
    if (count > 4) {
        jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
    } else {
        jQuery('ul#ListingTab li:nth-child(3)').removeClass('success');
        jQuery('ul#ListingTab li:nth-child(4) a').addClass('disabled');
    }

    // This function will figure out which tab to display
    var x = document.getElementsByClassName("outer-table");

    // Exit the function if any field in the current tab is invalid:
    //if (n == 1 && !validateForm()) return false;

    // Hide the current tab:
    x[currentTab].style.display = "none";

    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;

    // if you have reached the end of the form...
    if (currentTab >= x.length) {
    alert('done');
    // ... the form gets submitted:
    //document.getElementById("regForm").submit();
    return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}



