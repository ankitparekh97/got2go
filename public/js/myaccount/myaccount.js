$(document).ready(function() {

    $('#notificationForm').on('submit', function (e) {
        e.preventDefault();
        showLoader();

        $.ajax({
            type: "POST",
            url: notificationUrl,
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(msg) {
                hideLoader();

                if($.isEmptyObject(msg.error)){
                    if(msg.success == true){
                        // window.onbeforeunload = null;
                        setTimeout(function(){
                            $.unblockUI()
                        },1000)

                        Swal.fire({
                            icon: "success",
                            title: "Your notifications are saved!",
                            showConfirmButton: false,
                            timer: 3000,
                            heightAuto: false,
                            onAfterClose: () => $('html, body').animate({scrollTop:0}, 'slow')
                        })
                    }
                }
            }
        });
    });

    var isSubscribed = is_subscribed.is_subscribed;
    if(isSubscribed == 1){
        $("input[name='is_subscribed'").prop('checked', true);;
    }

    for (x in preferences) {
        var  title = preferences[x].communication_title;
        var  is_notification = preferences[x].is_notification;
        var  is_email = preferences[x].is_email;
        var  is_sms = preferences[x].is_sms;

        if(is_notification == '1') $("input[name='prefernces["+title+"][is_notification]']").prop('checked', true);
        if(is_email == '1') $("input[name='prefernces["+title+"][is_email]']").prop('checked', true);
        if(is_sms == '1') $("input[name='prefernces["+title+"][is_sms]']").prop('checked', true);
     }
});

