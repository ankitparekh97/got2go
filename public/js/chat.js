$(function () {
    $(document).ready(function () {
        let pusher = new Pusher($("#pusher_app_key").val(), {
            cluster: $("#pusher_cluster").val(),
            encrypted: true
        });

        let channel = pusher.subscribe('chat');
        searchForData();
        $(document).on('click', '.chat-back-button', function (e) {
            $('.chat-contact-mobile').css('display', 'block');
            $('.chat-detail-mobile').css('display', 'none');
        });
        // on click on any chat btn render the chat box

        $(document).on('click', '.chat-toggle', function (e) {
            checkScreenSize();
            e.preventDefault();

            let ele = $(this);

            let id = ele.attr("data-id");

            let username = ele.attr("data-user");

            let userimage = ele.attr("data-image");

            let toUserId = ele.attr('data-touserid');

            let userType = ele.attr('data-type');

            cloneChatBox(id, username, userimage, toUserId, userType, function () {
                $('.chatcontact').removeClass('active');
                unreadFlag(id);
                $('.chat_contact_' + id).find('.message-block .message-count').remove();
                ele.css('background-color', '');
                ele.addClass('active');
                let chatBox = $("#chat_box_" + id);
                loadLatestMessages(chatBox, id);



            });
        });

        // on close chat close the chat box but don't remove it from the dom
        $(".close-chat").on("click", function (e) {

            $(this).parents("div.chat-opened").removeClass("chat-opened").slideUp("fast");
        });

        // on change chat input text toggle the chat btn disabled state
        $(".chat_input").on("change keyup", function (e) {
            if ($(this).val() != "") {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", false);
            } else {
                $(this).parents(".form-controls").find(".btn-chat").prop("disabled", true);
            }
        });
        $('.chat_input').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                var button = $(this).parent().next().children();
                send(button.attr('data-id'), button.attr('data-to-user'), $("#chat_box_" + button.attr('data-id')).find(".chat_input").val());
            }
        });
        // on click the btn send the message
        $(".btn-chat").on("click", function (e) {
            send($(this).attr('data-id'), $(this).attr('data-to-user'), $("#chat_box_" + $(this).attr('data-id')).find(".chat_input").val());
        });
        // listen for the send event, this event will be triggered on click the send btn
        channel.bind('send', function (data) {
            displayMessage(data.data);
        });

        let lastScrollTop = 0;
        $(".chat-area").on("scroll", function (e) {
            let st = $(this).scrollTop();
            if (st < lastScrollTop) {
                fetchOldMessages($(this).parents(".chat-opened").find("#to_user_id").val(), $(this).find(".msg_container:first-child").attr("data-message-id"));
            }
            lastScrollTop = st;
        });
        // listen for the oldMsgs event, this event will be triggered on scroll top
        channel.bind('oldMsgs', function (data) {
            displayOldMessages(data);
        });

        //On click search show conatct list
        $("#search").keyup(function (e) {
            if (e.keyCode == 13) {
                /*  alert(2); */
                e.preventDefault();
                return false;
            }
            $('.searchclose').css('display', 'inline-block');
            var val = this.value;
            val = val.replace(/^\s|\s $/, "");

            if (val !== "") {
                searchForData(val);
            } else {
                // clearResult();
            }
        });

        //close search data
        $(document).on('click', '.searchclose', function (e) {
            e.preventDefault();
            searchForData();
            $('.searchclose').css('display', 'none');
            $("#search").val('');
        });
    });

});


/**
 * loaderHtml
 *
 * @returns {string}
 */
function loaderHtml() {
    return '<i class="glyphicon glyphicon-refresh loader"></i>';
}

/**
 * getMessageSenderHtml
 *
 * this is the message template for the sender
 *
 * @param message
 * @returns {string}
 */
function getMessageSenderHtml(message) {
    if (message.ownerphoto != null) {
        var imageHtml = '<img src="' + imagesendPath + '/' + message.ownerphoto + '" alt=""></img>';
    } else {
        var matches = message.ownerUserName.match(/\b(\w)/g);
        var acronym = matches.join('');
        var imageHtml = '<div id="profileImage" class="owner">' + acronym + '</div>';
    }
    var separatorHtml = '';
    if ($(".message-list ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }

    return `${separatorHtml}
            <li class="replies" data-message-id="${message.id}">
            <div class="img">
            ${imageHtml}
            </div>
            <div class="message-wrapper">
                <div class="message-box">
                    <p>${message.messages}</p>
                </div>
                <div class="time">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
            </div>
        </li>`;
}

/**
 * getMessageReceiverHtml
 *
 * this is the message template for the receiver
 *
 * @param message
 * @returns {string}
 */
function getMessageReceiverHtml(message) {
    if (message.type == 'admin') {
        var imageHtml = '<img src="' + adminLogo + '" alt=""></img>';
    } else {
        if (message.rentalphoto != null) {
            var imageHtml = '<img src="' + imagePath + '/' + message.rentalphoto + '" alt=""></img>';
        } else {
            var matches = message.rentalUserName.match(/\b(\w)/g);
            var acronym = matches.join('');
            var imageHtml = '<div id="profileImage" class="rental">' + acronym + '</div>';
        }
    }

    var separatorHtml = '';
    if ($(".message-list ul li.date-separator span").last().text() != 'Today') {
        var separatorHtml = `<li class="date-separator">
                            <div class="separator-info">
                            <span>Today</span>
                            </div>
                            </li >`;
    }
    return `${separatorHtml}
            <li class="sent" data-message-id="${message.id}">
            <div class="img">
            ${imageHtml}
            </div>
            <div class="message-wrapper">
                <div class="message-box">
                    <p>${message.messages}</p>
                </div>
                <div class="time">${converToLocalTimeZone(message.dateTimeStr, 'time')}</div>
            </div>
        </li>`;
}


/**
 * cloneChatBox
 *
 * this helper function make a copy of the html chat box depending on receiver user
 * then append it to 'chat-overlay' div
 *
 * @param user_id
 * @param username
 * @param callback
 */
function cloneChatBox(id, username, userimage, toUserId, userType, callback) {

    if ($("#chat_box_" + id).length == 0) {
        //$("#chat-overlay").html("");
        let cloned = $("#chat_box").clone(true);

        // change cloned box id
        cloned.attr("id", "chat_box_" + id);

        cloned.find(".chatcontent-header .name-wrap .name").text(username);

        cloned.find(".chat-action .btn-chat").attr("data-to-user", toUserId);

        cloned.find(".chat-action .btn-chat").attr("data-id", id);

        cloned.find("#id").val(id);
        cloned.find("#to_user_id").val(toUserId);

        if (userType == 'admin') {
            var imageHtmL = '<img src="' + adminLogo + '" alt="">';
            cloned.find(".chatcontent-header .name-wrap .name").addClass('admin_name');
            cloned.find('.chatcontent-header .content-link').hide();
            cloned.find('.chatcontent-footer').hide();
        }
        else {
            if (userimage != '') {
                var imageHtmL = '<img src="' + imagePath + '/' + userimage + '" alt="">';
            } else {
                var matches = username.match(/\b(\w)/g);
                var acronym = matches.join('');
                var imageHtmL = '<div id="profileImage" class="rental">' + acronym + '</div>';
            }
        }

        cloned.find('.chatcontent-header .content-userimg ').append(imageHtmL);
        cloned.css({ 'display': 'block' });

        $("#chat-overlay").html("");
        $("#chat-overlay").append(cloned);
    }

    callback();
}

/**
 * loadLatestMessages
 *
 * this function called on load to fetch the latest messages
 *
 * @param container
 * @param user_id
 */
function loadLatestMessages(container, id) {
    let chat_area = container.find(".chatcontent-body .message-list ul");

    chat_area.html("");

    $.ajax({
        url: getMessage,
        data: { user_id: id, _token: $("meta[name='csrf-token']").attr("content") },
        headers: {
            "Authorization": Authorization,
            "Timezone": timezone
        },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            showLoader();
        },
        success: function (response) {
            if (response.state == 1) {
                response.messages.map(function (val, index) {
                    $(val).appendTo(chat_area);

                });
            }
        },
        complete: function () {
            $("#chat_box_" + id + " .chatcontent-body").scrollTop($("#chat_box_" + id + " .chatcontent-body .message-list").outerHeight());
            hideLoader();
        }
    });
}
/**
 * send
 *
 * this function is the main function of chat as it send the message
 *
 * @param to_user
 * @param message
 */
function send(id, toUser, message) {
    let chat_box = $("#chat_box_" + id);
    let chat_area = chat_box.find(".chatcontent-body .message-list ul");
    $.ajax({
        url: sendMessage,
        data: { id: id, to_user: toUser, message: message, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
        },
        complete: function () {
            //hideLoader();
            // /chat_box.find(".btn-chat").prop("disabled", true);
            chat_box.find(".chat_input").val("");
            $("#chat_box_" + id + " .chatcontent-body").animate({ scrollTop: $("#chat_box_" + id + " .chatcontent-body .message-list").outerHeight() }, 800, 'swing');
        }
    });
}
/**
 * This function called by the send event triggered from pusher to display the message
 *
 * @param message
 */
function displayMessage(message) {
    let alert_sound = document.getElementById("chat-alert-sound");
    let chatBox = $("#chat_box_" + message.pair_user_id);
    if ($('#chat_contact_' + message.pair_user_id).length == 1) {

    }
    if ($("#current_user").val() == message.from_user_id && message.from_user_type == 'owner') {
        $('.chat_contact_' + message.pair_user_id).find('.preview').text(message.messages);
        $('.chat_contact_' + message.pair_user_id).find('.chat-date').text(converToLocalTimeZone(message.dateTimeStr, 'date'));
        $('.chat_contact_' + message.pair_user_id).prependTo($('.chat_contact_' + message.pair_user_id).parent());
        let messageLine = getMessageSenderHtml(message);
        $("#chat_box_" + message.pair_user_id).find(".chatcontent-body .message-list ul").append(messageLine);

    } else if ($("#current_user").val() == message.to_user_id && message.to_user_type == 'owner') {
        //alert_sound.play();
        if (message.type == 'admin') {
            message.rentalUserName = message.adminUserName;
        }
        if ($(".chat_contact_" + message.pair_user_id).length == 0) {
            let chatCloned = $(".chatcontact-list ul li:first").clone(true);
            chatCloned.removeAttr('class');
            chatCloned.addClass("chatcontact chat-toggle chat_contact_" + message.pair_user_id);
            chatCloned.attr('id', "#chat_contact_" + message.pair_user_id);
            chatCloned.attr('data-id', message.pair_user_id);
            chatCloned.attr('data-user', message.rentalUserName);
            chatCloned.attr('data-image', message.rentalphoto);
            chatCloned.attr('data-touserid', message.from_user_id);
            chatCloned.attr('data-type', message.type);
            chatCloned.find('.name').text(message.rentalUserName);
            chatCloned.find('.preview').text(message.messages);
            chatCloned.find('.message-block .message-count').remove();
            chatCloned.find('.message-block').append('<span class="message-count">' + message.message_count + '</span>');
            chatCloned.find('.chat-date').text(converToLocalTimeZone(message.dateTimeStr, 'date'));
            chatCloned.css("background-color", "#00800024");
            if (message.type == 'admin') {
                var imageHtml = '<img src="' + adminLogo + '" alt=""></img>';
            } else {
                if (message.rentalphoto != null) {
                    var imageHtml = '<img src="' + imagePath + '/' + message.rentalphoto + '" alt=""></img>';
                } else {
                    var matches = message.rentalUserName.match(/\b(\w)/g);
                    var acronym = matches.join('');
                    var imageHtml = '<div id="profileImage" class="rental">' + acronym + '</div>';
                }
            }
            chatCloned.find('.meta-left').html(imageHtml);
            $(".chatcontact-list ul").prepend(chatCloned);

        } else {
            if ($('.chat_contact_' + message.pair_user_id).hasClass('active')) {
                unreadFlag(message.pair_user_id);
            } else {
                $('.chat_contact_' + message.pair_user_id).find('.message-block .message-count').remove();
                $('.chat_contact_' + message.pair_user_id).css("background-color", "#00800024");
                $('.chat_contact_' + message.pair_user_id).find('.message-block').append('<span class="message-count">' + message.message_count + '</span>');
            }
            $('.chat_contact_' + message.pair_user_id).find('.preview').text(message.messages);
            $('.chat_contact_' + message.pair_user_id).find('.chat-date').text(converToLocalTimeZone(message.dateTimeStr, 'date'));
            $('.chat_contact_' + message.pair_user_id).prependTo($('.chat_contact_' + message.pair_user_id).parent());
            let messageLine = getMessageReceiverHtml(message);
            // append the message for the receiver user
            $("#chat_box_" + message.pair_user_id).find(".chatcontent-body .message-list ul").append(messageLine);
            $("#chat_box_" + message.pair_user_id + " .chatcontent-body").animate({ scrollTop: $("#chat_box_" + message.pair_user_id + " .chatcontent-body .message-list").outerHeight() }, 800, 'swing');
        }
    }
}
/**
 * fetchOldMessages
 *
 * this function load the old messages if scroll up triggerd
 *
 * @param to_user
 * @param old_message_id
 */
function fetchOldMessages(to_user, old_message_id) {
    let chat_box = $("#chat_box_" + to_user);
    let chat_area = chat_box.find(".chat-area");
    $.ajax({
        url: base_url + "/fetch-old-messages",
        data: { to_user: to_user, old_message_id: old_message_id, _token: $("meta[name='csrf-token']").attr("content") },
        method: "GET",
        dataType: "json",
        beforeSend: function () {
            if (chat_area.find(".loader").length == 0) {
                chat_area.prepend(loaderHtml());
            }
        },
        success: function (response) {
        },
        complete: function () {
            chat_area.find(".loader").remove();
        }
    });
}
function displayOldMessages(data) {
    if (data.data.length > 0) {
        data.data.map(function (val, index) {
            $("#chat_box_" + data.to_user).find(".chat-area").prepend(val);
        });
    }
}

function searchForData(value, isLoadMoreMode) {
    $.ajax({
        url: chatSearch,
        data: { name: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            if (response.users.length != 0) {
                $('.chatcontact-list').html(response.html);

                if (chatId != '') {
                    var ele = $('#chat_contact_' + chatId);

                    var id = ele.attr("data-id");

                    var username = ele.attr("data-user");

                    var userimage = ele.attr("data-image");

                    var toUserId = ele.attr('data-touserid');

                    var userType = ele.attr('data-type');
                } else {
                    var ele = $('.chatcontact-list ul li:first');

                    var id = ele.attr("data-id");

                    var username = ele.attr("data-user");

                    var userimage = ele.attr("data-image");

                    var toUserId = ele.attr('data-touserid');

                    var userType = ele.attr('data-type');
                }



                cloneChatBox(id, username, userimage, toUserId, userType, function () {
                    $('.chatcontact').removeClass('active');
                    unreadFlag(id);
                    $('.chat_contact_' + id).find('.message-block .message-count').remove();
                    ele.css('background-color', '');
                    ele.addClass('active');
                    let chatBox = $("#chat_box_" + id);
                    loadLatestMessages(chatBox, id);



                });
            } else {
                $('.chatcontact-list ul').css('display', 'none');
                $('.chatcontact-list').find('.no-user').remove();
                $('.chatcontact-list').prepend('<span class="no-user">No users found</span>');
            }

        }
    });
}
function converToLocalTimeZone(datetime, pattern) {
    var localTime = moment.utc(datetime).toDate();
    if (pattern == 'time') {
        localTime = moment(localTime).format('hh:mm A');
    } else if (pattern == 'date') {
        localTime = moment(localTime).format('MM/DD/YYYY');
    }
    return localTime;
}
function unreadFlag(value) {
    $.ajax({
        url: unreadUrl,
        data: { id: value, _token: $("meta[name='csrf-token']").attr("content") },
        method: "POST",
        dataType: "json",
        success: function (response) {
            $('#chat_contact_' + value).find('.message-count').remove();
        }
    });
}
$(window).on("resize", function (e) {
    checkReSize();
});
function checkScreenSize() {
    var newWindowWidth = $(window).width();
    if (newWindowWidth < 767) {
        $('.chat-contact-mobile').css('display', 'none');
        $('.chat-detail-mobile').css('display', 'block');
    }
}
function checkReSize() {
    var newWindowWidth = $(window).width();
    if (newWindowWidth > 767) {
        $('.chat-contact-mobile').css('display', 'block');
        $('.chat-detail-mobile').css('display', 'block');
    }
}
