$(document).ready(function () {
    $('.alert-popup-box').hide();
    $('#lets_go_tab').on('click', '#letsgo', function (e) {
        e.preventDefault();
        showLoader();

        let id = $(this).data('id');
        let check_in_date = checkIndate;
        let check_out_date = checkOutdate;
        let selectedCard = $('.selcted-card').find('.card_no').attr('data-id');

        let cardDetails = $('.cardDetails').is(":visible");
        let sel = $('.dropdown-toggle').text();
        if ($('.cardDetails').css("display") === "block" || sel.trim() == 'Add Debit/Credit Card') {

            Swal.fire({
                icon: "info",
                title: 'Please enter Payment Details or select different payment details.',
                showConfirmButton: true,
            })
            hideLoader();
            return false;

        } else {
            $.ajax({
                type: "POST",
                url: propertyBooking,
                dataType: "JSON",
                headers: {
                    "Authorization": Authorization,
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                },
                data: {
                    id: id,
                    check_in_date: check_in_date,
                    check_out_date: check_out_date,
                    guest: guest,
                    message: $('#message').val(),
                    card_type: $('#cardType').val(),
                    propertyOfferId: $("#propertyOfferId").val(),
                    selectedCard: selectedCard,
                    couponCodeName: $("#couponCodeName").val()
                },
                // processData: false,
                // contentType: false,
                success: function (msg) {
                    hideLoader();

                    if ($.isEmptyObject(msg.error)) {
                        if (msg.success == true) {
                            let checkInDate = new Date(check_in_date);
                            /* let check_in_dt = checkInDate.toLocaleString('default', { month: 'short' }) + ' ' + checkInDate.getDate() + ', ' + checkInDate.getFullYear(); */
                            let check_in_dt = moment(check_in_date).format('MMM DD, YYYY');

                            let checkOutDate = new Date(check_out_date);
                            /* let check_out_dt = checkOutDate.toLocaleString('default', { month: 'short' }) + ' ' + checkOutDate.getDate() + ', ' + checkOutDate.getFullYear(); */
                            let check_out_dt = moment(check_out_date).format('MMM DD, YYYY');

                            let istantBookingPropertyText = 'You have successfully booked "' + msg.propertyDetail.title + '" from ' + check_in_dt + ' to ' + check_out_dt + '. We will send you a confirmation email shortly.';
                            let normalBookingPropertyText = 'We have sent your booking request to the owner. They have 24 hours to approve your request. Upon approval,  you will recieve a booking and payment confirmation email. If approval is denied, we will notify you and suggest you and provide suggestions for similar stays.';
                            let content = (msg.propertyDetail.is_instant_booking_allowed == '1') ? istantBookingPropertyText : normalBookingPropertyText;

                            $('#lets_go_tab').remove();
                            $('#booking_success_modal .contact-host-wrapper p').html(content);
                            $('#booking_success_modal').modal('show');
                        }
                    } else {
                        $('.alert-popup-box p').empty().html(msg.error).show();
                        $('.alert-popup-box').show();
                        $('html, body').animate({ scrollTop: 50 }, 'slow')
                    }
                }
            });
        }

    });

    $('#lets_go_tab').on('click', '#lets_go_0', function (e) {
        e.preventDefault();
        showLoader();
        hideLoader();
        let getAvailblityStatus = $('.availabelDtsErr').is(":visible");
        if (getAvailblityStatus == false) {
            $('#propertyBooking').attr('action', '/review');
            $('#propertyBooking').submit();
        }

        // var url_string = window.location.href;
        // var url = new URL(url_string);
        // var c = url.pathname.substring(url.pathname.lastIndexOf('/') + 1);
        // location.href = '/review/'+c+url.search;
    });


    $("#text1").on("change keyup", function (e) {
        var charCount = $.trim($(this).val()).length;
        if (charCount != 0) {
            $(this).parents(".contact-host-wrapper").find("#save_btn").prop("disabled", false);
        } else {
            $(this).parents(".contact-host-wrapper").find("#save_btn").prop("disabled", true);
        }
    });
    $('#save_btn').on('click', function (e) {
        e.preventDefault();
        showLoader();

        let id = $(this).data('owner-id');
        let message = $('#text1').val();

        $.ajax({
            type: "POST",
            url: contacthost,
            dataType: "JSON",
            headers: {
                "Authorization": Authorization,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                to_user: id,
                message: message,
            },
            // processData: false,
            // contentType: false,
            success: function (msg) {
                hideLoader();

                if ($.isEmptyObject(msg.error)) {
                    if (msg.success == true) {
                        $('.contact-host-wrapper textarea').val('');
                        $(".contact-host-wrapper").find("#save_btn").prop("disabled", true);
                        $('#contact_host_success .contact-host-wrapper .success').remove();
                        $('#Contacthost').modal('hide');
                        $('#contact_host_success .contact-host-wrapper').prepend('<span class="success">Message sent successfully! Host will get back to you soon. </br> <a href=' + chatURL + '>Click here</a> to continue the chat. Thank You!</span>');
                        $('#contact_host_success').modal('show');

                        $('#text1').val('');
                    }
                }
            }
        });
    });

    // If JavaScript is enabled, hide fallback select field
    $('.no-js').removeClass('no-js').addClass('js');

    // When the user focuses on the credit card input field, hide the status
    $('.card input').bind('focus', function () {
        $('.card .status').hide();
    });

    // When the user tabs or clicks away from the credit card input field, show the status
    $('.card input').bind('blur', function () {
        $('.card .status').show();
    });

    // Run jQuery.cardcheck on the input
    $('.card input').cardcheck({
        callback: function (result) {

            var status = (result.validLen && result.validLuhn) ? 'valid' : 'invalid',
                message = '',
                types = '';

            // Get the names of all accepted card types to use in the status message.
            for (i in result.opts.types) {
                types += result.opts.types[i].name + ", ";
            }
            types = types.substring(0, types.length - 2);

            // Set status message
            if (result.len < 1) {
                message = 'Please provide a credit card number.';
            } else if (!result.cardClass) {
                message = 'We accept the following types of cards: ' + types + '.';
            } else if (!result.validLen) {
                message = result.cardName;
            } else if (!result.validLuhn) {
                message = result.cardName;
            } else {
                message = result.cardName;
            }
            $('#card_type').val(result.cardName);
            // Show credit card icon
            $('.card .card_icon').removeClass().addClass('card_icon ' + result.cardClass);

            // Show status message
            $('.card .status').removeClass('invalid valid').addClass(status).children('.status_message').text(message);
        }
    });

    // Credit Card Expiry Date validator
    $.validator.addMethod('CCExp', function (value, element, params) {
        var minMonth = new Date().getMonth() + 1;
        var minYear = new Date().getFullYear();
        var month = parseInt($(params.month).val(), 10);
        var year = parseInt($(params.year).val(), 10);

        return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
    }, 'Credit Card Expiration date is invalid.');


    $.validator.addMethod(
        "CcExpDate",
        function (value, element) {
            var today = new Date();
            var thisYear = today.getFullYear();

            const expDate = value.split('/');
            var expMonth = expDate[0].trim();
            if (expMonth.length == 1) return false;

            if (!expDate[1]) return false;

            var expYear = parseInt(expDate[1].trim());

            return (expMonth >= 1 && expMonth <= 12
                && (expYear >= thisYear && expYear < thisYear + 20)
                && (expYear == thisYear ? expMonth >= (today.getMonth() + 1) : true))
        },
        "Must be a valid Expiry Date"
    );

    // CVV validator
    $.validator.addMethod("CVV", function (value, element, min) {
        var cvv = value.length;
        var card = $('.status_message').text();

        if (card == "American Express" && cvv < 4) {
            return !cvv;
        } else if (card != "American Express" && cvv == 4) {
            return !cvv;
        } else {
            return true;
        }

    }, "Please enter in a valid security code");

    $('#last_four').on('keypress change', function () {
        $(this).val(function (index, value) {
            return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        });
    });

    // alphabet validator
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Letters and spaces only please");


    $(".payment-form #message").on("change keyup", function (e) {
        var charCount = $.trim($(this).val()).length;
        $('.payment-form .offer-error-span').remove();
        if (charCount == 0) {
            if ($(this).val() == "") {
                $('.payment-form .offer-error-span').remove();
            } else {
                $(this).parent('.payment-form').prepend('<span class="offer-error-span">Invalid Message</span>')
            }

        } else {
            $('.payment-form .offer-error-span').remove();
        }
    });
    var $rentalCompleteBookingPhoneForm = $("#rentalCompleteBookingPhoneForm");
    $rentalCompleteBookingPhoneForm.find("#rentalPhoneNumber").mask('(000) 000-0000', { placeholder: "Phone number (000) 000-0000" });

    var $rentalCompleteBookingPhoneFormValidate = $rentalCompleteBookingPhoneForm.validate({
        rules: {
            rentalPhoneNumber: {
                required: true,
                validateUSPhoneNumber: true
            }
        },
        messages: {
            rentalPhoneNumber: {
                required: 'Phone number is required.',
                validateUSPhoneNumber: 'Please enter valid phone number.'
            }
        },
        submitHandler: function () {
            let data = {
                phoneNumber: $rentalCompleteBookingPhoneForm.find("#rentalPhoneNumber").val()
            };
            saveRentalInfo(data);
        }
    });

    var $rentalCompleteBookingEmailForm = $("#rentalCompleteBookingEmailForm");
    var $rentalCompleteBookingEmailFormValidate = $rentalCompleteBookingEmailForm.validate({
        rules: {
            rentalEmail: {
                required: true,
                email: true
            }
        },
        messages: {
            rentalEmail: {
                required: 'Email address is required.',
                email: 'Please enter valid email address.'
            }
        },
        submitHandler: function () {
            let data = {
                email: $rentalCompleteBookingEmailForm.find("#rentalEmail").val()
            };

            saveRentalInfo(data);
        }
    });
});
function saveRentalInfo(data) {

    $.ajax({
        type: "POST",
        url: updateRentalProfile,
        dataType: "JSON",
        headers: {
            "Authorization": "Bearer {{session()->get('admin_token')}}",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: data,
        success: function (data) {
            window.location.reload();
        },
        error: function (xhr, status, error) {
        }
    });

}
function checkavaibility() {
    showLoader();

    let id = $('#profile-info').data('id');
    let check_in_date = $('#kt_datepicker_3').val();
    let check_out_date = $('#kt_datepicker_4').val();

    $.ajax({
        type: "POST",
        url: checkBooking,
        dataType: "JSON",
        data: {
            id: id,
            check_in_date: check_in_date,
            check_out_date: check_out_date,
        },
        // processData: false,
        // contentType: false,
        success: function (msg) {
            hideLoader();

            const getBookings = Object.keys(msg.isPropertyBooked).length;
            if (getBookings == 0) {
                $.unblockUI()
                let isAllowed = (isInstantBookingAllowed == '1') ? 'letsgo' : 'lets_go_0';
                let showLink = (isloggedIn == '') ? '<a style="cursor: pointer" href="#" class="login-modal red-btn" data-toggle="modal" data-target="#login_popup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> LET’S GO</a>' : '<a href="javascript:void(0)" id="' + isAllowed + '" class="red-btn" data-id=' + propertyId + ' > LET’S GO</a>';

                if (msg.propertyAvaliblity.length >= 1) {
                    $('.availabelDtsErr').hide();
                } else {
                    $('.availabelDtsErr').show();
                }
            } else {
                $('.availabelDtsErr').show();
            }
        }
    });
}
