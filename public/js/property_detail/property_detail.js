let propertyTripperPrice = $('.tripper_price').text().replace(/,/g, '');
if (parseInt(propertyTripperPrice) < parseInt(offerFlagPrice)) {
    var diff = parseInt(propertyPrice) - parseInt(offerFlagPrice);
    if (parseInt(propertyTripperPrice) < diff) {
        offerFlagPrice = parseInt(propertyTripperPrice - 10);
    } else {
        offerFlagPrice = parseInt(propertyTripperPrice) - diff;
    }

}

$(document).ready(function () {

    let checkInDatePickerObj = $('#kt_datepicker_3');
    let checkOutDatePickerObj = $('#kt_datepicker_4');

    let checkInDate = checkInDatePickerObj.val();
    let checkOutDate = checkOutDatePickerObj.val();

    $('#make_offer').click(function () {

        showLoader();

        let id = $(this).data('id');
        let propertyName = $('.title-large:first').text();
        let propertyAddress = $('.right-text:first h4').text();
        let propertyImage = $('.property_image').val();
        let ownerId = $('#owner_id').val();
        let checkInDate = checkInDatePickerObj.val();
        let checkOutDate = checkOutDatePickerObj.val();
        let makeOfferObj = $('#makeoffer');
        let guest = $('#guests').val();

        makeAnOfferFormValidate.resetForm();
        resetMakeAnOfferPlusMinusButton();
        $('.availabelDtsErr').html('').hide();
        $('.availabelDtsErrOffer').html('').hide();

        $.ajax({
            type: "POST",
            url: propertyInstantBooking,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                id: id,
                check_in_date: checkInDate,
                check_out_date: checkOutDate,
                guest: guest
            },
            // processData: false,
            // contentType: false,
            success: function (msg) {
                hideLoader();

                if ($.isEmptyObject(msg.error)) {
                    if (msg.success == true) {
                        //reset

                        makeOfferObj.find('.header-image .image img').remove();
                        makeOfferObj.find('#flexible_dates').prop('checked', true);
                        makeOfferObj.find('#offer_message').val('');

                        //end of reset

                        makeOfferObj.find('.header-image .image').append('<img src="' + propertyImagePath + '/' + propertyImage + '" alt="' + propertyName + '">');
                        makeOfferObj.find('.header-image .title h4').text(propertyName);
                        makeOfferObj.find('.header-image .title p').text(propertyAddress);
                        makeOfferObj.find('.currnt-night-price .price_night').text("$" + commaSeparateNumber(propertyTripperPrice));
                        makeOfferObj.find('.proposed-night-rate #tet123').val(Math.round(propertyTripperPrice) - 1);
                        makeOfferObj.find('.property_id').val(property_id);
                        makeOfferObj.find('.owner_id').val(ownerId);
                        makeOfferObj.find('#kt_datepicker_5').datepicker('setDate', checkInDate);
                        makeOfferObj.find('#kt_datepicker_6').datepicker('setDate', checkOutDate);
                        var date2 = makeOfferObj.find('#kt_datepicker_5').datepicker('getDate', '+1d');
                        date2.setDate(date2.getDate() + 1);
                        makeOfferObj.find('#kt_datepicker_6').datepicker('setStartDate', date2);
                        $('.offer-popup-content').show();
                        $('.offer-popup-success').hide();

                        $(this).closest('div').find('.plus')
                        makeOfferObj.find('.proposed-night-rate span.plus').addClass('max');

                        makeOfferObj.modal("show");
                    }
                } else {
                    $('.availabelDtsErr').show().html(msg.error);
                    $('.guest-info').addClass('error');
                }
            }
        });
    });

    $('#lets_go_tab').on('click', '#letsgo', function (e) {
        e.preventDefault();
        showLoader();
        $('.availabelDtsErr').html('').hide();

        let id = $(this).data('id');
        let check_in_date = $('#kt_datepicker_3').val();
        let check_out_date = $('#kt_datepicker_4').val();
        let guest = $('#guests').val();

        $.ajax({
            type: "POST",
            url: propertyInstantBooking,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                id: id,
                check_in_date: check_in_date,
                check_out_date: check_out_date,
                guest: guest,
                offerValidation: false
            },
            // processData: false,
            // contentType: false,
            success: function (msg) {
                hideLoader();

                if ($.isEmptyObject(msg.error)) {
                    if (msg.success == true) {
                        e.preventDefault();
                        // window.onbeforeunload = null;
                        location.href = reviewPage;
                    }
                } else {
                    $('.availabelDtsErr').html(msg.error).show();
                    $('.guest-info').addClass('error');
                }
            }
        });
    });

    $('#lets_go_tab').on('click', '#lets_go_0', function (e) {
        e.preventDefault();
        // window.onbeforeunload = null;
        location.href = reviewPage;

    });

    $("#text1").on("change keyup", function (e) {
        var charCount = $.trim($(this).val()).length;
        if (charCount != 0) {
            $(this).parents(".contact-host-wrapper").find("#save_btn").prop("disabled", false);
        } else {
            $(this).parents(".contact-host-wrapper").find("#save_btn").prop("disabled", true);
        }
    });
    $("#offer_message").on("change keyup", function (e) {
        var charCount = $.trim($(this).val()).length;
        $('.offer-bottom-form .offer-error-span').remove();
        if (charCount == 0) {
            if ($(this).val() == "") {
                $('.offer-bottom-form .offer-error-span').remove();
            } else {
                $(this).parent('.offer-bottom-form').prepend('<span class="offer-error-span">Invalid Message</span>')
            }

        } else {
            $('.offer-bottom-form .offer-error-span').remove();
        }
    });

    $('#save_btn').on('click', function (e) {
        e.preventDefault();
        $('#Contacthost').find('.error-class').remove();
        showLoader();

        let id = $(this).data('owner-id');
        let message = $('#text1').val();

        $.ajax({
            type: "POST",
            url: contacthost,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: {
                to_user: id,
                message: message,
            },
            // processData: false,
            // contentType: false,
            success: function (msg) {
                hideLoader();

                if ($.isEmptyObject(msg.error)) {
                    if (msg.success == true) {
                        $('.contact-host-wrapper textarea').val('');
                        $(".contact-host-wrapper").find("#save_btn").prop("disabled", true);
                        $('#contact_host_success .contact-host-wrapper .success').remove();
                        $('#Contacthost').modal('hide');
                        $('#contact_host_success .contact-host-wrapper').prepend('<span class="success">Message sent successfully! Host will get back to you soon. </br> <a href=' + chatURL + '>Click here</a> to continue the chat. Thank You!</span>');
                        $('#contact_host_success').modal('show');

                        $('#text1').val('');
                    }
                } else {
                    $html = '<p class="error-class">' + msg.error + '</p>';
                    $('#Contacthost').find('.contact-host-wrapper').prepend($html);
                }
            }
        });
    });

    $('#flexible_dates').on('change', function () {
        this.value = this.checked ? 1 : 0;

        let flexibleDateText = '';
        if (this.value == '1') {
            flexibleDateText = 'Dates are flexible';
        } else if (this.value == '0') {
            flexibleDateText = 'Dates are exact';
        }

        $('.flexibleDates-text').html(flexibleDateText);

    }).change();

    $("body").delegate("#kt_datepicker_5", "focusin", function () {
        $(this).datepicker({
            startDate: new Date(),
            format: 'mm/dd/yyyy',
            timepicker: false,
            autoclose: true,
            todayHighlight: false,
        }).on('changeDate', function (e) {
            if (window.location.href.indexOf('?') == -1) {
                $("#kt_datepicker_6").datepicker("destroy");
                var date2 = $('#kt_datepicker_5').datepicker('getDate', '+1d');
                date2.setDate(date2.getDate() + 1);
                $('#kt_datepicker_6').datepicker('setStartDate', date2);
                date2.setDate(date2.getDate() + 2);
                $('#kt_datepicker_6').datepicker('setDate', date2);
            } else {
                var date2 = $('#kt_datepicker_5').datepicker('getDate', '+1d');
                date2.setDate(date2.getDate() + 1);
                $('#kt_datepicker_6').datepicker('setStartDate', date2);
                $('#kt_datepicker_6').datepicker('setDate', tDate);
                if (!moment($('#kt_datepicker_5').val()).isSame(fDate)) {
                    $("#kt_datepicker_6").datepicker("destroy");
                    var date2 = $('#kt_datepicker_5').datepicker('getDate', '+1d');
                    date2.setDate(date2.getDate() + 1);
                    $('#kt_datepicker_6').datepicker('setStartDate', date2);
                    date2.setDate(date2.getDate() + 2);
                    $('#kt_datepicker_6').datepicker('setDate', date2);
                }
            }
        });
    });
    $("body").delegate("#kt_datepicker_6", "focusin", function () {
        $(this).datepicker({
            autoApply: true,
            format: 'mm/dd/yyyy',
            startDate: new Date(),
            minDate: new Date(),
            autoclose: true,
        }).on('changeDate', function (start) {
            $('.offer-error-span').remove();
            $('.availabelDtsErrOffer').html('').hide();
            $('.availabelDtsErr').html('').hide();
            if (!moment($('#kt_datepicker_5').val()).isSame($('#kt_datepicker_6').val())) {
                $('#lets_go_tab').find('#lets_go_0').bind('click', true);

                var minDate = start.format();
                $(this).datepicker('hide');
                var form_date = new Date($('#kt_datepicker_5').val());
                var to_date = new Date($('#kt_datepicker_6').val());
                var oneDay = 24 * 60 * 60 * 1000;	// hours*minutes*seconds*milliseconds
                var diffDays = Math.round((to_date - form_date) / oneDay);
                $('.tripper_days').text(diffDays);
                $('.regular_days').text(diffDays);
                priceChange();
            } else {
                $('<span class="offer-error-span">You can not select same dates</span>').insertAfter('.chekin-checkout-dates');
                $('#lets_go_tab').find('#lets_go_0').bind('click', false);
                return false;
            }

        });


    });

    $('#makeoffer_form').on('submit', function (e) {
        e.preventDefault();
        if (!$(this).valid()) {
            return false;
        }

        $('.availabelDtsErr').html('').hide();
        $('.availabelDtsErrOffer').html('').hide();
        $("#hiddenMakeAnOfferNumberOfGuests").val($("#guests").val());

        let propertyId = $("#hiddenMakeAnOfferPropertyId").val();
        let ownerId = $("#hiddenMakeAnOfferOwnerId").val();
        let tripperId = $("#hiddenMakeAnOfferTripperId").val();

        let newMakeAnPropertyOfferURL = makeAnPropertyOfferURL;
        newMakeAnPropertyOfferURL = newMakeAnPropertyOfferURL.replace('#propertyId', propertyId);
        newMakeAnPropertyOfferURL = newMakeAnPropertyOfferURL.replace('#tripperId', tripperId);
        showLoader();
        $.ajax({
            type: "POST",
            url: newMakeAnPropertyOfferURL,
            dataType: "JSON",
            headers: {
                "Authorization": "Bearer {{session()->get('rental_token')}}",
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (msg) {
                hideLoader();
                if ($.isEmptyObject(msg.error)) {
                    //if (msg.success == true) {
                    $('.offer-popup-content').hide();
                    $('.offer-popup-success').show();
                    //}
                } else {
                    $('#makeoffer_form').find('.availabelDtsErrOffer').html(msg.error).show();
                    $(".alert-danger").show();
                    $(".alert-danger span").html('');
                    $.each(msg.error, function (key, value) {
                        $(".alert-danger span").html(value);
                    });
                }
            }
        });
    });

    $('#propertyBooking .guest-info').find('.plus').on('click', function () {
        let $qty = $('#propertyBooking .guest-info').find('#guests');
        let currentVal = parseInt($qty.val());

        let min = 1;
        let max = parseInt(guestLimit);
        let increment = 1;
        console.log('y');
        if (!isNaN(currentVal) && currentVal < max) {
            $qty.val(currentVal + increment);
        }

        let divMinus = $(this).closest('div').find('.minus');
        let divPlus = $(this);
        if ($qty.val() >= max) {
            divPlus.css({
                'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
            });
        } else if ($qty.val() < max) {
            divPlus.css({
                'cssText': ''
            });
        }

        if ($qty.val() <= min) {
            divMinus.css({
                'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
            });
        } else if ($qty.val() > min) {
            divMinus.css({
                'cssText': ''
            });
        }

    });

    $('#propertyBooking .guest-info').find('.minus').on('click', function () {

        let $qty = $('#propertyBooking .guest-info').find('#guests');
        let currentVal = parseInt($qty.val());
        let min = 1;
        let max = parseInt(guestLimit);
        let increment = 1;
        console.log('x');
        if (!isNaN(currentVal) && currentVal > min) {
            $qty.val(currentVal - increment);
        }

        let divPlus = $(this).closest('div').find('.plus');
        let divMinus = $(this);

        if ($qty.val() >= max) {
            divPlus.css({
                'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
            });
        } else if ($qty.val() < max) {
            divPlus.css({
                'cssText': ''
            });
        }

        if ($qty.val() <= min) {
            divMinus.css({
                'cssText': 'color: #ebedf7 !important;border-color: #ebedf7'
            });
        } else if ($qty.val() > min) {
            divMinus.css({
                'cssText': ''
            });
        }
    });


    $('#makeoffer_form').find('.plus').on('click', function () {
        let $qty = $(this).closest('div').find('.cmn-add');
        let currentVal = parseInt($qty.val());

        let min = parseInt(offerFlagPrice);
        let max = parseInt(propertyTripperPrice);
        let increment = 10;

        if (!isNaN(currentVal) && currentVal < max) {
            $qty.val(currentVal + increment);
        }

        /*let divMinus = $(this).closest('div').find('.minus');
        let divPlus = $(this);
        if ($qty.val() >= max) {
            divPlus.addClass('max');
        } else if ($qty.val() < max) {
            divPlus.removeClass('max');
        }

        if ($qty.val() <= min) {
            divMinus.addClass('min');
        } else if ($qty.val() > min) {
            divMinus.removeClass('min');
        }*/
        handleMakeAnOfferPlusMinusButton();
    });

    $('#makeoffer_form').find('.minus').on('click', function () {
        let $qty = $(this).closest('div').find('.cmn-add');
        let currentVal = parseInt($qty.val());
        let min = parseInt(offerFlagPrice);
        let max = parseInt(propertyTripperPrice);
        let increment = 10;

        if (!isNaN(currentVal) && currentVal > min) {
            $qty.val(currentVal - increment);
        }

        /*let divPlus = $(this).closest('div').find('.plus');
        let divMinus = $(this);

        if ($qty.val() >= max) {
            divPlus.addClass('max');
        } else if ($qty.val() < max) {
            divPlus.removeClass('max');
        }

        if ($qty.val() <= min) {
            divMinus.addClass('min');
        } else if ($qty.val() > min) {
            divMinus.removeClass('min');
        }*/
        handleMakeAnOfferPlusMinusButton();
    });

    function handleMakeAnOfferPlusMinusButton() {
        let $qty = $('#makeoffer_form').find("#tet123");
        let min = parseInt(offerFlagPrice);
        let max = parseInt(propertyTripperPrice);
        let divPlus = $('#makeoffer_form').find('.plus');
        let divMinus = $('#makeoffer_form').find('.minus');

        if ($qty.val() >= max) {
            divPlus.addClass('max');
        } else if ($qty.val() < max) {
            divPlus.removeClass('max');
        }
        if ($qty.val() <= min) {
            divMinus.addClass('min');
        } else if ($qty.val() > min) {
            divMinus.removeClass('min');
        }
        makeAnOfferFormValidate.element("#tet123");
    }

    function resetMakeAnOfferPlusMinusButton() {
        let makeofferForm = $('#makeoffer_form');
        makeofferForm.find('.plus').removeClass('max');
        makeofferForm.find('.minus').removeClass('min');
        makeofferForm.find('#tet123').removeClass('error');
    }

    $('#makeoffer_form').find('#tet123').focusout(function () {
        handleMakeAnOfferPlusMinusButton();
    })
});
var form = jQuery("#makeoffer_form");
var maxOfferPrice = parseInt(propertyTripperPrice - 10);
var makeAnOfferFormValidate = form.validate({
    rules: {
        tripperOfferNightlyPrice: {
            required: true,
            number: true,
            min: parseInt(offerFlagPrice),
            max: parseInt(maxOfferPrice),
            //range: [offerFlagPrice, maxOfferPrice],
            integer: true
        },
    },
    messages: {
        tripperOfferNightlyPrice: {
            required: "Please enter price.",
            number: "Please enter price.",
            min: "The owner is not accepting any offers below $" + offerFlagPrice,
            max: "The proposed total price cannot be greater than $" + maxOfferPrice,
            //range: "Price should be between $" + offerFlagPrice + " - $" + maxOfferPrice,
            integer: "Price should be in an integer only"
        },
    }
});





