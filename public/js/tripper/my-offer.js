$(document).ready(function () {

    let makeAnNewOfferPopup = $('#makeAnNewOffer');
    makeAnNewOfferPopup.find('#offerStartDate').datepicker({
        startDate: '+0d',
        dateFormat: 'mm-dd-yy'
    }).on("changeDate", function (e) {
        makeAnNewOfferPopup.find('#offerEndDate').val('');
        let dateEndDateSet = e.date;
        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
        makeAnNewOfferPopup.find('#offerEndDate').datepicker('setStartDate', dateEndDateSet);
    });

    makeAnNewOfferPopup.find("#offerEndDate").datepicker({
        startDate: '+0d',
        dateFormat: 'mm-dd-yy',
    }).on("changeDate", function (e) {
        let offerStartDate = makeAnNewOfferPopup.find('#offerStartDate').datepicker('getDate');
        let offerEndDate = makeAnNewOfferPopup.find('#offerEndDate').datepicker('getDate');

        if (offerStartDate && offerEndDate) {
            let offerStartDateMoment = moment.utc(offerStartDate);
            let offerEndDateMoment = moment.utc(offerEndDate);
            let totalNightsToStay = offerEndDateMoment.diff(offerStartDateMoment, 'days');

            makeAnNewOfferPopup.find("#numberOfNights").text(totalNightsToStay);
            let standardNightlyPrice = makeAnNewOfferPopup.find("#makeAnOfferStandardPrice").val();

            if (standardNightlyPrice) {
                let totalStandardNightsToStayPrice = totalNightsToStay * standardNightlyPrice;
                makeAnNewOfferPopup.find("#totalPriceWithDateRange").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightsToStayPrice)));
            }

            let propertyId = makeCounterOfferPopup.find('#makeAnOfferPropertyId').val();
            if (propertyId) {
                TripperOffer.validation.validateBookingSlotAvailable(makeAnNewOfferPopup, propertyId, offerStartDate, offerEndDate);
            }
        }

    });

    let $makeNewOfferFormValidate = $("#makeAnNewOfferForm").validate({
        rules: {
            offerStartDate: {
                required: true,
            },
            offerEndDate: {
                required: true
            },
            offeredProposedTotalPrice: {
                required: true,
                number: true,
                integer: true,
            },
        },
        messages: {
            offerStartDate: {
                required: "Start date is required"
            },
            offerEndDate: {
                required: "End date is required"
            },
            offeredProposedTotalPrice: {
                required: "Proposed total price cannot be blank.",
                integer: "Proposed total price should be in an integer only"
            }
        },
        errorPlacement: function (error, element) {
            makeAnNewOfferPopup.find('.error-label').removeClass('d-none');
            makeAnNewOfferPopup.find('.error-label').addClass('show');
            makeAnNewOfferPopup.find('.error-label').html(error).removeClass('hide');
        }
    });

    let makeCounterOfferPopup = $('#makeCounterOffer');
    makeCounterOfferPopup.find('#offerStartDate').datepicker({
        startDate: '+0d',
        dateFormat: 'mm-dd-yy',
        autoclose: true
    }).on("changeDate", function (e) {
        makeCounterOfferPopup.find('#offerEndDate').val('');
        let dateEndDateSet = e.date;
        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
        makeCounterOfferPopup.find('#offerEndDate').datepicker('setStartDate', dateEndDateSet);
    });

    makeCounterOfferPopup.find("#offerEndDate").datepicker({
        startDate: '+1d',
        dateFormat: 'mm-dd-yy',
        autoclose: true,
    }).on("changeDate", function (e) {
        let offerStartDate = makeCounterOfferPopup.find('#offerStartDate').datepicker('getDate');
        let offerEndDate = makeCounterOfferPopup.find('#offerEndDate').datepicker('getDate');

        let totalNightsToStay = 0;
        if (offerStartDate && offerEndDate) {
            totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);

            makeCounterOfferPopup.find("#numberOfNights").text(totalNightsToStay);
            let standardNightlyPrice = makeCounterOfferPopup.find("#makeAnOfferStandardPrice").val();

            if (standardNightlyPrice) {
                let totalStandardNightsToStayPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                makeCounterOfferPopup.find("#totalPriceWithDateRange").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightsToStayPrice)));
            }

            let propertyId = makeCounterOfferPopup.find('#makeAnOfferPropertyId').val();
            if (propertyId) {
                TripperOffer.validation.validateBookingSlotAvailable(makeCounterOfferPopup, propertyId, offerStartDate, offerEndDate);
            }
        }
    });

    let $makeACounterOfferFormValidate = $("#makeACounterOfferForm").validate({
        rules: {
            offerStartDate: {
                required: true,
            },
            offerEndDate: {
                required: true
            },
            offeredProposedTotalPrice: {
                required: true,
                number: true,
                integer: true,
            },
        },
        messages: {
            offerStartDate: {
                required: "Start date is required"
            },
            offerEndDate: {
                required: "End date is required"
            },
            offeredProposedTotalPrice: {
                required: "Proposed total price cannot be blank.",
                integer: "Proposed total price should be in an integer only"
            }
        },
        errorPlacement: function (error, element) {
            makeCounterOfferPopup.find('.error-label').html(error);
            makeCounterOfferPopup.find('.error-label').removeClass('d-none');
            makeCounterOfferPopup.find('.error-label').addClass('show');
        }
    });

    makeCounterOfferPopup.find('#offeredProposedTotalPrice').keyup(function () {
        //TripperOffer.validation.validateCounterOfferStandardOffer($(this).val());

        TripperOffer.validation.addRangeRulesToMakeAnOffer(makeCounterOfferPopup);
        makeCounterOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled');
        makeCounterOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');
        if (!$("#makeACounterOfferForm").valid()) {
            makeCounterOfferPopup.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
            makeCounterOfferPopup.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
        }

    });

    makeAnNewOfferPopup.find('#offeredProposedTotalPrice').keyup(function () {
        //TripperOffer.validation.validateMakeAnOfferStandardOffer($(this).val());

        TripperOffer.validation.addRangeRulesToMakeAnOffer(makeAnNewOfferPopup);
        makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled');
        makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');
        if (!$("#makeAnNewOfferForm").valid()) {
            makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
            makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
        }
    });

    makeCounterOfferPopup.find('#flexibleDates').click(function(){
        let flexibleDateText = '';
        let flagFlexibleDate = false;
        if($(this).prop("checked") == true){
            flexibleDateText = 'Dates are flexible';
            flagFlexibleDate = true;
        } else if($(this).prop("checked") == false){
            flexibleDateText = 'Dates are exact';
            flagFlexibleDate = false;
        }
        makeCounterOfferPopup.find('.flexibleDates-text').text(flexibleDateText);
        TripperOffer.validation.enableDisableDatePickerAtCounterOffer(flagFlexibleDate)
    });

    makeAnNewOfferPopup.find('#flexibleDates').click(function(){
        let flexibleDateText = '';
        if($(this).prop("checked") == true){
            flexibleDateText = 'Dates are flexible';
        } else if($(this).prop("checked") == false){
            flexibleDateText = 'Dates are exact';
        }
        makeAnNewOfferPopup.find('.flexibleDates-text').text(flexibleDateText);
    });


    TripperOffer = ({
        validation: {
            validateCounterOfferStandardOffer: function (proposedTotalPrice) {

                let makeCounterOffer = $("#makeCounterOffer");
                let $globalOwnerRangeStartDate = makeCounterOffer.find("#offerStartDate");
                let $globalOwnerRangeEndDate = makeCounterOffer.find("#offerEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                proposedTotalPrice = HelperFunction.roundNumber(proposedTotalPrice);

                let standardNightlyPrice = makeCounterOffer.find('#makeAnOfferStandardPrice').val();
                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                totalStandardNightlyPrice = HelperFunction.roundNumber(totalStandardNightlyPrice);

                makeCounterOffer.find('.error-label').text('').addClass('d-none');
                makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');
                makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled');
                if (proposedTotalPrice < 1) {
                    makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                    makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
                    makeCounterOffer.find('.error-label').text('Proposed total price cannot be less than 1').removeClass('d-none');
                } else if (proposedTotalPrice >= totalStandardNightlyPrice) {
                    makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                    makeCounterOffer.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
                    makeCounterOffer.find('.error-label').text('Proposed total price $(' + proposedTotalPrice + ') cannot be greater than Standard total price $(' + totalStandardNightlyPrice + ')').removeClass('d-none');

                }
            },
            validateMakeAnOfferStandardOffer: function (proposedTotalPrice) {

                let makeAnNewOfferPopup = $('#makeAnNewOffer');
                let $globalOwnerRangeStartDate = makeAnNewOfferPopup.find("#offerStartDate");
                let $globalOwnerRangeEndDate = makeAnNewOfferPopup.find("#offerEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                proposedTotalPrice = HelperFunction.roundNumber(proposedTotalPrice);

                let standardNightlyPrice = makeAnNewOfferPopup.find('#makeAnOfferStandardPrice').val();
                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                totalStandardNightlyPrice = HelperFunction.roundNumber(totalStandardNightlyPrice);

                makeAnNewOfferPopup.find('.error-label').text('').addClass('d-none');
                makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled');
                makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');
                if (proposedTotalPrice < 1) {
                    makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                    makeAnNewOfferPopup.find('.error-label').text('Proposed total price cannot be less than 1').removeClass('d-none');
                    makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
                } else if (proposedTotalPrice >= totalStandardNightlyPrice) {
                    makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                    makeAnNewOfferPopup.find('.error-label').text('Proposed total price $(' + proposedTotalPrice + ') cannot be greater than Standard total price $(' + totalStandardNightlyPrice + ')').removeClass('d-none');
                    makeAnNewOfferPopup.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
                }
            },
            validateBookingSlotAvailable: function (popUpObj, propertyId, checkInDate, checkOutDate) {

                showLoader();
                popUpObj.find('.error-label').text('').addClass('d-none');
                popUpObj.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled');
                popUpObj.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');

                let propertyAvailabilityCheckURL = getPropertyAvailabilityCheckURL;
                checkInDate = moment.utc(checkInDate);
                checkOutDate = moment.utc(checkOutDate);
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#propertyId', propertyId);
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#checkInDate', checkInDate.format('Y-M-D'));
                propertyAvailabilityCheckURL = propertyAvailabilityCheckURL.replace('#checkOutDate', checkOutDate.format('Y-M-D'));

                TripperOffer.validation.addRangeRulesToMakeAnOffer(popUpObj);

                $.ajax({
                    type: "GET",
                    url: propertyAvailabilityCheckURL,
                    dataType: "JSON",
                    success: function (msg) {
                        if (!msg.success) {
                            popUpObj.find('#makeAndOfferModelSubmitOfferButton').attr('disabled', true);
                            popUpObj.find('#makeAndOfferModelSubmitOfferButton').addClass('button-disabled');
                            popUpObj.find('.error-label').text('The dates you selected are not available.').removeClass('d-none');
                        }
                        hideLoader();
                    }
                });
            },
            getStandardPriceWhileCounterOfferSubmit: function (globalMakeOfferObj) {
                let $globalOwnerRangeStartDate = globalMakeOfferObj.find("#offerStartDate");
                let $globalOwnerRangeEndDate = globalMakeOfferObj.find("#offerEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                let standardNightlyPrice = globalMakeOfferObj.find('#makeAnOfferStandardPrice').val();
                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                return HelperFunction.roundNumber(totalStandardNightlyPrice);
            },
            addRangeRulesToMakeAnOffer: function (elementObj) {

                elementObj.find('#offeredProposedTotalPrice').rules("remove", "range");

                let $globalOwnerRangeStartDate = elementObj.find("#offerStartDate");
                let $globalOwnerRangeEndDate = elementObj.find("#offerEndDate");

                let offerStartDate = moment.utc($globalOwnerRangeStartDate.datepicker('getDate'));
                let offerEndDate = moment.utc($globalOwnerRangeEndDate.datepicker('getDate'));

                let standardNightlyPrice = elementObj.find('#makeAnOfferStandardPrice').val();
                let minimumOfferedNightlyPrice = elementObj.find('#makeAnOfferPropertyMinimumOffer').val();
                let propertyActualNightlyPrice = elementObj.find('#makeAnOfferPropertyActualNightlyPrice').val();

                let totalStandardNightlyPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, standardNightlyPrice);
                let totalStandardMinimumPrice = 1;
                if (minimumOfferedNightlyPrice) {

                    if (parseInt(standardNightlyPrice) < parseInt(minimumOfferedNightlyPrice)) {
                        var diff = parseInt(propertyActualNightlyPrice) - parseInt(minimumOfferedNightlyPrice);
                        if (parseInt(standardNightlyPrice) < diff) {
                            minimumOfferedNightlyPrice = parseInt(standardNightlyPrice - 1);
                        } else {
                            minimumOfferedNightlyPrice = parseInt(standardNightlyPrice) - diff;
                        }
                    }

                    totalStandardMinimumPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, minimumOfferedNightlyPrice);

                }

                totalStandardNightlyPrice = HelperFunction.roundNumber(totalStandardNightlyPrice);
                totalStandardMinimumPrice = HelperFunction.roundNumber(totalStandardMinimumPrice);

                elementObj.find('#offeredProposedTotalPrice').rules("add", {
                    min: (totalStandardMinimumPrice),
                    max: (totalStandardNightlyPrice - 1),
                    messages: {
                        min: 'The owner is not accepting any offers below $' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardMinimumPrice)),
                        max: 'The proposed total price cannot be greater than $' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightlyPrice - 1) ),
                    }
                });
            },
            resetMakeAnOfferForm: function (elementValidateObj, elementObj) {
                elementValidateObj.resetForm();
                elementObj.find('form')[0].reset();
                elementObj.find('#makeAndOfferModelSubmitOfferButton').removeAttr('disabled', true);
                elementObj.find('#makeAndOfferModelSubmitOfferButton').removeClass('button-disabled');
                elementObj.find('.error-label').text('').addClass('d-none')
            },
            enableDisableDatePickerAtCounterOffer : function (flexibleDates) {

                let makeACounterOffer = $("#makeCounterOffer");

                /**
                 * flexibleDates must be boolean or integer (0 or 1)
                 */
                if (flexibleDates) {
                    makeACounterOffer.find(".datepicker-li").removeClass('disabled');
                    makeACounterOffer.find("#offerStartDate").attr('disabled', false);
                    makeACounterOffer.find("#offerEndDate").attr('disabled', false);
                } else {
                    makeACounterOffer.find(".datepicker-li").addClass('disabled');
                    makeACounterOffer.find("#offerStartDate").attr('disabled', true);
                    makeACounterOffer.find("#offerEndDate").attr('disabled', true);
                }


            }
        },
        offerCardTemplate: function (offerCard) {
            return `
                <div class="offer-card-wrapper">
                    <div class="offer-card">
                        <div class="offer-image">
                            <a href="`+ TripperOffer.getPropertyUrl(offerCard.property.id) + `"><img src="`+ TripperOffer.getFirstPropertyImage(offerCard.property.images) + `" alt=""></a>
                            <a href="javascript:void(0);" onclick="TripperOffer.openDeleteOfferPopup('`+ offerCard.ownerId + `','` + offerCard.rentalId + `','` + offerCard.propertyId + `','` + offerCard.id + `')" class="offer-deleteicon"><i class="got got-delete"></i></a>
                        </div>
                        <div class="offer-content">
                            <h2 class="common-fatfrank">`+ offerCard.property.title + `</h2>
                            <ul class="list-unstyled mb-10">
                                <li class="row">
                                    `+ TripperOffer.getRequestedOrOfferedDate(offerCard) + `
                                </li>
                                <li class="row">
                                    <div class="text-left"><span>Standard Rate</span></div>
                                    <div class="text-right"> <span>$`+ HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.property.tripperPrice)) + `/Night</span></div>
                                </li>
                                <li class="row">
                                    `+ TripperOffer.getWhomHasOffered(offerCard) + `
                                </li>
                            </ul>
                            `+ TripperOffer.getStatus(offerCard) + `
                            <div class="mt-6 mb-4 offer-action">
                                `+ TripperOffer.getGoForLabel(offerCard) + `
                            </div>
                            <div class=" mb-4 card-offer-footer">
                                <a href="#" class="common-link">View Negotiation</a>
                                `+ TripperOffer.getOfferButton(offerCard) + `
                           </div>
                        </div>
                    </div>
                </div>
            `;
        },
        getRequestedOrOfferedDate: function (offerCard) {

            let labelH4 = 'Exact Dates';
            if (offerCard.flexibleDates){
                labelH4 = 'Flexible Dates';
            }

            let tripperOfferStartDate = moment.utc(offerCard.tripperOfferStartDate);
            let tripperOfferEndDate = moment.utc(offerCard.tripperOfferEndDate);
            let ownerOfferStartDate = moment.utc(offerCard.ownerOfferStartDate);
            let ownerOfferEndDate = moment.utc(offerCard.ownerOfferEndDate);
            let mdyOfferShowStartDate = tripperOfferStartDate.format('MMM D');
            let mdyOfferShowEndDate = tripperOfferEndDate.format('MMM D');

            if (
                offerCard.flexibleDates &&
                offerCard.status === propertyOfferSettingData.status.counterOffer &&
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner
            ) {
                if(
                    tripperOfferStartDate.format('YYYY MM DD') !== ownerOfferStartDate.format('YYYY MM DD') ||
                    tripperOfferEndDate.format('YYYY MM DD') !== ownerOfferEndDate.format('YYYY MM DD')
                )
                {
                    mdyOfferShowStartDate = ownerOfferStartDate.format('MMM D');
                    mdyOfferShowEndDate = ownerOfferEndDate.format('MMM D');
                }
            }

            return `
                <div class="text-left">
                    <h4>`+ labelH4 + `</h4>
                </div>
                <div class="text-right">
                    <span>`+ mdyOfferShowStartDate + `</span>
                    <span>-</span>
                    <span>`+ mdyOfferShowEndDate + `</span>
                </div>
            `;


        },
        redirectToReviewPage: function (propertyOfferId) {
            let rentalRedirectReviewURL = getRedirectToReviewPageURL;
            rentalRedirectReviewURL = rentalRedirectReviewURL.replace('#propertyOfferId', propertyOfferId);
            // window.onbeforeunload = null;
            window.location.href = rentalRedirectReviewURL;
        },
        getFirstPropertyImage: function (propertyImage) {
            return propertyImage[0].photo;
        },
        getPropertyUrl: function (propertyId) {
            let propertyWebURL = getPropertyWebURL;
            propertyWebURL = propertyWebURL.replace('#propertyId', propertyId);
            return propertyWebURL;
        },
        getGoForLabel: function (offerCard) {

            if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental &&
                offerCard.status === propertyOfferSettingData.status.accepted
            ) {
                return `<a href="javascript:void(0)"  id="#" onClick="" class="w-100 button-default-animate button-default-custom button-disabled btn-purple-gradient">BOOKED FOR $` + HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.finalAcceptedNightlyPrice))  + `</a>`;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.system &&
                offerCard.status === propertyOfferSettingData.status.cancel
            ) {
                return `<a href="javascript:void(0)"  id="#" onClick="" class="w-100 button-default-animate button-default-custom button-disabled btn-purple-gradient">BOOKED FOR $` + HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCard.property.tripperPrice)) + `</a>`;
            }

            let offerCardPrice = 0;

            if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental &&
                (
                    offerCard.status === propertyOfferSettingData.status.pending ||
                    offerCard.status === propertyOfferSettingData.status.rejected
                )
            ) {
                offerCardPrice = offerCard.property.tripperPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner &&
                (
                    offerCard.status === propertyOfferSettingData.status.accepted
                )
            ) {
                offerCardPrice = offerCard.tripperOfferNightlyPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner &&
                (
                    offerCard.status === propertyOfferSettingData.status.counterOffer
                )
            ) {
                offerCardPrice = offerCard.ownerOfferNightlyPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner &&
                (
                    offerCard.status === propertyOfferSettingData.status.rejected
                )
            ) {
                offerCardPrice = offerCard.property.tripperPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.owner &&
                (
                    offerCard.status === propertyOfferSettingData.status.cancel
                )
            ) {
                offerCardPrice = offerCard.property.tripperPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental &&
                (
                    offerCard.status === propertyOfferSettingData.status.counterOffer
                )
            ) {
                offerCardPrice = offerCard.ownerOfferNightlyPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental &&
                offerCard.status === propertyOfferSettingData.status.cancel
            ) {
                offerCardPrice = offerCard.property.tripperPrice;
            } else if (
                offerCard.actionBy === propertyOfferSettingData.actionBy.system &&
                offerCard.status === propertyOfferSettingData.status.cancel
            ) {
                offerCardPrice = offerCard.property.tripperPrice;
            }

            return `<a href="javascript:void(0)" onclick="TripperOffer.redirectToReviewPage('` + offerCard.id + `')" id="#" class="w-100 button-default-animate button-default-custom btn-purple-gradient">GO FOR $` + HelperFunction.numberWithCommas(HelperFunction.roundNumber(offerCardPrice)) + `</a>`;

        },
        createOfferCard: function (offerCard) {

            const offTemp = TripperOffer.offerCardTemplate(offerCard);

            const OfferCardHtml = `
                <div id="offerId`+ offerCard.ownerId + `_` + offerCard.rentalId + `_` + offerCard.propertyId + `_` + offerCard.id +`" class="col-lg-12 col-xl-6">
                    `+ offTemp + `
                </div>
            `;

            $("#my-offer-body > .row").append(OfferCardHtml);
        },
        createOfferCards: function (offerCards) {
            let offerCardHtml = '';
            $.each(offerCards, function (key, offerCard) {
                //offerCardHtml += TripperOffer.createOfferCard(offerCard);
                TripperOffer.createOfferCard(offerCard);
            });
            //$("#my-offer-body .row").html(offerCardHtml);
        },
        updateOfferCard: function (offerCard) {
            const offTemp = TripperOffer.offerCardTemplate(offerCard);
            $("#offerId" + offerCard.ownerId + "_" + offerCard.rentalId + "_" + offerCard.propertyId + "_" + offerCard.id).html(offTemp);
        },
        deleteOfferCard: function (ownerId, rentalId, propertyId, propertyOfferId) {
            $("#offerId" + ownerId + "_" + rentalId + "_" + propertyId + "_" + propertyOfferId).remove();
        },
        getStatus: function (offerCard) {
            if (offerCard.status === propertyOfferSettingData.status.pending && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return `<p class="d-block mystay-status-text patience">` + 'Pending Owner\'s Response' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return `<p class="d-block mystay-status-text patience">` + 'Pending Owner\'s Response' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.accepted && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<p class="d-block mystay-status-text approved">` + 'Your Offer Was Accepted!' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.rejected && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<p class="d-block mystay-status-text rejected">` + 'Your Offer Was Declined' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {
                return `<p class="d-block mystay-status-text rejected">` + 'You Cancelled This Offer' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<p class="d-block mystay-status-text rejected">` + 'Your Offer Was Cancelled' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<p class="d-block mystay-status-text patience">` + 'Owner Is Waiting For Your Response' + `</p>`;
            } else if (offerCard.status === propertyOfferSettingData.status.accepted && offerCard.actionBy === propertyOfferSettingData.actionBy.rental) {

                if (offerCard.booking) {
                    if(
                        offerCard.booking.ownerStatus === 'auto_reject' ||
                        offerCard.booking.ownerStatus === 'rejected'
                    ) {
                        offerAcceptedStatusByTripper = 'Booking Request Declined'
                        return `<p class="d-block mystay-status-text rejected">` + offerAcceptedStatusByTripper + `</p>`;
                    } else if(offerCard.booking.ownerStatus === 'pending') {
                        offerAcceptedStatusByTripper = 'Booking Request Pending'
                        return `<p class="d-block mystay-status-text patience">` + offerAcceptedStatusByTripper + `</p>`;
                    } else if(
                        (
                            offerCard.booking.ownerStatus === 'approved' ||
                            offerCard.booking.ownerStatus === 'instant_booking'
                        ) &&
                        offerCard.booking.bookingType === 'vacation_rental'
                    ) {
                        offerAcceptedStatusByTripper = 'Pending Confirmation';
                        return `<p class="d-block mystay-status-text patience">` + offerAcceptedStatusByTripper + `</p>`;
                    } else if(offerCard.booking.ownerStatus === 'cancelled') {
                        offerAcceptedStatusByTripper = 'Booking Request Cancelled';
                        return `<p class="d-block mystay-status-text rejected">` + offerAcceptedStatusByTripper + `</p>`;
                    } else {
                        offerAcceptedStatusByTripper = 'Booked';
                        return `<p class="d-block mystay-status-text approved">` + offerAcceptedStatusByTripper + `</p>`;
                    }

                } else {
                    let offerAcceptedStatusByTripper = 'Booking Requested';
                    return `<p class="d-block mystay-status-text patience">` + offerAcceptedStatusByTripper + `</p>`;
                }

            } else if (offerCard.status === propertyOfferSettingData.status.cancel && offerCard.actionBy === propertyOfferSettingData.actionBy.system) {
                return `<p class="d-block mystay-status-text rejected">` + 'Booking Requested By Another Tripper' + `</p>`;
            }
            return '';
        },
        getWhomHasOffered: function (offerCard) {

            let whomHasOfferedLabel = 'You Offered';
            let offeredPrice = offerCard.tripperOfferNightlyPrice;

            if (offerCard.status === propertyOfferSettingData.status.counterOffer && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                whomHasOfferedLabel = 'Owner Offered';
                offeredPrice = offerCard.ownerOfferNightlyPrice;
            }

            return `
                    <div class="text-left"><span class="text-bold">`+ whomHasOfferedLabel + `</span></div>
                    <div class="text-right"> <span class="text-bold">$`+ HelperFunction.numberWithCommas(HelperFunction.roundNumber(offeredPrice)) + `/Night</span></div>
            `;
        },
        getOfferButton: function (offerCard) {

            if (offerCard.status === propertyOfferSettingData.status.rejected && offerCard.actionBy === propertyOfferSettingData.actionBy.owner) {
                return `<a href="javascript:void(0);" onclick="TripperOffer.openPopupMakeAnOffer('` + offerCard.id + `')"
                            class="button-default-animate btn-add-payment-method"
                            data-toggle="modal" data-target="#MakenewOffer"
                          >Make New Offer</a>`;
            } else if (
                offerCard.status === propertyOfferSettingData.status.cancel /*&&
                offerCard.actionBy === propertyOfferSettingData.actionBy.rental*/
            ) {
                return `<a href="javascript:void(0);" onclick="TripperOffer.openPopupMakeAnOffer('` + offerCard.id + `')"
                            class="button-default-animate btn-add-payment-method"
                            data-toggle="modal" data-target="#MakenewOffer"
                          >Make New Offer</a>`;
            } else if (offerCard.status === propertyOfferSettingData.status.counterOffer
                && offerCard.actionBy === propertyOfferSettingData.actionBy.owner
            ) {
                return `<a href="javascript:void(0);" onclick="TripperOffer.openPopupMakeAnCounterOffer('` + offerCard.id + `')"
                            class="button-default-animate btn-add-payment-method"
                            data-toggle="modal" data-target="#MakenewCounterOffer"
                          >Make Counter Offer</a>`;
            } else if (
                offerCard.status === propertyOfferSettingData.status.accepted
                && offerCard.actionBy === propertyOfferSettingData.actionBy.owner
            ) {
                return `<a href="javascript:void(0);"
                            onclick="TripperOffer.openCancelAcceptedOfferPopup('`+ offerCard.ownerId + `','` + offerCard.rentalId + `','` + offerCard.propertyId + `','` + offerCard.id + `')"
                            class="btn button-default-animate btn-800 btn-round btn-outline-grey"
                        >Cancel</a>`
            }
            return '';
        },
        openPopupMakeAnOffer: function (propertyOfferId) {
            showLoader();
            let makeAnNewOffer = $("#makeAnNewOffer");

            TripperOffer.validation.resetMakeAnOfferForm($makeNewOfferFormValidate, makeAnNewOffer);

            makeAnNewOffer.find("#numberOfNights").text('');
            makeAnNewOffer.find("#standardNightlyPrice").text('');
            makeAnNewOffer.find("#totalPriceWithDateRange").text('');
            makeAnNewOffer.find("#offerStartDate").val('');
            makeAnNewOffer.find("#offerEndDate").val('');
            makeAnNewOffer.find("#offeredProposedTotalPrice").val('');

            makeAnNewOffer.find("#makeAnOfferOwnerId").val('');
            makeAnNewOffer.find("#makeAnOfferRentalId").val('');
            makeAnNewOffer.find("#makeAnOfferPropertyId").val('');
            makeAnNewOffer.find("#makeAnOfferPropertyOfferId").val('');
            makeAnNewOffer.find("#makeAnOfferPropertyOfferStartDate").val('');
            makeAnNewOffer.find("#makeAnOfferPropertyOfferEndDate").val('');


            let propertyOfferByIdURL = getPropertyOfferByIdURL;
            propertyOfferByIdURL = propertyOfferByIdURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "GET",
                url: propertyOfferByIdURL,
                dataType: "JSON",
                success: function (msg) {

                    if ($.isEmptyObject(msg.error)) {

                        let offerStartDate = moment.utc(msg.data.tripperOfferStartDate);
                        let offerEndDate = moment.utc(msg.data.tripperOfferEndDate);
                        let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);
                        let offeredNightlyPrice = msg.data.offeredNightlyPrice;
                        let totalOfferedNightsToStayPrice = totalNightsToStay * offeredNightlyPrice;
                        let standardNightlyPrice = msg.data.property.tripperPrice;
                        let totalStandardNightsToStayPrice = totalNightsToStay * standardNightlyPrice;
                        makeAnNewOffer.find("#standardNightlyPrice").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(standardNightlyPrice)));
                        makeAnNewOffer.find("#totalPriceWithDateRange").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightsToStayPrice)));

                        makeAnNewOffer.find("#makeAnOfferOwnerId").val(msg.data.ownerId);
                        makeAnNewOffer.find("#makeAnOfferRentalId").val(msg.data.rentalId);
                        makeAnNewOffer.find("#makeAnOfferPropertyId").val(msg.data.propertyId);
                        makeAnNewOffer.find("#makeAnOfferPropertyOfferId").val(msg.data.id);
                        makeAnNewOffer.find("#makeAnOfferStandardPrice").val(msg.data.property.tripperPrice);
                        makeAnNewOffer.find('#makeAnOfferPropertyMinimumOffer').val(msg.data.property.offerPrice);
                        makeAnNewOffer.find('#makeAnOfferPropertyActualNightlyPrice').val(msg.data.property.price);
                        makeAnNewOffer.find("#offerStartDate").datepicker("setDate", HelperFunction.convertToDateObject(offerStartDate));
                        makeAnNewOffer.find("#offerEndDate").datepicker("setDate", HelperFunction.convertToDateObject(offerEndDate));
                        let dateEndDateSet = HelperFunction.convertToDateObject(offerStartDate);
                        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
                        makeAnNewOffer.find("#offerEndDate").datepicker('setStartDate', dateEndDateSet);
                        makeAnNewOffer.find("#flexibleDates").prop("checked", msg.data.flexibleDates);

                        let flexibleDateText = 'Dates are exact';
                        if(msg.data.flexibleDates){
                            flexibleDateText = 'Dates are flexible';
                        }
                        makeAnNewOffer.find(".flexibleDates-text").text(flexibleDateText);

                        makeAnNewOffer.find("#makeAnOfferpropertyNumberOfGuests").val(msg.data.numberOfGuests);

                        hideLoader();
                        makeAnNewOffer.modal('show');

                    }
                }
            });

        },
        openPopupMakeAnCounterOffer: function (propertyOfferId) {
            showLoader();
            let makeACounterOffer = $("#makeCounterOffer");

            TripperOffer.validation.resetMakeAnOfferForm($makeACounterOfferFormValidate, makeACounterOffer);

            makeACounterOffer.find("#numberOfNights").text('');
            makeACounterOffer.find("#standardNightlyPrice").text('');
            makeACounterOffer.find("#totalPriceWithDateRange").text('');
            makeACounterOffer.find("#offerStartDate").val('');
            makeACounterOffer.find("#offerEndDate").val('');
            makeACounterOffer.find("#offeredProposedTotalPrice").val('');

            makeACounterOffer.find("#makeAnOfferOwnerId").val('');
            makeACounterOffer.find("#makeAnOfferRentalId").val('');
            makeACounterOffer.find("#makeAnOfferPropertyId").val('');
            makeACounterOffer.find("#makeAnOfferPropertyOfferId").val('');
            makeACounterOffer.find("#makeAnOfferPropertyOfferStartDate").val('');
            makeACounterOffer.find("#makeAnOfferPropertyOfferEndDate").val('');

            let propertyOfferByIdURL = getPropertyOfferByIdURL;
            propertyOfferByIdURL = propertyOfferByIdURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "GET",
                url: propertyOfferByIdURL,
                dataType: "JSON",
                success: function (msg) {

                    if ($.isEmptyObject(msg.error)) {

                        let offerStartDate = moment.utc(msg.data.tripperOfferStartDate);
                        let offerEndDate = moment.utc(msg.data.tripperOfferEndDate);
                        if (
                            msg.data.flexibleDates && msg.data.ownerOfferStartDate && msg.data.ownerOfferEndDate
                        ) {
                            offerStartDate = moment.utc(msg.data.ownerOfferStartDate);
                            offerEndDate = moment.utc(msg.data.ownerOfferEndDate);
                        }

                        let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);
                        let ownerOfferedNightlyPrice = msg.data.ownerOfferNightlyPrice;
                        let totalStandardNightsToStayPrice = HelperFunction.calculateTotalPrice(offerStartDate, offerEndDate, ownerOfferedNightlyPrice);
                        makeACounterOffer.find("#numberOfNights").text(totalNightsToStay);
                        makeACounterOffer.find("#standardNightlyPrice").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(ownerOfferedNightlyPrice)));
                        makeACounterOffer.find("#totalPriceWithDateRange").text('$' + HelperFunction.numberWithCommas(HelperFunction.roundNumber(totalStandardNightsToStayPrice)));
                        makeACounterOffer.find("#offerStartDate").datepicker("setDate", HelperFunction.convertToDateObject(offerStartDate));
                        makeACounterOffer.find("#offerEndDate").datepicker("setDate", HelperFunction.convertToDateObject(offerEndDate));
                        let dateEndDateSet = HelperFunction.convertToDateObject(offerStartDate);
                        dateEndDateSet.setDate(dateEndDateSet.getDate() + 1);
                        makeACounterOffer.find("#offerEndDate").datepicker('setStartDate', dateEndDateSet);
                        //makeACounterOffer.find("#flexibleDates").prop("checked",true);
                        makeACounterOffer.find("#flexibleDates").prop("checked", msg.data.flexibleDates);

                        TripperOffer.validation.enableDisableDatePickerAtCounterOffer(msg.data.flexibleDates);

                        let flexibleDateText = 'Dates are exact';
                        if(msg.data.flexibleDates){
                            flexibleDateText = 'Dates are flexible';
                        }
                        makeACounterOffer.find(".flexibleDates-text").text(flexibleDateText);

                        makeACounterOffer.find("#makeAnOfferOwnerId").val(msg.data.ownerId);
                        makeACounterOffer.find("#makeAnOfferRentalId").val(msg.data.rentalId);
                        makeACounterOffer.find("#makeAnOfferPropertyId").val(msg.data.propertyId);
                        makeACounterOffer.find("#makeAnOfferPropertyOfferId").val(msg.data.id);
                        makeACounterOffer.find("#makeAnOfferPropertyOfferStartDate").val(offerStartDate);
                        makeACounterOffer.find("#makeAnOfferPropertyOfferEndDate").val(offerEndDate);
                        //makeACounterOffer.find("#makeAnOfferStandardPrice").val(msg.data.property.tripperPrice);
                        makeACounterOffer.find("#makeAnOfferStandardPrice").val(ownerOfferedNightlyPrice);
                        makeACounterOffer.find('#makeAnOfferPropertyMinimumOffer').val(msg.data.property.offerPrice);
                        makeACounterOffer.find('#makeAnOfferPropertyActualNightlyPrice').val(msg.data.property.price);
                        hideLoader();
                        makeACounterOffer.modal('show');

                    }
                }
            });
        },
        openDeleteOfferPopup: function (ownerId, rentalId, propertyId, propertyOfferId) {
            let deleteAcceptance = $("#deleteOfferPopup");
            deleteAcceptance.find('#deleteOfferPopupOwnerId').val(ownerId);
            deleteAcceptance.find('#deleteOfferRentalId').val(rentalId);
            deleteAcceptance.find('#deleteOfferPropertyId').val(propertyId);
            deleteAcceptance.find('#deleteOfferPopupPropertyOfferId').val(propertyOfferId);
            deleteAcceptance.modal('show');
        },
        openCancelAcceptedOfferPopup: function (ownerId, rentalId, propertyId, propertyOfferId) {
            let deleteAcceptance = $("#cancelAcceptedOfferPopup");
            deleteAcceptance.find('#cancelAcceptedOfferPopupOwnerId').val(ownerId);
            deleteAcceptance.find('#cancelAcceptedOfferRentalId').val(rentalId);
            deleteAcceptance.find('#cancelAcceptedOfferPropertyId').val(propertyId);
            deleteAcceptance.find('#cancelAcceptedOfferPopupPropertyOfferId').val(propertyOfferId);
            deleteAcceptance.modal('show');
        },
        acceptOffer: function (ownerId, propertyId, rentalId, propertyOfferId) {

            showLoader();
            let ownerAcceptPropertyOfferURL = getOwnerAcceptPropertyOfferURL;
            ownerAcceptPropertyOfferURL = ownerAcceptPropertyOfferURL.replace('#propertyId', propertyId);
            ownerAcceptPropertyOfferURL = ownerAcceptPropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: ownerAcceptPropertyOfferURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        TripperOffer.createOfferCards(msg.data);
                    }
                }
            });
        },
        cancelOffer: function () {

            showLoader();

            let cancelAcceptedOfferPopup = $("#cancelAcceptedOfferPopup");
            let ownerId = cancelAcceptedOfferPopup.find('#cancelAcceptedOfferPopupOwnerId').val();
            let rentalId = cancelAcceptedOfferPopup.find('#cancelAcceptedOfferRentalId').val();
            let propertyId = cancelAcceptedOfferPopup.find('#cancelAcceptedOfferPropertyId').val();
            let propertyOfferId = cancelAcceptedOfferPopup.find('#cancelAcceptedOfferPopupPropertyOfferId').val();

            let rentalCancelPropertyOfferURL = getRentalCancelPropertyOfferURL;
            rentalCancelPropertyOfferURL = rentalCancelPropertyOfferURL.replace('#propertyId', propertyId);
            rentalCancelPropertyOfferURL = rentalCancelPropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: rentalCancelPropertyOfferURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        TripperOffer.updateOfferCard(msg.data);
                    }
                    cancelAcceptedOfferPopup.modal('hide');
                }
            });
        },
        deleteOffer: function () {

            showLoader();
            let deleteOfferPopup = $("#deleteOfferPopup");
            let ownerId = deleteOfferPopup.find('#deleteOfferPopupOwnerId').val();
            let rentalId = deleteOfferPopup.find('#deleteOfferRentalId').val();
            let propertyId = deleteOfferPopup.find('#deleteOfferPropertyId').val();
            let propertyOfferId = deleteOfferPopup.find('#deleteOfferPopupPropertyOfferId').val();

            let rentalDeletePropertyOfferURL = getRentalDeletePropertyOfferURL;
            rentalDeletePropertyOfferURL = rentalDeletePropertyOfferURL.replace('#propertyId', propertyId);

            $.ajax({
                type: "DELETE",
                url: rentalDeletePropertyOfferURL,
                dataType: "JSON",
                data: {
                    'ownerId': ownerId,
                    'id': propertyOfferId
                },
                success: function (msg) {
                    hideLoader();
                    deleteOfferPopup.modal('hide');
                    if ($.isEmptyObject(msg.error)) {
                        TripperOffer.deleteOfferCard(ownerId, rentalId, propertyId, propertyOfferId);
                        let offerCount = parseInt(TripperOffer.getNumberOfOffers());
                        offerCount--;
                        TripperOffer.setNumberOfOffers(offerCount);
                    }
                }
            });
        },
        createCounterOffer: function () {

            let makeACounterOffer = $("#makeCounterOffer");
            let makeACounterOfferForm = makeACounterOffer.find('form');

            TripperOffer.validation.addRangeRulesToMakeAnOffer(makeACounterOffer);

            if (!makeACounterOfferForm.valid()) {
                return false;
            }

            let ownerId = makeACounterOffer.find('#makeAnOfferOwnerId').val();
            let rentalId = makeACounterOffer.find('#makeAnOfferRentalId').val();
            let propertyId = makeACounterOffer.find('#makeAnOfferPropertyId').val();
            let propertyOfferId = makeACounterOffer.find('#makeAnOfferPropertyOfferId').val();
            let propertyOfferStartDate = makeACounterOffer.find('#offerStartDate').val();
            let propertyOfferEndDate = makeACounterOffer.find('#offerEndDate').val();
            let proposedTotal = makeACounterOffer.find('#offeredProposedTotalPrice').val();
            let isDateFlexible = makeACounterOffer.find('#flexibleDates').is(":checked");
            let timezone = makeACounterOffer.find('#timezone').val();

            let localCounterOfferPropertyURL = counterOfferPropertyURL;
            localCounterOfferPropertyURL = localCounterOfferPropertyURL.replace('#propertyId', propertyId);
            localCounterOfferPropertyURL = localCounterOfferPropertyURL.replace('#propertyOfferId', propertyOfferId);

            if (!proposedTotal) {
                makeACounterOffer.find('.error-label').text('Proposed total price cannot be blank').removeClass('d-none');
                return;
            }

            showLoader();

            $.ajax({
                type: "POST",
                url: localCounterOfferPropertyURL,
                dataType: "JSON",
                data: {
                    'ownerId': ownerId,
                    'tripperOfferStartDate': propertyOfferStartDate,
                    'tripperOfferEndDate': propertyOfferEndDate,
                    'tripperOfferProposedTotal': proposedTotal,
                    'flexibleDates': isDateFlexible,
                    'timezone': timezone
                },
                success: function (msg) {
                    if (msg.success == 'false' || msg.success === false) {
                        makeACounterOffer.find('.error-label').text(msg.message).removeClass('d-none')
                    } else {
                        TripperOffer.updateOfferCard(msg.data);
                        makeACounterOffer.modal('hide');
                    }
                    hideLoader();
                }
            });

        },
        createMakeAnOffer: function () {

            let makeOffer = $("#makeAnNewOffer");
            let makeOfferForm = makeOffer.find('form');

            let ownerId = makeOffer.find('#makeAnOfferOwnerId').val();
            let rentalId = makeOffer.find('#makeAnOfferRentalId').val();
            let propertyId = makeOffer.find('#makeAnOfferPropertyId').val();
            let propertyOfferId = makeOffer.find('#makeAnOfferPropertyOfferId').val();
            let propertyOfferStartDate = makeOffer.find('#makeAnOfferPropertyOfferStartDate').val();
            let propertyOfferEndDate = makeOffer.find('#makeAnOfferPropertyOfferEndDate').val();
            let proposedTotal = makeOffer.find('#offeredProposedTotalPrice').val();
            let offerStartDate = makeOffer.find('#offerStartDate').datepicker('getDate');
            let offerEndDate = makeOffer.find('#offerEndDate').datepicker('getDate');
            let isDateFlexible = makeOffer.find('#flexibleDates').is(":checked");
            let numberOfGuests = makeOffer.find('#makeAnOfferpropertyNumberOfGuests').val();

            let timezone = makeOffer.find('#timezone').val();

            TripperOffer.validation.addRangeRulesToMakeAnOffer(makeOffer);

            if (!makeOfferForm.valid()) {
                return false;
            }

            showLoader();

            let localNewMakeAnPropertyOfferURL = makeAnPropertyOfferURL;
            localNewMakeAnPropertyOfferURL = localNewMakeAnPropertyOfferURL.replace('#propertyId', propertyId);
            localNewMakeAnPropertyOfferURL = localNewMakeAnPropertyOfferURL.replace('#propertyOfferId', propertyOfferId);

            $.ajax({
                type: "POST",
                url: localNewMakeAnPropertyOfferURL,
                dataType: "JSON",
                data: {
                    'ownerId': ownerId,
                    'tripperOfferStartDate': moment.utc(offerStartDate).format('D-M-Y'),
                    'tripperOfferEndDate': moment.utc(offerEndDate).format('D-M-Y'),
                    'tripperOfferProposedTotal': proposedTotal,
                    'flexibleDates': isDateFlexible,
                    'numberOfGuests': numberOfGuests,
                    'timezone': timezone,
                    'id': propertyOfferId
                },
                success: function (msg) {
                    if (msg.success == 'false' || msg.success === false) {
                        makeOffer.find('.error-label').text(msg.message).removeClass('d-none')
                    } else {
                        TripperOffer.updateOfferCard(msg.data);
                        makeOffer.modal('hide');
                    }
                    hideLoader();
                }
            });

        },
        getOffers: function () {

            showLoader();

            $.ajax({
                type: "GET",
                url: getRentalPropertyOffersURL,
                dataType: "JSON",
                success: function (msg) {
                    hideLoader();
                    if ($.isEmptyObject(msg.error)) {
                        TripperOffer.createOfferCards(msg.data);
                        TripperOffer.setNumberOfOffers(msg.data.length);
                    }
                }
            });
        },
        setNumberOfOffers: function (count) {
            $("#offerTotalCount").val(count);
            TripperOffer.displayNumberOfOffers();
        },
        getNumberOfOffers: function () {
            return $("#offerTotalCount").val();
        },
        displayNumberOfOffers: function () {
            let count = TripperOffer.getNumberOfOffers();
            let offerText = '';
            if (count && count > 1) {
                offerText = count + ' Offers';
            } else if (count == '1') {
                offerText = count + ' Offer';
            } else {
                offerText = '0 Offers';
            }
            $(".totalOfferCount").html(offerText);
        }
    });
    TripperOffer.getOffers();

    pusherChannel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
        if(data.type === PusherEventNotificationTypeConstant.propertyOffer.owner.counteredOffer){
            TripperOffer.updateOfferCard(data.propertyOffer);
        } else if(data.type === PusherEventNotificationTypeConstant.propertyOffer.owner.accepted){
            TripperOffer.updateOfferCard(data.propertyOffer);
        } else if(data.type === PusherEventNotificationTypeConstant.propertyOffer.owner.rejected){
            TripperOffer.updateOfferCard(data.propertyOffer);
        } else if(data.type === PusherEventNotificationTypeConstant.propertyOffer.owner.cancelled){
            TripperOffer.updateOfferCard(data.propertyOffer);
        }
    });

});
