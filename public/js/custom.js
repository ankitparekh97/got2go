/*
 *  Customized Jquery for Site.
 */


var currentTab = 0; // Current tab is set to be the first tab (0)
var isValid = true;
var oldProgressTab = 0;
var isPreview = false;
function showTab(n, isview) {

	var form = jQuery("#owner-panel-form");

	// This function will display the specified tab of the form...
	var x = document.getElementsByClassName("outer-table");
	var progressBar = parseInt(oldProgressTab);

	jQuery('.nav.nav-tabs.custom-tab').removeClass('counter' + progressBar);
	x[currentTab].style.display = "none"
	x[n].style.display = "block";


	//... and fix the Previous/Next buttons:
	currentTab = n;
    $('#last_tab_id').val(currentTab);

	if (n == 0) {
		document.getElementById("prevBtn").style.display = "none";
	} else {

		var progressBar = parseInt(currentTab) + parseInt(1);

		jQuery('.nav.nav-tabs.custom-tab').addClass('counter' + progressBar);
		document.getElementById("prevBtn").style.display = "inline";
	}
	if (n == (x.length - 1)) {
		isPreview = true;
		renderReviewPage()
		document.getElementById("nextBtn").innerHTML = "Submit";

	} else if (isPreview) {

		document.getElementById("nextBtn").innerHTML = "Save & Review";
	}
	else {

		document.getElementById("nextBtn").innerHTML = "Save & Continue";
	}
	oldProgressTab = progressBar;
    if (oldProgressTab > 3) {
        jQuery('ul#ListingTab li:nth-child(1)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
    }
    if (oldProgressTab > 8) {
        jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
    }
    if (oldProgressTab > 9) {
        jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
    }
    if (oldProgressTab > 11) {
        jQuery('ul#ListingTab li:nth-child(4)').addClass('success');
        jQuery('ul#ListingTab li:nth-child(5) a').removeClass('disabled');
    }

	//... and run a function that will display the correct step indicator:

}
var count = 2;
function nextPrev(n) {
	var form = jQuery("#owner-panel-form");

	if (form.valid() === true) {
		var jQueryqty = jQuery(this).closest('div').find('.cmn-add');
		var currentVal = parseInt(jQueryqty.val());
		if (!isNaN(currentVal)) {
			jQueryqty.val(currentVal + 1);
		}

		// This function will figure out which tab to display
		var x = document.getElementsByClassName("outer-table");
		// Exit the function if any field in the current tab is invalid:

		// Hide the current tab:
		x[currentTab].style.display = "none";
		// Increase or decrease the current tab by 1:
		currentTab = currentTab + n;
		// if you have reached the end of the form...
		if (currentTab >= x.length) {
			// addProperty();
			// ... the form gets submitted:
			//document.getElementById("regForm").submit();
			return false;
		}
		// Otherwise, display the correct tab:
		showTab(currentTab, false);

		var count = currentTab;
		if (count > 3) {
			jQuery('ul#ListingTab li:nth-child(1)').addClass('success');
			jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
		}
		if (count > 8) {
			jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
			jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
		}
		if (count > 9) {
			jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
			jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
		}
		if (count > 11) {
			jQuery('ul#ListingTab li:nth-child(4)').addClass('success');
			jQuery('ul#ListingTab li:nth-child(5) a').removeClass('disabled');
		}
	}
}
function nextPrev1(n) {

	if (isPreview) {
		var form = jQuery("#owner-panel-form");
		if (!form.valid()) {
			return false
		}
	}

	var jQueryqty = jQuery(this).closest('div').find('.cmn-add');
	var currentVal = parseInt(jQueryqty.val());
	if (!isNaN(currentVal) && currentVal > 0) {
		jQueryqty.val(currentVal - 1);
	}
	var count = currentTab;

	if (count > 4) {
		jQuery('ul#ListingTab li:nth-child(1)').addClass('success');
		jQuery('ul#ListingTab li:nth-child(2) a').removeClass('disabled');
	}
	if (count > 9) {
		jQuery('ul#ListingTab li:nth-child(2)').addClass('success');
		jQuery('ul#ListingTab li:nth-child(3) a').removeClass('disabled');
	}
	if (count > 10) {
		jQuery('ul#ListingTab li:nth-child(3)').addClass('success');
		jQuery('ul#ListingTab li:nth-child(4) a').removeClass('disabled');
	}
	if (count > 12) {
		jQuery('ul#ListingTab li:nth-child(4)').addClass('success');
		jQuery('ul#ListingTab li:nth-child(5) a').removeClass('disabled');
	}
	// This function will figure out which tab to display
	var x = document.getElementsByClassName("outer-table");
	// Exit the function if any field in the current tab is invalid:

	// Hide the current tab:
	x[currentTab].style.display = "none";

	// Increase or decrease the current tab by 1:
	currentTab = currentTab + n;
	// if you have reached the end of the form...
	if (currentTab >= x.length) {
		// ... the form gets submitted:
		//document.getElementById("regForm").submit();
		return false;
	}

	// Otherwise, display the correct tab:
	showTab(currentTab, false);
}
function isBetween1(dateFrom,dateTo,dateCheck)
{

	var from=moment(dateFrom).format('MM/DD/YYYY');
	var to=moment(dateTo).format('MM/DD/YYYY');
	var check = moment(dateCheck).format('MM/DD/YYYY');


	return moment(check).isBetween(from, to);

}

$('#name_your_listing').bind('input keyup change', function (e) {

	e.preventDefault()
	var tval = $(this).val(),
		tlength = tval.length,
		set = 50,
		remain = parseInt(set - tlength);
	$(this).siblings('p.charlength').text('(' + remain + ' characters remaining)');
	if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
		$('#name_your_listing').val((tval).substring(0, tlength))
	}
})



function renderReviewPage() {

	$('.name_your_listing_prev').text($('#name_your_listing').val())
	$('.describe_text').text($('#describe_listing').val())
	$('.about_space_txt').text($('#about_space').val())
	var type_of_property = '';
	var txt = 'Category: ';

	$('input[name="type_of_property"]:checked').each(function () {
		txt += (this.value == 'property') ? 'Private Residence' : 'Vacation Club';
		type_of_property = propertyType = this.value;
	});
	var propertyType = PropertyTypes[type_of_property];
	// property Type
	var p_type = $('#property_type').val();
	if (type_of_property == 'vacation_rental') {

		p_type = $('#vacation_club').val()
		$("#msg_available_property").show();
		txt += ', ' + PropertyTypes[type_of_property][p_type];
	} else {
		$("#msg_available_property").hide();
		txt += ', ' + PropertyTypes[type_of_property][p_type];
	}

	if ($('#subType').val() != '' && subPropertyType[p_type]) {
		txt += ', ' + subPropertyType[p_type][$('#subType').val()];
	}

	if (type_of_property == 'vacation_rental') {
		txt += ', ' + $('#txt-resort').val();
	}

	$('.propertyTypetxt').text(txt)

	// Rooms & Spaces
	var Beds = 0;
	$('input[name^="bedType"]').each(function () {
		Beds += parseInt(this.value);
	});
	Beds += parseInt($('#how_many_beds_there').val())
	var bedrooms = $('#how-many-bedrooms-there').val();
	var Bathrooms = $('#how_many_bathrooms_available').val()
	var room_space = 'Bedrooms: ' + bedrooms + '<br>Beds: ' + Beds + '<br>Bathrooms: ' + Bathrooms;
	$('.rooms_space').html(room_space);

	//Guest Accomodations
	$('#Accomodates').text($('#how-many-guests-allowed').val());

	$('input[name="guest_have"]:checked').each(function () {
		if (this.value == 0) {
			$('#guest_access').text('Entire Place');
		} else {
			$('#guest_access').text('Private Room');
		}

	});


	$('input[name="is_guest_only"]:checked').each(function () {
		if (this.value == 0) {
			$('#guest_only').text('Yes');
		} else {
			$('#guest_only').text('No');
		}

	});

	//Address
	$('#Address').text($('#where_is_listing').val() + ' ' + $('#zipcode').val())

	//Price & Fees
	$('#pricetxt').text($('#price').val());
    $('#cleaningFeetxt').text($('#cleaning_fee').val());

    $('input[name="is_negotiable"]:checked').each(function() {
        if(this.value == 0)
        {
            $('#is_negotiabletxt').text('No');
            $('#offerprice').hide();
        }else{
            $('#is_negotiabletxt').text('Yes');
            $('#offerPricetxt').empty().text($('#offer_price').val());
            $('#offerprice').show();
        }
    });

	$('input[name="is_instant_booking"]:checked').each(function () {
		if (this.value == 1) {
			$('#is_booking_alllowtxt').text('Yes');
		} else {
			$('#is_booking_alllowtxt').text('No');
		}

	});


	// $('input[name="is_partial_booking"]:checked').each(function () {
	// 	if (this.value == 1) {
	// 		$('#partial_bookingtxt').text('Yes');
	// 	} else {
	// 		$('#partial_bookingtxt').text('No');
	// 	}
	// });

	//Check In, Check Out & Availability
	$('#checkintxt').text($('#check-in-date').val() + ' ' + $('#sel-in-time').val())
	$('#checkouttxt').text($('#check-out-date').val() + ' ' + $('#sel-out-time').val());
    var availableDatetxt = ''; var allPastText = true;
    $('.pastDates').hide();
	$('input[name="property_available_when[]"]').each(function (index) {
		var i = parseInt(index);
		if ($('#frmdate' + i).val() != '' && $('#todate' + i).val() != '') {
            var currentDate = new Date();
            var toDate = new Date($('#todate' + i).val());
            let pastTxt = '';

            if(toDate < currentDate){
                pastTxt = '<span style="color:red;"> (Dates Passed)</span>';
            } else {
                allPastText = false;
            }

			if (index > 0)
				availableDatetxt += '<br>';
			availableDatetxt += $('#frmdate' + i).val() + ' - ' + $('#todate' + i).val() + pastTxt;
		}
	});

	var unavailableDatetxt = ''
	$('input[name="property_unavailable[]"]').each(function (index) {
		var i = parseInt(index) + 1;
		if ($('#frmunavaiblitydate' + i).val() != '' && $('#tounavaiblitydate' + i).val() != '') {
			if (index > 0)
				unavailableDatetxt += '<br>';
			unavailableDatetxt += $('#frmunavaiblitydate' + i).val() + ' - ' + $('#tounavaiblitydate' + i).val();
		}
	});

	if ($('input[name="property_available_when[]"]').val()) {
		$('#available').show();
        $('#allavailabletxt').html(availableDatetxt)

        if(allPastText){
            $('.pastDates').show();
        }

	} else {
		$('#available').hide();
		$('#allavailabletxt').html('')
	}

	if ($('input[name="property_unavailable[]"]').val()) {
		$('#unavailable').show();
		$('#allunavailabletxt').html(unavailableDatetxt)
	} else {
		$('#unavailable').hide();
		$('#allunavailabletxt').html('')
	}

	//Cancellationtxt
	$('input[name="cancellation_policy"]:checked').each(function () {
		$('#Cancellationtxt').text(this.value);
	});

	//Amenities
	var str = ''
	$('input[name="amenity_id[]"]:checked').each(function () {
		var parent = $(this).parent('.checkbox-btn');
		var svg = $('<div>').append(parent.find('.amenity').clone()).html();

		var name = parent.find('.amenity_name').text()
		str += '<div class="col-6">'
		str += '<label class="checkbox-btn">' + svg + ' ' + name + '</label>'
		str += '</div>'

	});

	$('#checkedAmenities').html(str)

	//Photos

	$('#propertyImages').html($('.photos').find('.uploaded').clone());
	$('#verificationDoc').html($('.proof_of_ownership').find('.uploaded').clone());
	$('#govId').html($('.gov_id').find('.uploaded').clone());



}

function overlayValidation(){

	var isValidM = true;
	$("input[name='property_available_when[]']").each(function(index){

		var from_date= $(this).val();
		var to_date= $('#todate'+index).val();
		var isValidA = true;

		if(index == 0)
		{
			$('#frmdate'+index).removeClass('overlayError');
			$('#frmdate'+index).removeClass('error');
			$('#todate'+index).removeClass('error');
			$('#todate'+index).removeClass('overlayError');
		}

		if(from_date !='' && to_date !='')
		{

			$("input[name='property_available_when[]']").each(function(index2){


				if(index2 != index && $(this).val() != '' && $('#todate'+index2).val() !=''){

					var check_date = $(this).val();
					var isValidB =  isBetween1(from_date,to_date,check_date);

					if(!isValidB){
						var check_date = $('#todate'+index2).val();
						isValidB  = isBetween1(from_date,to_date,check_date);

					}

					if(isValidB){
						isValidM = false
						$('#frmdate'+index2).addClass('error');
						$('#todate'+index2).addClass('error');
						//if(index == 0)
						{
							$('#frmdate'+index2).addClass('overlayError');
							$('#todate'+index2).addClass('overlayError');
							$('#frmdate'+index).addClass('overlayError');
							$('#todate'+index).addClass('overlayError');
						}
						$('#frmdate'+index).addClass('error');
						$('#todate'+index).addClass('error');

					}
				}
			});


		}



	});

	if(!isValidM){
		$('#availableError label').text('2 or more of your availability ranges are overlapping in dates.');
	}


	if( !isValidM){
		$('#availableError label').show();
	}


	return  isValidM
}
jQuery(function () {

	$.fn.datetimepicker.noConflict = function () {
		$.fn.datetimepicker = old;
		return this;
	};

	$.validator.addMethod("validateNewResort", function (value, element) {
		var resort_name = $(element).val().trim();
		var display = isMatching;

		if (resort_name.length <= 3) {
			$('#save-new-resort').hide();
			$('#empty-resorts').hide();
			$('#txt-resort').parents('.input-button').removeClass('green');
			$('#txt-resort').parents('.input-button').addClass('red');
			return false;
		}
		else if (newResort == '' && availableTags.indexOf(resort_name) == -1 && display === false) {
			$('#save-new-resort').show();
			$('#empty-resorts').show();
			$('#txt-resort').parents('.input-button').removeClass('green');
			$('#txt-resort').parents('.input-button').addClass('red');
			return false;
		}
		else if (newResort == '' && availableTags.indexOf(resort_name) == -1 && display) {
			$('#save-new-resort').hide();
			$('#empty-resorts').hide();
			$('#txt-resort').parents('.input-button').removeClass('green');
			$('#txt-resort').parents('.input-button').addClass('red');
			return false;
		} else if (newResort != '') {
			$('#save-new-resort').show();
			$('#empty-resorts').hide();
			$('#txt-resort').parents('.input-button').addClass('green');
			$('#txt-resort').parents('.input-button').removeClass('red');
		}
		else {
			$('#save-new-resort').hide();
			$('#empty-resorts').hide();
			$('#txt-resort').parents('.input-button').removeClass('green');
			$('#txt-resort').parents('.input-button').addClass('red');
		}
		return true;

	}, "Please enter in the name of the hotel.");

	$.validator.addMethod("dimention", function (value, element) {

		var isInvalid = true;
		for(var x in dataTransfer[element.id]){

			if(dataTransfer[element.id][x].valid == false){
				isInvalid = false;
			}
		}

		return isInvalid;

	}, 'Please upload an image with 1024x683 pixels dimension.');

	$.validator.addMethod("invalidfile", function (value, element) {

		var isInvalid = true;
		for(var x in dataTransfer[element.id]){

			if(dataTransfer[element.id][x].valid == false){
				isInvalid = false;
			}
		}

		return isInvalid;

	}, 'Please upload valid files. (Only jpg,jpeg,png,pdf) are allowed.');

	$.validator.addMethod("checkTotalFiles", function (value, element, param) {

		if (Object.size(dataTransfer[element.id]) < param) {
			if(element.id == 'photos'){
				$('h5.file-upload-desc').css('color', "#F80041");
			}

			return false;
		}

		if(element.id == 'photos'){
			$('h5.file-upload-desc').css('color', "#2E3347");
		}
		return true;
	}, '')

	$.validator.addMethod('filesize', function (value, element, param) {
		return  (Object.size(dataTransfer[element.id]) <= param)
	}, 'Maximum 6 files allowed.');

	$.validator.addMethod('matchAddress', function (value, element, param) {
		return $('#where_is_listing_clone').val().trim() === $(element).val().trim();

	}, 'Please select proper location.');

	$.validator.addMethod('checkOverlapAvailableDates', function (value, element, param) {

		var isValidM = overlayValidation();


		return  isValidM
	}, '');

	$.validator.addMethod('validateMinDays', function (value, element, param) {
		var isValidM = true;


		$("input[name='property_available_when[]']").each(function(index){
			var from_date=moment($(this).val());
			var to_date= moment($('#todate'+index).val());
			var diff = to_date.diff(from_date, 'days');   // =1

			var minDays = $("#min_night"+index);
			var span = $(minDays).next('label').next('div');

			if($("input[name='is_partial_booking_date"+index+"'][value='1']").is(':checked'))
			{

				if(parseInt(minDays.val()) > diff || parseInt(minDays.val()) <1 || minDays.val() == '')
				{
					isValidM = false;
					$(minDays).addClass('error');
					$(minDays).addClass('Minerror');
					if(diff > 0)
					$(span).find("label").text('Enter minimum day(s) between 1 and '+diff+'.')
					$(span).find("label").show();
					// $('#availableError label').text('Enter minimum day(s) between 1 and '+diff+'.');
				}
				else{
					$(span).find("label").text('')
					$(minDays).removeClass('Minerror');
					$(minDays).removeClass('error');
				}
			}else{
				$(span).find("label").text('')
				$(minDays).removeClass('Minerror');
				$(minDays).removeClass('error');
			}
		})
		return  isValidM
	}, '');


    $.validator.addMethod('validateAvailableDates', function (value, element, param) {

		var isValid = true;
        $("input[name='property_available_when[]']").each(function(index){

            if($(this).val() == '' )
            {
                $(this).addClass('error');
                isValid = false;

            }else{
                $(this).removeClass('error')
			}


        })

        $("input[name='property_available_when_to[]']").each(function(index){

            if($(this).val() == '')
            {
                $(this).addClass('error');
                isValid = false;

            }else{
                $(this).removeClass('error')
            }

		})




			$('#availableError label').text('Invalid Dates.');



        $('#availableError label').hide();
        if(!isValid){
            $('#availableError label').show();
		}
        return isValid;

    }, '');

    jQuery.validator.addMethod("dollarsscents", function(value, element) {

        return this.optional(element) || /^\d{0,7}(\.\d{0,2})?$/i.test(value);
    }, "You must include two decimal places");


    $.validator.addMethod("noDecimal", function(value, element) {
        return !(value % 1);
    }, "No decimal numbers");

    $.validator.addMethod("checkTime", function(value, element) {

        $valid = true;
        $checkInTime = $('#check-in-date').val();
        $checkInTimeAM = $('#sel-in-time').val();

        $checkOutTime = $('#check-out-date').val();
		$checkOutTimeAM = $('#sel-out-time').val();

		// same time
        if(($checkInTime == '11:00' && $checkInTimeAM == "AM") && ($checkOutTime == '11:00' && $checkOutTimeAM == "AM")){
            $valid = false;
        } else if(($checkInTime == '11:30' && $checkInTimeAM == "AM") && ($checkOutTime == '11:30' && $checkOutTimeAM == "AM")){
            $valid = false;
        } else if(($checkInTime == '12:00' && $checkInTimeAM == "PM") && ($checkOutTime == '12:00' && $checkOutTimeAM == "PM")){
            $valid = false;
        }

		// past check-in time
        if(($checkInTime == '11:00' && $checkInTimeAM == "AM") && ($checkOutTime == '11:30' && $checkOutTimeAM == "AM")){
            $valid = false;
        } else if(($checkInTime == '11:00' && $checkInTimeAM == "AM") && ($checkOutTime == '12:00' && $checkOutTimeAM == "PM")){
            $valid = false;
		}

		if(($checkInTime == '11:30' && $checkInTimeAM == "AM") && ($checkOutTime == '12:00' && $checkOutTimeAM == "PM")){
            $valid = false;
        }

        // check in
		if(($checkInTime == '1:00' || $checkInTime == "2:00" || $checkInTime == "3:00" ||
			$checkInTime == "4:00" || $checkInTime == "5:00" || $checkInTime == "6:00") && ($checkInTimeAM == "AM")){
                $valid = false;
		}

		if(($checkInTime == '12:00' || $checkInTime == '12:30' || $checkInTime == '1:30' || $checkInTime == "2:30" ||
			$checkInTime == "3:30" || $checkInTime == "4:30" || $checkInTime == "5:30") && ($checkInTimeAM == "AM")){
                $valid = false;
        }

        if(($checkInTime == '11:00' || $checkInTime == '11:30') && ($checkInTimeAM == "PM")){
                $valid = false;
		}

		// check out
		if(($checkOutTime == '9:00' || $checkOutTime == "10:00" || $checkOutTime == "11:00" ||
		    $checkOutTime == '9:30' || $checkOutTime == '10:30' || $checkOutTime == "11:30") && ($checkOutTimeAM == "PM")){
                $valid = false;
		}

        if($checkOutTime == '12:00' && $checkOutTimeAM == "AM"){
            $valid = false;
        }

        return $valid;

    }, "Please ensure that your check-in time is past your check-out time to ensure a smooth reservation process.");


    jQuery.validator.addMethod("totalBedRooms", function(value, element) {
        if(element.value == 0){
            return true;
        }

        var isValid = true;
        for(var x=1; x<=element.value; x++){
            var total = 0;
            $('#bedroom_'+x+' input[name^="bedType"]').each(function(){
                total = total + $(this).val();
            })

            if(total == 0){
                return false;
            }
        }
        return true;
        // return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
	}, "Please add at least one bed to each bedroom.");




	var form = jQuery("#owner-panel-form");
			form.validate({
                ignore:":not(:visible)",
				rules: {
					name_your_listing: {
						required: true,
						maxlength:50
					},
					describe_listing : {
						required: true,
						// maxlength:250,
						minlength: 100,
					},
					about_space : {
						required: true,
						// maxlength:250,
						minlength: 100,
					},
					where_is_listing:{
						required: true,
						matchAddress:true
					},
					private_residence_this:{
						required: true,
					},
					vacation_club_is_this:{
						required: true,
					},
					txt_resort:{
						required: true,
						validateNewResort: true,
					},
					what_kind_cabin:{
						required: true,
					},
					"how-many-guests-allowed":{
						required: true,
						min: 1
					},
					'how-many-bedrooms-there':{
                        required: true,
                        min: 1,
                        totalBedRooms:true
					},
					how_many_beds_there:{
						required: true,
					},
					how_many_bathrooms_available:{
                        required: true,
                        min: 1
					},
					guest_checkin_time:{
						required: true,
					},
					guest_checkin_time_format:{
						required: true,
                    },
                    guest_checkout_time:{
                        required: true,
                        checkTime: true
					},
					guest_checkout_time_format:{
						required: true,
					},
					listing:{
						required: true,
					},
					guest_have:{
						required: true,
					},
					is_guest_only:{
						required: true,
                    },
                    'amenity_id[]':{
                        required: true,
					},
					price:{
						required:true,
						number: true,
                        min:function() {
                            if($('.offerPrice').is(':visible')) {
                                return parseInt($('#offer_price').val());
                            }
                        },
                        max: 3000,
                        noDecimal: true
                    },
                    cleaning_fee:{
						required:true,
						number: true,
                        min:function() {
                            return 0;
						},
                        max: 10000,
                        dollarsscents: true
                    },
                    offer_price:{
						required:true,
						number: true,
                        min:1,
                        max: function() {
                            return parseInt($('#price').val() - 20);
                        },
                        noDecimal: true,
                    },
                    'property_available_when[]':{
						validateAvailableDates: true,
						checkOverlapAvailableDates:true,
						validateMinDays:true
					},
					'property_available_when_to[]':{
						checkOverlapAvailableDates:true,
						validateMinDays:true
					},
					"min_night[]":{
						validateMinDays:true
					},
                    cancellation_policy:{
                        required: true,
					},
					"photos[]": {
						extension: "jpg|jpeg|png",
						accept:"image/jpeg,image/jpg,image/png,video/mp4,video/x-m4v,video/*",
						checkTotalFiles:6,
                        dimention:true,
						filesize:15,
                     },
					 "proof_of_ownership[]":{
                        extension: "jpg|jpeg|png|pdf",
						accept: "image/jpeg,image/jpg,image/png,application/pdf",
						checkTotalFiles:1,
						invalidfile:true,
						filesize:6
					 },
					 "gov_id[]":{
						extension: "jpg|jpeg|png|pdf",
						accept: "image/jpeg,image/jpg,image/png,application/pdf",
						checkTotalFiles:1,
						invalidfile:true,
						filesize:6
					 },

				},
				messages: {
					"proof_of_ownership[]":{
						checkTotalFiles: "Please upload proof of ownership or lease document.",
						extension: "Please upload valid files. (Only jpg,jpeg,png,pdf) are allowed.",
						accept: "Please upload valid files. (Only jpg,jpeg,png,pdf) are allowed.",
					},
					"gov_id[]":{
						checkTotalFiles: "Please upload goverment issued ID.",
						extension: "Please upload valid files. (Only jpg,jpeg,png,pdf) are allowed.",
						accept: "Please upload valid files. (Only jpg,jpeg,png,pdf) are allowed.",
                    },
                    "photos[]":{
						accept: "Please upload valid files. (Only jpg,jpeg,png,MOV,MP4) are allowed.",
                        extension: "Please upload valid files. (Only jpg,jpeg,png,MOV,MP4) are allowed.",
						filesize:'Maximum 15 files allowed.',                    },
					name_your_listing: {
						required: "Please enter name of your listing.",
						max:"Property names cannot be longer than 50 characters."
                    },
                    describe_listing : {
						required: "Please describe your listing.",
						//max:"Property descriptions cannot be longer than 250 characters.",
						minlength: 'Please enter a minimum of 100 characters'
                    },
                    about_space : {
						required: "Please describe your surroundings",
						//max:'The \"About the Space\" section cannot be longer than 250 characters.',
						minlength: 'Please enter a minimum of 100 characters'
                    },
                    listing:{
                        required: "Please select type of listing.",
                    },
                    where_is_listing:{
                        required: "Please select your location.",
                    },
                    private_residence_this:{
                        required: "Please select type of property.",
                    },
                    vacation_club_is_this:{
                        required: "Please select type of vacation club.",
                    },
                    what_kind_cabin:{
                        required: "This field is required",
					},
					txt_resort:{
						required: "Please enter name.",
					},
                    "how-many-guests-allowed":{
                        required: "Please enter number of guest.",
                        min: "Please select atleast one guest."
                    },
                    'how-many-bedrooms-there':{
                        required: "This field is required",
                        min: "Please select at least one bedroom."
                    },
                    how_many_beds_there:{
                        required: "This field is required",
                    },
                    how_many_bathrooms_available:{
                        required: "This field is required",
                        min: "Please select atleast one bathroom."
                    },
                    guest_check_time:{
                        required: "This field is required",
                    },
                    guest_check_time_format:{
                        required: "This field is required",
                    },
                    guest_have:{
                        required: "Please select guest access type.",
                    },
                    is_guest_only:{
                        required: "This field is required",
                    },
                    'amenity_id[]':{
                        required: "Please select Amenities.",
					},
					price:{
						required: "Please enter price.",
						number: "Please enter price.",
                        min:  "Nightly price must be greater than minimum nightly rate.",
                        max: "Cannot list a stay for more than $3,000 per night",
                        noDecimal: "Nightly rate must be without a decimal",
                    },
                    cleaning_fee:{
						required: "Please enter cleaning fee.",
						number: "Please enter cleaning fee.",
                        min:  "Please enter proper cleaning fee.",
                        max: "Cleaning fee must be less than $10,000"
                    },
                    offer_price:{
						required: "Please enter minimum nightly price.",
                        number: "Please enter minimum nightly price.",
                        noDecimal: "Minimum nightly rate must be without a decimal",
                        min:  "Please enter proper minimum nightly price.",
                        max: jQuery.validator.format("Your minimum nightly rate cannot be greater than ${0}."),
					},
                    cancellation_policy:{
                        required: "This field is required",
                    },
                    video:{
                        accept: 'Plase upload valid video.'
                    }
                },
                errorPlacement: function(error, element) {
                    var listing = $('#listingErrorLabel');
                    var guest_have = $('#guest_haveErrorLabel');
                    var is_guest_only = $('#is_guest_onlyErrorLabel');
                    var amenityErrorLabel = $('#AmenityErrorLabel');
                    var cancellationPolicyErrorLabel = $('#cancellation_policyErrorLabel');
					var PrieceErrorLabel = $('#PrieceErrorLabel');
                    var PhotoError = $('#PhotoError');
                    var cleaningFeeErrorLabel = $('#cleaningFeeErrorLabel');
                    var offerPriceLabel = $('#offerPriceLabel');
                    var totalBedrErr = $('#totalBedrErr');
                    var checkTime = $('.check-time-error');

                    if (element.attr('name')=='listing') {
                        $(listing).append(error)
                    } else if (element.attr('name')=='guest_have') {
                        $(guest_have).append(error)
                    } else if (element.attr('name')=='is_guest_only') {
                        $(is_guest_only).append(error)
                    } else if (element.attr('name')=='amenity_id[]') {
                        $(amenityErrorLabel).append(error)
                    } else if (element.attr('name')=='price') {
                        $(PrieceErrorLabel).append(error)
                    } else if (element.attr('name')=='cancellation_policy') {
						$(cancellationPolicyErrorLabel).append(error)
					}else if (element.attr('name')=='photos[]') {
						if($(error).text()!='You need at least 6 photos.')
						$(PhotoError).append(error)
					}else if (element.attr('name')=='cleaning_fee') {
						$(cleaningFeeErrorLabel).append(error)
                    } else if (element.attr('name')=='offer_price') {
                        $("#owner-panel-form input[name='is_negotiable']").on('change',function(){
                            var is_negotiable = $(this).val();
                            if(is_negotiable == 0){
                                $('#offer_price').removeClass('error');
                                $(offerPriceLabel).html('')
                                $('.offerPrice').hide();
                            }
                        });
                        $(offerPriceLabel).append(error)

                    } else if (element.attr('name')=='property_available_when[]') {
                        if($(element).val()  != '' && !$(element).hasClass("overlayError"))
                        {
                            $(element).removeClass('error')
                        }
					}
					else if (element.attr('name')=='property_available_when_to[]') {
                        if($(element).val()  != '' && !$(element).hasClass("overlayError"))
                        {

                            $(element).removeClass('error')
                        }
                    }else if(element.attr('name')=="min_night[]"){
						if($(element).val()  != '' && !$(element).hasClass("Minerror"))
                        {

                            $(element).removeClass('error')
                        }
					}
                    else if (element.attr('name')=='guest_checkout_time') {
                        $(checkTime).append(error);
                    }
					 else  {
                        error.insertAfter(element);
                    }
                }
			});
	showTab(currentTab,false); // Display the current tab

    jQuery('#guest_check_in, #guest_checkout_date').datetimepicker({
        format: 'LT',
        icons: {
            up: "fa fa-chevron-circle-up",
            down: "fa fa-chevron-circle-down",
            next: 'fa fa-chevron-circle-right',
            previous: 'fa fa-chevron-circle-left'
        }
    });

});

(function (jQuery) {

	jQuery(document).ready(function () {
		var preloaded = Property_DB['propertyimages'];
		jQuery('.photos').imageUploader({
			imagesInputName: 'photos',
			isWidthCheck: true,
			Default: ['.jpg', '.jpeg', '.png', '.PNG'],
			maxSize: 2 * 1024 * 1024,
			maxFiles: 15,
			accept: "jpg|jpeg|png",
			preloaded: preloaded

		});
		$("#photos").attr('accept', 'image/jpeg,image/png,video/mp4,video/x-m4v,video/quicktime');




		jQuery('#photos').on('change', function () {


			var form = jQuery("#owner-panel-form");
			if (form.valid() === false) {
				//jQuery('#nextBtn').addClass('disabled')
			}
			else {
				jQuery('#nextBtn').removeClass('disabled')
			}
		})
		var preloaded = [];
		for(var x in Property_DB['propertyownershipdoc']){
			if(Property_DB['propertyownershipdoc'][x].is_govt_id == null)
			{
				Property_DB['propertyownershipdoc'][x]['photo'] = Property_DB['propertyownershipdoc'][x].document_name
				preloaded.push(Property_DB['propertyownershipdoc'][x])
			}
		}

		jQuery('.proof_of_ownership').imageUploader({
			imagesInputName: 'proof_of_ownership',
			Default: ['.jpg', '.jpeg', '.png', '.PNG', '.pdf'],
			maxSize: 2 * 1024 * 1024,
			maxFiles: 6,
			accept: "jpg|jpeg|png|pdf",
			preloaded: preloaded
		});
		$("#proof_of_ownership").attr('accept', 'image/jpeg,image/png,application/pdf');

		jQuery('#proof_of_ownership').on('change', function () {

			var form = jQuery("#owner-panel-form");

			if (form.valid() === false) {
				//jQuery('#nextBtn').addClass('disabled')
			}
			else {
				jQuery('#nextBtn').removeClass('disabled')
			}
		})
		var preloaded = [];
		for(var x in Property_DB['propertyownershipdoc']){
			if(Property_DB['propertyownershipdoc'][x].is_govt_id != null)
			{
				Property_DB['propertyownershipdoc'][x]['photo'] = Property_DB['propertyownershipdoc'][x].document_name
				preloaded.push(Property_DB['propertyownershipdoc'][x])
			}
		}
		jQuery('.gov_id').imageUploader({
			imagesInputName: 'gov_id',
			Default: ['.jpg', '.jpeg', '.png', '.PNG', '.pdf'],
			maxSize: 2 * 1024 * 1024,
			maxFiles: 6,
			accept: "jpg|jpeg|png|pdf",
			preloaded: preloaded
		});
		$("#gov_id").attr('accept', 'image/jpeg,image/png,application/pdf');



		jQuery(".header-user-info .responsive-menu").on("click", function () {
			jQuery('.responsive-menu-show').addClass('active');
			jQuery('.responsive-menu-layer').addClass('show');
		});

		jQuery(".responsive-menu-layer").on("click", function () {
			jQuery('.responsive-menu-show').removeClass('active');
			jQuery('.responsive-menu-layer').removeClass('show');
		});
		jQuery(".listing-footer-btns button#nextBtn").on("click", function () {

			jQuery(".center-container").mCustomScrollbar('scrollTo', 'top', {
				timeout: 0,
				scrollInertia: 0,
			});

		});

	});



})(jQuery);







