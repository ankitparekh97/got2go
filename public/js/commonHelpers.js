
const propertyOfferSettingData = {
    status: {
        pending: 'pending',
        counterOffer: 'counter_offer',
        accepted: 'accepted',
        rejected: 'rejected',
        cancel: 'cancel',
    },
    actionBy: {
        owner: 'owner',
        rental: 'rental',
        system: 'system'
    }
}

// Event Constant
const PusherEventNotificationTypeConstant = {
    propertyOffer: {
        tripper: {
            new: 'broadcast.propertyOffer.tripper.new',
            counterOffer: 'broadcast.propertyOffer.tripper.counteredOffer',
            deleted: 'broadcast.propertyOffer.tripper.deleted',
            newOnExisting: 'broadcast.propertyOffer.tripper.newOnExisting',
            accepted: 'broadcast.propertyOffer.tripper.accepted',
            cancelled: 'broadcast.propertyOffer.tripper.cancelled',
        },
        owner: {
            counteredOffer: 'broadcast.propertyOffer.owner.counteredOffer',
            accepted: 'broadcast.propertyOffer.owner.accepted',
            rejected: 'broadcast.propertyOffer.owner.rejected',
            cancelled: 'broadcast.propertyOffer.owner.cancelled',
        },
        alertBar: 'broadcast.propertyOffer.alertBar'
    }
};


HelperFunction = {
    numberWithCommas: function (num) {
        let fixedToLast2Digit = parseFloat(parseFloat((num)).toFixed(2));
        return fixedToLast2Digit.toLocaleString();

    },
    roundNumber: function (num) {
        return Math.round(parseFloat(num));
    },
    calculateTotalPrice: function (startDate, endDate, nightlyPrice) {
        let offerStartDate = moment.utc(startDate);
        let offerEndDate = moment.utc(endDate);
        let totalNightsToStay = HelperFunction.dateDiffInDays(offerStartDate, offerEndDate);
        if (totalNightsToStay > 0) {
            return (totalNightsToStay * nightlyPrice);
        }
        return nightlyPrice;
    },
    dateDiffInDays: function (startDate, endDate) {
        let offerStartDate = moment.utc(startDate).startOf('day');
        let offerEndDate = moment.utc(endDate).startOf('day');
        return offerEndDate.diff(offerStartDate, 'days', true);
    },
    convertToDateObject: function (momentUTCObject) {
        let year = momentUTCObject.format('YYYY');
        let month = momentUTCObject.format('MM');
        let date = momentUTCObject.format('DD');
        return new Date(year, month - 1, date);
    },
    getSingularOrPlural: function (count, strSingle, strPlural) {
        if (count > 1) {
            return strPlural;
        }
        return strSingle;
    },
    isValidString: function (str) {
        return !/[~`!@#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
    },
    getSubscriptionTransactionFee: function (value) {
        return HelperFunction.numberWithCommas(((value * 2.9) / 100) + 0.30);
    },
    getPayOutFee: function (value) {
        return Math.round((value * 0.25) / 100);
    },
    getOccupancyFee: function (value) {
        return Math.round(value * 0.1225);
    },
    getTransactionFee: function (value) {
        return Math.round(((value + 0.3) / (1 - 0.029)) - (value));
    },
    mask: {
        percentageMasking: function (val) {
            let tempVal = val.replace("%", "");
            console.log(tempVal);
            if (tempVal.length === 1) {
                return '0%';
            } else {
                return '00%';
            }
        },
    }
}

jQuery.validator.addMethod("validateUSPhoneNumber", function (value, element) {
    return this.optional(element) || /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/.test(value);
}, "Please enter valid phone number.");

jQuery.validator.addMethod("validateAlphaNumericWithoutUnderscore", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
}, "Alphanumerical characters only.");
