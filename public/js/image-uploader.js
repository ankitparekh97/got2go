/*! Image Uploader - v1.0.0 - 15/07/2019
 * Copyright (c) 2019 Christian Bayer; Licensed MIT */
let dataTransfer = {};
let deletedFiles= [];
var _URL = window.URL || window.webkitURL;
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
(function ($) {

    $.fn.imageUploader = function (options) {

        // Default settings
        let defaults = {
            preloaded: [],
            imagesInputName: 'images',
            preloadedInputName: 'preloaded',
            label: 'Drag & Drop files here or click to browse'
        };

        // Get instance
        let plugin = this;

        // Set empty settings
        plugin.settings = {};

        // Plugin constructor
        plugin.init = function () {

            // Define settings
            plugin.settings = $.extend(plugin.settings, defaults, options);

            // Run through the elements
            plugin.each(function (i, wrapper) {

                // Create the container
                let $container = createContainer();
                $($container).insertAfter($(wrapper).find('.inputUploader'));

                var insertAfter = $(wrapper).find('p.file-upload-desc');
                $uploadedContainer = $('<div>', { class: 'uploaded', id: plugin.settings.imagesInputName+"_uploaded" }).insertAfter($(insertAfter));


                // Set some bindings
                $container.on("dragover", fileDragHover.bind($container));
                $container.on("dragleave", fileDragHover.bind($container));
                $container.on("drop", fileSelectHandler.bind($container));

                // If there are preloaded images
                if (plugin.settings.preloaded.length) {



                    // Get the upload images container
                    let $uploadedContainer = $('#'+plugin.settings.imagesInputName+"_uploaded");;

                    // Set preloaded images preview
                    for (let i = 0; i < plugin.settings.preloaded.length; i++) {
                        var dataObj = {name: plugin.settings.preloaded[i].photo, size: plugin.settings.preloaded[i].photo, id: plugin.settings.preloaded[i].id, valid:true, uploaded:true};
                        dataTransfer[plugin.settings.imagesInputName][i] = dataObj;
                        var len = Object.size(dataTransfer);

                        if (plugin.settings.preloaded[i]['media_type'] == 'application/pdf') {
                            $uploadedContainer.append(createImg(mediaPublicPath+'/images/pdf-icon.svg', i, true, false));
                        } else if (plugin.settings.preloaded[i]['media_type'] == 'image/jpeg' || plugin.settings.preloaded[i]['media_type'] == 'image/jpg' || plugin.settings.preloaded[i]['media_type'] == 'image/png') {
                            // Set preview
                            $uploadedContainer.append(createImg(mediaPath+'/'+plugin.settings.preloaded[i].photo, i, true, false));


                        } else if (plugin.settings.preloaded[i]['media_type'] == 'video/mp4' || plugin.settings.preloaded[i]['media_type'] == 'video/quicktime') {
                            // Set preview
                            $uploadedContainer.append(createImg(mediaPath+'/'+plugin.settings.preloaded[i].photo, i, true, true));
                        } else {
                            $uploadedContainer.append(createImg(mediaPublicPath+'/images/invalid-image.png', i, false, false));
                        }


                    }

                }

            });

        };



        let createContainer = function () {
            dataTransfer[plugin.settings.imagesInputName] = [];

            deletedFiles[plugin.settings.imagesInputName] = [];

            // Create the image uploader container
            let $container = $('<div>', { class: 'image-uploader' }),

                // Create the input type file and append it to the container
                $input = $('<input>', {
                    type: 'file',
                    id: plugin.settings.imagesInputName,
                    name: plugin.settings.imagesInputName + '[]',
                    multiple: ''
                }).appendTo($container),
                $delinput = $('<input>', {
                    type: 'hidden',
                    id: plugin.settings.imagesInputName+'_deleted',
                    name: plugin.settings.imagesInputName+'_deleted',
                }).appendTo($container),


                // Create the text container and append it to the container
                $textContainer = $('<div>', {
                    class: 'upload-text'
                }).appendTo($container),

                // Create the icon and append it to the text container
                $i = $('<div>', { class: 'bold-text-upload', text: 'Drag & drop files here' }).appendTo($textContainer),

                // Create the text and append it to the text container
                $span = $('<span class="sub-btn">', { text: plugin.settings.label }).appendTo($textContainer);


            // Listen to container click and trigger input file click
            $container.on('click', function (e) {
                // Prevent browser default event and stop propagation
                prevent(e);

                // Trigger input click
                $input.trigger('click');
            });

            // Stop propagation on input click
            $input.on("click", function (e) {
                e.stopPropagation();
            });

            // Listen to input files changed
            $input.on('change', fileSelectHandler.bind($container));

            return $container;
        };


        let prevent = function (e) {
            // Prevent browser default event and stop propagation
            e.preventDefault();
            e.stopPropagation();
        };

        let createImg = function (src, id, isValid, isVideo) {


            // Create the upladed image container
            let $container = $('<div>', { class: 'uploaded-image' }),


            // Create the delete button
            $buttonDiv = $('<div>', { class: 'image-thumb-button' }).appendTo($container),
            $delbutton = $('<button>', { class: 'delete-image', "data-state":"play"}).appendTo($buttonDiv),

            // Create the delete icon
            $i = $('<i>', { class: 'icon-xl fa fa-trash', text: '' }).appendTo($delbutton);

            if(!isVideo || !arguments[3]){
                // Create the img tag
                $img = $('<img>', { src: src }).appendTo($container);

            } else{

                $playbutton = $('<button>', { class: 'delete-image' }).appendTo($container.find('.image-thumb-button')),
                $i = $('<i>', { class: 'icon-xl fa fa-play', text: '' }).appendTo($playbutton);

                // Create the Video tag
                $img = $('<video width="200" height="200" class="video">', { src: src }).appendTo($container);

                //Assign Video SRC
                $img.attr('src',src);
                if (document.addEventListener) {


                    // Add events for all buttons
                    $playbutton.on('click', function(e) {
                        // Prevent browser default event and stop propagation
                        prevent(e);
                        // let $video = parent[0];
                       let $v = $container.find('video')
                       video = $v[0];

                        if (video.paused || video.ended) {
                            $playbutton.attr('data-state', 'play');
                            $v.trigger('play');
                            $playbutton.addClass('play')
                        }
                        else  {
                            $playbutton.attr('data-state', 'pause');
                            $v.trigger('pause');
                            $playbutton.removeClass('play');
                        }
                    });
                }
            }


            // Add red border if file is invalid
            if(!isValid)
            $container.css('border','4px solid #FF0000');

            // If the images are preloaded
            if (plugin.settings.preloaded.length) {

                // Set a identifier
                $container.attr('data-preloaded', true);

                // Create the preloaded input and append it to the container
                let $preloaded = $('<input>', {
                    type: 'hidden',
                    name: plugin.settings.preloadedInputName + '[]',
                    value: id
                }).appendTo($container)
                $container.attr('data-index', id);

                if(plugin.settings.preloaded[id]){
                    $container.attr('data-imageid', plugin.settings.preloaded[id]['id']);
                }

            } else {

                // Set the identifier
                $container.attr('data-index', id);


            }

            // Stop propagation on click
            $container.on("click", function (e) {
                // Prevent browser default event and stop propagation
                prevent(e);
            });

            // Set delete action
            $delbutton.on("click", function (e) {
                // Prevent browser default event and stop propagation
                prevent(e);
                var input = $('#'+plugin.settings.imagesInputName);
                var imageUploader = input.parent('.image-uploader')
                var parent = $container.parent('.uploaded');
                // If is not a preloaded image
                if (parseInt($container.data('index')) >= 0) {

                    // Get the image index
                    let index = parseInt($container.data('index'));
                    let imageId = parseInt($container.data('imageid'));
                    // Remove this image from the container
                    $container.remove();


                    dataTransfer[plugin.settings.imagesInputName].splice(index,1)
                    deletedFiles[plugin.settings.imagesInputName].push(imageId)
                    parent.find('.uploaded-image[data-index]').each(function (i, cont) {

                    $(cont).attr('data-index', i );

                    });



                    // Remove the file from input
                    $('#'+plugin.settings.imagesInputName+'_deleted').val(deletedFiles[plugin.settings.imagesInputName].join(','))
                }
                // If there is no more uploaded files


                var form = $("#owner-panel-form");
                if (form.valid() === false) {
                    // $('#nextBtn').addClass('disabled')
                }
                else {
                    $('#nextBtn').removeClass('disabled')
                }

            });
            return $container;
        };

        let fileDragHover = function (e) {

            // Prevent browser default event and stop propagation
            prevent(e);

            // Change the container style
            if (e.type === "dragover") {
                $(this).addClass('drag-over');
            } else {
                $(this).removeClass('drag-over');
            }
        };

        let fileSelectHandler = function (e) {

            showLoader()
            // Prevent browser default event and stop propagation
            prevent(e);

            // Get the jQuery element instance
            let $container = $(this);

            // Change the container style
            $container.removeClass('drag-over');

            // Get the files
            let files = e.target.files || e.originalEvent.dataTransfer.files;

            // Makes the upload
            setPreview($container, files);
            var form = jQuery("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else {
                jQuery('#nextBtn').removeClass('disabled')
            }

        };



        let setPreview = function ($container, files) {



            // Get the upload images container
            let $uploadedContainer =  $('#'+plugin.settings.imagesInputName+"_uploaded");
            var accept = $('#'+plugin.settings.imagesInputName).attr('accept');
            $uploadedContainer.addClass('uploaded')
            // Get the files input
            $input = $container.find('input[type="file"]');
            var dataObj = dataTransfer[plugin.settings.imagesInputName];
            // Run through the files
            $(files).each(function (i, file) {


                var len = Object.size(dataObj);
                if(len >= 15)
                {   hideLoader()
                    return false;
                }

                dataObj.push({name: file.name, size: file.size, type: file.type, valid:true, uploaded:false});
                if (accept.indexOf(file.type) > -1 &&  file.type == 'application/pdf') {
                    $uploadedContainer.append(createImg(mediaPublicPath+'/images/pdf-icon.svg', len, true, false));
                    uploadMedia($uploadedContainer,file,len);
                } else if (accept.indexOf(file.type) > -1 && (file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png')) {
                    // Set preview
                    $uploadedContainer.append(createImg(URL.createObjectURL(file), len, true, false));

                    if(plugin.settings.isWidthCheck){
                        var img = new Image();
                        img.onload = function() {
                            if (this.width < 1024 || this.height < 683) {
                                $('.uploaded').find("[data-index='" + len + "']").css('border', '4px solid #FF0000')
                                $('.uploaded').find("[data-index='" + len + "']").addClass('invalid-image')
                                dataObj[len].valid = false;
                                hideLoader()
                            }else{
                               uploadMedia($uploadedContainer,file,len)
                            }

                            var form = $("#owner-panel-form");
                            if (form.valid() === false){
                                // jQuery('#nextBtn').addClass('disabled')
                            }
                            else {
                                jQuery('#nextBtn').removeClass('disabled')
                            }

                        };
                        img.onerror = function() {

                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    else{
                        uploadMedia($uploadedContainer,file,len);
                    }


                } else if (accept.indexOf(file.type) > -1 && (file.type == 'video/mp4' || file.type == 'video/quicktime')) {
                    // Set preview
                    $uploadedContainer.append(createImg(URL.createObjectURL(file), len, true, true));
                    uploadMedia($uploadedContainer,file,len)
                } else {
                    $uploadedContainer.append(createImg(mediaPublicPath+'/images/invalid-image.png', len, false, false));
                    dataObj[len].valid = false;
                    hideLoader()

                }


            });

            dataTransfer[plugin.settings.imagesInputName] = dataObj;
            $input.val('');
            var form = $("#owner-panel-form");
            if (form.valid() === false){
                // jQuery('#nextBtn').addClass('disabled')
            }
            else {
                jQuery('#nextBtn').removeClass('disabled')
            }



        };

        function uploadMedia($uploadedContainer,file, len){

            var form_data = new FormData()
            form_data.append(plugin.settings.imagesInputName+'[]', file)
            form_data.append(plugin.settings.imagesInputName+'_filetype', file.type)
            form_data.append('property_id',$('#hdn_property_id').val())
            $.ajax({
                type: "POST",
                url: propertyupload,
                dataType: "JSON",
                headers: {
                    "Authorization": "Bearer {{session()->get('owner_token')}}",
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: form_data,
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                success: function(msg) {
                    hideLoader();
                    $uploadedContainer.find(".uploaded-image[data-index='" + len + "']").attr('data-imageid', msg.media_id)
                }
            });
        }
        // Generate a random id
        let random = function () {
            return Date.now() + Math.floor((Math.random() * 100) + 1);
        };

        this.init();

        // Return the instance
        return this;
    };

}(jQuery));
