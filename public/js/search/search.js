$(document).ready(function() {

   
            $('#pageno').val(0)
            $('.gobutton').click(function(){
               $( "#searchForm" ).submit();
            });

           $('.tripper-page-carousalpopup').owlCarousel({
               loop:true,
               margin:30,
               center: true,
               nav:true,
               dots: false,
               autoHeight: false,
               responsive:{
                   0:{
                       items:1
                   },
                   640:{
                       items:2
                   },
                   767:{
                       items:3
                   },
                   992:{
                       items:2
                   },
                   1399:{
                       items:3
                   }
               }
           });

          function getPrice(price)
          {
              var priceArr = price.split(',');
              if(priceArr.length > 1)
              {
               var max = Math.max.apply(Math,priceArr); // 3
               var min = Math.min.apply(Math,priceArr); // 1
               //return min+' - '+max;
               return 'From $'+min;
              }
              else{
               return parseInt(price);
              }



          }

           $('#moreresult').hide();
           $('#noReresultSpan').hide()

           function setPropertyDetails(div, index, propertyObj,loggedId){
                $(div).find('.priceTxt').html(parseInt(propertyObj.price));
                $(div).find('.propert-desription-search').attr('data-id',propertyObj.id)

                var bookings = propertyObj.booking;
                var location = ''; var isBooked = false;
                if(bookings!=''){
                    for(var y in bookings)
                    {
                        if(loggedId!='' && loggedId == bookings[y]['rental_user_id']){
                            let bookingStatus = bookings[y]['owner_status'];
                            let bookingType = bookings[y]['booking_type'];
                            let currentDate = new Date();
                            checkoutDate = new Date(bookings[y]['check_out_date']);

                            if((currentDate < checkoutDate) && (bookingStatus == 'comfirmed' && bookingType == 'vacation_rental') || (bookingStatus == 'approved' || bookingStatus == 'instant_booking' && bookingType == 'property')){
                                isBooked = true;

                            } else {
                                isBooked = false;
                            }
                        } else {
                            isBooked = false;
                        }
                    }

                } else {
                    isBooked = false;
                }

                if(isBooked == true){
                    location +=  getlocationTxt(propertyObj.location_detail);
                } else {
                    location +=  propertyObj.city + ', ' + propertyObj.state;
                    location +=  (propertyObj.zipcode) ? ' ' + propertyObj.zipcode : '';
                }

                $(div).find('.location-city-state-text').html(location)
                var title = propertyObj.title;
                // if(propertyObj.resort_name) {
                //     title +=' - ('+ propertyObj.resort_name+')';
                // }

                var totalBed = propertyObj.total_beds+' Bed'+((propertyObj.total_beds > 1) ? 's' : '');
                $(div).find('.resort-text').html(title)
                $(div).find('.bedTxt').html(totalBed)
                $(div).find('.bathTxt').html(propertyObj.total_bathroom+' Bath'+((propertyObj.total_bathroom > 1) ? 's' : ''))
                $(div).find('.guestTxt').html(propertyObj.no_of_guest+' Guest'+((propertyObj.no_of_guest > 1) ? 's' : ''))
                var isLiked = 0;
                if(propertyObj.isLiked){
                    isLiked = 1;
                    $(div).find('.heart-favourite').addClass('fill')
                }
                $(div).find('.heart-favourite').attr('data-val', isLiked);
                $(div).find('.heart-favourite').attr('data-id', propertyObj.id);

                var slider_thumbnail_id = "slider_thumbnail_"+propertyObj.id;
                var slider_thumbnail = $(div).find('.slider_thumbnail').prop({id: 'slider_thumbnail_'+propertyObj.id});

                if(parseInt(propertyObj.sub_property_type_id) > 0){
                    var type = PropertyTypes[propertyObj.sub_property_type_id]
                    // if(type.length > 11){
                    //     type = PropertyTypes[propertyObj.sub_property_type_id].split(" ")[0];
                    // }
                    $(div).find('.villaTxt').html(type)
                }else{
                    var type = PropertyTypes[propertyObj.property_type_id]
                    // if(type.length > 11){
                    //     type = PropertyTypes[propertyObj.property_type_id].split(" ")[0];
                    // }
                    $(div).find('.villaTxt').html(type)
                }

                var images = propertyObj.propertyimages;

                for(var y in images)
                {
                    let propertyUrl = propertyDetailURL;
                    propertyUrl = propertyUrl.replace(':id', propertyObj.id);

                    var src = mediaPath+'/'+images[y]['photo'];
                    var active = (y==0) ? 'active' : '';
                    var image = '<div class="carousel-item '+active+'">';
                    if(images[y]['media_type'] == 'video/mp4' || images[y]['media_type'] == 'video/quicktime'){
                        image += '<div class="image-thumb-button"><button data-id='+images[y]['id']+' class="play-video"><i class="fa fa-play"></i></button>';
                        image += '<video id='+images[y]['id']+' class="d-block w-100" src="'+src+'" class="video"></div>';
                    } else {
                        image += '<a href="'+propertyUrl+'"><img class="d-block w-100" src="'+src+'" alt="First slide"></a>';
                    }
                    image +='</div>';
                   $(slider_thumbnail).find('.carousel-inner').append(image);

                    var indicator='<li data-target="#slider_thumbnail_'+propertyObj.id+'" data-slide-to="'+y+'" class="'+active+'"></li>';
                    $(slider_thumbnail).find('.carousel-indicators').append(indicator);

                    $('.image-thumb-button .play-video').on('click', function (e) {
                        e.preventDefault();
                        let id = $(this).data('id');

                        if($('video#'+id).get(0).paused == true || $('video#'+id).get(0).ended == true){
                            $('video#'+id).get(0).play();
                            $(this).find('i').attr('class', 'fa fa-pause');
                        }else{
                            $('video#'+id).get(0).pause();
                            $(this).find('i').attr('class', 'fa fa-play');
                        }
                    });
                }


                $(slider_thumbnail).find('a.carousel-control-prev').attr('href', '#slider_thumbnail_'+propertyObj.id)
                $(slider_thumbnail).find('a.carousel-control-next').attr('href', '#slider_thumbnail_'+propertyObj.id)

                // $('#'+slider_thumbnail_id).find('.carousel-inner .carousel-item').first().addClass('active');
                // $('#'+slider_thumbnail_id).find('.carousel-indicators > li').first().addClass('active');
                $('#'+slider_thumbnail_id).carousel("pause").removeData();
                $('#'+slider_thumbnail_id).carousel({interval:false});
                $('#'+slider_thumbnail_id).carousel(0);
           }
           // var propertyJson;
           function renderGrid(propertyJson,resortGroup, resortObj,loggedId)
           {
               if(Object.keys(propertyJson).length == 0){
                var $scrollTo = $('#noReresultSpan').parent('div');
               }else{
                var $scrollTo = $('#searchGrid');
               }

               if($('#hdn_resort_id').val() != ''){
                    $('.vacation-club-header').show()
                    $('.vacation-club-header').find('.back-arrow-cstm').show();
                }
                else{


                    var city = getlocationTxt($('#location_detail').val())
                    if(city !='' && Object.keys(propertyJson).length > 0)
                    {
                        $('.vacation-club-header').show()
                        $('.resortTitle').text(city)
                        $('.resortLocation').text(resortLocTXT)
                       
                    }
                    $('.vacation-club-header').find('.back-arrow-cstm').hide();
                }


               for(var x in propertyJson)
               {
                    var resort_id = propertyJson[x].resort_id;

                    var bookings = propertyJson[x].booking;
                    var location = ''; var isBooked = false;

                //    alert($('hdn_resort_id').val())
                   if($('#hdn_resort_id').val() != '' || !resortGroup[propertyJson[x].resort_id])
                   {
                       if($('#hdn_resort_id').val() != ''){
                            $('.resortTitle').text(propertyJson[x].resort_name)

                            // show address if booking is confirmed
                            if(bookings!=''){
                                for(var y in bookings)
                                {
                                    if(loggedId!='' && loggedId == bookings[y]['rental_user_id']){
                                        let bookingStatus = bookings[y]['owner_status'];
                                        let bookingType = bookings[y]['booking_type'];
                                        let currentDate = new Date();
                                        checkoutDate = new Date(bookings[y]['check_out_date']);

                                        if((currentDate < checkoutDate) && (bookingStatus == 'comfirmed' && bookingType == 'vacation_rental') || (bookingStatus == 'approved' || bookingStatus == 'instant_booking' && bookingType == 'property')){
                                            isBooked = true;
                                        } else {
                                            isBooked = false;
                                        }

                                    } else {
                                        isBooked = false;
                                    }
                                }

                            } else {
                                isBooked = false;
                            }

                            if(isBooked == true){
                                location = getlocationTxt(propertyJson[x].resort_location_detail);
                            } else {
                                location +=  propertyJson[x].city + ', ' + propertyJson[x].state + ' ' + propertyJson[x].zipcode;
                                location +=  (propertyJson[x].zipcode) ? ' ' + propertyJson[x].zipcode : '';
                            }

                            var resortLocTXT = propertyJson.length+' Units | '+ location;
                            // resortLocTXT += (propertyJson[x].zipcode) ? ' ' + propertyJson[x].zipcode : '';
                            $('.resortLocation').text(resortLocTXT)
                       }

                        var div = $("#propertyBlock").clone(true).prop({ id: "property_"+propertyJson[x].id}).appendTo($('#searchGrid'));

                        var price = parseInt(propertyJson[x].price);

                        setPropertyDetails(div, x, propertyJson[x],loggedId);
                        // $(div).find('.priceTxt').html(price);
                        // $(div).find('.bg-slider-text').attr('data-id',propertyJson[x].id)
                        // $(div).find('.location-city-state-text').html(propertyJson[x].location_detail)
                        // $(div).find('.resort-text').html(propertyJson[x].title.replace('(1)',''))
                        // $(div).find('.slider_thumbnail').prop({id: "slider_thumbnail_"+x});

                        var images = propertyJson[x].propertyimages;


                        div.show();
                   }
                   else if(resortGroup[propertyJson[x].resort_id] && propertyJson[x].type_of_property == 'vacation_rental'){

                            var div = $("#VacationClubDiv").clone(true).prop({ id: "property_"+propertyJson[x].resort_id}).appendTo($('#searchGrid'));

                            $(div).find('.VCTitle').html(propertyJson[x].resort_name)

                            // show address if booking is confirmed
                            if(bookings!=''){
                                for(var y in bookings)
                                {
                                    if(loggedId!='' && loggedId == bookings[y]['rental_user_id']){
                                        let bookingStatus = bookings[y]['owner_status'];
                                        let bookingType = bookings[y]['booking_type'];
                                        let currentDate = new Date();
                                        checkoutDate = new Date(bookings[y]['check_out_date']);

                                        if((currentDate < checkoutDate) && (bookingStatus == 'comfirmed' && bookingType == 'vacation_rental') || (bookingStatus == 'approved' || bookingStatus == 'instant_booking' && bookingType == 'property')){
                                            isBooked = true;
                                        } else {
                                            isBooked = false;
                                        }

                                    } else {
                                        isBooked = false;
                                    }
                                }

                            } else {
                                isBooked = false;
                            }

                            if(isBooked == true){
                                location = getlocationTxt(propertyJson[x].resort_location_detail);
                            } else {
                                location +=  propertyJson[x].city + ', ' + propertyJson[x].state;
                                location +=  (propertyJson[x].zipcode) ? ' ' + propertyJson[x].zipcode : '';
                            }

                            // \\var location = getlocationTxt(propertyJson[x].resort_location_detail)
                            // location +=  (propertyJson[x].zipcode) ? ' ' + propertyJson[x].zipcode : '';
                            $(div).find('.VCLocation').html(location);
                            var viewResort = $(div).find('.view-all-units');
                            $(viewResort).html('View '+propertyJson[x].totalresortcount+' Units');


                            $(viewResort).attr('data-reosrtid', resort_id);

                            if(resortGroup[propertyJson[x].resort_id]){


                                for(var p in resortGroup[resort_id]){

                                    var property_id = resortGroup[resort_id][p];
                                    var resort = resortObj[property_id][0];
                                    p = parseInt(p);
                                    var propertyBlock = $(div).find('.propertyBlock'+(p+1))
                                    setPropertyDetails(propertyBlock, x, resort,loggedId)

                                    // $(div).find('.propertyBlock'+(p+1)).find('.resort-text').html(resort.title)
                                    // $(div).find('.propertyBlock'+(p+1)).find('.location-city-state-text').html(resort.location_detail)
                                    // $(div).find('.propertyBlock'+(p+1)).find('.priceTxt').html(resort.price);

                                }
                            }

                            div.show();

                   }

                   if(x==0 && $('.vacation-club-header').is(":hidden")){
                        $scrollTo =  $('#searchGrid');
                    }else if(x==0 && $('.vacation-club-header').is(":visible")) {
                        $scrollTo = $('.vacation-club-header');
                    }
               }

               // var div = $("#propertyBlock").clone().prop({ id: "im-a-clone", html: "im-a-clone" }).appendTo($('#searchGrid'));
               // $(div).find('.price').html('5000/night');

            //    $([document.documentElement, document.body]).animate({
            //         scrollTop: $scrollTo.offset().top - 30
            //     }, 2000);
                $([document.documentElement, document.body]).scrollTop($scrollTo.offset().top - 30);
           }
           function getlocationTxt(location){
                var l = location.split(',');
                l.splice(-1,1);
                location = l.join(',');
                return location;
           }

           $('.back-arrow-cstm').on('click',function(e){
                $('#hdn_resort_id').val('').trigger('change');
           });

           $('.heart-favourite').click(function(e) {
                e.preventDefault();
                var property_id = $(this).attr('data-id');
                var isLike = $(this).attr('data-val');
                var _this = this;
                var url = '';
                if (isLike == 1) {
                    url = unlikeUrl
                } else if (isLike == 0) {
                    url = likeUrl
                }

                if (url != '') {
                    $.ajax({

                        type: "POST",
                        url: url,
                        dataType: "JSON",
                        headers: {
                            "Authorization": Authorization,
                        },
                        data: {
                            property_id: property_id
                        },
                        success: function(data) {

                            if (data.success === true) {
                                isLike = (isLike == 1) ? 0 : 1;

                                $(_this).attr('data-val', isLike);

                                if (isLike == 1) {
                                    $(_this).addClass('fill');
                                } else {
                                    $(_this).removeClass('fill');
                                }
                            }

                        },
                        error: function(xhr, status, error) {

                        }


                    });
                }
        })
           $('#hdn_resort_id').on('change',function(e){
               if($(this).val() != ''){
                    $('.vacation-club-header').show()

               }
               else{
                    $('.vacation-club-header').hide()
               }
               $( "#searchForm" ).submit();
            //    var $container = $(window),
            //     $scrollTo = $('#searchGrid');
            //     $([document.documentElement, document.body]).animate({
            //         scrollTop: $scrollTo.offset().top - 50
            //     }, 1000);
           });

           $('.view-all-units').click(function(e){
                var resortId = $(this).attr('data-reosrtid');
                $('#hdn_resort_id').val(resortId).trigger('change');
             });

           $('.propert-desription-search').click(function(e){
                e.preventDefault();
                if (!$(e.target).is('.heart-favourite, .heart-favourite *')) {
                var propertyId = $(this).attr('data-id');

                if(propertyId){
                    var url = propertyDetailURL;
                    url = url.replace(':id', propertyId);
                    if(fromDate != ''){
                        var url_string = "?form_date="+fromDate+"&to_date="+toDate+"&guest="+parseInt(guest);
                        url = url.concat(url_string);
                    }

                    window.location.href = url;
                }
                }

           });


           showLoader();

           $('#moreresult').click(function() {

               showLoader();
               getSreachResult();

           });

           //$( "#searchForm" ).submit();
           $('#searchGrid').html('');
           getSreachResult();


           
           $('ul.navbar-nav.ml-auto li.nav-item a').click(function() {
                   $('li.nav-item a').removeClass("active-menu-link");
                   $(this).addClass("active-menu-link");
           });

           $('input:checkbox[name="property_type[]"]').on('change',function()
           {
               $( "#searchForm" ).submit();
           })

           $('#searchForm').validate({


               rules: {
                location_detail: {
                    address_selection_required : true,
                    required:function(element) {
                        if(element.value == '' && $("#hdn_search_text").val() != ''){
                            return false;
                        }
                        else if(element.value == '' && $("#hdn_search_text").val() == ''){
                            return true;
                        }
                        else if(element.value == ''){
                            return true;
                        }

                        return false;

                  }
                },
                modal_location_detail:{                   
                    address_selection_required : true,
                    required:function(element) {
                        if(element.value == '' && $("#hdn_search_text").val() != ''){
                            return false;
                        }
                        else if(element.value == '' && $("#hdn_search_text").val() == ''){
                            return true;
                        }
                        else if(element.value == ''){
                            return true;
                        }

                        return false;

                  }

                },
                //    filter_dates:{
                //        required:true
                //    },
                //    filter_dates_picker:{
                //        required:true
                //    },
                //    modal_filter_dates:{
                //        required:true
                //    },
                   number_of_guest:{
                       required:true
                   }
               },
               messages: {
                   location_detail: {
                    address_selection_required: "Please type in a city.",
                    required:"Please type in a city.",

                   },
                   modal_location_detail: {
                    address_selection_required: "Please type in a city.",
                    required:"Please type in a city.",

                   },
                //    filter_dates:{
                //        required: 'Please select a date.'
                //    },
                //    filter_dates_picker:{
                //        required: 'Please select a date.'
                //    },
                //    modal_filter_dates:{
                //        required: 'Please select a date.'
                //    },
                   number_of_guest:{
                       required: 'Please select a guest.'
                   },
               }
           });
           jQuery.validator.addMethod("address_selection_required", function(value, element) {
               
                if(element.value == '' && $("#hdn_search_text").val() != ''){
                    return true;
                }
                else if(element.value == '' && $("#hdn_search_text").val() == ''){
                    return false;
                }else if(element.value != '' && $(".address_selected_from_drop").val() != element.value){
                    return false;
                }
                else if(element.value == ''){
                    return false;
                }
                return true;
            }, "");



           $( "#searchForm" ).submit(function( event ) {
               var filterDate = $('#filter_dates_picker').val();
               filterDate = filterDate.split(' - ');
               fromDate = moment(filterDate[0]).format('MM/DD/YYYY');
               toDate = moment(filterDate[1]).format('MM/DD/YYYY');
               guest = $('#number_of_guest').val();
               event.preventDefault();
               $('#moreresult').hide()
               let $form = $(this);
               if(!$form.valid()) return false;
               if($('#location_detail').val() != ''){
                $('#hdn_search_text').val('')
               }

               $('#filter_popup').modal('hide');

               showLoader();
               $('#pageno').val(0)
               $('.vacation-club-header').hide()
               $('#searchGrid').html('');

               getSreachResult();
               // var place = autocomplete.getPlace().address_components;
               // if(place)
               // {
               //     var city = place[0].long_name;
               //     var state = place[0].short_name;
               //     var country = 'USA';


               // }
               // else{
               //     return false;
               // }
               //var location = city+', '+state+', '+country;


           });

           $(window).keydown(function(event){
               if( (event.keyCode == 13) ) {
               event.preventDefault();
               return false;
               }
           });

       function getSreachResult()
       {
        //    alert($('#hdn_search_text').val())
           window.history.pushState(null,null, window.location.origin +window.location.pathname+'?'+$('#searchForm').serialize());

           //$('.loader').show();
           $.ajax({

                   type: "POST",
                   url: searchRequestURL,
                   dataType: "JSON",
                   headers: {
                       "Authorization": Authorization,
                   },
                   data: new FormData($('#searchForm')[0]),
                   processData: false,
                   contentType: false,
                   success: function(data) {
                       if(data.success = 'true')
                       {
                           var total = data.data.length;
                           renderGrid(data.data, data.resortGroup, data.resortObj,data.loggedId);


                            if($('#location_detail').val() !='')
                            {
                                if(data.zero_stay == 1){
                                    var txt = (total > 1) ? total+' Stays' : total+' Stay';
                                    $('.zero_stay').text('0 Stays Match Your Exact Search');
                                    $('.resortLocation').html('<b>HERE ARE '+txt.toUpperCase()+' STAYS OUTSIDE YOUR SEARCH WE THINK YOU’LL ENJOY</b>');
                                    $('.resortLocation').addClass('font_bold')
                                    
                                }

                            }

                           if(total == 0)
                           {
                               $('#moreresult').hide()
                               $('#noReresultSpan').show()
                           }
                           else{

                               if(total < 10){
                                   $('#moreresult').hide()
                               }
                               else{
                                   $('#moreresult').show()
                               }

                               $('#noReresultSpan').hide();
                               $('#pageno').val(data.page)

                           }
                       }
                       hideLoader();



                   },
                   error: function(xhr, status, error) {
                       $('#moreresult').hide()
                       $('#noReresultSpan').show()
                       hideLoader();
                   }


           });
           getSearchCount();
       }
      

       function getSearchCount()
       {
           //$('.loader').show();
           $('.numbers-of-property.property').text('(0)')
           $('.numbers-of-property.vacation_rental').text('(0)')
           $.ajax({

                   type: "POST",
                   url: searchCountRequestURL,
                   dataType: "JSON",
                   headers: {
                       "Authorization": Authorization,
                   },
                   data: new FormData($('#searchForm')[0]),
                   processData: false,
                   contentType: false,
                   success: function(data) {
                       if(data.success = 'true')
                       {

                        var obj =  data.data
                        var total = 0;
                        var count = 0;
                        for(var x in obj){
                            total += parseInt(obj[x].total_count);
                            count +=     $("input[value='" + obj[x].type_of_property + "']").prop('checked') ? parseInt(obj[x].total_count) : 0;

                            $('.numbers-of-property.'+obj[x].type_of_property).text('('+obj[x].total_count+')')
                        }
                        
                        var txt = (count > 1) ? count+' Stays' : count+' Stay';
                        if(data.zero_stay == 1){
                           
                            $('.zero_stay').text('0 Stays Match Your Exact Search');
                            $('.resortLocation').html('<b>HERE ARE '+txt.toUpperCase()+' STAYS OUTSIDE YOUR SEARCH WE THINK YOU’LL ENJOY</b>');
                            $('.resortLocation').addClass('font_bold')
                            
                        }else{
                            $('.zero_stay').text('');
                            $('.resortLocation').removeClass('font_bold')
                            $('.resortLocation').text(txt);
                        }

                       }




                   },
                   error: function(xhr, status, error) {
                       $('#moreresult').hide()
                       $('#noReresultSpan').show()
                       hideLoader();
                   }


           });
        //    getSearchCount();
       }
       function pad(num, size) {
        num = num.toString();
        while (num.length < size) num = "0" + num;
        return num;
    }
});
