$(function(){
  /*
   * For the sake keeping the code clean and the examples simple this file
   * contains only the plugin configuration & callbacks.
   *
   * UI functions ui_* can be located in:
   *   - assets/demo/uploader/js/ui-main.js
   *   - assets/demo/uploader/js/ui-multiple.js
   *   - assets/demo/uploader/js/ui-single.js
   */



  // property csv
  $('.propertyCsv').dmUploader({
    url: bulkDownload,
    fieldName: 'propertyData',
    // maxFileSize: 30000000000, // 3 Megs max
    //allowedTypes: ".csv, .xls, .xlsx, text/csv, application/csv,text/comma-separated-values, application/csv, application/excel,application/vnd.msexcel, text/anytext, application/vnd. ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    extFilter: ['csv'],
    // ...
    auto: false,
    queue: false,
    multiple: false,
    method: "POST",
    extraData: function(id) {
        return {
            unique_key : $('#unique_key').val()
        };
    },
    onDragEnter: function(){
      // Happens when dragging something over the DnD area
      this.addClass('active');
    },
    onDragLeave: function(){
      // Happens when dragging something OUT of the DnD area
      this.removeClass('active');
    },
    onInit: function(){
      // Plugin is ready to use
      ui_add_log('Penguin initialized :)', 'info');
      let reuploadKey = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
      if(reuploadKey!='' && reuploadCsv!=''){

          let uploadedcsv = '<li class="media" id="uploaderFile'+reuploadKey+'">'+
                            '<div class="media-body mb-1">'+
                            '<div class="row">'+
                            '<div class="col-2">'+
                            '<div class="icon">'+
                            '<svg width="35" height="50" viewBox="0 0 35 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M3 0C1.34315 0 0 1.34315 0 3V47C0 48.6569 1.34315 50 3 50H32C33.6569 50 35 48.6569 35 47V8.75L26.25 0H3ZM33.6931 11.1768C33.712 11.2014 33.7309 11.2259 33.75 11.2503H24.9463C24.2868 11.2503 23.75 10.7135 23.75 10.054V1.25C23.7748 1.26951 23.7999 1.28877 23.8249 1.30802C23.9181 1.37975 24.0114 1.45147 24.0951 1.53666L33.4621 10.9037C33.5483 10.9883 33.6207 11.0826 33.6931 11.1768Z" fill="#F5A73B"></path></svg>'+
                            '<div class="filename">CSV</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-10">'+
                            '<p class="mb-2">'+
                            '<strong>'+reuploadCsv+'</strong>'+
                            '<i class="fa fa-check" aria-hidden="true"></i>'+
                            '</p>'+
                            '<div class="progress mb-2">'+
                            '<div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>'+
                            '</div>'+
                            '<p class="controls" style="display: none;">'+
                            '<button href="#" class="btn btn-sm btn-primary start" role="button" disabled="">Start</button>'+
                            '<button href="#" class="btn btn-sm btn-danger cancel" role="button" disabled="">Cancel</button>'+
                            '</p>'+
                            '</div>'+
                            '</div>'+
                            '<hr class="mt-1 mb-1">'+
                            '</div>'+
                            '<div class="reupload-files">'+
                            '<br>  <span class="status text-success">Successfully Uploaded</span>'+
                            '<button href="#" class="btn btn-sm btn-primary reupload" role="button">Reupload</button>'+
                            '</div>'+
                            '</li>';

        $('#propertyCsv').addClass('upload-start success');
        $('#propertyCsv').find('ul li').html(uploadedcsv);
      }

    },
    onComplete: function(){
      // All files in the queue are processed (success or error)
      ui_add_log('All pending tranfers finished');
    },
    onNewFile: function(id, file){
      // When a new file is added using the file selector or the DnD area
      ui_add_log('New file added #' + id);
      ui_multi_add_file(id, file, $(this));

    },
    onBeforeUpload: function(id){
      // about tho start uploading a file
      ui_add_log('Starting the upload of #' + id);
      ui_multi_update_file_status(id, 'uploading', 'Uploading...');
      ui_multi_update_file_progress(id, 0, '', true);
      ui_multi_update_file_controls(id, false, true);  // change control buttons status
    },
    onUploadProgress: function(id, percent){
      // Updating file progress
      ui_multi_update_file_progress(id, percent);
    },
    onUploadSuccess: function(id, data){
      // A file was successfully uploaded
      if($.trim(data.uploadedCsv)){
        $('#UploadedCsv').empty().val(data.uploadedCsv);
        $('#unique_key').empty().val(data.uniqueId);
        ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
        ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
        ui_multi_update_file_status(id, 'success', 'Successfully Uploaded');
        ui_multi_update_file_progress(id, 100, 'success', false);
        ui_multi_update_file_controls(id, false, false);  // change control buttons status
      } else {

        ui_multi_update_file_progress(id, 0, 'danger', false);
        ui_multi_update_file_controls(id, true, false, true); // change control buttons status
      }

    },
    onUploadCanceled: function(id) {
      // Happens when a file is directly canceled by the user.
      ui_multi_update_file_status(id, 'warning', 'Canceled by User');
      ui_multi_update_file_progress(id, 0, 'warning', false);
      ui_multi_update_file_controls(id, true, false);
    },
    onUploadError: function(id, xhr, status, message){
      // Happens when an upload error happens
      ui_multi_update_file_status(id, 'danger', message);
      ui_multi_update_file_progress(id, 0, 'danger', false);
      ui_multi_update_file_controls(id, true, false, true); // change control buttons status
    },
    onFallbackMode: function(){
      // When the browser doesn't support this plugin :(
      ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
    },
    onFileSizeError: function(file){
      ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
    }
  });

  // property photos
  $('.propertyPhotos').dmUploader({
    url: bulkDownload,
    fieldName: 'propertyImages',
    // maxFileSize: 30000000000, // 3 Megs max
    allowedTypes: '.zip',
    auto: false,
    queue: false,
    multiple: false,
    method: "POST",
    extraData: function(id) {
        return {
            unique_key : $('#unique_key').val()
        };
    },
    onDragEnter: function(){
      // Happens when dragging something over the DnD area
      this.addClass('active');
    },
    onDragLeave: function(){
      // Happens when dragging something OUT of the DnD area
      this.removeClass('active');
    },
    onInit: function(){
      // Plugin is ready to use
      ui_add_log('Penguin initialized :)', 'info');

      let reuploadKey = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
      if(reuploadKey!='' && reuploadMedia!=''){

          let uploadedMedia = '<li class="media" id="uploaderFile'+reuploadKey+'">'+
                                '<div class="media-body mb-1">'+
                                '<div class="row">'+
                                '<div class="col-2">'+
                                '<div class="icon">'+
                                '<svg width="35" height="50" viewBox="0 0 35 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M3 0C1.34315 0 0 1.34315 0 3V47C0 48.6569 1.34315 50 3 50H32C33.6569 50 35 48.6569 35 47V8.75L26.25 0H3ZM33.6931 11.1768C33.712 11.2014 33.7309 11.2259 33.75 11.2503H24.9463C24.2868 11.2503 23.75 10.7135 23.75 10.054V1.25C23.7748 1.26951 23.7999 1.28877 23.8249 1.30802C23.9181 1.37975 24.0114 1.45147 24.0951 1.53666L33.4621 10.9037C33.5483 10.9883 33.6207 11.0826 33.6931 11.1768Z" fill="#F5A73B"></path></svg>'+
                                '<div class="filename">ZIP</div>'+
                                '</div>'+
                                '</div>'+
                                '<div class="col-10">'+
                                '<p class="mb-2">'+
                                '<strong>'+reuploadMedia+'</strong>'+
                                '<i class="fa fa-check" aria-hidden="true"></i>'+
                                '</p>'+
                                '<div class="progress mb-2">'+
                                '<div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>'+
                                '</div>'+
                                '<p class="controls" style="display: none;">'+
                                '<button href="#" class="btn btn-sm btn-primary start" role="button" disabled="">Start</button>'+
                                '<button href="#" class="btn btn-sm btn-danger cancel" role="button" disabled="">Cancel</button>'+
                                '</p>'+
                                '</div>'+
                                '</div>'+
                                '<hr class="mt-1 mb-1">'+
                                '</div>'+
                                '<div class="reupload-files">'+
                                '<br>  <span class="status text-success">Successfully Uploaded</span>'+
                                '<button href="#" class="btn btn-sm btn-primary reupload" role="button">Reupload</button>'+
                                '</div>'+
                                '</li>';

        $('#propertyPhotos').addClass('upload-start success');
        $('#propertyPhotos').find('ul li').html(uploadedMedia);
      }
    },
    onComplete: function(){
      // All files in the queue are processed (success or error)
      ui_add_log('All pending tranfers finished');
    },
    onNewFile: function(id, file){
      // When a new file is added using the file selector or the DnD area
      ui_add_log('New file added #' + id);
      ui_multi_add_file(id, file, $(this));
    },
    onBeforeUpload: function(id){
      // about tho start uploading a file
      ui_add_log('Starting the upload of #' + id);
      ui_multi_update_file_status(id, 'uploading', 'Uploading...');
      ui_multi_update_file_progress(id, 0, '', true);
      ui_multi_update_file_controls(id, false, true);  // change control buttons status
    },
    onUploadProgress: function(id, percent){
      // Updating file progress
      ui_multi_update_file_progress(id, percent);
    },
    onUploadSuccess: function(id, data){
      // A file was successfully uploaded
      if($.trim(data.propertyImagesChangeFileName)){
        $('#UploadedImages').empty().val(data.propertyImagesChangeFileName);
        $('#unique_key').empty().val(data.uniqueId);
        ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
        ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
        ui_multi_update_file_status(id, 'success', 'Successfully Uploaded');
        ui_multi_update_file_progress(id, 100, 'success', false);
        ui_multi_update_file_controls(id, false, false);  // change control buttons status
      } else {
        // Happens when an upload error happens
        // ui_multi_update_file_status(id, 'danger', message);
        ui_multi_update_file_progress(id, 0, 'danger', false);
        ui_multi_update_file_controls(id, true, false, true); // change control buttons status
      }

    },
    onUploadCanceled: function(id) {
      // Happens when a file is directly canceled by the user.
      ui_multi_update_file_status(id, 'warning', 'Canceled by User');
      ui_multi_update_file_progress(id, 0, 'warning', false);
      ui_multi_update_file_controls(id, true, false);
    },
    onUploadError: function(id, xhr, status, message){
      // Happens when an upload error happens
      ui_multi_update_file_status(id, 'danger', message);
      ui_multi_update_file_progress(id, 0, 'danger', false);
      ui_multi_update_file_controls(id, true, false, true); // change control buttons status
    },
    onFallbackMode: function(){
      // When the browser doesn't support this plugin :(
      ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
    },
    onFileSizeError: function(file){
      ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
    }
  });

   // property verification
  $('.verificationDocs').dmUploader({
    url: bulkDownload,
    fieldName: 'propertyVerficationDocs',
    // maxFileSize: 30000000000, // 3 Megs max
    allowedTypes: '.zip',
    auto: false,
    queue: false,
    multiple: false,
    method: "POST",
    extraData: function(id) {
        return {
            unique_key : $('#unique_key').val()
        };
    },
    onDragEnter: function(){
      // Happens when dragging something over the DnD area
      this.addClass('active');
    },
    onDragLeave: function(){
      // Happens when dragging something OUT of the DnD area
      this.removeClass('active');
    },
    onInit: function(){
      // Plugin is ready to use
      ui_add_log('Penguin initialized :)', 'info');

      let reuploadKey = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
      if(reuploadKey!='' && reuploadVerification!=''){

          let uploadedVerification = '<li class="media" id="uploaderFile'+reuploadKey+'">'+
                                    '<div class="media-body mb-1">'+
                                    '<div class="row">'+
                                    '<div class="col-2">'+
                                    '<div class="icon">'+
                                    '<svg width="35" height="50" viewBox="0 0 35 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M3 0C1.34315 0 0 1.34315 0 3V47C0 48.6569 1.34315 50 3 50H32C33.6569 50 35 48.6569 35 47V8.75L26.25 0H3ZM33.6931 11.1768C33.712 11.2014 33.7309 11.2259 33.75 11.2503H24.9463C24.2868 11.2503 23.75 10.7135 23.75 10.054V1.25C23.7748 1.26951 23.7999 1.28877 23.8249 1.30802C23.9181 1.37975 24.0114 1.45147 24.0951 1.53666L33.4621 10.9037C33.5483 10.9883 33.6207 11.0826 33.6931 11.1768Z" fill="#F5A73B"></path></svg>'+
                                    '<div class="filename">ZIP</div>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="col-10">'+
                                    '<p class="mb-2">'+
                                    '<strong>'+reuploadVerification+'</strong>'+
                                    '<i class="fa fa-check" aria-hidden="true"></i>'+
                                    '</p>'+
                                    '<div class="progress mb-2">'+
                                    '<div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>'+
                                    '</div>'+
                                    '<p class="controls" style="display: none;">'+
                                    '<button href="#" class="btn btn-sm btn-primary start" role="button" disabled="">Start</button>'+
                                    '<button href="#" class="btn btn-sm btn-danger cancel" role="button" disabled="">Cancel</button>'+
                                    '</p>'+
                                    '</div>'+
                                    '</div>'+
                                    '<hr class="mt-1 mb-1">'+
                                    '</div>'+
                                    '<div class="reupload-files">'+
                                    '<br>  <span class="status text-success">Successfully Uploaded</span>'+
                                    '<button href="#" class="btn btn-sm btn-primary reupload" role="button">Reupload</button>'+
                                    '</div>'+
                                    '</li>';

        $('#verificationDocs').addClass('upload-start success');
        $('#verificationDocs').find('ul li').html(uploadedVerification);
      }
    },
    onComplete: function(){
      // All files in the queue are processed (success or error)
      ui_add_log('All pending tranfers finished');
    },
    onNewFile: function(id, file){
      // When a new file is added using the file selector or the DnD area
      ui_add_log('New file added #' + id);
      ui_multi_add_file(id, file, $(this));
    },
    onBeforeUpload: function(id){
      // about tho start uploading a file
      ui_add_log('Starting the upload of #' + id);
      ui_multi_update_file_status(id, 'uploading', 'Uploading...');
      ui_multi_update_file_progress(id, 0, '', true);
      ui_multi_update_file_controls(id, false, true);  // change control buttons status
    },
    onUploadProgress: function(id, percent){
      // Updating file progress
      ui_multi_update_file_progress(id, percent);
    },
    onUploadSuccess: function(id, data){
      // A file was successfully uploaded

      if($.trim(data.propertyVerficationChangeFileName)){
        $('#UploadedVerificationDocs').empty().val(data.propertyVerficationChangeFileName);
        $('#unique_key').empty().val(data.uniqueId);
        ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
        ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
        ui_multi_update_file_status(id, 'success', 'Successfully Uploaded');
        ui_multi_update_file_progress(id, 100, 'success', false);
        ui_multi_update_file_controls(id, false, false);  // change control buttons status

      } else {

        ui_multi_update_file_progress(id, 0, 'danger', false);
        ui_multi_update_file_controls(id, true, false, true); // change control buttons status
      }

    },
    onUploadCanceled: function(id) {
      // Happens when a file is directly canceled by the user.
      ui_multi_update_file_status(id, 'warning', 'Canceled by User');
      ui_multi_update_file_progress(id, 0, 'warning', false);
      ui_multi_update_file_controls(id, true, false);
    },
    onUploadError: function(id, xhr, status, message){
      // Happens when an upload error happens
      ui_multi_update_file_status(id, 'danger', message);
      ui_multi_update_file_progress(id, 0, 'danger', false);
      ui_multi_update_file_controls(id, true, false, true); // change control buttons status
    },
    onFallbackMode: function(){
      // When the browser doesn't support this plugin :(
      ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
    },
    onFileSizeError: function(file){
      ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
    }
  });

  // property govtId
  $('.govtIdDocs').dmUploader({
    url: bulkDownload,
    fieldName: 'propertyGovtId',
    // maxFileSize: 30000000000, // 3 Megs max
    allowedTypes: '.zip',
    auto: false,
    queue: false,
    multiple: false,
    method: "POST",
    extraData: function(id) {
        return {
            unique_key : $('#unique_key').val()
        };
    },
    onDragEnter: function(){
      // Happens when dragging something over the DnD area
      this.addClass('active');
    },
    onDragLeave: function(){
      // Happens when dragging something OUT of the DnD area
      this.removeClass('active');
    },
    onInit: function(){
      // Plugin is ready to use
      ui_add_log('Penguin initialized :)', 'info');

      let reuploadKey = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
      if(reuploadKey!='' && reuploadgovtId!=''){

          let uploadedgovtId = '<li class="media" id="uploaderFile'+reuploadKey+'">'+
                                    '<div class="media-body mb-1">'+
                                    '<div class="row">'+
                                    '<div class="col-2">'+
                                    '<div class="icon">'+
                                    '<svg width="35" height="50" viewBox="0 0 35 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M3 0C1.34315 0 0 1.34315 0 3V47C0 48.6569 1.34315 50 3 50H32C33.6569 50 35 48.6569 35 47V8.75L26.25 0H3ZM33.6931 11.1768C33.712 11.2014 33.7309 11.2259 33.75 11.2503H24.9463C24.2868 11.2503 23.75 10.7135 23.75 10.054V1.25C23.7748 1.26951 23.7999 1.28877 23.8249 1.30802C23.9181 1.37975 24.0114 1.45147 24.0951 1.53666L33.4621 10.9037C33.5483 10.9883 33.6207 11.0826 33.6931 11.1768Z" fill="#F5A73B"></path></svg>'+
                                    '<div class="filename">ZIP</div>'+
                                    '</div>'+
                                    '</div>'+
                                    '<div class="col-10">'+
                                    '<p class="mb-2">'+
                                    '<strong>'+reuploadgovtId+'</strong>'+
                                    '<i class="fa fa-check" aria-hidden="true"></i>'+
                                    '</p>'+
                                    '<div class="progress mb-2">'+
                                    '<div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>'+
                                    '</div>'+
                                    '<p class="controls" style="display: none;">'+
                                    '<button href="#" class="btn btn-sm btn-primary start" role="button" disabled="">Start</button>'+
                                    '<button href="#" class="btn btn-sm btn-danger cancel" role="button" disabled="">Cancel</button>'+
                                    '</p>'+
                                    '</div>'+
                                    '</div>'+
                                    '<hr class="mt-1 mb-1">'+
                                    '</div>'+
                                    '<div class="reupload-files">'+
                                    '<br>  <span class="status text-success">Successfully Uploaded</span>'+
                                    '<button href="#" class="btn btn-sm btn-primary reupload" role="button">Reupload</button>'+
                                    '</div>'+
                                    '</li>';

        $('#govtIdDocs').addClass('upload-start success');
        $('#govtIdDocs').find('ul li').html(uploadedgovtId);
      }
    },
    onComplete: function(){
      // All files in the queue are processed (success or error)
      ui_add_log('All pending tranfers finished');
    },
    onNewFile: function(id, file){
      // When a new file is added using the file selector or the DnD area
      ui_add_log('New file added #' + id);
      ui_multi_add_file(id, file, $(this));

    },
    onBeforeUpload: function(id){
      // about tho start uploading a file
      ui_add_log('Starting the upload of #' + id);
      ui_multi_update_file_status(id, 'uploading', 'Uploading...');
      ui_multi_update_file_progress(id, 0, '', true);
      ui_multi_update_file_controls(id, false, true);  // change control buttons status
    },
    onUploadProgress: function(id, percent){
      // Updating file progress
      ui_multi_update_file_progress(id, percent);
    },
    onUploadSuccess: function(id, data){
      // A file was successfully uploaded
      if($.trim(data.propertyGovtIdChangeFileName)){
        $('#UploadedGovtId').empty().val(data.propertyGovtIdChangeFileName);
        $('#unique_key').empty().val(data.uniqueId);
        ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
        ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
        ui_multi_update_file_status(id, 'success', 'Successfully Uploaded');
        ui_multi_update_file_progress(id, 100, 'success', false);
        ui_multi_update_file_controls(id, false, false);  // change control buttons status
      } else {
        ui_multi_update_file_progress(id, 0, 'danger', false);
        ui_multi_update_file_controls(id, true, false, true); // change control buttons status
      }

    },
    onUploadCanceled: function(id) {
      // Happens when a file is directly canceled by the user.
      ui_multi_update_file_status(id, 'warning', 'Canceled by User');
      ui_multi_update_file_progress(id, 0, 'warning', false);
      ui_multi_update_file_controls(id, true, false);
    },
    onUploadError: function(id, xhr, status, message){
      // Happens when an upload error happens
      ui_multi_update_file_status(id, 'danger', message);
      ui_multi_update_file_progress(id, 0, 'danger', false);
      ui_multi_update_file_controls(id, true, false, true); // change control buttons status
    },
    onFallbackMode: function(){
      // When the browser doesn't support this plugin :(
      ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
    },
    onFileSizeError: function(file){
      ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
    }
  });

  /*
    Global controls
  */
  $('#btnApiStart').on('click', function(evt){
    evt.preventDefault();

    $('.drag-and-drop-zone').dmUploader('start');
  });

  $('#btnApiCancel').on('click', function(evt){
    evt.preventDefault();

    $('.drag-and-drop-zone').dmUploader('cancel');
  });

  /*
    Each File element action
   */

  $('.files').on('click', 'button.start', function(evt){

    var parent = $(this).closest('.drag-and-drop-zone');

    evt.preventDefault();

    var id = $(this).closest('li.media').data('file-id');
    $(parent).dmUploader('start', id);
  });

  $('.files').on('click', 'button.cancel', function(evt){
    var parent = $(this).closest('.drag-and-drop-zone');
    evt.preventDefault();
    $(parent).find('.hdnData').val('');
    $(parent).removeClass('success');
    var id = $(this).closest('li.media').data('file-id');
    $(parent).dmUploader('cancel', id);
    $(parent).removeClass('upload-start');
  });

  $('.files').on('click', 'button.reupload', function(evt){
    var parent = $(this).closest('.drag-and-drop-zone');
    evt.preventDefault();
    $(parent).find('.hdnData').val('');
    $(parent).removeClass('success');
    var id = $(this).closest('li.media').data('file-id');
    $(parent).dmUploader('cancel', id);
    $(parent).removeClass('upload-start');
  });


});
