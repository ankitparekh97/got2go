<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class RentalUser extends Authenticatable implements JWTSubject{
    use Notifiable;
    protected $guard = 'rental_user';
    protected $table = 'rental_user';

    protected $fillable = [
        'full_name', 'first_name','last_name', 'email', 'password', 'google_id', 'facebook_id','bio','otp_code','birthdate','is_otp_verified','birthdate','gender','phone','emergency_number','address','city','state','zip','site_language','currency','status','photo','goverment_id_doc','share_on_fb','marketing_email', 'activation_url', 'is_verified', 'is_tripper', 'tripper_status','linked_paypal_account','is_subscribed'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'rental_user_id');
    }

    public function paymentcards_user()
    {
        return $this->hasOne(PaymentCards::class,'user_id');
    }
    public function rentalUser()
    {
        return $this->belongsTo(OwnerRentalPair::class,'rental_id');
    }

    public function communication_preferences()
    {
        return $this->hasMany(CommunicationPreferences::class,'userid');
    }
}
