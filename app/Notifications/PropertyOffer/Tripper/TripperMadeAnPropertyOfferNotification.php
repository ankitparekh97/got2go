<?php

namespace App\Notifications\PropertyOffer\Tripper;

use App\Models\PropertyOffers;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class TripperMadeAnPropertyOfferNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var PropertyOffers $propertyOfferModel
     *
     * @param PropertyOffers $propertyOfferModel
     */
    public $propertyOfferModel;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PropertyOffers $propertyOfferModel)
    {
        $this->propertyOfferModel = $propertyOfferModel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }*/

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $returnArray =  [
            'propertyId' => $this->propertyOfferModel->property_id,
            'ownerId' => $this->propertyOfferModel->owner_id,
            'rentalId' => $this->propertyOfferModel->rental_id,
            'propertyOfferId' => $this->propertyOfferModel->rental_id,
        ];

        $propertyTitle = $this->propertyOfferModel->property->title;
        $rentalFullName = $this->propertyOfferModel->rental->getFullName();
        $tripperOfferStartDate = carbonParseDateFormatMDY($this->propertyOfferModel->tripper_offer_start_date);
        $tripperOfferEndDate = carbonParseDateFormatMDY($this->propertyOfferModel->tripper_offer_end_date);

        $returnArray['message'] = $rentalFullName.' created a offer on ['.$propertyTitle.'] dated [ '.$tripperOfferStartDate.'-'.$tripperOfferEndDate.' ]';

        return $returnArray;
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $propertyOffer = (new \App\Http\Resources\PropertyOffer\PropertyOffer($this->propertyOfferModel))->resolve();
        return (new BroadcastMessage(['propertyOffer' => $propertyOffer]))
            ->onQueue('broadcast-queue');
    }

    /**
     * Get the type of the notification being broadcast.
     *
     * @return string
     */
    public function broadcastType()
    {
        return 'broadcast.propertyOffer.tripper.new';
    }

    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array
     */
    public function viaQueues()
    {
        return [
            //'mail' => 'property-offer-mail-queue',
            'database' => 'database-queue',
            'broadcast' => 'broadcast-queue'
        ];
    }

}
