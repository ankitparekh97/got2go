<?php

namespace App\Notifications\PropertyOffer;

use App\Models\PropertyOffers;
use App\Services\PropertyOfferService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class BroadcastPropertyOfferAlertBarNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var PropertyOfferService $propertyOfferService
     * @var PropertyOffers $propertyOffer
     */
    public $propertyOfferService;
    public $propertyOffer;


    /**
     * Create a new notification instance.
     */
    public function __construct(PropertyOffers $propertyOffer)
    {
        $this->propertyOfferService = resolve(PropertyOfferService::class);
        $this->propertyOffer = $propertyOffer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $pendingResponseOnOfferByOwner = $this->propertyOfferService->getPendingResponseOnOfferByOwner($this->propertyOffer->owner_id);
        $pendingResponseOnOfferByTripper = $this->propertyOfferService->getPendingResponseOnOfferByTripper($this->propertyOffer->rental_id);
        return (new BroadcastMessage(['pendingResponseOnOfferByOwner' => $pendingResponseOnOfferByOwner,'pendingResponseOnOfferByTripper' => $pendingResponseOnOfferByTripper]))
            ->onQueue('broadcast-queue');
    }

    /**
     * Get the type of the notification being broadcast.
     *
     * @return string
     */
    public function broadcastType()
    {
        return 'broadcast.propertyOffer.alertBar';
    }

    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array
     */
    public function viaQueues()
    {
        return [
            'broadcast' => 'broadcast-queue'
        ];
    }

}
