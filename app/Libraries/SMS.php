<?php

namespace App\Libraries;
use Twilio\Rest\Client;

class SMS
{
    public static function sendTo($message,$recipients){
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        try {
           $message = $client->messages->create($recipients,
            ['from' => $twilio_number, 'body' => $message] );
            return ['success'=>true];
        } catch (\Exception $e) {
            // will return user to previous screen with twilio error

            return ['success'=>false,'error'=>$e->getMessage()];
        }


    }
}
