<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class OwnerTimeshareProperty extends Model
{
    // protected $guard = 'owner_timeshare_property';
     protected $table = 'owner_timeshare_property';

    protected $guarded = ['id'];

    // use Searchable;
    // protected $indexConfigurator = MyIndexConfigurator::class;

    protected $fillable = [
        'property_id', 'timeshare_description','no_of_guest','unit_size','check_in_date','check_out_date','min_days','allow_booking_before','ltd_time_frame','ltd_value','membership_number','document','structure_type'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    // public function searchableAs()
    // {
    //     return 'property';
    // }

    public function hotels()
    {
        return $this->belongsTo(Hotels::class);
    }

    // protected $mapping = [
    //     'properties' => [
    //         'timeshare_description' => [
    //             'type' => 'text',
    //             // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
    //             'fields' => [
    //                 'raw' => [
    //                     'type' => 'keyword',
    //                 ]
    //             ]
    //         ],
    //     ]
    // ];
}
