<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerRentalPair extends Model
{
    protected $guard = 'owner_rental_pair';
    protected $table = 'owner_rental_pair';

    protected $fillable = [
        'owner_id','rental_id','created_at, updated_at'
    ];

    public function ownerUser()
    {
        return $this->hasOne(Owner::class,'id','owner_id');
    }
    public function rentalUser()
    {
        return $this->hasOne(RentalUser::class,'id','rental_id');
    }

    public function messages()
    {
        return $this->hasMany(Messages::class,'pair_id');
    }

    public function lastMessages()
    {
        return $this->hasOne(Messages::class,'pair_id')->latest();
    }
   
}
