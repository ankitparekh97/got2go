<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'booking';

    protected $fillable = [
        'booking_type', 'rental_user_id', 'property_id', 'check_in_date', 'check_out_date', 'adults', 'childrens', 'rooms', 'actual_price', 'offer_amount', 'tax', 'occupancy_tax_fees', 'total', 'status', 'is_cancelled', 'cancellation_date', 'owner_status', 'property_status','confirmation_id'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }

     public function rentaluser()
    {
        return $this->belongsTo(RentalUser::class,'rental_user_id');
    }

    public function booking_payment()
    {
        return $this->hasOne(BookingPayment::class,'booking_id');
    }
}
