<?php

namespace App;
use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{

    use SoftDeletes;
    // use Searchable;
    // protected $indexConfigurator = MyIndexConfigurator::class;

    protected $table = 'property';

    protected $guarded = ['id'];

    public function ownerproperty()
    {
        return $this->hasOne(OwnerProperty::class,'property_id');
    }

    public function propertytype()
    {
        return $this->belongsTo(PropertyType::class,'property_type_id');
    }

    public function ownertimeshare()
    {
        return $this->hasOne(OwnerTimeshareProperty::class,'property_id');
    }

    public function propertyimages()
    {
        return $this->hasMany(PropertyImages::class,'property_id');
    }

    public function propertybedrooms()
    {
        return $this->hasMany(PropertyBedrooms::class,'property_id');
    }

    public function propertyamenities()
    {
        return $this->hasMany(PropertyAmenities::class,'property_id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class);
    }

    public function owner()
    {
       return $this->belongsTo(Booking::class,'property_id');
    }

    public function ownerdetails()
    {
        return $this->hasMany(Owner::class,'id','owner_id');
    }

  
    public function propertyreviews()
    {
        return $this->hasMany(PropertyReviews::class,'property_id');
    }

    // public function searchableAs()
    // {
    //     return 'property';
    // }

    // public function toSearchableArray()
    // {
    //     $array = Property::where('type_of_property','property')
    //     ->select('id','type_of_property', 'location_detail' )
    //     ->searchable(); 
    //     return  $array;
    // }

    public static function makeSearchable()
    {
        $array = Property::join('owner_property', 'owner_property.property_id','=','property.id')
        ->select('property.id','type_of_property', 'location_detail','lat_lang', 'owner_property.no_of_guest', 'city','state', 'state_abbrv','country' )
        ->where('type_of_property','property')
        // ->with('ownerproperty:no_of_guest')
        ->searchable();


        return  $array;
    }

    public static function makeSearchableById($ids)
    {
        $array = Property::join('owner_property', 'owner_property.property_id','=','property.id')
        ->select('property.id','type_of_property', 'location_detail','lat_lang', 'owner_property.no_of_guest', 'city','state', 'state_abbrv','country' )
        ->where('type_of_property','property')
        ->whereIn('property.id',$ids)
        // ->with('ownerproperty:no_of_guest')
        ->searchable();


        return  $array;
    }


    // Here you can specify a mapping for model fields
    // protected $mapping = [
    //     'properties' => [
    //         'text' => [
    //             'type' => 'text',
    //             // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
    //             'fields' => [
    //                 'lcoation_detail' => [
    //                     'type' => 'keyword',
    //                 ],
    //                 'lat_long' => [
    //                     "type"=> "geo_point"
    //                 ],
    //                 'city' => [
    //                     "type"=> "keyword"
    //                 ],
    //                 'state' => [
    //                     "type"=> "keyword"
    //                 ]
    //             ]
    //         ],
    //     ]
    // ];
}
