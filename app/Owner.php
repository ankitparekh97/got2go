<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Owner extends Authenticatable implements JWTSubject{
    use Notifiable;
    protected $guard = 'owner';
    protected $table = 'owner';

    protected $fillable = [
        'first_name','last_name', 'email', 'password', 'facebook_id','google_id','bio','otp_code','is_otp_verified','birthdate','phone','address','city','state','languages_known','currency','status','photo','unique_key', '# id, first_name, last_name, username, email, facebook_id, google_id, password, bio, birthdate, phone, address, city, state, languages_known, currency, status, photo, goverment_id_doc, created_at, updated_at, deleted_at'
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function property()
    {
        return $this->hasMany(Property::class,'owner_id');
    }

    public function ownerUser()
    {
        return $this->belongsTo(OwnerRentalPair::class,'owner_id');
    }
    
}
