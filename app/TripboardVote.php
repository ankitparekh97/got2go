<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripboardVote extends Model
{
    protected $table = 'tripboard_vote';
    protected $fillable = [
        'tripboard_id', 'property_id', 'votes', 'user_id', '# id, tripboard_id, property_id, votes, user_id, created_at, updated_at'
    ];    
}