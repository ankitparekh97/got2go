<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterAmenities extends Model
{
    protected $table = 'master_amenities';

    protected $guarded = ['id'];

    public function hotels()
    {
        return $this->belongsTo(Hotels::class);
    }
    public function propertyamenities()
    {
        return $this->hasMany(PropertyAmenities::class,'amenity_id');
    }
}
