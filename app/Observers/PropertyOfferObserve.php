<?php

namespace App\Observers;

use App\Models\PropertyOffers;
use App\Services\PropertyOfferServiceContract;

class PropertyOfferObserve
{
    public $propertyOfferService;
    public function __construct(PropertyOfferServiceContract $propertyOfferService)
    {
        $this->propertyOfferService = $propertyOfferService;
    }

    /**
     * Handle the property offers "created" event.
     *
     * @param \App\Models\PropertyOffers $propertyOffers
     * @return void
     */
    public function saved(PropertyOffers $propertyOffers)
    {
        $this->propertyOfferService->sendNotification($propertyOffers);
    }

    /**
     * Handle the property offers "created" event.
     *
     * @param  \App\Models\PropertyOffers  $propertyOffers
     * @return void
     */
    public function created(PropertyOffers $propertyOffers)
    {
        //
    }

    /**
     * Handle the property offers "updated" event.
     *
     * @param  \App\Models\PropertyOffers  $propertyOffers
     * @return void
     */
    public function updated(PropertyOffers $propertyOffers)
    {
        //
    }

    /**
     * Handle the property offers "deleted" event.
     *
     * @param  \App\Models\PropertyOffers  $propertyOffers
     * @return void
     */
    public function deleted(PropertyOffers $propertyOffers)
    {
        $this->propertyOfferService->sendNotification($propertyOffers);
    }

    /**
     * Handle the property offers "restored" event.
     *
     * @param  \App\Models\PropertyOffers  $propertyOffers
     * @return void
     */
    public function restored(PropertyOffers $propertyOffers)
    {
        //
    }

    /**
     * Handle the property offers "force deleted" event.
     *
     * @param  \App\Models\PropertyOffers  $propertyOffers
     * @return void
     */
    public function forceDeleted(PropertyOffers $propertyOffers)
    {
        //
    }
}
