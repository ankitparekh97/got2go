<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingPayment extends Model
{
    //
    protected $table = 'booking_payment';

    protected  $fillable = [ 
        'booking_id', 'rental_user_id', 'payment_type', 'transaction_number', 'amount'
    ];

    
}
