<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Hotels extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $guard = 'hotels';
    protected $table = 'hotels';

    protected $fillable = [
        'name', 'email', 'password', 'description', 'location','city','state','phone','website','property_type','check_in','check_out','private_room_accommodation','cottage_room_accommodation','occupancy_taxes_and_fees','is_partial_payment_allowed','delux_room_accommodation'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function ownertimeshare()
    {
        return $this->hasOne(OwnerTimeshareProperty::class,'hotels_id');
    }

    public function propertyamenities()
    {
        return $this->hasMany(PropertyAmenities::class,'property_id');
    }
}
