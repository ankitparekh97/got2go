<?php

use App\Helpers\GlobalConstantDeclaration;
use App\Models\PropertyOffers;
use App\Models\SubscriptionPlan;
use Illuminate\Support\Facades\Mail;
use App\Libraries\SMS;
use Illuminate\Support\Facades\DB;

if (!function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder) {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}

function getStatesWithAbbr(){
    return array(
        'AL'=> 'Alabama',
        'AK' => 'Alaska',
        'AZ'=> 'Arizona',
        'AR' => 'Arkansas',
        'CA'=> 'California',
        'CO' => 'Colorado',
        'CT'=> 'Connecticut',
        'DE' => 'Delaware',
        'DC'=> 'District Of Columbia',
        'FL' => 'Florida',
        'GA'=> 'Georgia',
        'HI' => 'Hawaii',
        'ID'=> 'Idaho',
        'IL' => 'Illinois',
        'IN'=> 'Indiana',
        'IA' => 'Iowa',
        'KS'=> 'Kansas',
        'KY' => 'Kentucky',
        'LA'=> 'Louisiana',
        'ME' => 'Maine',
        'MD'=> 'Maryland',
        'MA' => 'Massachusetts',
        'MI'=> 'Michigan',
        'MN' => 'Minnesota',
        'MS'=> 'Mississippi',
        'MO' => 'Missouri',
        'MT'=> 'Montana',
        'NE' => 'Nebraska',
        'NV'=> 'Nevada',
        'NH' => 'New Hampshire',
        'NJ'=> 'New Jersey',
        'NM' => 'New Mexico',
        'NY'=> 'New York',
        'NC' => 'North Carolina',
        'ND'=> 'North Dakota',
        'OH' => 'Ohio',
        'OK'=> 'Oklahoma',
        'OR' => 'Oregon',
        'PA'=> 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC'=> 'South Carolina',
        'SD' => 'South Dakota',
        'TN'=> 'Tennessee',
        'TX' => 'Texas',
        'UT'=> 'Utah',
        'VT' => 'Vermont',
        'VA'=> 'Virginia',
        'WA' => 'Washington',
        'WV'=> 'West Virginia',
        'WI' => 'Wisconsin',
        'WY'=> 'Wyoming',
    );
}

function getActiveClass($checkPageRoute,$returnActiveClass = 'active')
{
    if(url()->current() == $checkPageRoute){
        return $returnActiveClass;
    }

    return '';
}

function getPropertyOfferStatusMessage($propertyOfferStatus,$propertyOfferActionBy)
{
    if($propertyOfferActionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL){
        return getPropertyOfferStatusMessageForTripper($propertyOfferStatus,$propertyOfferActionBy);
    } else if($propertyOfferActionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER){
        return getPropertyOfferStatusMessageForOwner($propertyOfferStatus,$propertyOfferActionBy);
    }
}

function getStartDateAndEndDateFromPropertyOffer(PropertyOffers $propertyOfferService)
{
    $dateRangeArray = array();

    $dateRangeArray['startDate'] = $propertyOfferService->tripper_offer_start_date;
    $dateRangeArray['endDate'] = $propertyOfferService->tripper_offer_end_date;

    if(
        !empty($propertyOfferService) &&
        $propertyOfferService->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $propertyOfferService->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
    ){
        $dateRangeArray['startDate'] = $propertyOfferService->owner_offer_start_date;
        $dateRangeArray['endDate'] = $propertyOfferService->owner_offer_end_date;
    }

    return $dateRangeArray;
}

function getTripperPriceFromPropertyOffer(PropertyOffers $propertyOfferService)
{
    $tripperPrice = 0;
    if(!empty($propertyOfferService)){
        $tripperPrice = getTripperPrice($propertyOfferService->property->price);
    }

    if(
        !empty($propertyOfferService) &&
        $propertyOfferService->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $propertyOfferService->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
    ){
        $tripperPrice = $propertyOfferService->tripper_offer_nightly_price;
    } else if(
        !empty($propertyOfferService) &&
        $propertyOfferService->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $propertyOfferService->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
    ){
        $tripperPrice = $propertyOfferService->owner_offer_nightly_price;
    } else if(
        !empty($propertyOfferService) &&
        $propertyOfferService->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $propertyOfferService->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
    ){

        $tripperPrice = $propertyOfferService->owner_offer_nightly_price;
    }

    return $tripperPrice;
}

function getLiveSiteUrl()
{
    return env('APP_URL');
}

function getLiveSiteImageLogo()
{
    return getLiveSiteUrl().'/media/images/logo.svg';
}

function getPrevAnnualPlanPrice()
{

    /**
     * @var SubscriptionPlan $currentSubscriptionPlan
     */
    $currentSubscriptionPlan = SubscriptionPlan::orderBy(SubscriptionPlan::CREATED_AT,'desc')->first();

    /**
     * @var SubscriptionPlan $subscriptionPlan
     */

    $subscriptionPlan = SubscriptionPlan::where(SubscriptionPlan::AMOUNT,'>',$currentSubscriptionPlan->amount)->orderBy(SubscriptionPlan::CREATED_AT,'desc')->first();

    if($subscriptionPlan){
        return $subscriptionPlan->amount;
    }

    return 0;

}

function ordinal($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return $number. 'th';
    else
        return $number. $ends[$number % 10];
}

function getRefundAmount($totalAmount){
    return round($totalAmount*0.971) - 0.3;
}

function getRenterCleaningFee($cleaningFee){
    return round($cleaningFee + (0.1*$cleaningFee),2);
}

function getOccupancyTax($amount){
    return round($amount * 0.1225);
}

function getTransactionFee($amount){
    return round((( $amount +0.3)/(1-0.029)) - ($amount));
}

/* Random Password genrate for forgot password */
function randomPassword($len = 8) {

    //enforce min length 8
    if($len < 8)
        $len = 8;

    //define character libraries - remove ambiguous characters like iIl|1 0oO
    $sets = array();
    $sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    $sets[] = '23456789';
    $sets[]  = '!@#\$%\^\&*\)\(+=._-';

    $password = '';
    
    //append a character from each set - gets first 4 characters
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
    }

    //use all characters to fill up to $len
    while(strlen($password) < $len) {
        //get a random set
        $randomSet = $sets[array_rand($sets)];
        
        //add a random char from the random set
        $password .= $randomSet[array_rand(str_split($randomSet))]; 
    }
    
    //shuffle the password string before returning!
    return str_shuffle($password);
}

function send_email($body,$toMail,$toName,$subject,$from='no-reply@timeshare.com') {
        Mail::send([], [], function ($message) use ($toMail,$toName, $subject, $body,$from) {
          $message->to($toMail, $toName);
          $message->subject($subject);
          $message->setBody($body, 'text/html');
          $message->from($from, 'Timeshare');
        });
       
}

//Single function for user verification Mail sent OWNER?RENTER
function sendUserEmail($email,$otp)
    {
        // Registration email to user
        $toMail = $email;
        $toName = '';
        $emailRecord = DB::table('email_template')->where('task','owner_registration')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body);
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', getLiveSiteUrl(), $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', getLiveSiteImageLogo() , $body);
        $body = str_replace('{otp}', $otp, $body);
        $body = str_replace('{email}', $email , $body);
        $body = str_replace('{user_name}', ", " .ucfirst('user'), $body);        

        send_email($body,$toMail,$toName=null,$emailRecord->subject,$emailRecord->from_address);
}

function sendUserMessage($phone,$otp)
    {
        // Registration email to user
        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        $number = $phone;
        $messgae = "your verification code for Gotgo: ".$otp;
        // Send OTP to user's registered contact number
        SMS::sendTo($messgae,$number);
    }
