<?php

use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use Illuminate\Support\Facades\Session;

function getOwner()
{
    return Auth::guard('owner')->user();
}

function getJwtToken()
{
    return JWTAuth::getToken();
}

function getOwnerId()
{
    return getOwner()->id;
}

function getOwnerPropertyCount(){
    $property = '';
    if(!empty(Session::get('user'))){
        $property = Property::where('owner_id', Session::get('user')['id'])->get();
    }

    return $property;
}

function getPropertyOfferStatusMessageForOwner(
    $status,
    $actionBy
)
{
    if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING
    ) {
        return 'Tripper Is Waiting For Your Response';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
    ) {
        return 'Your Offer Was Accepted By Tripper!';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED
    ) {
        return 'Your Offer Was Declined By Tripper!';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
    ) {
        return 'Your\'ve Accepted Tripper\'s Offer';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED
    ) {
        return 'Your\'ve Rejected Tripper\'s Offer';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
    ) {
        return 'Your Counter Offer Is Pending On Tripper';
    }

}

function getPropertyImageLink($propertyImage){
    return asset("/uploads/property/{$propertyImage}");
}
