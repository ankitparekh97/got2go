<?php


namespace App\Helpers;


use App\Models\Configuration;
use App\Models\Property;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class Helper
{
    public static function getConfig(){
        return Configuration::first();
   }

    public function saveConfig(){
        $config = new Configuration();
    }

    public static function urlsafe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public static function urlsafe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public static function convertToLocal($date,$timezone){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $date, 'UTC');   // specify UTC otherwise defaults to locale time zone as per ini setting
        $carbon->tz = $timezone;   // ... set to the current users timezone
        // /dd($carbon);
        return $carbon;  // output in users timezone
    }
    public static function includeRouteFiles($folder) {
        try {
            $rdi = new \recursiveDirectoryIterator($folder);
            $it = new \recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (!$it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function formatMoney($number, $cents = 1, $withPrefixCurrency = true)
    { // cents: 0=never, 1=if needed, 2=always
        if (is_numeric($number)) { // a number
            if (!$number) { // zero
                $money = ($cents == 2 ? '0.00' : '0'); // output zero
            } else { // value
                if (floor($number) == $number) { // whole number
                    $money = number_format($number, ($cents == 2 ? 2 : 0)); // format
                } else { // cents
                    $money = number_format(round($number, 2), ($cents == 0 ? 0 : 2)); // format
                } // integer or decimal
            } // value

            if($withPrefixCurrency)
            {
                return '$' . $money;
            }

            return $money;
        } // numeric
    } // formatMoney

    public static function getRecentlyViewed(){
        $recentlyViewed = '';
        if(Session::get('recentlyViewed')!=''){
            $recentlyViewed = Session::get('recentlyViewed');
            $property = Property::with('propertytype')->whereIn('id',$recentlyViewed)->orderBy('id','desc')->get();
        } else {
            $property = array();
        }
        return $property;
    }

}
