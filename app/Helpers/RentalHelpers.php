<?php

use App\Helpers\GlobalConstantDeclaration;
use App\Models\RentalUserSubscription;
use App\Models\Subscription;

function getLoggedInRental()
{
    return Auth::guard('rentaluser')->user();
}

function isLoggedInUserIsTripper()
{
    return getLoggedInRental()->is_tripper;
}

function getLoggedInRentalId()
{
    return getLoggedInRental()->id;
}

function checkLoggedInAndGetRentalUserId()
{
    if(!empty(Auth::guard('rentaluser')->user())){
        return  getLoggedInRentalId();
    }

    return null;
}

/**
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
function getRegisterTripper()
{
    return Auth::guard('rentaluser')->user();
}

function getRegisterTripperId()
{
    return Auth::guard('rentaluser')->user()->id;
}

function isUserIsSubscribed()
{
    return Auth::guard('rentaluser')->user()->is_subscribed;
}

function isUserOtpVerified()
{
    return getLoggedInRental()->is_otp_verified;
}

function getPropertyOfferStatusMessageForTripper(
    $status,
    $actionBy
)
{
    if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING
    ) {
        return 'Pending Owner\'s Response';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
    ) {
        return 'Your Offer Was Accepted';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED
    ) {
        return 'Your Offer Was Declined';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
    ) {
        return 'Owner Is Waiting For Your Response';
    } else if(
        $actionBy === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL &&
        $status == GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
    ) {
        return 'You\'ve Accepted Owner\'s Counter Offer';
    }

}

function getTripperPrice($standardPrice)
{
    $discount = Subscription::first()->discount_percentage;
    return round($standardPrice - (($discount / 100) * $standardPrice),2);
}

function getLoggedInUserSubscription()
{
    return getLoggedInRental()->subscription;
}

function getLoggedUserSubcribeDate()
{
    $allSubscriptions =  getLoggedInRental()->allSubscriptions;
    if(!empty($allSubscriptions)){
        /**
         * @var RentalUserSubscription $allSubscription
         */
        $allSubscription = $allSubscriptions->sortByDesc('id')->first();

        if(!empty($allSubscription)){
            return $allSubscription->billing_period_start_date;
        }
    }
    return '';
}

function getTripperAllSavedAmountTill()
{
    /**
     * @var \App\Models\RentalUser $loggedInRental
     */
    $loggedInRental = getLoggedInRental();

    /**
     * @var RentalUserSubscription $currentSubscription
     */
    $currentSubscription = getLoggedInUserSubscription();
    $bookingCount = 0;
    if(!empty($currentSubscription)){
        $subscriptionStartDate = $currentSubscription->created_at;
        $subscriptionEndDate = carbonParseDate($currentSubscription->created_at)->addYear();

        /**
         * @var \App\Models\BookingPayment $bookingPayment
         */
        $bookingPayment =  $loggedInRental->load(['booking' => function ($bookingPaymentsQry) use($subscriptionStartDate,$subscriptionEndDate) {
            /**
             * @var \App\Models\Booking $bookingPaymentsQry
             */
            $bookingPaymentsQry->checkStartDateEndDateBetweenDates($subscriptionStartDate,$subscriptionEndDate);
            $bookingPaymentsQry->whereNotIn(\App\Models\Booking::STATUS,['decline','refunded']);
            $bookingPaymentsQry->whereNotIn(\App\Models\Booking::OWNER_STATUS,['cancelled','rejected', 'auto_reject']);
            $bookingPaymentsQry->has('bookingPayment');
        }]);


        if(!empty($bookingPayment->booking)){
            $bookingCount = $bookingPayment->booking->sum(\App\Models\Booking::TRIPPER_SAVING_AMOUNT);
        }
    }
    return $bookingCount;
}

function getPayoutFee($value)
{
    return round(($value * 0.25) / 100);
}
