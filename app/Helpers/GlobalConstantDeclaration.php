<?php


namespace App\Helpers;


class GlobalConstantDeclaration
{
    public const ORDER_BY_ASC = 'asc';
    public const ORDER_BY_DESC = 'desc';

    public const VALUE_TRUE = 1;
    public const VALUE_FALSE = 0;

    public const PROPERTY_OFFER_ACTION_BY_RENTAL = 'rental';
    public const PROPERTY_OFFER_ACTION_BY_OWNER = 'owner';
    public const PROPERTY_OFFER_ACTION_BY_SYSTEM = 'system';

    public const PROPERTY_OFFER_STATUS_PENDING = 'pending';
    public const PROPERTY_OFFER_STATUS_ACCEPTED = 'accepted';
    public const PROPERTY_OFFER_STATUS_REJECTED = 'rejected';
    public const PROPERTY_OFFER_STATUS_COUNTER_OFFER = 'counter_offer';
    public const PROPERTY_OFFER_STATUS_CANCEL = 'cancel';

    public const RENTAL_TRIPPER_STATUS_ACTIVE = 'active';
    public const RENTAL_TRIPPER_STATUS_PAUSED = 'paused';

    public const PLAN_TYPE_YEARLY = 'year';
    public const PLAN_TYPE_MONTHLY = 'month';
    public const PLAN_TYPE_WEEKLY = 'week';
    public const PLAN_TYPE_DAILY = 'day';

    public const PROPERTY_STATUS_PENDING = 'pending';
    public const PROPERTY_STATUS_DRAFT = 'draft';
    public const PROPERTY_STATUS_REJECTED = 'rejected';
    public const PROPERTY_STATUS_APPROVED = 'approved';

    public const BOOKING_STATUS_PENDING = 'pending';
    public const BOOKING_STATUS_REJECTED = 'rejected';
    public const BOOKING_STATUS_AUTO_REJECTED = 'auto_reject';
    public const BOOKING_STATUS_CANCELLED = 'cancelled';
    public const BOOKING_STATUS_APPROVED = 'approved';
    public const BOOKING_STATUS_CONFIRMED = 'confirmed';
    public const BOOKING_STATUS_INSTANT_BOOKING = 'instant_booking';

    public const BOOKING_STATUS_REFUNDED = 'refunded';
    public const BOOKING_STATUS_COMPLETED = 'completed';

    public const STATUS_FAILED = 'failed';
    public const STATUS_COMPLETED = 'completed';

    public const BOOKING_TYPE_PROPERTY = 'property';
    public const BOOKING_TYPE_VACATION_RENTAL = 'vacation_rental';

    public const TYPE_BANK_ACCOUNT = '1';
    public const TYPE_CARD = '2';

    public const TOTAL = 'total';
}
