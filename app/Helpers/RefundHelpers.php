<?php

use DateTime as DateTime;
use Carbon\Carbon;

/*Calculation of refund for renter/tripper */
    function calculateRenterRefund($checkInDate,$days,$startingPrice,$cancellationType,$paymentCreatedDate,$cleanningFee,$totalAmount,$transactionFee) {
       
        $now = Carbon::now('UTC');
        $refundAmount = 0;
        $hourdiff = round((strtotime($checkInDate) - strtotime($now))/3600, 1);
        $paymenthourdiff = round((strtotime($now) - strtotime($paymentCreatedDate))/3600, 1);
        //For flexible
        if($cancellationType == 'Flexible'){

            if($hourdiff >= 24){
                $refundAmount = $totalAmount - $transactionFee;
            }else if($hourdiff >= 0 && $hourdiff <= 24){
                $startingPrice = $startingPrice + $cleanningFee;
                $refundAmount = $startingPrice - (0.5*$startingPrice);
            }

        }else if($cancellationType == 'Moderate'){

            if($hourdiff >= 120){
                $refundAmount = $totalAmount - $transactionFee;
            }else if($hourdiff >= 0 && $hourdiff <= 120){
                $perNightPrice = $startingPrice / $days;
                $startingPrice = $startingPrice - $perNightPrice;
                 $startingPrice = $startingPrice + $cleanningFee;
                $refundAmount = ($startingPrice - (0.5*$startingPrice));
            }

        }else if($cancellationType == 'Strict'){

            if($paymenthourdiff > 0 && $paymenthourdiff <= 48 && $hourdiff >= 336){
                    $refundAmount = $totalAmount - $transactionFee;
            }else if($paymenthourdiff > 48 && $hourdiff >= 336){
                $startingPrice = $startingPrice + $cleanningFee;
                $refundAmount = $startingPrice - (0.5*$startingPrice);
            }else if($hourdiff <= 336 && $hourdiff >= 0){
                $startingPrice = $startingPrice + $cleanningFee;
                $refundAmount = $startingPrice - (0.5*$startingPrice);
            }

        }
        return $refundAmount;

   }

   /* Calculate Refund for owner */
   function calculateOwnerRefund($checkInDate,$days,$startingPrice,$paymentCreatedDate,$totalAmount) {
       
        $now = Carbon::now('UTC');
        $refundAmount = 0;
        $hourdiff = round((strtotime($checkInDate) - strtotime($now))/3600, 1);
        $paymenthourdiff = round((strtotime($paymentCreatedDate) - strtotime($now))/3600, 1);


        if($hourdiff >= 48){
                $refundAmount = $startingPrice;

        }else if($hourdiff < 48 ){

            $refundAmount = 0;

        }
        return $refundAmount;

    }

    function cancellationText($bookingDetails,$paymnetDate){
        $bookingChcekInDate = $bookingDetails->check_in_date.' '.date("H:i:s", strtotime($bookingDetails->check_in));
        $checkInDate = carbonParseDate($bookingChcekInDate,'America/Chicago');
        $now = Carbon::now('UTC');
        $hourdiff = round((strtotime($checkInDate) - strtotime($now))/3600, 1);
        $paymenthourdiff = 0;
        if($paymnetDate){
            $paymenthourdiff = round((strtotime($paymnetDate) - strtotime($now))/3600, 1);
        }


        $dynamicText = "No Free Cancellation";

        if($bookingDetails->cancellation_type == 'Flexible'){
            $date = new DateTime($bookingDetails->check_in_date);
            $date->modify('-1 day');
            if($hourdiff >= 24){
                $dynamicText = "Free Cancellation untill ".$bookingDetails->check_in." on ".$date->format('M jS, Y');
            }

        }else if($bookingDetails->cancellation_type == 'Moderate'){
            $date = new DateTime($bookingDetails->check_in_date);
            $date->modify('-5 day');
            if($hourdiff >= 120){
                $dynamicText = "Free Cancellation untill ".$bookingDetails->check_in." on ".$date->format('M jS, Y');
            }

        }else if($bookingDetails->cancellation_type == 'Strict'){
            $date = new DateTime($bookingDetails->check_in_date);
            $date->modify('-14 day');
            if($paymenthourdiff > 0 && $paymenthourdiff <= 48 && $hourdiff >= 336){
                    $dynamicText = "Free Cancellation untill ".$bookingDetails->check_in." on ".$date->format('M jS, Y');
            }else if($hourdiff >= 336){
                    $dynamicText = "Free Cancellation untill ".$bookingDetails->check_in." on ".$date->format('M jS, Y');
            }

        }
        return $dynamicText;
   }
