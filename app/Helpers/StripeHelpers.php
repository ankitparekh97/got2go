<?php

use App\Models\RentalUser;
use \App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;

function convertRentalModelInToStripeCreateCustomerDTO(RentalUser  $rentalUser) {

    $customerReqDTO = new CustomerRequestDTO();
    $customerReqDTO->name = $rentalUser->full_name;
    $customerReqDTO->email = $rentalUser->email;
    $customerReqDTO->phone = $rentalUser->phone;
    //$customerReqDTO->source = $rentalUser->phone;

    return $customerReqDTO;
}

function generateRandomAlphaNumeric($prefix) {
    return uniqid($prefix);
}

function stripeErrorMessage($th){
    if($th->getErrorCode() == 'card_declined'){
        $error = "We could not process your payment at this time. Please try again in few minutes.";
    }else{
        $error = $th->getMessage();
    }
    return $error;
}
