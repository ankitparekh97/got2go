<?php

use Illuminate\Support\Carbon;

/**
 * @param $date
 * @return Carbon
 */
function carbonParseDate($date,$timezone = ''){

    $carbonDate =  Carbon::parse($date);

    if(!empty($timezone)) {
        $carbonDateF =  Carbon::parse($date)->format('Y-m-d H:i:s');
        $carbonDate = Carbon::createFromFormat('Y-m-d H:i:s', $carbonDateF, $timezone);   // specify UTC otherwise defaults to locale time zone as per ini setting
        $carbonDate->tz = 'UTC';   // ... set to the current users timezone
    }
    return $carbonDate;
}

function carbonParseDateFormatYMD($date){
      return Carbon::parse($date)->format('Y-m-d');
}

function carbonParseDateFormatMDY($date){
    return Carbon::parse($date)->format('m/d/Y');
}

function carbonParseDateFormatMDYWithComma($date){
    return Carbon::parse($date)->format('M d, Y');
}

function carbonParseDateFormatGIA($date){
    return Carbon::parse($date)->format('g:i A');
}

function carbonGetTomorrow(){
    return Carbon::tomorrow();
}

function carbonDateDiffInDays($startDate,$endDate){
    $startDate = carbonParseDate($startDate);
    $endDate = carbonParseDate($endDate);

    return $startDate->diffInDays($endDate);
}

function carbonGetYesterDay(){
    return Carbon::yesterday();
}

function carbonGetCurrentYear(){
    return Carbon::now()->isoFormat('YYYY');
}

function carbonDateDiffInHours($startDate,$endDate){
    $startDate = carbonParseDate($startDate);
    $endDate = carbonParseDate($endDate);

    return $startDate->diffInHours($endDate);
}
function carbonDateDiffInMinutes($startDate,$endDate){
    $startDate = carbonParseDate($startDate);
    $endDate = carbonParseDate($endDate);

    return $startDate->diffInMinutes($endDate);
}

function carbonDateDiffInHrs($startDate,$endDate){
    $from = carbonParseDate($startDate);
    $to = carbonParseDate($endDate);
    $days = carbonDateDiffInDays($from,$to);
    $hours = carbonDateDiffInDays($from,$to);
    $minutes = carbonDateDiffInDays($from,$to);
    return $diffInHrs = $days * 24 + $hours.':'.$minutes;
}

function carbonParseDateFormatMDYWithHIA($date){
    return Carbon::parse($date)->format('m/d/Y h:i A');
}

function carbonParseDateFormatFDY($date){
    return Carbon::parse($date)->format('F d, Y');
}

