<?php

function getCardProductName($productId){
    return config('braintree.braintreeCardProduct.'.$productId);
}

function getCardCvvResponseCode($cvvResponseCode){
    return config('braintree.cvvResponseCode.'.$cvvResponseCode);
}

/**
 * @return \Illuminate\Contracts\Foundation\Application|mixed
 */
function getObjectBraintree()
{
    return app(\App\Services\PaymentServices\Braintree\BraintreePaymentService::class);
}

function getBrainTreeClientToken()
{
    return app(\App\Services\PaymentServices\Braintree\BraintreePaymentService::class)->getClientToken();
}
