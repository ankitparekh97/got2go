<?php


namespace App\Services;

use App\Models\Property;
use App\Http\DTO\Api\RentalUserDTO;

interface RegisterServiceContract
{
    public function createUser($firstName,$lastName,$password,$tripper);
}
