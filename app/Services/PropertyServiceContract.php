<?php


namespace App\Services;

use App\Http\DTO\Api\AmenitiesDTO;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Http\DTO\Api\HotelsDTO;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use Illuminate\Support\Facades\Request;

interface PropertyServiceContract
{
    public function getListings($ownerId,$status);

    public function findPropertyById($id);

    public function getPropertyByUniqueKey($uniqueId);

    public function getById($id);

    public function getOwnerById($id);

    public function getByParamsObject($params);

    public function getByParamsCollection($params);

    public function getHotelsByParamsObject($params);

    public function getHotelsByParamsCollection($params);

    public function getPropertyTypes();

    public function getPropertyTypesByParams();

    public function getMasterAmenities();

    public function getMasterAmenitiesByArray();

    public function getHotels();

    public function publishProperty($id);

    public function deleteProperty($id);

    public function saveProperty(PropertyDTO $propertyDTO);

    public function savepropertyBedRooms(PropertyBedroomsDTO $propertyBedroomsDTO,$totalBeds,$propertyId);

    public function saveHotel(HotelsDTO $hotelsDTO);

    public function saveAmenities(AmenitiesDTO $amenitiesDTO,$property);

    public function saveAvaiblities(Request $request,$property);

    public function savePropertyMediaDocuments($request);

    public function deleteDocuments($request);
}
