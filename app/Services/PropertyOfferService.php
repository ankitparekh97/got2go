<?php


namespace App\Services;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\MakeOfferDTO;
use App\Models\DTO\PropertyOfferDTO;
use App\Models\PropertyOffers;
use App\Notifications\PropertyOffer\BroadcastPropertyOfferAlertBarNotification;
use App\Notifications\PropertyOffer\Owner\OwnerAcceptedPropertyOfferNotification;
use App\Notifications\PropertyOffer\Owner\OwnerCancelledPropertyOfferNotification;
use App\Notifications\PropertyOffer\Owner\OwnerCounteredPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperAcceptedAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperCancelledAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperDeletedAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnCounterPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferOnExistingOfferNotification;
use App\Repositories\PropertyOfferInterface;

class PropertyOfferService implements PropertyOfferServiceContract
{
    protected $propertyRepository;
    protected $propertyOfferRepository;

    public function __construct(
        PropertyOfferInterface $propertyOfferRepository
    )
    {
        $this->propertyOfferRepository = $propertyOfferRepository;
    }

    public function tipperMakeAnOffer()
    {
        // TODO: Implement tipperMakeAnOffer() method.
    }

    public function getPropertyOfferById($propertyOfferId)
    {
        return $this->propertyOfferRepository->getPropertyOfferById($propertyOfferId);
    }

    ## Owner
    public function getOwnerPropertiesOffer($ownerId)
    {
        return $this->propertyOfferRepository->getPropertyOffersByUserId(
            $ownerId,
            GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER
        );
    }

    public function acceptOwnerPropertyOffer($propertyOfferId)
    {
        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;

    }

    public function declineOwnerPropertyOffer($propertyOfferId)
    {
        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;

    }

    /**
     * @param MakeOfferDTO $requestDTO
     * @param $propertyOfferId
     * @return mixed
     */
    public function ownerCounterOfferProperty(MakeOfferDTO $requestDTO, $propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyOfferModel
         */
        $propertyOfferModel = $this->propertyOfferRepository->getPropertyOfferById($propertyOfferId);

        /**
         * @var PropertyOffers $checkOfferIsOverlappingWithOtherOffer
         */
        $checkOfferIsOverlappingWithOtherOffer = $this->propertyOfferRepository->checkOfferDateIsOverLappingByTripperIdAndOtherOffer($requestDTO->ownerOfferStartDate,$requestDTO->ownerOfferEndDate,$propertyOfferModel->rental_id,$propertyOfferModel->property_id,$propertyOfferId);

        if($checkOfferIsOverlappingWithOtherOffer->count() > 1){
            throw new \Exception('It seems that same Tripper\'s offer dates is overlapping with his/her other existing offer');
        }

        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;

        $propertyOfferDTO->ownerOfferStartDate = $requestDTO->ownerOfferStartDate;
        $propertyOfferDTO->ownerOfferEndDate = $requestDTO->ownerOfferEndDate;
        $propertyOfferDTO->ownerOfferProposedTotal = $requestDTO->ownerOfferProposedTotal;

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER;

        $numberOfNights = carbonDateDiffInDays($propertyOfferDTO->ownerOfferStartDate,$propertyOfferDTO->ownerOfferEndDate);
        if($numberOfNights > 0){
            $propertyOfferDTO->ownerOfferNightlyPrice = round($propertyOfferDTO->ownerOfferProposedTotal/$numberOfNights,2);
        }

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;
    }


    public function cancelOwnerOPropertyOffer($propertyOfferId) {

        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_CANCEL;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;

    }

    ## End Owner Section

    ## Tripper
    public function getTripperPropertiesOffer($tripperId) {
        return $this->propertyOfferRepository->getPropertyOffersByUserId(
            $tripperId,
            GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL
        );
    }

    public function acceptTripperPropertyOffer($propertyOfferId,$bookingId) {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = $this->propertyOfferRepository->getPropertyOfferById($propertyOfferId);

        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->bookingId = $bookingId;
        $getCheckInCheckoutDate = getStartDateAndEndDateFromPropertyOffer($propertyOffer);

        $propertyOfferDTO->finalAcceptedStartDate = $getCheckInCheckoutDate['startDate'];
        $propertyOfferDTO->finalAcceptedEndDate = $getCheckInCheckoutDate['endDate'];
        $propertyOfferDTO->finalAcceptedNightlyPrice = getTripperPriceFromPropertyOffer($propertyOffer);

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        $this->propertyOfferRepository->softDeleteOtherOverLappingOffers($savedPropertyOffer->id);

        return $savedPropertyOffer;
    }
    ## End Tripper Section

    /**
     * @param $propertyOfferId
     * @return mixed
     */
    public function declineTripperPropertyOffer($propertyOfferId)
    {
        // TODO: Implement declineTripperPropertyOffer() method.
    }

    /**
     * @param MakeOfferDTO $requestDTO
     * @param $propertyOfferId
     * @return mixed
     */
    public function tripperCounterOfferProperty(MakeOfferDTO $requestDTO,$propertyOfferId)
    {

        /**
         * @var PropertyOffers $propertyOfferModel
         */
        $propertyOfferModel = $this->propertyOfferRepository->getPropertyOfferById($propertyOfferId);

        /**
         * @var PropertyOffers $checkOfferIsOverlappingWithOtherOffer
         */
        $checkOfferIsOverlappingWithOtherOffer = $this->propertyOfferRepository->checkOfferDateIsOverLappingByTripperIdAndOtherOffer($requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate,$propertyOfferModel->rental_id,$propertyOfferModel->property_id,$propertyOfferId);

        if($checkOfferIsOverlappingWithOtherOffer->count() > 1){
            throw new \Exception('It seems that you have a offer whose dates is overlapping with requested dates');
        }

        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->tripperOfferStartDate = $requestDTO->tripperOfferStartDate;
        $propertyOfferDTO->tripperOfferEndDate = $requestDTO->tripperOfferEndDate;

        $propertyOfferDTO->tripperOfferProposedTotal = $requestDTO->tripperOfferProposedTotal;
        $numberOfNights = carbonDateDiffInDays($propertyOfferDTO->tripperOfferStartDate,$propertyOfferDTO->tripperOfferEndDate);
        if($numberOfNights > 0){
            $propertyOfferDTO->tripperOfferNightlyPrice = round($propertyOfferDTO->tripperOfferProposedTotal/$numberOfNights,2);
        }
        $propertyOfferDTO->flexibleDates = $requestDTO->flexibleDates;

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER;
        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;

    }

    /**
     * @param MakeOfferDTO $requestDTO
     * @param  $propertyOfferId
     * @return mixed
     */
    public function tripperMakeAnOfferProperty(MakeOfferDTO $requestDTO)
    {
        $propertyOfferDTO = new PropertyOfferDTO;

        if($requestDTO->id){
            $propertyOfferDTO->id = $requestDTO->id;
        }
        $propertyOfferDTO->propertyId = $requestDTO->propertyId;
        $propertyOfferDTO->ownerId = $requestDTO->ownerId;
        $propertyOfferDTO->rentalId = $requestDTO->rentalId;

        $propertyOfferDTO->numberOfGuests = $requestDTO->numberOfGuests;

        $propertyOfferDTO->tripperOfferStartDate = $requestDTO->tripperOfferStartDate;
        $propertyOfferDTO->tripperOfferEndDate = $requestDTO->tripperOfferEndDate;

        $numberOfNights = carbonDateDiffInDays($propertyOfferDTO->tripperOfferStartDate,$propertyOfferDTO->tripperOfferEndDate);
        if($requestDTO->tripperOfferProposedTotal) {
            $propertyOfferDTO->tripperOfferProposedTotal = $requestDTO->tripperOfferProposedTotal;
            if($numberOfNights > 0){
                $propertyOfferDTO->tripperOfferNightlyPrice = round($propertyOfferDTO->tripperOfferProposedTotal/$numberOfNights,2);
            }
        } else {
            $propertyOfferDTO->tripperOfferNightlyPrice = $requestDTO->tripperOfferNightlyPrice;
            if($numberOfNights > 0){
                $propertyOfferDTO->tripperOfferProposedTotal = $requestDTO->tripperOfferNightlyPrice * $numberOfNights;
            }
        }

        $propertyOfferDTO->flexibleDates = $requestDTO->flexibleDates ?? 0;

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;
    }

    public function tripperMakeAnOfferOnExistingOffer(MakeOfferDTO $requestDTO,$propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyOfferModel
         */
        $propertyOfferModel = $this->propertyOfferRepository->getPropertyOfferById($propertyOfferId);

        /**
         * @var PropertyOffers $checkOfferIsOverlappingWithOtherOffer
         */
        $checkOfferIsOverlappingWithOtherOffer = $this->propertyOfferRepository->checkOfferDateIsOverLappingByTripperIdAndOtherOffer($requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate,$propertyOfferModel->rental_id,$propertyOfferModel->property_id,$propertyOfferId);

        if($checkOfferIsOverlappingWithOtherOffer->count() > 1){
            throw new \Exception('There is already an offer pending between the above selected dates.');
        }

        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->tripperOfferStartDate = $requestDTO->tripperOfferStartDate;
        $propertyOfferDTO->tripperOfferEndDate = $requestDTO->tripperOfferEndDate;

        $propertyOfferDTO->tripperOfferProposedTotal = $requestDTO->tripperOfferProposedTotal;
        $numberOfNights = carbonDateDiffInDays($propertyOfferDTO->tripperOfferStartDate,$propertyOfferDTO->tripperOfferEndDate);
        if($numberOfNights > 0){
            $propertyOfferDTO->tripperOfferNightlyPrice = round($propertyOfferDTO->tripperOfferProposedTotal/$numberOfNights,2);
        }
        $propertyOfferDTO->flexibleDates = $requestDTO->flexibleDates;

        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING;
        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;
    }

    /**
     * @param MakeOfferDTO $requestDTO
     * @return mixed
     */
    public function deleteTripperPropertyOffer(MakeOfferDTO $requestDTO)
    {
        return $this->propertyOfferRepository->softDeleteOfferById($requestDTO->id);
    }

    /**
     * @param $propertyOfferId
     * @return mixed
     */
    public function cancelTripperPropertyOffer($propertyOfferId)
    {

        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOfferDTO = new PropertyOfferDTO;
        $propertyOfferDTO->id = $propertyOfferId;
        $propertyOfferDTO->actionBy = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOfferDTO->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_CANCEL;

        $savedPropertyOffer = $this->propertyOfferRepository->savePropertyOffer($propertyOfferDTO);

        return $savedPropertyOffer;

    }

    public function checkOfferDateIsOverLappingByTripperId($startDate,$endDate,$tripperId,$propertyId)
    {
        return $this->propertyOfferRepository->checkOfferDateIsOverLappingByTripperId($startDate,$endDate,$tripperId,$propertyId);
    }

    public function sendNotification(PropertyOffers $propertyOffers)
    {
        $propertyOffers->load(['property.propertyimages','booking']);
        $propertyOffers->load('rental');
        $rentalUser = $propertyOffers->rental;
        $propertyOffers->load('owner');
        $owner = $propertyOffers->owner;

        if($propertyOffers->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER){
            if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
            ){
                $rentalUser->notify(new OwnerAcceptedPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED
            ){
                $rentalUser->notify(new OwnerCancelledPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_CANCEL
            ){
                $rentalUser->notify(new OwnerCancelledPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
            ){
                $rentalUser->notify(new OwnerCounteredPropertyOfferNotification($propertyOffers));
            }

        }

        if($propertyOffers->action_by === GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL){
            if($propertyOffers->trashed()) {
                $owner->notify(new TripperDeletedAnPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING
            ){
                if($propertyOffers->wasRecentlyCreated === true){
                    $owner->notify(new TripperMadeAnPropertyOfferNotification($propertyOffers));
                } else {
                    $owner->notify(new TripperMadeAnPropertyOfferOnExistingOfferNotification($propertyOffers));
                }
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER
            ){
                $owner->notify(new TripperMadeAnCounterPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_CANCEL
            ){
                $owner->notify(new TripperCancelledAnPropertyOfferNotification($propertyOffers));
            } else if(
                $propertyOffers->status === GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED
            ){
                $owner->notify(new TripperAcceptedAnPropertyOfferNotification($propertyOffers));
            }
        }

        $owner->notify(new BroadcastPropertyOfferAlertBarNotification($propertyOffers));
        $rentalUser->notify(new BroadcastPropertyOfferAlertBarNotification($propertyOffers));
    }

    public function getPendingResponseOnOfferByOwner($ownerId){
        $pendingResponseOnOfferByOwner = $this->propertyOfferRepository->getPendingResponseOnOfferByOwner($ownerId);
        return $pendingResponseOnOfferByOwner->count();
    }

    public function getPendingResponseOnOfferByTripper($tripperId){
        $pendingResponseOnOfferByTripper = $this->propertyOfferRepository->getPendingResponseOnOfferByTripper($tripperId);
        return $pendingResponseOnOfferByTripper->count();
    }

}
