<?php

namespace App\Services;

interface SearchContract
{
    public function generateQuery($input);

    public function selectDataFromQuery($input, $isRentalUser=false);

    public function searchProperty($input, $isRentalUser=false);

    public function searchPropertyCount($input);

    public function getHotDeals($input);

    public function getHotDealsByLimit();
}
