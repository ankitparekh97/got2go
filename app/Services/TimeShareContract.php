<?php
namespace App\Services;

interface TimeShareContract
{
    public function timeShareList();

    public function bulkTimeshareList();

    public function propertyApprove($input);

    public function viewDocument($type,$id);

    public function propertyApproveAll($input);
}
