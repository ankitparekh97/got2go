<?php


namespace App\Services;


use App\Models\Booking;
use App\Models\BookingPaymentOwner;
use Illuminate\Support\Carbon;
use App\Services\PaymentServices\Stripe\Transfer\TransferRequestDTO;
use App\Services\PaymentServices\Stripe\PaymentService;

class TransferPaymentService implements TransferPaymentServiceContract
{

    public function makeTransfer(Carbon $date)
    {

        $bookings = Booking::with('rentaluser','property','property.ownerdetails')->whereDate('check_out_date',$date)->get();

        foreach($bookings as $booking){
            $serviceFee = ($booking->rentaluser->is_tripper == '1')? 0 :$booking->tax;
            $amount = $booking->total - $serviceFee;

            $transferRequestDTO = new TransferRequestDTO();
            $transferRequestDTO->amount =$amount;
            $transferRequestDTO->currency = 'usd';
            $transferRequestDTO->destination = $booking->property->ownerdetails->stripe_account_id;

            //dd($transferRequestDTO);
            $PaymentService = new PaymentService();
            $transfer = $PaymentService->createTransfer($transferRequestDTO);

            BookingPaymentOwner::create([
                'booking_id' => $booking->id,
                'owner_id' => $booking->property->owner_id,
                'payment_type' => 'completed',
                'transaction_number' => $transfer['id'],
                'amount' => $amount,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }


}
