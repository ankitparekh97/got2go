<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Repositories\RentalInterface;
use App\Repositories\OwnerInterface;
use App\Repositories\AdminInterface;
use App\Repositories\PaymentCardInterface;
use App\Repositories\CommunicationPreferencesInterface;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\RentalUserServiceContract;
use Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Hash;


use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;



class RegisterService implements RegisterServiceContract
{
    
    
    protected $bookingRepository;
    protected $propertyRepository;
    

    public function __construct(
        RentalInterface $rentalRepository,
        PaymentCardInterface $paymentCardRepository,
        OwnerInterface $ownerRepository,
        AdminInterface $adminRepository,
        RentalUserServiceContract $rentalUserService,
        PaymentServiceInterface  $paymentServiceInterface,
        CommunicationPreferencesInterface  $communicationPreferencesRepository      
    )
    {
        $this->rentalRepository = $rentalRepository;
        $this->paymentCardRepository = $paymentCardRepository;
        $this->ownerRepository = $ownerRepository;
        $this->adminRepository = $adminRepository;
        $this->paymentService = $paymentServiceInterface;
        $this->rentalUserService = $rentalUserService;
        $this->communicationPreferencesRepository = $communicationPreferencesRepository;
    }

    public function createRentalTripperUser($email,$phone){
        $otp =  rand(10000,99999);
        
        $rentalUser = $this->rentalRepository->checkUser($email,$phone);
        $encryptedId = ($rentalUser!='' ? encrypt($rentalUser->id) : '');

        if(Session::has('userOtp')== true){
            session()->forget('userOtp');
        }
        if(Session::has('userEmail')== true){
            session()->forget('userEmail');
        }
        if(Session::has('userPhone')== true){
            session()->forget('userPhone');
        }
        if(Session::has('otpTime')== true){
            session()->forget('otpTime');
        }

        if($rentalUser == ''){
            Session::put('userOtp',encrypt($otp));
            Session::put('userEmail',$email);
            Session::put('userPhone',$phone);
            Session::put('otpTime',Carbon::now());
            Session::save();
            if($email){
                sendUserEmail($email,$otp);
            }
            if($phone){
                $phone = preg_replace('/\D+/', '', $phone);
                sendUserMessage('+1'.$phone,$otp);
            }    
        }
        

        return ['userOtp'=>encrypt($otp),'otp' => $otp, 'userEmail'=>$email,'userPhone'=> $phone,'rentalUser'=>$rentalUser,'encryptedId'=>$encryptedId];
     }  
     
     public function createUser($firstName,$lastName,$password,$tripper){
        $this->createRental($firstName,$lastName,$password,$tripper);
        $this->createOwner($firstName,$lastName,$password);
     }

     protected function createRental($firstName,$lastName,$password,$tripper)
    {
        $rentalUserArray = [
            'firstName' =>$firstName,
            'lastName' => $lastName,
            'email' => Session::get('userEmail'),
            'password' => Hash::make($password),
            'activationUrl' => Str::random(50),
            'isOtpVerified' => '1',
            'isVerified' => '1',
            'phone' => Session::get('userPhone'),
        ];
        $rentaluser = $this->rentalRepository->createUser((object)$rentalUserArray);
        if(!empty($rentaluser)){
            $prefrnces = [
                'userType' => 'rental',
                'userid' => $rentaluser->id,
                'communicationTitle' => 'account_support',
                'isNotification' => '1',
                'isEmail' => '1',
                'isSms' => '0',
                'createdAt' => Carbon::now(),
            ];
            $this->communicationPreferencesRepository->createCommunicationPreferences((object)$prefrnces);
        }

        $encryptedId = encrypt($rentaluser->id);
        $recentlyCreated = $rentaluser->wasRecentlyCreated;

        $token = Auth::guard('rentaluser')->login($rentaluser);
        Session::put('rental_token', $token);  // this alone won't write to session
        Session::put('user', $rentaluser);
        Session::remove('wasRecentlyCreated');
        if($tripper){
            Session::put('wasRecentlyCreated', $recentlyCreated);
        }

        Session::save();
    }

    protected function createOwner($firstName,$lastName,$password)
    {
        $ownerArray =[
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phone' => Session::get('userPhone'),
            'email' => Session::get('userEmail'),
            'password' => Hash::make($password),
            'isOtpVerified' => '1',
            'uniqueKey' => Str::random(50),
        ];
        $owner = $this->ownerRepository->createUser((object)$ownerArray);
        $token = Auth::guard('owner')->login($owner);
        $owner = Auth::guard('owner')->user()->toArray();
        Session::put('owner_token', $token);  // this alone won't write to session
        Session::put('owner',$owner['first_name']);
        Session::put('unique_key',$owner['unique_key']);
        Session::put('user',$owner);
        Session::save();
        
    }

    public function registerTripper($dtoData,$userId){
        return $this->rentalUserService->registerTripper($dtoData,$userId);;
    }
}
