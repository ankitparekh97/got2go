<?php


namespace App\Services;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\AmenitiesDTO;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Http\DTO\Api\HotelsDTO;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use App\Http\DTO\Api\PropertyVerificationDTO;
use App\Repositories\AmenitiesInterface;
use App\Repositories\BulkUploadInterface;
use App\Repositories\BulkUploadLogsInterface;
use App\Repositories\HotelsInterface;
use App\Repositories\OwnerInterface;
use App\Repositories\PropertyAmenitiesInterface;
use App\Repositories\PropertyAvablityInterface;
use App\Repositories\PropertyBedroomsInterface;
use App\Repositories\PropertyDocumentsInterface;
use App\Repositories\PropertyImagesInterface;
use App\Repositories\PropertyInterface;
use App\Repositories\PropertyTypeInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class BulkUploadService implements BulkUploadServiceContract
{
    protected $propertyRepository;
    protected $bulkUploadRepository;
    protected $bulkUploadLogsRepository;
    protected $propertyTypeRepository;
    protected $amenityRepository;
    protected $hotelRepository;
    protected $ownerRepository;
    protected $propertyImagesRepository;
    protected $propertyDocumentsRepository;
    protected $propertyBedRoonsRepository;
    protected $propertyAmenityRepository;
    protected $propertyAvaiblitiesRepository;

    protected $publicPath;

    public function __construct(
        PropertyInterface $propertyRepository,
        BulkUploadInterface $bulkUploadRepository,
        BulkUploadLogsInterface $bulkUploadLogsRepository,
        PropertyTypeInterface $propertyTypeRepository,
        AmenitiesInterface $amenityRepository,
        HotelsInterface $hotelRepository,
        OwnerInterface $ownerRepository,
        PropertyImagesInterface $propertyImagesRepository,
        PropertyDocumentsInterface $propertyDocumentsRepository,
        PropertyBedroomsInterface $propertyBedRoomsRepository,
        PropertyAmenitiesInterface $propertyAmenityRepository,
        PropertyAvablityInterface $propertyAvaiblitiesRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->bulkUploadRepository = $bulkUploadRepository;
        $this->bulkUploadLogsRepository = $bulkUploadLogsRepository;
        $this->propertyTypeRepository = $propertyTypeRepository;
        $this->amenityRepository = $amenityRepository;
        $this->hotelRepository = $hotelRepository;
        $this->ownerRepository = $ownerRepository;
        $this->propertyImagesRepository = $propertyImagesRepository;
        $this->propertyDocumentsRepository = $propertyDocumentsRepository;
        $this->propertyBedRoomsRepository = $propertyBedRoomsRepository;
        $this->propertyAmenityRepository = $propertyAmenityRepository;
        $this->propertyAvaiblitiesRepository = $propertyAvaiblitiesRepository;

        $this->publicPath = public_path('uploads/property/');
    }


    public function getById($id){
        return $this->bulkUploadRepository->getById($id);
    }

    public function getByUniqueId($unique_key){
        return $this->bulkUploadRepository->getByUniqueId($unique_key);
    }

    public function getBulkUploadByParamsObject($params){
        return $this->bulkUploadRepository->getBulkUploadByParamsObject($params);
    }

    public function getPropertyByIds($ids){
        return $this->propertyRepository->getPropertyByIds($ids);
    }

    public function bulkDownload($request){

        $uniqueId = ($request->unique_key == '') ? uniqid() : $request->unique_key;

        $dir = 'app/bulkUpload/'.$uniqueId.'/';
        $storagePath = storage_path($dir);
        if(!File::isDirectory($storagePath)){
            File::makeDirectory($storagePath, intval(0777,8), true, true);
        }

        if($request->hasFile('propertyData'))
        {
            // delete same file if exists before uploading a new one
            if (Storage::disk('local')->exists('bulkUpload/' . $uniqueId . '/' . $request->get('propertyData'))){
                $propertyDataZip = $request->file('propertyData');
                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $propertyDataZip->getClientOriginalName());
            }

            $uploadCsv = $request->file('propertyData');
            $uploadCsv->storeAs('bulkUpload/'.$uniqueId, $uploadCsv->getClientOriginalName());

            return response()->json([
                'uploadedCsv'=>$uploadCsv->getClientOriginalName(),
                'uniqueId'=>$uniqueId
            ]);
        }

        if($request->hasFile('propertyImages'))
        {
            // delete same file if exists before uploading a new one
            if (Storage::disk('local')->exists('bulkUpload/' . $uniqueId . '/' . $request->get('propertyImages'))){
                $propertyImagesZip = $request->file('propertyImages');
                $mediaZip = pathinfo($storagePath . '/' . $propertyImagesZip->getClientOriginalName(), PATHINFO_FILENAME);

                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $mediaZip);
                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $propertyImagesZip);
            }

            $uploadpropertyImages = $request->file('propertyImages');
            $uploadpropertyImages->storeAs('bulkUpload/'.$uniqueId, $uploadpropertyImages->getClientOriginalName());

            return response()->json([
                'propertyImagesChangeFileName'=>$uploadpropertyImages->getClientOriginalName(),
                'uniqueId'=>$uniqueId
            ]);
        }

        if($request->hasFile('propertyVerficationDocs'))
        {
            // delete same file if exists before uploading a new one
            if (Storage::disk('local')->exists('bulkUpload/' . $uniqueId . '/' . $request->get('propertyVerficationDocs'))){
                $propertyVerficationZip = $request->file('propertyVerficationDocs');
                $verficationZip = pathinfo($storagePath . '/' . $propertyVerficationZip->getClientOriginalName(), PATHINFO_FILENAME);

                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $verficationZip);
                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $propertyVerficationZip);
            }

            $uploadPropertyVerficationDocs = $request->file('propertyVerficationDocs');
            $uploadPropertyVerficationDocs->storeAs('bulkUpload/'.$uniqueId, $uploadPropertyVerficationDocs->getClientOriginalName());

            return response()->json([
                'propertyVerficationChangeFileName'=>$uploadPropertyVerficationDocs->getClientOriginalName(),
                'uniqueId'=>$uniqueId
            ]);
        }

        if($request->hasFile('propertyGovtId'))
        {
            // delete same file if exists before uploading a new one
            if (Storage::disk('local')->exists('bulkUpload/' . $uniqueId . '/' . $request->get('propertyGovtId'))){
                $propertyGovtIdZip = $request->file('propertyGovtId');
                $govtIdZip = pathinfo($storagePath . '/' . $propertyGovtIdZip->getClientOriginalName(), PATHINFO_FILENAME);

                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $govtIdZip);
                Storage::disk('local')->deleteDirectory('bulkUpload/' . $uniqueId . '/' . $propertyGovtIdZip);
            }

            $uploadpropertyGovtId = $request->file('propertyGovtId');
            $uploadpropertyGovtId->storeAs('bulkUpload/'.$uniqueId, $uploadpropertyGovtId->getClientOriginalName());

            return response()->json([
                'propertyGovtIdChangeFileName'=> $uploadpropertyGovtId->getClientOriginalName(),
                'uniqueId'=>$uniqueId
            ]);
        }
    }

    public function getBulkUploadLogs($Id){
        $listings = $this->bulkUploadLogsRepository->getLogs($Id);

        $getJson = ($listings->uploaded_logs != null) ? json_decode($listings->uploaded_logs, true) : '';
        $getErrors = ($listings->failed_logs != null) ? json_decode($listings->failed_logs, true) : '';
        $getMediaErrors = ($listings->failed_logs_media != null) ? json_decode($listings->failed_logs_media, true) : '';
        $getVerificationErrors = ($listings->failed_logs_verification != null) ? json_decode($listings->failed_logs_verification, true) : '';
        $getGovtIdErrors = ($listings->failed_logs_govtId != null) ? json_decode($listings->failed_logs_govtId, true) : '';
        $total = ($getJson!='') ? count($getJson) : '0';

        if($getJson != ''){
            $getProperty = $this->getPropertyByIds($getJson);
        } else {
            $getProperty = array();
        }

        $errorCnt = 0;
        if((!empty($getErrors))) {

            $csvErrorsCount = array();
            if(!empty($getErrors)){
                foreach ($getErrors as $key => $value) {
                    if($key!='InvalidPropertyID'){
                        $csvErrorsCount[] = $key;
                    }
                }
            }

        } else {
            $csvErrorsCount[] = '';
        }

        $mediaCount = array();
        if(!empty($getMediaErrors)){
            foreach($getMediaErrors as $key => $value) {
                $mediaCount[] = $key;
            }
        } else {
            $mediaCount[] = '';
        }

        $verificationCount = array();
        if(!empty($getVerificationErrors)){
            foreach($getVerificationErrors as $key => $value) {
                $verificationCount[] = $key;
            }
        } else {
            $verificationCount[] = '';
        }

        $mergedArray = array_unique(array_merge(array_filter($csvErrorsCount),array_filter($mediaCount),array_filter($verificationCount)), SORT_REGULAR);
        $errorCnt = ($getGovtIdErrors == '' ? count($mergedArray) : 1);

        $bulkUploadLogs = array(
                    'total'=>$total,
                    'listings'=>$listings,
                    'property'=>$getProperty,
                    'errorCnt'=>$errorCnt,
                    'errors'=>$getErrors,
                    'mediaErrors'=>$getMediaErrors,
                    'verificationErrors'=>$getVerificationErrors,
                    'govtErrors'=>$getGovtIdErrors,
            );

        return $bulkUploadLogs;
    }

    public function getBulkUploadListings($ownerId,$id){
        return $this->bulkUploadRepository->getListings($ownerId,$id);
    }

    public function getQueueCount(){
        return $this->bulkUploadLogsRepository->getQueueCount();
    }

    public function getrowPosition($queueId){
        return $this->bulkUploadLogsRepository->getrowPosition($queueId);
    }

    public function saveBulkUpload(BulkUploadDTO $bulkUploadDTO){
        return $this->bulkUploadRepository->saveBulkUpload($bulkUploadDTO);
    }

    public function bulkUploadJob($ownerId,$latLong,$uniqueId,$records){

        $locationDetail = $records['StreetAddress'] . ', ' . $records['City'] . ', ' . $records['State'];

        $resortId = '0';
        if($records['ResortName'] && $records['ResortName']!='' && $records['PropertyCategory'] != 'Private Residence') {
            $getHotel = $this->propertyServiceContract->getHotelsByParamsObject(array('name'=>$records['ResortName']));
            if($getHotel == ''){

                $addResort = array(
                    'name' => $records['ResortName'],
                    'email' => 'testemail@test.test',
                    'password' => Hash::make('123456'),
                    'location' => $locationDetail,
                    'city'=>$records['City'],
                    'state'=>$records['State']
                );

                $hotelDTO = HotelsDTO::convertToObject($addResort);
                $hotel = $this->propertyServiceContract->saveHotel($hotelDTO);
                $resortId = (int)$hotel->id;

            } else {
                $resortId = $getHotel->id;
            }
        }

        $propertyTypes = collect($this->propertyTypeRepository->getPropertyTypesByParams());
        $subPropertyTypeId = '0';
        $propertyTypesToArray = $propertyTypes->pluck('property_type','id')->toArray();

        $propertyTypeId = array_search(strtolower($records['PropertyType']),$propertyTypesToArray);

        $cleaningFee = (int) preg_replace("/[^0-9\.]/", "", $records['CleaningFee']);
        $nightlyPrice = (int) preg_replace("/[^0-9\.]/", "", $records['NightlyRate']);

        $data = array(
            'owner_id'=> $ownerId,
            'title'=> $records['PropertyName'],
            'description'=> $records['Description'],
            'about_the_space'=> $records['AboutTheSpace'],
            'location_detail'=> $locationDetail,
            'city'=> $records['City'],
            'state'=> $records['State'],
            'state_abbrv'=> $records['State'],
            'country'=>'USA',
            'zipcode'=> $records['Zipcode'] ? $records['Zipcode'] : '0',
            'lat_lang'=> $latLong,
            'type_of_property'=>$records['PropertyCategory'] == 'Private Residence' ? 'property' : 'vacation_rental',
            'property_type_id'=> ($propertyTypeId != '') ? $propertyTypeId : '',
            'sub_property_type_id'=>$subPropertyTypeId,
            'resort_id'=> $resortId,
            'guest_will_have'=> $records['PrivateSpace'] == 'YES' ? '1' : '0',
            'is_for_guest'=> $records['GuestsOnly'] == 'YES' ? '1' : '0',
            'no_of_guest'=> $records['Guests'],
            'total_beds'=> $records['Bedrooms'],
            'extra_beds'=> $records['Bedrooms'],
            'total_bedroom'=>$records['Bedrooms'],
            'total_bathroom'=> $records['Bathrooms'],
            'price'=> ($nightlyPrice=='') ? '0' : $nightlyPrice,
            'cleaning_fee'=> ($cleaningFee=='') ? '0' : $cleaningFee,
            'is_instant_booking_allowed'=> $records['InstantBooking'] == 'YES' ? '1' : '0',
            'is_partial_payment_allowed'=> $records['PartialBooking'] == 'YES' ? '1' : '0',
            'check_in'=> $records['CheckInTime'],
            'check_out'=> $records['CheckOutTime'],
            'cancellation_type'=>$records['CancellationPolicy'],
            'is_hotdeals'=> '0',
            'publish' => '1',
            'last_tab_id' => '12',
            'status'=> 'pending',
            'bulk_upload_id'=>$records['ID'],
            'bulkupload_uniquekey' =>  $uniqueId,
            'partial_payment_amount'=> ($nightlyPrice=='') ? '0' : $nightlyPrice,
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now(),
        );

        $property = $this->propertyRepository->createProperty($data);
        $getProperty =  $this->propertyRepository->findPropertyById($property->id);
        return $getProperty;
    }

    public function bulkUploadPropertyAttributes($records,$propertyId){

        $amenities = array( $records['Amenity1'],$records['Amenity2'],$records['Amenity3'],$records['Amenity4'],$records['Amenity5'],
                            $records['Amenity6'],$records['Amenity7'],$records['Amenity8'],$records['Amenity9'],$records['Amenity10'],
                            $records['Amenity11'],$records['Amenity12']);

        $amenitiesArrays = array_filter($amenities, function($v){
            return trim($v);
        });

        $masterAmenities = array_map('strtolower', $this->amenityRepository->getMasterAmenitiesByArray());

        // amenities
        foreach($amenitiesArrays as $amenity){
            if($amenity!=''){
                $chckAmenities = array_search(strtolower($amenity),$masterAmenities);
                if($chckAmenities){
                    $amenitiesArray[] = [
                        'property_type'=> 'property',
                        'property_id'=> $propertyId,
                        'amenity_id'=> $chckAmenities,
                        'amenities_type'=>'unit',
                        'created_at'=> Carbon::now(),
                        'updated_at'=> Carbon::now(),
                    ];;
                }
            }
        }

        $this->propertyAmenityRepository->saveAmenities($amenitiesArray);

        // avaibility dates
        if($records['Availiblities'] != ''){
            $propertyAvailiblities = ($records['Availiblities'] != '') ? explode(',',$records['Availiblities']) : '';
            foreach($propertyAvailiblities as $key=>$val){
                $availblty[$key] = explode(' - ',$val);
                $avaibiltyFrom[$records['ID']][trim($availblty[$key][0])] = trim($availblty[$key][1]);
            }
        }

        $propertyAvailabilites = [];
        if(count($avaibiltyFrom)>0){
            $propertyAvailableWhen = $avaibiltyFrom;

            foreach($propertyAvailableWhen as $key=>$value){
                foreach($value as $from=>$to){
                    $propertyAvailabilites[] = [
                        'property_id'=> $propertyId,
                        'available_from'=> Carbon::createFromFormat('m/d/Y', $from)->format('Y/m/d'),
                        'available_to'=> Carbon::createFromFormat('m/d/Y', $to)->format('Y/m/d')
                    ];
                }

                $data = array_filter($propertyAvailabilites);
                $this->propertyAvaiblitiesRepository->savePropertyAvaiblities($data);
                break;
            }
        }

        // bedrooms
        $bedrooms = [];
        for ($x = 1; $x <= 4; $x++) {
            $bedroomArray[] = [
                'property_id'=>$propertyId,
                'bedroom_no'=>$x,
                'bed'=>'1',
                'bed_type'=>'1'
            ];
        }
        $this->propertyBedRoomsRepository->savePropertyBedrooms($bedrooms);

        $collection = collect($bedroomArray);
        $collection->sum('bed');

        $data = array('total_beds'=>$records['Bedrooms'] + $collection->sum('bed'));
        $this->propertyRepository->updateProperty($propertyId,$data);

        return true;
    }

    public function bulkUploadMedia($folder,$property){

        // get list of all the directories and files from unzipped directory
        $isExists = File::exists($folder);
        if($isExists === true){
            $files = File::allfiles($folder);
            foreach($files as $file){

                // check image type
                if($file->getExtension() == 'pdf' || $file->getExtension() == 'doc'
                    || $file->getExtension() == 'docx' || $file->getExtension() == 'xlsx' || $file->getExtension() == 'csv'){
                        continue;
                }

                // get image dimensions
                list($width, $height) = getimagesize($file->getRealPath());
                if(($height < 683 || $width < 1024) && $file->getExtension() != 'mp4'){
                    continue;
                }

                // generate array of property images
                if($property!=''){

                    // move image to public/uploads/property/
                    $photo = 'property_photo_'.$property->id . uniqid() . '.' . $file->getExtension();
                    File::move( $file->getRealPath(), $this->publicPath . $photo);

                    // the image will be replaced with an optimized version which should be smaller
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize($this->publicPath . $photo);

                    $propertyPhotosArray[] = array(
                        'property_id' => $property->id,
                        'photo' => $photo,
                        'media_type' => ($file->getExtension() == 'mp4') ? 'video/'.$file->getExtension() : 'image/'.$file->getExtension(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    );
                }
            }
        }

         // pick only first 15, do not upload any more
         $slicedPhotos = array_slice($propertyPhotosArray, 0, 15);
         if(!empty($slicedPhotos)){
             $this->propertyImagesRepository->saveMedia($slicedPhotos);
         }

         //save cover photo
         $data = array(
             'cover_photo'=> $slicedPhotos[0]['photo'],
             'status'=> GlobalConstantDeclaration::BOOKING_STATUS_PENDING,
             'last_tab_id'=>'12'
         );
         $this->propertyRepository->updateProperty($property->id,$data);
    }

    public function bulkUploadDocs($folder,$property){

        // get list of all the directories and files from unzipped directory
        $isExists = File::exists($folder);
        if($isExists === true){
            $files = File::allfiles($folder);
            foreach($files as $file){

                // check document type
                if($file->getExtension() == 'doc' || $file->getExtension() == 'docx' || $file->getExtension() == 'mp4' || $file->getExtension() == 'sql' ||
                    $file->getExtension() == 'xlsx' || $file->getExtension() == 'csv' || $file->getExtension() == 'mp3'){
                    continue;
                }

                // generate array of property images
                if($property!=''){

                    // move image to public/uploads/property/
                    $verificationDocument = 'ownership_document_'. $property->id . uniqid() . '.' . $file->getExtension();
                    File::move( $file->getRealPath(), $this->publicPath . $verificationDocument);

                    // the image will be replaced with an optimized version which should be smaller
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize($this->publicPath . $verificationDocument);

                    $verificationDocumentArray[] = array(
                        'property_id' => $property->id,
                        'document_name' => $verificationDocument,
                        'media_type' => ($file->getExtension() == 'pdf') ? 'application/'.$file->getExtension() : 'image/'.$file->getExtension(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    );
                }
            }

        }

        // pick only first 6, do not upload any more
        $slicedDocs = array_slice($verificationDocumentArray, 0, 6);
        if(!empty($slicedDocs)){

            // PropertyImages::insert($slicedPhotos);
            $this->propertyDocumentsRepository->savePropertyDocuments($slicedDocs);
        }
   }

   public function bulkUploadGovtDocs($folder,$propertyId){

        // get list of all the directories and files from unzipped directory
        $files = File::allfiles($folder);
        foreach($files as $file){

            // check document type
            if($file->getExtension() == 'doc' || $file->getExtension() == 'docx' || $file->getExtension() == 'mp4' ||
                $file->getExtension() == 'xlsx' || $file->getExtension() == 'csv' || $file->getExtension() == 'mp3' || $file->getExtension() == 'sql'){
                continue;
            }

            // move image to public/uploads/property/
            $govtIdDocument = 'govtId_document_'. $propertyId . uniqid() . '.' . $file->getExtension();
            File::copy( $file->getRealPath(), $this->publicPath . $govtIdDocument);

            // the image will be replaced with an optimized version which should be smaller
            $optimizerChain = OptimizerChainFactory::create();
            $optimizerChain->optimize($this->publicPath . $govtIdDocument);

            $govtIdDocumentArray[] = array(
                'property_id' => $propertyId,
                'document_name' => $govtIdDocument,
                'media_type' => ($file->getExtension() == 'pdf') ? 'application/pdf' : 'image/'.$file->getExtension(),
                'is_govt_id' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );

        }

        // store govtId
        if(!empty($govtIdDocumentArray))

            // pick only first 6, do not upload any more
            $slicedGovt = array_slice($govtIdDocumentArray, 0, 6);
            if(!empty($slicedGovt)){
                $this->propertyDocumentsRepository->savePropertyDocuments($slicedGovt);
                // PropertyOwnershipDocuments::insert($slicedGovtId);
            }
   }

   public function bulkUploadComplete(
                    $serializeUploadedLogs,
                    $serializeErrLogs,
                    $serializeMediaErrLogs,
                    $serializeVerificationErrLogs,
                    $serializeGovtIdErrLogs,
                    $bulkUploadId,
                    $status
    )
    {

        if($status != GlobalConstantDeclaration::STATUS_FAILED){

            $serializeUploadedData= (count($serializeUploadedLogs) > 0) ?  json_encode($serializeUploadedLogs) : null;
            $serializeErrData = (count($serializeErrLogs) > 0) ? json_encode($serializeErrLogs) : null;
            $serializeMediaErrData = (count($serializeMediaErrLogs) > 0) ? json_encode($serializeMediaErrLogs) : null;
            $serializeVerificationErrData =  (count($serializeVerificationErrLogs) > 0) ? json_encode($serializeVerificationErrLogs) : null;
            $serializeGovtIdErrData = (count($serializeGovtIdErrLogs) > 0) ? json_encode($serializeGovtIdErrLogs) : null;

            $insertAllLogsInOneArr = array(
                'bulk_upload_id' => $bulkUploadId,
                'uploaded_logs' => $serializeUploadedData,
                'failed_logs' => $serializeErrData,
                'failed_logs_media' => $serializeMediaErrData,
                'failed_logs_verification' => $serializeVerificationErrData,
                'failed_logs_govtId' => $serializeGovtIdErrData,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );

            $this->bulkUploadLogsRepository->saveLogs($insertAllLogsInOneArr);
        }

        $this->saveBulkUploadStatus($bulkUploadId,$status);
   }

   final function saveBulkUploadStatus($bulkUploadId,$status){

         // update the status of the job once done
        return $this->bulkUploadRepository->update($bulkUploadId, array('status'=>$status,'updated_at'=> Carbon::now()));
   }

}
