<?php


namespace App\Services;

use App\Models\Property;

interface LoginServiceContract
{
     public function userLogin($request);
     public function ownerLogin($ownertoken);
     public function rentalLogin($rentaltoken);
     public function adminLogin($request);
     public function forgotRenterTripperPassword($email);
     public function forgotAdminPassword($email);
     public function socialLogin($googleId=null,$facebookId=null,$email,$name,$loginfor);

}
