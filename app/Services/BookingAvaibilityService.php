<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Repositories\BookingInterface;
use App\Repositories\PropertyInterface;
use Helper;
use Carbon\Carbon;


class BookingAvaibilityService implements BookingAvaibilityServiceContract
{
    
    protected $bookingRepository;
    protected $propertyRepository;
    

    public function __construct(
        BookingInterface $bookingRepository,
        PropertyInterface $propertyRepository        
    )
    {
        $this->bookingRepository = $bookingRepository;
        $this->propertyRepository = $propertyRepository;
    }


   /*Description check Booking form database*/
    public function checkBookings($id,$checkInDate,$checkOutDate)
    {
        $isPropertyBooked = $this->bookingRepository->checkBookings($id,$checkInDate,$checkOutDate);
        return $isPropertyBooked;
    }

    public function propertyAvailablity($id,$checkInDate,$checkOutDate)
    {
        $checkInDate = date('Y-m-d',strtotime($checkInDate));
        $checkOutDate = date('Y-m-d',strtotime($checkOutDate));
        $propertyAvaliblity = $this->propertyRepository->propertyAvailablity($id,$checkInDate,$checkOutDate);
        return $propertyAvaliblity;
    }
}
