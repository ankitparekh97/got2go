<?php


namespace App\Services;


use App\Http\DTO\Api\SubscriptionPlanDTO;
use App\Models\SubscriptionPlan;
use App\Repositories\SubscriptionPlanInterface;
use Illuminate\Http\Request;

class SubscriptionPlanService implements SubscriptionPlanContract
{
    public $subscriptionPlan;

    public function __construct(
        SubscriptionPlanInterface $subscriptionPlan
    )
    {
        $this->subscriptionPlan = $subscriptionPlan;
    }

    public function createSubscriptionPlan(SubscriptionPlanDTO $subscriptionPlanDTO)
    {
        $this->subscriptionPlan->createSubscriptionPlan($subscriptionPlanDTO);
    }

    public function getLatestSubscription()
    {
        /**
         * @var SubscriptionPlan $subscriptionPlan
         */
        $subscriptionPlan = $this->subscriptionPlan->getLastInsertedSubscriptionPlan();

        return $subscriptionPlan;
    }
}
