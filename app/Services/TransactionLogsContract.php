<?php
namespace App\Services;

interface TransactionLogsContract
{
    public function getTransactionLogs();

    public function refundBooking($input);

}
