<?php


namespace App\Services;


use App\Http\DTO\Api\SubscriptionPlanDTO;

interface SubscriptionPlanContract
{
    public function createSubscriptionPlan(SubscriptionPlanDTO $subscriptionPlanDTO);

    public function getLatestSubscription();
}
