<?php


namespace App\Services\Renter;

use App\Models\Property;

interface PaymentbookingServiceContract
{
    public function getPaymentDetails($propertyId,$checkInDate,$checkOutDate,$propertyOfferService,$couponData);

    public function createBooking($bookingDetails,$checkInDate,$checkOutDate,$propertyOfferId,$guest,$selectedCard,$couponId);

    public function createBookingPayment($bookingDetails,$booking,$selectedCard);

}
