<?php


namespace App\Services\Renter;

use App\Models\Property;

interface TripperServiceContract
{
    public function getSubscription();

    public function subscriptionApprove($rentalUserId,$subscriptionAction);

    public function getSubscriptionById($subscriptionId);

    public function tripperList();

    public function subscriptionCancel($tripperId);

    public function adminSubscriptionSave($annualSubscriptionAmount,$subscriptionId,$discountPercentage,$strikeThroughAmount);
}
