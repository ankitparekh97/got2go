<?php


namespace App\Services\Renter;

use App\Models\Property;

interface SubscriptionServiceContract
{
    public function cancelPaymentGatewaySubscription($tripperId);
}
