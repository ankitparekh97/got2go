<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\SubscriptionPlanDTO;
use App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;
use App\Models\Property;
use App\Repositories\SubscriptionInterface;
use App\Repositories\RentalInterface;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\SubscriptionPlanContract;

class TripperService implements TripperServiceContract
{
    protected $subscriptionRepository;

    public function __construct(
        SubscriptionInterface $subscriptionRepository,
        RentalInterface $rentalRepository,
        PaymentServiceInterface $paymentService,
        SubscriptionPlanContract $subscriptionPlanService
    )
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->rentalRepository = $rentalRepository;
        $this->paymentService = $paymentService;
        $this->subscriptionPlanService = $subscriptionPlanService;
    }


    public function getSubscription()
    {
        return $this->subscriptionRepository->getSubscription();
    }

    public function getSubscriptionById($subscriptionId)
    {
        return $this->subscriptionRepository->getSubscriptionById($subscriptionId);
    }

    public function tripperList(){
        return $this->rentalRepository->getTripperList();
    }

    public function subscriptionApprove($rentalUserId,$subscriptionAction){

        $this->rentalRepository->updateSubscriptionStatus($rentalUserId,$subscriptionAction);

        if($subscriptionAction == 'active') {
            $this->rentalRepository->activePaymentGatewaySubscription($rentalUserId);
        } else {
            $this->rentalRepository->pausePaymentGatewaySubscription($rentalUserId);
        }

        $message = 'Tripper subscription '.(($subscriptionAction == 'active')?'activated':$subscriptionAction).' successfully';
        return response()->json(['success'=>true,'message'=>$message]);
    }

    public function subscriptionCancel($tripperId){
        return $this->rentalRepository->cancelPaymentGatewaySubscription($tripperId);
    }

    public function adminSubscriptionSave($annualSubscriptionAmount,$subscriptionId,$discountPercentage,$strikeThroughAmount){

        $subscriptions = $this->getSubscriptionById($subscriptionId);

        if($subscriptions->annual_subscription != $annualSubscriptionAmount) {

            $planId = generateRandomAlphaNumeric('Plan_');
            $stripeSubscriptionPlanDTO = new PlanRequestDTO;
            $stripeSubscriptionPlanDTO->id = $planId;
            $stripeSubscriptionPlanDTO->name = 'Tripper Subscription Plan '.$annualSubscriptionAmount;
            $stripeSubscriptionPlanDTO->amount = $annualSubscriptionAmount;
            $stripeSubscriptionPlanDTO->currency = 'USD';
            $stripeSubscriptionPlanDTO->interval = GlobalConstantDeclaration::PLAN_TYPE_YEARLY;

            /**
             * @var \App\Services\PaymentServices\Stripe\Plan\PlanResponseDTO $subscriptionPlan
             */
            $subscriptionPlan = $this->paymentService->createPlan($stripeSubscriptionPlanDTO);


            $subscriptionPlanDTO = new SubscriptionPlanDTO;
            $subscriptionPlanDTO->id = $subscriptionPlan->id;
            $subscriptionPlanDTO->name = $subscriptionPlan->name;
            $subscriptionPlanDTO->amount = $annualSubscriptionAmount;
            $subscriptionPlanDTO->currency = $subscriptionPlan->currency;
            $subscriptionPlanDTO->interval = $subscriptionPlan->interval;
            $subscriptionPlanDTO->statementDescriptor = $subscriptionPlan->statementDescriptor;

            $this->subscriptionPlanService->createSubscriptionPlan($subscriptionPlanDTO);
        }
        $this->subscriptionRepository->updateSubscriptionDetails($subscriptionId,$annualSubscriptionAmount,$discountPercentage,$strikeThroughAmount);
    }
}
