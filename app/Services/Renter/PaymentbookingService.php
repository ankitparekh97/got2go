<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use App\Models\PaymentCards;
use App\Repositories\PropertyInterface;
use App\Repositories\ConfigurationInterface;
use App\Repositories\SubscriptionInterface;
use App\Repositories\FavrioteTripboardInterface;
use App\Repositories\TripBoardInterface;
use App\Repositories\BookingInterface;
use App\Repositories\PaymentCardInterface;
use App\Repositories\BookingPaymentInterface;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use Spatie\DataTransferObject\DataTransferObject;
use Carbon\Carbon;

class PaymentbookingService implements PaymentbookingServiceContract
{
    protected $propertyRepository;
    protected $configurationRepository;
    protected $subscriptionRepository;
    protected $favrioteTripboardRepository;
    protected $tripboardRepository;
    protected $bookingRepository;
    protected $paymentCardRepository;
    protected $bookingPaymentRepository;

    public function __construct(
        PropertyInterface $propertyRepository,
        ConfigurationInterface $configurationRepository,
        SubscriptionInterface $subscriptionRepository,
        FavrioteTripboardInterface $favrioteTripboardRepository,
        TripboardInterface $tripboardRepository,
        BookingInterface $bookingRepository,
        PaymentCardInterface $paymentCardRepository,
        BookingPaymentInterface $bookingPaymentRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->configurationRepository = $configurationRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->favrioteTripboardRepository = $favrioteTripboardRepository;
        $this->tripboardRepository = $tripboardRepository;
        $this->bookingRepository = $bookingRepository;
        $this->paymentCardRepository = $paymentCardRepository;
        $this->bookingPaymentRepository = $bookingPaymentRepository;
    }


    public function getPaymentDetails($propertyId,$checkInDate,$checkOutDate,$propertyOfferService,$couponData=null)
    {

        /* Fetch Property Details */
        $propertyDetails =  $this->propertyRepository->getPropertyDetailsById($propertyId);

        if($propertyDetails){

            /* Fetch Login user */
            $rental = getLoggedInRental();
            $showStAdd = false;
            $rentalAmountArray =[];
            $tripperAmountArray = [];
            $checkInDateFormated = carbonParseDate($checkInDate)->format('m/d/Y');
            $checkOutDateFormated = carbonParseDate($checkOutDate)->format('m/d/Y');

            $days = carbonDateDiffInDays($checkInDate,$checkOutDate);

            /* Check Tripboard details for loggedin user */
            if($rental){
                /* Fetch payment cards details */
                $rentalUser = $this->paymentCardRepository->getPaymentCardDetails($rental->id);
                /* Check booking for future date to show full address */
                $booking = $this->bookingRepository->checkBookingByUser($rental->id,$propertyId);
                if(!empty($booking->toarray())){
                    $showStAdd = true;
                }

                /* Fetch Configuration setting for service fee */
                $configuration = $this->configurationRepository->getConfiguration();

                $serviceFeeArray = explode(',', $configuration);
                $serviceFee = $serviceFeeArray[0];
                $serviceType = $serviceFeeArray[1];

                /* Fetch Subscription discount for Tripper Price */
                $discount = $this->subscriptionRepository->getSubscription()->discount_percentage;

                /* Price according to user type */
                $price = round($propertyDetails->price,2);
                $cleaningFee = getRenterCleaningFee($propertyDetails->cleaning_fee);
                $reservationAmount = $price * $days;

                ## Calculating Service Fee
                if($serviceType == 2){
                    $serviceFee = ($serviceFee / 100) *  ($price * $days);
                }
                $serviceFee = round($serviceFee,2);
                ## End of Calculating Service;
                $rentalAmountArray = $this->calculateAmount($reservationAmount,$cleaningFee,$serviceFee);
                if($rental->is_tripper == 1){
                    $price = $propertyDetails->price - (($discount / 100) * $propertyDetails->price);
                    // if offer is accepted by owner than and than need to set below offered price
                    if(!empty($propertyOfferService)) {
                        $offerTripperOfferPrice = getTripperPriceFromPropertyOffer($propertyOfferService);
                        if ($offerTripperOfferPrice > 0) {
                            $price = $offerTripperOfferPrice;
                        }
                    }
                    $price = round($price,2);
                    $cleaningFee = round($propertyDetails->cleaning_fee,2);
                    $reservationAmount = $price * $days;
                    $serviceFee = 0;
                    $tripperAmountArray = $this->calculateAmount($reservationAmount,$cleaningFee,$serviceFee);
                    $tripperAmountArray['totalSaved'] = $rentalAmountArray['totalAmount'] - $tripperAmountArray['totalAmount'];
                    $rentalAmountArray = $tripperAmountArray;
                }

                if(!empty($couponData) && !empty($couponData->discount_percentage)){
                    $totalDiscountedPrice = ($rentalAmountArray['totalAmount'] * $couponData->discount_percentage)/100;
                    $rentalAmountArray['totalAmount'] = $rentalAmountArray['totalAmount'] - $totalDiscountedPrice;
                }
            }
            return [
                    'propertyDetails'=>$propertyDetails,
                    'rentalUser'=>$rentalUser,
                    'price'=>$price,
                    'reservationAmount'=>$reservationAmount,
                    'days'=>$days,
                    'occupnactTax'=>$rentalAmountArray['occupnactTax'],
                    'total'=>$rentalAmountArray['totalAmount'],
                    'showStAdd'=>$showStAdd,
                    'transactionFee'=>$rentalAmountArray['transactionFee'],
                    'serviceFee'=>$serviceFee,
                    'payoutFee'=>$rentalAmountArray['payoutFee'],
                    'cleaningFee'=>$cleaningFee,
                    'checkInDate'=>$checkInDate,
                    'checkOutDate'=>$checkOutDate,
                    'totalSaved'=>$rentalAmountArray['totalSaved']
                ];
        }

        return ['propertyDetails'=>[]];
    }

    private function calculateAmount($reservationAmount,$cleaningFee,$serviceFee){
        ##Calculating Payout Fee
        $payoutFee = round(getPayoutFee($reservationAmount) + getPayoutFee($cleaningFee) + 0.25);
        ##End of calculating Payout Fee

        $totalAmount = $reservationAmount + $cleaningFee + $serviceFee;

        ##Calculating Occupancy Tax
        $occupnactTax = getOccupancyTax($totalAmount);
        ##End of calculating Occupancy Tax

        $totalAmount = $totalAmount + $payoutFee + $occupnactTax;

        ##Calculating Transaction Fee for stripe
        $transactionFee = getTransactionFee($totalAmount);
        ##End of Transaction Fee for stripe

        $totalAmount = $totalAmount + $transactionFee;
        $transactionFee = $transactionFee + $serviceFee;

        return [
            'payoutFee'=>$payoutFee,
            'occupnactTax'=>$occupnactTax,
            'transactionFee'=>$transactionFee,
            'totalAmount'=>$totalAmount,
            'totalSaved'=>0
        ];
    }

    public function createBooking($bookingDetails,$checkInDate,$checkOutDate,$propertyOfferId,$guest,$selectedCard,$couponId){

        $propertyDetails = $bookingDetails['propertyDetails'];
        $user = getLoggedInRental();

        $bookingArray = [
            'bookingType' => $propertyDetails->type_of_property,
            'rentalUserId' => $user->id,
            'propertyId' => $propertyDetails->id,
            'checkInDate' => Carbon::createFromFormat('m/d/Y', $checkInDate)->format('Y-m-d'),
            'checkOutDate' => Carbon::createFromFormat('m/d/Y', $checkOutDate)->format('Y-m-d'),
            'adults' => $guest,
            'childrens' => '0',
            'rooms' => $propertyDetails->total_bedroom,
            'actualPrice' =>$bookingDetails['reservationAmount'],
            'offerAmount' => 0,
            'serviceFee' => $bookingDetails['serviceFee'],
            'occupancyTaxFees' => $bookingDetails['occupnactTax'],
            'total' => $bookingDetails['total'],
            'status' => 'approved',
            'propertyStatus' => ($propertyDetails->is_instant_booking_allowed == '1') ? 'pending': 'approved',
            'ownerStatus' => ($propertyDetails->is_instant_booking_allowed == '1') ? 'instant_booking': 'pending',
            'propertyOffersId' => !empty($propertyOfferId) ? $propertyOfferId : null,
            'cardToken'=>$selectedCard,
            'tripperSavingAmount' => $bookingDetails['totalSaved'],
            'couponCodesId' => !empty($couponId) ? $couponId : null,
            'payoutFee' => $bookingDetails['payoutFee'],
            'cleaningFee'=>$bookingDetails['cleaningFee'],
            'transactionFee'=>$bookingDetails['transactionFee'],
            'createdAt' => Carbon::now(),
            'updatedAt' => Carbon::now(),
        ];

        $booking =  $this->bookingRepository->createBooking((object)$bookingArray);
        return $booking;

    }

    public function createBookingPayment($bookingDetails,$booking,$selectedCard){

        $propertyDetails = $bookingDetails['propertyDetails'];
        $user = getLoggedInRental();
        $total = $bookingDetails['total'];

        $chargeRequestDTO = new ChargeRequestDTO();
        $chargeRequestDTO->amount = $total;
        $chargeRequestDTO->currency = "USD";
        $chargeRequestDTO->source = $selectedCard;
        $chargeRequestDTO->description = "Payment for ".$propertyDetails->title;
        $chargeRequestDTO->customer = $user->customer_id;
        $chargeRequestDTO->bookingId = $booking->id;
        $PaymentService = new PaymentService();
        $charge = $PaymentService->createCharge($chargeRequestDTO);

        $bookingPaymentArray = [
                        'bookingId' => $booking->id,
                        'renterUserId' => $user->id,
                        'paymentType' => 'completed',
                        'transactionNumber' => $charge['id'],
                        'amount' => round($total,2),
                        'refundAmount' => '0',
                        'createdAt' => Carbon::now(),
                        'updatedAt' => Carbon::now(),
        ];

         return $this->bookingPaymentRepository->createBookingPayment((object)$bookingPaymentArray);

    }
}
