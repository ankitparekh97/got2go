<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Repositories\OwnerRentalPairInterface;
use App\Repositories\MessagesInterface;
use Helper;
use Session;
use Carbon\Carbon;
use App\Libraries\PusherFactory;

class ChatService implements ChatServiceContract
{
    protected $propertyRepository;

    public function __construct(
        OwnerRentalPairInterface $ownerRentalPairRepository,
        MessagesInterface $messagesInterfaceRepository
    )
    {
        $this->ownerRentalPairRepository = $ownerRentalPairRepository;
        $this->messagesInterfaceRepository = $messagesInterfaceRepository;
    }

    public function getRentalChatData($name){
        return $this->ownerRentalPairRepository->fetchRentalChatContact($name);
    }   

    public function getOwnerChatData($name){
        return $this->ownerRentalPairRepository->fetchOwnerChatContact($name);
    }

    public function getMessages($pairId){

        
        $messages = $this->messagesInterfaceRepository->fetchMessages($pairId);

        $messgaeArray = [];

        foreach($messages as $message){
            if (!array_key_exists(date('Y-m-d',strtotime($message->created_at)),$messgaeArray)){
                $messgaeArray[date('Y-m-d',strtotime($message->created_at))] = [];
            }
            array_push($messgaeArray[date('Y-m-d',strtotime($message->created_at))],$message);
        }
        $messages = $messgaeArray;

        $return = [];

        foreach ($messages as $key=>$message) {

            $today = Helper::convertToLocal(Carbon::today()->toDateTimeString(),Session::get('timezone'));
            $yesterday = Helper::convertToLocal(Carbon::yesterday()->toDateTimeString(),Session::get('timezone'));
            $messageDate = Helper::convertToLocal($key.'00:00:00',Session::get('timezone'));
            if($today->toDateString() == $messageDate->toDateString()){
                $date = 'Today';
            }else if($yesterday->toDateString() == $messageDate->toDateString()){
                $date = 'Yesterday';
            }else{
                $date = Carbon::parse($messageDate->toDateString())->format('F d, y');
            }
            $return[] = view('rentaluser.chat.message-line')->with(['messages'=>$message,'date'=>$date])->render();
        }

        return $return;
    }

    public function unreadFalg($pairId,$toUserId){
        $this->messagesInterfaceRepository->unreadFlag($pairId,$toUserId);
    }

    public function getOwnerMessages($pairId){

        
        $messages = $this->messagesInterfaceRepository->fetchMessages($pairId);
       
        $messgaeArray = [];

        foreach($messages as $message){
            if (!array_key_exists(date('Y-m-d',strtotime($message->created_at)),$messgaeArray)){
                $messgaeArray[date('Y-m-d',strtotime($message->created_at))] = [];
            }
            array_push($messgaeArray[date('Y-m-d',strtotime($message->created_at))],$message);
        }
        $messages = $messgaeArray;
        
        $return = [];

        foreach ($messages as $key=>$message) {

            //dd(Session::get('timezone'));
            $today = Helper::convertToLocal(Carbon::today()->toDateTimeString(),Session::get('timezone'));
            $yesterday = Helper::convertToLocal(Carbon::yesterday()->toDateTimeString(),Session::get('timezone'));
            $messageDate = Helper::convertToLocal($key.'00:00:00',Session::get('timezone'));
            if($today->toDateString() == $messageDate->toDateString()){
                $date = 'Today';
            }else if($yesterday->toDateString() == $messageDate->toDateString()){
                $date = 'Yesterday';
            }else{
                $date = Carbon::parse($messageDate->toDateString())->format('F d, y');
            }
            $return[] = view('owner.chat.message-line')->with(['messages'=>$message,'date'=>$date])->render();
        }
        return $return;
    }
}
