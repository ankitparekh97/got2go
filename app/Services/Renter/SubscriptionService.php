<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use App\Repositories\RentalInterface;
use App\Repositories\SubscriptionInterface;

class SubscriptionService implements SubscriptionServiceContract
{
    protected $rentalRepository;

    public function __construct(
        RentalInterface $rentalRepository
    )
    {
        $this->rentalRepository = $rentalRepository;
    }

    public function cancelPaymentGatewaySubscription($tripperId){
        $this->rentalRepository->cancelPaymentGatewaySubscription($tripperId);
    }
}
