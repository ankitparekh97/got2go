<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use App\Repositories\PropertyInterface;
use App\Repositories\ConfigurationInterface;
use App\Repositories\SubscriptionInterface;
use App\Repositories\FavrioteTripboardInterface;
use App\Repositories\TripBoardInterface;
use App\Repositories\BookingInterface;
use App\Services\BookingAvaibilityServiceContract;
use App\Services\PropertyOfferServiceContract;
use DateTime;

class PropertyDetailService implements PropertyDetailServiceContract
{
    protected $propertyRepository;

    public function __construct(
        PropertyInterface $propertyRepository,
        ConfigurationInterface $configurationRepository,
        SubscriptionInterface $subscriptionRepository,
        FavrioteTripboardInterface $favrioteTripboardRepository,
        TripBoardInterface $tripboardRepository,
        BookingInterface $bookingRepository,
        BookingAvaibilityServiceContract $bookingAvaibilityServiceContract,
        PropertyOfferServiceContract  $propertyOfferServiceContract
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->configurationRepository = $configurationRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->favrioteTripboardRepository = $favrioteTripboardRepository;
        $this->tripboardRepository = $tripboardRepository;
        $this->bookingRepository = $bookingRepository;
        $this->bookingAvaibilityServiceContract = $bookingAvaibilityServiceContract;
        $this->propertyOfferServiceContract = $propertyOfferServiceContract;
    }


    public function getPropertyDetailsById($propertyId)
    {
        /* Fetch Property Details */
        $propertyDetails =  $this->propertyRepository->getPropertyDetailsById($propertyId);

        if($propertyDetails){
             /* Fetch Configuration setting for service fee */
            $configuration = $this->configurationRepository->getConfiguration();

            $serviceFeeArray = explode(',', $configuration);
            $serviceFee = $serviceFeeArray[0];
            $serviceType = $serviceFeeArray[1];

            /* Fetch Subscription discount for Tripper Price */
            $discount = $this->subscriptionRepository->getSubscription()->discount_percentage;
            $tripperPrice = $propertyDetails->price - (($discount / 100) * $propertyDetails->price);

            /* Fetch Login user */
            $rentalUser = Auth::guard('rentaluser')->user();
            $isLiked = 0;
            $tripboards =[];
            $tripboardProperty =[];
            $showStAdd = false;

            /* Check Tripboard details for loggedin user */
            if($rentalUser){
                $rentalUserId = $rentalUser->id;
                /* Fetch Property is favrioted or not for logged in user*/
                $favTripboard = $this->favrioteTripboardRepository->getFavTriboardsWithPropertyId($rentalUserId,$propertyId);
                if($favTripboard->exists()){
                    $isLiked = 1;
                }

                /* Fetch Tripboard for property*/
                $tripboards = $this->tripboardRepository->getTriboardsWithPropertyId($rentalUserId,$propertyId);

                /* Check booking for future date to show full address */
                $booking = $this->bookingRepository->checkBookingByUser($rentalUserId,$propertyId);
                if(!empty($booking->toarray())){
                    $showStAdd = true;
                }

            }

            return [
                    'propertyDetails'=>$propertyDetails,
                    'tripboards'=>$tripboards,
                    'isLiked'=>$isLiked,
                    'tripperPrice'=>$tripperPrice,
                    'serviceFee'=>$serviceFee,
                    'serviceType'=>$serviceType,
                    'showStAdd'=>$showStAdd,
                ];
        }

        return ['propertyDetails'=>[]];
    }

    public function propertyCheckBooking($propertyId,$checkInDate,$checkOutDate,$needOfferValidation){

        $bookingList = $this->bookingAvaibilityServiceContract->checkBookings($propertyId,$checkInDate,$checkOutDate);

        if(!empty($bookingList->toarray())){
            return ['success'=>false ,'type' => 1 , 'error'=> "This stay is not available for the above selected dates. Please change the dates."];
        }

        $propertyAvailability = $this->bookingAvaibilityServiceContract->propertyAvailablity($propertyId,$checkInDate,$checkOutDate);
        if(empty($propertyAvailability->toarray())){
            return ['success'=>false ,'type' => 2 , 'error'=> "This stay is not available for the above selected dates. Please change the dates."];
        }

        if(!empty($propertyAvailability->toarray())){

            foreach($propertyAvailability as $key=>$val){
                $dStart =  new DateTime($checkOutDate);
                $dEnd =   new DateTime($checkInDate);
                $dDiff = $dEnd->diff($dStart);
                $mindays = $dDiff->format('%r%a');

                if($val->property_id == $propertyId && $val->is_partial_booking_allowed == 1 &&  $mindays < $val->minimum_days  ){
                    return ['success'=>false ,'type' => 2 , 'error'=> "The minimum nightly stay for this property is ".$val->minimum_days." nights."];
                }

            }
        }

        if($needOfferValidation == 'false'){
                $needOfferValidation = false;
            }

            if($needOfferValidation && !empty(getLoggedInRental())){
                /**
                 * @var PropertyOffers $isOfferAlreadyExist
                 */
                $isOfferAlreadyExist = $this->propertyOfferServiceContract->checkOfferDateIsOverLappingByTripperId(
                    carbonParseDate($checkInDate),carbonParseDate($checkOutDate),getLoggedInRentalId(),$propertyId
                );

                if($isOfferAlreadyExist->count() > 0){
                    $myOfferLink = '<a href="'.route('web.tripper.myOffers').'"><b>View Offers</b></a>';
                    $statusMessage = "You already have a pending offer between these dates. Click to $myOfferLink.";
                    return ['success'=> false, 'type' => 3 , 'error'=> $statusMessage];
                }
            }
         return ['success'=>true , 'error'=> ""];

    }
}
