<?php


namespace App\Services\Renter;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use App\Repositories\PropertyInterface;
use App\Repositories\FavrioteTripboardInterface;
use App\Repositories\TripBoardInterface;

class FavoriteService implements FavoriteServiceContract
{
    protected $propertyRepository;

    public function __construct(
        PropertyInterface $propertyRepository,
        FavrioteTripboardInterface $favrioteTripboardRepository,
        TripBoardInterface $tripboardRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->favrioteTripboardRepository = $favrioteTripboardRepository;
        $this->tripboardRepository = $tripboardRepository;
    }


    public function getFavouriteTripboards($userId)
    {
        /* Fetch Favourite Tripboard Details */
        $favTripboard = $this->favrioteTripboardRepository->getFavTriboards($userId);

        $similarListing = array();
        if(!empty($favTripboard->toarray())){
            $avgPrice = 0;
            $totalCount = $favTripboard->count();
            foreach($favTripboard as $fav){
                $avgPrice = $avgPrice + $fav->property->price;
            }
            $avgPrice = $avgPrice / $totalCount;

            $averagePlus = 0;
            $averageMinus = 0;
            $percentage = 25;

            if(!empty($avgPrice)) {
                $averagePlus = round(($avgPrice + ($percentage / 100) * $avgPrice),2);
                $averageMinus = round(($avgPrice - ($percentage / 100) * $avgPrice),2);
            }

            $propertyId = $favTripboard->pluck('property_id');
            $similarListing = $this->propertyRepository->simillarlisting($propertyId,$averageMinus,$averagePlus);
        }

        return ['potentialListing'=> $favTripboard,'similarListing' => $similarListing];

    }

    public function unlikeTripboard($favTripboard){
        return $this->favrioteTripboardRepository->removePropertyFromFav($favTripboard);
    }
}
