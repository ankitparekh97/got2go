<?php


namespace App\Services\Renter;

use App\Models\Property;

interface PropertyDetailServiceContract
{
    public function getPropertyDetailsById($propertyId);

    public function propertyCheckBooking($propertyId,$checkInDate,$checkOutDate,$needOfferValidation);

}
