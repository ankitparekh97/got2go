<?php


namespace App\Services\Renter;

use App\Models\Property;

interface FavoriteServiceContract
{
    public function getFavouriteTripboards($userId);

    public function unlikeTripboard($favTripboard);

}
