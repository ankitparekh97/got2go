<?php


namespace App\Services\Renter;

use App\Models\Property;

interface ChatServiceContract
{
    public function getRentalChatData($name);

    public function getMessages($pairId);

    public function unreadFalg($pairId,$toUserId);

    public function getOwnerChatData($name);

    public function getOwnerMessages($pairId);
}
