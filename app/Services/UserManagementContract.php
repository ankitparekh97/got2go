<?php
namespace App\Services;

interface UserManagementContract
{
    public function userList();

    public function userActivate($userId,$propertyId,$status);

    public function getownerPropertyById($id);
}
