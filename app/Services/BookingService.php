<?php


namespace App\Services;

use App\Helpers\GlobalConstantDeclaration;
use App\Helpers\Helper;
use App\Http\DTO\Api\BookingDTO;
use App\Http\DTO\Api\BookingPaymentDTO;
use App\Repositories\BookingInterface;
use App\Repositories\BookingPaymentInterface;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use App\Services\PaymentServices\Stripe\PaymentService;
use Carbon\Carbon;

class BookingService implements BookingContract
{
    public $paymentCardRepository;
    public $bookingPaymentRepository;

    public function __construct(
        BookingInterface $bookingRepository,
        BookingPaymentInterface $bookingPaymentRepository
    )
    {
        $this->bookingRepository = $bookingRepository;
        $this->bookingPaymentRepository = $bookingPaymentRepository;
    }

    public function getPropertyByOwnerId($ownerId,$status)
    {
        return $this->bookingRepository->getBookingsByOwnerId($ownerId,$status);
    }

    public function getByParamsObject($params)
    {
        return $this->bookingPaymentRepository->getByParamsObject($params);
    }

    public function getByParamsCollection($params)
    {
        return $this->bookingRepository->getByParamsCollection($params);
    }

    public function getBookingById($Id){
        return $this->bookingRepository->getBookingById($Id);
    }

    public function checkBookings($propertyId,$bookingId,$checkInDate,$checkoutDate){
        $cancelBookings = $this->bookingRepository->checkExistingBookings($propertyId,$bookingId,$checkInDate,$checkoutDate);

        $rejectbookings = [];
        if(count($cancelBookings)>0){
            foreach($cancelBookings as $item){
                if($item->check_out_date <= $checkInDate){
                    $rejectbookings[] = '';
                } else {
                    $rejectbookings[] = $item->id . ',' . $item->check_in_date . ' == ' . $item->check_out_date;
                    $this->updateBookingStatus($item->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
                }
            }
            array_filter($rejectbookings,'strlen');
        }
    }

    public function updateBookingStatus($bookingId,$status,$confirmationId){
        $isCancelled = '0'; $cancelledDate = '';


        if($status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED
        || $status == GlobalConstantDeclaration::PROPERTY_STATUS_REJECTED
        || $status == GlobalConstantDeclaration::BOOKING_STATUS_CONFIRMED){
            $statusUpdate = $status;
        }else if($status == GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED){
            $isCancelled = '1';
            $cancelledDate = Carbon::now();
            $statusUpdate = GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED;
        } else {
            $statusUpdate = GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED;
        }

        $bookingDTO = new BookingDTO();
        $bookingDTO->id = $bookingId;
        $bookingDTO->ownerStatus = $statusUpdate;
        $bookingDTO->isCancelled = $isCancelled;
        $bookingDTO->cancelledDate = $cancelledDate;
        $bookingDTO->confirmationId = $confirmationId;
        $bookingDTO->updatedDate = Carbon::now();
        $booking = $this->bookingRepository->updateBookingStatus($bookingDTO);
        return $booking;
    }

    public function stripePayment($bookings,$cardDetails,$status){

        $paymentService = new PaymentService();
        if($status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED || $status == GlobalConstantDeclaration::BOOKING_STATUS_CONFIRMED){
            if($bookings->owner_status != 'is_partial_payment_allowed'){

                $chargeRequestDTO = ChargeRequestDTO::convertModelToObject($bookings,$cardDetails);
                $charge = $paymentService->createCharge($chargeRequestDTO);

                if($status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED){
                    $this->checkBookings($bookings->property_id,$bookings->id,$bookings->check_in_date,$bookings->check_out_date);
                }
                $this->createBookingPayment($bookings,$bookings->total,$charge,GlobalConstantDeclaration::BOOKING_STATUS_COMPLETED);
            }
        }

        return true;
    }

    public function createBookingPayment($bookings,$amount,$charge,$status){
        $bookingPaymentDTO = new BookingPaymentDTO();
        $bookingPaymentDTO->bookingId = $bookings->id;
        $bookingPaymentDTO->renterUserId = $bookings->rental_user_id;
        $bookingPaymentDTO->paymentType = $status;
        $bookingPaymentDTO->transactionNumber = $charge['id'];
        $bookingPaymentDTO->amount = $amount;
        $bookingPaymentDTO->refundId = uniqid();
        $bookingPaymentDTO->refundAmount = 0;
        $bookingPaymentDTO->createdAt = Carbon::now();
        $bookingPaymentDTO->updatedAt = Carbon::now();
        $bookingPayment = $this->bookingPaymentRepository->createBookingPayment($bookingPaymentDTO);

        if($status == GlobalConstantDeclaration::BOOKING_STATUS_REFUNDED){
            $this->updateBookingStatus($bookings->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
        }

        return $bookingPayment;
    }

     public function updateBookingOwnerStatus($bookingId, $status){
        $bookingDTO = new BookingDTO;
        $bookingDTO->id = $bookingId;
        $bookingDTO->owner_status = 'cancelled';
        $this->bookingRepository->updateBookingStatus($bookingDTO);
    }

}
