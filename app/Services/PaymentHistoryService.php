<?php


namespace App\Services;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PaymentHistoryDTO;
use App\Repositories\BookingPaymentInterface;
use App\Repositories\PaymentCardInterface;
use App\Repositories\PaymentSavingAccountsInterface;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\PaymentService;

class PaymentHistoryService implements PaymentHistoryContract
{
    public $bookingPaymentRepository;
    public $paymentSavingAccountsRepository;

    public function __construct(
        BookingPaymentInterface $bookingPaymentRepository,
        PaymentSavingAccountsInterface $paymentSavingAccountsRepository
    )
    {
        $this->bookingPaymentRepository = $bookingPaymentRepository;
        $this->paymentSavingAccountsRepository = $paymentSavingAccountsRepository;
    }

    public function bookingHistory(){
        return $this->bookingPaymentRepository->bookingHistory();
    }

    public function paymentMethod($params){
        return $this->paymentSavingAccountsRepository->paymentMethod($params);
    }

    public function paymentMethodObject($params){
        return $this->paymentSavingAccountsRepository->paymentMethod($params);
    }

    public function checkDuplicateAccount($params,$accountNumber){
        $allCards = $this->paymentMethod($params);
        if($allCards) {
            foreach($allCards as $allCard) {
               if($allCard->account_number == $accountNumber) {
                    return true;
                }
            }
            return false;
        }
    }

    public function saveCard(PaymentHistoryDTO $paymentHistoryDTO,$account,$user){

        if(!$paymentHistoryDTO->id) {
            $paymentHistoryDTO->userType = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
            $paymentHistoryDTO->userId = $user->id;
            $paymentHistoryDTO->stripeAcountId = $account['id'];
            $paymentHistoryDTO->fingerPrint = $account['external_accounts']['data'][0]['fingerprint'];
        }

        $paymentHistoryDetails = $this->paymentSavingAccountsRepository->saveCard($paymentHistoryDTO);
        return $paymentHistoryDetails;
    }
    public function deletepaymentHistory($id){

        $card = $this->paymentSavingAccountsRepository->deletepaymentHistory($id);
        $PaymentService = new PaymentService();

        $cardRequestDTO = new CardRequestDTO();
        $cardRequestDTO->token = $card->stripe_account_id;
        $PaymentService->deleteBankAccount($cardRequestDTO);

        return $card;
    }
}
