<?php
namespace App\Services;
use App\Models\Configuration;
use DB;
use App\Repositories\ConfigurationInterface;

class ServiceFeeService implements ServiceFeeContract
{
    private $configurationRepository = null;
    public function __construct(
        ConfigurationInterface $configurationRepository
    ){
        $this->configurationRepository = $configurationRepository;
    }

    public function serviceFeeSave($servicefee,$serviceId){
        return $this->configurationRepository->serviceFeeSave($servicefee,$serviceId);
    }
}
