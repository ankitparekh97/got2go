<?php


namespace App\Services;

use App\Http\DTO\Api\PaymentHistoryDTO;

interface PaymentHistoryContract
{
    public function bookingHistory();

    public function paymentMethod($params);

    public function paymentMethodObject($params);

    public function checkDuplicateAccount($params,$accountNumber);

    public function saveCard(PaymentHistoryDTO $paymentHistoryDTO,$account,$user);

    public function deletepaymentHistory($id);
}
