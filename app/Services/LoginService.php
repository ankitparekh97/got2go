<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Repositories\RentalInterface;
use App\Repositories\OwnerInterface;
use App\Repositories\AdminInterface;
use App\Repositories\PaymentCardInterface;
use App\Repositories\CommunicationPreferencesInterface;
use Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Hash;

use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use JWTAuth;
use Exception;
use Illuminate\Support\Str;


class LoginService implements LoginServiceContract
{
    
    protected $bookingRepository;
    protected $propertyRepository;
    

    public function __construct(
        RentalInterface $rentalRepository,
        PaymentCardInterface $paymentCardRepository,
        OwnerInterface $ownerRepository,
        AdminInterface $adminRepository,
        CommunicationPreferencesInterface  $communicationPreferencesRepository  
    )
    {
        $this->rentalRepository = $rentalRepository;
        $this->paymentCardRepository = $paymentCardRepository;
        $this->ownerRepository = $ownerRepository;
        $this->adminRepository = $adminRepository;
        $this->communicationPreferencesRepository = $communicationPreferencesRepository;
    }

    public function userLogin($request)
    {
        $rentalIndividual = true;
        $ownerIndividual = true;
        $redirect = '';
        $is_tripper = '';
        $user ='';
        $owner='';
        $userId = '';

        if($request->phone != '' && $request->email != ''){
            $credentials = $request->only('email','phone', 'password');  
        }
        else if($request->email != ''){
            $credentials = $request->only('email', 'password');  
        }else if($request->phone != ''){
            $credentials = $request->only('phone', 'password');  
        }
    
        if (! $rentaltoken = Auth::guard('rentaluser')->attempt($credentials)) {
            $rentalIndividual = false;
        }
        if (! $ownertoken = Auth::guard('owner')->attempt($credentials)) {
            $ownerIndividual = false;
        } 
        if($ownerIndividual == false && $rentalIndividual == false){
            return ['success'=>false,'error' => array("Couldn't find an account with this number/email. Please try again with valid phone number/Email.")];
        }else if($ownerIndividual == true && $rentalIndividual == false){
            $owner = $this->ownerLogin($ownertoken);
        }else if($ownerIndividual == false && $rentalIndividual == true){
            $user = $this->rentalLogin($rentaltoken);
        }
        else {
            $owner = $this->ownerLogin($ownertoken);
            $user = $this->rentalLogin($rentaltoken);
        }

        if($user != ''){
            $userId = encrypt($user['id']);
            $is_tripper = ($user['is_tripper'] == '1' ? true : false);
            $redirect = $user['redirect'];
        }
        $propertyCount = getOwnerPropertyCount()->count();
        Session::put('timezone',$request->timezone);
        Session::save();

        return ['success'=>true , 'error'=> '','is_tripboard_redirect'=> $redirect,'is_tripper'=>$is_tripper , 'user'=>$user ,'owner'=>$owner, 'userId'=> $userId,"propertyCount"=>$propertyCount, 'session' => Session::all()];
    }

    public function ownerLogin($ownertoken)
    {
        $owner = Auth::guard('owner')->user()->toArray();
        Session::put('owner_token', $ownertoken);  // this alone won't write to session
        Session::put('owner',$owner['first_name']);
        Session::put('user',$owner);
        Session::save();
        return $owner;
    }

    public function rentalLogin($rentaltoken)
    {
        $user = Auth::guard('rentaluser')->user()->toArray();
        $userId = encrypt($user['id']);

        $billingDetails = $this->paymentCardRepository->getPaymentCardDetails($user['id']);
        $user['paymentcards_user'] = $billingDetails;
        $user['token'] = $rentaltoken;

        Session::put('rental_token', $rentaltoken);  // this alone won't write to session
        Session::save();
        $is_tripper = ($user['is_tripper'] == '1' ? true : false);
        $redirect = '';
        $user['redirect'] = $redirect;
        return $user;

    }

    public function adminLogin($request)
    {
        $credentials = $request->only('email', 'password');

        if (! $token = Auth::guard('admin')->attempt($credentials)) {
            return ['success'=>false,'error' => array('Email or password is invalid please try again')];
        }else{
            $admin = Auth::guard('admin')->user();
            Session::put('admin_token', $token);  // this alone won't write to session
            Session::put('admin', $admin);
            Session::save();
        }
    }

    public function forgotRenterTripperPassword($email){
        
            $genrate_password = randomPassword(8);
            $password = Hash::make($genrate_password);
            $rentalUser = $this->rentalRepository->updateUserPassword($email,$password);
            $owner = $this->ownerRepository->updateUserPassword($email,$password);

            $toMail = $email;
            $toName = $rentalUser->first_name.' '.$rentalUser->last_name;
            $emailRecord = DB::table('email_template')->where('task','rental_forgot_password')->first();
            $body = $emailRecord->message;
            $body = str_replace('{break}', '<br/>', $body);
            $body = str_replace('{footer_year}', date('Y'), $body);
            $body = str_replace('{site_url}', getLiveSiteUrl(), $body);
            $body = str_replace('{site_name}', 'Timeshare', $body);
            $body = str_replace('{logo_url}', 'Timeshare' , $body);
            $body = str_replace('{rental_name}', $toName, $body);
            $body = str_replace('{password}',$genrate_password,$body);

            send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);

            return response()->json(['success'=>true,'error'=> '','redirect_url'=> '/']);
    }


    public function forgotAdminPassword($email){
        
            $generatePassword = randomPassword(8);
            $password = Hash::make($generatePassword);

            $admin = $rentalUser = $this->adminRepository->updateUserPassword($email,$password);

            $toMail = $email;
            $toName = $admin->name;

            $emailRecord = DB::table('email_template')->where('task','admin_forgot_password')->first();

            $body = $emailRecord->message;
            $body = str_replace('{break}', '<br/>', $body);
            $body = str_replace('{footer_year}', date('Y'), $body);
            $body = str_replace('{site_url}', getLiveSiteUrl(), $body);
            $body = str_replace('{site_name}', 'Timeshare', $body);
            $body = str_replace('{logo_url}', 'Timeshare' , $body);
            $body = str_replace('{admin_name}', $toName, $body);
            $body = str_replace('{password}',$generatePassword, $body);

            send_email($body, $toMail, $toName, $emailRecord->subject, $emailRecord->from_address);
            Session::flash('success', 'A new password has been sent to your registered email!');
            return response()->json(['success'=>true, 'message' =>array('A new password has been sent to your registered email!'), 'redirect_url'=>url('/password/reset')]);
    }

    public function socialLogin($googleId=null,$facebookId=null,$email,$name,$loginfor){ 
        $fullName = explode(' ',$name);
        $firstName = $fullName[0];
        $lastName = $fullName[1]; 
        $findowner = $this->ownerRepository->getOwnerByGoogleId($googleId,$email);
        if($findowner){
            $token = Auth::guard('owner')->login($findowner);
            Session::put('owner_token', $token);  // this alone won't write to session
            Session::put('owner',$findowner->first_name);
            Session::save();

        }else{
            $ownerArray =[
                'firstName' => $firstName,
                'lastName' => $lastName,
                'email' =>$email,
                'password' => encrypt('12345678'),
                'isOtpVerified' => '1',
                'uniqueKey' => Str::random(50),
                'googleId'=> $googleId,
                'facebooId'=>$facebookId
            ];
            $owner = $this->ownerRepository->createUser((object)$ownerArray);
            $token = Auth::guard('owner')->login($owner);
            Session::put('owner_token', $token);  // this alone won't write to session
            Session::put('userType','owner');
            Session::put('owner',$firstName);
            Session::save();
        }
        $findrental = $this->rentalRepository->getRentalByGoogleId($googleId,$email);
        $isTripper = (strpos($loginfor,"tripper")==true) ? '1' : '0';
        if($findrental){
            $token = Auth::guard('rentaluser')->login($findrental);
            Session::put('rental_token', $token);  // this alone won't write to session
            Session::put('user', $findrental);
            Session::save();
            session()->flash('message', 'Your Google Login is successful !');

            if(strpos($loginfor,"tripper")){
                if(count($findrental->paymentcards_user)=="0"){
                    return redirect(route('tripper.singUpForm' , ['id'=> encrypt(Session::get('user')['id'])]));
                } 
                return redirect(route('tripper.dashboard' , ['id'=> encrypt(Session::get('user')['id'])]));
            } else {
                return redirect($loginfor);
            }

        }else{
            $rentalUserArray = [
                'firstName' =>$firstName,
                'lastName' => $lastName,
                'email' =>$email,
                'password' => encrypt('12345678'),
                'activationUrl' => Str::random(50),
                'isOtpVerified' => '1',
                'isVerified' => '1',
                'googleId'=> $googleId,
                'facebookId'=>$facebookId
            ];
            $rentaluser = $this->rentalRepository->createUser((object)$rentalUserArray);
            if(!empty($rentaluser)){
                $prefrnces = [
                    'userType' => 'rental',
                    'userid' => $rentaluser->id,
                    'communicationTitle' => 'account_support',
                    'isNotification' => '1',
                    'isEmail' => '1',
                    'isSms' => '0',
                    'createdAt' => Carbon::now(),
                ];
                $this->communicationPreferencesRepository->createCommunicationPreferences((object)$prefrnces);
            }

            $token = Auth::guard('rentaluser')->login($rentaluser);
            Session::put('rental_token', $token);  // this alone won't write to session
            Session::put('user', $rentaluser);
            Session::save();

            session()->flash('message', 'Your Google Sign Up is successful !');
            

            if(strpos($loginfor,"tripper")){
                if(count($rentaluser->paymentcards_user)=="0"){
                    return redirect(route('tripper.singUpForm' , ['id'=> encrypt(Session::get('user')['id'])]));
                } 
                return redirect(route('tripper.dashboard' , ['id'=> encrypt(Session::get('user')['id'])]));

            } else {
                return redirect($loginfor);
            }
        }

    }
}
