<?php


namespace App\Services;


use App\Http\DTO\Api\CouponCodeDTO;

interface CouponCodeContract
{
    public function getAllCouponCode();

    public function addCouponCode(CouponCodeDTO $couponCodeDTO);

    public function activeCouponCode($couponCodeId);

    public function deActiveCouponCode($couponCodeId);

    public function checkDuplicateCouponCode($couponCodeName);

    public function applyCouponCode($couponCodeName,$rentalId);

    public function numberOfTimesCouponCodeUsed($couponCodeId);

    public function numberOfTimesCouponCodeUsedByRentalId($couponCodeId,$rentalId);
}
