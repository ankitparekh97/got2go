<?php


namespace App\Services;


use App\Http\DTO\Api\MakeOfferDTO;
use Illuminate\Support\Carbon;

interface TransferPaymentServiceContract
{
        public function makeTransfer(Carbon $date);
}
