<?php
namespace App\Services;
use App\Models\Configuration;
use DB;
use App\Repositories\BookingPaymentInterface;
use App\Repositories\BookingInterface;
use Carbon\Carbon;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Services\SendMessageServiceContract;
use App\Helpers\Helper;

class TransactionLogsService implements TransactionLogsContract
{
    private $TransactionLogsRepository = null;
    public function __construct(
        BookingPaymentInterface  $bookingPaymentInterface,
        BookingInterface $bookingInterface,
        PaymentServiceInterface  $paymentService,
        SendMessageServiceContract $sendMessageServiceContract

    )
    {
        $this->bookingPaymentRepository = $bookingPaymentInterface;
        $this->paymentRepository =  $paymentService;
        $this->bookingRepository = $bookingInterface;
        $this->sendMessageService = $sendMessageServiceContract;
    }

    public function getTransactionLogs(){
        return $this->bookingPaymentRepository->getTransactionLogs();
    }


    public function refundBooking($input){
         $transaction = DB::transaction(function() use($input){
            $transactionId = $input['id'];
            $bookings = $this->bookingPaymentRepository->getBookingPaymentWithId($transactionId,null);
            $status = 'refunded';
            $amountRefund = 0;
            $amountRefund = $input['amount'];
            $chkId =  $input['chk_fixed'];

            if($chkId == 1 && $amountRefund > $bookings->amount) {
                return ['success'=>false,'message'=>'You cannot refund more than the total cost of the transaction'];
            }
            
            $amountRefund =  str_replace(",", "", $amountRefund);
            if ($chkId == 1) {
                $amountRefund =   round(($amountRefund),2);
            } else {
                $amountRefund = round(($amountRefund / 100) * $bookings->amount,2);
            }
            
            if($amountRefund){
                $refundRequestDTO = new RefundRequestDTO();
                $refundRequestDTO->amount = round($amountRefund,2);
                $refundRequestDTO->charge = $bookings->transaction_number;
                $refundRequestDTO->bookingId = $bookings->booking_id;
                $refund = $this->paymentRepository->createRefund($refundRequestDTO);

                $refund_array = [
                    'bookingId'=>$bookings->booking_id,
                    'renterUserId'=>$bookings->rental_user_id,
                    'paymentType'=>'refunded',
                    'amount'=>$amountRefund,
                    'transactionNumber'=>$refund['id'],
                    'createdAt'=>Carbon::now(),
                    'updatedAt'=>Carbon::now(),
                ];
                $refund = $this->bookingPaymentRepository->createBookingPayment((object)$refund_array);

                $this->bookingRepository->changeBookingStatus($bookings->booking->id, $status);

                /* Send Message */
                $toUser = Helper::urlsafe_b64encode($bookings->rental_user_id);
                $userMessage = "The admin has sent you a refund of $".$amountRefund." for your reservation ".$bookings->booking_id." from ".date('M jS, Y',strtotime($bookings->booking->check_in_date))." to ".date('M jS, Y',strtotime($bookings->booking->check_out_date));

                $this->sendMessageService->postAdminRentalSendMessage($toUser,$userMessage);
                /* End of Send Message */

                return ['success'=>true,'message'=>'Payment refunded successfully', 'booking_status'=>$status];
            }
        });
        return $transaction;
    }

}
