<?php


namespace App\Services;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PaymentCardDTO;
use App\Http\DTO\Api\RentalUserDTO;
use App\Http\DTO\Api\BookingDTO;
use App\Http\DTO\Api\RentalUserSubscriptionDTO;
use App\Models\SubscriptionPlan;
use App\Models\RentalUser;
use App\Repositories\PaymentCardInterface;
use App\Repositories\RentalInterface;
use App\Repositories\OwnerInterface;
use App\Repositories\RentalUserSubscriptionInterface;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;

use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;
use App\Repositories\PropertyInterface;
use App\Repositories\BookingInterface;
use App\Repositories\SubscriptionInterface;
use App\Services\BookingAvaibilityServiceContract;
use App\Repositories\BookingPaymentInterface;
use App\Repositories\PropertyAmenitiesInterface;
use App\Repositories\CommunicationPreferencesInterface;
use DB;
use App\Helpers\Helper;
use Carbon\Carbon;
use App\Services\SendMessageServiceContract;

class RentalUserService implements RentalUserServiceContract
{
    public $rentalRepository;
    public $subscriptionPlan;
    public $paymentCardRepository;
    public $rentalUserSubscription;

    public function __construct(
        RentalInterface $rentalRepository,
        OwnerInterface $ownerRepository,
        SubscriptionPlanContract $subscriptionPlan,
        PaymentCardInterface $paymentCardRepository,
        RentalUserSubscriptionInterface $rentalUserSubscription,
        PropertyInterface $propertyRepository,
        BookingInterface $bookingRepository,
        SubscriptionInterface $subscriptionRepository,
        BookingAvaibilityServiceContract $bookingAvaibilityServiceContract,
        BookingPaymentInterface $bookingPaymentInterface,
        PropertyAmenitiesInterface $propertyAmenitiesInterface,
        CommunicationPreferencesInterface $communicationPreferencesInterface,
        SendMessageServiceContract $sendMessageServiceContract

    )
    {
        $this->rentalRepository = $rentalRepository;
        $this->ownerRepository = $ownerRepository;
        $this->subscriptionPlan = $subscriptionPlan;
        $this->paymentCardRepository = $paymentCardRepository;
        $this->rentalUserSubscription = $rentalUserSubscription;
        $this->propertyRepository = $propertyRepository;
        $this->bookingRepository = $bookingRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->bookingAvaibilityServiceContract = $bookingAvaibilityServiceContract;
        $this->bookingPaymentRepository = $bookingPaymentInterface;
        $this->propertyAmenitiesRepository = $propertyAmenitiesInterface;
        $this->communicationPreferencesRepository = $communicationPreferencesInterface;
        $this->sendMessageService = $sendMessageServiceContract;


    }

    public function registerTripper(RentalUserDTO $rentalUserDTO, int $rentalUserId)
    {

        DB::transaction(function () use($rentalUserDTO,$rentalUserId){

            /**
             * @var SubscriptionPlan $getLatestSubscription
             */
            $getLatestSubscription = $this->subscriptionPlan->getLatestSubscription();

            // do subscription and do database entry
            /**
             * @var RentalUser $rentalUser
             */
            $rentalUser = $this->rentalRepository->createPaymentGatewayCustomer($rentalUserId);

            $createdCard = $this->rentalRepository->createPaymentGatewayCard($rentalUserId,$rentalUserDTO->paymentCard);
            // storing card detail to payment card
            $paymentCardDTO = new PaymentCardDTO;
            $paymentCardDTO->userType = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
            $paymentCardDTO->nameOnCard = $rentalUserDTO->paymentCard->nameOnCard ?? $rentalUser->first_name;
            $paymentCardDTO->email = $rentalUserDTO->email ?? $rentalUser->email;
            $paymentCardDTO->cardType = $createdCard['brand'];
            $paymentCardDTO->lastFour = $createdCard['last4'];
            $paymentCardDTO->default = true;
            $paymentCardDTO->billingPhone = $rentalUserDTO->paymentCard->billingPhone;
            $paymentCardDTO->token = $rentalUserDTO->paymentCard->token;
            $paymentCardDTO->cardToken = $createdCard['id'];
            $paymentCardDTO->expirationDate = $createdCard['exp_month'].'/'.$createdCard['exp_year'];
            $this->paymentCardRepository->savePaymentCard($paymentCardDTO,$rentalUserId);

            /**
             * @var SubscriptionResponseDTO $subscriptionResponse
             */
            $subscriptionResponse = $this->rentalRepository->createPaymentGatewaySubscription($rentalUserId,$getLatestSubscription->id);
            $subscriptionDTO = new RentalUserSubscriptionDTO;
            $subscriptionDTO->rentalId = $rentalUserId;
            $subscriptionDTO->subscriptionId = $subscriptionResponse->id;
            $subscriptionDTO->planId = $subscriptionResponse->plan->id;
            $subscriptionDTO->price = $subscriptionResponse->plan->amount;
            $subscriptionDTO->status = $subscriptionResponse->status;
            $subscriptionDTO->billingPeriodStartDate = $subscriptionResponse->billingPeriodStartDate;
            $subscriptionDTO->billingPeriodEndDate = $subscriptionResponse->billingPeriodEndDate;
            $subscriptionDTO->firstBillingDate = $subscriptionResponse->createdAt;
            $subscriptionDTO->nextBillingAmount = $subscriptionResponse->billingPeriodEndDate;
            $subscriptionDTO->nextBillingAmount = $subscriptionResponse->plan->amount;
            $subscriptionDTO->paidThroughDate = $subscriptionResponse->billingPeriodStartDate;
            $this->rentalUserSubscription->saveRentalUserSubscription($subscriptionDTO);

            /**
             * @var RentalUser $rentalUser
             */
            $rentalUser = $this->rentalRepository->saveAsTripper($rentalUserDTO,$rentalUserId);

            return $rentalUser;

        });
    }

    public function getHotDealsByLimit()
    {
        return $this->propertyRepository->getHotDealsByLimit();
    }

    public function getUserById($userId)
    {
        return  $this->rentalRepository->getById($userId);
    }

    public function getOwnerById($userId)
    {
        return  $this->ownerRepository->getOwnerById($userId);
    }
    public function getCardDetails($id)
    {
        return $this->paymentCardRepository->getPaymentCardDetails($id);
    }

    public function saveSetting($input)
    {
        $id = $input['id'];
        if($rentalUser = $this->getUserById($id))
        {
            $this->rentalRepository->updateRentalUserSetting($input);
            $this->ownerRepository->updateOwnerUserSetting($input);
            return ['success'=>true,'message'=>'Profile settings updated'];
        } else {
            return ['success'=>false,'error'=>array('Rental user not found!')];
        }
    }


    public function getMyStay($userId)
    {
        $upcomingStays = $this->bookingRepository->getUpcomingStays($userId);

        $pastStays = $this->bookingRepository->getPastStays($userId);

        $subscription = $this->subscriptionRepository->getSubscriptionById(1);
        $discount = $subscription->discount_percentage;


        return ['url' => 'rentaluser.mystay','upcomingStays' => $upcomingStays, 'pastStays' => $pastStays, 'discount' => $discount];
    }

    public function mystayDetails($propertyId,$bookingId, $userId)
    {
        $bookingDetails = $this->bookingRepository->getBookingDetailByPropertyId($propertyId,$bookingId);

        $showStAdd = false;
        $checkbooking = $this->bookingRepository->checkBookingByUser($userId,$propertyId);
                if(!empty($checkbooking->toarray())){
                    $showStAdd = true;
                }

        $paymnetStatus = $this->bookingPaymentRepository->getLatestBookingPayment($bookingId);
        $paymnetDate = '';
        if($paymnetStatus){
            $paymnetDate = $paymnetStatus->created_at;
            $paymnetStatus = $paymnetStatus->paymentType;
        }
        $amenityParam = ['property_id'=>$propertyId];
        $propertyAmenities = $this->propertyAmenitiesRepository->getPropertyAmenitiesByParamsCollection($amenityParam);
        $bookingCheckInDate = carbonParseDateFormatMDY($bookingDetails->check_in_date);
        $bookingCheckOutDate = carbonParseDateFormatMDY($bookingDetails->check_out_date);

        $subscription = $this->subscriptionRepository->getSubscriptionById(1);
        $discount = $subscription->discount_percentage;

        /* Dynamic cancellation text according cancellation policy */
        $dynamicCancellationText = cancellationText($bookingDetails,$paymnetDate);
        return [
            'url' => 'rentaluser.mystay-details',
            'bookingDetails' => $bookingDetails,
            'propertyAmenities' => $propertyAmenities,
            'bookingCheckInDate' => $bookingCheckInDate,
            'bookingCheckOutDate' => $bookingCheckOutDate,
            'checkbooking'=>$checkbooking,
            'discount' =>$discount,
            'showStAdd'=>$showStAdd,
            'paymnetStatus'=>$paymnetStatus,
            'cancellationText'=>$dynamicCancellationText,
            ];
    }

    public function getRentalUserWithCommunicationPref($userId)
    {
       return  $this->rentalRepository->getRentalUserWithCommunicationPref($userId);
    }

    public function saveNotificationSettings($userId, $input)
    {
       
        $subscriptionStatus = isset($input['is_subscribed']) ? 1 : 0;
        $this->rentalRepository->updateUserSubscriptionStatus($userId, $subscriptionStatus);
       
        $trashCurrentPreferences = $this->communicationPreferencesRepository->deleteCommunicationPreferencesByUserId($userId);

        foreach($input['prefernces'] as $key=>$value){

            $notificatonPreferences = [
                'userType' => 'rental',
                'userid' => $userId,
                'communicationTitle' => $key,
                'isNotification' => (isset($value['is_notification'])) ? '1' : '0',
                'isEmail' => (isset($value['is_email'])) ? '1' : '0',
                'isSms' => (isset($value['is_sms'])) ? '1' : '0',
                'createdAt' => Carbon::now(),
            ];

            $this->communicationPreferencesRepository->createCommunicationPreferences((Object)$notificatonPreferences);
        }

       return true;

    }

    public function updateProfilePic($userId, $imageName){
        $this->rentalRepository->updateProfilePic($userId, $imageName);
        return $this->rentalRepository->updateProfilePic($userId, $imageName);
    }

    public function SavePaymentCards($card, $user, $token){
        $paymentCardDTO = new PaymentCardDTO;
        $paymentCardDTO->userType = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $paymentCardDTO->nameOnCard = $user->first_name.' '.$user->last_name;
        $paymentCardDTO->email =  $user->email;
        $paymentCardDTO->cardType = $card['brand'] ? $card['brand'] : 'Credit Card';
        $paymentCardDTO->lastFour = $card['last4'];
        $paymentCardDTO->token = $token;
        $paymentCardDTO->cardToken = $card['id'];
        $paymentCardDTO->expirationDate = $card['exp_month'].'/'.$card['exp_year'];
        $paymentCardDTO->fingerPrint = $card['fingerprint'];
        $paymentCardDTO->userId = $user->id;
        $userId = $user->id;
        return $this->paymentCardRepository->savePaymentCard($paymentCardDTO, $userId);
    }

    public function updateCustomerId($customerId, $userId)
    {
        return $this->rentalRepository->updateCustomerId($customerId, $userId);
    }

    public function deletePaymentCard($id, $customerId){

        $paymentDetails = $this->paymentCardRepository->getPaymentCardDetailById($id);
        $cardRequestDTO = new CardRequestDTO();
        $cardRequestDTO->token = $paymentDetails->card_token;
        $cardRequestDTO->customerId = $customerId;
        return $this->paymentCardRepository->deletePaymentCard($paymentDetails->id);

    }

    public function checkDuplicateCard($token, $userId) {
        $duplicate_found = false;
        $allCards = $this->paymentCardRepository->getPaymentCardDetails($userId);
        if($allCards) {
            foreach($allCards as $allCard) {
                $token_fingerprint = $token['fingerprint'];
                $token_exp_month = $token['exp_month'];
                $token_exp_year = $token['exp_year'];
                if ($allCard->fingerprint == $token_fingerprint) {
                        $exp_month = trim(\explode('/',$allCard->expiration_date)[0]);
                        $exp_year = trim(\explode('/',$allCard->expiration_date)[1]);
                    if (($exp_month == $token_exp_month) && ($exp_year == $token_exp_year)) {
                        $duplicate_found = true;
                        break;
                    }
                    $duplicate_found = true;
                    break;
                }
            }
        }
        return $duplicate_found;
    }

    public function calculateRefund( $bookingId)
    {
        $booking = $this->bookingRepository->getBookingById($bookingId);
        if($booking->bookingPayment == null){
            $this->updateBookingOwnerStatus($booking->id, 'cancelled');

            return[
            'refundAmount' => 0,
            'penidngRequest'=>'yes',
            ];

        }
        $bookingChcekInDate = $booking->check_in_date.' '.date("H:i:s", strtotime($booking->property->check_in));
        $bookingChcekOutDate = $booking->check_out_date.' '.date("H:i:s", strtotime($booking->property->check_out));
        $checkInDate = carbonParseDate($bookingChcekInDate,'America/Chicago');
        $checkOutDate = carbonParseDate($bookingChcekOutDate,'America/Chicago');

        $paymentCreatedDate = $booking->bookingPayment->created_at;
        $days = carbonDateDiffInDays($checkInDate,$checkOutDate);
        $cleanningFee =  $booking->cleaning_fee;
        $startingPrice = $booking->actual_price;
        $cancellationType = $booking->property->cancellation_type;
        $totalAmount = $booking->total;
        $transactionFee = $booking->transaction_fee;
        $refundAmount = calculateRenterRefund($checkInDate,$days,$startingPrice,$cancellationType,$paymentCreatedDate,$cleanningFee,$totalAmount,$transactionFee);
        return [
            'refundAmount' => $refundAmount,
            'penidngRequest'=>'',
            ];
    }

    public function updateBookingOwnerStatus($bookingId, $status){
        $bookingDTO = new BookingDTO;
        $bookingDTO->id = $bookingId;
        $bookingDTO->owner_status = 'cancelled';
        $this->bookingRepository->updateBookingStatus($bookingDTO);
    }

    public function cancelBooking($bookingId, $refundAmmount, $user){
        $bookings = $this->bookingPaymentRepository->getBookingPaymentWithId(null,$bookingId);
        if($bookings->payment_type == 'completed')
        {
            $status = 'refunded';
            if($refundAmmount != 0){
                $refundRequestDTO = new RefundRequestDTO();
                $refundRequestDTO->amount = round($refundAmmount,2);
                $refundRequestDTO->charge = $bookings->transaction_number;
                $refundRequestDTO->bookingId = $bookings->booking_id;
                $PaymentService = new PaymentService();
                $refund = $PaymentService->createRefund($refundRequestDTO);

                $bookingPaymentArray = [
                    'bookingId' => $bookings->id,
                    'renterUserId' => $bookings->rental_user_id,
                    'paymentType' => 'refunded',
                    'transactionNumber' => $refund['id'],
                    'amount' => $refundAmmount,
                    'createdAt' => Carbon::now(),
                    'updatedAt' => Carbon::now(),
                ];

                $this->bookingPaymentRepository->createBookingPayment((object)$bookingPaymentArray);
                $this->updateBookingOwnerStatus($bookings->booking->id, $status);


                /* Send Message */
                $toUser = Helper::urlsafe_b64encode($bookings->booking->property->owner_id);
                $userMessage = $user->first_name." ". $user->last_name." has canceled the reservation for ".$bookings->booking->property->title." from ".date('M jS, Y',strtotime($bookings->booking->check_in_date))." to ".date('M jS, Y',strtotime($bookings->booking->check_out_date)).". A total of $".$refundAmmount." will be refunded back to the guest.";

                $this->sendMessageService->postRentalOwnerSendMessage($toUser,$userMessage);
                /* End of Send Message */

                /* Send Message */
                $toUser = Helper::urlsafe_b64encode($user->id);
                $userMessage = "You have canceled the reservation for ".$bookings->booking->property->title." from ".date('M jS, Y',strtotime($bookings->booking->check_in_date))." to ".date('M jS, Y',strtotime($bookings->booking->check_out_date)).". A total of $".$refundAmmount." will be refunded.";

                $this->sendMessageService->postAdminRentalSendMessage($toUser,$userMessage);
                /* End of Send Message */

                return response()->json(['success'=>true,'message'=>'Payment refunded successfully', 'booking_status'=>$status]);
            }else{
                $status = 'cancelled';

                $this->updateBookingOwnerStatus($bookings->booking->id, $status);

                return response()->json(['success'=>true,'message'=>'Payment refunded successfully', 'booking_status'=>$status]);
            }
        }else{
            return response()->json(['success'=>true,'message'=>'', 'booking_status'=>'']);
        }
    }

    public function saveRenterUser(RentalUserDTO $rentalUserDTO,$userId = null){
        return $this->rentalRepository->saveRenterUser($rentalUserDTO, $userId);
    }

}
