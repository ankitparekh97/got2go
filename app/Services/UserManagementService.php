<?php
namespace App\Services;
use App\Models\Configuration;
use DB;
use App\Repositories\OwnerInterface;
use App\Repositories\PropertyInterface;

class UserManagementService implements UserManagementContract
{
    private $UserManagementRepository = null;
    public function __construct(
        OwnerInterface  $ownerInterface,
        PropertyInterface $propertyInterface
    )
    {
        $this->ownerInterface = $ownerInterface;
        $this->propertyService = $propertyInterface;
    }

    public function userList(){
        return $this->ownerInterface->userList();
    }

    public function userActivate($userId,$propertyId,$status){
        return $this->ownerInterface->userActivate($userId,$propertyId,$status);
    }

    public function getownerPropertyById($id){
        return $this->propertyService->getownerPropertyById($id);
    }


}
