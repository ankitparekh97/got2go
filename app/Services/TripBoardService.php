<?php


namespace App\Services;
use App\Repositories\TripBoardInterface;
use App\Repositories\TripBoardPropertyInterface;
use App\Repositories\TripBoardVoteInterface;
use App\Repositories\TripBoardBuddyInterface;
use App\Repositories\TripBoardSharedInterface;
use App\Repositories\SubscriptionInterface;
use App\Repositories\FavrioteTripboardInterface;
use App\Repositories\PropertyInterface;
use App\Repositories\GroupMessagesInterface;
use Helper;
use Auth;
use Carbon\Carbon;
use Session;

class TripBoardService implements TripBoardContract
{
    public function __construct(
        TripBoardInterface $tripBoardRepository,
        TripBoardPropertyInterface $tripBoardPropertyRepository,
        SubscriptionInterface $subscriptionRepository,
        PropertyInterface $PropertyRepository,
        GroupMessagesInterface $groupMessagesRepository,
        FavrioteTripboardInterface $favrioteTripboardRepository,
        PropertyInterface $propertyRepository,
        TripBoardVoteInterface $tripBoardVoteRepository,
        TripBoardBuddyInterface $tripBoardBuddyRepository,
        TripBoardSharedInterface $tripBoardSharedRepository
    )
    {
      $this->tripBoardRepository = $tripBoardRepository;
      $this->subscriptionRepository = $subscriptionRepository;
      $this->PropertyRepository = $PropertyRepository;
      $this->groupMessagesRepository = $groupMessagesRepository;
      $this->favrioteTripboardRepository = $favrioteTripboardRepository;
      $this->propertyRepository = $propertyRepository;
      $this->tripBoardPropertyRepository = $tripBoardPropertyRepository;
      $this->tripBoardVoteRepository = $tripBoardVoteRepository;
      $this->tripBoardBuddyRepository = $tripBoardBuddyRepository;
      $this->tripBoardSharedRepository = $tripBoardSharedRepository;

    }

    public function getTripBoardById(int $tripBoardId,array $with = array())
    {
        return $this->tripBoardRepository->getTripBoardById($tripBoardId,$with);
    }

    public function getUserTripBoards($userId)
    {
        return $this->tripBoardRepository->getUserTripBoards($userId);
    }

    public function getFavouriteTripBoards($userId)
    {
        return $this->favrioteTripboardRepository->getFavouriteTripBoards($userId);
    }

    public function getTripBoardPropertyImages($id)
    {
        return $this->propertyRepository->getTripBoardPropertyImages($id);
    }

    public function addTripboard($tripboardName, $userId, $propertyId)
    {
        $insertId = NULL;
        $getTripboardId = $this->tripBoardRepository->addTripboard($tripboardName, $userId);
        if($getTripboardId!=''){
            $insertId = $getTripboardId;
        }

        if(isset($propertyId) && $propertyId != ''){
            $tripboardId = $insertId;
            $this->tripBoardPropertyRepository->addPropertyToTripboard($tripboardId, $propertyId);
        }
        return $insertId;

    }

    function tripboarddetail($tripboardId, $userId){

        $tripboard = $this->tripBoardRepository->getTripBoardById($tripboardId);
        if(!$tripboard)
        {
            return ['redirect_url' =>true];
        }
        $travelbuddies = $this->tripBoardBuddyRepository->getTravelBuddies($tripboardId);
        $potentialListing = $this->tripBoardRepository->getTripBoardProperties($tripboardId);
        $similarListing =  $this->propertyRepository->similarListing($tripboard);


        $discount =  $this->subscriptionRepository->getSubscription();
        $discount = $discount->discount_percentage;
        return ['tripboard'=>$tripboard, 'travelbuddies' =>$travelbuddies,'potentialListing' => $potentialListing,'discount' => $discount,'similarListing' => $similarListing];

    }
    public function saveTripboardDetail($input)
    {
        return $this->tripBoardRepository->saveTripboardDetail($input);
    }

    public function saveTravelBuddy($input, $userId)
    {
        return $this->tripBoardBuddyRepository->saveTravelBuddy((Object)$input, $userId);
    }

    function deleteTravelBuddy($buddyId){
        return $this->tripBoardBuddyInterface->deleteTravelBuddy($buddyId);
    }

    public function likeUnlikeProperties($param)
    {
        return $this->favrioteTripboardRepository->likeUnlikeProperties($param);
    }

    public function AddOrRemovePropertiesFromTripboard($param)
    {
        return $this->tripBoardPropertyRepository->AddOrRemovePropertiesFromTripboard($param);
    }

    public function voteTripboard($param)
    {
        return $this->tripBoardVoteRepository->voteTripboard($param);
    }

    public function tripBoardRemoveListing($input)
    {
        return $this->tripBoardPropertyRepository->tripBoardRemoveListing((Object)$input);
    }

    
    public function postSendMessage($tripboardId,$userMessage)
    {
        $re = '/(\(?\d{3}\D{0}? ?[()\.-]? ?\d{3}\D{0}?[()\.-]? ?\d{0,10}).*?/i';
        $subst = '**********';
        $userMessage = preg_replace($re, $subst, $userMessage);
        $re = '/(gmail|mail|proton|\bcom(?!\S)|outlook|yahoo|@)/i';

        preg_match_all($re, $userMessage, $matches, PREG_SET_ORDER, 0);
        if($matches){
            $userMessage =  '**********';
        }
        $messageArray = [
            'tripboard_id' => Helper::urlsafe_b64decode($tripboardId),
            'from_user' => Auth::guard('rentaluser')->user()->id,
            'messages' => $userMessage,
            'is_read'=>0,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ];
        return $messageArray;
    }

    public function getLoadLatestMessages($tripboardId)
    {
        
        $messages = $this->groupMessagesRepository->getMessages($tripboardId);
        $return = [];
        if($messages){
            $messgaeArray = [];
            foreach($messages as $message){
                if (!array_key_exists(date('Y-m-d',strtotime($message->created_at)),$messgaeArray)){
                    $messgaeArray[date('Y-m-d',strtotime($message->created_at))] = [];
                }
                array_push($messgaeArray[date('Y-m-d',strtotime($message->created_at))],$message);
            }
            $messages = $messgaeArray;
            foreach ($messages as $key=>$message) {
                $today = Helper::convertToLocal(Carbon::today()->toDateTimeString(),Session::get('timezone'));
                $yesterday = Helper::convertToLocal(Carbon::yesterday()->toDateTimeString(),Session::get('timezone'));
                $messageDate = Helper::convertToLocal($key.'00:00:00',Session::get('timezone'));
                if($today->toDateString() == $messageDate->toDateString()){
                    $date = 'Today';
                }else if($yesterday->toDateString() == $messageDate->toDateString()){
                    $date = 'Yesterday';
                }else{
                    $date = Carbon::parse($messageDate->toDateString())->format('F d, y');
                }
                $return[] = view('rentaluser.tripboard-message-line')->with(['messages'=>$message,'date'=>$date])->render();
            }
        }
        return $return;
    }

    public function checkForSharedTripBoard($id, $userId)
    {
        $tripBoardDetail = $this->tripBoardRepository->getTripBoardById($id);
        $loggedinUserId = $userId;
        $tripBoardUserId = NULL;
        if(!empty($tripBoardDetail)){
            $tripBoardUserId = $tripBoardDetail->user_id;
        }

        if($tripBoardUserId != $loggedinUserId){
            $checkIfThisTripBoardSharedToLoggedInUser = $this->tripBoardSharedRepository->checkForSharedTripBoard($id, $userId);
            if(empty($checkIfThisTripBoardSharedToLoggedInUser)){
                return [
                    'shared' => true,
                    'TripBoard_detail' => $tripBoardDetail
                ];
            }
        }
        return [
            'shared' => false,
            'TripBoard_detail' => $tripBoardDetail
        ];
    }

    public function saveTripBoard($tripboardId, $userId)
    {
        return $this->tripBoardSharedRepository->saveSharedTripBoard($tripboardId, $userId);
    }

    /**
     * @param $userId
     * @param $propertyId
     * @return mixed
     */
    public function getTriboardsWithPropertyId($userId,$propertyId)
    {
        return $this->tripBoardRepository->getTriboardsWithPropertyId($userId,$propertyId);
    }
}
