<?php
namespace App\Services;
use Auth;
use DateTime;
use Carbon\Carbon;
use DB;
use Session;

use App\Models\Property;
use App\Repositories\PropertyInterface;

class SearchService implements SearchContract
{
    private $geoCode = null;
    private $propertyRepository = null;
    public function __construct(PropertyInterface $propertyRepository){
        $this->propertyRepository = $propertyRepository;
        $this->geoCode = env("GEOCODE");
    }

    public function generateQuery($input){
        return  $this->propertyRepository->generateQuery($input);
    }


    public function selectDataFromQuery($input, $isRentalUser = false){
        return  $this->propertyRepository->selectDataFromQuery($input, $isRentalUser);
    }

    public function searchProperty($input, $isRentalUser = false)
    {
        $q = $this->generateQuery($input);
        $selectArr = $this->selectDataFromQuery($input, $isRentalUser);
        return  $this->propertyRepository->searchProperty($q, $selectArr,  $input, $isRentalUser);

    }

    public function searchPropertyCount($input)
    {
        return $this->propertyRepository->searchPropertyCount($input);
    }

    public function getHotDeals($input)
    {
        return $this->propertyRepository->getHotDeals($input);
    }

    public function getHotDealsByLimit()
    {
        return $this->propertyRepository->getHotDealsByLimit();
    }

}
