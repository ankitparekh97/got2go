<?php


namespace App\Services\PaymentServices;



use App\Services\PaymentServices\Braintree\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionRequestDTO;

interface BrainTreePaymentServiceInterface
{

    public function getAllPlans();

    public function createCustomer(CustomerRequestDTO $params);

    public function editCustomer($clientId,$params);

    public function findCustomer($clientId);

    public function createCreditCard($clientId,$params);

    public function updateCreditCard($creditCardTokenId,$params);

    public function findCreditCard($creditCardTokenId);

    public function createSubscription(SubscriptionRequestDTO $params);

    public function updateSubscription($params);

    public function cancelSubscription($subscriptionId);

    public function getSubscriptionById($id);

    public function createPaymentMethod(PaymentMethodRequestDTO $params);
    public function deletePaymentMethod($token);

    /**
     * @param float $amount
     * @param string $paymentMethodNonce
     * @param array $options
     * @return mixed
     */
    public function doPayment(
        float $amount,
        string $paymentMethodNonce,
        array $options
    );
}
