<?php


namespace App\Services\PaymentServices;

use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionParamsDTO;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use App\Services\PaymentServices\Stripe\Account\AccountRequestDTO;
use App\Services\PaymentServices\Stripe\Transfer\TransferRequestDTO;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;

interface PaymentServiceInterface
{
    public function createToken(TokenRequestDTO $params);

    public function createCustomer(CustomerRequestDTO $params,$returnAsObj = false);

    public function editCustomer($clientId,$params);

    public function findCustomer($clientId);

    public function createCreditCard($clientId,$params);

    public function updateCreditCard($creditCardTokenId,$params);

    public function findCreditCard($creditCardTokenId);

    public function createPlan(PlanRequestDTO $planRequestDTO);

    public function createSubscription($customerId,SubscriptionParamsDTO $subscriptionParamsDTO);

    public function updateSubscription($params);

    public function cancelSubscription($customerId,$subscriptionId);

    public function pauseSubscription($customerId,$subscriptionId);

    public function reactiveSubscription($customerId,$subscriptionId);

    public function getSubscriptionById($id);

    public function deletePaymentMethod($token);

    public function createCharge(ChargeRequestDTO $params);

    public function createAccount(AccountRequestDTO $params);

    public function createTransfer(TransferRequestDTO $params);

    public function createCard(CardRequestDTO $params);

    public function createRefund(RefundRequestDTO $params);
}
