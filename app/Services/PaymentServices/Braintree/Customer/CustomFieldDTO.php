<?php


namespace App\Services\PaymentServices\Braintree\Customer;


use Spatie\DataTransferObject\DataTransferObject;

class CustomFieldDTO extends DataTransferObject
{
    /**
     * @var string $key
     */
    public $key;

    /**
     * @var string $value
     */
    public $value;

    public static function convertToObject($key,$value) {
        return new self([
            $key => $value
        ]);
    }
}
