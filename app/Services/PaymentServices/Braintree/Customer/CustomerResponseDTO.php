<?php


namespace App\Services\PaymentServices\Braintree\Customer;


use App\Services\PaymentServices\Braintree\Address\AddressResponseDTO;
use Spatie\DataTransferObject\DataTransferObject;

class CustomerResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var \App\Services\PaymentServices\Braintree\Address\AddressResponseDTO[]|null
     */
    public $addresses;

    /**
     * @var string|null $company
     */
    public $company;

    /**
     * @var string|null $firstName
     */
    public $firstName;

    /**
     * @var string|null $lastName
     */
    public $lastName;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $phone
     */
    public $phone;

    /**
     * @var \App\Services\PaymentServices\Braintree\Customer\CustomFieldDTO[]|null
     */
    public $customFields;

    /**
     * @var \App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO[]|null
     */
    public $creditCards;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var \Illuminate\Support\Carbon|null $updatedAt
     */
    public $updatedAt;

    public static function convertObject($customer)
    {
        $customerArray = array();

        $customerArray['id'] = $customer->id;
        $customerArray['company'] = $customer->company;
        $customerArray['firstName'] = $customer->firstName;
        $customerArray['lastName'] = $customer->lastName;
        $customerArray['email'] = $customer->email;
        $customerArray['phone'] = $customer->phone;
        $customerArray['createdAt'] = carbonParseDate($customer->createdAt);
        $customerArray['updatedAt'] = carbonParseDate($customer->updatedAt);

        if(!empty($customer->addresses)){

            $addressesArray = array();

            foreach ($customer->addresses as $address) {
                array_push($addressesArray,AddressResponseDTO::convertObject($address));
            }

            $customerArray['addresses'] = $addressesArray;
        }

        /*if(!empty($customer->creditCards)){

            $creditCardsArray = array();

            foreach ($customer->creditCards as $creditCard) {
                array_push($creditCardsArray,AddressResponseDTO::convertObject($creditCard));
            }

            $customerArray['creditCards'] = $creditCardsArray;
        }*/

        if(!empty($customer->customFields)){

            $customFieldsArray = array();

            foreach ($customer->creditCards as $customField) {
                array_push($customFieldsArray,AddressResponseDTO::convertObject($customField));
            }

            $customerArray['customFields'] = $customFieldsArray;
        }

        return new self($customerArray);

    }
}
