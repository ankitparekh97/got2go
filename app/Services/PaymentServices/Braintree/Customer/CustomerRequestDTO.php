<?php


namespace App\Services\PaymentServices\Braintree\Customer;


class CustomerRequestDTO
{
    /**
     * @var string|null $company
     */
    public $company;

    /**
     * @var string|null $firstName
     */
    public $firstName;

    /**
     * @var string|null $lastName
     */
    public $lastName;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $phone
     */
    public $phone;

    /**
     * @var \App\Services\PaymentServices\Braintree\Customer\CustomFieldDTO[]|null
     */
    public $customFields;
}
