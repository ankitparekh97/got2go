<?php


namespace App\Services\PaymentServices\Braintree\Transaction;


use App\Services\PaymentServices\Braintree\Address\AddressResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomFieldDTO;
use Spatie\DataTransferObject\DataTransferObject;

class TransactionResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var string|null $additionalProcessorResponse
     */
    public $additionalProcessorResponse;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var \App\Services\PaymentServices\Braintree\Address\AddressResponseDTO[]|null $billingDetails
     */
    public $billingDetails;

    /**
     * @var string|null $channel
     */
    public $channel;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var \App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO|null $creditCardDetails
     */
    public $creditCardDetail;

    /**
     * @var string|null $channel
     */
    public $currencyIsoCode;

    /**
     * @var \App\Services\PaymentServices\Braintree\Customer\CustomFieldDTO|null $customFields
     */
    public $customFields;

    /**
     * @var \App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO|null $customerDetails
     */
    public $customerDetails;

    /**`
     * @var string|null $cvvResponseCode
     */
    public $cvvResponseCode;

    /**`
     * @var string|null $gatewayRejectionReason
     */
    public $gatewayRejectionReason;

    /**`
     * @var string|null $processorAuthorizationCode
     */
    public $processorAuthorizationCode;

    /**`
     * @var string|null $taxAmount
     */
    public $taxAmount;

    /**`
     * @var bool|null $taxExempt
     */
    public $taxExempt;

    /**`
     * @var string|null $subscriptionId
     */
    public $subscriptionId;

    /**`
     * @var bool|null $recurring
     */
    public $recurring;

    /**`
     * @var string|null $paymentInstrumentType
     */
    public $paymentInstrumentType;

    public static function convertObject($transaction)
    {
        $transactionArray = array();

        $transactionArray['id'] = $transaction->id;
        /*$transactionArray['status'] = $transaction->status;
        $transactionArray['type'] = $transaction->type;
        $transactionArray['additionalProcessorResponse'] = $transaction->additionalProcessorResponse;
        $transactionArray['price'] = $transaction->price;
        $transactionArray['channel'] = $transaction->channel;
        $transactionArray['createdAt'] = carbonParseDate($transaction->createdAt);
        $transactionArray['currencyIsoCode'] = $transaction->currencyIsoCode;
        $transactionArray['gatewayRejectionReason'] = $transaction->gatewayRejectionReason;
        $transactionArray['processorAuthorizationCode'] = $transaction->processorAuthorizationCode;
        $transactionArray['processorResponseCode'] = $transaction->processorResponseCode;
        $transactionArray['processorResponseText'] = $transaction->processorResponseText;
        $transactionArray['taxAmount'] = $transaction->taxAmount;
        $transactionArray['taxExempt'] = $transaction->taxExempt;
        $transactionArray['subscriptionId'] = $transaction->subscriptionId;
        $transactionArray['recurring'] = $transaction->recurring;

        if(!empty($transaction->billingDetails)){
            $transactionArray['billingDetails'] = AddressResponseDTO::convertObject($transaction->billingDetails);
        }

        if(!empty($transaction->creditCardDetails)){
            $transactionArray['creditCardDetails'] = AddressResponseDTO::convertObject($transaction->creditCardDetails);
        }

        if(!empty($transaction->customFields)){

            $customFieldsArr = array();
            foreach ($transaction->customFields as $key => $value){
                array_push($customFieldsArr,CustomFieldDTO::convertToObject($key,$value));
            }
            $transactionArray['creditCardDetails'] = $customFieldsArr;

        }

        if(!empty($transaction->customerDetails)){

            $customerDetailsArr = array();
            foreach ($transaction->customerDetails as $customerDetail){
                array_push($customerDetailsArr,CustomerResponseDTO::convertObject($customerDetail));
            }
            $transactionArray['customerDetails'] = $customerDetailsArr;

        }*/

        return new self($transactionArray);
    }
}
