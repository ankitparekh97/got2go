<?php


namespace App\Services\PaymentServices\Braintree\PaymentMethod;


use Spatie\DataTransferObject\DataTransferObject;

class PaymentMethodRequestDTO extends DataTransferObject
{
    /**
     * @var \App\Services\PaymentServices\Braintree\Address\AddressRequestDTO|null $billingAddress
     */
    public $billingAddress;

    /**
     * @var string|null $cardholderName
     */
    public $cardholderName;

    /**
     * @var string|null $customerId
     */
    public $customerId;

    /**
     * @var string|null $paymentMethodNonce
     */
    public $paymentMethodNonce;

}
