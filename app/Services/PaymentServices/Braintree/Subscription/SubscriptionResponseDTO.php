<?php


namespace App\Services\PaymentServices\Braintree\Subscription;


use App\Services\PaymentServices\Braintree\Transaction\TransactionResponseDTO;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Ref: https://developers.braintreepayments.com/reference/response/subscription/php
 * Class SubscriptionResponseDTO
 * @package App\Services\PaymentServices\Braintree\Subscirption
 */
class SubscriptionResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $balance
     */
    public $balance;

    /**
     * @var int|null $billingDayOfMonth
     */
    public $billingDayOfMonth;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodEndDate
     */
    public $billingPeriodEndDate;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodStartDate
     */
    public $billingPeriodStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var \Illuminate\Support\Carbon|null $firstBillingDate
     */
    public $firstBillingDate;

    /**
     * @var int|null $currentBillingCycle
     */
    public $currentBillingCycle;

    /**
     * @var int|null $currentBillingCycle
     */
    public $daysPastDue;

    /**
     * @var string|null $description
     */
    public $description;

    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var bool|null $neverExpires
     */
    public $neverExpires;

    /**
     * @var \Illuminate\Support\Carbon|null $nextBillingDate
     */
    public $nextBillingDate;

    /**
     * @var string|null $nextBillAmount
     */
    public $nextBillAmount;

    /**
     * @var string|null $nextBillingPeriodAmount
     */
    public $nextBillingPeriodAmount;

    /**
     * @var string|null $numberOfBillingCycles
     */
    public $numberOfBillingCycles;

    /**
     * @var \Illuminate\Support\Carbon|null $paidThroughDate
     */
    public $paidThroughDate;

    /**
     * @var string|null $paymentMethodToken
     */
    public $paymentMethodToken;

    /**
     * @var string|null $planId
     */
    public $planId;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var string|null $status
     */
    public $status;

    /**
     * @var int|null $failureCount
     */
    public $failureCount;

    /**
     * @var \Illuminate\Support\Carbon|null $updatedAt
     */
    public $updatedAt;

    /**
     * @var \App\Services\PaymentServices\Braintree\Subscription\SubscriptionStatusHistoryResponseDTO[]|null $statusHistory
     */
    public $statusHistory;

    /**
     * @var \App\Services\PaymentServices\Braintree\Transaction\TransactionResponseDTO[]|null $transactions
     */
    public $transactions;

    public static function convertObject($subscription)
    {
        $subscriptionArr['balance'] = $subscription->balance;
        $subscriptionArr['billingDayOfMonth'] = $subscription->billingDayOfMonth;
        $subscriptionArr['billingPeriodEndDate'] = carbonParseDate($subscription->billingPeriodEndDate);
        $subscriptionArr['billingPeriodStartDate'] = carbonParseDate($subscription->billingPeriodStartDate);
        $subscriptionArr['createdAt'] = carbonParseDate($subscription->createdAt);
        $subscriptionArr['firstBillingDate'] = carbonParseDate($subscription->firstBillingDate);
        $subscriptionArr['currentBillingCycle'] = $subscription->currentBillingCycle;
        $subscriptionArr['daysPastDue'] = $subscription->daysPastDue;
        $subscriptionArr['description'] = $subscription->description;
        $subscriptionArr['id'] = $subscription->id;
        $subscriptionArr['neverExpires'] = $subscription->neverExpires;
        $subscriptionArr['nextBillingDate'] = carbonParseDate($subscription->nextBillingDate);
        $subscriptionArr['nextBillingPeriodAmount'] = $subscription->nextBillingPeriodAmount;
        $subscriptionArr['numberOfBillingCycles'] = $subscription->numberOfBillingCycles;
        $subscriptionArr['paidThroughDate'] = carbonParseDate($subscription->paidThroughDate);
        $subscriptionArr['paymentMethodToken'] = $subscription->paymentMethodToken;
        $subscriptionArr['planId'] = $subscription->planId;
        $subscriptionArr['price'] = $subscription->price;
        $subscriptionArr['status'] = $subscription->status;
        $subscriptionArr['updatedAt'] = carbonParseDate($subscription->updatedAt);

        if(!empty($subscription->statusHistory)){
            $statusHistoryArr = array();
            foreach ($subscription->statusHistory as $statusHistory){
                array_push($statusHistoryArr,SubscriptionStatusHistoryResponseDTO::convertToObject($statusHistory));
            }
            $subscriptionArr['statusHistory'] = $statusHistoryArr;
        }

        if(!empty($subscription->transactions)){
            $transactionsArr = array();
            foreach ($subscription->transactions as $transactions){
                array_push($transactionsArr,TransactionResponseDTO::convertObject($transactions));
            }
            $subscriptionArr['transactions'] = $transactionsArr;
        }

        return new self($subscriptionArr);
    }
}
