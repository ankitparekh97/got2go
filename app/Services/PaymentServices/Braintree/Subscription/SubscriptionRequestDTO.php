<?php


namespace App\Services\PaymentServices\Braintree\Subscription;


use Spatie\DataTransferObject\DataTransferObject;

/**
 * Ref https://developers.braintreepayments.com/reference/request/subscription/create/php#payment_method_token
 * Class SubscriptionRequestDTO
 * @package App\Services\PaymentServices\Braintree\Subscirption
 */
class SubscriptionRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $paymentMethodNonce
     */
    public $paymentMethodNonce;

    /**
     * @var string|null $paymentMethodToken
     */
    public $paymentMethodToken;

    /**
     * @var string|null $planId
     */
    public $planId;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var bool|null $neverExpires
     */
    public $neverExpires;
}
