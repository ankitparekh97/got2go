<?php


namespace App\Services\PaymentServices\Braintree\Subscription;


use Spatie\DataTransferObject\DataTransferObject;

class SubscriptionStatusHistoryResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $status
     */
    public $status;

    /**
     * @var string|null $balance
     */
    public $balance;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var string|null $currencyIsoCode
     */
    public $currencyIsoCode;

    /**
     * @var string|null $planId
     */
    public $planId;

    public static function convertToObject($subscriptionStatus)
    {
        $subscriptionStatusArr = array();
        $subscriptionStatusArr['status'] = $subscriptionStatus->status;
        $subscriptionStatusArr['balance'] = $subscriptionStatus->balance;
        $subscriptionStatusArr['price'] = $subscriptionStatus->price;
        $subscriptionStatusArr['currencyIsoCode'] = $subscriptionStatus->currencyIsoCode;
        $subscriptionStatusArr['planId'] = $subscriptionStatus->planId;

        return new self($subscriptionStatusArr);
    }
}
