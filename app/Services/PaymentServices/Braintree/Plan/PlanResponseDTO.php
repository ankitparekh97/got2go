<?php


namespace App\Services\PaymentServices\Braintree\Plan;


use Illuminate\Support\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

class PlanResponseDTO extends DataTransferObject
{
    /**
     * @var int|null $billingDayOfMonth
     */
    public $billingDayOfMonth;

    /**
     * @var int|null $billingFrequency
     */
    public $billingFrequency;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var string|null $currencyIsoCode
     */
    public $currencyIsoCode;

    /**
     * @var string|null $description
     */
    public $description;

    /**
     * @var \App\Services\PaymentServices\Braintree\Plan\DiscountResponseDTO[]|null $discount
     */
    public $discount;

    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $numberOfBillingCycles
     */
    public $numberOfBillingCycles;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var \Illuminate\Support\Carbon|null $updatedAt
     */
    public $updatedAt;

    public static function createObject($plans)
    {
        return new self([
            'billingDayOfMonth' => $plans->billingDayOfMonth,
            'billingFrequency' => $plans->billingFrequency,
            'createdAt' => carbonParseDate($plans->createdAt),
            'currencyIsoCode' => $plans->currencyIsoCode,
            'description' => $plans->description,
            'discount' => null,
            'id' => $plans->id,
            'name' => $plans->name,
            'numberOfBillingCycles' => $plans->numberOfBillingCycles,
            'price' => $plans->price,
            'updatedAt' => carbonParseDate($plans->updatedAt),
        ]);
    }

}
