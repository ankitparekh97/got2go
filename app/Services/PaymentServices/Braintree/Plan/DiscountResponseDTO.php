<?php


namespace App\Services\PaymentServices\Braintree\Plan;


use Spatie\DataTransferObject\DataTransferObject;

class DiscountResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var int|null $currentBillingCycle
     */
    public $currentBillingCycle;

    /**
     * @var string|null $description
     */
    public $description;

    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var string|null $kind
     */
    public $kind;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var bool|null $neverExpires
     */
    public $neverExpires;

    /**
     * @var int|null $numberOfBillingCycles
     */
    public $numberOfBillingCycles;

    /**
     * @var int|null $quantity
     */
    public $quantity;
}
