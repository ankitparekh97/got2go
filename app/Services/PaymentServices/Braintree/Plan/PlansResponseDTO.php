<?php


namespace App\Services\PaymentServices\Braintree\Plan;


use Spatie\DataTransferObject\DataTransferObject;

class PlansResponseDTO extends DataTransferObject
{
    /**
     * @var \App\Services\PaymentServices\Braintree\Plan\PlanResponseDTO[]|null $planResponse
     */
    public $planResponse;

    public static function createObject($plans)
    {
        $plansArray = array();
        foreach ($plans as $plan){
            array_push($plansArray,PlanResponseDTO::createObject($plan));
        }

        return new self([
            'planResponse' => $plansArray
        ]);
    }
}
