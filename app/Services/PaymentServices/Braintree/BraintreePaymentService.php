<?php


namespace App\Services\PaymentServices\Braintree;


use App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Braintree\Plan\PlansResponseDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionResponseDTO;
use App\Services\PaymentServices\BrainTreePaymentServiceInterface;

class BraintreePaymentService implements BrainTreePaymentServiceInterface
{
    private $gateWay;

    public function __construct()
    {
        //dd(5);
        $this->gateWay = new \Braintree\Gateway([
            'environment' => env('BRAINTREE_ENV','sandbox'),
            'merchantId' => env('BRAINTREE_MERCHANT_ID'),
            'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
            'privateKey' => env('BRAINTREE_PRIVATE_KEY')
        ]);
    }

    public function getClientToken()
    {
        
        return $this->gateWay->clientToken()->generate();
    }

    public function getAllPlans()
    {
        $plans = $this->gateWay->plan()->all();
        return PlansResponseDTO::createObject($plans);
    }

    public function getFirstPlan()
    {
        $plans = $this->getAllPlans();
        return $plans->planResponse[0];
    }

    public function createCustomer(CustomerRequestDTO $params)
    {
        $customerData = array();

        if($params->firstName !== null){
            $customerData['firstName'] = $params->firstName;
        }
        if($params->lastName !== null){
            $customerData['lastName'] = $params->lastName;
        }
        if($params->email !== null){
            $customerData['email'] = $params->email;
        }
        if($params->phone !== null){
            $customerData['phone'] = $params->phone;
        }
        if($params->customFields !== null){
            $customerData['customFields'] = $params->customFields;
        }

        $customer = $this->gateWay->customer()->create($customerData);
        return CustomerResponseDTO::convertObject($customer->customer);
    }

    public function editCustomer($clientId, $params)
    {
        // TODO: Implement editClient() method.
    }

    public function findCustomer($clientId = 216336541)
    {
        $customer = $this->gateWay->customer()->find($clientId);
        return CustomerResponseDTO::convertObject($customer);
    }

    public function createCreditCard($clientId, $params)
    {
        // TODO: Implement createCreditCard() method.
    }

    public function updateCreditCard($creditCardTokenId, $params)
    {
        // TODO: Implement updateCreditCard() method.
    }

    public function findCreditCard($creditCardTokenId)
    {
        // TODO: Implement findCreditCard() method.
    }

    public function createSubscription(SubscriptionRequestDTO $params)
    {
        $subscriptionData = array();

        if($params->paymentMethodNonce !== null){
            $subscriptionData['paymentMethodNonce'] = $params->paymentMethodNonce;
        }
        if($params->paymentMethodToken !== null){
            $subscriptionData['paymentMethodToken'] = $params->paymentMethodToken;
        }

        $plan = $this->getFirstPlan();
        $subscriptionData['planId'] = $plan->id;

/*        if($params->planId !== null){
            //$subscriptionData['planId'] = $params->planId;
            $subscriptionData['planId'] = $this->planId;
        }*/
        if($params->price !== null){
            $subscriptionData['price'] = $params->price;
        }
        if($params->neverExpires !== null){
            $subscriptionData['neverExpires'] = $params->neverExpires;
        }

        $subscription = $this->gateWay->subscription()->create($subscriptionData);
        return SubscriptionResponseDTO::convertObject($subscription->subscription);
    }

    public function cancelSubscription($id)
    {
        $subscription = $this->gateWay->subscription()->cancel($id);
        // need to handle our side database logic using webhook
        if($subscription->success) {
            return SubscriptionResponseDTO::convertObject($subscription->subscription);
        }
        return null;
    }

    public function updateSubscription($params)
    {
        // TODO: Implement updateSubscription() method.
    }

    public function getSubscriptionById($id)
    {
        $subscription = $this->gateWay->subscription()->find('b54pzm');
        return SubscriptionResponseDTO::convertObject($subscription);
    }

    public function createPaymentMethod(PaymentMethodRequestDTO $params)
    {
        $paymentMethod = array();

        if($params->billingAddress !== null){
            $paymentMethod['billingAddress'] = array();

            if($params->billingAddress->company !== null){
                $paymentMethod['billingAddress']['company'] = $params->billingAddress->company;
            }
            if($params->billingAddress->countryCodeAlpha2 !== null){
                $paymentMethod['billingAddress']['countryCodeAlpha2'] = $params->billingAddress->countryCodeAlpha2;
            }
            if($params->billingAddress->countryCodeAlpha3 !== null){
                $paymentMethod['billingAddress']['countryCodeAlpha3'] = $params->billingAddress->countryCodeAlpha3;
            }
            if($params->billingAddress->countryCodeNumeric !== null){
                $paymentMethod['billingAddress']['countryCodeNumeric'] = $params->billingAddress->countryCodeNumeric;
            }
            if($params->billingAddress->countryName !== null){
                $paymentMethod['billingAddress']['countryName'] = $params->billingAddress->countryName;
            }
            if($params->billingAddress->customerId !== null){
                $paymentMethod['billingAddress']['customerId'] = $params->billingAddress->customerId;
            }
            if($params->billingAddress->extendedAddress !== null){
                $paymentMethod['billingAddress']['extendedAddress'] = $params->billingAddress->extendedAddress;
            }
            if($params->billingAddress->firstName !== null){
                $paymentMethod['billingAddress']['firstName'] = $params->billingAddress->firstName;
            }
            if($params->billingAddress->lastName !== null){
                $paymentMethod['billingAddress']['lastName'] = $params->billingAddress->lastName;
            }
            if($params->billingAddress->locality !== null){
                $paymentMethod['billingAddress']['locality'] = $params->billingAddress->locality;
            }
            if($params->billingAddress->postalCode !== null){
                $paymentMethod['billingAddress']['postalCode'] = $params->billingAddress->postalCode;
            }
            if($params->billingAddress->region !== null){
                $paymentMethod['billingAddress']['region'] = $params->billingAddress->region;
            }
            if($params->billingAddress->streetAddress !== null){
                $paymentMethod['billingAddress']['streetAddress'] = $params->billingAddress->streetAddress;
            }
        }

        if($params->paymentMethodNonce !== null){
            $paymentMethod['paymentMethodNonce'] = $params->paymentMethodNonce;
        }

        if($params->cardholderName !== null){
            $paymentMethod['cardholderName'] = $params->cardholderName;
        }

        if($params->customerId !== null){
            $paymentMethod['customerId'] = $params->customerId;
        }

        $paymentMethod['options'] = array();
        $paymentMethod['options']['verifyCard'] = true;
        //dd($paymentMethod);
        $paymentMethod = $this->gateWay->paymentMethod()->create($paymentMethod);

        if($paymentMethod->success){
            $paymentMethodClass = get_class($paymentMethod->paymentMethod);
            if($paymentMethodClass === 'Braintree\CreditCard'){
                return CreditCardResponseDTO::convertObject($paymentMethod->paymentMethod);
            }
        }

        return null;
    }

    public function doPayment(float $amount, string $paymentMethodNonce, array $options)
    {
        $this->gateWay->transaction()->sale([

        ]);
    }

    public function deletePaymentMethod($token)
    {   
        $paymentMethod = $this->gateWay->paymentMethod()->delete($token);        
        if($paymentMethod->success){            
            return true;            
        }
        return null;
    }
}
