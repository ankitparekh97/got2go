<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


use Spatie\DataTransferObject\DataTransferObject;

/**
 * Ref: https://developers.braintreepayments.com/reference/response/credit-card-verification/php
 * Class CreditCardVerificationResponseDTO
 * @package App\Services\PaymentServices\Braintree\CreditCard
 */
class CreditCardVerificationResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var string|null $currencyIsoCode
     */
    public $currencyIsoCode;

    /**
     * @var string|null $currencyIsoCode
     */
    public $cvvResponseCode;

    /**
     * @var string|null $gatewayRejectionReason
     */
    public $gatewayRejectionReason;

    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var string|null $merchantAccountId
     */
    public $merchantAccountId;

    /**
     * @var string|null $networkResponseCode
     */
    public $networkResponseCode;

    /**
     * @var string|null $networkResponseText
     */
    public $networkResponseText;

    /**
     * @var string|null $processorResponseCode
     */
    public $processorResponseCode;

    /**
     * @var string|null $processorResponseText
     */
    public $processorResponseText;

    /**
     * @var string|null $processorResponseType
     */
    public $processorResponseType;

    /**
     * @var string|null $processorResponseType
     */
    public $status;

    public static function convertToObject($creditCardVerification)
    {
        $creditCardVerificationArr = array();
        $creditCardVerificationArr['amount'] = $creditCardVerification->amount;
        $creditCardVerificationArr['createdAt'] = carbonParseDate($creditCardVerification->createdAt);
        $creditCardVerificationArr['currencyIsoCode'] = $creditCardVerification->currencyIsoCode;
        $creditCardVerificationArr['cvvResponseCode'] = getCardCvvResponseCode($creditCardVerification->cvvResponseCode);
        $creditCardVerificationArr['gatewayRejectionReason'] = $creditCardVerification->gatewayRejectionReason;
        $creditCardVerificationArr['id'] = $creditCardVerification->id;
        $creditCardVerificationArr['merchantAccountId'] = $creditCardVerification->merchantAccountId;
        $creditCardVerificationArr['networkResponseCode'] = $creditCardVerification->networkResponseCode;
        $creditCardVerificationArr['networkResponseText'] = $creditCardVerification->networkResponseText;
        $creditCardVerificationArr['processorResponseCode'] = $creditCardVerification->processorResponseCode;
        $creditCardVerificationArr['processorResponseText'] = $creditCardVerification->processorResponseText;
        $creditCardVerificationArr['processorResponseType'] = $creditCardVerification->processorResponseType;
        $creditCardVerificationArr['status'] = $creditCardVerification->status;

        return new self($creditCardVerificationArr);
    }

}
