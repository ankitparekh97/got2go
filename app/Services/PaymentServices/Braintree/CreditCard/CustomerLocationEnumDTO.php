<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


use Spatie\DataTransferObject\DataTransferObject;

class CustomerLocationEnumDTO extends DataTransferObject
{
    /**
     * @var bool|null
     */
    public $creditCardInternational;

    /**
     * @var bool|null
     */
    public $creditCardUS;
}
