<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;



use App\Services\PaymentServices\Braintree\Address\AddressResponseDTO;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Ref : https://developers.braintreepayments.com/reference/response/credit-card/php
 * Class CreditCardResponseDTO
 * @package App\Services\PaymentServices\Braintree\CreditCard
 */
class CreditCardResponseDTO extends DataTransferObject
{
    /**
     * @var \App\Services\PaymentServices\Braintree\Address\AddressResponseDTO|null $billingAddress
     */
    public $billingAddress;

    /**
     * @var string|null $bin
     */
    public $bin;

    /**
     * @var string|null $cardType
     */
    public $cardType;

    /**
     * @var string|null $cardHolderName
     */
    public $cardholderName;

    /**
     * @var string|null $commercial
     */
    public $commercial;

    /**
     * @var string|null $countryOfIssuance
     */
    public $countryOfIssuance;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var string|null $customerId
     */
    public $customerId;

    /**
     * @var string|null $customerLocation
     */
    public $customerLocation;

    /**
     * @var string|null $debit
     */
    public $debit;

    /**
     * @var bool|null $default
     */
    public $default;

    /**
     * @var string|null $expirationDate
     */
    public $expirationDate;

    /**
     * @var string|null $expirationDate
     */
    public $expirationMonth;

    /**
     * @var string|null $expirationYear
     */
    public $expirationYear;

    /**
     * @var bool|null $expired
     */
    public $expired;

    /**
     * @var string|null $imageUrl
     */
    public $imageUrl;

    /**
     * @var string|null $issuingBank
     */
    public $issuingBank;

    /**
     * @var string|null $last4
     */
    public $last4;

    /**
     * @var string|null $maskedNumber
     */
    public $maskedNumber;

    /**
     * @var string|null $productId
     */
    public $productId;

    /**
     * @var string|null $token
     */
    public $token;

    /**
     * @var string|null $token
     */
    public $uniqueNumberIdentifier;

    /**
     * @var string|null $token
     */
    public $globalId;

    /**
     * @var string|null $token
     */
    public $customerGlobalId;

    /**
     * @var \Illuminate\Support\Carbon|null $token
     */
    public $updatedAt;

    /**
     * @var \App\Services\PaymentServices\Braintree\CreditCard\CreditCardVerificationResponseDTO|null $verification
     */
    public $verification;

    public static function convertObject($creditCard)
    {
        $creditCardArray = array();
        $creditCardArray['bin'] = $creditCard->bin;
        $creditCardArray['cardType'] = $creditCard->cardType;
        $creditCardArray['cardholderName'] = $creditCard->cardholderName;
        $creditCardArray['commercial'] = $creditCard->commercial;
        $creditCardArray['countryOfIssuance'] = $creditCard->countryOfIssuance;
        $creditCardArray['createdAt'] = carbonParseDate($creditCard->createdAt);
        $creditCardArray['customerId'] = $creditCard->customerId;
        $creditCardArray['customerLocation'] = $creditCard->customerLocation;
        $creditCardArray['debit'] = $creditCard->debit;
        $creditCardArray['default'] = $creditCard->default;
        $creditCardArray['expirationDate'] = $creditCard->expirationDate;
        $creditCardArray['expirationMonth'] = $creditCard->expirationMonth;
        $creditCardArray['expirationYear'] = $creditCard->expirationYear;
        $creditCardArray['expired'] = $creditCard->expired;
        $creditCardArray['last4'] = $creditCard->last4;
        $creditCardArray['maskedNumber'] = $creditCard->maskedNumber;
        $creditCardArray['productId'] = getCardProductName($creditCard->productId);
        $creditCardArray['token'] = $creditCard->token;
        $creditCardArray['uniqueNumberIdentifier'] = $creditCard->uniqueNumberIdentifier;
        $creditCardArray['updatedAt'] = carbonParseDate($creditCard->updatedAt);
        if(!empty($creditCard->billingAddress)){
            $creditCardArray['billingAddress'] = AddressResponseDTO::convertObject($creditCard->billingAddress);
        }
        if(!empty($creditCard->verification)){
            $creditCardArray['verification'] = CreditCardVerificationResponseDTO::convertToObject($creditCard->verification);
        }
        return new self($creditCardArray);
    }
}
