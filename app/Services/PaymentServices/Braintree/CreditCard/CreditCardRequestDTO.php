<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


use Spatie\DataTransferObject\DataTransferObject;

class CreditCardRequestDTO extends DataTransferObject
{
    /**
     * @var string|null
     */
    public $billingAddressId;

    /**
     * @var string|null
     */
    public $cardHolderName;

    /**
     * @var string
     */
    public $customerId;

    /**
     * @var string|null
     */
    public $cvv;

    /**
     * @var \Illuminate\Support\Carbon|null
     */
    public $expirationDate;

    /**
     * @var string|null
     */
    public $expirationMonth;

    /**
     * @var string|null
     */
    public $expirationYear;

    /**
     * @var \App\Services\PaymentServices\Braintree\CreditCard\CreditCardRequestOptionDTO|null
     */
    public $options;

    /**
     * @var string|null
     */
    public $paymentMethod;

}
