<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


use Spatie\DataTransferObject\DataTransferObject;

class CreditCardRequestOptionDTO extends DataTransferObject
{
    /**
     * @var bool|null
     */
    public $verifyCard;
}
