<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


class CreditCardCommercialEnumResponseDTO
{
    /**
     * @var bool|null
     */
    public $commercialYes;

    /**
     * @var bool|null
     */
    public $commercialNo;

    /**
     * @var bool|null
     */
    public $commercialUnknown;
}
