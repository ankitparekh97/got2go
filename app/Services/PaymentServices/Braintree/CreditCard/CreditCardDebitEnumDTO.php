<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


class CreditCardDebitEnumDTO
{
    /**
     * @var bool|null
     */
    public $debitYes;

    /**
     * @var bool|null
     */
    public $debitNo;

    /**
     * @var bool|null
     */
    public $debitUnknown;
}
