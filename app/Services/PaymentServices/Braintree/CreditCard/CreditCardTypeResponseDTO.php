<?php


namespace App\Services\PaymentServices\Braintree\CreditCard;


class CreditCardTypeResponseDTO
{
    /**
     * @var bool|null
     */
    public $americanExpress;

    /**
     * @var bool|null
     */
    public $carteBlanche;

    /**
     * @var bool|null
     */
    public $chinaUnionPay;

    /**
     * @var bool|null
     */
    public $discovery;

    /**
     * @var bool|null
     */
    public $elo;

    /**
     * @var bool|null
     */
    public $jcb;

    /**
     * @var bool|null
     */
    public $laser;

    /**
     * @var bool|null
     */
    public $maestro;

    /**
     * @var bool|null
     */
    public $masterCard;

    /**
     * @var bool|null
     */
    public $solo;

    /**
     * @var bool|null
     */
    public $switch;

    /**
     * @var bool|null
     */
    public $visa;


    /**
     * @var bool|null
     */
    public $unknown;
}
