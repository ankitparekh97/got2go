<?php


namespace App\Services\PaymentServices\Braintree\Address;


use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class AddressResponseDTO
 * @package App\Services\PaymentServices\Braintree\Address
 */
class AddressResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $company
     */
    public $id;

    /**
     * @var string|null $company
     */
    public $company;

    /**
     * @var string|null $countryCodeAlpha2
     */
    public $countryCodeAlpha2;

    /**
     * @var string|null $countryCodeAlpha3
     */
    public $countryCodeAlpha3;

    /**
     * @var string|null $countryCodeNumeric
     */
    public $countryCodeNumeric;

    /**
     * @var string|null $countryName
     */
    public $countryName;

    /**
     * @var string|null $customerId
     */
    public $customerId;

    /**
     * @var string|null $extendedAddress
     */
    public $extendedAddress;

    /**
     * @var string|null $firstName
     */
    public $firstName;

    /**
     * @var string|null $lastName
     */
    public $lastName;

    /**
     * @var string|null $locality
     */
    public $locality;

    /**
     * @var string|null $postalCode
     */
    public $postalCode;

    /**
     * @var string|null $region
     */
    public $region;

    /**
     * @var string|null $streetAddress
     */
    public $streetAddress;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var \Illuminate\Support\Carbon|null $updatedAt
     */
    public $updatedAt;

    public static function convertObject($address)
    {
        $addressArray = array();

        $addressArray['id'] = $address->id;
        $addressArray['company'] = $address->company;
        $addressArray['countryCodeAlpha2'] = $address->countryCodeAlpha2;
        $addressArray['countryCodeAlpha3'] = $address->countryCodeAlpha3;
        $addressArray['countryCodeNumeric'] = $address->countryCodeNumeric;
        $addressArray['countryName'] = $address->countryName;
        $addressArray['customerId'] = $address->customerId;
        $addressArray['extendedAddress'] = $address->extendedAddress;
        $addressArray['firstName'] = $address->firstName;
        $addressArray['lastName'] = $address->lastName;
        $addressArray['locality'] = $address->locality;
        $addressArray['postalCode'] = $address->postalCode;
        $addressArray['region'] = $address->region;
        $addressArray['streetAddress'] = $address->streetAddress;
        $addressArray['createdAt'] = carbonParseDate($address->createdAt);
        $addressArray['updatedAt'] = carbonParseDate($address->updatedAt);

        return new self($addressArray);

    }

}
