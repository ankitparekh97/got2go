<?php


namespace App\Services\PaymentServices\Stripe\Plan;


use Spatie\DataTransferObject\DataTransferObject;

class PlanResponseDTO extends DataTransferObject
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var int $amount
     */
    public $amount;

    /**
     * 3-letter ISO code
     * @var string $currency
     */
    public $currency;

    /**
     * week,month or year
     * @var string $interval
     */
    public $interval;

    /**
     * @var integer|null $interval_count
     */
    public $intervalCount;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string|null $trialPeriodDays
     */
    public $trialPeriodDays;

    /**
     * @var string|null $statementDescriptor
     */
    public $statementDescriptor;

    /**
     * @var \App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO[]|null
     */
    public $metaData;

    public static function createObject($plan)
    {
        return new self([
            'id' => $plan['id'],
            'amount' => $plan['amount'],
            'currency' => $plan['currency'],
            'interval' => $plan['interval'],
            'intervalCount' => $plan['interval_count'],
            'name' => $plan['name'],
            'trialPeriodDays' => $plan['trial_period_days'],
            'statementDescriptor' => $plan['statement_descriptor'],
            'metaData' => $plan['metadata'],
        ]);
    }

}
