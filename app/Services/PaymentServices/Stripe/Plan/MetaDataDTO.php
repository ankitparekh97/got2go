<?php


namespace App\Services\PaymentServices\Stripe\Plan;


use Spatie\DataTransferObject\DataTransferObject;

class MetaDataDTO extends DataTransferObject
{
    /**
     * @var string $key
     */
    public $key;

    /**
     * @var string $value
     */
    public $value;

    public static function convertToObject($key,$value) {
        return new self([
            $key => $value
        ]);
    }
}
