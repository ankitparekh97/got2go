<?php


namespace App\Services\PaymentServices\Stripe\Plan;


use Spatie\DataTransferObject\DataTransferObject;

class PlanRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var float|null $amount
     */
    public $amount;

    /**
     * 3-letter ISO code
     * @var string|null $currency
     */
    public $currency;

    /**
     * week,month or year
     * @var string|null $interval
     */
    public $interval;

    /**
     * @var integer|null $interval_count
     */
    public $intervalCount;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $name
     */
    public $trialPeriodDays;

    /**
     * @var string|null $name
     */
    public $statementDescriptor;

    /**
     * @var \App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO[]|null
     */
    public $metaData;
}
