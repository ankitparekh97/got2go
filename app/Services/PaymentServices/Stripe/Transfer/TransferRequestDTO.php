<?php


namespace App\Services\PaymentServices\Stripe\Transfer;


class TransferRequestDTO
{
    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var string|null $currency
     */
    public $currency;

    /**
     * @var string|null $destination
     */
    public $destination;
}
