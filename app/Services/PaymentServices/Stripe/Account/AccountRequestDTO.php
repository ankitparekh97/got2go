<?php


namespace App\Services\PaymentServices\Stripe\Account;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class AccountRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $type
     */
    public $type;

    /**
     * @var string|null $country
     */
    public $country;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $object
     */
    public $object;

    /**
     * @var string|null $currency
     */
    public $currency;

    /**
     * @var string|null $accountHolderName
     */
    public $accountHolderName;

    /**
     * @var string|null $routingNumber
     */
    public $routingNumber;

    /**
     * @var string|null $accountNumber
     */
    public $accountNumber;

    /**
     * @var string|null $accountHolderType
     */
    public $accountHolderType;

    /**
     * @var string|null $city
     */
    public $city;

    /**
     * @var string|null $line1
     */
    public $line1;

    /**
     * @var string|null $line2
     */
    public $line2;

    /**
     * @var string|null $postalCode
     */
    public $postalCode;

    /**
     * @var string|null $state
     */
    public $state;

    /**
     * @var string|null $dob
     */
    public $dob;

    /**
     * @var string|null $firstName
     */
    public $firstName;

    /**
     * @var string|null $lastName
     */
    public $lastName;

    /**
     * @var string|null $legalType
     */
    public $legalType;

    /**
     * @var string|null $ssnLast4
     */
    public $ssnLast4;

    /**
     * @var string|null $businessName
     */
    public $businessName;

    /**
     * @var int|null $businessTaxId
     */
    public $businessTaxId;
    /**
     * @var int|null $day
     */
    public $day;

    /**
     * @var int|null $month
     */
    public $month;

    /**
     * @var int|null $year
     */
    public $year;

     /**
     * @var string|null $phone
     */
    public $phone;

    /**
     * @var string|null $businessUrl
     */
    public $businessUrl;

    /**
     * @var int|null $mcc
     */
    public $mcc;

    /**
     * @var bool|null $default
     */
    public $default;


    public static function formRequest(Request $request)
    {
        $default = false;
        if($request->post('default_option') == 'on'){
            $default = true;
        }

        $accountRequestDTO = array();
        $accountRequestDTO['type'] = "custom";
        $accountRequestDTO['country'] = "US";
        $accountRequestDTO['object'] = "bank_account";
        $accountRequestDTO['currency'] = "usd";
        $accountRequestDTO['accountHolderName'] = !empty($request->post('accountHolderName')) ? $request->post('accountHolderName') : null;
        $accountRequestDTO['routingNumber'] = !empty($request->post('routingNumber')) ? $request->post('routingNumber') : null;
        $accountRequestDTO['accountNumber'] = !empty($request->post('accountNumber')) ? $request->post('accountNumber') : null;
        $accountRequestDTO['accountHolderType'] = !empty($request->post('accountHolderType')) ? $request->post('accountHolderType') : null;
        $accountRequestDTO['city'] = 'New Jersey';
        $accountRequestDTO['line1'] = '1000 The American Rd';
        $accountRequestDTO['line2'] = 'Morris Plains';
        $accountRequestDTO['postalCode'] = '07950';
        $accountRequestDTO['state'] = 'New Jersey';
        $accountRequestDTO['firstName'] = !empty($request->post('accountHolderName')) ? $request->post('accountHolderName') : null;
        $accountRequestDTO['legalType'] = !empty($request->post('accountHolderType')) ? $request->post('accountHolderType') : null;
        $accountRequestDTO['ssnLast4'] = '0000';
        $accountRequestDTO['businessName'] = 'Got2Go';
        $accountRequestDTO['businessTaxId'] = 123456789;
        $accountRequestDTO['phone'] = '(801) 206-9790';
        $accountRequestDTO['businessUrl'] = 'https://got2go.kitelytechdev.com';
        $accountRequestDTO['mcc'] = 5734;
        $accountRequestDTO['default'] = $default;

        return new self($accountRequestDTO);
    }
}
