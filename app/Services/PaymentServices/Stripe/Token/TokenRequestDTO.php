<?php


namespace App\Services\PaymentServices\Stripe\Token;


class TokenRequestDTO
{
    /**
     * @var string|null $number
     */
    public $number;

    /**
     * @var string|null $expMonth
     */
    public $expMonth;

    /**
     * @var string|null $expYear
     */
    public $expYear;

    /**
     * @var string|null $cvv
     */
    public $cvv;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $addressLine1
     */
    public $addressLine1;

    /**
     * @var string|null $addressLine2
     */
    public $addressLine2;

    /**
     * @var string|null $addressCity
     */
    public $addressCity;

    /**
     * @var string|null $addressState
     */
    public $addressState;
}
