<?php


namespace App\Services\PaymentServices\Stripe\Refund;

use Spatie\DataTransferObject\DataTransferObject;

class RefundRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var double|null $charge
     */
    public $charge;

     /**
     * @var int|null $bookingId
     */
    public $bookingId;

    public static function convertModelToObject($bookingPayments,$refundAmmount)
    {
        $refundRequestDTO = array();
        $refundRequestDTO['amount'] = $bookingPayments->transaction_number;
        $refundRequestDTO['charge'] = round($refundAmmount,2);
        $refundRequestDTO['bookingId'] = $bookingPayments->booking_id;

        return new self($refundRequestDTO);
    }
}
