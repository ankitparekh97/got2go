<?php


namespace App\Services\PaymentServices\Stripe;


use App\Models\SubscriptionPlan;
use App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO;
use App\Services\PaymentServices\Stripe\Plan\PlanResponseDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionParamsDTO;
use Cartalyst\Stripe\Stripe;
use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use App\Services\PaymentServices\Stripe\Account\AccountRequestDTO;
use App\Services\PaymentServices\Stripe\Transfer\TransferRequestDTO;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Services\PaymentServices\PaymentServiceInterface;

class PaymentService implements PaymentServiceInterface
{
    private $gateWay;

    public function __construct()
    {
        $this->gateWay = Stripe::make(env('STRIPE_SECRET_KEY'));
    }

    public function createToken(TokenRequestDTO $params)
    {
        $tokenData = array();
        $cardData = array();

        if($params->number !== null){
            $cardData['number'] = $params->number;
        }
        if($params->expMonth !== null){
            $cardData['exp_month'] = $params->expMonth;
        }
        if($params->expYear !== null){
            $cardData['exp_year'] = $params->expYear;
        }
        if($params->cvv !== null){
            $cardData['cvc'] = $params->cvv;
        }
        if($params->name !== null){
            $cardData['name'] = $params->name;
        }
        if($params->addressLine1 !== null){
            $cardData['address_line1'] = $params->addressLine1;
        }
        if($params->addressLine2 !== null){
            $cardData['address_line2'] = $params->addressLine2;
        }
        if($params->addressCity !== null){
            $cardData['address_city'] = $params->addressCity;
        }
        if($params->addressState !== null){
            $cardData['address_state'] = $params->addressState;
        }

        $tokenData['card'] = $cardData;
        $token = $this->gateWay->tokens()->create($tokenData);
        return $token;
    }

    public function createCustomer(CustomerRequestDTO $params,$returnAsObj = false)
    {
        $customerData = array();

        if($params->name !== null){
            $cardData['name'] = $params->name;
        }
        if($params->email !== null){
            $customerData['email'] = $params->email;
        }
        if($params->phone !== null){
            $customerData['phone'] = $params->phone;
        }
        if($params->source !== null){
            $customerData['source'] = $params->source;
        }

        $customer = $this->gateWay->customers()->create($customerData);

        if($returnAsObj) {
            return CustomerResponseDTO::convertObject($customer);
        }

        return $customer;
    }

    public function editCustomer($clientId, $params)
    {
        // TODO: Implement editClient() method.
    }

    public function findCustomer($clientId = 216336541)
    {
        // TODO: Implement findCustomer() method.
    }

    public function createCreditCard($clientId, $params)
    {
        // TODO: Implement createCreditCard() method.
    }

    public function updateCreditCard($creditCardTokenId, $params)
    {
        // TODO: Implement updateCreditCard() method.
    }

    public function findCreditCard($creditCardTokenId)
    {
        // TODO: Implement findCreditCard() method.
    }

    public function createPlan(PlanRequestDTO $planRequestDTO)
    {
        $plan = $this->gateWay->plans()->create([
            'id' => $planRequestDTO->id,
            'name' => $planRequestDTO->name,
            'amount' => $planRequestDTO->amount,
            'currency' => $planRequestDTO->currency,
            'interval' => $planRequestDTO->interval,
            'statement_descriptor' => $planRequestDTO->statementDescriptor,
        ]);

        return PlanResponseDTO::createObject($plan);
    }

    public function createSubscription($customerId,SubscriptionParamsDTO $subscriptionParamsDTO)
    {
        $dataSubscription = $this->gateWay->subscriptions()->create($customerId,[
            'plan' => $subscriptionParamsDTO->plan
        ]);
        return SubscriptionResponseDTO::convertObject($dataSubscription);
    }

    public function cancelSubscription($customerId,$subscriptionId)
    {
        $subscription = $this->gateWay->subscriptions()->cancel($customerId,$subscriptionId);

        return SubscriptionResponseDTO::convertObject($subscription);
    }

    public function pauseSubscription($customerId,$subscriptionId)
    {
        $subscription = $this->gateWay->subscriptions()->cancel($customerId,$subscriptionId,true);

        return SubscriptionResponseDTO::convertObject($subscription);
    }

    public function reactiveSubscription($customerId,$subscriptionId)
    {
        $subscription = $this->gateWay->subscriptions()->reactivate($customerId,$subscriptionId);

        return SubscriptionResponseDTO::convertObject($subscription);
    }


    public function updateSubscription($params)
    {
        // TODO: Implement updateSubscription() method.
    }

    public function getSubscriptionById($id)
    {
        $subscription = $this->gateWay->subscription()->find('b54pzm');
        return SubscriptionResponseDTO::convertObject($subscription);
    }

    public function deletePaymentMethod($token)
    {
        $paymentMethod = $this->gateWay->paymentMethod()->delete($token);
        if($paymentMethod->success){
            return true;
        }
        return null;
    }

    public function createCharge(ChargeRequestDTO $params)
    {
        $chargeData = array();
        $metaData = array();

        if($params->amount !== null){
            $chargeData['amount'] = $params->amount;
        }
        if($params->currency !== null){
            $chargeData['currency'] = $params->currency;
        }
        if($params->source !== null){
            $chargeData['source'] = $params->source;
        }
        if($params->description !== null){
            $chargeData['description'] = $params->description;
        }
        if($params->customer !== null){
            $chargeData['customer'] = $params->customer;
        }
        if($params->bookingId !== null){
            $metaData['booking_id'] = $params->bookingId;
        }

        $chargeData['metadata'] = $metaData;
        //dd($chargeData);
        $charge = $this->gateWay->charges()->create($chargeData);
        return $charge;
    }

    public function createAccount(AccountRequestDTO $params)
    {
        $accountData = array();
        $externalAccountData = array();
        $capabilitiesData = [
            'card_payments' => ['requested' => 'true'],
            'transfers' => ['requested' => 'true'],
        ];
        $toAcceptanceData = [
            'date' => time(),
            'ip' => $_SERVER['REMOTE_ADDR'],
        ];
        $addressData = [];
        $legalEntityData = [];
        $dobData = [];
        if($params->type !== null){
            $accountData['type'] = $params->type;
        }
        if($params->country !== null){
            $accountData['country'] = $params->country;
        }
        if($params->email !== null){
            $accountData['email'] = $params->email;
        }
        if($params->businessUrl !== null){
            $accountData['business_url'] = $params->businessUrl;
        }
        if($params->businessName !== null){
            $accountData['business_name'] = $params->businessName;
        }
        if($params->mcc !== null){
            $accountData['mcc'] = $params->mcc;
        }
        if($params->object !== null){
            $externalAccountData['object'] = $params->object;
        }
        if($params->country !== null){
            $externalAccountData['country'] = $params->country;
        }
        if($params->currency !== null){
            $externalAccountData['currency'] = $params->currency;
        }
        if($params->accountHolderName !== null){
            $externalAccountData['account_holder_name'] = $params->accountHolderName;
        }
        if($params->routingNumber !== null){
            $externalAccountData['routing_number'] = $params->routingNumber;
        }
        if($params->accountNumber !== null){
            $externalAccountData['account_number'] = $params->accountNumber;
        }
        if($params->accountHolderType !== null){
            $externalAccountData['account_holder_type'] = $params->accountHolderType;
        }
        if($params->city !== null){
            $addressData['city'] = $params->city;
        }
        if($params->country !== null){
            $addressData['country'] = $params->country;
        }
        if($params->line1 !== null){
            $addressData['line1'] = $params->line1;
        }
        if($params->line2 !== null){
            $addressData['line2'] = $params->line2;
        }
        if($params->postalCode !== null){
            $addressData['postal_code'] = $params->postalCode;
        }
        if($params->state !== null){
            $addressData['state'] = $params->state;
        }
        if($params->day !== null){
            $dobData['day'] = $params->day;
        }
        if($params->month !== null){
            $dobData['month'] = $params->month;
        }
        if($params->year !== null){
            $dobData['year'] = $params->year;
        }
        if($params->firstName !== null){
            $legalEntityData['first_name'] = $params->firstName;
        }
        if($params->lastName !== null){
            $legalEntityData['last_name'] = $params->lastName;
        }
        if($params->legalType !== null){
            $legalEntityData['type'] = $params->legalType;
        }
        if($params->ssnLast4 !== null){
            $legalEntityData['ssn_last_4'] = $params->ssnLast4;
        }
        if($params->email !== null){
            $legalEntityData['personal_email'] = $params->email;
        }
        if($params->phone !== null){
            $legalEntityData['personal_phone_number'] = $params->phone;
        }
        if($params->legalType !== null){
            $legalEntityData['type'] = $params->legalType;
        }
        if($params->ssnLast4 !== null){
            $legalEntityData['ssn_last_4'] = $params->ssnLast4;
        }
        /* if($params->business_url !== null){
            $legalEntityData['business_url'] = $params->ssnLast4;
        } */

        if($params->accountHolderType == 'company'){
            if($params->businessName !== null){
                $legalEntityData['business_name'] = $params->businessName;
            }
            if($params->businessTaxId !== null){
                $legalEntityData['business_tax_id'] = $params->businessTaxId;
            }
        }
        $legalEntityData['address'] = $addressData;
        $legalEntityData['dob'] = $dobData;
        $accountData['legal_entity']= $legalEntityData;
        $accountData['external_account'] = $externalAccountData;
        $accountData['tos_acceptance']= $toAcceptanceData;
        $accountData['capabilities']= $capabilitiesData;

        //dd($accountData);
        $account = $this->gateWay->account()->create($accountData);
        return $account;
    }

    public function createTransfer(TransferRequestDTO $params)
    {

        $transferData = array();
        if($params->amount !== null){
            $transferData['amount'] = $params->amount;
        }
        if($params->currency !== null){
            $transferData['currency'] = $params->currency;
        }
        if($params->destination !== null){
            $transferData['destination'] = $params->destination;
        }
        $transfer = $this->gateWay->transfers()->create($transferData);
        return $transfer;
    }

    public function createCard(CardRequestDTO $params)
    {
        $card = $this->gateWay->cards()->create($params->customerId, $params->token);
        return $card;
    }

    public function deleteCard(CardRequestDTO $params)
    {
        return $this->gateWay->cards()->delete($params->customerId, $params->token);
    }

    public function deleteBankAccount(CardRequestDTO $params)
    {
        return $this->gateWay->account()->delete($params->token);
    }

    public function updateCard(CardRequestDTO $params)
    {
        return $this->gateWay->cards()->update($params->customerId, $params->token, ['name' => $params->name]);
    }
    public function updateAccount(AccountRequestDTO $params) {
        return $this->gateWay->account()->update($params->account,['metadata' => ['individual.first_name' => $params->accountHolderName]]);
    }
    public function checkCard(CardRequestDTO $params) {
        return $this->gateWay->tokens()->retrieve($params->token);
    }
    //Refund
    public function createRefund(RefundRequestDTO $params)
    {
        $refundData = array();
        $metaData = array();

       /*  if($params->amount !== null){
            $refundData['amount'] = $params->amount;
        }
        if($params->charge !== null){
            $refundData['charge'] = $params->charge;
        } */
        if($params->bookingId !== null){
            $metaData['booking_id'] = $params->bookingId;
        }

        $refundData['metadata'] = $metaData;
        //dd($refundData);
        $refund = $this->gateWay->refunds()->create($params->charge,$params->amount,$refundData);
        return $refund;
    }
}
