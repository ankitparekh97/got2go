<?php


namespace App\Services\PaymentServices\Stripe\Address;


use Spatie\DataTransferObject\DataTransferObject;

class AddressRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $company
     */
    public $company;

    /**
     * @var string|null $countryCodeAlpha2
     */
    public $countryCodeAlpha2;

    /**
     * @var string|null $countryCodeAlpha3
     */
    public $countryCodeAlpha3;

    /**
     * @var string|null $countryCodeNumeric
     */
    public $countryCodeNumeric;

    /**
     * @var string|null $countryName
     */
    public $countryName;

    /**
     * @var string|null $countryName
     */
    public $customerId;

    /**
     * @var string|null $extendedAddress
     */
    public $extendedAddress;

    /**
     * @var string|null $firstName
     */
    public $firstName;

    /**
     * @var string|null $lastName
     */
    public $lastName;

    /**
     * @var string|null $locality
     */
    public $locality;

    /**
     * @var string|null $postalCode
     */
    public $postalCode;

    /**
     * @var string|null $region
     */
    public $region;

    /**
     * @var string|null $streetAddress
     */
    public $streetAddress;

}
