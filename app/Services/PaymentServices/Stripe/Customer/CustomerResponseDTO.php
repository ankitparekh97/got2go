<?php


namespace App\Services\PaymentServices\Stripe\Customer;


use App\Services\PaymentServices\Braintree\Address\AddressResponseDTO;
use Spatie\DataTransferObject\DataTransferObject;

class CustomerResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $phone
     */
    public $phone;

    /**
     * @var string|null $source
     */
    public $source;

    public static function convertObject($customer)
    {
        return new self($customer);
    }
}
