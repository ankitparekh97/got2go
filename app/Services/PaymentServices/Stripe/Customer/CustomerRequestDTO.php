<?php


namespace App\Services\PaymentServices\Stripe\Customer;


class CustomerRequestDTO
{
    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $phone
     */
    public $phone;

    /**
     * @var string|null $source
     */
    public $source;
}
