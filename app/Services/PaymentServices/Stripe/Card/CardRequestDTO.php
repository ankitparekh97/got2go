<?php


namespace App\Services\PaymentServices\Stripe\Card;


class CardRequestDTO
{


    /**
     * @var string
     */
    public $customerId;

    /**
     * @var string|null
     */
    public $token;

}
