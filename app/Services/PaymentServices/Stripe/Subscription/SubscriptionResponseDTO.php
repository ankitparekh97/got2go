<?php


namespace App\Services\PaymentServices\Stripe\Subscription;


use App\Services\PaymentServices\Braintree\Transaction\TransactionResponseDTO;
use App\Services\PaymentServices\Stripe\Plan\PlanResponseDTO;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Ref: https://developers.braintreepayments.com/reference/response/subscription/php
 * Class SubscriptionResponseDTO
 * @package App\Services\PaymentServices\Braintree\Subscirption
 */
class SubscriptionResponseDTO extends DataTransferObject
{
    /**
     * @var string|null $id
     */
    public $id;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodEndDate
     */
    public $billingPeriodEndDate;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodStartDate
     */
    public $billingPeriodStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $createdAt;

    /**
     * @var \Illuminate\Support\Carbon|null $createdAt
     */
    public $cancelledAt;

    /**
     * @var string|null $customerId
     */
    public $customerId;

    /**
     * @var string|null $status
     */
    public $status;

    /**
     * @var \App\Services\PaymentServices\Stripe\Plan\PlanResponseDTO|null $plan
     */
    public $plan;


    public static function convertObject($subscription)
    {
        $subscriptionArr = array();
        $subscriptionArr['id'] = $subscription['id'];
        $subscriptionArr['billingPeriodStartDate'] = carbonParseDate($subscription['current_period_start']);
        $subscriptionArr['billingPeriodEndDate'] = carbonParseDate($subscription['current_period_end']);
        $subscriptionArr['createdAt'] = carbonParseDate($subscription['start_date']);
        $subscriptionArr['cancelledAt'] = !empty($subscription['canceled_at']) ? carbonParseDate($subscription['canceled_at']) : null;
        $subscriptionArr['customerId'] = $subscription['customer'];
        $subscriptionArr['status'] = $subscription['status'];

        if(!empty($subscription['plan'])){
            $subscriptionArr['plan'] = PlanResponseDTO::createObject($subscription['plan']);
        }

        return new self($subscriptionArr);
    }
}
