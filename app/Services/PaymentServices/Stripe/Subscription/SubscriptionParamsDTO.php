<?php


namespace App\Services\PaymentServices\Stripe\Subscription;


class SubscriptionParamsDTO
{
    /**
     * @var string $plan
     */
    public $plan;

    /**
     * @var string $coupon
     */
    public $coupon;

    /**
     * @var int|null $coupon
     */
    public $quantity;

    /**
     * @var float|null $applicationFeePercent
     */
    public $applicationFeePercent;

    /**
     * @var float|null $taxPercent
     */
    public $taxPercent;
}
