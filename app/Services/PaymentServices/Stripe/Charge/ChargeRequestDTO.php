<?php


namespace App\Services\PaymentServices\Stripe\Charge;

use Spatie\DataTransferObject\DataTransferObject;

class ChargeRequestDTO extends DataTransferObject
{
    /**
     * @var string|null $amount
     */
    public $amount;

    /**
     * @var string|null $currency
     */
    public $currency;

    /**
     * @var string|null $source
     */
    public $source;

    /**
     * @var string|null $description
     */
    public $description;

    /**
     * @var string|null $customer
     */
    public $customer;

    /**
     * @var int|null $bookingId
     */
    public $bookingId;

    public static function convertModelToObject($bookings,$cardDetails)
    {
        $chargeRequestDTO = array();
        $chargeRequestDTO['amount'] = $bookings->total;
        $chargeRequestDTO['currency'] = "USD";
        $chargeRequestDTO['source'] = $bookings->card_token;
        $chargeRequestDTO['description'] = "Payment for ".$bookings->property->title;
        $chargeRequestDTO['customer'] = $cardDetails->rental_user->customer_id;
        $chargeRequestDTO['bookingId'] = $bookings->id;

        return new self($chargeRequestDTO);
    }
}
