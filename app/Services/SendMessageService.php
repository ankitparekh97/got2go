<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Property;
use App\Models\PaymentCards;
use App\Repositories\OwnerRentalPairInterface;
use App\Repositories\MessagesInterface;
use App\Repositories\GroupMessagesInterface;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use App\Models\GroupMessages;
use Helper;
use Carbon\Carbon;
use App\Libraries\PusherFactory;


class SendMessageService implements SendMessageServiceContract
{

    protected $ownerRentalPairRepository;
    protected $messagesRepository;


    public function __construct(
        OwnerRentalPairInterface $ownerRentalPairRepository,
        MessagesInterface $messagesRepository,
        GroupMessagesInterface $groupMessagesRepository
    )
    {
        $this->ownerRentalPairRepository = $ownerRentalPairRepository;
        $this->messagesRepository = $messagesRepository;
        $this->groupMessagesRepository = $groupMessagesRepository;
    }


    /* Send Message Rental to Owner */
    public function postRentalOwnerSendMessage($toUser,$userMessage){

        $ownerId = Helper::urlsafe_b64decode($toUser);
        $rentalId = Auth::guard('rentaluser')->user()->id;
        $type = 'rental';
        $pairId = $this->ownerRentalPairRepository->saveOwnerRentalPair($ownerId,$rentalId,$type);
        $fromUser =$rentalId;
        $this->postCommonSendMessage($pairId->id,'rental','owner',$fromUser,$toUser,'rentaluser',$userMessage);
    }

    /* Send Message Owner to Rental */
    public function postOwnerRentalSendMessage($toUser,$userMessage){

        $ownerId = Auth::guard('owner')->user()->id;
        $rentalId = Helper::urlsafe_b64decode($toUser);
        $type = 'rental';
        $pairId = $this->ownerRentalPairRepository->saveOwnerRentalPair($ownerId,$rentalId,$type);
        $fromUser =$ownerId;
        $this->postCommonSendMessage($pairId->id,'owner','rental',$fromUser,$toUser,'owner',$userMessage);
    }

    /* Send Message Admin to Rental */
    public function postAdminRentalSendMessage($toUser,$userMessage){

        $adminId = 1;
        $rentalId = Helper::urlsafe_b64decode($toUser);
        $type = 'admin';
        $pairId = $this->ownerRentalPairRepository->saveAdminRentalPair($adminId,$rentalId,$type);
        $fromUser = $adminId;
        $this->postCommonSendMessage($pairId->id,'admin','rental',$fromUser,$toUser,'admin',$userMessage);
    }

    /* Send Message Admin to Owner */
    public function postAdminOwnerSendMessage($toUser,$userMessage){

        $adminId = 1;
        $rentalId = Helper::urlsafe_b64decode($toUser);
        $type = 'admin';
        $pairId = $this->ownerRentalPairRepository->saveAdminOwnerPair($adminId,$rentalId,$type);
        $fromUser = $adminId;
        $this->postCommonSendMessage($pairId->id,'admin','owner',$fromUser,$toUser,'admin',$userMessage);
    }

     /* Common Function to post message */
    public function postCommonSendMessage($pairId,$fromType,$toType,$fromUser,$toUser,$guard,$message){

        $re = '/(\(?\d{3}\D{0}? ?[()\.-]? ?\d{3}\D{0}?[()\.-]? ?\d{0,10}).*?/i';
        $subst = '**********';
        $message = preg_replace($re, $subst, $message);
        $re = '/(gmail|mail|proton|\bcom(?!\S)|outlook|yahoo|@)/i';

        preg_match_all($re, $message, $matches, PREG_SET_ORDER, 0);
        if($matches){
            $message =  '**********';
        }

        $messageArray = [
            'pair_id' =>$pairId,
            'from_user' => $fromUser,
            'to_user' => Helper::urlsafe_b64decode($toUser),
            'messages' => $message,
            'from_user_type' => $fromType,
            'to_user_type' => $toType,
            'is_read'=>1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ];
        $this->send($toUser,$messageArray,$guard);
    }

    /*Description Sending message form */
    public function send($toUser,$messageArray,$guard) {
        //Save Messages
        $message = $this->messagesRepository->saveMessages($messageArray);
        $this->ownerRentalPairRepository->updateOwnerRentalPair($message->pair_id);

        $toUserType = $message->to_user_type;
        if($guard == 'admin'){
            $fromUserId = 1;
        }else{
            $fromUserId = Auth::guard($guard)->user()->id;
        }
        $message->message_count = $this->messagesRepository->getMessageCount($message,$toUserType);
        // prepare some data to send with the response
        $message->dateTimeStr = $message->created_at;
        $message->ownerUserName = ($message->pairUser->owner_id != null)?$message->pairUser->ownerUser->first_name.' '.$message->pairUser->ownerUser->last_name:'';
        $message->from_user_id = Helper::urlsafe_b64encode($fromUserId);
        $message->rentalUserName = ($message->pairUser->rental_id != null)?$message->pairUser->rentalUser->first_name.' '.$message->pairUser->rentalUser->last_name:'';
        $message->adminUserName = ($message->pairUser->admin_id != null)?$message->pairUser->adminUser->name:'';
        $message->to_user_id = $toUser;
        $message->pair_user_id = Helper::urlsafe_b64encode($message->pair_id);
        $message->ownerphoto = ($message->pairUser->owner_id != null)?$message->pairUser->ownerUser->photo:null;
        $message->rentalphoto = ($message->pairUser->rental_id != null)?$message->pairUser->rentalUser->photo:null;
        $message->type = $message->pairUser->type;

        PusherFactory::make()->trigger('chat', 'send', ['data' => $message]);

        return response()->json(['state' => 1, 'data' => $message]);

   }

   /*Description Sending message form */
    public function sendGroupMessage($messageArray,$guard) {
        //Save Messages
        $message = $this->groupMessagesRepository->saveMessages($messageArray);

        // prepare some data to send with the response
        $message->dateTimeStr = $message->created_at;
        $message->from_user_id = Helper::urlsafe_b64encode(Auth::guard($guard)->user()->id);
        $message->rentalUserName = $message->rentalUser->first_name.' '.$message->rentalUser->last_name;
        $message->pair_user_id = Helper::urlsafe_b64encode($message->tripboard_id);
        $message->rentalphoto = $message->rentalUser->photo;

        PusherFactory::make()->trigger('group-chat', 'send', ['data' => $message]);

        return response()->json(['state' => 1, 'data' => $message]);

   }
}
