<?php


namespace App\Services;

use App\Models\Property;

interface BookingAvaibilityServiceContract
{
    public function checkBookings($id,$checkInDate,$checkOutDate);

    public function propertyAvailablity($id,$checkInDate,$checkOutDate);

}
