<?php


namespace App\Services;

use App\Http\DTO\Api\OwnerUserDTO;
use App\Http\DTO\Api\PaymentCardDTO;

interface PaymentCardContract
{
    public function createPaymentCard(PaymentCardDTO  $paymentCardDTO,$userId);

    public function getcardDetails($userType,$rentalUserId);

    public function paymentMethodObject($params);

    public function getAllPaymentCards($params);

    public function getPaymentCard($params);

    public function checkDuplicateCard($params,$token, $exp_month, $exp_year);

    public function saveCard(PaymentCardDTO $paymentCardDTO,$token,$user);

    public function saveOwnerCustomerId(OwnerUserDTO $ownerUserDTO);

    public function deletepaymentCard($id);
}
