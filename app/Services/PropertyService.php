<?php


namespace App\Services;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\AmenitiesDTO;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Http\DTO\Api\HotelsDTO;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use App\Http\DTO\Api\PropertyVerificationDTO;
use App\Repositories\AmenitiesInterface;
use App\Repositories\BulkUploadInterface;
use App\Repositories\BulkUploadLogsInterface;
use App\Repositories\HotelsInterface;
use App\Repositories\OwnerInterface;
use App\Repositories\PropertyAmenitiesInterface;
use App\Repositories\PropertyAvablityInterface;
use App\Repositories\PropertyBedroomsInterface;
use App\Repositories\PropertyDocumentsInterface;
use App\Repositories\PropertyImagesInterface;
use App\Repositories\PropertyInterface;
use App\Repositories\PropertyTypeInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Validator;

class PropertyService implements PropertyServiceContract
{
    protected $propertyRepository;
    protected $bulkUploadRepository;
    protected $bulkUploadLogsRepository;
    protected $propertyTypeRepository;
    protected $amenityRepository;
    protected $hotelRepository;
    protected $ownerRepository;
    protected $propertyImagesRepository;
    protected $propertyDocumentsRepository;
    protected $propertyBedRoonsRepository;
    protected $propertyAmenityRepository;
    protected $propertyAvaiblitiesRepository;

    public function __construct(
        PropertyInterface $propertyRepository,
        BulkUploadInterface $bulkUploadRepository,
        BulkUploadLogsInterface $bulkUploadLogsRepository,
        PropertyTypeInterface $propertyTypeRepository,
        AmenitiesInterface $amenityRepository,
        HotelsInterface $hotelRepository,
        OwnerInterface $ownerRepository,
        PropertyImagesInterface $propertyImagesRepository,
        PropertyDocumentsInterface $propertyDocumentsRepository,
        PropertyBedroomsInterface $propertyBedRoomsRepository,
        PropertyAmenitiesInterface $propertyAmenityRepository,
        PropertyAvablityInterface $propertyAvaiblitiesRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->bulkUploadRepository = $bulkUploadRepository;
        $this->bulkUploadLogsRepository = $bulkUploadLogsRepository;
        $this->propertyTypeRepository = $propertyTypeRepository;
        $this->amenityRepository = $amenityRepository;
        $this->hotelRepository = $hotelRepository;
        $this->ownerRepository = $ownerRepository;
        $this->propertyImagesRepository = $propertyImagesRepository;
        $this->propertyDocumentsRepository = $propertyDocumentsRepository;
        $this->propertyBedRoomsRepository = $propertyBedRoomsRepository;
        $this->propertyAmenityRepository = $propertyAmenityRepository;
        $this->propertyAvaiblitiesRepository = $propertyAvaiblitiesRepository;
    }

    public function getListings($ownerId,$status){
        return $this->propertyRepository->getPropertyByOwnerId($ownerId,$status);
    }

    public function findPropertyById($id){
        return $this->propertyRepository->findPropertyById($id);
    }

    public function getPropertyByUniqueKey($uniqueId){
        return $this->propertyRepository->getPropertyByUniqueKey($uniqueId);
    }

    public function getById($id){
        return $this->bulkUploadRepository->getById($id);
    }

    public function getByParamsCollection($params){
        return $this->propertyRepository->getByParamsCollection($params);
    }

    public function getByParamsObject($params){
        return $this->propertyRepository->getByParamsObject($params);
    }

    public function getOwnerById($id){
        return $this->ownerRepository->getOwnerById($id);
    }

    public function getPropertyTypes(){
        return $this->propertyTypeRepository->getPropertyTypes();
    }

    public function getPropertyTypesByParams(){
        return $this->propertyTypeRepository->getPropertyTypesByParams();
    }

    public function getMasterAmenities(){
        return $this->amenityRepository->getMasterAmenities();
    }

    public function getMasterAmenitiesByArray(){
        return $this->amenityRepository->getMasterAmenitiesByArray();
    }
    public function getHotels(){
        return $this->hotelRepository->getHotels();
    }

    public function getHotelsByParamsObject($params){
        return $this->hotelRepository->getHotelsByParamsObject($params);
    }

    public function getHotelsByParamsCollection($params){
        return $this->hotelRepository->getHotelsByParamsCollection($params);
    }

    public function getPropertyImagesByParamsObject($params){
        return $this->propertyImagesRepository->getPropertyImagesByParamsObject($params);
    }

    public function getPropertyImagesByParamsCollection($params){
        return $this->propertyImagesRepository->getPropertyImagesByParamsCollection($params);
    }

    public function saveHotel(HotelsDTO $hotelsDTO){

        return $this->hotelRepository->saveHotel($hotelsDTO);
    }

    public function publishProperty($id){
        $property = $this->propertyRepository->publishProperty($id);
        $updatedAt = date('m/d/Y h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($property->updated_at, Session::get('timezone'))));

        return array('property'=>$property, 'updatedAt'=>$updatedAt);
    }

    public function deleteProperty($id){
        return $this->propertyRepository->deleteProperty($id);
    }

    final function propertyDataMapping($propertyData){

        $data = array(
            'owner_id'=> $propertyData['ownerId'],
            'title'=>$propertyData['title'],
            'description'=>$propertyData['description'],
            'about_the_space'=>$propertyData['aboutTheSpace'],
            'list_as_company'=>$propertyData['listAsCo'],
            'location_detail'=>$propertyData['locationDetail'],
            'city'=>$propertyData['city'],
            'state'=>$propertyData['state'],
            'state_abbrv'=>$propertyData['state'],
            'country'=>$propertyData['country'],
            'zipcode'=> $propertyData['zipcode'],
            'type_of_property'=>$propertyData['typeOfProperty'],
            'property_type_id'=>$propertyData['propertyTypeId'],
            'sub_property_type_id'=>$propertyData['subPropertyTypeId'],
            'resort_id'=>$propertyData['resortId'],
            'guest_will_have'=>$propertyData['guestWillHave'],
            'is_for_guest'=>$propertyData['isForGuest'],
            'no_of_guest'=>$propertyData['noOfGuest'],
            'total_beds'=>$propertyData['totalBeds'],
            'extra_beds'=>$propertyData['extraBeds'],
            'total_bedroom'=>$propertyData['totalBedroom'],
            'total_bathroom'=>$propertyData['totalBatroom'],
            'price'=>$propertyData['price'],
            'offer_price'=>$propertyData['offerPrice'],
            'cleaning_fee'=>$propertyData['cleaningFee'],
            'is_price_negotiable'=>$propertyData['isPriceNegotiable'],
            'is_instant_booking_allowed'=> $propertyData['isInstantBookingAllowed'],
            'is_partial_payment_allowed'=>$propertyData['isPartialPaymentAllowed'],
            'check_in'=>$propertyData['checkIn'],
            'check_out'=>$propertyData['checkOut'],
            'cancellation_type'=>$propertyData['cancellationType'],
            'is_hotdeals'=> $propertyData['isHotDeals'],
            'publish' => $propertyData['publish'],
            'last_tab_id' => $propertyData['lastTabId'],
            'partial_payment_amount'=>$propertyData['partialPaymentAmt'],
            'lat_lang' => $propertyData['latlang'],
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        );

        return $data;
    }

    public function saveProperty($propertyData){

        $data = $this->propertyDataMapping($propertyData);
        $property = $this->findPropertyById($propertyData['id']);
        if(empty($property)){
            $property = $this->propertyRepository->createProperty($data);
        }

        if($data['last_tab_id'] == '12' && $property->status == GlobalConstantDeclaration::PROPERTY_STATUS_DRAFT){
            $data['status'] = GlobalConstantDeclaration::PROPERTY_STATUS_PENDING;
        }

        if($data['type_of_property'] == 'property')
        {
            $data['resort_id'] = $property->id;
        }else{
            $data['resort_id'] = $propertyData['resortId'];
        }

        $propertyImages = $this->getPropertyImagesByParamsObject(array('property_id'=>$property->id));
        if(!empty($propertyImages)){
            $data['cover_photo'] = $propertyImages->photo;
        }

        $$property = $this->propertyRepository->updateProperty($property->id,$data);
        return $property;
    }

    public function savepropertyBedRooms(PropertyBedroomsDTO $propertyBedroomsDTO,$totalBeds,$property){

        $propertyBedRooms[] = '';
        if($propertyBedroomsDTO->bedType){
            $this->propertyBedRoomsRepository->deletePropertyBedrooms($property->id);
            $bed = $propertyBedroomsDTO->bedType;
            $bedTypes = array_flip(config('property.bed_types'));

            $bedroomData[] = '';
            foreach($bed as $bedType=>$value){
                foreach($value as $key=>$bedNo){
                    if($key=='0' || $bedNo == '0'){
                        continue;
                    }

                    $bedroomData[] = [
                        'property_id'=>$property->id,
                        'bed'=>$bedNo,
                        'bed_type'=>$bedTypes[$bedType],
                        'bedroom_no'=>$key,
                    ];
                }
            }

            $bDdata = array_filter($bedroomData);
            if(!empty($bDdata)){
                $propertyBedRooms = $this->propertyBedRoomsRepository->savePropertyBedrooms($bDdata);
            }

            $collection = collect($bDdata);
            $collection->sum('bed');

            $data = array('total_beds'=>$collection->sum('bed') + $totalBeds);
            $this->propertyRepository->updateProperty($property->id,$data);

            return;
        }
    }

    public function saveAmenities(AmenitiesDTO $amenitiesDTO,$property){

        if($amenitiesDTO->amenityId){
            $this->propertyAmenityRepository->deletePropertyAmenities($property->id);

            $aminities = $amenitiesDTO->amenityId;
            foreach($aminities as $key=>$value){

                $aminitiesArray[] = [
                    'property_id'=>$property->id,
                    'property_type'=>$amenitiesDTO->propertyType,
                    'amenity_id'=>$aminities[$key],
                    'amenities_type'=>$amenitiesDTO->amenitiesType
                ];
            }
            $propertyAmenities = $this->propertyAmenityRepository->saveAmenities($aminitiesArray);

            return $propertyAmenities;
        }

    }

    public function saveAvaiblities($request,$property){

        if($request->has('property_available_when') && $request->has('property_available_when_to')){
            $this->propertyAvaiblitiesRepository->deletePropertyAvaiblities($property->id);

            $availableFrom = $request->get('property_available_when');
            $availableTo = $request->get('property_available_when_to');
            $minimumNight = $request->get('min_night');

            $propertyAvailableArray[] = '';
            foreach($availableFrom as $key=>$value){
                if($value && $availableTo[$key]){

                    $propertyAvailableArray[] = [
                        'property_id'=>$property->id,
                        'available_from'=> Carbon::createFromFormat('m/d/Y', $availableFrom[$key])->format('Y/m/d'),
                        'available_to'=> Carbon::createFromFormat('m/d/Y', $availableTo[$key])->format('Y/m/d'),
                        'is_partial_booking_allowed'=> $request->get('is_partial_booking_date'.$key),
                        'minimum_days'=> ($minimumNight[$key] > 0 && $request->get('is_partial_booking_date'.$key) == '1' ) ? $minimumNight[$key] : 1
                    ];

                }
            }

            $data = array_filter($propertyAvailableArray);
            return $this->propertyAvaiblitiesRepository->savePropertyAvaiblities($data);
        }
    }

    public function savePropertyMediaDocuments($request){

        $path = public_path('uploads/property');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        if($request->hasFile('photos')){

            // Multiple photos
            foreach ($request->file('photos') as $image)
            {
                $photos = 'property_photo_'.uniqid().'.'.$image->getClientOriginalExtension();
                $image->move($path, $photos);

                // the image will be replaced with an optimized version which should be smaller
                $optimizerChain = OptimizerChainFactory::create();
                $optimizerChain->optimize($path . '/' . $photos);

                $imageArray = [
                    'property_id'=>$request->property_id,
                    'photo'=>$photos,
                    'media_type'=>$request->photos_filetype,
                    'originalName'=>$image->getClientOriginalName()
                ];


            }
            $propertyMedia = $this->propertyImagesRepository->saveMedia($imageArray);
            return response()->json(['success'=>true,'media_id'=>$propertyMedia]);
        }
        else if($request->has('proof_of_ownership'))
        {
            if(count($request->file('proof_of_ownership')) > 0){
                foreach ($request->file('proof_of_ownership') as $image)
                {
                    $ownership_document = 'ownership_document_'.uniqid().'.'.$image->getClientOriginalExtension();
                    $image->move($path, $ownership_document);

                    // the image will be replaced with an optimized version which should be smaller
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize($path . '/' . $ownership_document);

                    $documentsArray = [
                        'property_id'=>$request->property_id,
                        'document_name'=>$ownership_document,
                        'media_type'=>$request->proof_of_ownership_filetype,
                        'created_at'=> Carbon::now(),
                        'originalName'=>$image->getClientOriginalName()
                    ];

                }
                $propertyVerification = $this->propertyDocumentsRepository->savePropertyDocuments($documentsArray);
                return response()->json(['success'=>true,'media_id'=>$propertyVerification]);
            }
        }
        else if($request->hasFile('gov_id'))
        {
            if(count($request->file('gov_id')) > 0){
                foreach ($request->file('gov_id') as $govId)
                {
                    $govtId_document = 'govtId_document_'.uniqid().'.'.$govId->getClientOriginalExtension();
                    $govId->move($path, $govtId_document);

                     // the image will be replaced with an optimized version which should be smaller
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize($path . '/' . $govtId_document);

                    $params = [
                        'property_id'=>$request->property_id,
                        'document_name'=>$govtId_document,
                        'media_type'=>$request->gov_id_filetype,
                        'is_govt_id'=> '1',
                        'created_at'=> Carbon::now(),
                        'originalName'=>$govId->getClientOriginalName()
                    ];
                }

                $propertyGovt = $this->propertyDocumentsRepository->savePropertyDocuments($params);
                return response()->json(['success'=>true,'media_id'=>$propertyGovt]);
            }
        }
    }

    public function deleteDocuments($request){

        // Photos
        if($request->has('photos_deleted'))
        {
            $imagesIds = explode(',', $request->get('photos_deleted'));
            if(count($imagesIds) >= 1){
                $this->propertyImagesRepository->deletePropertyImages($imagesIds);
            }
        }

        // Ownership documents
        if($request->has('proof_of_ownership_deleted'))
        {
            $verificationIds = explode(',', $request->get('proof_of_ownership_deleted'));
            if(count($verificationIds) > 0) {
                $this->propertyDocumentsRepository->deletePropertyDocuments($verificationIds);
            }
        }

        // Govt ID documents
        if($request->has('gov_id_deleted'))
        {
            $govtIds = explode(',', $request->get('gov_id_deleted'));
            if(count($govtIds) > 0) {
                $this->propertyDocumentsRepository->deletePropertyDocuments($govtIds);
            }
        }

        return true;
    }

}
