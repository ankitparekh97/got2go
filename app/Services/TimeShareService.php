<?php
namespace App\Services;
use App\Models\PropertyOwnershipDocuments;
use App\Models\Property;
use DB;
use App\Repositories\PropertyInterface;
use App\Repositories\PropertyOwnershipDocumentInterface;

class TimeShareService implements TimeShareContract
{

    private $propertyRepository = null;
    public function __construct(
        PropertyInterface  $propertyRepository,
        PropertyOwnershipDocumentInterface  $propertyOwnershipDocumentRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->propertyOwnershipDocumentRepository = $propertyOwnershipDocumentRepository;
    }

    public function timeShareList(){
       return $this->propertyRepository->timeShareList();
    }

    public function bulkTimeshareList(){
        return $this->propertyRepository->bulkTimeshareList();
    }

    public function propertyApprove($input){
        return $this->propertyRepository->propertyApprove($input);
    }

    public function viewDocument($type,$id){
        return $this->propertyOwnershipDocumentRepository->viewDocument($type,$id);
    }

    public function propertyApproveAll($input){
        return $this->propertyRepository->propertyApproveAll($input);
    }
}
