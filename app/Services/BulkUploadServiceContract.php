<?php


namespace App\Services;

use App\Http\DTO\Api\AmenitiesDTO;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Http\DTO\Api\HotelsDTO;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use Illuminate\Support\Facades\Request;

interface BulkUploadServiceContract
{

    public function getById($id);

    public function getByUniqueId($unique_key);

    public function getPropertyByIds($ids);

    public function getBulkUploadByParamsObject($params);

    public function bulkDownload(Request $request);

    public function getBulkUploadLogs($Id);

    public function getBulkUploadListings($ownerId,$id);

    public function getQueueCount();

    public function getrowPosition($queueId);

    public function saveBulkUpload(BulkUploadDTO $bulkUploadDTO);

    public function bulkUploadJob($ownerId,$latLong,$uniqueId,$records);

    public function bulkUploadPropertyAttributes($records,$propertyId);

    public function bulkUploadMedia($mediaFolder,$propertyId);

    public function bulkUploadDocs($folder,$property);

    public function bulkUploadGovtDocs($folder,$propertyId);

    public function bulkUploadComplete(
        $serializeUploadedLogs,
        $serializeErrLogs,
        $serializeMediaErrLogs,
        $serializeVerificationErrLogs,
        $serializeGovtIdErrLogs,
        $bulkUploadId,
        $status
    );
}
