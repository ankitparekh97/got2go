<?php


namespace App\Services;


use App\Http\DTO\Api\RentalUserDTO;
use Illuminate\Http\Request;

interface RentalUserServiceContract
{
    public function registerTripper(RentalUserDTO $rentalUserDTO,int $rentalUserId);
    public function getHotDealsByLimit();
    public function getUserById($userId);
    public function getOwnerById($userId);
    public function saveSetting($input);
    public function getMyStay($userId);
    public function mystayDetails($propertyId,$bookingId, $userId);
    public function getCardDetails($id);
    public function getRentalUserWithCommunicationPref($userId);
    public function saveNotificationSettings($userId, $input);
    public function updateProfilePic($userId, $imageName);
    public function SavePaymentCards($card, $user, $token);
    public function updateCustomerId($customerId, $userId);
    public function deletePaymentCard($id, $customerId);
    public function checkDuplicateCard($token, $userId);
    public function calculateRefund($bookingId);
    public function updateBookingOwnerStatus($bookingId, $status);
    public function cancelBooking($bookingId, $refundAmmount, $user);
    public function saveRenterUser(RentalUserDTO $rentalUserDTO,$userId = null);

}
