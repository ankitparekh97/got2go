<?php


namespace App\Services;

interface TripBoardContract
{
    public function getTripBoardById(int $tripBoardId,array $with = array());

    public function getUserTripBoards($userId);

    public function getFavouriteTripBoards($userId);

    public function getTripBoardPropertyImages($id);

  
    public function tripboarddetail($tripboardId, $userId);
  

    public function likeUnlikeProperties($param);

    public function addOrRemovePropertiesFromTripboard($param);

    public function voteTripboard($param);

    public function tripBoardRemoveListing($input);

    public function postSendMessage($tripboardId,$userMessage);
    public function getLoadLatestMessages($tripboardId);

    public function checkForSharedTripBoard($id, $userId);

    public function saveTripBoard($tripboardId, $userId);

    public function saveTripboardDetail($input);
    public function getTriboardsWithPropertyId($userId,$propertyId);
    public function  saveTravelBuddy($input, $userId);
    public function  deleteTravelBuddy($buddyId);
    public function addTripboard($tripboardName, $userId, $propertyId);

}
