<?php


namespace App\Services;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\OwnerUserDTO;
use App\Http\DTO\Api\PaymentCardDTO;
use App\Repositories\OwnerInterface;
use App\Repositories\PaymentCardInterface;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;

class PaymentCardService implements PaymentCardContract
{
    public $paymentCardRepository;
    public $ownerRepository;

    public function __construct(
        PaymentCardInterface $paymentCardRepository,
        OwnerInterface $ownerRepository
    )
    {
        $this->paymentCardRepository = $paymentCardRepository;
        $this->ownerRepository = $ownerRepository;
    }

    public function createPaymentCard(PaymentCardDTO $paymentCardDTO,$userId)
    {
        return $this->paymentCardRepository->savePaymentCard($paymentCardDTO,$userId);
    }

    public function paymentMethodObject($params){
        return $this->paymentCardRepository->paymentMethodObject($params);
    }

    public function getcardDetails($userType,$rentalUserId){
        return $this->paymentCardRepository->cardDetails($userType,$rentalUserId);
    }

    public function getAllPaymentCards($params)
    {
        return $this->paymentCardRepository->paymentMethodCredit($params);
    }

    public function getPaymentCard($params)
    {
        return $this->paymentCardRepository->paymentMethodObject($params);
    }

    public function checkDuplicateCard($params,$token, $expMonth, $expYear){
        $allCards = $this->getAllPaymentCards($params);
        if($allCards) {
            foreach($allCards as $allCard) {
                $tokenFingerprint = $token['card']['fingerprint'];
                $tokenExpMonth = $token['card']['exp_month'];
                $tokenExpYear = $token['card']['exp_year'];
                if ($allCard->fingerprint == $tokenFingerprint) {
                    if (($expMonth == $tokenExpMonth) && ($expYear == $tokenExpYear)) {
                        return true;
                    }
                    return true;
                }
            }
            return false;
        }
    }

    public function saveOwnerCustomerId(OwnerUserDTO $ownerUserDTO){
        $owner = $this->ownerRepository->save($ownerUserDTO);
        return $owner;
    }

    public function saveCard(PaymentCardDTO $paymentCardDTO,$token,$user){

        if(!$paymentCardDTO->id) {
            $paymentCardDTO->userId = $user->id;
            $paymentCardDTO->email = $user->email;
            $paymentCardDTO->cardType = $token['card']['brand'] ? $token['card']['brand'] : 'Credit Card';
            $paymentCardDTO->lastFour = $token['card']['last4'];
            $paymentCardDTO->token = $token['id'];
            $paymentCardDTO->cardToken = $token['card']['id'];
            $paymentCardDTO->fingerPrint = $token['card']['fingerprint'];

        } else {
            $paymentCardDTO->debit = $token['funding'];
        }

        $paymentDetails = $this->paymentCardRepository->saveCard($paymentCardDTO);
        return $paymentDetails;
    }

    public function deletepaymentCard($id)
    {
        $paymentService = new PaymentService();
        $paymentDetails = $this->paymentCardRepository->deletePaymentCard($id);

        $user = getOwner();

        $cardRequestDTO = new CardRequestDTO();
        $cardRequestDTO->token = $paymentDetails->card_token;
        $cardRequestDTO->customerId = $user->customer_id;
        $paymentService->deleteCard($cardRequestDTO);
    }
}
