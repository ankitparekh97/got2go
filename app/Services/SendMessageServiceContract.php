<?php


namespace App\Services;

use App\Models\Property;

interface SendMessageServiceContract
{
    public function postRentalOwnerSendMessage($toUser,$userMessage);

    public function postOwnerRentalSendMessage($toUser,$userMessage);

    public function postAdminRentalSendMessage($toUser,$userMessage);

    public function postAdminOwnerSendMessage($toUser,$userMessage);

    public function postCommonSendMessage($pairId,$fromType,$toType,$fromUser,$toUser,$guard,$message);

    public function send($toUser,$messageArray,$guard);

    public function sendGroupMessage($messageArray,$guard);

}
