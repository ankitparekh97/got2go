<?php


namespace App\Services;


use App\Models\Booking;
use App\Http\DTO\Api\CouponCodeDTO;
use App\Models\CouponCode;
use App\Repositories\CouponCodeInterface;

class CouponCodeService implements CouponCodeContract
{

    public $couponCodeRepository;

    public function __construct(CouponCodeInterface $couponCodeRepository)
    {
        $this->couponCodeRepository = $couponCodeRepository;
    }

    public function getAllCouponCode()
    {
        return $this->couponCodeRepository->getAllCoupons();
    }

    public function addCouponCode(CouponCodeDTO $couponCodeDTO)
    {
        $flagIsDuplicate = $this->checkDuplicateCouponCode($couponCodeDTO->name);
        if($flagIsDuplicate){
            throw new \Exception('This coupon code already has been generated.');
        }
        return $this->couponCodeRepository->saveCoupon($couponCodeDTO);
    }

    public function activeCouponCode($couponCodeId)
    {
        $couponCode = $this->couponCodeRepository->findCouponCodeById($couponCodeId);

        $couponCodeDTO = CouponCodeDTO::convertModelToObject($couponCode);
        $couponCodeDTO->status = true;

        return $this->couponCodeRepository->saveCoupon($couponCodeDTO);
    }

    public function deActiveCouponCode($couponCodeId)
    {
        $couponCode = $this->couponCodeRepository->findCouponCodeById($couponCodeId);

        $couponCodeDTO = CouponCodeDTO::convertModelToObject($couponCode);
        $couponCodeDTO->status = false;

        return $this->couponCodeRepository->saveCoupon($couponCodeDTO);
    }

    public function checkDuplicateCouponCode($couponCodeName)
    {
        $data = $this->couponCodeRepository->findCouponCodeByName($couponCodeName);
        if(!empty($data)){
            return $data->count() > 0;
        }
        return false;
    }

    public function applyCouponCode($couponCodeName,$rentalId)
    {
        /**
         * @var CouponCode $data
         */
        return $this->checkCouponCodeUsedAlreadyByUser($couponCodeName,$rentalId);
    }

    public function checkCouponCodeUsedAlreadyByUser($couponCodeName,$rentalId)
    {
        /**
         * @var CouponCode $data
         */
        $data = $this->couponCodeRepository->findCouponCodeByName($couponCodeName);

        if(empty($data)){
           throw new \Exception('Invalid coupon code');
        }

        if($data->status == '0'){
           throw new \Exception('Invalid coupon code');
        }

        $bookingHasCoupon = $this->numberOfTimesCouponCodeUsedByRentalId($data->id,$rentalId);

        if($data->is_single_use && $bookingHasCoupon > 0){
            throw new \Exception('You have already used this coupon code');
        }

        return $data;
    }

    public function numberOfTimesCouponCodeUsed($couponId){
        return Booking::where(Booking::COUPON_CODES_ID,$couponId)
            ->whereIn(Booking::OWNER_STATUS,['pending','approved','instant_booking','confirmed'])->count();
    }

    public function numberOfTimesCouponCodeUsedByRentalId($couponId,$rentalId){
        return Booking::where(Booking::COUPON_CODES_ID,$couponId)
            ->where(Booking::RENTAL_USER_ID,$rentalId)
            ->whereIn(Booking::OWNER_STATUS,['pending','approved','instant_booking','confirmed'])
            ->count();
    }
}
