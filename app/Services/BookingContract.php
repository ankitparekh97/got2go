<?php


namespace App\Services;

use App\Http\DTO\Api\BookingDTO;
use App\Http\DTO\Api\BookingPaymentDTO;

interface BookingContract
{
    public function getPropertyByOwnerId($ownerId,$status);

    public function getBookingById($Id);

    public function getByParamsObject($params);

    public function getByParamsCollection($params);

    public function checkBookings($propertyId,$bookingId,$checkInDate,$checkoutDate);

    public function updateBookingStatus($bookingId,$status,$confirmationId);

    public function stripePayment($bookings,$cardDetails,$status);

    public function createBookingPayment($bookings,$amount,$charge,$status);

     public function updateBookingOwnerStatus($bookingId, $status);

}
