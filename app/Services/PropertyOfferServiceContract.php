<?php


namespace App\Services;


use App\Http\DTO\Api\MakeOfferDTO;
use App\Models\PropertyOffers;

interface PropertyOfferServiceContract
{
    public function tipperMakeAnOffer();

    public function getPropertyOfferById($propertyOfferId);

    ## Owner
    public function getOwnerPropertiesOffer($ownerId);

    public function acceptOwnerPropertyOffer($propertyOfferId);

    public function declineOwnerPropertyOffer($propertyOfferId);

    public function ownerCounterOfferProperty(MakeOfferDTO $requestDTO,$propertyOfferId);

    public function cancelOwnerOPropertyOffer($propertyOfferId);
    ## End Owner Section

    ## Tripper
    public function getTripperPropertiesOffer($tripperId);

    public function acceptTripperPropertyOffer($propertyOfferId,$bookingId);

    public function declineTripperPropertyOffer($propertyOfferId);

    public function tripperCounterOfferProperty(MakeOfferDTO $requestDTO,$propertyOfferId);

    public function tripperMakeAnOfferProperty(MakeOfferDTO $requestDTO);

    public function tripperMakeAnOfferOnExistingOffer(MakeOfferDTO $requestDTO,$propertyOfferId);

    public function deleteTripperPropertyOffer(MakeOfferDTO $requestDTO);

    public function cancelTripperPropertyOffer($propertyOfferId);

    public function checkOfferDateIsOverLappingByTripperId($startDate,$endDate,$tripperId,$propertyId);
    ## End Tripper Section

    public function sendNotification(PropertyOffers $propertyOffers);

    public function getPendingResponseOnOfferByOwner($ownerId);

    public function getPendingResponseOnOfferByTripper($tripperId);

}
