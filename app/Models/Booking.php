<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Booking
 *
 * @property int $id
 * @property string $booking_type
 * @property int $rental_user_id
 * @property int|null $property_id
 * @property string|null $check_in_date
 * @property string|null $check_out_date
 * @property int|null $adults
 * @property int|null $childrens
 * @property int|null $rooms
 * @property string $actual_price
 * @property string $offer_amount
 * @property string $tax
 * @property string $occupancy_tax_fees
 * @property string $total
 * @property string $status
 * @property string $is_cancelled
 * @property string|null $cancellation_date
 * @property string $owner_status
 * @property string|null $property_status
 * @property int|null $confirmation_id
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $property_offers_id
 * @property string|null $card_token
 * @property string|null $tripper_saving_amount
 * @property string|null $coupon_codes_id
 * @property string|null $payout_fee
 * @property string|null $cleaning_fee
 * @property string|null $transaction_fee
 * @property-read \App\Models\BookingPayment|null $booking_payment
 * @property-read \App\Models\Property|null $property
 * @property-read \App\Models\RentalUser $rentaluser
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking query()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereActualPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereAdults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereBookingType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCancellationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCheckInDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCheckOutDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereChildrens($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereConfirmationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereIsCancelled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereOccupancyTaxFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereOfferAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereOwnerStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking wherePropertyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking wherePropertyOffersId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking wherePropertyStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereRentalUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereRooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking checkStartDateEndDateBetweenDates($startDate, $endDate)
 * @mixin \Eloquent
 */
class Booking extends Model
{
    //

    public const TABLE_NAME = 'booking';
    public const ID = 'id';
    public const BOOKING_TYPE = 'booking_type';
    public const RENTAL_USER_ID = 'rental_user_id';
    public const PROPERTY_ID = 'property_id';
    public const CHECK_IN_DATE = 'check_in_date';
    public const CHECK_OUT_DATE = 'check_out_date';
    public const ADULTS = 'adults';
    public const CHILDRENS = 'childrens';
    public const ROOMS = 'rooms';
    public const ACTUAL_PRICE = 'actual_price';
    public const OFFER_AMOUNT = 'offer_amount';
    public const TAX = 'tax';
    public const OCCUPANCY_TAX_FEES = 'occupancy_tax_fees';
    public const TOTAL = 'total';
    public const STATUS = 'status';
    public const IS_CANCELLED = 'is_cancelled';
    public const CANCELLATION_DATE = 'cancellation_date';
    public const OWNER_STATUS = 'owner_status';
    public const PROPERTY_STATUS = 'property_status';
    public const CONFIRMATION_ID = 'confirmation_id';
    public const MESSAGE = 'message';
    public const PROPERTY_OFFERS_ID = 'property_offers_id';
    public const CARD_TOKEN = 'card_token';
    public const TRIPPER_SAVING_AMOUNT = 'tripper_saving_amount';
    public const COUPON_CODES_ID = 'coupon_codes_id';
    public const PAYOUT_FEE = 'payout_fee';
    public const CLEANING_FEE = 'cleaning_fee';
    public const TRANSACTION_FEE = 'transaction_fee';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::TABLE_NAME,
        self::ID,
        self::BOOKING_TYPE,
        self::RENTAL_USER_ID,
        self::PROPERTY_ID,
        self::CHECK_IN_DATE,
        self::CHECK_OUT_DATE,
        self::ADULTS,
        self::CHILDRENS,
        self::ROOMS,
        self::ACTUAL_PRICE,
        self::OFFER_AMOUNT,
        self::TAX,
        self::OCCUPANCY_TAX_FEES,
        self::TOTAL,
        self::STATUS,
        self::IS_CANCELLED,
        self::CANCELLATION_DATE,
        self::OWNER_STATUS,
        self::PROPERTY_STATUS,
        self::CONFIRMATION_ID,
        self::MESSAGE,
        self::PROPERTY_OFFERS_ID,
        self::CARD_TOKEN,
        self::TRIPPER_SAVING_AMOUNT,
        self::COUPON_CODES_ID,
        self::PAYOUT_FEE,
        self::CLEANING_FEE,
        self::TRANSACTION_FEE,
    ];

    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }

     public function rentaluser()
    {
        return $this->belongsTo(RentalUser::class,'rental_user_id');
    }

    public function bookingPayment()
    {
        return $this->hasOne(BookingPayment::class);
    }

    ### Scope

    public function scopeCheckStartDateEndDateBetweenDates($checkStartDateEndDateBetweenDates,$startDate,$endDate){

        $checkStartDateEndDateBetweenDates->where(function($bookingDateRangeQry) use($startDate,$endDate){
            $bookingDateRangeQry->where(function($bookingDateRangeQry3) use($startDate,$endDate){
                $bookingDateRangeQry3->where(Booking::CREATED_AT,'>=',$startDate);
                $bookingDateRangeQry3->where(Booking::CREATED_AT,'<=',$endDate);
            });
        });

        return $checkStartDateEndDateBetweenDates;
    }
}
