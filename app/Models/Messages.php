<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    public const TABLE_NAME = 'messages';
    public const ID = 'id';
    public const PAIR_ID = 'pair_id';
    public const FROM_USER = 'from_user';
    public const TO_USER = 'to_user';
    public const MESSAGES = 'messages';
    public const FROM_USER_TYPE = 'from_user_type';
    public const TO_USER_TYPE = 'to_user_type';
    public const MESSAGE_TYPE = 'message_type';
    public const IS_READ = 'is_read';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected $table = self::TABLE_NAME;

    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['pair_id','from_user','to_user','messages','from_user_type','to_user_type','is_read'];

    /**
     * A message belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fromUser()
    {
        return $this->belongsTo('App\User', 'from_user');
    }

    public function toUser()
    {
        return $this->belongsTo('App\User', 'to_user');
    }

    public function pairUser()
    {
        return $this->hasOne(OwnerRentalPair::class,'id','pair_id');
    }
}
