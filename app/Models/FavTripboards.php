<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavTripboards extends Model
{
    protected $table = 'favourite_tripboard';

    public function property(){
        return $this->hasOne(Property::class,'id','property_id');
    }

}
