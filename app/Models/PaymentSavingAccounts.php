<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSavingAccounts extends Model
{
    public const ID = 'id';

    public const TABLE_NAME = 'payment_savings_accounts';
    protected $table = self::TABLE_NAME;

    protected $fillable = ['user_type','user_id','first_name','routing_number','institution_name',
                            'account_number','default', 'ssn','city','state','zipcode','stripe_account_id','fingerprint'];

}
