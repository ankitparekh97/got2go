<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class OwnerProperty extends Model
{
    // use Searchable;
    // protected $indexConfigurator = MyIndexConfiguratorOwner::class;

    protected $table = 'owner_property';

    protected $guarded = ['id'];


    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function propertyreviews()
    {
        return $this->hasMany(PropertyReviews::class,'owner_property_id','id')->with('ownerproperty');
    }

    // public function searchableAs()
    // {
    //     return 'ownerproperty';
    // }

    // protected $mapping = [
    //     'properties' => [
    //         'name_your_place' => [
    //             'type' => 'text',
    //             // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
    //             'fields' => [
    //                 'raw' => [
    //                     'type' => 'keyword',
    //                 ]
    //             ]
    //         ],
    //     ]
    // ];
}
