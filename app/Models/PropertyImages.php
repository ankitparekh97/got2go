<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PropertyImages
 *
 * @property int $id
 * @property int|null $property_id
 * @property string $photo
 * @property string|null $media_type
 * @property string|null $originalName
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Property|null $property
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages query()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages whereMediaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages wherePropertyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImages whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PropertyImages extends Model
{
    protected $table = 'property_images';

    protected $guarded = ['id'];

    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }
}
