<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripboardBuddies extends Model
{
    protected $table = 'tripboard_travel_buddies';
}
