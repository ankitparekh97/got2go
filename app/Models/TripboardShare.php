<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripboardShare extends Model
{
    protected $table = 'tripboard_share';
    protected $fillable = ['tripboard_id','user_id'];

}
