<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentCards
 *
 * @property int $id
 * @property string $user_type
 * @property int $user_id
 * @property string $name_on_card
 * @property string $email
 * @property string $card_type
 * @property string $last_four
 * @property string $expiration_date
 * @property string|null $debit
 * @property int|null $default
 * @property string|null $image_url
 * @property string|null $masked_number
 * @property string|null $token
 * @property string|null $unique_identifier
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $address
 * @property string|null $address2
 * @property string|null $state
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $card_token
 * @property-read \App\Models\RentalUser $rental_user
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereDebit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereMaskedNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereNameOnCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereUniqueIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCards whereUserType($value)
 * @mixin \Eloquent
 */
class PaymentCards extends Model
{
    public const ID = 'id';

    public const TABLE_NAME = 'payment_cards';
    protected $table = self::TABLE_NAME;

    protected $fillable = ['user_type','user_id','name_on_card','email','card_type','last_four',
                            'expiration_date','cvv','default', 'address','address2','state','city','phone'.'masked_number','token','card_token', 'fingerprint'];

    public function rental_user()
    {
        return $this->belongsTo(RentalUser::class,'user_id');
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class,'user_id');
    }
}
