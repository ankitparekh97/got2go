<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripboardProperty extends Model
{
    protected $table = 'tripboard_property';

    public function tripboard()
    {
        return $this->belongsTo(Tripboards::class,'tripboard_id');
    }
}
