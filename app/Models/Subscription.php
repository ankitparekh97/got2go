<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property float|null $annual_subscription
 * @property float|null $discount_percentage
 * @property float|null $strike_through_amount
 * @property int|null $rental_user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereAnnualSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereRentalUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStrikeThroughAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Subscription extends Model
{
    protected $table = 'subscription';
    protected $fillable = [
        'annual_subscription', 'discount_percentage', 'rental_user_id', 'strike_through_amount', '# id, annual_subscription, discount_percentage, rental_user_id, created_at, updated_at'
    ];


}
