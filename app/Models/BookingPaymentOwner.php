<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingPaymentOwner extends Model
{
    //
    protected $table = 'booking_payment_owner';

    protected  $fillable = [
        'booking_id', 'owner_id', 'payment_type', 'transaction_number', 'amount'
    ];


}
