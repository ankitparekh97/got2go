<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAmenities extends Model
{
    protected $guard = 'property_amenities';
    protected $table = 'property_amenities';

    protected $guarded = ['id'];

    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }
    public function hotels()
    {
        return $this->belongsTo(Hotels::class,'property_id');
    }
    public function masteramenity()
    {
        return $this->belongsTo(MasterAmenities::class,'amenity_id');
    }
}
