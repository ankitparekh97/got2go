<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RentalUserSubscription
 *
 * @property int $id
 * @property int|null $rental_id
 * @property string|null $billing_period_start_date
 * @property string|null $billing_period_end_date
 * @property string|null $first_billing_date
 * @property string|null $current_billing_cycle
 * @property string|null $description
 * @property string|null $subscription_id
 * @property string|null $next_billing_date
 * @property string|null $next_billing_amount
 * @property string|null $paid_through_date
 * @property string|null $plan_id
 * @property string|null $price
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $paid_through
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription newQuery()
 * @method static \Illuminate\Database\Query\Builder|RentalUserSubscription onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereBillingPeriodEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereBillingPeriodStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereCurrentBillingCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereFirstBillingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereNextBillingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereNextBillingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription wherePaidThrough($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription wherePaidThroughDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereRentalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUserSubscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|RentalUserSubscription withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RentalUserSubscription withoutTrashed()
 * @mixin \Eloquent
 */
class RentalUserSubscription extends Model
{
    use SoftDeletes;

    protected $table = 'rental_user_subscription';
}
