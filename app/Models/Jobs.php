<?php

namespace App\Models;

use App\Jobs\BulkUpload;
use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    public const ID = 'id';
    public const QUEUE = 'queue';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'jobs';

    protected $table = self::TABLE_NAME;

    public function queue()
    {
        return $this->hasOne(BulkUpload::class,'queue_id');
    }
}
