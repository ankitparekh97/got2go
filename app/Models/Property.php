<?php

namespace App\Models;
use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Property
 *
 * @property int $id
 * @property int $owner_id
 * @property int $resort_id
 * @property int|null $property_type_id
 * @property int $sub_property_type_id
 * @property string|null $title
 * @property string $description
 * @property string|null $about_the_space
 * @property string|null $location_detail
 * @property string $is_for_guest 0=no|1=yes
 * @property int $guest_will_have
 * @property string $list_as_company 0=no|1=yes
 * @property string|null $city
 * @property string|null $state
 * @property string|null $state_abbrv
 * @property string|null $country
 * @property int $zipcode
 * @property int $no_of_guest
 * @property int|null $total_bedroom
 * @property int|null $total_bathroom
 * @property int|null $total_beds
 * @property string|null $lat_lang
 * @property string $is_instant_booking_allowed
 * @property string $price
 * @property string $is_price_negotiable
 * @property string $is_partial_payment_allowed
 * @property string $partial_payment_amount
 * @property string|null $check_in
 * @property string|null $check_out
 * @property string|null $available_from
 * @property string|null $available_to
 * @property string|null $unavailable_from
 * @property string|null $unavailable_to
 * @property string|null $proof_of_ownership_or_lease
 * @property string|null $gov_issue_id
 * @property string $cancellation_type
 * @property string $publish
 * @property string $status
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $type_of_property
 * @property string|null $cover_photo
 * @property string $is_hotdeals
 * @property string|null $video
 * @property float|null $cleaning_fee
 * @property int $extra_beds
 * @property int $last_tab_id
 * @property string|null $bulk_upload_id
 * @property int|null $offer_price
 * @property string|null $reason_decline
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Booking[] $booking
 * @property-read int|null $booking_count
 * @property-read \App\Models\Booking $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Owner[] $ownerdetails
 * @property-read int|null $ownerdetails_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyAmenities[] $propertyamenities
 * @property-read int|null $propertyamenities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyAvaiblities[] $propertyavailiblity
 * @property-read int|null $propertyavailiblity_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyBedrooms[] $propertybedrooms
 * @property-read int|null $propertybedrooms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyImages[] $propertyimages
 * @property-read int|null $propertyimages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyOwnershipDocuments[] $propertyownershipdoc
 * @property-read int|null $propertyownershipdoc_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyReviews[] $propertyreviews
 * @property-read int|null $propertyreviews_count
 * @property-read \App\Models\PropertyType|null $propertytype
 * @property-read \App\Models\Hotels|null $resort
 * @method static \Illuminate\Database\Eloquent\Builder|Property newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Property newQuery()
 * @method static \Illuminate\Database\Query\Builder|Property onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Property query()
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereAboutTheSpace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereAvailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereAvailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCancellationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCheckIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCheckOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCleaningFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCoverPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereExtraBeds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereGovIssueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereGuestWillHave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereIsForGuest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereIsHotdeals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereIsInstantBookingAllowed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereIsPartialPaymentAllowed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereIsPriceNegotiable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereLastTabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereLatLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereListAsCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereLocationDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereNoOfGuest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property wherePartialPaymentAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereProofOfOwnershipOrLease($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property wherePropertyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property wherePublish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereResortId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereStateAbbrv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereSubPropertyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereTotalBathroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereTotalBedroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereTotalBeds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereTypeOfProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereUnavailableFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereUnavailableTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereZipcode($value)
 * @method static \Illuminate\Database\Query\Builder|Property withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Property withoutTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereOfferPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereReasonDecline($value)
 * @mixin \Eloquent
 */
class Property extends Model
{

    use SoftDeletes;
    // use Searchable;
    // protected $indexConfigurator = MyIndexConfigurator::class;

    public const ID = 'id';
    public const OWNER_ID = 'owner_id';
    public const STATUS = 'status';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'property';

    protected $table = self::TABLE_NAME;

    protected $guarded = ['id'];

    public function propertytype()
    {
        return $this->belongsTo(PropertyType::class,'property_type_id');
    }

    public function resort()
    {
        return $this->belongsTo(Hotels::class,'resort_id');
    }

    public function propertyimages()
    {
        return $this->hasMany(PropertyImages::class,'property_id');
    }

    public function propertyownershipdoc()
    {
        return $this->hasMany(PropertyOwnershipDocuments::class,'property_id');
    }

    public function propertyavailiblity()
    {
        return $this->hasMany(PropertyAvaiblities::class,'property_id');
    }

    public function propertybedrooms()
    {
        return $this->hasMany(PropertyBedrooms::class,'property_id');
    }

    public function propertyamenities()
    {
        return $this->hasMany(PropertyAmenities::class,'property_id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class);
    }

    public function owner()
    {
       return $this->belongsTo(Booking::class,'property_id');
    }

    public function ownerdetails()
    {
        return $this->belongsTo(Owner::class,'owner_id');
    }


    public function propertyreviews()
    {
        return $this->hasMany(PropertyReviews::class,'property_id');
    }

    public function getTotalReviews()
    {
        return $this->attributes['count'] = $this->propertyreviews->avg('rating');
    }

    public function getAverageReviews()
    {
        return $this->attributes['avg'] = $this->propertyreviews->avg('rating');
    }

    public function favTripboards()
    {
        return $this->belongsTo(FavTripboards::class,'id');
    }

    // public function searchableAs()
    // {
    //     return 'property';
    // }

    // public function toSearchableArray()
    // {
    //     $array = Property::where('type_of_property','property')
    //     ->select('id','type_of_property', 'location_detail' )
    //     ->searchable();
    //     return  $array;
    // }

    public static function makeSearchable()
    {
        $array = Property::join('owner_property', 'owner_property.property_id','=','property.id')
        ->select('property.id','type_of_property', 'location_detail','lat_lang', 'owner_property.no_of_guest', 'city','state', 'state_abbrv','country' )
        ->where('type_of_property','property')
        // ->with('ownerproperty:no_of_guest')
        ->searchable();


        return  $array;
    }

    public static function makeSearchableById($ids)
    {
        $array = Property::join('owner_property', 'owner_property.property_id','=','property.id')
        ->select('property.id','type_of_property', 'location_detail','lat_lang', 'owner_property.no_of_guest', 'city','state', 'state_abbrv','country' )
        ->where('type_of_property','property')
        ->whereIn('property.id',$ids)
        // ->with('ownerproperty:no_of_guest')
        ->searchable();


        return  $array;
    }


    // Here you can specify a mapping for model fields
    // protected $mapping = [
    //     'properties' => [
    //         'text' => [
    //             'type' => 'text',
    //             // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
    //             'fields' => [
    //                 'lcoation_detail' => [
    //                     'type' => 'keyword',
    //                 ],
    //                 'lat_long' => [
    //                     "type"=> "geo_point"
    //                 ],
    //                 'city' => [
    //                     "type"=> "keyword"
    //                 ],
    //                 'state' => [
    //                     "type"=> "keyword"
    //                 ]
    //             ]
    //         ],
    //     ]
    // ];
}
