<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulkUpload extends Model
{
    public const ID = 'id';
    public const OWNER_ID = 'owner_id';
    public const PROPERTY_DOC = 'property_doc';
    public const PROPERTY_MEDIA = 'property_images';
    public const PROPERTY_VERIFICATION = 'verfication_doc';
    public const PROPERTY_GOVT = 'govtid_doc';
    public const STATUS = 'status';
    public const UNIQUE_KEY = 'unique_key';
    public const IS_REUPLOAD = 'is_reupload';
    public const QUEUE_ID = 'queue_id';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'bulk_upload';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'owner_id', 'property_doc', 'property_images', 'verfication_doc', 'govtid_doc', 'status', 'unique_key','queue_id'
    ];

    public function owner()
    {
        return $this->belongsTo(Owner::class,'owner_id');
    }

    public function logs()
    {
        return $this->hasOne(BulkUploadLogs::class,'bulk_upload_id');
    }

    public function queue()
    {
        return $this->belongsTo(Jobs::class);
    }
}
