<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunicationPreferences extends Model
{
    protected $table = 'communication_preferences';

    protected  $fillable = [
        'user_type', 'userid', 'communication_title', 'is_notification', 'is_email', 'is_sms'
    ];

    public function rental_user(){
        return $this->belongsTo(RentalUser::class);
    }
}
