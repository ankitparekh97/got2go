<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CouponCode
 *
 * @property int $id
 * @property string|null $name
 * @property float|null $discount_percentage
 * @property int|null $is_single_use
 * @property int|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereDiscountPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereIsSingleUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponCode whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CouponCode extends Model
{
    use SoftDeletes;

    public const ID = 'id';
    public const NAME = 'name';
    public const DISCOUNT_PERCENTAGE = 'discount_percentage';
    public const IS_SINGLE_USE = 'is_single_use';
    public const STATUS = 'status';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'coupon_codes';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::ID,
        self::NAME,
        self::DISCOUNT_PERCENTAGE,
        self::IS_SINGLE_USE,
        self::STATUS,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
    ];

}
