<?php


namespace App\Models\DTO;


use Spatie\DataTransferObject\DataTransferObject;

class PropertyOfferDTO extends DataTransferObject
{
    /**
     * @var int|null $propertyId
     */
    public $id;

    /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var int|null $ownerId
     */
    public $ownerId;

    /**
     * @var int|null $rentalId
     */
    public $rentalId;

    /**
     * @var bool|null $actionBy
     */
    public $actionBy;

    /**
     * @var bool|null $status
     */
    public $status;

    /**
     * @var float|null $tripperOfferNightlyPrice
     */
    public $tripperOfferNightlyPrice;

    /**
     * @var float|null $ownerOfferNightlyPrice
     */
    public $ownerOfferNightlyPrice;

    /**
     * @var \Illuminate\Support\Carbon|null $tripperOfferStartDate
     */
    public $tripperOfferStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $tripperofferEndDate
     */
    public $tripperOfferEndDate;

    /**
     * @var \Illuminate\Support\Carbon|null $ownerOfferStartDate
     */
    public $ownerOfferStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $ownerOfferEndDate
     */
    public $ownerOfferEndDate;

    /**
     * @var float|null $ownerOfferProposedTotal
     */
    public $ownerOfferProposedTotal;

    /**
     * @var float|null $tripperOfferProposedTotal
     */
    public $tripperOfferProposedTotal;

    /**
     * @var int|null $numberOfGuests
     */
    public $numberOfGuests;

    /**
     * @var bool|null $flexibleDates
     */
    public $flexibleDates;

    /**
     * @var float|null $finalAcceptedNightlyPrice
     */
    public $finalAcceptedNightlyPrice;

    /**
     * @var \Illuminate\Support\Carbon|null $finalAcceptedStartDate
     */
    public $finalAcceptedStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $finalAcceptedEndDate
     */
    public $finalAcceptedEndDate;

    /**
     * @var int|null $bookingId
     */
    public $bookingId;

    /*public static function convertPostDataToObject($request)
    {
        $rentalUserDtoArr = array();
        $rentalUserDtoArr['propertyId'] = $request->post('propertyId');
        $rentalUserDtoArr['ownerId'] = $request->post('ownerId');
        $rentalUserDtoArr['rentalId'] = $request->post('rentalId');
        $rentalUserDtoArr['status'] = $request->post('status');
        $rentalUserDtoArr['tripperOfferNightlyPrice'] = $request->post('tripperOfferNightlyPrice');
        $rentalUserDtoArr['ownerOfferNightlyPrice'] = $request->post('ownerOfferNightlyPrice');
        $rentalUserDtoArr['tripperOfferStartDate'] = $request->post('tripperOfferStartDate');
        $rentalUserDtoArr['tripperOfferEndDate'] = $request->post('tripperOfferEndDate');
        $rentalUserDtoArr['ownerOfferStartDate'] = $request->post('ownerOfferStartDate');
        $rentalUserDtoArr['ownerOfferEndDate'] = $request->post('ownerOfferEndDate');
        $rentalUserDtoArr['ownerOfferProposedTotal'] = $request->post('ownerOfferProposedTotal');
        $rentalUserDtoArr['tripperOfferProposedTotal'] = $request->post('tripperOfferProposedTotal');
        $rentalUserDtoArr['numberOfGuests'] = $request->post('numberOfGuests');
        $rentalUserDtoArr['flexibleDates'] = $request->post('flexibleDates');
        return new self($rentalUserDtoArr);
    }*/
}
