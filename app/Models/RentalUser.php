<?php

namespace App\Models;

use App\Models\RentalUserSubscription;
use App\Traits\RentalSubscriptionTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * App\Models\RentalUser
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $google_id
 * @property string|null $facebook_id
 * @property string $email
 * @property string|null $password
 * @property string|null $bio
 * @property string|null $otp_code
 * @property string $is_otp_verified
 * @property string|null $birthdate
 * @property string|null $gender
 * @property string|null $phone
 * @property string|null $emergency_number
 * @property string|null $address
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $site_language
 * @property string|null $currency
 * @property string $status
 * @property string|null $photo
 * @property string|null $goverment_id_doc
 * @property string $share_on_fb
 * @property string $marketing_email
 * @property string|null $linked_paypal_account
 * @property string|null $activation_url
 * @property string $is_verified
 * @property int $is_tripper
 * @property string $tripper_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $is_subscribed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Booking[] $booking
 * @property-read int|null $booking_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CommunicationPreferences[] $communication_preferences
 * @property-read int|null $communication_preferences_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\PaymentCards|null $paymentcards_user
 * @property-read \App\Models\OwnerRentalPair $rentalUser
 * @property-read \App\Models\RentalUserSubscription $subscription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RentalUserSubscription[] $allSubscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookingPayment[] $bookingPayments
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereActivationUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereBio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereEmergencyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereGovermentIdDoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereIsOtpVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereIsSubscribed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereIsTripper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereLinkedPaypalAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereMarketingEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereOtpCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereShareOnFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereSiteLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereTripperStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereZip($value)
 * @mixin \Eloquent
 * @property string|null $customer_id
 * @property-read \App\Models\GroupMessages $groupMessages
 * @property-read int|null $paymentcards_user_count
 * @method static \Illuminate\Database\Eloquent\Builder|RentalUser whereCustomerId($value)
 */
class RentalUser extends Authenticatable implements JWTSubject{
    use Notifiable;
    protected $guard = 'rental_user';
    protected $table = 'rental_user';

    protected $fillable = [
        'full_name', 'first_name','last_name', 'email', 'password', 'google_id', 'facebook_id','bio','otp_code','birthdate','is_otp_verified','birthdate','gender','phone','emergency_number','address','city','state','zip','site_language','currency','status','photo','goverment_id_doc','share_on_fb','marketing_email', 'activation_url', 'is_verified', 'is_tripper', 'tripper_status','linked_paypal_account','is_subscribed','customer_id'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'rental_user_id');
    }

    public function bookingPayments()
    {
        return $this->hasMany(BookingPayment::class, 'rental_user_id');
    }

    public function paymentcards_user()
    {
        return $this->hasMany(PaymentCards::class,'user_id');
    }
    public function rentalUser()
    {
        return $this->belongsTo(OwnerRentalPair::class,'rental_id');
    }

    public function communication_preferences()
    {
        return $this->hasMany(CommunicationPreferences::class,'userid');
    }

    public function groupMessages()
    {
        return $this->belongsTo(GroupMessages::class,'from_user');
    }

    public function getFullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getTripperSubscriptionAmount()
    {
        return Subscription::first()->annual_subscription;
    }

    public function subscription()
    {
        return $this->hasOne(
            RentalUserSubscription::class,
            'rental_id',
            'id'
        );
    }

    public function allSubscriptions()
    {
        return $this->hasMany(
            RentalUserSubscription::class,
            'rental_id',
            'id'
        )->withTrashed();
    }
}
