<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingPayment extends Model
{
    //
    public const TABLE_NAME = 'booking_payment';

    public const ID = 'id';
    public const BOOKING_TYPE = 'booking_id';
    public const RENTAL_USER_ID = 'rental_user_id';
    public const PAYMENT_TYPE = 'payment_type';
    public const TRANSACTION_NUMBER = 'transaction_number';
    public const AMOUNT = 'amount';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    protected $table = self::TABLE_NAME;

    protected  $fillable = [
        'booking_id', 'rental_user_id', 'payment_type', 'transaction_number', 'amount'
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

}
