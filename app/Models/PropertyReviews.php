<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyReviews extends Model
{
    public const ID = 'id';
    public const PROPERTY_ID = 'property_id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'property_reviews';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'property_id', 'rating', 'reviews'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
