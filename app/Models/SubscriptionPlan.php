<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubscriptionPlan
 *
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property string|null $amount
 * @property string|null $currency
 * @property string|null $interval
 * @property string|null $statement_descriptor
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereStatementDescriptor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereUpdatedAt($value)
 */
class SubscriptionPlan extends Model
{
    public const TABLE_NAME = 'subscription_plan';

    public const ID = 'id';
    public const NAME = 'name';
    public const AMOUNT = 'amount';
    public const CURRENCY = 'currency';
    public const INTERVAL = 'interval';
    public const STATEMENT_DESCRIPTOR = 'statement_descriptor';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected $table = self::TABLE_NAME;

    protected $keyType = 'string';

    protected $fillable = [
        self::ID,
        self::NAME,
        self::AMOUNT,
        self::CURRENCY,
        self::INTERVAL,
        self::STATEMENT_DESCRIPTOR,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
    ];
}
