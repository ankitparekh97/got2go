<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class
BulkUploadLogs extends Model
{
    public const ID = 'id';
    public const BULK_UPLOAD_ID = 'bulk_upload_id';
    public const UPLOADED_LOGS = 'uploaded_logs';
    public const FAILED_LOGS = 'failed_logs';
    public const FAILED_LOGS_MEDIA = 'failed_logs_media';
    public const FAILED_LOGS_VERIFICATION = 'failed_logs_verification';
    public const FAILED_LOGS_GOVT = 'failed_logs_govtId';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'bulk_upload_logs';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'bulk_upload_id', 'uploaded_logs', 'failed_logs', 'failed_logs_media', 'failed_logs_verification', 'failed_logs_govtId'
    ];

    public function bulkUpload()
    {
        return $this->belongsTo(BulkUpload::class);
    }

}
