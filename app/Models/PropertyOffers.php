<?php

namespace App\Models;

use App\Models\Booking;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Owner;
use App\Models\Property;
use App\Models\RentalUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * App\Models\PropertyOffers
 *
 * @property int $id
 * @property int $property_id
 * @property int $owner_id
 * @property int $rental_id
 * @property string|null $action_by
 * @property string|null $status
 * @property string|null $tripper_offer_nightly_price
 * @property string|null $owner_offer_nightly_price
 * @property \Illuminate\Support\Carbon|null $tripper_offer_start_date
 * @property \Illuminate\Support\Carbon|null $tripper_offer_end_date
 * @property \Illuminate\Support\Carbon|null $owner_offer_start_date
 * @property \Illuminate\Support\Carbon|null $owner_offer_end_date
 * @property string|null $tripper_offer_proposed_total
 * @property string|null $owner_offer_proposed_total
 * @property int|null $number_of_guests
 * @property float|null $final_accepted_nightly_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $flexible_dates
 * @property string|null $final_accepted_start_date
 * @property string|null $final_accepted_end_date
 * @property int|null $booking_id
 * @property-read Booking|null $booking
 * @property-read Property $property
 * @method static Builder|PropertyOffers checkOfferDateIsOverLapping($startDate, $endDate)
 * @method static Builder|PropertyOffers pendingResponseOnOfferByOwner($ownerId)
 * @method static Builder|PropertyOffers pendingResponseOnOfferByTripper($tripperId)
 * @method static Builder|PropertyOffers newModelQuery()
 * @method static Builder|PropertyOffers newQuery()
 * @method static \Illuminate\Database\Query\Builder|PropertyOffers onlyTrashed()
 * @method static Builder|PropertyOffers query()
 * @method static Builder|PropertyOffers whereActionBy($value)
 * @method static Builder|PropertyOffers whereBookingId($value)
 * @method static Builder|PropertyOffers whereCreatedAt($value)
 * @method static Builder|PropertyOffers whereDeletedAt($value)
 * @method static Builder|PropertyOffers whereFinalAcceptedEndDate($value)
 * @method static Builder|PropertyOffers whereFinalAcceptedNightlyPrice($value)
 * @method static Builder|PropertyOffers whereFinalAcceptedStartDate($value)
 * @method static Builder|PropertyOffers whereFlexibleDates($value)
 * @method static Builder|PropertyOffers whereId($value)
 * @method static Builder|PropertyOffers whereNumberOfGuests($value)
 * @method static Builder|PropertyOffers whereOwnerId($value)
 * @method static Builder|PropertyOffers whereOwnerOfferEndDate($value)
 * @method static Builder|PropertyOffers whereOwnerOfferNightlyPrice($value)
 * @method static Builder|PropertyOffers whereOwnerOfferProposedTotal($value)
 * @method static Builder|PropertyOffers whereOwnerOfferStartDate($value)
 * @method static Builder|PropertyOffers wherePropertyId($value)
 * @method static Builder|PropertyOffers whereRentalId($value)
 * @method static Builder|PropertyOffers whereStatus($value)
 * @method static Builder|PropertyOffers whereTripperOfferEndDate($value)
 * @method static Builder|PropertyOffers whereTripperOfferNightlyPrice($value)
 * @method static Builder|PropertyOffers whereTripperOfferProposedTotal($value)
 * @method static Builder|PropertyOffers whereTripperOfferStartDate($value)
 * @method static Builder|PropertyOffers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|PropertyOffers withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PropertyOffers withoutTrashed()
 * @mixin \Eloquent
 */
class PropertyOffers extends Model
{
    use SoftDeletes;

    public const ID = 'id';
    public const PROPERTY_ID = 'property_id';
    public const OWNER_ID = 'owner_id';
    public const RENTAL_ID = 'rental_id';
    public const ACTION_BY = 'action_by';
    public const STATUS = 'status';
    public const TRIPPER_OFFER_NIGHTLY_PRICE  = 'tripper_offer_nightly_price';
    public const OWNER_OFFER_NIGHTLY_PRICE = 'owner_offer_nightly_price';
    public const TRIPPER_OFFER_START_DATE = 'tripper_offer_start_date';
    public const TRIPPER_OFFER_END_DATE = 'tripper_offer_end_date';
    public const OWNER_OFFER_START_DATE = 'owner_offer_start_date';
    public const OWNER_OFFER_END_DATE = 'owner_offer_end_date';
    public const TRIPPER_OFFER_PROPOSED_TOTAL = 'tripper_offer_proposed_total';
    public const OWNER_OFFER_PROPOSED_TOTAL = 'owner_offer_proposed_total';
    public const NUMBER_OF_GUEST = 'number_of_guests';
    public const FLEXIBLE_DATES = 'flexible_dates';
    public const FINAL_ACCEPTED_NIGHTLY_PRICE = 'final_accepted_nightly_price';
    public const FINAL_ACCEPTED_START_DATE = 'final_accepted_start_date';
    public const FINAL_ACCEPTED_END_DATE = 'final_accepted_end_date';
    public const BOOKING_ID = 'booking_id';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'property_offers';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::ID,
        self::PROPERTY_ID,
        self::OWNER_ID,
        self::RENTAL_ID,
        self::ACTION_BY,
        self::STATUS,
        self::TRIPPER_OFFER_NIGHTLY_PRICE,
        self::OWNER_OFFER_NIGHTLY_PRICE,
        self::TRIPPER_OFFER_START_DATE,
        self::TRIPPER_OFFER_END_DATE,
        self::OWNER_OFFER_START_DATE,
        self::OWNER_OFFER_END_DATE,
        self::TRIPPER_OFFER_PROPOSED_TOTAL,
        self::OWNER_OFFER_PROPOSED_TOTAL,
        self::NUMBER_OF_GUEST,
        self::FLEXIBLE_DATES,
        self::FINAL_ACCEPTED_NIGHTLY_PRICE,
        self::FINAL_ACCEPTED_START_DATE,
        self::FINAL_ACCEPTED_END_DATE,
        self::BOOKING_ID,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
    ];

    ### Relationships

    public function property()
    {
        return $this->belongsTo(
            Property::class,
            self::PROPERTY_ID,
            'id'
        );
    }

    public function booking()
    {
        return $this->belongsTo(
            Booking::class,
            self::BOOKING_ID,
            'id'
        );
    }

    public function owner()
    {
        return $this->belongsTo(
            Owner::class,
            self::OWNER_ID,
            'id'
        );
    }

    public function rental()
    {
        return $this->belongsTo(
            RentalUser::class,
            self::RENTAL_ID,
            'id'
        );
    }

    ### End of Relationships

    ### Scope

    public function scopeCheckOfferDateIsOverLapping($checkOfferDateIsOverLappingQuery,$startDate,$endDate){

        $checkOfferDateIsOverLappingQuery->where(function($propertyOfferDateRangeQry) use($startDate,$endDate){

            $propertyOfferDateRangeQry->where(function($propertyOfferDateRangeQry1) use($startDate,$endDate) {

                $propertyOfferDateRangeQry1->whereRaw("'{$startDate}' between ".PropertyOffers::TRIPPER_OFFER_START_DATE." and ".PropertyOffers::TRIPPER_OFFER_END_DATE);
                $propertyOfferDateRangeQry1->orWhereRaw("'{$endDate}' between ".PropertyOffers::TRIPPER_OFFER_START_DATE." and ".PropertyOffers::TRIPPER_OFFER_END_DATE);

            });

            $propertyOfferDateRangeQry->orWhere(function($propertyOfferDateRangeQry2) use($startDate,$endDate){

                $propertyOfferDateRangeQry2->whereRaw("'{$startDate}' between ".PropertyOffers::OWNER_OFFER_START_DATE." and ".PropertyOffers::OWNER_OFFER_END_DATE);
                $propertyOfferDateRangeQry2->orWhereRaw("'{$endDate}' between ".PropertyOffers::OWNER_OFFER_START_DATE." and ".PropertyOffers::OWNER_OFFER_END_DATE);

            });

            $propertyOfferDateRangeQry->orWhere(function($propertyOfferDateRangeQry3) use($startDate,$endDate){

                $propertyOfferDateRangeQry3->whereDate(PropertyOffers::TRIPPER_OFFER_START_DATE,'>',$startDate);
                $propertyOfferDateRangeQry3->whereDate(PropertyOffers::TRIPPER_OFFER_END_DATE,'<',$endDate);

            });

            $propertyOfferDateRangeQry->orWhere(function($propertyOfferDateRangeQry4) use($startDate,$endDate){

                $propertyOfferDateRangeQry4->whereDate(PropertyOffers::OWNER_OFFER_START_DATE,'>',$startDate);
                $propertyOfferDateRangeQry4->whereDate(PropertyOffers::OWNER_OFFER_END_DATE,'<',$endDate);

            });

        });

        return $checkOfferDateIsOverLappingQuery;
    }

    public function scopePendingResponseOnOfferByOwner($pendingResponseOnOfferByOwner,$ownerId){
        $pendingResponseOnOfferByOwner->where(PropertyOffers::OWNER_ID,$ownerId);
        $pendingResponseOnOfferByOwner->where(PropertyOffers::ACTION_BY,GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL)
            ->whereIn(PropertyOffers::STATUS,[
                GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING,
                GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_COUNTER_OFFER,
            ]);
    }

    public function scopePendingResponseOnOfferByTripper($pendingResponseOnOfferByTripper,$tripperId){
        $pendingResponseOnOfferByTripper->where(PropertyOffers::RENTAL_ID,$tripperId);
        $pendingResponseOnOfferByTripper->where(PropertyOffers::ACTION_BY,GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER)
            ->where(PropertyOffers::STATUS,[
                GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_ACCEPTED,
            ]);
    }

    ### End of Scope

    protected $dates = [
        self::TRIPPER_OFFER_START_DATE,
        self::TRIPPER_OFFER_END_DATE,
        self::OWNER_OFFER_START_DATE,
        self::OWNER_OFFER_END_DATE,
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT
    ];

}
