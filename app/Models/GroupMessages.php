<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMessages extends Model
{
     protected $table = 'group_messages';
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['tripboard_id','from_user','messages','is_read'];

    /**
     * A message belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rentalUser()
    {
        return $this->hasOne(RentalUser::class,'id','from_user');
    }
}
