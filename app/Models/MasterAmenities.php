<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterAmenities extends Model
{
    public const ID = 'id';
    public const NAME = 'amenity_name';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const TABLE_NAME = 'master_amenities';

    protected $table = self::TABLE_NAME;

    protected $guarded = ['id'];

    public function hotels()
    {
        return $this->belongsTo(Hotels::class);
    }
    public function propertyamenities()
    {
        return $this->hasMany(PropertyAmenities::class,'amenity_id');
    }
}
