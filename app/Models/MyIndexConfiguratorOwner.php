<?php

namespace App\Models;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class MyIndexConfiguratorOwner extends IndexConfigurator
{
    use Migratable;

    protected $name = 'ownerproperty';
    /**
     * @var array
     */
    protected $settings = [
        //
    ];
}
