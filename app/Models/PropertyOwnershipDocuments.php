<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyOwnershipDocuments extends Model
{
    //
    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }

}
