<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tripboards extends Model
{
    protected $table = 'tripboard';

     public function tripboardproperty()
    {
        return $this->hasMany(TripboardProperty::class,'tripboard_id');
    }

    public function properties()
    {
        return $this->belongsToMany(
            Property::class,
            'tripboard_property',
            'tripboard_id',
            'property_id'
        );
    }
}
