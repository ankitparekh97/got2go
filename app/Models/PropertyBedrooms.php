<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyBedrooms extends Model
{
    protected $table = 'property_bedrooms';

    protected $guarded = ['id'];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
