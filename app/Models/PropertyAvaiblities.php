<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAvaiblities extends Model
{
    //
    public function property()
    {
        return $this->belongsTo(Property::class,'property_id');
    }
}
