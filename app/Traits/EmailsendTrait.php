<?php
namespace App\Traits;
use Mail;
trait EmailsendTrait
{  
    
    /*Description Sending email form get email template form database*/
    public function send_email($body,$toMail,$toName,$subject,$from='no-reply@timeshare.com') {
        Mail::send([], [], function ($message) use ($toMail,$toName, $subject, $body,$from) {
          $message->to($toMail, $toName);
          $message->subject($subject);
          $message->setBody($body, 'text/html');
          $message->from($from, 'Timeshare');
        });
       
   }
}
