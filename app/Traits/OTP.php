<?php


namespace App\Traits;


use App\Libraries\SMS;
use App\Owner;
use App\RentalUser;
use Illuminate\Support\Facades\DB;

trait OTP
{
    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param Number $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $userDetail)
    {
        // verify user details
        $user = Owner::where('unique_key',$userDetail->unique_key)->first();

        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        $number = (env("TWILIO_TEST") == true ? "+919824833551" : $user->phone);

        // Send OTP to user's registered contact number
        SMS::sendTo($message,$number);

        // Registration email to user
        $toMail = $user->email;
        $toName = $user->name .' '. $user->last_name;
        $emailRecord = DB::table('email_template')->where('task','owner_registration')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body);
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{otp}', $user->otp_code, $body);
        $body = str_replace('{user_name}', $user->name .' '. $user->last_name, $body);
        $body = str_replace('{email}', $user->email , $body);

        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
    }

    private function sendTripperEmail($userDetail)
    {
        // verify user details
        $user = RentalUser::where('id',$userDetail->id)->first();

        // Registration email to user
        $toMail = $user->email;
        $toName = '';
        $emailRecord = DB::table('email_template')->where('task','tripper_registration')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body);
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', LIVE_SITE_LOGO , $body);
        $body = str_replace('{otp}', $user->otp_code, $body);
        $body = str_replace('{email}', $user->email , $body);

        $this->send_email($body,$toMail,$toName=null,$emailRecord->subject,$emailRecord->from_address);
    }
}
