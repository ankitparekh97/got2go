<?php
namespace App\Traits;
use Mail;
use App\OwnerRentalPair;
use App\Messages;
use Helper;
use Carbon\Carbon;
use App\Libraries\PusherFactory;
use Illuminate\Support\Facades\Auth;
trait SendMessageTrait
{  
    
    /*Description Sending email form get email template form database*/
    public function send($request,$messageArray,$guard) {
        //Save Messages
        $message = Messages::create($messageArray);
        OwnerRentalPair::where('id',$message->pair_id )->update(['updated_at'=>Carbon::now()]);

        if($guard == 'owner'){
            $to_user_type = 'rental';
        }else if($guard == 'rentaluser'){
            $to_user_type = 'owner';
        }
        $message->message_count = Messages::where(['is_read'=>'1','to_user'=>$message->to_user,'pair_id'=>$message->pair_id,'to_user_type'=>$to_user_type])->count();
        // prepare some data to send with the response
        $message->dateTimeStr = $message->created_at;
        $message->ownerUserName = $message->pairUser->ownerUser->first_name.' '.$message->pairUser->ownerUser->last_name;
        $message->from_user_id = Helper::urlsafe_b64encode(Auth::guard($guard)->user()->id);
        $message->rentalUserName = $message->pairUser->rentalUser->first_name.' '.$message->pairUser->rentalUser->last_name;
        $message->to_user_id = $request->to_user;
        $message->pair_user_id = $request->id;
        $message->ownerphoto = $message->pairUser->ownerUser->photo;
        $message->rentalphoto = $message->pairUser->rentalUser->photo;

        //dd($message->toarray());

        PusherFactory::make()->trigger('chat', 'send', ['data' => $message]);

        return response()->json(['state' => 1, 'data' => $message]);
       
   }
}