<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCards extends Model
{
    protected $table = 'payment_cards';
    protected $fillable = ['user_type','user_id','name_on_card','email','card_type','last_four',
                            'expiration_date','cvv','address','address2','state','city','phone'];

    public function rental_user()
    {
        return $this->belongsTo(RentalUser::class,'user_id');
    }
}
