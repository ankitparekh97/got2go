<?php


namespace App\Repositories;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PaymentCardDTO;
use App\Models\PaymentCards;
use DB;

trait PaymentCardRepository
{

    public function cardDetails($userType,$rentalUserId){

        /**
         * @var PaymentCards $cardDetails
         */

        $cardDetails = (new $this->paymentCards)::query();

        $cardDetails->when($userType, function ($query) use($userType){
            $query->where('user_type',$userType);
        });

        $cardDetails->when($rentalUserId, function ($query) use($rentalUserId){
            $query->where('user_id',$rentalUserId);
        });

        $cardDetails->orderBy(PaymentCards::ID,'DESC');
        if($userType == GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL){
            $cardDetails->with(['rental_user']);
        }
        else if($userType == GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER){
            $cardDetails->with(['owner']);
        }

        $cardDetail = $cardDetails->first();

        return $cardDetail;
    }

    /**
     * @param PaymentCardDTO $paymentCardDTO
     * @param $userId
     * @return mixed
     * @throws \Throwable
     */
    public function savePaymentCard(
        PaymentCardDTO $paymentCardDTO,
        $userId
    )
    {
        /**
         * @var PaymentCards $paymentCard
         */
        $paymentCard = (new $this->paymentCards);
        return DB::transaction(function () use(&$paymentCard,&$paymentCardDTO,&$userId){
            $paymentCard->user_type = $paymentCardDTO->userType ?? $paymentCard->user_type;
            $paymentCard->user_id = $userId;
            $paymentCard->name_on_card = $paymentCardDTO->nameOnCard ?? $paymentCard->name_on_card;
            $paymentCard->email = $paymentCardDTO->email ?? $paymentCard->email;
            $paymentCard->card_type = $paymentCardDTO->cardType ?? $paymentCard->card_type;
            $paymentCard->last_four = $paymentCardDTO->lastFour ?? $paymentCard->last_four;
            $paymentCard->expiration_date = $paymentCardDTO->expirationDate ?? $paymentCard->expiration_date;
            $paymentCard->address = $paymentCardDTO->address ?? $paymentCard->address;
            $paymentCard->address2 = $paymentCardDTO->address2 ?? $paymentCard->address2;
            $paymentCard->state = $paymentCardDTO->state ?? $paymentCard->state;
            $paymentCard->city = $paymentCardDTO->city ?? $paymentCard->city;
            $paymentCard->phone = $paymentCardDTO->billingPhone ?? $paymentCard->phone;
            $paymentCard->debit = $paymentCardDTO->debit ?? $paymentCard->debit;
            $paymentCard->default = $paymentCardDTO->default ?? $paymentCard->default;
            $paymentCard->image_url = $paymentCardDTO->imageUrl ?? $paymentCard->image_url;
            $paymentCard->masked_number = $paymentCardDTO->maskedNumber ?? $paymentCard->masked_number;
            $paymentCard->token = $paymentCardDTO->token ?? $paymentCard->token;
            $paymentCard->unique_identifier = $paymentCardDTO->uniqueIdentifier ?? $paymentCard->unique_identifier;
            $paymentCard->card_token = $paymentCardDTO->cardToken ?? $paymentCard->card_token;
            $paymentCard->save();

            return $paymentCard;
        });

    }

    public function paymentMethodObject($params){
        /**
        * @var PaymentCards $paymentCardsModel
        */

        $paymentCardsModel = (new $this->paymentCards)::query();
        $paymentCardsModel->where($params);

       return $paymentCardsModel->first();
   }

    public function paymentMethodCredit($params){
        /**
        * @var PaymentCards $paymentCardsModel
        */

        $paymentCardsModel = (new $this->paymentCards)::query();
        $paymentCardsModel->where($params);

        $paymentCardsModel->orderBy(PaymentCards::ID, GlobalConstantDeclaration::ORDER_BY_DESC);

        return $paymentCardsModel->get();
   }

   public function saveCard(PaymentCardDTO $paymentCardDTO){
        /**
        * @var PaymentCards $paymentCardsModel
        */

        $paymentCardsModel = (new $this->paymentCards);

        if($paymentCardDTO->id){
            $paymentCardsModel = $this->paymentMethodObject(array('id'=>$paymentCardDTO->id));
        }

        $paymentCardsModel->user_id = !empty($paymentCardDTO->userId) ? $paymentCardDTO->userId : $paymentCardsModel->user_id;
        $paymentCardsModel->user_type = isset($paymentCardDTO->userType) ? $paymentCardDTO->userType : $paymentCardsModel->user_type;
        $paymentCardsModel->name_on_card = !empty($paymentCardDTO->nameOnCard) ? $paymentCardDTO->nameOnCard : $paymentCardsModel->name_on_card;
        $paymentCardsModel->email = !empty($paymentCardDTO->email) ? $paymentCardDTO->email : $paymentCardsModel->email;
        $paymentCardsModel->card_type = !empty($paymentCardDTO->cardType) ? $paymentCardDTO->cardType : $paymentCardsModel->card_type;
        $paymentCardsModel->last_four = !empty($paymentCardDTO->lastFour) ? $paymentCardDTO->lastFour : $paymentCardsModel->last_four;
        $paymentCardsModel->masked_number = !empty($paymentCardDTO->maskedNumber) ? $paymentCardDTO->maskedNumber : $paymentCardsModel->masked_number;
        $paymentCardsModel->expiration_date = isset($paymentCardDTO->expirationDate) ? $paymentCardDTO->expirationDate : $paymentCardsModel->expiration_date;
        $paymentCardsModel->cvv = isset($paymentCardDTO->cvv) ? $paymentCardDTO->cvv : $paymentCardsModel->cvv;
        $paymentCardsModel->default = isset($paymentCardDTO->default) ? $paymentCardDTO->default : $paymentCardsModel->default;
        $paymentCardsModel->address = isset($paymentCardDTO->address) ? $paymentCardDTO->address : $paymentCardsModel->address;
        $paymentCardsModel->address2 = isset($paymentCardDTO->address2) ? $paymentCardDTO->address2 : $paymentCardsModel->address2;
        $paymentCardsModel->state = isset($paymentCardDTO->state) ? $paymentCardDTO->state : $paymentCardsModel->state;
        $paymentCardsModel->city = isset($paymentCardDTO->city) ? $paymentCardDTO->city : $paymentCardsModel->city;
        $paymentCardsModel->phone = isset($paymentCardDTO->billingPhone) ? $paymentCardDTO->billingPhone : $paymentCardsModel->phone;
        $paymentCardsModel->token = isset($paymentCardDTO->token) ? $paymentCardDTO->token : $paymentCardsModel->token;
        $paymentCardsModel->card_token = isset($paymentCardDTO->cardToken) ? $paymentCardDTO->cardToken : $paymentCardsModel->card_token;
        $paymentCardsModel->fingerprint = isset($paymentCardDTO->fingerPrint) ? $paymentCardDTO->fingerPrint : $paymentCardsModel->fingerprint;

        $paymentCardsModel->save();
        return $paymentCardsModel->id;
   }

   public function deletePaymentCard($id){
        /**
        * @var PaymentCards $paymentCardsModel
        */

        $paymentCardsModel = $this->paymentMethodObject(array('id' => $id));
        $paymentCardsModel->delete();

        return $paymentCardsModel;
  }
    public function getPaymentCardDetails($userId){
        return  PaymentCards::with('rental_user')
                                ->where(['user_id'=>$userId,'user_type'=>'rental'])
                                ->get();
    }

    public function getPaymentCardDetailById($id){
        return  PaymentCards::find($id);
    }
}
