<?php


namespace App\Repositories;



interface OwnerRentalPairInterface
{
    public function saveOwnerRentalPair($ownerId,$rentalId,$type);

    public function saveAdminRentalPair($adminId,$rentalId,$type);

    public function saveAdminOwnerPair($adminId,$ownerId,$type);

    public function updateOwnerRentalPair($pairId);

    public function getData($request);

    public function update($params);

    public function getOwnerRentalPairByParamsObject($params);
    public function fetchRentalChatContact($name);
    public function fetchOwnerChatContact($name);
}
