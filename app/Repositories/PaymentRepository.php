<?php


namespace App\Repositories;


use App\Http\DTO\Api\PaymentCardDTO;
use App\Models\RentalUser;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionParamsDTO;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;

trait PaymentRepository
{
    public function createPaymentGatewayCustomer($userId) {

        /**
         * @var RentalUser $rentalUser
         */
        $rentalUser = $this->getById($userId);

        if(!$rentalUser->customer_id){

            $stripeCreateCustomerDTO = convertRentalModelInToStripeCreateCustomerDTO($rentalUser);

            /**
             * @var CustomerResponseDTO $createdCustomer
             */
            $createdCustomer =  $this->paymentService->createCustomer($stripeCreateCustomerDTO);

            $rentalUser = $this->savePaymentCustomerId($createdCustomer['id'],$userId);
        }

        return $rentalUser;
    }

    public function createPaymentGatewaySubscription($userId,$planId) {

        /**
         * @var RentalUser $userModel
         */
        $userModel = $this->createPaymentGatewayCustomer($userId);

        $subscriptionParams = new SubscriptionParamsDTO;
        $subscriptionParams->plan = $planId;

        $subscriptionData = $this->paymentService->createSubscription($userModel->customer_id,$subscriptionParams);

        return $subscriptionData;
    }

    public function cancelPaymentGatewaySubscription($userId){
        /**
         * @var RentalUser $userModel
         */
        $userModel = $this->getById($userId);
        $userModel->load('subscription');

        /**
         * @var SubscriptionResponseDTO $cancelSubscription
         */
        $cancelSubscription = $this->paymentService->cancelSubscription($userModel->customer_id,$userModel->subscription->subscription_id);

        // Making user to non tripper
        $this->tripperToNormalRental($userId);

        $userModel->subscription->delete();

        return $cancelSubscription;
    }

    public function pausePaymentGatewaySubscription($userId){
        /**
         * @var RentalUser $userModel
         */
        $userModel = $this->getById($userId);
        $userModel->load('subscription');

        /**
         * @var SubscriptionResponseDTO $cancelSubscription
         */
        $cancelSubscription = $this->paymentService->pauseSubscription($userModel->customer_id,$userModel->subscription->subscription_id);

        $userModel->subscription()->update([
            'status' => 'pause'
        ]);

        return $cancelSubscription;
    }

    public function activePaymentGatewaySubscription($userId){
        /**
         * @var RentalUser $userModel
         */
        $userModel = $this->getById($userId);
        $userModel->load('subscription');

        /**
         * @var SubscriptionResponseDTO $cancelSubscription
         */
        $cancelSubscription = $this->paymentService->reactiveSubscription($userModel->customer_id,$userModel->subscription->subscription_id);

        $userModel->subscription()->update([
            'status' => 'active'
        ]);

        return $cancelSubscription;
    }

    public function createPaymentGatewayCard($userId,PaymentCardDTO $paymentCardDTO) {

        /**
         * @var RentalUser $userModel
         */
        $userModel = $this->getById($userId);

        $cardRequestDTO = new CardRequestDTO;
        $cardRequestDTO->token = $paymentCardDTO->token;
        $cardRequestDTO->customerId = $userModel->customer_id;

        $createdCard = $this->paymentService->createCard($cardRequestDTO);

        return $createdCard;
    }

}
