<?php


namespace App\Repositories;


use App\Models\Admin;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

class AdminRepository implements AdminInterface
{


    public function __construct(
        Admin  $admin
    )
    {
        $this->admin = $admin;
    }


    /**
     * @param $email
     * @param $password
     * @return Admin|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function updateUserPassword($email,$password)
    {
        /**
         * @var Admin $admin
         */
        $admin = (new $this->admin())::query();
        $admin->where('email',$email)->first();
        $admin->password = $password;
        $admin->save();
        return $admin;
    }


}
