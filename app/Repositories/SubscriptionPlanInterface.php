<?php


namespace App\Repositories;


use App\Http\DTO\Api\SubscriptionPlanDTO;

interface SubscriptionPlanInterface
{
    public function createSubscriptionPlan(SubscriptionPlanDTO $subscriptionPlanDTO);

    public function getLastInsertedSubscriptionPlan();
}
