<?php


namespace App\Repositories;

use App\Models\Configuration;
use DB;
use Illuminate\Support\Carbon;

class ConfigurationRepository implements ConfigurationInterface
{

    public $configurationModel;

    public function __construct(
        Configuration $configurationModel
    )
    {
        $this->configurationModel = $configurationModel;
    }

    public function getConfiguration()
    {
        return Configuration::first()->service_fee;
    }

    public function serviceFeeSave($servicefee,$serviceId){
        $configuration = Configuration::find($serviceId);
        if ($configuration) {
            $configuration->service_fee = $servicefee;
        } else {
            $configuration = new Configuration([
                'service_fee' => $servicefee,
                'created_at' => now(),
            ]);
        }
        $configuration->save();
    }
}
