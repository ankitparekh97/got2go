<?php


namespace App\Repositories;


use App\Models\TripboardVote;
use Carbon\Carbon;
use DB;

class TripBoardVoteRepository implements TripBoardVoteInterface
{
    public function voteTripboard($param)
    {
        $message = "Unvoted to tripboard.";
        if(TripboardVote::where($param)->exists()){
            TripboardVote::where($param)->delete();
        }else{
            $message = "Vote Added to tripboard.";
            TripboardVote::insert([
                'tripboard_id' => $param['tripboard_id'],
                'property_id' => $param['property_id'],
                'votes' => 1,
                'user_id'=>$param['user_id'],
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);
        }

        $tripboardVote = TripboardVote::where('tripboard_id',$param['tripboard_id'])
                ->where('property_id',$param['property_id'])
                ->where('user_id',$param['user_id'])
                ->select('votes','property_id')
                ->groupBy(['property_id','tripboard_id'])
                ->first();
        $totalVote = TripboardVote::where('tripboard_id',$param['tripboard_id'])
                ->where('property_id',$param['property_id'])
                ->select(DB::raw("IFNULL(sum(votes),0)  as totalVotes"))
                ->groupBy(['property_id','tripboard_id'])
                ->first();
                if(empty($totalVote)){
                    $totalVote = (object)[];  
                    $totalVote->totalVotes = 0;
                }

        return ['success'=>true,'message'=>$message,'voteData'=>$tripboardVote,'totalVote' => $totalVote];
    }

    public function deleteTripBoardPropertyVotes($tripboardId, $propertyId){
        TripboardVote::where('tripboard_id', $tripboardId)
        ->where('property_id', $propertyId)
        ->delete();
    }
}
