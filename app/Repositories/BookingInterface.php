<?php


namespace App\Repositories;

use App\Models\Booking;
use App\Http\DTO\Api\BookingDTO;

interface BookingInterface
{
    public function checkBookingByUser($userId,$propertyId);

    public function getBookingById($Id);

    public function getBookingsByOwnerId($ownerId,$status);

    public function getByParamsObject($params);

    public function getByParamsCollection($params);

    public function checkExistingBookings($propertyId,$bookingId,$checkInDate,$checkoutDate);

    public function updateBookingStatus(BookingDTO $bookingDTO);

    public function createBooking($bookingDetails);
    public function changeBookingStatus($bookingId, $status);
    public function checkBookings($id,$checkInDate,$checkOutDate);

    public function getUpcomingStays($userId);
    public function getPastStays($userId);
    public function getBookingDetailByPropertyId($propertyId, $bookingId);

}
