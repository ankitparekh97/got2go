<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\Hotels;
use App\Http\DTO\Api\HotelsDTO;

class HotelsRepository implements HotelsInterface
{
    public $hotelsModel;

    public function __construct(
        Hotels $hotelsModel
    )
    {
        $this->hotelsModel = $hotelsModel;
    }

    public function getHotels(){

         /**
         * @var Hotels $hotels
         */

        $getHotels = (new $this->hotelsModel)::query();
        $hotels = $getHotels->get();

        return $hotels;
    }

    public function getHotelsByParamsCollection($params)
    {
         /**
         * @var Hotels $hotels
         */

        $hotels = (new $this->hotelsModel)::query();
        $hotels->where($params);

        return $hotels->get();
    }

    public function getHotelsByParamsObject($params)
    {
         /**
         * @var Hotels $hotels
         */

        $hotels = (new $this->hotelsModel)::query();
        $hotels->where($params);

        return $hotels->first();
    }

    public function saveHotel(HotelsDTO $hotelsDTO)
    {
        /**
         * @var Hotels|Builder $hotels
         */

        $hotels = (new $this->hotelsModel);
        if($hotelsDTO->id){
            $hotels = $this->getHotelsByParamsObject(array('id'=>$hotelsDTO->id));
        }

        $hotels->name = !empty($hotelsDTO->name) ? $hotelsDTO->name : $hotels->name;
        $hotels->email = !empty($hotelsDTO->email) ? $hotelsDTO->email : $hotels->email;
        $hotels->password = !empty($hotelsDTO->password) ? $hotelsDTO->password : $hotels->password;
        $hotels->location = !empty($hotelsDTO->location) ? $hotelsDTO->location : $hotels->location;
        $hotels->city = !empty($hotelsDTO->city) ? $hotelsDTO->city : $hotels->city;
        $hotels->state = !empty($hotelsDTO->state) ? $hotelsDTO->state : $hotels->state;
        $hotels->save();

        return $hotels;
    }
}
