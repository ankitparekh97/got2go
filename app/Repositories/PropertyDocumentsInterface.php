<?php

namespace App\Repositories;

use App\Http\DTO\Api\PropertyVerificationDTO;

interface PropertyDocumentsInterface
{
    public function getPropertyDocumentsByParamsCollection($params);

    public function getPropertyDocumentsByParamsObject($params);

    public function savePropertyDocuments($params);

    public function deletePropertyDocuments($params);
}
