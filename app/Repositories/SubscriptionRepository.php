<?php


namespace App\Repositories;

use App\Models\Subscription;
use DB;
use Illuminate\Support\Carbon;

class SubscriptionRepository implements SubscriptionInterface
{

    public $subscriptionModel;

    public function __construct(
        Subscription $subscriptionModel
    )
    {
        $this->subscriptionModel = $subscriptionModel;
    }

    public function getSubscription()
    {
        return Subscription::first();
    }

    public function getSubscriptionById($subscriptionId)
    {
        return Subscription::find($subscriptionId);
    }

    /**
     * @param $subscriptionId
     * @param $discountPercentage
     * @param $strikeThroughAmount
     * @return Subscription
     * @throws \Throwable
     */
    public function updateSubscriptionDetails($subscriptionId,$annualSubscriptionAmount,$discountPercentage,$strikeThroughAmount)
    {
        /**
         * @var Subscription $subscription
         */
        $subscription = $this->getSubscriptionById($subscriptionId);
        $subscription->annualSubscriptionAmount = $annualSubscriptionAmount;
        $subscription->discount_percentage = $discountPercentage;
        $subscription->strike_through_amount = $strikeThroughAmount;
        $subscription->save();
        return $subscription;
    }
}
