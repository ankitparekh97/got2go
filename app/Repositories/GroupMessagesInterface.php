<?php


namespace App\Repositories;

use App\Models\GroupMessages;

interface GroupMessagesInterface
{
    public function saveMessages($messageArray);

    public function getMessages($tripboardId);
}
