<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\Messages;
use Carbon\Carbon;
use DB;

class MessagesRepository implements MessagesInterface
{

    public $messagesModel;

    public function __construct(
        Messages $messagesModel
    )
    {
        $this->messagesModel = $messagesModel;
    }

    public function getByParamsCollection($params)
    {
        $messages = (new $this->messagesModel)::query();
        $messages->where($params);

        $messages->with(['fromUser','toUser','pairUser']);
        return $messages->get();
    }

    public function getByParamsObject($params)
    {
        $messages = (new $this->messagesModel)::query();
        $messages->where($params);

        $messages->with(['fromUser','toUser','pairUser']);
        return $messages->first();
    }

    public function getMessages($userId){

        $messages = (new $this->messagesModel)::query();

        $messages->where('pair_id',$userId);
        $messages->orderBy('created_at',GlobalConstantDeclaration::ORDER_BY_DESC);
        $messages->with(['pairUser','pairUser.ownerUser','pairUser.rentalUser','pairUser.adminUser']);

        $messages = $messages->get();
        return $messages;
    }

    public function create($params){

        $messages = (new $this->messagesModel);
        return $messages::create($params);
    }

    /**
     * @param $ownerId
     * @param $rentalId
     * @param $type
     * @return mixed
     */
    public function saveMessages($messageArray)
    {
        return Messages::create($messageArray);
    }

    /**
     * @param $pairId
     * @return mixed
     */
    public function getMessageCount($message,$toUserType){
        return Messages::where(['is_read'=>'1','to_user'=>$message->to_user,'pair_id'=>$message->pair_id,'to_user_type'=>$toUserType])->count();
    }

    /**
     * @param $pairId
     * @return mixed
     */
    public function fetchMessages($pairId){
        return Messages::with(['pairUser','pairUser.ownerUser','pairUser.rentalUser','pairUser.adminUser'])->where('pair_id',$pairId)->orderBy('created_at', 'ASC')->get();
    }

    /**
     * @param $pairId
     * @param $toUserId
     * @return mixed
     */
    public function unreadFlag($pairId,$toUserId){
        return Messages::where(['pair_id'=>$pairId,'to_user'=>$toUserId])->update(['is_read'=>'0']);
    }
}
