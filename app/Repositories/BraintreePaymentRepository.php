<?php


namespace App\Repositories;


use App\Http\DTO\Api\PaymentCardDTO;
use App\Http\DTO\Api\RentalUserDTO;
use App\Http\DTO\Api\RentalUserSubscriptionDTO;
use App\Models\RentalUserSubscription;
use App\Models\PaymentCards;
use App\Models\RentalUser;
use App\Models\Owner;
use App\Services\PaymentServices\Braintree\BraintreePaymentService;
use App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionResponseDTO;

trait BraintreePaymentRepository
{
    public function createBraintreeCustomer(
        $userId
    ) {

        $user = $this->getById($userId);

        /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();

        if(!$user->braintree_customer_id) {
            $customerDto = new CustomerRequestDTO();
            $customerDto->firstName = $user->first_name;
            $customerDto->lastName = $user->last_name;
            $customerDto->email = $user->email;
            $customerDto->phone = $user->phone;

            /**
             * @var CustomerResponseDTO $customer
             */
            $customer = $braintreePaymentService->createCustomer($customerDto);

            $user->braintree_customer_id = $customer->id;
            $user->save();

            return $customer;
        } else {
             return $braintreePaymentService->findCustomer($user->braintree_customer_id);
        }

    }

    public function createBraintreePaymentMethod(
        PaymentMethodRequestDTO $paymentMethodRequestDTO,
        $userId
    )
    {
        /**
         * @var RentalUser $user
         */
        $user = $this->getById($userId);



        /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();

        /**
         * @var CreditCardResponseDTO $paymentMethod
         */
        $paymentMethod = $braintreePaymentService->createPaymentMethod($paymentMethodRequestDTO);

        $paymentCardSaveDb = new PaymentCardDTO();
        $paymentCardSaveDb->userType = ($user->is_tripper == 1) ?'tripper':'rental';
        $paymentCardSaveDb->nameOnCard = $paymentMethod->cardholderName;
        $paymentCardSaveDb->email = $user->email;
        $paymentCardSaveDb->billingPhone = $user->phone;
        $paymentCardSaveDb->cardType = $paymentMethod->cardType;
        $paymentCardSaveDb->lastFour = $paymentMethod->last4;
        $paymentCardSaveDb->expirationDate = $paymentMethod->expirationDate;
        $paymentCardSaveDb->debit = $paymentMethod->debit;
        $paymentCardSaveDb->default = $paymentMethod->default;
        $paymentCardSaveDb->imageUrl = $paymentMethod->imageUrl;
        $paymentCardSaveDb->maskedNumber = $paymentMethod->maskedNumber;
        $paymentCardSaveDb->token = $paymentMethod->token;
        $paymentCardSaveDb->uniqueIdentifier = $paymentMethod->uniqueNumberIdentifier;
        if($paymentMethod->billingAddress) {
            $paymentCardSaveDb->address = $paymentMethod->billingAddress->streetAddress;
            $paymentCardSaveDb->address2 = $paymentMethod->billingAddress->extendedAddress;
            $paymentCardSaveDb->state = $paymentMethod->billingAddress->region;
            $paymentCardSaveDb->city = $paymentMethod->billingAddress->locality;
        }

        /**
         * @var PaymentCards $paymentCardDetails
         */
        $this->savePaymentCard($paymentCardSaveDb,$userId);

        return $paymentMethod;
    }

    public function createBraintreeSubscription(SubscriptionRequestDTO $subscriptionRequestDTO,$userId)
    {
        /**
         * @var RentalUser $user
         */
        $user = $this->getById($userId);

        /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();

        /**
         * @var SubscriptionResponseDTO $subscription
         */
        $subscription = $braintreePaymentService->createSubscription($subscriptionRequestDTO);

        $user->is_subscribed = self::TRIPPER_STATUS_PAUSED;
        if($subscription->status === 'Active') {
            $user->is_subscribed = self::TRIPPER_STATUS_ACTIVE;
        }
        $user->save();

        $this->makeAndSaveRentalSubscription($subscription,$userId);

        return $subscription;
    }

    public function cancelBraintreeSubscription($userId)
    {
        /**
         * @var RentalUser $user
         */
        $user = $this->getById($userId);

        if($user->subscription->subscription_id) {
            /**
             * @var BraintreePaymentService $braintreePaymentService
             */
            $braintreePaymentService = getObjectBraintree();

            $subscription = $braintreePaymentService->cancelSubscription($user->subscription->subscription_id);

            /**
             * @var RentalUserSubscription $rentalUserSubscription
             */

            if($subscription !== null) {
                $rentalUserSubscription = $this->makeAndSaveRentalSubscription($subscription, $userId);
                $this->deleteRentalUserSubscription($rentalUserSubscription->id);
            }
        }

        $rentalUserDto = new RentalUserDTO();
        //$rentalUserDto->tripperStatus = self::TRIPPER_STATUS_CANCELED;
        $rentalUserDto->isTripper = false;
        $rentalUserDto->isSubscribed = false;
        $this->saveRenterUser($rentalUserDto,$userId);
    }

    public function makeAndSaveRentalSubscription(
        SubscriptionResponseDTO $subscription,
        $userId
    )
    {
        $rentalUserSubscriptionDTO = new RentalUserSubscriptionDTO();
        $rentalUserSubscriptionDTO->rentalId = $userId;
        $rentalUserSubscriptionDTO->billingPeriodStartDate = $subscription->billingPeriodStartDate;
        $rentalUserSubscriptionDTO->billingPeriodEndDate = $subscription->billingPeriodEndDate;
        $rentalUserSubscriptionDTO->firstBillingDate = $subscription->firstBillingDate;
        $rentalUserSubscriptionDTO->currentBillingCycle = $subscription->currentBillingCycle;
        $rentalUserSubscriptionDTO->description = $subscription->description;
        $rentalUserSubscriptionDTO->subscriptionId = $subscription->id;
        $rentalUserSubscriptionDTO->neverExpires = $subscription->neverExpires;
        $rentalUserSubscriptionDTO->nextBillingDate = $subscription->nextBillingDate;
        $rentalUserSubscriptionDTO->nextBillingAmount = $subscription->nextBillAmount;
        $rentalUserSubscriptionDTO->nextBillingPeriodAmount = $subscription->nextBillingPeriodAmount;
        $rentalUserSubscriptionDTO->numberOfBillingCycles = $subscription->numberOfBillingCycles;
        $rentalUserSubscriptionDTO->paidThroughDate = $subscription->paidThroughDate;
        $rentalUserSubscriptionDTO->paymentMethodToken = $subscription->paymentMethodToken;
        $rentalUserSubscriptionDTO->planId = $subscription->planId;
        $rentalUserSubscriptionDTO->status = $subscription->status;
        $rentalUserSubscriptionDTO->failureCount = $subscription->failureCount;
        $rentalUserSubscriptionDTO->transactionId = $subscription->transactions[0]->id;

        return $this->saveRentalUserSubscription($rentalUserSubscriptionDTO,$userId);
    }


    public function createBraintreeCustomerOwner(
        $userId
    ) {

        $user = $this->getOwnerById($userId);


        /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();

        if(!$user->braintree_customer_id) {
            $customerDto = new CustomerRequestDTO();
            $customerDto->firstName = $user->first_name;
            $customerDto->lastName = $user->last_name;
            $customerDto->email = $user->email;
            $customerDto->phone = $user->phone;

            /**
             * @var CustomerResponseDTO $customer
             */
            $customer = $braintreePaymentService->createCustomer($customerDto);

            $user->braintree_customer_id = $customer->id;
            $user->save();

            return $customer;
        } else {
             return $braintreePaymentService->findCustomer($user->braintree_customer_id);
        }

    }

    public function createBraintreePaymentMethodOwner(
        PaymentMethodRequestDTO $paymentMethodRequestDTO,
        $userId
    )
    {
        /**
         * @var Owner $user
         */
        $user = $this->getOwnerById($userId);

        /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();

        /**
         * @var CreditCardResponseDTO $paymentMethod
         */
        $paymentMethod = $braintreePaymentService->createPaymentMethod($paymentMethodRequestDTO);
        $paymentCardSaveDb = new PaymentCardDTO();
        $paymentCardSaveDb->userType = 'owner';
        $paymentCardSaveDb->nameOnCard = $paymentMethod->cardholderName;
        $paymentCardSaveDb->email = $user->email;
        $paymentCardSaveDb->billingPhone = $user->phone;
        $paymentCardSaveDb->cardType = $paymentMethod->cardType;
        $paymentCardSaveDb->lastFour = $paymentMethod->last4;
        $paymentCardSaveDb->expirationDate = $paymentMethod->expirationDate;
        $paymentCardSaveDb->debit = $paymentMethod->debit;
        $paymentCardSaveDb->default = $paymentMethod->default;
        $paymentCardSaveDb->imageUrl = $paymentMethod->imageUrl;
        $paymentCardSaveDb->maskedNumber = $paymentMethod->maskedNumber;
        $paymentCardSaveDb->token = $paymentMethod->token;
        $paymentCardSaveDb->uniqueIdentifier = $paymentMethod->uniqueNumberIdentifier;
        if($paymentMethod->billingAddress) {
            $paymentCardSaveDb->address = $paymentMethod->billingAddress->streetAddress;
            $paymentCardSaveDb->address2 = $paymentMethod->billingAddress->extendedAddress;
            $paymentCardSaveDb->state = $paymentMethod->billingAddress->region;
            $paymentCardSaveDb->city = $paymentMethod->billingAddress->locality;
        }
        /**
         * @var PaymentCards $paymentCardDetails
         */
        $this->savePaymentCard($paymentCardSaveDb,$userId);

        return $paymentMethod;
    }

    public function deleteBraintreePaymentMethodOwner($token)
    {
       /**
         * @var BraintreePaymentService $braintreePaymentService
         */
        $braintreePaymentService = getObjectBraintree();
        /**
         * @var CreditCardResponseDTO $paymentMethod
         */
        return $braintreePaymentService->deletePaymentMethod($token);
    }

}
