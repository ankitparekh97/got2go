<?php


namespace App\Repositories;

use App\Models\Booking;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\BookingDTO;
use DB;
use Illuminate\Support\Carbon;

class BookingRepository implements BookingInterface
{

    public $bookingModel;

    public function __construct(
        Booking $bookingModel
    )
    {
        $this->bookingModel = $bookingModel;
    }

    /**
     * @param $propertyId
     * @return mixed
     */
    public function checkBookingByUser($userId,$propertyId)
    {
        $now = Carbon::now()->toDateString();
        return Booking::where(['property_id'=>$propertyId,'rental_user_id'=>$userId])
                                ->whereDate('check_out_date', '>=',date('Y-m-d',strtotime($now)))
                                ->whereIn('owner_status', array('confirmed', 'approved', 'instant_booking'))
                                ->get();
    }

    /**
     * @param $Id
     * @return mixed
     */
    public function getBookingById($Id)
    {
        $booking = (new $this->bookingModel)::query();
        $booking->where(Booking::ID,$Id);

        $booking->with(['property','rentaluser','bookingPayment']);
        return $booking->first();
    }

    public function getBookingsByOwnerId($ownerId,$status){

        /**
         * @var Booking $bookings
         */

        $select = [
            \DB::raw("(CASE WHEN owner_status = 'pending' or
                        ((owner_status = 'approved' or owner_status = 'instant_booking') and booking_type = 'vacation_rental') THEN
                            1
                        ELSE
                            0
                        END) as bookingStatus"),
            \DB::raw("booking.*"),
            \DB::raw("owner_rental_pair.id as messageid")
        ];

        $bookings = (new $this->bookingModel)::query();

        $bookings->select($select, '*');
        $bookings->whereHas('property', function ($query) use ($ownerId) {
            $query->where(['owner_id' => $ownerId]);
        });

        $bookings ->leftjoin('owner_rental_pair', function($query) use ($ownerId){
            $query->on('owner_rental_pair.rental_id', '=','booking.rental_user_id')
                  ->where('owner_rental_pair.owner_id',$ownerId);
        });

        $bookings->whereIn('owner_status',$status);

        $bookings->orderBy('bookingStatus', GlobalConstantDeclaration::ORDER_BY_DESC);
        $bookings->orderBy(Booking::CREATED_AT, GlobalConstantDeclaration::ORDER_BY_DESC);
        $bookings->with(['property','rentaluser','bookingPayment']);

        $propertiesbookings = $bookings->get();
        return $propertiesbookings;
    }

    public function getByParamsCollection($params)
    {
        $bookingModel = (new $this->bookingModel)::query();
        $bookingModel->where($params);

        $bookingModel->with(['property','rentaluser','bookingPayment']);
        return $bookingModel->get();
    }

    public function getByParamsObject($params)
    {
        $bookingModel = (new $this->bookingModel)::query();
        $bookingModel->where($params);

        $bookingModel->with(['property','rentaluser','bookingPayment']);
        return $bookingModel->first();
    }

    public function checkExistingBookings($propertyId,$bookingId,$checkInDate,$checkoutDate){

        /**
         * @var Booking $bookings
         */

        $bookings = (new $this->bookingModel)::query();

        $bookings->where(function($query) use($checkInDate,$checkoutDate){
            $query->where(function($q) use($checkInDate,$checkoutDate){
                  $q->whereDate('check_in_date', '>',date('H:i:s',strtotime($checkInDate)))
                        ->whereDate('check_in_date', '<',date('H:i:s',strtotime($checkoutDate)));
                    })
                    ->orwhere(function($q) use($checkInDate,$checkoutDate){
                        $q->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkInDate)))
                            ->whereDate('check_out_date', '<',date('Y-m-d',strtotime($checkoutDate)));
                        })
                    ->orwhere(function($q) use($checkInDate,$checkoutDate){
                        $q->whereDate('check_in_date', '<',date('Y-m-d',strtotime($checkInDate)))
                            ->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkInDate)));
                    })
                    ->orwhere(function($q) use($checkInDate,$checkoutDate){
                        $q->whereDate('check_in_date', '<',date('Y-m-d',strtotime($checkoutDate)))
                            ->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkoutDate)));
                    })
                    ->orwhere(function($q) use($checkInDate,$checkoutDate){
                        $q->whereDate('check_in_date', '=',date('Y-m-d',strtotime($checkInDate)))
                            ->whereDate('check_out_date', '=',date('Y-m-d',strtotime($checkoutDate)));
                    });
            });

        $bookings->where('property_id',$propertyId);
        $bookings->where('id', '!=' , $bookingId);
        $bookings->where('owner_status', GlobalConstantDeclaration::PROPERTY_STATUS_PENDING);

        $propertiesbookings = $bookings->get();
        return $propertiesbookings;
    }

    public function updateBookingStatus(BookingDTO $bookingDTO){

        $booking = (new $this->bookingModel);
        if($bookingDTO->id){
            $booking = $this->getBookingById($bookingDTO->id);
        }

        $booking->owner_status = !empty($bookingDTO->ownerStatus) ? $bookingDTO->ownerStatus : $booking->owner_status;
        $booking->is_cancelled = isset($bookingDTO->isCancelled) ? $bookingDTO->status : $booking->is_cancelled;
        $booking->cancellation_date = !empty($bookingDTO->cancelledDate) ? $bookingDTO->cancelledDate : $booking->cancellation_date;
        $booking->confirmation_id = !empty($bookingDTO->confirmationId) ? $bookingDTO->confirmationId : $booking->confirmation_id;
        $booking->created_at = !empty($bookingDTO->createdAt) ? $bookingDTO->createdAt : $booking->created_at;
        $booking->updated_at = !empty($bookingDTO->updatedDate) ? $bookingDTO->updatedDate : $booking->updated_at;
        $booking->save();

        return $booking;
    }

    /** @param $bookingDetails
     * @return mixed
     */

    public function createBooking($bookingDetails)
    {
        return Booking::create([
            'booking_type' => $bookingDetails->bookingType,
            'rental_user_id' => $bookingDetails->rentalUserId,
            'property_id' => $bookingDetails->propertyId,
            'check_in_date' => $bookingDetails->checkInDate,
            'check_out_date' => $bookingDetails->checkOutDate,
            'adults' => $bookingDetails->adults,
            'childrens' => $bookingDetails->childrens,
            'rooms' => $bookingDetails->rooms,
            'actual_price' =>$bookingDetails->actualPrice,
            'offer_amount' => $bookingDetails->offerAmount,
            'service_fee' => $bookingDetails->serviceFee,
            'occupancy_tax_fees' => $bookingDetails->occupancyTaxFees,
            'total' => $bookingDetails->total,
            'status' => $bookingDetails->status,
            'property_status' => $bookingDetails->propertyStatus,
            'owner_status' => $bookingDetails->ownerStatus,
            'property_offers_id' =>$bookingDetails->propertyOffersId,
            'card_token'=>$bookingDetails->cardToken,
            'tripper_saving_amount' => $bookingDetails->tripperSavingAmount,
            'coupon_codes_id' => $bookingDetails->couponCodesId,
            'payout_fee' => $bookingDetails->payoutFee,
            'cleaning_fee'=>$bookingDetails->cleaningFee,
            'transaction_fee'=>$bookingDetails->transactionFee,
            'created_at' => $bookingDetails->createdAt,
            'updated_at' => $bookingDetails->updatedAt
        ]);
    }

    public function changeBookingStatus($bookingId, $status){
        return Booking::where('id', $bookingId)->update(['status'=>$status]);
    }

    public function checkBookings($id,$checkInDate,$checkOutDate){
        return Booking::where(['property_id'=>$id])
                                ->where(function($query) use($checkInDate,$checkOutDate){
                                    $query->where(function($q) use($checkInDate,$checkOutDate){
                                        $q->whereDate('check_in_date', '>',date('H:i:s',strtotime($checkInDate)))
                                                ->whereDate('check_in_date', '<',date('H:i:s',strtotime($checkOutDate)));
                                            })
                                        ->orwhere(function($q) use($checkInDate,$checkOutDate){
                                            $q->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkInDate)))
                                                ->whereDate('check_out_date', '<',date('Y-m-d',strtotime($checkOutDate)));
                                            })
                                        ->orwhere(function($q) use($checkInDate,$checkOutDate){
                                            $q->whereDate('check_in_date', '<',date('Y-m-d',strtotime($checkInDate)))
                                                ->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkInDate)));
                                        })
                                        ->orwhere(function($q) use($checkInDate,$checkOutDate){
                                            $q->whereDate('check_in_date', '<',date('Y-m-d',strtotime($checkOutDate)))
                                                ->whereDate('check_out_date', '>',date('Y-m-d',strtotime($checkOutDate)));
                                        })
                                        ->orwhere(function($q) use($checkInDate,$checkOutDate){
                                            $q->whereDate('check_in_date', '=',date('Y-m-d',strtotime($checkInDate)))
                                                ->whereDate('check_out_date', '=',date('Y-m-d',strtotime($checkOutDate)));
                                        });
                                })
                                ->whereIn('owner_status', array('confirmed', 'approved', 'instant_booking'))
                                ->get();
    }

    public function getUpcomingStays($userId)
    {
        return Booking::select('property.id as propertyid','booking.id as bookingId','property.title','property.price as price','check_in', 'check_out', 'location_detail', 'city', 'state','zipcode','price', 'cover_photo', 'total', 'owner_status', 'check_in_date', 'check_out_date','booking_type','actual_price','booking.cleaning_fee','adults','payment_type')
                                    ->join('property','booking.property_id','=','property.id')
                                    ->leftJoin('booking_payment', function($query) {
                                        $query->on('booking.id','=','booking_payment.booking_id')
                                        ->whereRaw('booking_payment.id IN (select MAX(a2.id) from booking_payment as a2 join booking as u2 on u2.id = a2.booking_id group by u2.id)');
                                        })
                                    ->where('check_in_date','>=', Carbon::parse(Carbon::now())->format('Y-m-d'))
                                    ->where('booking.rental_user_id','=',$userId)
                                    ->orderBy('check_in_date','desc')
                                    ->get();
    }

    public function getPastStays($userId)
    {
        return Booking::select('property.id as propertyid','booking.id as bookingId','property.title','check_in', 'check_out', 'city', 'state','zipcode', 'location_detail','price', 'cover_photo', 'total', 'owner_status', 'check_in_date', 'check_out_date','booking_type','actual_price','booking.cleaning_fee','adults','payment_type')
                                    ->join('property','booking.property_id','=','property.id')
                                    ->leftJoin('booking_payment', function($query) {
                                        $query->on('booking.id','=','booking_payment.booking_id')
                                            ->whereRaw('booking_payment.id IN (select MAX(a2.id) from booking_payment as a2 join booking as u2 on u2.id = a2.booking_id group by u2.id)');
                                        })
                                    ->where('check_in_date','<',Carbon::parse(Carbon::now())->format('Y-m-d'))
                                    ->where('booking.rental_user_id','=',$userId)
                                    ->whereIn('owner_status', array('approved','confirmed','instant_booking','rejected','auto_reject','cancelled'))
                                    ->orderBy('check_in_date','desc')
                                    ->get();
    }

    public function getBookingDetailByPropertyId($propertyId, $bookingId){
        return $bookingDetails = Booking::join('property','property.id','=','booking.property_id')
        ->join('owner','owner.id','=','property.owner_id')

        ->select('property.id as propertyid','property.title','check_in', 'check_out', 'property.city', 'property.state','price', 'cover_photo', 'total', 'owner_status', 'check_in_date', 'check_out_date', 'location_detail','zipcode','about_the_space','description','first_name', 'last_name','photo','total_bathroom','total_beds','total_bedroom','no_of_guest','type_of_property','cancellation_type','actual_price','booking_type','adults','booking.cleaning_fee','booking.id as bookingid')
        ->where(['booking.property_id'=>$propertyId,'booking.id' =>$bookingId])->first();
    }
}
