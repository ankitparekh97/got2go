<?php


namespace App\Repositories;


use App\Http\DTO\Api\RentalUserDTO;
use Illuminate\Support\Carbon;

interface AdminInterface
{
    public function updateUserPassword($email,$password);

}
