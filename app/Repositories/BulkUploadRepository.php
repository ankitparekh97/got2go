<?php


namespace App\Repositories;

use App\Models\BulkUpload;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Models\Jobs;
use DB;

class BulkUploadRepository implements BulkUploadInterface
{
    private $bulkUploadModel;

    public function __construct(
        BulkUpload $bulkUploadModel
    )
    {
        $this->bulkUploadModel = $bulkUploadModel;
    }

    /**
     * @param $id
     * @return BulkUpload|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getById($id)
    {
        /**
         * @var BulkUpload $bulkUploadModel
         */
        $bulkUploadModel = (new $this->bulkUploadModel())::query();
        $bulkUploadModel->where('id',$id);
        return $bulkUploadModel->first();
    }

     /**
     * @param $id
     * @return BulkUpload|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByUniqueId($unique_key)
    {
        /**
         * @var BulkUpload $bulkUploadModel
         */
        $bulkUploadModel = (new $this->bulkUploadModel)::query();
        $bulkUploadModel->where('unique_key',$unique_key);
        return $bulkUploadModel->first();
    }

    public function getListings($ownerId,$id){

        /**
         * @var BulkUpload $bulkUploadListings
         */

        $listings = (new $this->bulkUploadModel)::query();

        $listings->when($id, function ($query) use($id){
            $query->where('id',$id);
        });

        $listings->when($ownerId, function ($query) use($ownerId){
            $query->whereHas('owner', function ($q) use($ownerId){
                $q->where('id',$ownerId);
            });
        });

        $listings->orderBy(BulkUpload::ID,'DESC');
        $listings->with(['logs','owner']);

        if($ownerId=null){
            $bulkUploadListings = $listings->first();
        }
        $bulkUploadListings = $listings->get();

        return $bulkUploadListings;
    }

    public function getBulkUploadByParamsObject($params)
    {
        /**
         * @var BulkUpload $bulkUploadModel
         */

        $bulkUpload = (new $this->bulkUploadModel)::query();
        $bulkUpload->where($params);

        return $bulkUpload->first();
    }


    /**
     * @param BulkUploadDTO $bulkUploadDTO
     * @param null $id
     * @return mixed
     * @throws \Throwable
     */
    public function saveBulkUpload(BulkUploadDTO $bulkUploadDTO)
    {
        /**
         * @var BulkUpload $bulkUploadModel
         */

        $bulkUpload = (new $this->bulkUploadModel);

        if($bulkUploadDTO->id){
            $bulkUpload = $this->getById($bulkUploadDTO->id);
        }

        $bulkUpload->owner_id = !empty($bulkUploadDTO->ownerId) ? $bulkUploadDTO->ownerId : $bulkUpload->owner_id;
        $bulkUpload->property_doc = isset($bulkUploadDTO->propertyDoc) ? $bulkUploadDTO->propertyDoc : $bulkUpload->property_doc;
        $bulkUpload->property_images = !empty($bulkUploadDTO->propertyMedia) ? $bulkUploadDTO->propertyMedia : $bulkUpload->property_images;
        $bulkUpload->verfication_doc = !empty($bulkUploadDTO->propertyVerification) ? $bulkUploadDTO->propertyVerification : $bulkUpload->verfication_doc;
        $bulkUpload->govtid_doc = !empty($bulkUploadDTO->propertyGovtId) ? $bulkUploadDTO->propertyGovtId : $bulkUpload->govtid_doc;
        $bulkUpload->status = !empty($bulkUploadDTO->status) ? $bulkUploadDTO->status : $bulkUpload->status;
        $bulkUpload->unique_key = !empty($bulkUploadDTO->uniqueKey) ? $bulkUploadDTO->uniqueKey : $bulkUpload->unique_key;
        $bulkUpload->is_reupload = isset($bulkUploadDTO->isReupload) ? $bulkUploadDTO->isReupload : $bulkUpload->is_reupload;
        $bulkUpload->queue_id = isset($bulkUploadDTO->queue_id) ? $bulkUploadDTO->queue_id : $bulkUpload->queue_id;

        $bulkUpload->save();

        return $bulkUpload->id;
    }

    public function update($id,$data){
        $bulkUpload = BulkUpload::where('id',$id)->update($data);
        return $bulkUpload;
    }
}
