<?php


namespace App\Repositories;


use App\Http\DTO\Api\SubscriptionPlanDTO;
use App\Models\SubscriptionPlan;

class SubscriptionPlanRepository implements SubscriptionPlanInterface
{
    public $subscriptionPlan;

    public function __construct(
        SubscriptionPlan  $subscriptionPlan
    )
    {
        $this->subscriptionPlan = $subscriptionPlan;
    }

    public function createSubscriptionPlan(SubscriptionPlanDTO $subscriptionPlanDTO)
    {
        $subscriptionPlan = new $this->subscriptionPlan;

        $subscriptionPlan->id = !empty($subscriptionPlanDTO->id) ? $subscriptionPlanDTO->id : $subscriptionPlan->id;
        $subscriptionPlan->name = !empty($subscriptionPlanDTO->name) ? $subscriptionPlanDTO->name : $subscriptionPlan->name;
        $subscriptionPlan->amount = !empty($subscriptionPlanDTO->amount) ? $subscriptionPlanDTO->amount : $subscriptionPlan->amount;
        $subscriptionPlan->currency = !empty($subscriptionPlanDTO->currency) ? $subscriptionPlanDTO->currency : $subscriptionPlan->currency;
        $subscriptionPlan->interval = !empty($subscriptionPlanDTO->interval) ? $subscriptionPlanDTO->interval : $subscriptionPlan->interval;
        $subscriptionPlan->statement_descriptor = !empty($subscriptionPlanDTO->statementDescriptor) ? $subscriptionPlanDTO->statementDescriptor : $subscriptionPlan->statement_descriptor;

        $subscriptionPlan->save();

        return $subscriptionPlan;
    }

    public function getLastInsertedSubscriptionPlan()
    {
        /**
         * @var SubscriptionPlan  $subscriptionPlan
         */
        $subscriptionPlan = (new $this->subscriptionPlan)::query();
        $subscriptionPlan->orderBy('updated_at','desc');
        return $subscriptionPlan->first();
    }
}
