<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PropertyDTO as PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use App\Models\Property;
use App\Models\PropertyImages;
use App\Models\PropertyAvaiblities;
use DB;
use Illuminate\Support\Carbon;
use Auth;
use Session;
use DateTime;
class PropertyRepository implements PropertyInterface
{
    public $propertyModel;
    public $propertyMediaModel;

    public function __construct(
        Property $propertyModel,
        PropertyImages $propertyMediaModel
    )
    {
        $this->propertyModel = $propertyModel;
        $this->propertyMediaModel = $propertyMediaModel;
        $this->geoCode = env("GEOCODE");
    }

    public function getPropertyByIds($Ids){

        /**
         * @var Property $properties
         */

        $listings = (new $this->propertyModel)::query();

        $listings->whereIn(Property::ID, $Ids);
        $listings->with(['resort','propertytype']);
        $properties = $listings->get();

        return $properties;
    }

    public function getPropertyByOwnerId($ownerId,$status){

        /**
         * @var Property $listings
         */

        $listings = (new $this->propertyModel)::query();

        $listings->whereIn(Property::STATUS, $status);
        $listings->where(Property::OWNER_ID,$ownerId);
        $listings->orderBy(Property::ID,'DESC');
        $listings->with(['resort','propertytype']);

        $propertiesListings = $listings->get();
        return $propertiesListings;
    }

    public function getByParamsCollection($params)
    {
        $messages = (new $this->messagesModel)::query();
        $messages->where($params);

        $messages->with(['propertyimages','propertyownershipdoc', 'propertyavailiblity','propertybedrooms','propertyamenities']);
        return $messages->get();
    }

    public function getByParamsObject($params)
    {
        $property = (new $this->propertyModel)::query();
        $property->where($params);

        $property->with(['propertyimages','propertyownershipdoc', 'propertyavailiblity','propertybedrooms','propertyamenities']);
        return $property->first();
    }

    public function publishProperty($id){

        /**
         * @var Property $property
         */

        $property = (new $this->propertyModel);
        if($id){
            $property = $this->findPropertyById($id);
        }

        $property->publish = ($property->publish == '1') ? '0' : '1';
        if($property->publish == '0'){
            $property->updated_at = Carbon::now();
        }

        $property->save();
        return $property;
    }

    public function deleteProperty($id){

        /**
         * @var Property $property
         */

        $property = (new $this->propertyModel);
        if($id){
            $property = $this->findPropertyById($id);
        }

        $property->delete();
        return $property;
    }

    public function createProperty($property){
        $property = Property::insertGetId($property);
        Property::where(['id'=>$property,'type_of_property'=>'property'])->update(['resort_id'=>$property]);
        $getProperty = $this->findPropertyById($property);
        return $getProperty;
    }

    public function updateProperty($id,$data){
        $property = Property::where('id',$id)->update($data);
        return $property;
    }

    public function findPropertyById($Id)
    {
        /**
         * @var Property $property
         */
        $property = (new $this->propertyModel)::query();
        $property->where(Property::ID,$Id);

        $property->with(['propertyimages','propertyownershipdoc', 'propertyavailiblity','propertybedrooms','propertyamenities']);
        return $property->first();
    }

    public function getPropertyByUniqueKey($uniqueId)
    {
        return Property::select('bulk_upload_id')
                        ->where('bulkupload_uniquekey',$uniqueId)
                        ->get()->keyBy('bulk_upload_id','bulk_upload_id')->toArray();
    }

    /**
     * @param $propertyId
     * @return mixed
     */
    public function getPropertyDetailsById($propertyId)
    {
        return Property::with(['propertyimages','propertybedrooms','propertyamenities', 'ownerdetails','propertyreviews'=>function($reviews) {
            return $reviews->selectRaw('avg(rating) as avg_rating, property_id,count(*) as total_review')->groupBy('property_id')->first();}])->where(['id'=>$propertyId])->first();
    }


    /**
     * @param $propertyId
     * @param $averageMinus
     * @param $averagePlus
     * @return mixed
     */
    public function simillarlisting($propertyId,$averageMinus,$averagePlus)
    {
        return Property::join('property_type','property_type.id', '=','property.property_type_id')
                        ->select('property_type.property_type','property.*','property.id')
                        ->where('property.price','>=', $averageMinus)
                        ->where('property.price','<=', $averagePlus)
                        ->whereNotIn('property.id', $propertyId)
                        ->where('property.status', 'approved')
                        ->where('property.publish', '1')
                        ->take(4)
                        ->orderBy('property.id','desc')
                        ->get();
    }

    public function timeShareList(){
        $list =  Property::join('owner', 'owner.id', '=', 'property.owner_id')
                ->leftJoin('property_ownership_documents','property_ownership_documents.property_id','=','property.id')
                ->select(
                    'property.*',
                    'owner.first_name as ownerUserFirstName',
                    'owner.last_name as ownerUserLastName',
                    'owner.email as email',
                    'owner.goverment_id_doc',
                    'property_ownership_documents.document_name as ownershipDoc',
                    DB::raw('CASE WHEN property.status = "pending" then 0 else 1 END as statustype')
                )
                ->where('property.status', '!=', 'draft')
                ->whereRaw("( property.bulkupload_uniquekey='' or property.bulkupload_uniquekey is null) ")
                ->orderBy('statustype')
                ->orderBy('property.id','desc')
                ->groupBy('property.id')
                ->get();

        return $list;
    }

    public function bulkTimeshareList(){
        $list = Property::join('owner', 'owner.id', '=', 'property.owner_id')
                ->select(
                    'property.*',
                    'owner.first_name as ownerUserFirstName',
                    'owner.last_name as ownerUserLastName',
                    'owner.email as email',
                    'owner.goverment_id_doc',
                    DB::raw('CASE WHEN property.status = "pending" then 0 else 1 END as statustype'),
                    DB::raw('COUNT(property.id) as listings')
                )
                // ->where('property.status', '=', 'pending')
                ->where('property.bulkupload_uniquekey','!=','')
                ->orderBy('statustype')
                ->orderBy('property.id','desc')
                ->groupBy('property.bulkupload_uniquekey')
                ->get();

        return $list;
    }

    public function propertyApprove($input){
        $propertyId = $input['id'];
        $status = $input['action'];
        $properties = Property::find($propertyId);
        $properties->status = $status;
        $properties->save();
        return $properties;
    }

    public function viewDocument($type,$id){
        if($type == 'property'){
            $isGovtId = NULL;
        }else{
            $isGovtId = 1;
        }
        $Doc = PropertyOwnershipDocuments::where('property_id',$id)->where('is_govt_id',$isGovtId)->get()->toArray();
        return $Doc;
    }

    public function propertyApproveAll($input){
        $bulkUploadUniqueKey = $input['bulkupload_uniquekey'];
        $status = $input['action'];
        $properties = Property::where('bulkupload_uniquekey',$bulkUploadUniqueKey)->first();
        if(!empty($properties)){
            if($status == 'review_individually'){
                Property::where('bulkupload_uniquekey',$bulkUploadUniqueKey)->update([
                    'bulkupload_uniquekey' => NULL
                ]);
            }else{
                Property::where('bulkupload_uniquekey',$bulkUploadUniqueKey)->update([
                    'status' => $status
                ]);
            }
        }
        return $properties;
    }

    public function generateQuery($input){
        DB::enableQueryLog();
        $locationDetail = explode(',',$input['location_detail']);
        $noOfGuest = '1';
        $bedroom = '1';
        $bathroom = '1';
        $pageno = 0;
        $resortId = '';
        $searchText = '';
        if(isset($input['searchText']) && $input['searchText'] != '')
        {
            $searchText = $input['searchText'];
        }

        if(isset($input['pageno']) && $input['pageno'] != '')
        {
            $pageno = $input['pageno'] * 10;
        }

        if(isset($input['resort_id']) && $input['resort_id'] != '')
        {
            $resortId = $input['resort_id'];
        }

        if(isset($input['number_of_guest']) && $input['number_of_guest'] != '')
        {
            $noOfGuest = $input['number_of_guest'];
        }



        if(isset($input['bedroom']) && $input['bedroom'] != '')
        {
            $bedroom = $input['bedroom'];
        }

        if(isset($input['bathroom']) && $input['bathroom'] != '')
        {
            $bathroom = $input['bathroom'];
        }

        $checkinDate = '';
        $checkoutDate = '';
        if(isset($input['filter_dates']) && $input['filter_dates'] != '')
        {
            $dates = explode(' - ', $input['filter_dates']);
            $checkinDate = date('Y-m-d',strtotime($dates[0]));
            $checkoutDate = date('Y-m-d',strtotime($dates[1]));
        }

        $propertyType = [];

        if(isset($input['property_type']) && !empty($input['property_type']))
        {
            $propertyType = $input['property_type'];
        }

        $city ='' ; $state = '';
        if(count($locationDetail) == 3 && (isset($locationDetail[2]) && ltrim($locationDetail[2],' ') == 'USA'))
        {
            $city = rtrim(ltrim($locationDetail[0], ' '), ' ');
            $state = rtrim(ltrim($locationDetail[1], ' '), ' ');
        }

        $lower = 0; $upper = 1000;
        if(isset($input['lower']) && $input['lower'] != '')
        {
            $lower = $input['lower'];
        }

        if(isset($input['upper']) && $input['upper'] != '')
        {
            $upper = $input['upper'];
        }

        $subProperty = [0];
        if(isset($input['subPropertyType']) && !empty($input['subPropertyType']))
        {
            $subProperty = $input['subPropertyType'];
        }

        $amenities = [];
        if(isset($input['amenities']) && !empty($input['amenities']))
        {
            $amenities = $input['amenities'];
        }

        $q = Property::leftjoin('hotels','hotels.id','=','resort_id');
        $q->where('property.status',  'approved')->where('publish',  '1');
        $q->where('price', '>=', $lower)->where('price', '<=', $upper)->where('no_of_guest', '>=', $noOfGuest)->where('total_bedroom', '>=', $bedroom)->where('total_bathroom', '>=', $bathroom);

        if(Auth::guard('rentaluser')->user())
        {
            $userId= Auth::guard('rentaluser')->user()->id;
            $q->leftjoin('favourite_tripboard',function($join) use($userId){
                $join->on("favourite_tripboard.property_id" ,"property.id");
                $join->where('favourite_tripboard.user_id',$userId);
            });
        }

        if($checkinDate!=''){

            $dStart =  new DateTime($checkoutDate);
            $dEnd =   new DateTime($checkinDate);
            $dDiff = $dEnd->diff($dStart);
            $mindays = $dDiff->format('%r%a');
            $q->joinSub("select property_id,count(id) noOfAvailble from property_avaiblities where
            ( property_avaiblities.available_from <= '$checkinDate' and  property_avaiblities.available_to >= '$checkoutDate'
            and is_partial_booking_allowed = 1 and minimum_days <= '$mindays')
            or ( property_avaiblities.available_from = '$checkinDate' and  property_avaiblities.available_to = '$checkoutDate'
            and is_partial_booking_allowed = 0 )
            group by property_id",'avilableProperty', 'property.id', '=', 'avilableProperty.property_id', 'inner');

            $checkinDate = $dEnd->modify("+1 days")->format('Y-m-d');
            $q->leftjoin('booking', function($join) use($checkinDate,$checkoutDate)
            {
                $join->on("property.id" ,"booking.property_id");
                $join->whereRaw("(  ( '$checkinDate' BETWEEN booking.check_in_date  and booking.check_out_date)
                or ('$checkoutDate' BETWEEN booking.check_in_date  and booking.check_out_date) )
                and (booking.owner_status in ('approved','confirmed','instant_booking'))");
            })->whereRaw('booking.id is null');

        }else{
            $date = date('Y-m-d',time());
            $q->joinSub("select property_id,count(id) noOfAvailble from property_avaiblities where
            ( property_avaiblities.available_to >= '$date' )
            group by property_id",
            'avilableProperty', 'property.id', '=', 'avilableProperty.property_id', 'inner');
        }

        $q->with(['propertyimages','booking']);
        if(count($amenities) >= 1)
        {
            $amenitiesStr = implode(',',$amenities);
            $q->whereRaw(DB::raw('property.id in (SELECT property_id FROM property_amenities where  property_id = property.id  and amenity_id in('.$amenitiesStr.') GROUP BY property_id HAVING COUNT(DISTINCT property_amenities.amenity_id) = '.count($amenities).')'));
        }

        if($resortId != '' )
        {
            $q->where('hotels.id', '=', $resortId);
            $q->where('property.type_of_property', '=', 'vacation_rental');
        }

        if($city != '')
        {
            if(!isset($input['zero_stay']))
            {
                $q->where('property.city',$city)->where('property.state',$state);
            }
        }
        elseif($searchText !='')
        {
            $q->whereRaw("(
                property.title like '%$searchText%'
                or property.city like '%$searchText%'
                or hotels.name like '%$searchText%'
                or hotels.city like '%$searchText%'
            ) ");
        }

        if(count($subProperty) > 0)
        {
            $q->whereIn('property_type_id', $subProperty);
        }

        $q->whereIn('property.type_of_property', $propertyType);


        return $q;
    }

    public function selectDataFromQuery($input, $isRentalUser=false){
        $resortId = '';
        if(isset($input['resort_id']) && $input['resort_id'] != '')
        {
            $resortId = $input['resort_id'];
        }

        $pageno = 0;
        if(isset($input['pageno']) && $input['pageno'] != '')
        {
            $pageno = $input['pageno'] * 10;
        }

        $selectArr[] =  DB::raw('property.*, hotels.name as resort_name,hotels.location as resort_location_detail');

        if($resortId == ''){
            $selectArr[] = DB::raw('case when property.type_of_property ="vacation_rental" then count(hotels.id)  else 1 end as totalresortcount');
            $selectArr[] = DB::raw('case when property.type_of_property ="vacation_rental" and count(hotels.id) > 1 then substring_index(group_concat(distinct property.id order by price), ",", 2) else NULL end as vacation_clubs');
        }

        $selectArr[] = DB::raw('case when property.lat_lang IS NOT NULL then SUBSTRING_INDEX(lat_lang,"#",1) else NULL end as latitude');
        $selectArr[] = DB::raw('case when property.lat_lang IS NOT NULL then SUBSTRING_INDEX(lat_lang,"#",-1) else NULL end as longitude');

        $lat = 0;
        $lang = 0;

        if($input['location_detail'] != ''){
            $geoCode = $this->geoCode;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($input['location_detail'])."&key=".$geoCode;
            $resultString = file_get_contents($url);
            $resultG = json_decode($resultString, true);
            if(!empty($resultG['results']) && $resultG['status'] != 'ZERO_RESULTS')
            {
                $resultG1[]=$resultG['results'][0];
                $lat = $resultG1[0]['geometry']['location']['lat'];
                $lang = $resultG1[0]['geometry']['location']['lng'];
            }
        }
        $selectArr[] = DB::raw("SQRT(
            POW(69.1 * (SUBSTRING_INDEX(lat_lang,'#',1) - $lat), 2) +
            POW(69.1 * ($lang - SUBSTRING_INDEX(lat_lang,'#',-1)) * COS(SUBSTRING_INDEX(lat_lang,'#',1) / 57.3), 2)) AS distance");

        if($isRentalUser)
        {
            $selectArr[] = DB::raw('case when favourite_tripboard.id is null then 0 else 1 end as isLiked');
        }

        return $selectArr;
    }

    public function searchProperty($q, $selectArr, $input, $isRentalUser = false)
    {
        $q->select($selectArr)->orderBy('price');

        $resortId = '';
        if(isset($input['resort_id']))
        {
            $resortId = $input['resort_id'];
        }

        $pageno = 0;
        if(isset($input['pageno']) && $input['pageno'] != '')
        {
            $pageno = $input['pageno'] * 10;
        }

        if($resortId == '')
        {
            $q->groupBy('resort_id', 'type_of_property');
            $q->offset($pageno)->limit(10);
        }

        $properties= $q->get();
        $zeroStay = 0;

        if(count($properties) == 0)
        {
            $input['zero_stay'] = 1;
            $zeroStay = 1;
            $q = $this->generateQuery($input);
            $selectArr = $this->selectDataFromQuery($input, $isRentalUser);
            $q->select($selectArr)->orderBy('price');
            $resortId = '';

            if(isset($input['resort_id']) && $input['resort_id'] != '')
            {
                $resortId = $input['resort_id'];
            }

            $pageno = 0;
            if(isset($input['pageno']) && $input['pageno'] != '')
            {
                $pageno = $input['pageno'] * 10;
            }

            $q->having('distance', '<=', 100);

            if($resortId == ''){
                $q->groupBy('resort_id', 'type_of_property');
                $q->offset($pageno)->limit(10);
            }

            $properties= $q->get();

        }

        $resortGroup = [];
        $resortObj =[];
        if($resortId == '')
        {
            $resortArr = [];
            foreach($properties as $key=>$property)
            {
                $resortGroup[$property->resort_id] =  ($property->totalresortcount > 1) ? explode(',',$property->vacation_clubs) : null;
            }

            $resortGroup = array_filter($resortGroup);
            foreach($resortGroup as $resort)
            {
                foreach($resort as $r)
                {
                    $resortArr[] = $r;
                }
            }

            $resortObj = Property::whereIn('id',$resortArr)->with(['propertyimages'])->orderBy('price', 'asc')->get();
            $resortObj = collect($resortObj)->groupBy('id');
        }

        Session::forget('propertyId');
        Session::forget('no_of_guest');
        Session::forget('check_in_date');
        Session::forget('check_out_date');

        if(Auth::guard('rentaluser')->user())
        {
            $userId= Auth::guard('rentaluser')->user()->id;
        }
        else
        {
            $userId = '';
        }

        $pageno = $input['pageno'] + 1;

        return [
            'properties' => $properties,
            'resortGroup' => $resortGroup,
            'resortObj' => $resortObj,
            'user_id' => $userId,
            'page' => $pageno,
            'zero_stay' => $zeroStay
        ];

    }

    public function searchPropertyCount($input)
    {
        $propertyType = array('property','vacation_rental','hotels');

        $input['property_type'] = $propertyType;

        $q = $this->generateQuery($input);

        $resortId = '';
        if(isset($input['resort_id']) && $input['resort_id'] != '')
        {
            $resortId = $input['resort_id'];
        }

        $selectArr[] =  DB::raw(' type_of_property, count(*) as total_count');

        $q->select($selectArr)->orderBy('price');

        if($resortId == ''){
            $q->groupBy( 'type_of_property');
        }

        $properties= $q->get();

        $zeroStay = 0;

        if(count($properties) == 0){
            if($input['location_detail'] != ''){
                $geoCode = $this->geoCode;
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($input['location_detail'])."&key=".$geoCode;
                $result_string = file_get_contents($url);
                $result_G = json_decode($result_string, true);
                if(!empty($result_G['results']) && $result_G['status'] != 'ZERO_RESULTS')
                {
                    $resultG1[]=$result_G['results'][0];
                    $lat = $resultG1[0]['geometry']['location']['lat'];
                    $lang = $resultG1[0]['geometry']['location']['lng'];
                }
            }

            $zeroStay = 1;
            $input['zero_stay'] = 1;
            $q = $this->generateQuery($input);

            $resortId = '';
            if(isset($input['resort_id']) && $input['resort_id'] != '')
            {
                $resortId = $input['resort_id'];
            }

            $selectArr[] =  DB::raw(' type_of_property, count(*) as total_count');
            $selectArr[] = DB::raw("case when SQRT(
                POW(69.1 * (SUBSTRING_INDEX(lat_lang,'#',1) - $lat), 2) +
                POW(69.1 * ($lang - SUBSTRING_INDEX(lat_lang,'#',-1)) * COS(SUBSTRING_INDEX(lat_lang,'#',1) / 57.3), 2)) <= 100 then 1 else 0 end AS distance");

            $q->select($selectArr)->orderBy('price');

            if($resortId == ''){
                $q->having('distance', '=', 1);
                $q->groupBy( 'type_of_property', 'distance');
            }
            $properties= $q->get();
        }

        Session::forget('propertyId');
        Session::forget('no_of_guest');
        Session::forget('check_in_date');
        Session::forget('check_out_date');

        $pageno = $input['pageno'] + 1;
        return [
            'properties' => $properties,
            'page' => $pageno,
            'zero_stay' => $zeroStay
        ];
    }

    public function getHotDeals($input)
    {
        $locationDetail = explode(',',$input['hotdeal_location']);
        $noOfGuest = '1';
        $city ='' ; $state = '';
        if(count($locationDetail) == 3 && (isset($locationDetail[2]) && ltrim($locationDetail[2],' ') == 'USA'))
        {
            $city = rtrim(ltrim($locationDetail[0], ' '), ' ');
            $state = rtrim(ltrim($locationDetail[1], ' '), ' ');
        }

        if(isset($input['number_of_guest']))
        {
            $noOfGuest = $input['number_of_guest'];
        }

        $checkinDate = '';
        $checkoutDate = '';
        if(isset($input['hotdeal_dates']) && $input['hotdeal_dates'] != '')
        {
            $dates = explode(' - ', $input['hotdeal_dates']);
            $checkinDate = date('Y-m-d',strtotime($dates[0]));
            $checkoutDate = date('Y-m-d',strtotime($dates[1]));
        }

        $hotDealsSlider = Property::select(
            'property.id as propertyid',
            'city',
            'state',
            'price',
            'cover_photo',
            \DB::raw('avg(property_reviews.rating) as avgRating'),
            \DB::raw('count(property_reviews.rating) as totalRating')
        )
        ->leftJoin('property_reviews','property.id','=','property_reviews.property_id')
        ->where('is_hotdeals','1');
        if($city != '')
        {
            $hotDealsSlider->where('property.city',$city)->where('property.state',$state);
        }
        $hotDealsSlider->where('property.status',  'approved')->where('publish',  '1');
        $hotDealsSlider->where('no_of_guest', '>=', $noOfGuest)->where('total_bedroom', '>=', 1)->where('total_bathroom', '>=', 1);
        if($checkinDate!='')
        {
            $dStart =  new DateTime($checkoutDate);
            $dEnd =   new DateTime($checkinDate);
            $dDiff = $dEnd->diff($dStart);
            $mindays = $dDiff->format('%r%a');
            $hotDealsSlider->joinSub("select property_id,count(id) noOfAvailble from property_avaiblities where
                                    ( property_avaiblities.available_from <= '$checkinDate' and  property_avaiblities.available_to >=   '$checkoutDate'
                                    and is_partial_booking_allowed = 1 and minimum_days <= '$mindays')
                                    or ( property_avaiblities.available_from = '$checkinDate' and  property_avaiblities.available_to =      '$checkoutDate'
                                    and is_partial_booking_allowed = 0 )
                                    group by property_id",
                                    'avilableProperty',
                                    'property.id', '=', 'avilableProperty.property_id', 'inner');
            $checkinDate = $dEnd->modify("+1 days")->format('Y-m-d');
            $hotDealsSlider->leftjoin('booking', function($join) use($checkinDate,$checkoutDate)
            {
                $join->on("property.id" ,"booking.property_id");
                $join->whereRaw("(  ( '$checkinDate' BETWEEN booking.check_in_date  and booking.check_out_date)
                            or ('$checkoutDate' BETWEEN booking.check_in_date  and booking.check_out_date) )
                            and (booking.owner_status in ('approved','confirmed','instant_booking'))");
            })->whereRaw('booking.id is null');
        }
        else
        {
            $date = date('Y-m-d',time());
            $hotDealsSlider->joinSub("select property_id,count(id) noOfAvailble from property_avaiblities where
                            ( property_avaiblities.available_to >= '$date' )
                            group by property_id",
                            'avilableProperty', 'property.id', '=', 'avilableProperty.property_id', 'inner');
        }
            $hotDealsSlider->limit(9)
            ->groupBy('property.id')
            ->orderBy('property.id','desc');

            $hotDealsSlider= $hotDealsSlider->get();

            return $hotDealsSlider;

    }

    public function getHotDealsByLimit(){
        $hotDealsSlider = Property::select(
            'property.id as propertyid',
            'city',
            'state',
            'price',
            'cover_photo',
            \DB::raw('avg(property_reviews.rating) as avgRating'),
            \DB::raw('count(property_reviews.rating) as totalRating')
        )
        ->leftJoin('property_reviews','property.id','=','property_reviews.property_id')
        ->where('is_hotdeals','1')
        ->limit(9)
        ->groupBy('property.id')
        ->orderBy('property.id','desc')
        ->get();
        return $hotDealsSlider;

    }

    public function getownerPropertyById($id){
        return Property::select('id','title','cover_photo')->where('owner_id',$id)->limit(20)->get();
    }

    public function similarListing($tripboard)
    {
        $averagePrice = DB::table('property')
        ->join('tripboard_property','tripboard_property.property_id','=','property.id')
        ->select(DB::raw('round(AVG(price),2) as price'))
        ->where('tripboard_property.tripboard_id', $tripboard->id)
        ->take(20)
        ->first();

        $averagePlus = 0;
        $averageMinus = 0;
        $percentage = 25;

        if(!empty($averagePrice)) {
            $averagePlus = round(($averagePrice->price + ($percentage / 100) * $averagePrice->price),2);
            $averageMinus = round(($averagePrice->price - ($percentage / 100) * $averagePrice->price),2);
        }
        $similarListing = [];
        if(!empty($averagePrice)) {
            $q = Property::joinSub("select property_id from property_avaiblities where property_avaiblities.available_from <= '$tripboard->from_date' and  property_avaiblities.available_to >= '$tripboard->to_date' group by property_id",
            'avilableProperty', 'property.id', '=', 'avilableProperty.property_id', 'inner');
            $q->join('property_type','property_type.id', '=','property.property_type_id');
            $q->select('property_type.property_type','property.*','property.id');
            $q->where('property.price','>=', $averageMinus);
            $q->where('property.price','<=', $averagePlus);
            $q->where('property.no_of_guest', '>=',$tripboard->no_of_guest);
            $q->whereNotIn('property.id', DB::table('tripboard_property')->where('tripboard_property.tripboard_id', $tripboard->id)->pluck('property_id'));
            $q->where('property.status', 'approved');
            $q->where('property.publish', '1');
            $q->take(5);
            $q->orderBy('property.id','desc');
            $similarListing = $q->get();
        }

        return $similarListing;
    }

    public function getTripBoardPropertyImages($id)
    {
        return Property::join('tripboard_property','tripboard_property.property_id','=','property.id')->select('tripboard_property.property_id','property.cover_photo','tripboard_property.tripboard_id')->where('tripboard_property.tripboard_id',$id)->take(3)->get();
    }

    public function propertyAvailablity($id,$checkInDate,$checkOutDate){
        return PropertyAvaiblities::where('property_id', $id)
                            ->whereRaw(" ((property_avaiblities.available_from <= '$checkInDate' and  property_avaiblities.available_to >= '$checkOutDate'
                            and is_partial_booking_allowed = 1)
                            or ( property_avaiblities.available_from = '$checkInDate' and  property_avaiblities.available_to = '$checkOutDate'
                            and is_partial_booking_allowed = 0 ) )")
                            ->get();

    }
}
