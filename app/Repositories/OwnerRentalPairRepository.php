<?php


namespace App\Repositories;

use App\Helpers\GlobalConstantDeclaration;
use App\Models\OwnerRentalPair;
use Carbon\Carbon;
use DB;

class OwnerRentalPairRepository implements OwnerRentalPairInterface
{

    public $ownerRentalPairModel;

    public function __construct(
        OwnerRentalPair $ownerRentalPairModel
    )
    {
        $this->ownerRentalPairModel = $ownerRentalPairModel;
    }

    public function getOwnerRentalPairByParamsObject($params)
    {
        $ownerRentalPair = (new $this->ownerRentalPairModel)::query();
        $ownerRentalPair->where($params);

        $ownerRentalPair->with(['ownerUser','rentalUser','adminUser','messages','lastMessages']);
        return $ownerRentalPair->first();
    }

    public function getData($name){

        $ownerRentalPair = (new $this->ownerRentalPairModel)::query();

        $ownerRentalPair->withCount(['messages'=>function($query){
            return $query->where(['is_read'=>'1','to_user'=> getOwnerId()]);
        }]);

        $ownerRentalPair->where(function ($query) use ($name){
            $query->whereHas('rentalUser', function ($query1) use ($name) {
                $query1->where('first_name', 'like', "%{$name}%")
                        ->orWhere('last_name',  'like', "%{$name}%")
                        ->orWhere(DB::raw("CONCAT(first_name,' ', last_name)"),  'like', "%{$name}%");
                })
                ->orWhereHas('adminUser', function ($query) use ($name) {
                    $query->where('name', 'like', "%{$name}%");
                });
        });

        $ownerRentalPair->where('owner_id',getOwnerId());
        $ownerRentalPair->orderBy('updated_at',GlobalConstantDeclaration::ORDER_BY_DESC);
        $ownerRentalPair->with(['rentalUser','ownerUser','lastMessages','adminUser']);

        $users = $ownerRentalPair->get();
        return $users;
    }

    public function update($params){

        $getId = $this->getOwnerRentalPairByParamsObject($params);
        if(!empty($getId)){
            $getId->updated_at = Carbon::now();
            $getId->save();
        }
    }

    /**
     * @param $ownerId
     * @param $rentalId
     * @param $type
     * @return mixed
     */
    public function saveOwnerRentalPair($ownerId,$rentalId,$type)
    {
        return OwnerRentalPair::updateOrCreate(['owner_id'=>$ownerId,'rental_id'=>$rentalId,'type'=>$type],['created_at'=>Carbon::now(),'updated_at'=>Carbon::now()]);;
    }

    /**
     * @param $pairId
     * @return mixed
     */
    public function updateOwnerRentalPair($pairId){
        OwnerRentalPair::where('id',$pairId)->update(['updated_at'=>Carbon::now()]);
    }

     /**
     * @param $adminId
     * @param $rentalId
     * @param $type
     * @return mixed
     */
    public function saveAdminRentalPair($adminId,$rentalId,$type)
    {
        return OwnerRentalPair::updateOrCreate(['rental_id'=>$rentalId,'admin_id'=>1,'type'=>$type],['created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now()]);
    }

    /**
     * @param $adminId
     * @param $ownerId
     * @param $type
     * @return mixed
     */
    public function saveAdminOwnerPair($adminId,$ownerId,$type)
    {
        return OwnerRentalPair::updateOrCreate(['owner_id'=>$ownerId,'admin_id'=>1,'type'=>$type],['created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now()]);
    }

    public function fetchRentalChatContact($name)
    {
        $rentalId = getLoggedInRentalId();
        return OwnerRentalPair::with(['rentalUser','ownerUser','lastMessages','adminUser'])
                ->withCount(['messages'=>function($query) use($rentalId){
                            return $query->where(['is_read'=>'1','to_user'=>$rentalId]);
                        }])
                ->where(function ($query) use ($name){
                    $query->whereHas('ownerUser', function ($q) use ($name) {
                        $q->where('first_name', 'like', "%{$name}%")
                            ->orWhere('last_name',  'like', "%{$name}%")
                            ->orWhere(DB::raw("CONCAT(first_name,' ', last_name)"),  'like', "%{$name}%");})
                    ->orWhereHas('adminUser', function ($query) use ($name) {
                            $query->where('name', 'like', "%{$name}%");});
                    })
                ->where('rental_id',$rentalId)
                ->orderBy('updated_at','desc')->get();
    }

    public function fetchOwnerChatContact($name)
    {
        $ownerId = getOwnerId();
        return OwnerRentalPair::with(['rentalUser','ownerUser','lastMessages','adminUser'])
                ->withCount(['messages'=>function($query) use($ownerId){
                            return $query->where(['is_read'=>'1','to_user'=>$ownerId]);
                        }])
                ->where(function ($query) use ($name){
                    $query->whereHas('rentalUser', function ($q) use ($name) {
                        $q->where('first_name', 'like', "%{$name}%")
                            ->orWhere('last_name',  'like', "%{$name}%")
                            ->orWhere(DB::raw("CONCAT(first_name,' ', last_name)"),  'like', "%{$name}%");})
                    ->orWhereHas('adminUser', function ($query) use ($name) {
                            $query->where('name', 'like', "%{$name}%");});
                    })
                ->where('owner_id',$ownerId)     
                ->orderBy('updated_at','desc')->get();
    }
}
