<?php


namespace App\Repositories;



interface MessagesInterface
{
    public function saveMessages($messageArray);

    public function getMessageCount($message,$toUserType);

    public function getMessages($userId);

    public function getByParamsCollection($params);

    public function getByParamsObject($params);

    public function create($params);

    public function fetchMessages($pairId);

    public function unreadFlag($pairId,$toUserId);
}
