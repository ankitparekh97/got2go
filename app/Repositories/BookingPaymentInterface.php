<?php


namespace App\Repositories;

use App\Http\DTO\Api\BookingPaymentDTO;

interface BookingPaymentInterface
{
    public function createBookingPayment($bookingPaymentDetails);

    public function getByParamsObject($params);

    public function getByParamsCollection($params);

    public function bookingHistory();

    public function saveBookingPayment(BookingPaymentDTO $bookingPaymentDTO);
    public function getTransactionLogs();

    public function getBookingPaymentWithId($id=null,$bookingId=null);

    public function insertBookingPayment($input);
    public function getLatestBookingPayment($bookingId);

}
