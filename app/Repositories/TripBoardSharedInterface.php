<?php


namespace App\Repositories;

interface TripBoardSharedInterface
{
    public function checkForSharedTripBoard($id, $userId);
    public function saveSharedTripBoard($tripboardId, $userId);
}
