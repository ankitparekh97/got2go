<?php


namespace App\Repositories;

interface TripBoardVoteInterface
{
    public function voteTripboard($param);

    public function deleteTripBoardPropertyVotes($tripboardId, $propertyId);

}
