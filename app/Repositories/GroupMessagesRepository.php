<?php


namespace App\Repositories;

use App\Models\GroupMessages;
use DB;
use Illuminate\Support\Carbon;

class GroupMessagesRepository implements GroupMessagesInterface
{

    public $messagesModel;

    public function __construct(
        GroupMessages $groupMessagesModel
    )
    {
        $this->groupMessagesModel = $groupMessagesModel;
    }

    /**
     * @param $ownerId
     * @param $rentalId
     * @param $type
     * @return mixed
     */
    public function saveMessages($messageArray)
    {
        return GroupMessages::create($messageArray);;
    }

    /**
     * @param $tripboardId
     * @return mixed
     */
    public function getMessages($tripboardId){
        return GroupMessages::with(['rentalUser'])->where('tripboard_id',$tripboardId)->orderBy('created_at', 'ASC')->get();
    }
}
