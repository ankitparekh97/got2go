<?php


namespace App\Repositories;


use App\Http\DTO\Api\MakeOfferDTO;

interface MakeOfferInterface
{
    public function saveMakeOffer(MakeOfferDTO $makeOfferDTO,$rentalId = null);
}
