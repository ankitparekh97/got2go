<?php


namespace App\Repositories;

use App\Models\BulkUpload;
use App\Models\BulkUploadLogs;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Models\Jobs;
use DB;

class BulkUploadLogsRepository implements BulkUploadLogsInterface
{
    private $bulkUploadLogsModel, $jobsModel;

    public function __construct(
        BulkUploadLogs $bulkUploadLogsModel,
        Jobs $jobsModel
    )
    {
        $this->bulkUploadLogsModel = $bulkUploadLogsModel;
        $this->jobsModel = $jobsModel;
    }

    public function getLogs($Id){

        /**
         * @var BulkUploadLogs $bulkUploadLogs
         */

        $listings = (new $this->bulkUploadLogsModel)::query();

        $listings->where(BulkUploadLogs::ID,$Id);
        $listings->orderBy(BulkUploadLogs::ID,'DESC');
        $listings->with(['bulkUpload']);
        $propertiesListings = $listings->first();

        return $propertiesListings;
    }



    public function getQueueCount(){

        /**
         * @var Jobs $jobsModel
         */

        $queueCount = (new $this->jobsModel)::query();

        $queueCount->where(Jobs::QUEUE,'default');
        $queue = $queueCount->get()->count();

        return $queue;
    }

    public function getrowPosition($queueId){


        $position = "SELECT id,queue, @rownum := @rownum + 1 AS position
        FROM jobs, (SELECT @rownum := 0) r
        where queue = 'default'
        ORDER BY id asc";

        $rank =   DB::select(DB::raw("select z.position FROM ($position) as z WHERE id = $queueId"));
        return $rank;
    }

    public function saveLogs($params){
        return BulkUploadLogs::insert($params);
    }

}
