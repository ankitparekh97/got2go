<?php


namespace App\Repositories;

use App\Http\DTO\Api\OwnerUserDTO;
use App\Http\DTO\Api\RentalUserDTO;
use App\Owner;
use Illuminate\Support\Carbon;

interface OwnerInterface extends BraintreePaymentInterface
{
    public function getAll();
    public function getOwnerById($id);
    public function userList();
    public function save(OwnerUserDTO $ownerDTO);

    public function userActivate($userId,$propertyId,$status);
    public function updateUserPassword($email,$password);
    public function createUser($ownerArray);
    public function getOwnerByGoogleId($googleId,$email);
    public function updateOwnerUserSetting($input);

}
