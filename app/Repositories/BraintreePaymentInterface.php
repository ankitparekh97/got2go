<?php


namespace App\Repositories;



use App\Services\PaymentServices\Braintree\Address\AddressRequestDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionRequestDTO;

interface BraintreePaymentInterface
{
    public function createBraintreeCustomer($userId);

    public function createBraintreePaymentMethod(PaymentMethodRequestDTO $paymentMethodRequestDTO,$userId);

    public function createBraintreeSubscription(SubscriptionRequestDTO $subscriptionRequestDTO,$userId);

    public function cancelBraintreeSubscription($userId);

    public function createBraintreeCustomerOwner($userId);
    public function createBraintreePaymentMethodOwner(PaymentMethodRequestDTO $paymentMethodRequestDTO,$userId);
    public function deleteBraintreePaymentMethodOwner($token);

    
}
