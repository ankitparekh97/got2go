<?php


namespace App\Repositories;


use App\Http\DTO\Api\PaymentCardDTO;

interface PaymentCardInterface
{
    /**
     * @param PaymentCardDTO $paymentCardDTO
     * @param $userId
     * @return mixed
     */
    public function savePaymentCard(PaymentCardDTO $paymentCardDTO, $userId);

    public function cardDetails($userType,$rentalUserId);

    public function paymentMethodCredit($params);

    public function paymentMethodObject($params);

    public function saveCard(PaymentCardDTO $paymentCardDTO);

    public function deletePaymentCard($id);
    
    public function getPaymentCardDetails($userId);
    public function getPaymentCardDetailById($id);
}
