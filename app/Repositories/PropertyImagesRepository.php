<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PropertyMediaDTO;
use App\Models\PropertyImages;

class PropertyImagesRepository implements PropertyImagesInterface
{
    public $propertyImagesModel;

    public function __construct(
        PropertyImages $propertyImagesModel
    )
    {
        $this->propertyImagesModel = $propertyImagesModel;
    }


    public function getPropertyImagesByParamsCollection($params)
    {
         /**
         * @var PropertyImages $propertyImages
         */

        $propertyImages = (new $this->propertyImagesModel)::query();
        $propertyImages->where($params);

        return $propertyImages->get();
    }

    public function getPropertyImagesByParamsObject($params)
    {
         /**
         * @var PropertyImages $propertyImages
         */

        $propertyImages = (new $this->propertyImagesModel)::query();
        $propertyImages->where($params);

        return $propertyImages->first();
    }

    public function saveMedia($params)
    {
        $propertyImages = PropertyImages::insert($params);
        return $propertyImages;
    }

    public function deletePropertyImages($params){
        /**
         * @var PropertyImages $propertyImages
         */

        $propertyImages = PropertyImages::whereIn('id',$params)->delete();
        if($propertyImages){
            return true;
        }
    }
}
