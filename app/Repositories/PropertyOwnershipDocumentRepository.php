<?php


namespace App\Repositories;

use App\Models\PropertyOwnershipDocuments;

Class PropertyOwnershipDocumentRepository  implements PropertyOwnershipDocumentInterface
{
    public function viewDocument($type,$id){
        if($type == 'property'){
            $isGovtId = NULL;
        }else{
            $isGovtId = 1;
        }
        $Doc = PropertyOwnershipDocuments::where('property_id',$id)->where('is_govt_id',$isGovtId)->get()->toArray();
        return $Doc;
    }

}
