<?php


namespace App\Repositories;

use App\Http\DTO\Api\PaymentHistoryDTO;

interface PaymentSavingAccountsInterface
{

    public function paymentMethod($params);

    public function paymentMethodObject($params);

    public function saveCard(PaymentHistoryDTO $paymentHistoryDTO);

    public function deletepaymentHistory($id);
}
