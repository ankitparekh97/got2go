<?php


namespace App\Repositories;
use App\Models\TripboardShare;

class TripBoardSharedRepository implements TripBoardSharedInterface
{
    public function checkForSharedTripBoard($id, $userId)
    {
        return TripboardShare::where('tripboard_id',$id)->where('user_id',$userId)->first();
    }

    public function saveSharedTripBoard($tripboardId, $userId)
    {
        $loggedinUserId = $userId;
        $result = TripboardShare::create([
            'user_id' => $loggedinUserId,
            'tripboard_id' =>$tripboardId
        ]);
        return $result;
    }
}
