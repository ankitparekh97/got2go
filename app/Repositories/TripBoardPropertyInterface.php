<?php


namespace App\Repositories;

interface TripBoardPropertyInterface
{
   

    public function addPropertyToTripboard($tripboardId, $propertyId);
    public function AddOrRemovePropertiesFromTripboard($param);
    public function tripBoardRemoveListing($param);

}
