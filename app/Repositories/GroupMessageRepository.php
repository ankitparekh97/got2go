<?php


namespace App\Repositories;

use App\Models\GroupMessages;

class GroupMessageRepository implements GroupMessageInterface
{

    public function __construct(
        GroupMessages $groupMessagesModel
    )
    {
        $this->groupMessagesModel = $groupMessagesModel;
    }

    public function create($params){

        $groupMessages = (new $this->groupMessagesModel);
        return $groupMessages::create($params);
    }
}
