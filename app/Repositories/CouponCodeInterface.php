<?php


namespace App\Repositories;


use App\Http\DTO\Api\CouponCodeDTO;

interface CouponCodeInterface
{
    public function getAllCoupons();

    public function saveCoupon(CouponCodeDTO  $couponCodeDTO);

    public function findCouponCodeByName($couponCodeName);

    public function findCouponCodeById($couponCodeId);
}
