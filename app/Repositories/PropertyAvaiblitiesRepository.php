<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Models\PropertyAvaiblities;
use DB;
use Illuminate\Support\Carbon;

class PropertyAvaiblitiesRepository implements PropertyAvablityInterface
{
    public $propertyAvaiblitiesModel;

    public function __construct(
        PropertyAvaiblities $propertyAvaiblitiesModel
    )
    {
        $this->propertyAvaiblitiesModel = $propertyAvaiblitiesModel;
    }


    public function getPropertyAvaiblitiesByParamsCollection($params)
    {
         /**
         * @var PropertyAvaiblities $propertyAvaiblitiesModel
         */

        $propertyAvaiblitiesModel = (new $this->propertyAvaiblitiesModel)::query();
        $propertyAvaiblitiesModel->where($params);

        return $propertyAvaiblitiesModel->get();
    }

    public function getPropertyAvaiblitiesByParamsObject($params)
    {
         /**
         * @var PropertyAvaiblities $propertyAvaiblitiesModel
         */

        $propertyAvaiblitiesModel = (new $this->propertyAvaiblitiesModel)::query();
        $propertyAvaiblitiesModel->where($params);

        return $propertyAvaiblitiesModel->first();
    }

	public function savePropertyAvaiblities($avaiblities)
    {
        return PropertyAvaiblities::insert($avaiblities);
    }

    public function deletePropertyAvaiblities($propertyId){
        /**
         * @var PropertyAvaiblities $propertyAvaiblitiesModel
         */

        $propertyAvaiblitiesModel = PropertyAvaiblities::where('property_id', $propertyId)->delete();
        if($propertyAvaiblitiesModel){
            return true;
        }
    }

}
