<?php

namespace App\Repositories;

use App\Http\DTO\Api\AvaiblitiesDTO;

interface PropertyAvablityInterface
{
    public function getPropertyAvaiblitiesByParamsCollection($params);

    public function getPropertyAvaiblitiesByParamsObject($params);

    public function savePropertyAvaiblities($avaiblities);

    public function deletePropertyAvaiblities($propertyId);
}
