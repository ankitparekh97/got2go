<?php

namespace App\Repositories;

use App\Http\DTO\Api\HotelsDTO;

interface HotelsInterface
{
    public function getHotels();

    public function getHotelsByParamsCollection($params);

    public function getHotelsByParamsObject($params);

    public function saveHotel(HotelsDTO $hotelsDTO);
}
