<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\AmenitiesDTO;
use App\Models\PropertyAmenities;


class PropertyAmenitiesRepository implements PropertyAmenitiesInterface
{
    public $propertyAmenitiesModel;

    public function __construct(
        PropertyAmenities $propertyAmenitiesModel
    )
    {
        $this->propertyAmenitiesModel = $propertyAmenitiesModel;
    }

    public function getPropertyAmenitiesByParamsCollection($params)
    {
         /**
         * @var PropertyAmenities $propertyAmenitiesModel
         */

        $propertyAmenitiesModel = (new $this->propertyAmenitiesModel)::query();
        $propertyAmenitiesModel->where($params);

        return $propertyAmenitiesModel->get();
    }

    public function getPropertyAmenitiesByParamsObject($params)
    {
         /**
         * @var PropertyAmenities $propertyAmenitiesModel
         */

        $propertyAmenitiesModel = (new $this->propertyAmenitiesModel)::query();
        $propertyAmenitiesModel->where($params);

        return $propertyAmenitiesModel->first();
    }

	public function saveAmenities($amenities)
    {
        return PropertyAmenities::insert($amenities);
    }

    public function deletePropertyAmenities($propertyId){
        /**
         * @var PropertyAmenities $propertyAmenitiesModel
         */

        $propertyAmenitiesModel = PropertyAmenities::where('property_id', $propertyId)->delete();
        if($propertyAmenitiesModel){
            return true;
        }
    }

}
