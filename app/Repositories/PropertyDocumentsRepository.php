<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PropertyVerificationDTO;
use App\Models\PropertyOwnershipDocuments;


class PropertyDocumentsRepository implements PropertyDocumentsInterface
{
    public $propertyOwnershipDocumentsModel;

    public function __construct(
        PropertyOwnershipDocuments $propertyOwnershipDocumentsModel
    )
    {
        $this->propertyOwnershipDocumentsModel = $propertyOwnershipDocumentsModel;
    }


    public function getPropertyDocumentsByParamsCollection($params)
    {
         /**
         * @var PropertyOwnershipDocuments $propertyOwnershipDocumentsModel
         */

        $propertyOwnershipDocument = (new $this->propertyOwnershipDocumentsModel)::query();
        $propertyOwnershipDocument->where($params);

        return $propertyOwnershipDocument->get();
    }

    public function getPropertyDocumentsByParamsObject($params)
    {
         /**
         * @var PropertyOwnershipDocuments $propertyOwnershipDocumentsModel
         */

        $propertyOwnershipDocument = (new $this->propertyOwnershipDocumentsModel)::query();
        $propertyOwnershipDocument->where($params);

        return $propertyOwnershipDocument->first();
    }

    public function savePropertyDocuments($params)
    {
        /**
         * @var PropertyOwnershipDocuments $propertyVerification
         */

        $docs = PropertyOwnershipDocuments::insert($params);
        return $docs;
    }


    public function deletePropertyDocuments($params){
        /**
         * @var PropertyOwnershipDocuments $propertyOwnershipDocumentsModel
         */

        $propertyOwnershipDocumentsModel = PropertyOwnershipDocuments::whereIn('id',$params)->delete();
        if($propertyOwnershipDocumentsModel){
            return true;
        }
    }
}
