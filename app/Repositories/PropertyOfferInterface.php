<?php


namespace App\Repositories;


use App\Models\DTO\PropertyOfferDTO;
use App\Models\PropertyOffers;

interface PropertyOfferInterface
{
    public function getAll();

    public function savePropertyOffer(PropertyOfferDTO $propertyOfferDTO);

    public function getPropertyOfferById($propertyOfferId);

    public function getPropertyOffersByUserId($userId,$userType);

    public function softDeletePreviousOffersBatch($propertyId,$ownerId,$rentalId);

    public function softDeleteOfferById($propertyOfferId);

    public function softDeleteOtherOverLappingOffers($propertyOfferId);

    public function checkOfferDateIsOverLappingByTripperId($startDate,$endDate,$tripperId,$propertyId);

    public function checkOfferDateIsOverLappingByTripperIdAndOtherOffer($startDate,$endDate,$tripperId,$propertyId,$propertyOfferId);

    public function getPendingResponseOnOfferByOwner($ownerId);

    public function getPendingResponseOnOfferByTripper($tripperId);

}
