<?php


namespace App\Repositories;

use App\Models\Subscription;

interface SubscriptionInterface
{
    public function getSubscription();

    public function getSubscriptionById($subscriptionId);

    public function updateSubscriptionDetails($subscriptionId,$annualSubscriptionAmount,$discountPercentage,$strikeThroughAmount);
}
