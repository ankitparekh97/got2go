<?php

namespace App\Repositories;

use App\Http\DTO\Api\AmenitiesDTO;

interface PropertyAmenitiesInterface
{

    public function getPropertyAmenitiesByParamsCollection($params);

    public function getPropertyAmenitiesByParamsObject($params);

    public function saveAmenities($amenities);

    public function deletePropertyAmenities($propertyId);
}
