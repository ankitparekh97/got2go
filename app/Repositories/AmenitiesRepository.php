<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\AmenitiesDTO;
use App\Models\DTO\PropertyDTO;
use App\Models\MasterAmenities;
use DB;
use Illuminate\Support\Carbon;

class AmenitiesRepository implements AmenitiesInterface
{
    public $amentiesModel;

    public function __construct(
        MasterAmenities $amentiesModel
    )
    {
        $this->amentiesModel = $amentiesModel;
    }

    public function getMasterAmenities(){

         /**
         * @var MasterAmenities $amenities
         */

        $amenity = (new $this->amentiesModel)::query();
        $amenity->orderBy(MasterAmenities::NAME,'DESC');
        $amenties = $amenity->get();

        return $amenties;
    }

    public function getMasterAmenitiesByArray(){

        /**
        * @var MasterAmenities $amenities
        */

       return MasterAmenities::get()->pluck('amenity_name','id')->toArray();
   }

}
