<?php

namespace App\Repositories;

use App\Http\DTO\Api\PropertyMediaDTO;

interface PropertyImagesInterface
{
    public function getPropertyImagesByParamsCollection($params);

    public function getPropertyImagesByParamsObject($params);

    public function saveMedia($params);

    public function deletePropertyImages($params);
}
