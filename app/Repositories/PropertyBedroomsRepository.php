<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Models\PropertyBedrooms;

class PropertyBedroomsRepository implements PropertyBedroomsInterface
{
    public $propertyBedRoomsModel;

    public function __construct(
        PropertyBedrooms $propertyBedRoomsModel
    )
    {
        $this->propertyBedRoomsModel = $propertyBedRoomsModel;
    }


    public function getPropertyBedroomsByParamsCollection($params)
    {
         /**
         * @var PropertyBedrooms $propertyBedRoomsModel
         */

        $propertyBedRooms = (new $this->propertyBedRoomsModel)::query();
        $propertyBedRooms->where($params);

        return $propertyBedRooms->get();
    }

    public function getPropertyBedroomsByParamsObject($params)
    {
         /**
         * @var PropertyBedrooms $propertyBedRoomsModel
         */

        $propertyBedRooms = (new $this->propertyBedRoomsModel)::query();
        $propertyBedRooms->where($params);

        return $propertyBedRooms->first();
    }

    public function savePropertyBedrooms($data)
    {
        return PropertyBedrooms::insert($data);
    }

    public function deletePropertyBedrooms($propertyId){
        /**
         * @var PropertyBedrooms|Builder $propertyBedRoomsModel
         */

        $propertyBedRooms = PropertyBedrooms::where('property_id', $propertyId)->delete();
        if($propertyBedRooms){
            return true;
        }
   }
}
