<?php


namespace App\Repositories;

use App\Models\FavTripboards;
use DB;
use Illuminate\Support\Carbon;

class FavrioteTripboardRepository implements FavrioteTripboardInterface
{

    public $favTripboardsModel;

    public function __construct(
        FavTripboards $favTripboardsModel
    )
    {
        $this->favTripboardsModel = $favTripboardsModel;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getFavTriboards($userId)
    {
        return FavTripboards::with('property')
                    ->where('favourite_tripboard.user_id',$userId)
                    ->get();
    }

    /**
     * @param $userId
     * @param $propertyId
     * @return mixed
     */
    public function getFavTriboardsWithPropertyId($userId,$propertyId)
    {
        return FavTripboards::where(['user_id'=>$userId,'property_id'=>$propertyId]);
    }

     /**
     * @param $favTripboard
     * @return mixed
     */
    public function removePropertyFromFav($favTripboard)
    {
        return FavTripboards::where($favTripboard)->delete();
    }


    public function getFavouriteTripBoards($userId)
    {
        return FavTripboards::where('favourite_tripboard.user_id',$userId)->groupBy('favourite_tripboard.user_id')->get();
    }


    public function likeUnlikeProperties($param)
    {
        if(!FavTripboards::where($param)->exists()){
            FavTripboards::insert($param);
            return response()->json(['success'=>true,'data'=>"Property Added to Favourite."]);
        }else{
            FavTripboards::where($param)->delete();
            return response()->json(['success'=>true,'data'=>"Property removed from Favourite."]);
        }
    }
}
