<?php


namespace App\Repositories;

use App\Models\BookingPayment;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\BookingPaymentDTO;
use DB;
use Illuminate\Support\Carbon;

class BookingPaymentRepository implements BookingPaymentInterface
{

    public $bookingPaymentModel;

    public function __construct(
        BookingPayment $bookingPaymentModel
    )
    {
        $this->bookingPaymentModel = $bookingPaymentModel;
    }

    public function bookingHistory(){

        /**
         * @var BookingPayment $bookingPaymentModel
         */

        $select = [
            'booking_payment.*',
            'property.id as propertyId',
            'property.title',
            'rental_user.first_name',
            'rental_user.last_name',
            'property.price',
            'booking.check_in_date',
            'booking.check_out_date',
            'rental_user.is_tripper',
            'rental_user.is_verified',
            'property.city',
            'property.state',
            'property.zipcode',
            'property.cleaning_fee',
            'booking.actual_price',
            'owner_rental_pair.id as messageid'
        ];

        $bookingPayment = (new $this->bookingPaymentModel)::query();

        $bookingPayment->select($select, '*');
        $bookingPayment->join('booking', 'booking.id', '=', 'booking_payment.booking_id');
        $bookingPayment->join('property','property.id', '=','booking.property_id');
        $bookingPayment->join('rental_user','booking.rental_user_id', '=' ,'rental_user.id');
        $bookingPayment->join('owner','property.owner_id', '=','owner.id');

        $bookingPayment->join('owner_rental_pair', function($query) {
            $query->on('owner_rental_pair.rental_id', '=' ,'rental_user.id')
                ->where('owner_rental_pair.owner_id', getOwnerId());
        });

        $bookingPayment->whereHas('booking.property', function ($query) {
            $query->where(['owner_id' => getOwnerId()]);
        });

        $bookingPayment->whereHas('booking.property', function ($query) {
            $query->where(['owner_id' => getOwnerId()]);
        });

        $bookingPayment->orderBy('booking_payment.id', GlobalConstantDeclaration::ORDER_BY_DESC);
        $bookingPayment->with(['booking']);

        $bookingHistory = $bookingPayment->get();
        return $bookingHistory;
    }

    public function getByParamsCollection($params)
    {
        $bookingPaymentModel = (new $this->bookingPaymentModel)::query();
        $bookingPaymentModel->where($params);

        $bookingPaymentModel->with(['booking']);
        return $bookingPaymentModel->get();
    }

    public function getByParamsObject($params)
    {
        $bookingPaymentModel = (new $this->bookingPaymentModel)::query();
        $bookingPaymentModel->where($params);

        $bookingPaymentModel->with(['booking']);
        return $bookingPaymentModel->first();
    }

    /**
     * @param $bookingDetails
     * @return mixed
     */
    public function createBookingPayment($bookingPaymentDetails)
    {
        return BookingPayment::create([
                        'booking_id' => $bookingPaymentDetails->bookingId,
                        'rental_user_id' => $bookingPaymentDetails->renterUserId,
                        'payment_type' => $bookingPaymentDetails->paymentType,
                        'transaction_number' => $bookingPaymentDetails->transactionNumber,
                        'amount' => $bookingPaymentDetails->amount,
                        'refund_amount' =>(isset($bookingPaymentDetails->refundAmount))?$bookingPaymentDetails->refundAmount:'',
                        'created_at' => $bookingPaymentDetails->createdAt,
                        'updated_at' => $bookingPaymentDetails->updatedAt,
                    ]);
    }

    public function saveBookingPayment(BookingPaymentDTO $bookingPaymentDTO){

        $bookingPaymentModel = (new $this->bookingModel);
        if($bookingPaymentDTO->id){
            $bookingPaymentModel = $this->getByParamsObject(array('id'=>$bookingPaymentDTO->id));
        }

        $bookingPaymentModel->owner_status = !empty($bookingPaymentDTO->ownerStatus) ? $bookingPaymentDTO->ownerStatus : $bookingPaymentModel->owner_status;
        $bookingPaymentModel->is_cancelled = isset($bookingPaymentDTO->isCancelled) ? $bookingPaymentDTO->status : $bookingPaymentModel->is_cancelled;
        $bookingPaymentModel->cancellation_date = !empty($bookingPaymentDTO->cancelledDate) ? $bookingPaymentDTO->cancelledDate : $bookingPaymentModel->cancellation_date;
        $bookingPaymentModel->confirmation_id = !empty($bookingPaymentDTO->confirmationId) ? $bookingPaymentDTO->confirmationId : $bookingPaymentModel->confirmation_id;
        $bookingPaymentModel->created_at = !empty($bookingPaymentDTO->createdAt) ? $bookingPaymentDTO->createdAt : $bookingPaymentModel->created_at;
        $bookingPaymentModel->updated_at = !empty($bookingPaymentDTO->updatedDate) ? $bookingPaymentDTO->updatedDate : $bookingPaymentModel->updated_at;
        $bookingPaymentModel->save();

        return $bookingPaymentModel;
    }

    public function getTransactionLogs(){
        $transactionLogs = DB::table('booking_payment')
        ->join('booking', 'booking.id', '=', 'booking_payment.booking_id')
        ->join('property','property.id', '=','booking.property_id')
        ->join('rental_user','booking.rental_user_id', '=','rental_user.id')
        ->join('owner','property.owner_id', '=','owner.id')
        ->select(
            'booking_payment.*',
            'booking.check_in_date as check_in_date',
            'booking.check_out_date as check_out_date',
            'booking.status as status',
            'property.title as title',
            'property.id as propertyId',
            'property.city as city',
            'property.state as state',
            'rental_user.first_name as rentalUserFirstName',
            'rental_user.last_name as rentalUserLastName',
            'owner.first_name as ownerUserFirstName',
            'owner.last_name as ownerUserLastName',
            'booking_payment.payment_type as paymentType',
            'booking_payment.amount as refundAmount'
        )
        ->orderBy('booking_payment.id','desc')
        ->get();

        return $transactionLogs;
    }

    public function getBookingPaymentWithId($id=null,$bookingId=null){
        if($id){
            return BookingPayment::with('booking','booking.property')->find($id);
        }
        if($bookingId){
            return BookingPayment::with('booking','booking.property')->where('booking_id',$bookingId)->first();
        }
        
    }

    public function insertBookingPayment($input){
        return BookingPayment::insert($input);
    }

    public function getLatestBookingPayment($bookingId)
    {
       return  BookingPayment::select('booking_payment.payment_type as paymentType','created_at')->where('booking_id',$bookingId)->latest()->first();
    }
}










