<?php

namespace App\Repositories;

interface PropertyTypeInterface
{
    public function getPropertyTypes();

    public function getPropertyTypesByParams();
}
