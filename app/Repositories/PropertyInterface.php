<?php

namespace App\Repositories;

use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;

interface PropertyInterface
{
    public function getPropertyDetailsById($propertyId);

    public function getPropertyByOwnerId($ownerId,$status);

    public function getByParamsObject($params);

    public function getByParamsCollection($params);

    public function findPropertyById($Id);

    public function getPropertyByIds($ids);

    public function getPropertyByUniqueKey($uniqueId);

    public function publishProperty($id);

    public function deleteProperty($id);

    public function createProperty($property);
    public function updateProperty($id,$data);
    public function timeShareList();
    public function bulkTimeshareList();
    public function propertyApprove($input);
    public function propertyApproveAll($input);
    public function generateQuery($input);
    public function selectDataFromQuery($input, $isRentalUser=false);
    public function searchProperty($q, $selectArr, $input, $isRentalUser = false);
    public function searchPropertyCount($input);
    public function getHotDeals($input);
    public function getHotDealsByLimit();
    public function getownerPropertyById($id);
    public function similarListing($tripboard);
    public function getTripBoardPropertyImages($id);
    public function propertyAvailablity($id,$checkInDate,$checkOutDate);
}
