<?php


namespace App\Repositories;


use App\Http\DTO\Api\OwnerUserDTO;
use App\Http\DTO\Api\PaymentCardDTO;
use App\Models\PaymentCards;
use App\Models\Owner;
use App\Models\Property;
use App\Services\PaymentServices\Braintree\BraintreePaymentService;
use App\Services\PaymentServices\Braintree\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Hash;
class OwnerRepository implements OwnerInterface,PaymentCardInterface
{
    private $owner,$paymentCards;

    public const OWNER_STATUS_ACTIVE = 'active';
    public const OWNER_STATUS_CANCELED = 'canceled';

    use PaymentCardRepository;
    use BraintreePaymentRepository;


    public function __construct(
        Owner $owner,
        PaymentCards $paymentCards
    )
    {
        $this->owner = $owner;
        $this->paymentCards = $paymentCards;
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $id
     * @return Owner|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOwnerById($id)
    {
        /**
         * @var Owner $ownerUser
         */
        $ownerUser = (new $this->owner)::query();
        $ownerUser->where('id',$id);
        return $ownerUser->first();
    }

    /**
     * @param $email
     * @param $password
     * @return Owner|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function updateUserPassword($email,$password)
    {
        /**
         * @var OwnerUser $owner
         */
        $owner = (new $this->owner())::query();
        $owner->where('email',$email)->first();
        $owner->password = $password;
        $owner->save();
        return $owner;
    }


    public function createUser($ownerArray){
        return Owner::create([
            'first_name' => $ownerArray->firstName,
            'last_name' => $ownerArray->lastName,
            'phone' => (isset($ownerArray->phone))?$ownerArray->phone:null,
            'email' => $ownerArray->email,
            'password' => $ownerArray->password,
            'is_otp_verified' => $ownerArray->isOtpVerified,
            'unique_key' => $ownerArray->uniqueKey,
            'full_name'=>$ownerArray->firstName.' '.$ownerArray->lastName,
            'google_id'=>(isset($ownerArray->googleId))?$ownerArray->googleId:null,
            'facebook_id'=>(isset($ownerArray->facebookId))?$ownerArray->facebookId:null
        ]);
    }
    public function userList(){
        $list = Owner::Join('property', 'owner.id', '=', 'property.owner_id','left outer')
            ->select('owner.*','property.id as propertyId', DB::raw("count(property.owner_id) as total_property"))
            ->groupBy('owner.id')
            ->orderBy('owner.id')
            ->whereRaw('property.deleted_at is null')
            ->get();
        return $list;
    }

    public function userActivate($userId,$propertyId,$status){
        $owners = Owner::find($userId);
        $owners->status = $status;
        $owners->save();

        $pro_status = '';
        if($status == 'decline'){
            $pro_status = 'rejected';
        }
        if($pro_status != '' && $propertyId != ''){
            $property = Property::find($propertyId);
            if($property)
            {
                $property->status = $pro_status;
                $property->save();
            }
            
        }
        return true;
    }

/**
     * @param Owner $owner
     * @param null $id
     * @return mixed
     * @throws \Throwable
     */
    public function save(OwnerUserDTO $ownerDTO)
    {
        /**
         * @var Owner $ownerModel
         */

        $ownerModel = (new $this->owner);

        if($ownerDTO->id){
            $ownerModel = $this->getOwnerById($ownerDTO->id);
        }

        $ownerModel->customer_id = isset($ownerDTO->customerId) ? $ownerDTO->customerId : $ownerModel->customer_id;
        $ownerModel->stripe_account_id = isset($ownerDTO->stripeAcountId) ? $ownerDTO->stripeAcountId : $ownerModel->stripe_account_id;
        $ownerModel->save();

        return $ownerModel->id;
    }
    public function getOwnerByGoogleId($googleId,$email){
        return Owner::where('google_id', $googleId)->orWhere('email',$email)->first();
    }

    public function updateOwnerUserSetting($input){
        
        $id = $input['id'];
        $owner = $this->getOwnerById($id);
        $owner->first_name = $input['firstname'];
        $owner->last_name = $input['lastname'];
        $owner->birthdate = Carbon::parse($input['dateofbirth'])->format('Y-m-d');
        if($input['updatepassword']) {
            $password = Hash::make($input['updatepassword']);
            $owner->password = $password;
        }
        $owner->phone = $input['phonenumber'];
        
        return $owner->save();
    }

}
