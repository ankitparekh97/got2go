<?php


namespace App\Repositories;

use App\Models\Configuration;

interface CommunicationPreferencesInterface
{
    public function createCommunicationPreferences($prefernces);
    public function deleteCommunicationPreferencesByUserId($userId);
}
