<?php


namespace App\Repositories;


use App\Http\DTO\Api\RentalUserDTO;
use App\Models\RentalUserSubscription;
use App\Models\PaymentCards;
use App\Models\RentalUser;
use App\Models\Owner;
use App\Services\PaymentServices\PaymentServiceInterface;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Hash;
class RentalRepository implements RentalInterface,PaymentCardInterface
{
    private $rentalUser,$paymentCards,$rentalUserSubscription;

    public $paymentService;

    public const TRIPPER_STATUS_ACTIVE = 'active';
    public const TRIPPER_STATUS_PAUSED = 'paused';
    public const TRIPPER_STATUS_CANCELED = 'canceled';

    use PaymentCardRepository;
    use RentalUserSubscriptionRepository;
    use PaymentRepository;

    public function __construct(
        RentalUser  $rentalUser,
        PaymentCards $paymentCards,
        RentalUserSubscription $rentalUserSubscription,
        PaymentServiceInterface $paymentService,
        Owner  $owner
    )
    {
        $this->rentalUser = $rentalUser;
        $this->paymentCards = $paymentCards;
        $this->rentalUserSubscription = $rentalUserSubscription;
        $this->paymentService = $paymentService;
        $this->owner = $owner;
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param $id
     * @return RentalUser|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getById($id)
    {
        /**
         * @var RentalUser $rentalUser
         */
        $rentalUser = (new $this->rentalUser())::query();
        $rentalUser->where('id',$id);
        return $rentalUser->first();
    }

    /**
     * @param $id
     * @return Owner|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getOwnerById($id)
    {
        /**
         * @var RentalUser $rentalUser
         */
        $owner = (new $this->owner())::query();
        $owner->where('id',$id);
        return $owner->first();
    }

    /**
     * @param RentalUserDTO $rentalUserDTO
     * @param null $userId
     * @return mixed
     * @throws \Throwable
     */
    public function saveRenterUser(
        RentalUserDTO $rentalUserDTO,
        $userId = null
    )
    {
        /**
         * @var RentalUser $rentalUser
         */
        if($userId){
            $rentalUser = $this->getById($userId);
        }
        else{
            $rentalUser = new RentalUser();
        }

        return DB::transaction(function () use(&$rentalUserDTO,&$rentalUser,&$userId){
            $rentalUser->first_name = $rentalUserDTO->firstName ?? $rentalUser->first_name;
            $rentalUser->last_name = $rentalUserDTO->lastName ?? $rentalUser->last_name;
            $rentalUser->full_name = $rentalUser->first_name.' '.$rentalUser->last_name;
            $rentalUser->google_id = $rentalUserDTO->googleId ?? $rentalUser->google_id;
            $rentalUser->facebook_id = $rentalUserDTO->facebookId ?? $rentalUser->facebook_id;
            $rentalUser->email = $rentalUserDTO->email ?? $rentalUser->email;
            $rentalUser->bio = $rentalUserDTO->bio ?? $rentalUser->bio;
            $rentalUser->is_otp_verified = $rentalUserDTO->isOtpVerified ?? $rentalUser->is_otp_verified;
            $rentalUser->birthdate = $rentalUserDTO->birthdate ?? $rentalUser->birthdate;
            $rentalUser->gender = $rentalUserDTO->gender ?? $rentalUser->gender;
            $rentalUser->phone = $rentalUserDTO->phoneNumber ?? $rentalUser->phone;
            $rentalUser->emergency_number = $rentalUserDTO->emergencyNumber ?? $rentalUser->emergency_number;
            $rentalUser->address = $rentalUserDTO->address ?? $rentalUser->address;
            $rentalUser->city = $rentalUserDTO->city ?? $rentalUser->city;
            $rentalUser->state = $rentalUserDTO->state ?? $rentalUser->state;
            $rentalUser->zip = $rentalUserDTO->zip ?? $rentalUser->zip;
            $rentalUser->site_language = $rentalUserDTO->siteLanguage ?? $rentalUser->site_language;
            $rentalUser->currency = $rentalUserDTO->currency ?? $rentalUser->currency;
            $rentalUser->status = $rentalUserDTO->status ?? $rentalUser->status;
            $rentalUser->photo = $rentalUserDTO->photo ?? $rentalUser->photo;
            $rentalUser->goverment_id_doc = $rentalUserDTO->governmentIdDoc ?? $rentalUser->goverment_id_doc;
            $rentalUser->share_on_fb = $rentalUserDTO->shareOnFb ?? $rentalUser->share_on_fb;
            $rentalUser->marketing_email = $rentalUserDTO->marketingEmail ?? $rentalUser->marketing_email;
            $rentalUser->is_verified = $rentalUserDTO->isVerified ?? $rentalUser->is_verified;
            $rentalUser->is_tripper = $rentalUserDTO->isTripper ?? $rentalUser->is_tripper;
            $rentalUser->is_subscribed = $rentalUserDTO->isSubscribed ?? $rentalUser->is_subscribed;
            $rentalUser->tripper_status = $rentalUserDTO->tripperStatus ?? $rentalUser->tripper_status;
            $rentalUser->customer_id = $rentalUserDTO->customerId ?? $rentalUser->customer_id;

            $rentalUser->save();
            //$this->saveAsOwner($rentalUserDTO,$userId);


            return $rentalUser;
        });
    }

    /**
     * @param RentalUserDTO $rentalUserDTO
     * @param $userId
     * @return RentalUser
     * @throws \Throwable
     */
    public function saveAsTripper(
        RentalUserDTO $rentalUserDTO,
        $userId
    )
    {
        /**
         * @var RentalUser $user
         */
        $rentalUserDTO->isTripper = true;
        $rentalUserDTO->isSubscribed = true;
        $rentalUserDTO->tripperStatus = self::TRIPPER_STATUS_ACTIVE;
        $user = $this->saveRenterUser($rentalUserDTO,$userId);
        return $user;
    }

    /**
     * @param RentalUserDTO $rentalUserDTO
     * @param $userId
     * @return Owner
     * @throws \Throwable
     */
    public function saveAsOwner(
        RentalUserDTO $rentalUserDTO,
        $userId=null
    )
    {
        /**
         * @var Owner $user
         */
         if($userId){
            $owner = $this->getOwnerById($userId);
        }
        else{
            $owner = new Owner();
        }

        $owner->first_name = $rentalUserDTO->firstName ?? $owner->first_name;
        $owner->last_name = $rentalUserDTO->lastName ?? $owner->last_name;
        $owner->username = $owner->first_name.' '.$owner->last_name;
        $owner->google_id = $rentalUserDTO->googleId ?? $owner->google_id;
        $owner->facebook_id = $rentalUserDTO->facebookId ?? $owner->facebook_id;
        $owner->email = $rentalUserDTO->email ?? $owner->email;
        $owner->bio = $rentalUserDTO->bio ?? $owner->bio;
        $owner->is_otp_verified = $rentalUserDTO->isOtpVerified ?? $owner->is_otp_verified;
        $owner->birthdate = $rentalUserDTO->birthdate ?? $owner->birthdate;
        $owner->phone = $rentalUserDTO->phoneNumber ?? $owner->phone;
        $owner->status = $rentalUserDTO->status ?? $owner->status;
        $owner->save();
        return $owner;
    }

    /**
     * @param $userId
     * @return RentalUser
     * @throws \Throwable
     */
    public function tripperToNormalRental($userId)
    {
        $rentalUserDTO = new RentalUserDTO;

        /**
         * @var RentalUser $user
         */
        $rentalUserDTO->isTripper = false;
        $rentalUserDTO->isSubscribed = false;
        $rentalUserDTO->tripperStatus = self::TRIPPER_STATUS_PAUSED;
        $user = $this->saveRenterUser($rentalUserDTO,$userId);
        return $user;
    }

    public function getRentalUserSubscriptionExpireByDate(
        Carbon $date
    )
    {

        /**
         * @var RentalUser|Builder $user
         */
        $user = (new $this->rentalUser())::query();
        $user->whereHas('subscription',function ($subscriptionQry) use($date) {

            /**
             * @var RentalUserSubscription $subscriptionQry
             */
            $subscriptionQry->whereDate('billing_period_end_date',$date);
            $subscriptionQry->where('status','<>', 'active');

        });

        $user->with(['subscription' => function ($subscriptionQry) use($date) {

            /**
             * @var RentalUserSubscription $subscriptionQry
             */
            $subscriptionQry->whereDate('billing_period_end_date',$date);
            $subscriptionQry->where('status','<>', 'active');

        }]);

        return $user->get();
    }

    public function savePaymentCustomerId($paymentCustomerId,$userId)
    {
        /**
         * @var RentalUser $rentalUser
         */
        $rentalUser = $this->getById($userId);
        $rentalUser->customer_id = $paymentCustomerId;
        $rentalUser->save();
        return $rentalUser;
    }

    public function getTripperList(){
        return RentalUser::Join('booking', 'rental_user.id', '=', 'booking.rental_user_id','left outer')
            ->select('rental_user.*', DB::raw("count(booking.rental_user_id) as total_booking"))
            ->where('is_tripper',1)
            ->groupBy('rental_user.id')
            ->orderBy('rental_user.id')
            ->get();
    }

    /**
     * @param $userId
     * @param $subscriptionAction
     * @return RentalUser
     * @throws \Throwable
     */
    public function updateSubscriptionStatus($rentalUserId,$subscriptionAction)
    {
        /**
         * @var RentalUser $rentalUser
         */
        $rentalUser = $this->getById($rentalUserId);
        $rentalUser->tripper_status = $subscriptionAction;
        $rentalUser->save();
        return $rentalUser;
    }

    /**
     * @param $email
     * @return RentalUser|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function updateUserPassword($email,$password)
    {
        /**
         * @var RentalUser $rentalUser
         */
        $rentalUser = (new $this->rentalUser())::query();
        $rentalUser->where('email',$email)->first();
        $rentalUser->password = $password;
            $rentalUser->save();
        return $rentalUser;
    }

    public function checkUser($email,$phone){
        $query =  (new $this->rentalUser())::query();
        if($email && $phone){
            $query->where('email',$email)->orWhere('phone',$phone);
        }
        else if($email){
            $query->where('email', $email);
        }
        else if($phone){
            $query->where('phone',$phone);
        }

        $rentalUser = $query->first();
        return $rentalUser;
    }

    public function createUser($rentalUserArray){
        return RentalUser::create([
            'first_name' => $rentalUserArray->firstName,
            'last_name' => $rentalUserArray->lastName,
            'email' => $rentalUserArray->email,
            'password' => $rentalUserArray->password,
            'activation_url' => $rentalUserArray->activationUrl,
            'is_otp_verified' => $rentalUserArray->isOtpVerified,
            'is_verified' => $rentalUserArray->isVerified,
            'phone' => (isset($rentalUserArray->phone))?$rentalUserArray->phone:null,
            'full_name'=>$rentalUserArray->firstName.' '.$rentalUserArray->lastName,
            'google_id'=>(isset($rentalUserArray->googleId))?$rentalUserArray->googleId:null,
            'facebook_id'=>(isset($rentalUserArray->facebookId))?$rentalUserArray->facebookId:null
        ]);
    }

    public function getRentalByGoogleId($googleId,$email){
        return RentalUser::with('paymentcards_user')->where(['google_id'=>$googleId,'email'=>$email])->first();
    }

    public function updateRentalUserSetting($input)
    {
        $id = $input['id'];
        $rentalUser = $this->getById($id);
        $rentalUser->first_name = $input['firstname'];
        $rentalUser->last_name = $input['lastname'];
        $rentalUser->birthdate = Carbon::parse($input['dateofbirth'])->format('Y-m-d');
        if($input['updatepassword']) {
            $password = Hash::make($input['updatepassword']);
            $rentalUser->password = $password;
        }
        $rentalUser->phone = $input['phonenumber'];
        $rentalUser->save();
    }

    public function getRentalUserWithCommunicationPref($userId)
    {
        return RentalUser::with('communication_preferences')->where('id', $userId)->first();
    }

    public function updateUserSubscriptionStatus($userId, $subscriptionStatus)
    {
        $rentalUser = $this->getById($userId);
        
        $rentalUser->is_subscribed = $subscriptionStatus;
        return $rentalUser->save();
    }
    public function updateProfilePic($userId, $imageName){
        $rentalUser = $this->getById($userId);
        $rentalUser->photo = $imageName;
        $rentalUser->save();
    }

    public function updateCustomerId($customerId, $userId)
    {
        return RentalUser::where('id',$userId)->update(['customer_id'=>$customerId]);
    }
}
