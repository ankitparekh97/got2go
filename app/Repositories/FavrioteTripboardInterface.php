<?php


namespace App\Repositories;

use App\Models\FavTripboards;

interface FavrioteTripboardInterface
{
    public function getFavTriboards($userId);

    public function getFavTriboardsWithPropertyId($userId,$propertyId);

    public function removePropertyFromFav($favTripboard);

    public function getFavouriteTripBoards($userId);

    public function likeUnlikeProperties($param);
}
