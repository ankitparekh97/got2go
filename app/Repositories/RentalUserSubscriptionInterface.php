<?php


namespace App\Repositories;


use App\Http\DTO\Api\RentalUserSubscriptionDTO;

interface RentalUserSubscriptionInterface
{
    public function saveRentalUserSubscription(RentalUserSubscriptionDTO $rentalUserSubscriptionDTO,$rentalId = null);

    public function findRentalUserSubscription($rentalUserSubscriptionId);

    public function deleteRentalUserSubscription($rentalUserSubscriptionId);
}
