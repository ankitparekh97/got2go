<?php


namespace App\Repositories;


use App\Http\DTO\Api\RentalUserSubscriptionDTO;
use App\Models\RentalUserSubscription;

trait RentalUserSubscriptionRepository
{
    /**
     * @param RentalUserSubscriptionDTO $rentalUserSubscriptionDTO
     * @param null $rentalId
     * @return RentalUserSubscription|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function saveRentalUserSubscription(
        RentalUserSubscriptionDTO $rentalUserSubscriptionDTO,
        $rentalId = null
    )
    {
        /**
         * @var RentalUserSubscription $rentalUserSubscription
         */
        $rentalUserSubscription = $this->findRentalUserSubscriptionByUserId($rentalId);
        if(empty($rentalUserSubscription)){
            $rentalUserSubscription = new $this->rentalUserSubscription;
        }

        $rentalUserSubscription->rental_id = $rentalUserSubscriptionDTO->rentalId;
        $rentalUserSubscription->billing_period_start_date = $rentalUserSubscriptionDTO->billingPeriodStartDate;
        $rentalUserSubscription->billing_period_end_date = $rentalUserSubscriptionDTO->billingPeriodEndDate;
        $rentalUserSubscription->first_billing_date = $rentalUserSubscriptionDTO->firstBillingDate;
        $rentalUserSubscription->current_billing_cycle = $rentalUserSubscriptionDTO->currentBillingCycle;
        $rentalUserSubscription->description = $rentalUserSubscriptionDTO->description;
        $rentalUserSubscription->subscription_id = $rentalUserSubscriptionDTO->subscriptionId;
        $rentalUserSubscription->next_billing_date = $rentalUserSubscriptionDTO->nextBillingDate;
        $rentalUserSubscription->next_billing_amount = $rentalUserSubscriptionDTO->nextBillingAmount;
        $rentalUserSubscription->paid_through_date = $rentalUserSubscriptionDTO->paidThroughDate;
        $rentalUserSubscription->plan_id = $rentalUserSubscriptionDTO->planId;
        $rentalUserSubscription->status = $rentalUserSubscriptionDTO->status;
        $rentalUserSubscription->paid_through = $rentalUserSubscriptionDTO->paidThrough;
        $rentalUserSubscription->price = $rentalUserSubscriptionDTO->price;
        $rentalUserSubscription->save();

        return $rentalUserSubscription;
    }

    /**
     * @param $rentalUserSubscriptionId
     * @return RentalUserSubscription|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function findRentalUserSubscription($rentalUserSubscriptionId)
    {
        /**
         * @var RentalUserSubscription $rentalUserSubscription
         */
        $rentalUserSubscription = (new $this->rentalUserSubscription())::query();
        $rentalUserSubscription->where('id',$rentalUserSubscriptionId);
        return $rentalUserSubscription->first();

    }

    /**
     * @param $rentalId
     * @return RentalUserSubscription|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function findRentalUserSubscriptionByUserId($rentalId)
    {
        /**
         * @var RentalUserSubscription $rentalUserSubscription
         */
        $rentalUserSubscription = (new $this->rentalUserSubscription())::query();
        $rentalUserSubscription->where('rental_id',$rentalId);
        return $rentalUserSubscription->first();

    }


    /**
     * @param $rentalUserSubscriptionId
     * @throws \Exception
     */
    public function deleteRentalUserSubscription($rentalUserSubscriptionId)
    {
        /**
         * @var RentalUserSubscription $rentalUserSubscription
         */
        $rentalUserSubscription = (new $this->rentalUserSubscription())::query();
        $rentalUserSubscription->where('id',$rentalUserSubscriptionId);
        $rentalUserSubscription->delete();
    }

}
