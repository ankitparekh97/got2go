<?php


namespace App\Repositories;

use App\Models\Configuration;

interface ConfigurationInterface
{
    public function getConfiguration();
    public function serviceFeeSave($servicefee,$serviceId);
}
