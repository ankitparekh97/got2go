<?php


namespace App\Repositories;

use App\Models\CommunicationPreferences;
use DB;
use Illuminate\Support\Carbon;

class CommunicationPreferencesRepository implements CommunicationPreferencesInterface
{

    public $configurationModel;

    public function __construct(
        CommunicationPreferences $communicationPreferences
    )
    {
        $this->communicationPreferences = $communicationPreferences;
    }
    public function deleteCommunicationPreferencesByUserId($userId){
        return CommunicationPreferences::where('userid',$userId)->delete();
    }
    public function createCommunicationPreferences($prefernces)
    {
       return CommunicationPreferences::create([
                'user_type' => $prefernces->userType,
                'userid' => $prefernces->userid,
                'communication_title' => $prefernces->communicationTitle,
                'is_notification' => $prefernces->isNotification,
                'is_email' => $prefernces->isEmail,
                'is_sms' => $prefernces->isSms,
                'created_at' => $prefernces->createdAt,
            ]);
    }

}
