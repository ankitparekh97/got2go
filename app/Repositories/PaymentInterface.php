<?php


namespace App\Repositories;


use App\Http\DTO\Api\PaymentCardDTO;

interface PaymentInterface
{
    public function createPaymentGatewayCustomer($userId);

    public function createPaymentGatewayCard($userId,PaymentCardDTO $paymentCardDTO);

    public function createPaymentGatewaySubscription($userId,$subscriptionId);

    public function cancelPaymentGatewaySubscription($userId);

    public function pausePaymentGatewaySubscription($userId);

    public function activePaymentGatewaySubscription($userId);
}
