<?php


namespace App\Repositories;


use App\Http\DTO\Api\MakeOfferDTO;
use App\Models\PropertyOffers;
use Illuminate\Support\Carbon;

class MakeOfferRepository implements MakeOfferInterface
{
    /**
     * @param MakeOfferDTO $makeOfferDTO
     * @param null $rentalId
     * @return PropertyOffers|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function saveMakeOffer(MakeOfferDTO $makeOfferDTO,$rentalId = null)
    {
        /**
         * @var PropertyOffers $propertyOffers
         */
        $propertyOffers = new PropertyOffers();

        $propertyOffers->property_id = $makeOfferDTO->propertyId;
        $propertyOffers->owner_id = $makeOfferDTO->ownerId;
        $propertyOffers->rental_id = $rentalId;
        $propertyOffers->offered_nightly_price = $makeOfferDTO->offeredNightlyPrice;
        $propertyOffers->offer_start_date = $makeOfferDTO->offerStartDate;
        $propertyOffers->offer_end_date = $makeOfferDTO->offerEndDate;
        $propertyOffers->flexible_dates = $makeOfferDTO->flexibleDates;
        $propertyOffers->created_at = Carbon::now();
        $propertyOffers->updated_at = Carbon::now();
        $propertyOffers->save();

        return $propertyOffers;
    }
}
