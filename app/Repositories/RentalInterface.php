<?php


namespace App\Repositories;


use App\Http\DTO\Api\RentalUserDTO;
use App\Models\RentalUser;
use Illuminate\Support\Carbon;

interface RentalInterface extends PaymentInterface,RentalUserSubscriptionInterface
{
    public function getAll();

    public function getById($id);

    public function saveRenterUser(RentalUserDTO $rentalUserDTO,$userId = null);

    public function saveAsTripper(RentalUserDTO $rentalUserDTO,$userId);

    public function tripperToNormalRental($userId);

    public function savePaymentCustomerId($paymentCustomerId,$userId);

    public function getRentalUserSubscriptionExpireByDate(Carbon $date);

    public function getTripperList();

    public function updateSubscriptionStatus($rentalUserId,$subscriptionAction);

    public function updateUserPassword($email,$password);

    public function createUser($rentalUserArray);

    public function getRentalByGoogleId($googleId,$email);

    public function updateRentalUserSetting($input);
    public function getRentalUserWithCommunicationPref($userId);
    public function updateUserSubscriptionStatus($userId, $subscriptionStatus);
    public function updateProfilePic($userId, $imageName);
    public function updateCustomerId($customerId, $userId);

}
