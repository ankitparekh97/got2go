<?php


namespace App\Repositories;

use App\Models\PropertyOwnershipDocuments;

interface PropertyOwnershipDocumentInterface
{
   public function viewDocument($type,$id);
}
