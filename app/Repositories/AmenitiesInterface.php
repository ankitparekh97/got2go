<?php

namespace App\Repositories;

interface AmenitiesInterface
{
    public function getMasterAmenities();

    public function getMasterAmenitiesByArray();
}
