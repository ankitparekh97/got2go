<?php


namespace App\Repositories;


use App\Models\TripboardProperty;
use App\Models\TripboardVote;
use Auth;
use Carbon\Carbon;
use DB;
use Helper;
use Session;
use  App\Repositories\TripBoardVoteInterface;

class TripBoardPropertyRepository implements TripBoardPropertyInterface
{
    public function __construct( TripBoardVoteInterface $tripBoardVoteRepository)
    {
        $this->tripBoardVoteRepository = $tripBoardVoteRepository;
    }

    
    public function addPropertyToTripboard($tripboardId, $propertyId)
    {
        $param = [
            'tripboard_id' => $tripboardId,
            'property_id' => $propertyId
        ];
        
        if(!TripboardProperty::where($param)->exists()){
            $inserted = TripboardProperty::insert([
                'tripboard_id' => $tripboardId,
                'property_id' => $propertyId
            ]);
            return response()->json(['success'=>true,'data'=>"Property Added to tripboard.",'type'=>"save"]);
        }else{
            TripboardProperty::where('tripboard_id', $tripboardId)
            ->where('property_id', $propertyId)
            ->delete();
            $this->tripBoardVoteRepository->deleteTripBoardPropertyVotes($tripboardId, $propertyId);
           
            return response()->json(['success'=>true,'data'=>"Property Removed from tripboard.",'type'=>"remove"]);
        }
    }

    public function AddOrRemovePropertiesFromTripboard($param)
    {
        if(!TripboardProperty::where($param)->exists()){
            $inserted = TripboardProperty::insert($param);
            return response()->json(['success'=>true,'data'=>"Property Added to tripboard.",'type'=>"save"]);
        }else{
            TripboardProperty::where('tripboard_id', $param['tripboard_id'])
            ->where('property_id', $param['property_id'])
            ->delete();

            $this->tripBoardVoteRepository->deleteTripBoardPropertyVotes($param['tripboard_id'],  $param['property_id']);
            return response()->json(['success'=>true,'data'=>"Property Removed from tripboard.",'type'=>"remove"]);
        }
    }

    public function tripBoardRemoveListing($param) 
    {
        try {
            TripboardProperty::where('tripboard_id', $param->tripboard_id)
            ->where('property_id', $param->property_id)
            ->delete();
            $this->tripBoardVoteRepository->deleteTripBoardPropertyVotes($param->tripboard_id,  $param->property_id);
            return ['success'=>true,'message'=>'Tripboard listing removed successfully'];
        } catch (\Throwable $th) {
            return ['success'=>false,'message'=>$th->getMessage()];
        }
    }


}
