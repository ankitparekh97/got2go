<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\DTO\PropertyOfferDTO;
use App\Models\PropertyOffers;
use DB;
use Illuminate\Support\Carbon;

class PropertyOfferRepository implements PropertyOfferInterface
{
    public $propertyOfferModel;

    public function __construct(
        PropertyOffers $propertyOfferModel
    )
    {
        $this->propertyOfferModel = $propertyOfferModel;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param PropertyOfferDTO $propertyOfferDTO
     * @return mixed
     */
    public function savePropertyOffer(PropertyOfferDTO $propertyOfferDTO)
    {

        /**
         * @var PropertyOffers $propertyOffers
         */
        $propertyOffers = (new $this->propertyOfferModel);

        if($propertyOfferDTO->id){
            $propertyOffers = $this->getPropertyOfferById($propertyOfferDTO->id);
        }

        $propertyOffers->property_id = $propertyOfferDTO->propertyId ?? $propertyOffers->property_id;
        $propertyOffers->owner_id = $propertyOfferDTO->ownerId ?? $propertyOffers->owner_id;
        $propertyOffers->rental_id = $propertyOfferDTO->rentalId ?? $propertyOffers->rental_id;
        $propertyOffers->action_by = $propertyOfferDTO->actionBy ?? GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL;
        $propertyOffers->status = $propertyOfferDTO->status ?? GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_PENDING;
        $propertyOffers->tripper_offer_nightly_price = $propertyOfferDTO->tripperOfferNightlyPrice ?? $propertyOffers->tripper_offer_nightly_price;
        $propertyOffers->owner_offer_nightly_price = $propertyOfferDTO->ownerOfferNightlyPrice ?? $propertyOffers->owner_offer_nightly_price;
        $propertyOffers->tripper_offer_start_date = $propertyOfferDTO->tripperOfferStartDate ?? $propertyOffers->tripper_offer_start_date;
        $propertyOffers->tripper_offer_end_date = $propertyOfferDTO->tripperOfferEndDate ?? $propertyOffers->tripper_offer_end_date;
        $propertyOffers->owner_offer_start_date = $propertyOfferDTO->ownerOfferStartDate ?? $propertyOffers->owner_offer_start_date;
        $propertyOffers->owner_offer_end_date = $propertyOfferDTO->ownerOfferEndDate ?? $propertyOffers->owner_offer_end_date;
        $propertyOffers->tripper_offer_proposed_total = $propertyOfferDTO->tripperOfferProposedTotal ?? $propertyOffers->tripper_offer_proposed_total;
        $propertyOffers->owner_offer_proposed_total = $propertyOfferDTO->ownerOfferProposedTotal ?? $propertyOffers->owner_offer_proposed_total;
        $propertyOffers->number_of_guests = $propertyOfferDTO->numberOfGuests ?? $propertyOffers->number_of_guests;
        $propertyOffers->flexible_dates = $propertyOfferDTO->flexibleDates ?? $propertyOffers->flexible_dates;
        $propertyOffers->final_accepted_start_date = $propertyOfferDTO->finalAcceptedStartDate ?? $propertyOffers->final_accepted_start_date;
        $propertyOffers->final_accepted_end_date = $propertyOfferDTO->finalAcceptedEndDate ?? $propertyOffers->final_accepted_end_date;
        $propertyOffers->final_accepted_nightly_price = $propertyOfferDTO->finalAcceptedNightlyPrice ?? $propertyOffers->final_accepted_nightly_price;
        $propertyOffers->booking_id = $propertyOfferDTO->bookingId ?? $propertyOffers->booking_id;
        $propertyOffers->save();

        return $propertyOffers->load(['property.propertyimages','booking']);
    }

    public function getPropertyOffersByUserId($userId, $userType)
    {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = (new $this->propertyOfferModel)::query();

        if (GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER === $userType) {
            $propertyOffer->where(PropertyOffers::OWNER_ID, $userId);
            $propertyOffer->where(PropertyOffers::STATUS, '<>', GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED);
            $propertyOffer->orWhere(function ($whereQuery) {
                $whereQuery->where(PropertyOffers::STATUS,GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_REJECTED);
                $whereQuery->where(PropertyOffers::ACTION_BY,GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL);
            });
        } else if (GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL === $userType) {
            $propertyOffer->where(PropertyOffers::RENTAL_ID, $userId);
        }
        $propertyOffer->with(['property.propertyimages','booking']);

        return $propertyOffer->get();

    }

    /**
     * @param $propertyOfferId
     * @return mixed
     */
    public function getPropertyOfferById($propertyOfferId)
    {
        $propertyOffer = ($this->propertyOfferModel)::findOrFail($propertyOfferId);
        return $propertyOffer->load(['property.propertyimages','booking']);
    }

    /**
     * @param $propertyId
     * @param $ownerId
     * @param $rentalId
     * @throws \Exception
     */
    public function softDeletePreviousOffersBatch($propertyId, $ownerId, $rentalId)
    {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = (new $this->propertyOfferModel)::query();
        $propertyOffer->where(PropertyOffers::PROPERTY_ID,$propertyId);
        $propertyOffer->where(PropertyOffers::OWNER_ID,$ownerId);
        $propertyOffer->where(PropertyOffers::RENTAL_ID,$rentalId);
        $propertyOffer->delete();
    }

    public function softDeleteOtherOverLappingOffers($propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = $this->getPropertyOfferById($propertyOfferId);

        $ownerId = $propertyOffer->owner_id;
        $rentalId = $propertyOffer->rental_id;
        $offerStartDate = $propertyOffer->final_accepted_start_date;
        $offerEndDate = $propertyOffer->final_accepted_end_date;
        $propertyId = $propertyOffer->property_id;

        /**
         * @var PropertyOffers $propertyOfferModelQuery
         */
        $propertyOfferModelQuery = (new $this->propertyOfferModel)::query();

        $propertyOfferModelQuery->where(PropertyOffers::PROPERTY_ID,$propertyId);
        $propertyOfferModelQuery->where(PropertyOffers::ID,'<>',$propertyOfferId);

        $propertyOfferModelQuery->checkOfferDateIsOverLapping($offerStartDate,$offerEndDate);

        $dataToRemoveCollection = $propertyOfferModelQuery->get();

        if(!empty($dataToRemoveCollection)) {
            foreach($dataToRemoveCollection as $dataToRemoveObj) {
                $this->cancelAllPendingOffers($dataToRemoveObj->id);
            }
        }
    }

    public function cancelAllPendingOffers($propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyModel
         */
        $propertyModel = $this->getPropertyOfferById($propertyOfferId);
        $propertyModel->action_by = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_SYSTEM;
        $propertyModel->status = GlobalConstantDeclaration::PROPERTY_OFFER_STATUS_CANCEL;
        $propertyModel->save();

    }

    public function softDeleteOfferById($propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyModel
         */
        $propertyModel = (new $this->propertyOfferModel)::query();
        $propertyModelFirst = $propertyModel->where('id',$propertyOfferId)->first();
        $propertyModelFirst->delete();
        return true;
    }

    public function checkOfferDateIsOverLappingByTripperId($startDate,$endDate,$tripperId,$propertyId)
    {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = (new $this->propertyOfferModel)::query();

        $propertyOffer->where(PropertyOffers::PROPERTY_ID,$propertyId);
        $propertyOffer->where(PropertyOffers::RENTAL_ID,$tripperId);

        $propertyOffer->checkOfferDateIsOverLapping($startDate,$endDate);

        return $propertyOffer->get();
    }

    public function checkOfferDateIsOverLappingByTripperIdAndOtherOffer($startDate,$endDate,$tripperId,$propertyId,$propertyOfferId)
    {
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffer = (new $this->propertyOfferModel)::query();

        $propertyOffer->where(PropertyOffers::ID,'<>',$propertyOfferId);
        $propertyOffer->where(PropertyOffers::PROPERTY_ID,$propertyId);
        $propertyOffer->where(PropertyOffers::RENTAL_ID,$tripperId);

        $propertyOffer->checkOfferDateIsOverLapping($startDate,$endDate);

        return $propertyOffer->get();
    }

    public function getPendingResponseOnOfferByOwner($ownerId){
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffers = (new $this->propertyOfferModel)::query();
        $propertyOffers = $propertyOffers->pendingResponseOnOfferByOwner($ownerId)->get();

        return $propertyOffers;
    }

    public function getPendingResponseOnOfferByTripper($tripperId){
        /**
         * @var PropertyOffers $propertyOffer
         */
        $propertyOffers = (new $this->propertyOfferModel)::query();
        $propertyOffers = $propertyOffers->pendingResponseOnOfferByTripper($tripperId)->get();

        return $propertyOffers;
    }

}
