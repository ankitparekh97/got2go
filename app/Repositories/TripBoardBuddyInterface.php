<?php


namespace App\Repositories;

interface TripBoardBuddyInterface
{
    public function getTravelBuddies($tripboardId);
    public function  saveTravelBuddy($input, $userId);
    public function  deleteTravelBuddy($buddyId);
}
