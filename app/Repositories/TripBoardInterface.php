<?php


namespace App\Repositories;

interface TripBoardInterface
{
    public function getTripBoardById(int $tripBoardId,array $with = array());

    public function getUserTripBoards($userId);

    public function getTripBoardProperties($tripboardId);

    public function getLoadLatestMessages($request);

    public function saveTripboardDetail($input);
    public function getTriboardsWithPropertyId($userId,$propertyId);
    public function addTripboard($tripboardName, $userId);
}
