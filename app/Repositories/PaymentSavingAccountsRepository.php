<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\PaymentHistoryDTO;
use App\Models\PaymentSavingAccounts as PaymentSavingAccounts;

class PaymentSavingAccountsRepository implements PaymentSavingAccountsInterface
{
    public $paymentSavingAccountsModel;

    public function __construct(
        PaymentSavingAccounts $paymentSavingAccountsModel
    )
    {
        $this->paymentSavingAccountsModel = $paymentSavingAccountsModel;
    }

    public function paymentMethodObject($params){
         /**
         * @var PaymentSavingAccounts $paymentSavingAccountsModel
         */

        $paymentSavingAccountsModel = (new $this->paymentSavingAccountsModel)::query();
        $paymentSavingAccountsModel->where($params);

        return $paymentSavingAccountsModel->first();
    }

    public function paymentMethod($params){
        /**
        * @var PaymentSavingAccounts $paymentSavingAccountsModel
        */

        $paymentSavingAccountsModel = (new $this->paymentSavingAccountsModel)::query();
        $paymentSavingAccountsModel->where($params);

        $paymentSavingAccountsModel->orderBy(PaymentSavingAccounts::ID, GlobalConstantDeclaration::ORDER_BY_DESC);

       return $paymentSavingAccountsModel->get();
   }

   public function saveCard(PaymentHistoryDTO $paymentHistoryDTO){
        /**
        * @var PaymentSavingAccounts $paymentSavingAccountsModel
        */

        $paymentSavingAccountsModel = (new $this->paymentSavingAccountsModel);

        if($paymentHistoryDTO->id){
            $paymentSavingAccountsModel = $this->paymentMethodObject(array('id'=>$paymentHistoryDTO->id));
        }

        $paymentSavingAccountsModel->user_id = !empty($paymentHistoryDTO->userId) ? $paymentHistoryDTO->userId : $paymentSavingAccountsModel->user_id;
        $paymentSavingAccountsModel->user_type = isset($paymentHistoryDTO->userType) ? $paymentHistoryDTO->userType : $paymentSavingAccountsModel->user_type;
        $paymentSavingAccountsModel->first_name = !empty($paymentHistoryDTO->firstName) ? $paymentHistoryDTO->firstName : $paymentSavingAccountsModel->first_name;
        $paymentSavingAccountsModel->routing_number = !empty($paymentHistoryDTO->routingNumber) ? $paymentHistoryDTO->routingNumber : $paymentSavingAccountsModel->routing_number;
        $paymentSavingAccountsModel->institution_name = !empty($paymentHistoryDTO->institutionName) ? $paymentHistoryDTO->institutionName : $paymentSavingAccountsModel->institution_name;
        $paymentSavingAccountsModel->account_number = !empty($paymentHistoryDTO->accountNumber) ? $paymentHistoryDTO->accountNumber : $paymentSavingAccountsModel->account_number;
        $paymentSavingAccountsModel->default = isset($paymentHistoryDTO->default) ? $paymentHistoryDTO->default : $paymentSavingAccountsModel->default;
        $paymentSavingAccountsModel->state = isset($paymentHistoryDTO->state) ? $paymentHistoryDTO->state : $paymentSavingAccountsModel->state;
        $paymentSavingAccountsModel->city = isset($paymentHistoryDTO->city) ? $paymentHistoryDTO->city : $paymentSavingAccountsModel->city;
        $paymentSavingAccountsModel->ssn = isset($paymentHistoryDTO->ssn) ? $paymentHistoryDTO->ssn : $paymentSavingAccountsModel->ssn;
        $paymentSavingAccountsModel->zipcode = isset($paymentHistoryDTO->zipcode) ? $paymentHistoryDTO->zipcode : $paymentSavingAccountsModel->zipcode;
        $paymentSavingAccountsModel->stripe_account_id = isset($paymentHistoryDTO->stripeAcountId) ? $paymentHistoryDTO->stripeAcountId : $paymentSavingAccountsModel->stripe_account_id;
        $paymentSavingAccountsModel->fingerprint = isset($paymentHistoryDTO->fingerPrint) ? $paymentHistoryDTO->fingerPrint : $paymentSavingAccountsModel->fingerprint;

        $paymentSavingAccountsModel->save();

        return $paymentSavingAccountsModel->id;
   }

   public function deletepaymentHistory($id){
        /**
        * @var PaymentSavingAccounts $paymentSavingAccountsModel
        */

        $paymentSavingAccountsModel = $this->paymentMethodObject(array('id' => $id));
        $paymentSavingAccountsModel->delete();

        return $paymentSavingAccountsModel;
   }
}
