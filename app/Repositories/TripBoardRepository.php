<?php


namespace App\Repositories;


use App\Models\Tripboards;
use Auth;
use Carbon\Carbon;
use DB;
use App\Models\GroupMessages;
use Helper;
use Session;
use App\Models\TripboardShare;
class TripBoardRepository implements TripBoardInterface
{
    public function getTripBoardById(int $tripBoardId,array $with = array())
    {
        $tripBoard = Tripboards::query();

        $tripBoard->where('id',$tripBoardId);

        if(in_array('tripBoardProperties',$with,true)){
            $tripBoard->with('tripBoardProperties');
        }

        if(in_array('properties',$with,true)){
            $tripBoard->with('properties');
        }

        return $tripBoard->first();
    }

    public function getUserTripBoards($userId)
    {
        return Tripboards::leftJoin('tripboard_share',function($join){
            $join->on('tripboard_share.tripboard_id','=','tripboard.id');
        })->where('tripboard.user_id',$userId)->orWhere('tripboard_share.user_id',$userId)->select('tripboard.*')->get();
    }

    public function getTripBoardProperties($tripboardId)
    {
        $properties_obj = Tripboards::leftJoin('tripboard_property','tripboard_property.tripboard_id','=','tripboard.id')
            ->leftJoin('property','property.id','=','tripboard_property.property_id')
            ->leftJoin('tripboard_vote',function($join) {
                $join->on('tripboard_vote.user_id','=','tripboard.user_id');
                $join->on('tripboard_vote.property_id','=','tripboard_property.property_id');
            })
            ->select(
                \DB::raw("(SELECT sum(tripboard_vote.votes) FROM tripboard_vote
                                WHERE tripboard_vote.property_id = tripboard_property.property_id
                                AND tripboard_vote.tripboard_id = tripboard_property.tripboard_id
                                GROUP BY tripboard_vote.property_id,tripboard_vote.tripboard_id
                                ) as totalVote"),
                                'tripboard_vote.votes','property.*', 'tripboard.from_date','tripboard.to_date','tripboard_property.tripboard_id','tripboard_property.property_id')
            ->where('tripboard.id', $tripboardId)
            ->where('property.status', 'approved')
            ->where('property.publish', '1')
            ->whereNull('property.deleted_at')
            ->whereNotNull('property.id')
            ->groupBy(['tripboard_property.property_id']);
         $result = $properties_obj->get();
         return $result;
    }

    public function getLoadLatestMessages($request)
    {

    }


    /**
     * @param $userId
     * @param $propertyId
     * @return mixed
     */
    public function getTriboardsWithPropertyId($userId,$propertyId)
    {
        return $tripboards = Tripboards::leftJoin('tripboard_share',function($join){
                $join->on('tripboard_share.tripboard_id','=','tripboard.id');
            })->where('tripboard.user_id',$userId)->orWhere('tripboard_share.user_id',$userId)
                ->with(['tripboardproperty'=> function($tripboardproperty) use ($propertyId) {
                    $tripboardproperty->where(['property_id'=>$propertyId]);
                    return $tripboardproperty;
                }])->select('tripboard.*')->get();
    }

    function addTripboard($tripboardName, $userId) {

        $rental_user = $userId;
        $fromDate =  date('Y-m-d', time());
        $toDate =  date('Y-m-d', time());
        $typeOfDate = 0;
        $param=['user_id' => $userId, 'tripboard_name' => $tripboardName];

        if( $tripboard = Tripboards::where($param)->first()){
            $insertId =  $tripboard->id;
        } else {
            $insertId = DB::table('tripboard')->insertGetId(
                [
                    'tripboard_name' => $tripboardName,
                    'from_date' =>$fromDate,
                    'to_date' => $toDate,
                    'type_of_date' => $typeOfDate,
                    'user_id' => $userId
                ]
            );
        }

        return $insertId;
    }

    function saveTripboardDetail($input){

        $fromDate =  $input['from_date'];
        $toDate =  $input['to_date'];
        $typeOfDate = $input['type_of_date'];
        $totalGuest = $input['no_of_guest'];
        $insert = DB::table('tripboard')->where('id', $input['tripboard_id'])
        ->update(
            [
                'tripboard_name' => $input['tripboard_name'],
                'from_date' => date('Y-m-d',strtotime($fromDate)),
                'to_date' => date('Y-m-d',strtotime($toDate)),
                'type_of_date' => $typeOfDate,
                'no_of_guest' => $totalGuest
            ]
        );
        return response()->json(['success'=>true,'data'=>[]]);
    }
}
