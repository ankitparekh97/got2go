<?php


namespace App\Repositories;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\DTO\PropertyDTO;
use App\Models\PropertyType;
use DB;
use Illuminate\Support\Carbon;

class PropertyTypeRepository implements PropertyTypeInterface
{
    public $propertyModel;

    public function __construct(
        PropertyType $propertyModel
    )
    {
        $this->propertyModel = $propertyModel;
    }

    public function getPropertyTypes(){

         /**
         * @var PropertyType $propertiesTypes
         */

        $types = (new $this->propertyModel)::query();
        $propertiesTypes = $types->get();

        return $propertiesTypes;
    }

    public function getPropertyTypesByParams(){

       return PropertyType::select(DB::raw('LOWER(property_type) as property_type, LOWER(type_of_property) as type_of_property,id'))
                        ->where('type_of_property','!=','hotels')
                        ->where('parent_id','=','0')
                        ->get()->keyBy('id');
   }
}
