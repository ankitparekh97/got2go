<?php

namespace App\Repositories;

use App\Http\DTO\Api\PropertyBedroomsDTO;

interface PropertyBedroomsInterface
{
    public function getPropertyBedroomsByParamsCollection($params);

    public function getPropertyBedroomsByParamsObject($params);

    public function savePropertyBedrooms($data);

}
