<?php

namespace App\Repositories;

use App\Http\DTO\Api\BulkUploadDTO;

interface BulkUploadLogsInterface
{
    public function getLogs($Id);

    public function getrowPosition($Id);

    public function getQueueCount();

    public function saveLogs($params);
}
