<?php


namespace App\Repositories;

use App\Models\TripboardBuddies;

use App\Models\TripboardVote;
use DB;

class TripBoardBuddyRepository implements TripBoardBuddyInterface
{

    public function getTravelBuddies($tripboardId)
    {
        return TripboardBuddies::where('tripboard_id',$tripboardId)->get();
    }

    function saveTravelBuddy($input, $userId){

        if($input->isNew && $input->isNew == 'true'){
            $insertId =  TripboardBuddies::insertGetId(
                ['tripboard_id' => $input->tripboard_id, 'buddy_name' => $input->buddy_name,'user_id' => $userId]
            );
            $totalGuest = TripboardBuddies::select(DB::raw('count(*) as total'))->where('tripboard_id', $input->tripboard_id)
            ->first();
            DB::table('tripboard')->where('id', $input->tripboard_id)
            ->update(
                [
                    'no_of_guest' => $totalGuest->total
                ]
            );
            return $insertId;
        }else{
            TripboardBuddies::where('id', $input->buddy_id)
            ->update(
                ['tripboard_id' => $input->tripboard_id, 'buddy_name' => $input->buddy_name]
            );
            return $input->buddy_id;
        }
    }

    function deleteTravelBuddy($buddyId){
        return $delete = TripboardBuddies::where('id',$buddyId)->delete();
    }
}
