<?php


namespace App\Repositories;


use App\Http\DTO\Api\CouponCodeDTO;
use App\Models\CouponCode;
use Illuminate\Database\Eloquent\Builder;

class CouponCodeRepository implements CouponCodeInterface
{

    public $couponCode;

    public function __construct(CouponCode $couponCode)
    {
        $this->couponCode = $couponCode;
    }

    public function getAllCoupons()
    {
        /**
         * @var CouponCode|Builder $couponCode
         */
        $couponCode = (new $this->couponCode)::query();
        $couponCode->orderBy(CouponCode::UPDATED_AT,'DESC');
        $couponCodes = $couponCode->get();

        return $couponCodes;
    }

    public function saveCoupon(CouponCodeDTO $couponCodeDTO)
    {
        /**
         * @var CouponCode|Builder $couponCode
         */
        $couponCode = (new $this->couponCode);

        if($couponCodeDTO->id){
            $couponCode = $couponCode->where(CouponCode::ID,$couponCodeDTO->id)->first();
        }

        $couponCode->name = !empty($couponCodeDTO->name) ? $couponCodeDTO->name : $couponCode->name;
        $couponCode->status = isset($couponCodeDTO->status) ? $couponCodeDTO->status : $couponCode->status;
        $couponCode->discount_percentage = !empty($couponCodeDTO->discountPercentage) ? $couponCodeDTO->discountPercentage : $couponCode->discount_percentage;
        $couponCode->is_single_use = !empty($couponCodeDTO->isSingleUse) ? $couponCodeDTO->isSingleUse : $couponCode->is_single_use;
        $couponCode->save();

        return $couponCode;
    }

    public function findCouponCodeByName($couponCodeName)
    {
        /**
         * @var CouponCode $couponCode
         */
        $couponCode = (new $this->couponCode)::query();
        $couponCode->where(CouponCode::NAME,$couponCodeName);
        return $couponCode->first();
    }

    public function findCouponCodeById($couponCodeId)
    {
        /**
         * @var CouponCode $couponCode
         */
        $couponCode = (new $this->couponCode)::query();
        $couponCode->where(CouponCode::ID,$couponCodeId);
        return $couponCode->first();
    }
}
