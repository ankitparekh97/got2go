<?php

namespace App\Repositories;

use App\Http\DTO\Api\BulkUploadDTO;

interface BulkUploadInterface
{
    public function getById($id);

    public function getListings($ownerId,$id);

    public function getByUniqueId($unique_key);

    public function getBulkUploadByParamsObject($params);

    public function saveBulkUpload(BulkUploadDTO $bulkUploadDTO);

    public function update($id,$data);
}
