<?php

namespace App\Http\Controllers\HotelPanel;

use App\Http\Controllers\Controller;
use App\Hotels;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotels  $hotels
     * @return \Illuminate\Http\Response
     */
    public function show(Hotels $hotels)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotels  $hotels
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotels $hotels)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotels  $hotels
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotels $hotels)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotels  $hotels
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotels $hotels)
    {
        //
    }

    public function changePasswordView(){
        return view('hotel.changepassword');
    }
    /**
     * Change password for hotel user
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        
        $validator =  Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',

        ]); 
         if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        $hotel = Hotels::find(Auth::guard('hotel')->user()->id);
        $hotel->password = Hash::make($request['password']);
        $hotel->save();

        Session::flash('success', 'Password Changed Successfully!');
        return response()->json(['success'=>'yes','redirect_url'=>url('/change')]);
    }
}
