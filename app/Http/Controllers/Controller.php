<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Request;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $view;
    protected $redirect;
    function renderData($params){

        if(Request::is('api*')){
            return response()->json($params);
        }
        else if($this->view != '')
        {
            return view($this->view,$params);
        }
        else if($this->redirect != ''){
             return redirect($this->redirect);
        }

    }

    public function returnResponseWithoutData(
        $successFlag = true,
        $message = '',
        $successCode = Response::HTTP_OK
    )
    {
        return response([
            'success' => $successFlag,
            'message' => $message,
        ],
            $successCode
        );
    }

    public function returnResponseWithData(
        $data,
        $message = '',
        $successCode = Response::HTTP_OK
    )
    {
        return response([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ],
            $successCode
        );
    }
}
