<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\RentalUser;

use App\Models\TripboardShare;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Socialite;
use Auth;
use Exception;
use Illuminate\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log as FacadesLog;
use Illuminate\Support\Facades\Session;
use App\Models\CommunicationPreferences;
use Carbon\Carbon;
use App\Services\LoginServiceContract;
use Illuminate\Support\Facades\Validator;

class SocialAuthController extends Controller
{


    public function __construct(
        LoginServiceContract $loginServiceContract
    )
    {
        $this->loginServiceContract = $loginServiceContract;
    }

    public function rentalRedirectToGoogle()
    {
        Session::put('SocialAuthUrl', url()->previous());
        Session::save();
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {

        try {
            $loginfor =  Session::get('SocialAuthUrl');
            $user = Socialite::driver('google')->user();
            $googleId = $user->id;
            $email = $user->email;
            $name = $user->name;
            return $this->loginServiceContract->socialLogin($googleId,null,$email,$name,$loginfor);
        } catch (Exception $e) {
            Session::flash('warning', 'Something went Wrong! Try again after some time!');
            return redirect(url()->previous());
        }
    }

    public function rentalRedirectToFacebook()
    {
        Session::put('SocialAuthUrl', url()->previous());
        Session::save();
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {

        try {
            $loginfor =  Session::get('SocialAuthUrl');
            $user = Socialite::driver('facebook')->user();
            $facebookId = $user->id;
            $email = $user->email;
            $name = $user->name;
            return $this->loginServiceContract->socialLogin(null,$facebookId,$email,$name,$loginfor);

        } catch (Exception $e) {
            Session::flash('warning', 'Something went Wrong! Try again after some time!');
            return redirect(url()->previous());
        }
    }

}
