<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class OwnerController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $this->view = 'home';
        return $this->renderData(['success'=>true]);
    }

    public function listYourStay(Request $request) {
        $totalEarn = mt_rand( 10000, 100000);

        $this->view = 'owner.list-your-stay';
        return $this->renderData(compact('totalEarn'));
    }
}
