<?php

namespace App\Http\Controllers\Api\Tripper;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\MakeOfferDTO;
use App\Http\Requests\Api\Tripper\PropertyOffer\CounterOfferRequest;
use App\Http\Requests\Api\Tripper\PropertyOffer\MakeOfferRequest;
use App\Http\Resources\PropertyOffer\PropertyOffer;
use App\Models\PropertyOffers;
use App\Services\PropertyOfferServiceContract;
use App\Services\SendMessageServiceContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helpers\Helper;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use App\Libraries\PusherFactory;
use App\Services\BookingAvaibilityServiceContract;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PropertyOfferController extends Controller
{
    public $propertyOfferServiceContract;



    public function __construct(
        PropertyOfferServiceContract  $propertyOfferServiceContract,
        SendMessageServiceContract $sendMessageServiceContract,
        BookingAvaibilityServiceContract $bookingAvaibilityServiceContract
    )
    {
        $this->propertyOfferServiceContract = $propertyOfferServiceContract;
        $this->sendMessageService = $sendMessageServiceContract;
        $this->bookingAvaibilityServiceContract = $bookingAvaibilityServiceContract;
    }

    public function getMyOffers(
        $tripperId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->getTripperPropertiesOffer($tripperId);
        $properties = PropertyOffer::collection($ownerOfferedProperties);
        return $this->returnResponseWithData($properties);
    }

    public function getPropertyOfferById(
        $tripperId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->getPropertyOfferById($propertyOfferId);
        $property = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($property);
    }

    public function counterPropertyOffer(
        $tripperId,$propertyId,$propertyOfferId,CounterOfferRequest $request
    )
    {
        try {
            $requestDTO = MakeOfferDTO::convertPostDataToObject($request);
            $requestDTO->rentalId = $tripperId;
            $requestDTO->propertyId = $propertyId;

            $ownerOfferedProperties = $this->propertyOfferServiceContract->tripperCounterOfferProperty(
                $requestDTO,$propertyOfferId
            );
            $propertyOffer = new PropertyOffer($ownerOfferedProperties);
            return $this->returnResponseWithData($propertyOffer);
        } catch (\Exception $exception) {
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

    public function makeAnOffer(
        $tripperId,$propertyId,MakeOfferRequest $request
    )
    {
        $requestDTO = MakeOfferDTO::convertPostDataToObject($request);
        $requestDTO->rentalId = $tripperId;
        $requestDTO->propertyId = $propertyId;

        $bookingList = $this->bookingAvaibilityServiceContract->checkBookings($propertyId,$requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate);
        if(!empty($bookingList->toarray())){
            return $this->renderData(['success'=>false ,'type' => 1 , 'error'=> "This stay is not available for the above selected dates. Please change the dates."]);
        }

        $propertyAvailability = $this->bookingAvaibilityServiceContract->propertyAvailablity($propertyId,$requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate);
        if(empty($propertyAvailability->toarray())){
            return $this->renderData(['success'=>false ,'type' => 2 , 'error'=> "This stay is not available for the above selected dates. Please change the dates."]);
        }

        /**
         * @var PropertyOffers $isOfferAlreadyExist
         */
        $isOfferAlreadyExist = $this->propertyOfferServiceContract->checkOfferDateIsOverLappingByTripperId(
            $requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate,$tripperId,$propertyId
        );

        if($isOfferAlreadyExist->count() > 0){
            $myOfferLink = '<a href="'.route('web.tripper.myOffers').'"><b>View Offers</b></a>';
            $statusMessage = "You already have a pending offer between these dates. Click to  $myOfferLink.";
            return $this->renderData(['success'=> false, 'type' => 3 , 'error'=> $statusMessage ]);
        }

        $ownerOfferedProperties = $this->propertyOfferServiceContract->tripperMakeAnOfferProperty(
            $requestDTO
        );
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);

        if($request->post('message')) {
            /* Send Message */
            $toUser = Helper::urlsafe_b64encode($requestDTO->ownerId);
            $userMessage = $request->post('message');

            $this->sendMessageService->postRentalOwnerSendMessage($toUser,$userMessage);
            /* End of Send Message */
        }

        return $this->returnResponseWithData($propertyOffer);
    }

    public function makeAnOfferOnExistingOffer(
        $tripperId,$propertyId,$propertyOfferId,MakeOfferRequest $request
    )
    {
        try{
            $requestDTO = MakeOfferDTO::convertPostDataToObject($request);
            $requestDTO->rentalId = $tripperId;
            $requestDTO->propertyId = $propertyId;
            $requestDTO->id = $propertyOfferId;

            $bookingList = $this->bookingAvaibilityServiceContract->checkBookings($propertyId,$requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate);
            if(!empty($bookingList->toarray())){
                return $this->returnResponseWithoutData(false,"This stay is not available for the above selected dates. Please change the dates.");
            }

            $propertyAvailability = $this->bookingAvaibilityServiceContract->propertyAvailablity($propertyId,$requestDTO->tripperOfferStartDate,$requestDTO->tripperOfferEndDate);
            if(empty($propertyAvailability->toarray())){
                return $this->returnResponseWithoutData(false,"This stay is not available for the above selected dates. Please change the dates.");
            }

            $ownerOfferedProperties = $this->propertyOfferServiceContract->tripperMakeAnOfferOnExistingOffer(
                $requestDTO,$propertyOfferId
            );

            $propertyOffer = new PropertyOffer($ownerOfferedProperties);

            return $this->returnResponseWithData($propertyOffer);
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

    public function deleteAnPropertyOffer(
        $tripperId,$propertyId,MakeOfferRequest $request
    )
    {
        $requestDTO = MakeOfferDTO::convertPostDataToObject($request);
        $requestDTO->rentalId = $tripperId;
        $requestDTO->propertyId = $propertyId;

        $this->propertyOfferServiceContract->deleteTripperPropertyOffer(
            $requestDTO
        );

        return $this->returnResponseWithoutData(
            true,
            'Successfully Deleted',
            Response::HTTP_OK
        );

    }

    public function cancelPropertyOffer(
        $tripperId,$propertyId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->cancelTripperPropertyOffer(
            $propertyOfferId
        );
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffer);
    }


}
