<?php

namespace App\Http\Controllers\Api\Tripper;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\ReadNotificationDTO;
use App\Notifications\PropertyOffer\Owner\OwnerAcceptedPropertyOfferNotification;
use Illuminate\Http\Request;

class ReadNotificationController extends Controller
{
    public function readOfferAcceptedNotifications(Request $request) {
        try{
            $readNotificationDTO = ReadNotificationDTO::convertToObj($request);
            getLoggedInRental()->unreadNotifications
                ->whereIn('type',[OwnerAcceptedPropertyOfferNotification::class])
                ->markAsRead();

            return $this->returnResponseWithoutData(true,'All '.$readNotificationDTO->type.' has been read');
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }
}
