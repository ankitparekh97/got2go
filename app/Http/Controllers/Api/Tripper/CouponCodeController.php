<?php

namespace App\Http\Controllers\Api\Tripper;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\CouponCodeDTO;
use App\Http\Resources\CouponCode\CouponCodeResource;
use App\Services\CouponCodeContract;
use Illuminate\Http\Request;

class CouponCodeController extends Controller
{
    public $couponCodeService;

    public function __construct(
        CouponCodeContract  $couponCodeService
    )
    {
        $this->couponCodeService = $couponCodeService;
    }

    public function applyCouponCode($rentalId,$couponCode)
    {
        try{
            $couponCode = $this->couponCodeService->applyCouponCode($couponCode,$rentalId);
            return $this->returnResponseWithData($couponCode,'Available');
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }
}
