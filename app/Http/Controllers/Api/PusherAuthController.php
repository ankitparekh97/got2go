<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\PusherFactory;
use Illuminate\Http\Request;

class PusherAuthController extends Controller
{
    public $pusher;

    public function __construct()
    {
        $this->pusher = PusherFactory::make();
    }

    public function authOwnerForPrivateChannel(Request $request)
    {
        $this->middleware = array('jwtowner.verify:owner');

        $socketId = $request->post('socket_id');
        $channelName = $request->post('channel_name');

        return $this->pusher->socket_auth($channelName,$socketId);
    }

    public function authTripperForPrivateChannel(Request $request)
    {
        $this->middleware = array('jwtrental.verify:rentaluser');

        $socketId = $request->post('socket_id');
        $channelName = $request->post('channel_name');

        return $this->pusher->socket_auth($channelName,$socketId);
    }

}
