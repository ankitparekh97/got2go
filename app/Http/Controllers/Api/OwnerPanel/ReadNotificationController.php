<?php

namespace App\Http\Controllers\Api\OwnerPanel;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\ReadNotificationDTO;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnCounterPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferOnExistingOfferNotification;
use Illuminate\Http\Request;

class ReadNotificationController extends Controller
{
    public function readMakeAnOfferAndCounterOfferNotifications(Request $request) {
        try{
            $readNotificationDTO = ReadNotificationDTO::convertToObj($request);
            getOwner()->unreadNotifications
                ->whereIn('type',[
                    TripperMadeAnPropertyOfferNotification::class,
                    TripperMadeAnCounterPropertyOfferNotification::class,
                    TripperMadeAnPropertyOfferOnExistingOfferNotification::class
                ])
                ->markAsRead();

            return $this->returnResponseWithoutData(true,'All '.$readNotificationDTO->type.' has been read');
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }
}
