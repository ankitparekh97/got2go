<?php

namespace App\Http\Controllers\Api\OwnerPanel;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\MakeOfferDTO;
use App\Http\Requests\Api\Owner\PropertyOffer\CounterOfferRequest;
use App\Http\Resources\PropertyOffer\PropertyOffer;
use App\Services\PropertyOfferServiceContract;
use Illuminate\Http\Request;

class PropertyOfferController extends Controller
{
    public $propertyOfferServiceContract;

    public function __construct(
        PropertyOfferServiceContract  $propertyOfferServiceContract
    )
    {
        $this->propertyOfferServiceContract = $propertyOfferServiceContract;
    }

    public function getRequestedOffers(
        $ownerId,Request $request
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->getOwnerPropertiesOffer($ownerId);
        $propertyOffers = PropertyOffer::collection($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffers);
    }

    public function acceptPropertyOffer(
        $ownerId,$propertyId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->acceptOwnerPropertyOffer($propertyOfferId);
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffer);
    }

    public function declinePropertyOffer(
        $ownerId,$propertyId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->declineOwnerPropertyOffer($propertyOfferId);
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffer);
    }

    public function getPropertyOfferById(
        $ownerId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->getPropertyOfferById($propertyOfferId);
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffer);
    }

    public function counterPropertyOffer(
        $ownerId,$propertyId,$propertyOfferId,CounterOfferRequest $request
    )
    {
        try {

            $requestDTO = MakeOfferDTO::convertPostDataToObject($request);
            $requestDTO->ownerId = $ownerId;
            $requestDTO->propertyId = $propertyId;
            $ownerOfferedProperties = $this->propertyOfferServiceContract->ownerCounterOfferProperty(
                $requestDTO, $propertyOfferId
            );
            $propertyOffer = new PropertyOffer($ownerOfferedProperties);
            return $this->returnResponseWithData($propertyOffer);

        } catch (\Exception $exception) {
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

    public function cancelPropertyOffer(
        $ownerId,$propertyId,$propertyOfferId
    )
    {
        $ownerOfferedProperties = $this->propertyOfferServiceContract->cancelOwnerOPropertyOffer(
            $propertyOfferId
        );
        $propertyOffer = new PropertyOffer($ownerOfferedProperties);
        return $this->returnResponseWithData($propertyOffer);
    }


}
