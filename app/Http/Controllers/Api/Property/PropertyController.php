<?php

namespace App\Http\Controllers\Api\Property;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Services\BookingAvaibilityServiceContract;

class PropertyController extends Controller
{
    

    public function __construct(
        BookingAvaibilityServiceContract $bookingAvaibilityServiceContract
    )
    {
        $this->bookingAvaibilityServiceContract = $bookingAvaibilityServiceContract;
    }

    public function checkAvailability($propertyId, $checkInDate, $checkOutDate)
    {
        $requestValidation = ['id'=>$propertyId];
        $validator = Validator::make($requestValidation,[
            'id' => 'required|exists:property,id'
        ]);

        if($validator->fails()){
            $success = false;
            $message = 'Forbidden';
        }

        $bookingList = $this->bookingAvaibilityServiceContract->checkBookings($propertyId,$checkInDate,$checkOutDate);
        $propertyAvailability = $this->bookingAvaibilityServiceContract->propertyAvailablity($propertyId,$checkInDate,$checkOutDate);

        $success = true;
        $message = 'Available';
        if(!empty($bookingList->toarray()) || empty($propertyAvailability->toarray())) {
            $success = false;
            $message = 'Not Available';
        }

        return $this->returnResponseWithoutData(
            $success,
            $message,
            Response::HTTP_OK
        );
    }
}
