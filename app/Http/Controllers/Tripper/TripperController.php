<?php

namespace App\Http\Controllers\Tripper;

use App\Http\Controllers\Controller;
use App\Models\PaymentCards;
use App\Models\RentalUser;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use App\Services\PaymentServices\Braintree\BraintreePaymentService;


use Carbon\Carbon;
use Exception;
use JWTAuth;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;
use App\Models\Subscription;
use App\Services\Renter\TripperServiceContract;

class TripperController extends Controller
{



   protected $braintreePaymentService;

    protected $rentalRepository;

    public function __construct(
        RentalInterface $rentalRepository,
        TripperServiceContract $tripperServiceContract
    )
    {
        $this->braintreePaymentService = getObjectBraintree();
        $this->rentalRepository = $rentalRepository;
        $this->tripperServiceContract = $tripperServiceContract;
    }

    public function index(){
        $subscription = $this->tripperServiceContract->getSubscription();
        $this->view = 'tripper.tripper';
        return $this->renderData(['subscription'=>$subscription]);
    }

    public function registerForm()
    {
        try{

            if(Session::get('userEmail')==''){
                return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
            }

            return view('tripper.sign-up');

        } catch(Exception $exception){
            Log::error($exception->getMessage());
            return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
        }

    }

    public function activation()
    {

        try{

            if(Session::get('userEmail')==''){
                return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
            }

            return view('tripper.activation');

        } catch(Exception $exception){
            Log::error($exception->getMessage());
            return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
        }
    }

    public function singUpForm()
    {
        try {
            $subscription = $this->tripperServiceContract->getSubscription();
            $rentalUser = getLoggedInRental();

            if($rentalUser!='')
            {
                if(isLoggedInUserIsTripper() && isUserOtpVerified() == "0"){
                    return Redirect::route('tripper.activation')->with(['error' => 'You are not authorized to view this page']);
                }
                $this->view = 'tripper.register';
                return $this->renderData(['subscription'=>$subscription,'user'=>$rentalUser,'id'=>$rentalUser->id]);
            }
        } catch(Exception $exception){
            Log::error($exception->getMessage());
            return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
        }

    }

    public function regSucess()
    {
        try {

            $rentalUser = getLoggedInRental();
            $id = $rentalUser->id;
            if($rentalUser->is_otp_verified == "0"){
                return Redirect::route('tripper.activation')->with(['error' => 'You are not authorized to view this page']);
            }
            Session::forget('wasRecentlyCreated');
            return view('tripper.registrationSucess', compact('id'));

        } catch(Exception $exception){
            Log::error($exception->getMessage());
            return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
        }
    }

    public function dashboard()
    {
        try {

            $user = Auth::guard('rentaluser')->user();
            if($user->is_otp_verified == "0"){
                return Redirect::route('tripper.activation')->with(['error' => 'You are not authorized to view this page']);
            } else if($user->paymentcards_user == null){
                return Redirect::route('tripper.singUpForm')->with(['error' => 'You are not authorized to view this page']);
            }

            return view('tripper.dashboard');

        } catch(Exception $exception){
            Log::error($exception->getMessage());
            return Redirect::route('tripper.index')->with(['error' => 'You are not authorized to view this page']);
        }

    }

    public function myOffers()
    {
        return view('tripper.my-offer');
    }
}
