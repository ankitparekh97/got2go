<?php

namespace App\Http\Controllers\Tripper;

use App\Http\Controllers\Controller;
use App\Repositories\RentalInterface;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;
use App\Services\SubscriptionPlanContract;
use Illuminate\Http\Request;
use App\Services\Renter\SubscriptionServiceContract;
use Validator;
use Illuminate\Validation\Rule;

class SubscriptionController extends Controller
{
    public $rentalRepository;

    public function __construct(
        RentalInterface $rentalRepository,
        SubscriptionPlanContract $subscriptionPlanService,
        PaymentServiceInterface $paymentService,
        SubscriptionServiceContract $subscriptionServiceContract
    )
    {
        $this->rentalRepository = $rentalRepository;
        $this->subscriptionServiceContract = $subscriptionServiceContract;
    }

    /**
     * @param $tripperId
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscriptionCancel($tripperId)
    {
        $rentalUser = getLoggedInRental();
        $requestCheck = ['rental_id'=>500];
        $validator = Validator::make($requestCheck,[
            'rental_id' => ['required',
                        Rule::exists('rental_user_subscription')->where(function ($query) use($requestCheck) {
                            $query->where($requestCheck);
                        })
                    ]    
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>"something went wrong. Please Try again."]);
        }
        try {

            /**
             * @var SubscriptionResponseDTO $cancelPaymentSubscription
             */
            $this->subscriptionServiceContract->cancelPaymentGatewaySubscription($tripperId);

            return response()->json(['success'=>true,'message'=>'Tripper subscription cancelled successfully']);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

}
