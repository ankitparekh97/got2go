<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MasterAmenities;
use Validator;
use Session;
class MasterAmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $amenities = MasterAmenities::all();
        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$amenities->toArray()]);
        }else{
            return view('admin.amenity.index',compact('amenities'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.amenity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'amenity_name'=>'required|unique:master_amenities',
            'type'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if(MasterAmenities::create($data)){

            Session::flash('success', 'Amenity saved successfully!');
            return response()->json(['success'=>true,'redirect_url'=>url('/amenity')]);
        }
        else{
            return response()->json(['success'=>false]);
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $amenities = MasterAmenities::find($id);
        return view('admin.amenity.view',compact('amenities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $amenities = MasterAmenities::find($id);
        return view('admin.amenity.edit',compact('amenities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'amenity_name'=>'required|unique:master_amenities,amenity_name,'.$id,
            'type'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        unset($data['_method']);
        unset($data['_token']);
        MasterAmenities::where('id',$id)->update($data);

        Session::flash('success', 'Amenity updated successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/amenity')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $amenity = MasterAmenities::find($id);
        $amenity->delete();
        return response()->json(['success'=>true]);
    }
}
