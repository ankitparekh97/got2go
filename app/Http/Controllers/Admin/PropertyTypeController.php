<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PropertyType;
use Validator;
use Session;

class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $propertiesType = PropertyType::all();
        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$propertiesType->toArray()]);
        }else{
            return view('admin.property_type.index',compact('propertiesType'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.property_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'property_type'=>'required|unique:property_type',
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if(PropertyType::create($data)){

            Session::flash('success', 'Property Type saved successfully!');
            return response()->json(['success'=>true,'redirect_url'=>url('/propertytype')]);
        }
        else{
            return response()->json(['success'=>false]);
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $propertyType = PropertyType::find($id);
        return view('admin.property_type.view',compact('propertyType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $propertyType = PropertyType::find($id);
        return view('admin.property_type.edit',compact('propertyType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'property_type'=>'required|unique:property_type,property_type,'.$id,
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        unset($data['_method']);
        unset($data['_token']);
        PropertyType::where('id',$id)->update($data);

        Session::flash('success', 'Property Type updated successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/propertytype')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = PropertyType::find($id);
        $property->delete();
        return response()->json(['success'=>true]);
    }
}
