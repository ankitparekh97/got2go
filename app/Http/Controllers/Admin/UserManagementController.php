<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Owner;
use App\Models\Property;
use Image;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;

use App\Services\UserManagementContract;

class UserManagementController extends Controller
{
    const OWNER_USER_ID = 'owner.id';

    public function __construct(
        UserManagementContract  $userManagementService
    )
    {
        $this->userManagementService = $userManagementService;
    }

    public function userList(Request $request)
    {
        if($request->ajax())
        {
           try {
            $userList = $this->userManagementService->userList();

            return datatables()->of($userList)
            ->editColumn('id', function ($userList) {
                return $userList->id;
            })
            ->editColumn('propertyId', function ($userList) {
                return $userList->propertyId;
            })
            ->editColumn('name', function ($userList) {
                return $userList->first_name . ' ' . $userList->last_name;
            })
            ->editColumn('email', function ($userList) {
                return $userList->email;
            })

            ->addColumn('property', function ($userList) {
                return $userList->total_property;
            })
            ->editColumn('status', function ($userList) {
                if($userList->status == 'approved'){
                    $status = 'Activated';
                } else if($userList->status == 'pending'){
                    $status = 'Awaiting Approval';
                } else if ($userList->status == 'decline') {
                    $status = 'Deactivated';
                }
                return $status;
            })
            ->addColumn('action', function ($userList) {
                if($userList->status == 'approved') {
                    $status = '<div class="action-tool"><a href="javascript:void(0)" class="switch-btn" data-toggle="modal" data-target="#deactivate" onclick = "deactivate('.$userList->id.', '.$userList->propertyId.')">
                                <label class="switch-custom">
                                <input type="checkbox" checked>
                                <span class="slider round"></span>
                                <span class="absolute-no">OFF</span>
                            </label>
                        </a>';
                    $status .= '&nbsp;<a id="view_profile" href="javascript:void(0)" class="btn btn-yellow btn-yellowicon" title=""><i class="got got-user" title="Owner profile"></i></a> </div>';
                } else if($userList->status == 'pending') {
                    $status = '<div class="action-tool"><a href="javascript:void(0)" data-toggle="modal" class="btn  btn-yellow" data-target="#activate" data-target="#activate" onclick = "activate('.$userList->id.', '.$userList->propertyId.')">Approve Owner</a>';
                    $status .= '&nbsp;<a id="view_profile" href="javascript:void(0)" class="btn btn-yellow btn-yellowicon"><i class="got got-user" title="Owner profile"></i></a></div>';
                } else if ($userList->status == 'decline') {
                    $status = '<div class="action-tool"><a href="javascript:void(0)" class="switch-btn" data-toggle="modal" data-target="#activate" onclick = "activate('.$userList->id.', '.$userList->propertyId.')">
                            <label class="switch-custom">
                                <input type="checkbox">
                                <span class="slider round"></span>
                                <span class="absolute-no">OFF</span>
                            </label>
                        </a></div>';
                }
                return $status;
            })
            ->rawColumns(['','id','name','email', 'property', 'status','action','ownerpic'])
            ->make(true);
                    } catch (\Throwable $th) {
                        return response()->json(['success'=>false,'error'=>$th->getMessage()]);
                    }
                }
        return view('admin.usermanagement.list');
    }

    public function userActivate (Request $request) {
        //try {
            $request->validate([
                'id' => 'required|numeric',
                'propertyid' => 'nullable|numeric'
            ]);

            $userId = $request->id;
            $propertyId = $request->propertyid;
            $status = $request->status;
            $this->userManagementService->userActivate($userId,$propertyId,$status);
            $msg = '';
            if($status == 'decline'){
                $msg = 'Owner and all of their listings successfully deactivated';
            }elseif($status == 'approved'){
                $msg = 'Owner successfully activated';
            }
            return response()->json(['success'=>true,'message'=>$msg]);
        // } catch (\Throwable $th) {
        //     return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        // }
    }

    public function ownerProperty (Request $request) {
        try {

            $request->validate([
                'id' => 'required|numeric'
            ]);

            $ownerId = $request->id;
            $propertList = $this->userManagementService->getownerPropertyById($ownerId);
           return response()->json(['success'=>true,'propertylist'=>$propertList->toArray()]);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }
}
