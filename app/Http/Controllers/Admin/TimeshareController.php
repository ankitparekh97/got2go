<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;
use App\Services\SendMessageServiceContract;
use App\Services\TimeShareContract;

class TimeshareController extends Controller
{

    public $timeShareService;

    public function __construct(
        SendMessageServiceContract $sendMessageServiceContract,
        TimeShareContract  $timeShareService
    )
    {
        $this->sendMessageService = $sendMessageServiceContract;
         $this->timeShareService = $timeShareService;
    }

    //To get list of property in admin panel
    //TODO: Optimize Query, sanitised user input
    public function timeShareList(Request $request,$type=NULL) {

        if($request->ajax())
        {
           try {
                $transactionLogs = $this->timeShareService->timeShareList();

                return datatables()->of($transactionLogs)
                ->editColumn('id', function ($transactionLogs) {
                    return $transactionLogs->id;
                })
                ->addColumn('property_name', function ($transactionLogs) {
                    return '<a class="custom-link-blue" href=' .URL("propertydetail/".$transactionLogs->id).' target="_blank">'.$transactionLogs->title.'</a>';
                })
                ->addColumn('owner', function ($transactionLogs) {
                    return $transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName;
                })
                ->addColumn('email', function ($transactionLogs) {
                    return $transactionLogs->email;
                })
                ->addColumn('ownershipDoc', function ($transactionLogs) use($type){
                    return '<a class="custom-link-blue" href="#" onClick="view_document(\'property\','.$transactionLogs->id.')">View Document(s)</a>';
                })
                ->addColumn('timeshare_member_ID', function ($transactionLogs) {
                    return '<a class="custom-link-blue" href="#" onClick="view_document(\'timeshare\','.$transactionLogs->id.')">View Document(s)</a>';
                })
                ->editColumn('status', function ($transactionLogs) {
                    $status ='';
                    if($transactionLogs->status=='approved'){
                        $status = 'Approved';
                    } else if($transactionLogs->status=='pending'){
                        $status = 'Pending';
                    } else if($transactionLogs->status=='rejected'){
                        $status = 'Denied';
                    } else if($transactionLogs->status=='draft'){
                        $status = 'Draft';
                    }
                    return $status;
                })
                ->addColumn('action', function ($transactionLogs) {
                    if($transactionLogs->status =='pending'){
                        $status = '<a href="javascript:void(0)" data-toggle="modal" class="btn button-default-custom btn-approve-custom" data-target="#approve" onclick = "approveproperty('.$transactionLogs->id.')">Approve</a>';
                        $status .= '&nbsp;&nbsp;<a href="javascript:void(0)" data-toggle="modal" class="btn button-default-custom btn-deny-custom" data-target="#deny" onclick = "denyproperty('.$transactionLogs->id.')">Deny</a>';
                    } else if($transactionLogs->status =='rejected'){
                        $status = '';
                    } else {
                        $status = '';
                    }
                    return $status;
                })
                ->rawColumns(['id','property_name','email', 'owner', 'ownershipDoc', 'timeshare_member_ID','status','action'])
                ->make(true);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'error'=>$th->getMessage()]);
            }
        }
        return view('admin.timeshare.list');
    }

    //Bulck Upload listin in admin panel
    //TODO: Optimize Query, sanitised user input
    public function BulkTimeshareList(Request $request) {

        if($request->ajax())
        {
           try {
                $transactionLogs = $this->timeShareService->bulkTimeshareList();

                return datatables()->of($transactionLogs)
                ->editColumn('id', function ($transactionLogs) {
                    return $transactionLogs->id;
                })
                ->addColumn('owner', function ($transactionLogs) {
                    return $transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName;
                })
                ->addColumn('email', function ($transactionLogs) {
                    return $transactionLogs->email;
                })
                ->addColumn('govID', function ($transactionLogs) {
                    return '<a class="custom-link-blue" href="#" onClick="view_document(\'timeshare\','.$transactionLogs->id.')">View Document</a>';
                })
                ->editColumn('status', function ($transactionLogs) {
                    $status ='';
                    if($transactionLogs->status=='approved'){
                        $status = 'Approved';
                    } else if($transactionLogs->status=='pending'){
                        $status = 'Pending';
                    } else if($transactionLogs->status=='rejected'){
                        $status = 'Denied';
                    } else if($transactionLogs->status=='draft'){
                        $status = 'Draft';
                    }
                    return $status;
                })
                ->addColumn('action', function ($transactionLogs) {
                    if($transactionLogs->status =='pending'){
                        $status = '<a href="javascript:void(0)" data-toggle="modal" class="btn button-default-custom btn-approve-custom" data-target="#approve_all" onclick = "approveproperty_all(\''.$transactionLogs->bulkupload_uniquekey.'\',\''.$transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName.'\',\''.$transactionLogs->listings.'\')">Approve All</a>';
                        $status .= '&nbsp;&nbsp;<a href="javascript:void(0)" data-toggle="modal" class="btn button-default-custom btn-approve-custom" data-target="#deny_all" onclick = "denyproperty_all(\''.$transactionLogs->bulkupload_uniquekey.'\',\''.$transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName.'\',\''.$transactionLogs->listings.'\')">Deny All</a>';
                        $status .= '&nbsp;&nbsp;<a href="javascript:void(0)" data-toggle="modal" class="btn button-default-custom btn-deny-custom" data-target="#individual" onclick = "review_all_individually(\''.$transactionLogs->bulkupload_uniquekey.'\',\''.$transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName.'\',\''.$transactionLogs->listings.'\')">Individually Reiew</a>';
                    } else if($transactionLogs->status =='rejected'){
                        $status = '';
                    } else {
                        $status = '';
                    }
                    return $status;
                })
                ->rawColumns(['id','email', 'owner', 'govID', 'status','action'])
                ->make(true);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'error'=>$th->getMessage()]);
            }
        }
        return view('admin.timeshare.BulkTimeshareList');
    }

    //Approve/Deny Property listing
    public function propertyApprove(Request $request) {
        try {

            $request->validate([
                'id' => 'required|numeric',
                'action' => 'required'
            ]);
            $input = $request->all();
            $properties = $this->timeShareService->propertyApprove($input);
            $status = $request->action;
            if($request->reason_message) {
                $request['to_user'] = Helper::urlsafe_b64encode($properties->owner_id);
                $this->postSendMessage($request);
            }
            return response()->json(['success'=>true,'message'=>'Property status '.$status.' successfully', 'status'=>$status]);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    //Send Message to Owner why listing is denied
    //TODO: Need to check can make common function for this
    public function postSendMessage($request)
    {
        $request['message'] = $request->reason_message;
        /* Send Message */
        $toUser = Helper::urlsafe_b64encode($request->id);
        $userMessage = $request->reason_message;
        $this->sendMessageService->postAdminOwnerSendMessage($toUser,$userMessage);
        /* End of Send Message */
        return response()->json(['success'=>true]);
    }

    //Documnent View
    public function view_document($type,$id){
        $Doc = $this->timeShareService->viewDocument($type,$id);
        return view('admin.timeshare.view_document',compact('Doc'));
    }

    //Approve all bulck upload property
    public function propertyApproveAll(Request $request) {
        try {
            $request->validate([
                'bulkupload_uniquekey' => 'required',
                'action' => 'required'
            ]);
            $input = $request->all();
            $properties = $this->timeShareService->propertyApproveAll($input);
            $status = $request->action;
            if($request->reason_message) {
                $request['to_user'] = Helper::urlsafe_b64encode($properties->owner_id);
                $this->postSendMessage($request);
            }
            return response()->json(['success'=>true,'message'=>'Property status '.str_replace('_',' ',$status).' successfully', 'status'=>$status]);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }
}
