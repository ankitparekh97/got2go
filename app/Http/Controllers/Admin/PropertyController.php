<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\OwnerProperty;
use App\PropertyAmenities;
use App\PropertyImages;
use App\PropertyBedrooms;
use File;
use Validator;
use Session;
use Carbon\Carbon;

class PropertyController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $properties = Property::with(['ownerproperty','propertyimages','propertybedrooms','propertyamenities'])->where(['type_of_property'=>'property'])->get();
        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$properties->toArray()]);
        }else{
            return view('admin.property.index',compact('properties'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // echo 'hi';
        return view('admin.property.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'owner_id'=>'required',
            'title' => 'required',
            'property_type_id' => 'required',
            'description'=>'required',
            'guest_will_have'=>'required',
            'location_detail'=>'required',
            'common_room'=>'required|numeric',
            'sofa'=>'required|numeric',
            'common_bathroom'=>'required|numeric',
            'bathroom.*'=>'required|numeric',
            'bed.*'=>'required|numeric',
            'bed_type.*'=>'required|numeric',
            'guest_per_room.*'=>'required|numeric',
            'proof_of_ownership_or_lease'=>'required',
            'price'=>'required|numeric',
            'service_fees'=>'required|numeric',
            'occupancy_taxes_and_fees'=>'required|numeric',
            'is_instant_booking_allowed'=>'required',
            'is_partial_payment_allowed'=>'required',
            'is_partial_payment_allowed'=>'required',
            'partial_payment_amount'=>'numeric',
            'check_in'=>'required',
            'check_out'=>'required',
            'cancellation_type'=>'required',
            'cover_photo'=>'required',
            'video'=>'required',
            'aminities.*'=>'required',
            'images.*'=>'required',
            'is_for_guest'=>'required',
            'list_as_company'=>'required'

        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        if($request->hasFile('proof_of_ownership_or_lease')){
            $fileName = 'property_proof_'.time().'.'.$request->file('proof_of_ownership_or_lease')->extension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('proof_of_ownership_or_lease')->move($path, $fileName);
             $request['proof_of_ownership_or_lease'] = $proof_of_ownership_or_lease = $fileName;

        }
        if($request->hasFile('cover_photo')){
            $fileName = 'property_photo_'.time().'.'.$request->file('cover_photo')->getClientOriginalExtension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('cover_photo')->move($path, $fileName);
            $request['cover_photo'] = $photo = $fileName;
        }

        if($request->hasFile('video')){
            $fileName = 'property_video_'.time().'.'.$request->file('video')->extension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('video')->move($path, $fileName);
            unset($request['video']);
            $request->video = $video = $fileName;
        }
        //dd($request->owner);
        $data = array(
            'owner_id'=>$request->owner_id,
            'property_type_id'=>$request->property_type_id,
            'description'=>$request->description,
            'location_detail'=>$request->location_detail,
            'price'=>$request->price,
            'service_fees'=>$request->service_fees,
            'occupancy_taxes_and_fees'=>$request->occupancy_taxes_and_fees,
            'is_partial_payment_allowed'=>$request->is_partial_payment_allowed,
            'partial_payment_amount'=>$request->partial_payment_amount,
            'cancellation_type'=>$request->cancellation_type,
            'type_of_property'=>'property',
            'status'=>'approved'

        ); 

        if($property = Property::create($data)){

            OwnerProperty::create([
                'property_id'=>$property->id,
                'title'=>$request->title,
                'is_for_guest'=>$request->is_for_guest,
                'guest_will_have'=>$request->guest_will_have,
                'list_as_company'=>$request->list_as_company,
                'common_room'=>$request->common_room,
                'sofa'=>$request->sofa,
                'common_bathroom'=>$request->common_bathroom,
                'proof_of_ownership_or_lease'=>$proof_of_ownership_or_lease,
                'is_instant_booking_allowed'=>$request->is_instant_booking_allowed,
                'check_in'=>$request->check_in,
                'check_out'=>$request->check_out,
                'cover_photo'=>$photo,
                'video'=>$video,
            ]);

            if($request->hasFile('images')){

            foreach($request->file('images') as $image){
                $fileName = 'property_photo_'.time().'.'.$image->getClientOriginalExtension();

                $path = public_path('uploads/property');

                if(!File::isDirectory($path)){

                    File::makeDirectory($path, 0777, true, true);

                }
                $image->move($path, $fileName);
                $image_array[] = [
                    'property_id'=>$property->id,
                    'photo'=>$fileName,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ];
            }

             PropertyImages::insert($image_array);
            
        }

        if($request->has('bed')){

            $bathroom = $request->bathroom;
            $bed = $request->bed;
            $bed_type = $request->bed_type;
            $guest_per_room = $request->guest_per_room;
            foreach($bed as $key=>$value){
                $bedroom_array[] = [
                    'property_id'=>$property->id,
                    'bathroom'=>$bathroom[$key],
                    'bed'=>$bed[$key],
                    'bed_type'=>$bed_type[$key],
                    'guest_per_room'=>$guest_per_room[$key],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ];
            }

             PropertyBedrooms::insert($bedroom_array);
            
        }

        if($request->has('aminities')){

            $aminities = $request->aminities;
            foreach($aminities as $key=>$value){
                $aminities_array[] = [
                    'property_id'=>$property->id,
                    'property_type'=>'property',
                    'amenity_id'=>$aminities[$key],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                    'amenities_type'=>'unit'
                ];
            }

             PropertyAmenities::insert($aminities_array);
            
        }



            Session::flash('success', 'Property saved successfully!');
            return response()->json(['success'=>'yes','redirect_url'=>url('/property')]);
        }
        else{
            return response()->json(['success'=>false]);
         }
        }
            

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property,$id)
    {
        $property = Property::with(['ownerproperty','propertyimages','propertybedrooms','propertyamenities'])->where(['id'=>$id])->first();
        return view('admin.property.view',compact('property'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property,$id)
    {
         $property = Property::with(['ownerproperty','propertyimages','propertybedrooms','propertyamenities'])->where(['id'=>$id])->first();
        return view('admin.property.edit',compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        //dd($request->all());
        $validator = \Validator::make($request->all(),[
            'owner_id'=>'required',
            'title' => 'required',
            'property_type_id' => 'required',
            'description'=>'required',
            'guest_will_have'=>'required',
            'location_detail'=>'required',
            'common_room'=>'required|numeric',
            'sofa'=>'required|numeric',
            'common_bathroom'=>'required|numeric',
            'bathroom.*'=>'required|numeric',
            'bed.*'=>'required|numeric',
            'bed_type.*'=>'required|numeric',
            'guest_per_room.*'=>'required|numeric',
            //'proof_of_ownership_or_lease'=>'required',
            'price'=>'required|numeric',
            'service_fees'=>'required|numeric',
            'occupancy_taxes_and_fees'=>'required|numeric',
            'is_instant_booking_allowed'=>'required',
            'is_partial_payment_allowed'=>'required',
            'is_partial_payment_allowed'=>'required',
            'partial_payment_amount'=>'numeric',
            'check_in'=>'required',
            'check_out'=>'required',
            'cancellation_type'=>'required',
            //'photo'=>'required',
            //'video'=>'required',

        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()->all()]);
        }


        if($request->hasFile('proof_of_ownership_or_lease')){
            $fileName = 'property_proof_'.time().'.'.$request->file('proof_of_ownership_or_lease')->extension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('proof_of_ownership_or_lease')->move($path, $fileName);
             $request['proof_of_ownership_or_lease'] = $proof_of_ownership_or_lease = $fileName;

        }
        if($request->hasFile('cover_photo')){
            $fileName = 'property_photo_'.time().'.'.$request->file('cover_photo')->getClientOriginalExtension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('cover_photo')->move($path, $fileName);
            $request['cover_photo'] = $photo = $fileName;
        }

        if($request->hasFile('video')){
            $fileName = 'property_video_'.time().'.'.$request->file('video')->extension();

            $path = public_path('uploads/property');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('video')->move($path, $fileName);
            unset($request['video']);
            $request->video = $video = $fileName;
        }

        
        $data = array(
            'owner_id'=>$request->owner_id,
            'property_type_id'=>$request->property_type_id,
            'description'=>$request->description,
            'location_detail'=>$request->location_detail,
            'price'=>$request->price,
            'service_fees'=>$request->service_fees,
            'occupancy_taxes_and_fees'=>$request->occupancy_taxes_and_fees,
            'is_partial_payment_allowed'=>$request->is_partial_payment_allowed,
            'partial_payment_amount'=>$request->partial_payment_amount,
            'cancellation_type'=>$request->cancellation_type,
            'type_of_property'=>'property',

        ); 
        $update_data = array(
            'title'=>$request->title,
            'is_for_guest'=>$request->is_for_guest,
            'guest_will_have'=>$request->guest_will_have,
            'list_as_company'=>$request->list_as_company,
            'common_room'=>$request->common_room,
            'sofa'=>$request->sofa,
            'common_bathroom'=>$request->common_bathroom,
            'is_instant_booking_allowed'=>$request->is_instant_booking_allowed,
            'check_in'=>$request->check_in,
            'check_out'=>$request->check_out
        );

         if(isset($proof_of_ownership_or_lease))
            $update_data['proof_of_ownership_or_lease']=$proof_of_ownership_or_lease;
        if(isset($video))
            $update_data['video']=$video;
        if(isset($photo))
            $update_data['cover_photo']=$photo;

        Property::where('id',$id)->update($data);
        OwnerProperty::where('property_id',$id)->update($update_data);

        if($request->hasFile('images')){

            foreach($request->file('images') as $image){
                $fileName = 'property_photo_'.time().'.'.$image->getClientOriginalExtension();

                $path = public_path('uploads/property');

                if(!File::isDirectory($path)){

                    File::makeDirectory($path, 0777, true, true);

                }
                $image->move($path, $fileName);
                $image_array[] = [
                    'property_id'=>$id,
                    'photo'=>$fileName,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ];
            }

             PropertyImages::insert($image_array);
            
        }

        $oldPropertyAmenities = PropertyAmenities::where(['property_id'=>$id,'property_type'=>'property'])->latest()->get()->pluck('amenity_id')->toArray(); 
        
        if(!empty($oldPropertyAmenities)){ 
           
           if(!empty($request->aminities)){
            $removeAmenities=array_diff($oldPropertyAmenities,$request->aminities);
            $insertAmenities= array_diff($request->aminities,$oldPropertyAmenities);
            if(isset($removeAmenities)&& !empty($removeAmenities)){
                foreach($removeAmenities as $remove){
                    PropertyAmenities::where(array('property_id'=>$id,'amenity_id'=>$remove,'amenities_type'=>'unit','property_type'=>'property'))->delete();
                }
            }
            if(isset($insertAmenities)&& !empty($insertAmenities)){
                foreach($insertAmenities as $insert){
                   PropertyAmenities::create(array('amenity_id'=>$insert,'property_id'=>$id,'amenities_type'=>'unit','property_type'=>'property'));
                }
            }
        }else { PropertyAmenities::where(array('property_id'=>$id,'property_type'=>'property'))->delete();}
        }else{
            if(!empty($request->aminities)){
                foreach($request->aminities as $amenity)
                {   
                   PropertyAmenities::create(array('amenity_id'=>$amenity,'property_id'=>$id,'amenities_type'=>'unit','property_type'=>'property'));
                }
            }
        }

        if($request->has('bed')){

            $deletebedroom = $request->property_bedrooms;
            PropertyBedrooms::destroy($deletebedroom);
            $bathroom = $request->bathroom;
            $bed = $request->bed;
            $bed_type = $request->bed_type;
            $guest_per_room = $request->guest_per_room;
            foreach($bed as $key=>$value){
                $bedroom_array[] = [
                    'property_id'=>$id,
                    'bathroom'=>$bathroom[$key],
                    'bed'=>$bed[$key],
                    'bed_type'=>$bed_type[$key],
                    'guest_per_room'=>$guest_per_room[$key],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ];
            }

             PropertyBedrooms::insert($bedroom_array);
            
        }

            Session::flash('success', 'Property updated successfully!');
            return response()->json(['success'=>'yes','redirect_url'=>url('/property')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::find($id);
        $property->delete();
        return response()->json(['success'=>true]);

    }

    public function acceptRequest(Request $request)
    {
       
        $timeshare = Property::find($request->id);
        $timeshare->status = 'approved';
        $timeshare->save();
        return response()->json(['success'=>true]);
    }

    public function deletepicture(Request $request){
        $getRecord = PropertyImages::where('id',$request->id)->first();
        $rsltDelRec = PropertyImages::find($request->id);
        $rsltDelRec->delete();  
        $oldfilename =  substr(strrchr($getRecord->photo, '/'), 1); 
        $image_path = '/uploads/property/'.$oldfilename;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }    
        return response()->json(['success'=>true]);
    }
}
