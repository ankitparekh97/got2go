<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hotels;
use Image;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Traits\EmailsendTrait;

class HotelController extends Controller
{
    use EmailsendTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request);
        $hotels = Hotels::all();
        //dd($owners);
        
        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$hotels->toArray()]);
        }else{
           return view('admin.hotel.index',compact('hotels'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:hotels',

        ]); 
         if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 


        $random_password = Str::random(8);
        $hotel = new Hotels([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($random_password),
        ]);
        $hotel->save();

        $toMail = $request->email;
        $toName = $request->name;
        $emailRecord = DB::table('email_template')->where('task','send_hotel_password')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body); 
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{user_name}', $request->name, $body);
        $body = str_replace('{email}', $request->email , $body);
        $body = str_replace('{password}', $random_password , $body);
    
    
        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
        Session::flash('success', 'Hotel Registerd successfully!');
        return response()->json(['success'=>'yes','redirect_url'=>url('/hotels')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotel = Hotels::find($id);
        return view('admin.hotel.view',compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::find($id);
        //dd($owner);
        return view('admin.hotel.edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    //    / dd($request->all());
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'location'=>'required',
            'city'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'state'=>'required',
            'website'=>'required',
            'property_type'=>'required',
            'check_in'=>'required',
            'check_out'=>'required',
            'private_room_accommodation'=>'required',
            'cottage_room_accommodation'=>'required',
            'delux_room_accommodation'=>'required',
        ]); 

         if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        
        $hotel = Hotels::where('id',$id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'location' => $request->location,
            'city' => $request->city,
            'state' => $request->state,
            'phone' => $request->phone,
            'website' => $request->website,
            'property_type' => $request->property_type,
            'check_in' => $request->check_in,
            'check_out' => $request->check_out,
            'private_room_accommodation' => $request->private_room_accommodation,
            'cottage_room_accommodation'=> $request->cottage_room_accommodation,
            'delux_room_accommodation'=>$request->delux_room_accommodation,
        ]);

        Session::flash('success', 'Hotels updated successfully!');
        return response()->json(['success'=>'yes','redirect_url'=>url('/hotels')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotels::find($id);
        $hotel->delete();

        return response()->json(['success'=>true]);
    }
}
