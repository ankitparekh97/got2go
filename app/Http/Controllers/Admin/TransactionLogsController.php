<?php

namespace App\Http\Controllers\Admin;

use App\Models\Booking;
use App\Models\BookingPayment;
use App\Models\OwnerRentalPair;
use App\Models\Messages;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Services\TransactionLogsContract;
use App\Services\SendMessageServiceContract;
use DB;

class TransactionLogsController extends Controller
{
    const BOOKING_ID = 'booking.id';

    public $transactionLogsService;

    public function __construct(
        TransactionLogsContract  $transactionLogsService,
        SendMessageServiceContract $sendMessageServiceContract
    )
    {
        $this->transactionLogsService = $transactionLogsService;
        $this->sendMessageService = $sendMessageServiceContract;
    }

    public function index(){
        return view('admin.index');
    }

    //TODO: Optimize Query
    public function transactionLogs(Request $request)
    {
        if($request->ajax())
        {
           try {
                $transactionLogs = $this->transactionLogsService->GetTransactionLogs();
                return datatables()->of($transactionLogs)
                ->editColumn('id', function ($transactionLogs) {
                    return $transactionLogs->booking_id;
                })
                ->editColumn('rental_user_id', function ($transactionLogs) {
                    return $transactionLogs->rentalUserFirstName . ' ' . $transactionLogs->rentalUserLastName;
                })
                ->editColumn('date', function ($transactionLogs) {
                    return Carbon::parse($transactionLogs->check_in_date)->format('M d, Y') . ' - ' . Carbon::parse($transactionLogs->check_out_date)->format('M d, Y');
                })
                ->editColumn('total', function ($transactionLogs) {
                    if ($transactionLogs->paymentType =='refunded') {
                        return '- $'. number_format($transactionLogs->refundAmount, 2);
                    }
                    return '+ $'. number_format($transactionLogs->amount, 2);
                })
                ->addColumn('property_name', function ($transactionLogs) {
                    return '<a class="custom-link-blue" href=' .URL("propertydetail/".$transactionLogs->propertyId).' target="_blank">'.$transactionLogs->title.'</a>';
                })
                ->addColumn('location', function ($transactionLogs) {
                    return $transactionLogs->city . ', ' . $transactionLogs->state;
                })
                ->addColumn('owner', function ($transactionLogs) {
                    return $transactionLogs->ownerUserFirstName . ' ' . $transactionLogs->ownerUserLastName;
                })
                ->editColumn('status', function ($transactionLogs) {
                    $status ='';
                    if($transactionLogs->paymentType=='completed' || $transactionLogs->status=='approved'){
                        $status = 'Completed';
                    } else if($transactionLogs->paymentType=='pending' || $transactionLogs->status=='pending'){
                        $status = 'Pending';
                    } else if($transactionLogs->paymentType=='decline' || $transactionLogs->status=='decline'){
                        $status = 'Declined';
                    } else if($transactionLogs->paymentType=='refunded' || $transactionLogs->status=='refunded'){
                        $status = 'Refunded';
                    }
                    return $status;
                })
                ->addColumn('action', function ($transactionLogs) {
                    if($transactionLogs->status =='approved' && $transactionLogs->paymentType !=='refunded'){
                        $status = '<div class="action-tool"><a href="javascript:void(0)" data-id="'.$transactionLogs->id.'" data-action="refund" data-renterName="'.$transactionLogs->rentalUserFirstName . ' ' . $transactionLogs->rentalUserLastName.'" data-reservationDate="'.Carbon::parse($transactionLogs->check_in_date)->format('M d, Y') . ' - ' . Carbon::parse($transactionLogs->check_out_date)->format('M d, Y').'" data-toggle="modal" class="btn btn-yellow " data-target="#refund" onclick = "refundamount(this)">Refund</a></div>';
                    } else {
                        $status = '';
                    }
                    return $status;
                })
                ->rawColumns(['id','rental_user_id','date','total','property_name','location', 'status','action'])
                ->make(true);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'error'=>$th->getMessage()]);
            }
        }
        return view('admin.transaction.logs');
    }

    public function refundBooking(Request $request)
    {
        
        /* try { */
                $request->validate([
                    'id' => 'required|numeric',
                ]);

               
                $transactionId = $request->id;
                $input = $request->all();
               
                return $this->transactionLogsService->refundBooking($input);
        /* } catch (\Throwable $th) {
            $error = stripeErrorMessage($th);
            return response()->json(['success'=>false,'error'=>$error]);
        } */
    }
}
