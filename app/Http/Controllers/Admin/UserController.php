<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RentalUser;
use Image;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Traits\EmailsendTrait;
class UserController extends Controller
{
    use EmailsendTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = RentalUser::all();
        return view('admin.rentaluser.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rentaluser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator =  Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required|email|unique:rental_user',
            'bio'=>'required',
            'birthdate'=>'required|date',
            'gender'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'zip'=>'required',
            'currency'=>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'marketing_email'=>'required',
            'linked_paypal_account'=>'required',

       ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        if($request->hasFile('photo')){
            $avatar = $request->file('photo');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/users/' . $filename ) );
            $request->photo = $filename;
        }

        $random_password = Str::random(6);
        $user = new RentalUser([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'bio' => $request->bio,
            'password' => Hash::make($random_password),
            'birthdate' => date('Y-m-d',strtotime($request->birthdate)),
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'currency' => $request->currency,
            'photo' => $request->photo,
            'marketing_email' => $request->marketing_email,
            'linked_paypal_account' => $request->linked_paypal_account,
            'is_otp_verified'=>'1',
            'emergency_number'=>$request->emergency_number,
            'site_language'=>$request->site_language,
            'status'=>'approved'
        ]);
        $user->save();

        $toMail = $request->email;
        $toName = $request->full_name;
        $emailRecord = DB::table('email_template')->where('task','send_user_password')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body); 
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{user_name}', $request->full_name, $body);
        $body = str_replace('{email}', $request->email , $body);
        $body = str_replace('{password}', $random_password , $body);
    
    
        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
        Session::flash('success', 'User Added successfully!');
        return response()->json(['success'=>'yes','redirect_url'=>url('/users')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = RentalUser::find($id);
        return view('admin.rentaluser.view',compact('user',$user));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = RentalUser::find($id);
        //dd($user);
        return view('admin.rentaluser.edit', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'full_name' => 'required',
            'bio'=>'required',
            'birthdate'=>'required|date',
            'gender'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'zip'=>'required',
            'currency'=>'required',
            'marketing_email'=>'required',
            'linked_paypal_account'=>'required',

       ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        if($request->hasFile('photo')){
            $avatar = $request->file('photo');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/users/' . $filename ) );
            $request->photo = $filename;
        }

        
        $user = RentalUser::where('id',$id)->update([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'bio' => $request->bio,
            'birthdate' => date('Y-m-d',strtotime($request->birthdate)),
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'currency' => $request->currency,
            'photo' => $request->photo,
            'marketing_email' => $request->marketing_email,
            'linked_paypal_account' => $request->linked_paypal_account,
            'is_otp_verified'=>'1',
            'emergency_number'=>$request->emergency_number,
            'site_language'=>$request->site_language,
            'status'=>'approved'
        ]);
        
        Session::flash('success', 'User updated successfully!');
        return response()->json(['success'=>'yes','redirect_url'=>url('/users')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = RentalUser::find($id);
        $user->delete();

        return response()->json(['success'=>'User Deleted.']);
    }

    public function getUsers(){
        $users = RentalUser::all();
        return response()->json(compact('users'),201);
    }
    
}
