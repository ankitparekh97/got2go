<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Owner;
use App\Booking;
use Image;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Traits\EmailsendTrait;

class UserOwnerController extends Controller
{
    use EmailsendTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = Owner::all();
        //dd($owners);
        return view('admin.owner.index',compact('owners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:owner',
            'bio'=>'required',
            'birthdate'=>'required|date',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address'=>'required',
            'city'=>'required',
            'currency'=>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]); 
         if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        if($request->hasFile('photo')){
            $avatar = $request->file('photo');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/owners/' . $filename ) );
            $request->photo = $filename;
        }

        $random_password = Str::random(6);
        $owner = new Owner([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($random_password),
            'bio' => $request->bio,
            'birthdate' => date('Y-m-d',strtotime($request->birthdate)),
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'currency' => $request->currency,
            'photo' => $request->photo,
            'is_otp_verified'=>'1',
            'languages_known'=>$request->languages_known,
            'status'=>'approved'
        ]);
        $owner->save();

        $toMail = $request->email;
        $toName = $request->first_name.' '.$request->last_name;
        $emailRecord = DB::table('email_template')->where('task','send_user_password')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body); 
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{user_name}', $request->first_name.' '.$request->last_name, $body);
        $body = str_replace('{email}', $request->email , $body);
        $body = str_replace('{password}', $random_password , $body);
    
    
        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
        Session::flash('success', 'Owner saved successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/owners')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner = Owner::with(['property','property.booking'])->find($id);
        $bookings = Booking::with(['rentaluser','property'=> function($owner) use ($id) {
             $owner->where(['owner_id'=>$id]);
             return $owner;
        },'property.ownerproperty'])->get();
        //echo "<pre>"; print_r($bookings->toarray()); exit();
        return view('admin.owner.view',compact('owner','bookings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner = Owner::find($id);
        //dd($owner);
        return view('admin.owner.edit', compact('owner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'bio'=>'required',
            'birthdate'=>'required|date',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address'=>'required',
            'city'=>'required',
            'currency'=>'required',
        ]); 

         if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        } 

        if($request->hasFile('photo')){
            $avatar = $request->file('photo');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/owners/' . $filename ) );
            $request->photo = $filename;
        }

        $user = Owner::where('id',$id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'bio' => $request->bio,
            'birthdate' => date('Y-m-d',strtotime($request->birthdate)),
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'currency' => $request->currency,
            'photo' => $request->photo,
            'is_otp_verified'=>'1',
            'languages_known'=>$request->languages_known,
            'status'=>'approved'
        ]);

        Session::flash('success', 'Owner updated successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/owners')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = Owner::find($id);
        $owner->delete();

        return response()->json(['success'=>'Owner Deleted.']);
    }

    public function getOwners(Request $request)
    {
        if($request->ajax())
        {
            try {

                $OwnerTimeshares = Owner::with(['ownerproperty'])
                                            ->whereHas('ownerproperty.property', function ($query){
                                                $query->where('type_of_property','vacation_rental');
                                            })
                                            ->orderBy('id','desc')
                                            ->get();

                return datatables()->of($OwnerTimeshares)
                    ->addColumn('owner', function ($OwnerTimeshares) {
                        return $OwnerTimeshares->first_name . ', ' . $OwnerTimeshares->last_name;
                    })
                    ->addColumn('property_name', function ($OwnerTimeshares) {
                        return $OwnerTimeshares->property->title;
                    })
                    ->addColumn('document', function ($OwnerTimeshares) {
                        return '<a href="'.$OwnerTimeshares->proof_of_ownership_or_lease.'" target="_blank" data-id="'.$OwnerTimeshares->id.'" class="">View Document</a>';
                    })
                    ->editColumn('status', function ($OwnerTimeshares) {
                        if($OwnerTimeshares->status=='approved'){
                            $status = 'Complete';
                        } else if($OwnerTimeshares->status=='pending'){
                            $status = 'Pending';
                        } else if($OwnerTimeshares->status=='decline'){
                            $status = 'Declined';
                        } else if($OwnerTimeshares->status=='refunded'){
                            $status = 'Refunded';
                        }
                        return $status;
                    })
                    ->addColumn('action', function ($OwnerTimeshares) {
                        if($OwnerTimeshares->status=='pending'){
                            $approve = '<a href="javascript:void(0)" data-id="'.$OwnerTimeshares->id.'" data-action="approve" data-renterName="'.$OwnerTimeshares->rentaluser->first_name . ' ' . $OwnerTimeshares->rentaluser->last_name.'" class="btn btn-danger font-weight-bold btn-square">Approve</a>';
                            $deny = '<a href="javascript:void(0)" data-id="'.$OwnerTimeshares->id.'" data-action="deny" data-renterName="'.$OwnerTimeshares->rentaluser->first_name . ' ' . $OwnerTimeshares->rentaluser->last_name.'"  class="btn btn-danger font-weight-bold btn-square">Deny</a>';
                        } else {
                            $approve = '';
                            $deny = '';
                        }
                        return $approve . ' ' . $deny;
                    })
                    ->rawColumns(['rental_user_id','date','actual_price','property_name','owner','status','action'])
                    ->make(true);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'error'=>$th->getMessage()]);
            }
        }
        return response()->json(compact('owners'),201);
    }

    public function updateStatus(Request $request)
    {
        try {
            $owner_id = $request->id;
            $status = $request->action;

            if($status == 'approve'){
                $statusUpdate = 'approved';
            } else if($status == 'deny'){
                $statusUpdate = 'decline';
            }

            $owner = Owner::find($owner_id);
            $owner->status = $statusUpdate;
            $owner->save();

            Session::flash('success', 'Owner status changed to '.ucfirst($status).' Successfully!');

            return response()->json(['success'=>true,'message'=>'Owner status updated to '.ucfirst($status).' Successfully', 'status'=>$status]);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }
}
