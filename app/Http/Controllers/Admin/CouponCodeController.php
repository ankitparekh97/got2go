<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\CouponCodeDTO;
use App\Http\Resources\CouponCode\CouponCodeResource;
use App\Services\CouponCodeContract;
use Illuminate\Http\Request;

class CouponCodeController extends Controller
{
    public $couponCodeService;

    public function __construct(
        CouponCodeContract  $couponCodeService
    )
    {
        $this->couponCodeService = $couponCodeService;
    }

    public function index()
    {
        return view('admin.couponCode.list');
    }

    public function getAllCouponCodes()
    {
        $couponCodes = $this->couponCodeService->getAllCouponCode();
        //$couponCodesReturn = CouponCodeResource::collection($couponCodes);

        return datatables()->of($couponCodes)
            ->editColumn('discount_percentage',function ($couponCode) {
                return $couponCode->discount_percentage.'%';
            })->editColumn('is_single_use',function ($couponCode) {
                $isSingleUse = 'No';
                if($couponCode->is_single_use == '1'){
                    $isSingleUse = 'Yes';
                }
                return $isSingleUse;
            })->editColumn('created_at',function ($couponCode) {
                return carbonParseDate($couponCode->created_at)->format('m/d/Y');
            })->addColumn('number_of_time_used',function ($couponCode) {
                return $this->couponCodeService->numberOfTimesCouponCodeUsed($couponCode->id);
            })->addColumn('action', function ($couponCode) {
                $status = '';
                if($couponCode->status == '0') {
                    $status = '<div class="action-tool">
                                    <a href="javascript:void(0)" class="switch-btn" data-toggle="modal" onclick= "openActivatePopup('.$couponCode->id.')">
                                        <label class="switch-custom">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                            <span class="absolute-no">OFF</span>
                                        </label>
                                    </a>
                        </div>';
                } else if ($couponCode->status == '1') {
                    $status = '<div class="action-tool">
                                <a href="javascript:void(0)" class="switch-btn" data-toggle="modal" onclick= "openDeActivatePopup('.$couponCode->id.')">
                                    <label class="switch-custom">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                        <span class="absolute-no">OFF</span>
                                    </label>
                                </a>
                            </div>';
                }
                return $status;
            }
        )
            ->rawColumns(['action','discount_percentage','created_at','number_of_time_used'])
            ->make(true);
    }

    public function saveCouponCode(Request  $request)
    {
        try{
            $couponCodeDTO = CouponCodeDTO::convertToObject($request);
            $couponCodeDTO->status = true;
            $couponCode = $this->couponCodeService->addCouponCode($couponCodeDTO);
            return $this->returnResponseWithData($couponCode,'Saved',201);
        } catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

    public function checkDuplicateCouponCode($couponCodeName)
    {
        $isExistCouponCode = $this->couponCodeService->checkDuplicateCouponCode($couponCodeName);

        if($isExistCouponCode){
            return $this->returnResponseWithoutData(true,'Already available');
        }
        return $this->returnResponseWithoutData(false,'not available');
    }

    public function activeCouponCode($couponCodeId)
    {
        try{
            $this->couponCodeService->activeCouponCode($couponCodeId);
            return $this->returnResponseWithoutData(true,'Activated coupon');
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

    public function deActiveCouponCode($couponCodeId)
    {
        try{
            $this->couponCodeService->deActiveCouponCode($couponCodeId);
            return $this->returnResponseWithoutData(true,'Deactivated coupon');
        }catch (\Exception $exception){
            return $this->returnResponseWithoutData(false,$exception->getMessage());
        }
    }

}
