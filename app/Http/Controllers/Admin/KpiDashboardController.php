<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Owner;
use App\Models\Property;
use Image;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;


class KpiDashboardController extends Controller
{

    public function chartList(Request $request)
    {
        $month = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $data  = array(100, 50, 10, 150, 100,70,100,211,25,10,110,75);
        return view('admin.kpidashboard.charts',['xAxisData' => $month, 'yAxisData' => $data]);

    }


}
