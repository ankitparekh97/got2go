<?php

namespace App\Http\Controllers\Admin;

use Helper;
use App\Models\Configuration;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\ServiceFeeContract;


class ServiceFeeController extends Controller
{
    public $ServiceFeeContract;

    public function __construct(
        ServiceFeeContract  $ServiceFeeContract
    )
    {
        $this->ServiceFeeContract = $ServiceFeeContract;
    }

    //Fetch Service Fee and display
    public function serviceFee(Request $request)
    {
        $data = Helper::getConfig();
        return view('admin.servicefee.list',compact('data'));
    }

    //Create or update service fee
    public function serviceFeeSave(Request $request) {
        try {

            $request->validate([
                'id' => 'required|numeric'
            ]);

            $servicefee = $request->servicefee;
            $serviceId = $request->id;
            $this->ServiceFeeContract->serviceFeeSave($servicefee,$serviceId);
            $serviceFee = explode(',', $servicefee);
            $serviceId = $serviceFee[1];
            $data = Helper::getConfig();
            return response()->json(['success'=>true,'servicefee' => $serviceFee[0],'serviceId' =>$serviceId, 'autoid' => $data->id, 'message'=>'The GOT2GO Service Fee has been updated']);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

}
