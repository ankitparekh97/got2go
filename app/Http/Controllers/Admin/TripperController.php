<?php

namespace App\Http\Controllers\Admin;

use App\Models\Booking;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\DTO\Api\SubscriptionPlanDTO;
use App\Models\RentalUser;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use App\Repositories\RentalUserSubscriptionRepository;
use App\Services\PaymentServices\Braintree\BraintreePaymentService;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\PaymentServices\Stripe\Subscription\SubscriptionResponseDTO;
use App\Services\SubscriptionPlanContract;
use App\Models\Subscription;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Services\PaymentServices\Stripe\Plan\PlanRequestDTO;
use App\Services\Renter\TripperServiceContract;
use Validator;

class TripperController extends Controller
{
    const RENTAL_USER_ID = 'rental_user.id';

    public $rentalRepository;
    public $subscriptionPlanService;
    public $paymentService;

    public function __construct(
        RentalInterface $rentalRepository,
        SubscriptionPlanContract $subscriptionPlanService,
        PaymentServiceInterface $paymentService,
        TripperServiceContract $tripperServiceContract
    )
    {
        $this->rentalRepository = $rentalRepository;
        $this->subscriptionPlanService = $subscriptionPlanService;
        $this->paymentService = $paymentService;
        $this->tripperServiceContract = $tripperServiceContract;
    }

    public function tripperList(Request $request)
    {
        $subscription = $this->tripperServiceContract->getSubscription();

        if($request->ajax())
        {
           try {
            $tripperList = $this->tripperServiceContract->tripperList();
            return datatables()->of($tripperList)
            ->editColumn('id', function ($tripperList) {
                return $tripperList->id;
            })
            ->editColumn('name', function ($tripperList) {
                return $tripperList->first_name . ' ' . $tripperList->last_name;
            })
            ->editColumn('email', function ($tripperList) {
                return $tripperList->email;
            })
            ->editColumn('age', function ($tripperList) {
                if ($tripperList->birthdate) {
                    $dob = $tripperList->birthdate;
                    return (date('Y') - date('Y',strtotime($dob)));
                }
                return 'N/A';
            })
            ->addColumn('booking', function ($tripperList) {
                return $tripperList->total_booking;
            })

            ->editColumn('status', function ($tripperList) {
                if($tripperList->tripper_status == 'active'){
                    $status = 'Active';
                } else if($tripperList->tripper_status == 'paused'){
                    $status = 'Paused';
                }
                return $status;
            })
            ->addColumn('action', function ($tripperList) {
                if($tripperList->tripper_status == 'active'){
                    $status = '<a href="javascript:void(0)" data-toggle="modal" class="btn-yellow-default btn-yellow-pause" data-target="#paused" onclick = "paused('.$tripperList->id.')"></a>';
                    $status .= '&nbsp;<a href="javascript:void(0)" data-toggle="modal" class="btn-yellow-default btn-yellow-cancel" data-target="#cancel" onclick = "cancel('.$tripperList->id.')"></a>';
                } elseif($tripperList->tripper_status == 'paused') {
                    $status = '<a href="javascript:void(0)" data-toggle="modal" class="btn-yellow-default btn-yellow-play" data-target="#paused" onclick = "active('.$tripperList->id.')"></a>';
                    $status .= '&nbsp;<a href="javascript:void(0)" data-toggle="modal" class="btn-yellow-default btn-yellow-cancel" data-target="#cancel" onclick = "cancel('.$tripperList->id.')"></a>';
                } else {
                    $status = '';
                }
                return $status;
            })
            ->rawColumns(['id','name','email','age','booking', 'status','action'])
            ->make(true);
                    } catch (\Throwable $th) {
                        return response()->json(['success'=>false,'error'=>$th->getMessage()]);
                    }
                }
        return view('admin.trippers.list', compact('subscription'));
    }

    public function subscriptionApprove(Request $request)
    {
        $requestCheck = ['id'=>$request->id,'tripper_status'=>$request->subscription_action];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:rental_user,id',
            'tripper_status' => 'required|in:active,paused'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        try {
            $rentalUserId = $request->id;
            $subscriptionAction = $request->subscription_action;

            return $this->tripperServiceContract->subscriptionApprove($rentalUserId,$subscriptionAction);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function subscriptionCancel(Request $request)
    {
        $requestCheck = ['rental_id'=>$request->post('id')];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:rental_user_subscription,rental_id,'.$requestCheck['rental_id'],
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>"something went wrong. Please Try again."]);
        }
        try {

            /**
             * @var SubscriptionResponseDTO $cancelPaymentSubscription
             */
            $tripperId = $request->post('id');
            $this->tripperServiceContract->subscriptionCancel($tripperId);
            return response()->json(['success'=>true,'message'=>'Tripper subscription cancelled successfully']);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function subscriptionSave(Request $request)
    {
        $requestCheck = ['id'=>$request->id,'annual_subscription'=>$request->annual_subscription,'discount_percentage'=>$request->discount_percentage,'strike_through_amount'=>$request->annual_strike_subscription_amount];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:subscription,id',
            'annual_subscription'=>'required|numeric|integer',
            'discount_percentage'=>'required|numeric|between:0,80',
            'strike_through_amount'=>'required|numeric|integer|gt:annual_subscription'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        try {

            $annualSubscriptionAmount = $request->annual_subscription;
            $subscriptionId = $request->id;
            $discountPercentage = $request->discount_percentage;
            $strikeThroughAmount = $request->annual_strike_subscription_amount;
            $this->tripperServiceContract->adminSubscriptionSave($annualSubscriptionAmount,$subscriptionId,$discountPercentage,$strikeThroughAmount);

            return response()->json(['success'=>true,'message'=>'Subscription saved successfully']);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

}
