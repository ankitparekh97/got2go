<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PropertyType;
use App\Services\SearchContract;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $geoCode = null;
    private $searchService = null;
    public function __construct(SearchContract $searchService)
    {
        $this->searchService = $searchService;
        $this->geoCode = env("GEOCODE");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotDealsSlider = $this->searchService->getHotDealsByLimit();
        return view('rentaluser.home', ['url' => 'rentaluser','hotDealsSlider' => $hotDealsSlider]);
    }

    public function gethotdeals(Request $request)
    {
        $input = $request->all();
        $hotDealsSlider = $this->searchService->getHotDeals($input);
        return response()->json(['success'=>true,'hotDealsSlider' => $hotDealsSlider]);
    }

    public function showsearch(Request $request)
    {

        $hotDealsSlider = $this->searchService->getHotDealsByLimit();
        $propertyType = PropertyType::all();
        $propertyType = collect($propertyType)->mapWithKeys(function ($property) {
            return [$property['id'] => $property['property_type']];
        });
        return view('rentaluser.search', compact('hotDealsSlider', 'propertyType'));
    }

    public function search(Request $request)
    {
        $input = $request->all();
        $result = $this->searchService->searchProperty($input,$isRentalUser=(bool)Auth::guard('rentaluser')->user());
        return response()->json([
            'success'=>true,
            'data'=>$result['properties'],
            'resortGroup'=> $result['resortGroup'],
            'resortObj'=> $result['resortObj'],
            'loggedId'=> $result['user_id'],
            'page'=>$result['page'],
            'zero_stay' => $result['zero_stay']
        ]);
    }

    public function getSearchCount(Request $request)
    {
        $input = $request->all();
        $result = $this->searchService->searchPropertyCount($input);
        return response()->json([
            'success'=>true,
            'data'=>$result['properties'],
            'page'=>$result['page'],
            'zero_stay' => $result['zero_stay']
        ]);
    }
}
