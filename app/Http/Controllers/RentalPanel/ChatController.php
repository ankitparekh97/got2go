<?php

namespace App\Http\Controllers\RentalPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use Helper;
use Carbon\Carbon;
use App\Libraries\PusherFactory;

use DB;
use Session;
use App\Services\SendMessageServiceContract;
use App\Services\Renter\ChatServiceContract;
use Validator;
use Illuminate\Validation\Rule;

class ChatController extends Controller
{


    public function __construct(
        SendMessageServiceContract $sendMessageServiceContract,
        ChatServiceContract $chatServiceContract
    )
    {
        $this->sendMessageService = $sendMessageServiceContract;
        $this->chatService = $chatServiceContract;
    }
   /**
     * Show the application chat dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        return view('rentaluser.chat.index');
    }

    public function getData(Request $request)
    {
        $name = $request->name;
        $users = $this->chatService->getRentalChatData($name);
        $html = view('rentaluser.chat.chat-contact')->with('users', $users)->render();
        return $this->renderData(['success'=>true,'users'=>$users,'html'=>$html]);
    }

    /**
     * getLoadLatestMessages
     *
     *
     * @param Request $request
     */
    public function getLoadLatestMessages(Request $request)
    {
        Session::put('timezone',$request->header('Timezone'));
        $pairId = Helper::urlsafe_b64decode($request->user_id);
        $requestCheck = ['id'=>$pairId];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:owner_rental_pair,id',
        ]);

        if($validator->fails()){
            return response()->json(['state'=>0,'error'=>"something went wrong. Please Try again."]);
        }
        $return = $this->chatService->getMessages($pairId);
        return response()->json(['state' => 1, 'messages' => $return]);
    }

    /**
     * postSendMessage
     *
     * @param Request $request
     */
    public function postSendMessage(Request $request)
    {
        $pairId = Helper::urlsafe_b64decode($request->id);
        $requestValidation = ['id'=>$pairId,'message'=>$request->message];
        $validator = Validator::make($requestValidation,[
            'id' => 'required|exists:owner_rental_pair,id',
            'message'=>'required|min:2|max:1000'
        ]);
        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        $pairId = Helper::urlsafe_b64decode($request->id);
        $fromUser = Auth::guard('rentaluser')->user()->id;
        $toUser = $request->to_user;
        $message = $request->message;
        $this->sendMessageService->postCommonSendMessage($pairId,'rental','owner',$fromUser,$toUser,'rentaluser',$message);
        return response()->json(['success'=>true]);
    }

    public function unreadFlag(Request $request){

        $pairId = Helper::urlsafe_b64decode($request->id);
        $toUserId = Auth::guard('rentaluser')->user()->id;
        $requestCheck = ['pair_id'=>$pairId,'to_user'=>$toUserId];
        $validator = Validator::make($requestCheck,[
            'pair_id' => [
                        'required',
                        Rule::exists('messages')->where(function ($query) use($requestCheck) {
                            $query->where($requestCheck);
                        }),
                    ],
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>"something went wrong. Please Try again."]);
        }

        $this->chatService->unreadFalg($pairId,$toUserId);
        return response()->json(['success' => true]);
    }
}
