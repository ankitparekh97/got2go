<?php

namespace App\Http\Controllers\RentalPanel;

use App\Models\FavTripboards;
use App\Http\Controllers\Controller;
use App\Repositories\TripBoardRepository;
use App\Models\TripboardProperty;
use App\Models\Property;
use App\Models\Tripboards;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\TripboardVote;
use Carbon\Carbon;
use App\Models\Subscription;
use App\Models\GroupMessages;
use App\Models\TripboardShare;
use Helper;
use App\Libraries\PusherFactory;

use Illuminate\Support\Facades\Session;
use Cookie;
use App\Services\Renter\FavoriteServiceContract;
use Validator;

class FavoriteController extends Controller
{


    /**
     * TripBoardController constructor.
     * @param TripBoardRepository $tripBoardRepository
     */
    public function __construct(
       FavoriteServiceContract  $favoriteServiceContract
    )
    {
        $this->favoriteServiceContract = $favoriteServiceContract;
    }

    public function favorites(Request $request){
        $rentalUser = getLoggedInRental();
        $favtripboard = $this->favoriteServiceContract->getFavouriteTripboards($rentalUser->id);
        $this->view = 'rentaluser.saved-favorites';
        return $this->renderData($favtripboard);
    }

    function unlikeTripBoard(Request $request){
        $rentalUser = getLoggedInRental();

        $requestCheck = ['id'=>$request->id,'user_id'=>$rentalUser->id];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:favourite_tripboard,id,user_id,'.$requestCheck['user_id'],
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'data'=>"something went wrong. Please Try again."]);
        }
        $this->favoriteServiceContract->unlikeTripboard($requestCheck);
        return response()->json(['success'=>true,'data'=>"Property removed from Favourite."]);

    }


}
