<?php

namespace App\Http\Controllers\RentalPanel;

use App\Models\FavTripboards;
use App\Models\Subscription;
use App\Http\Controllers\Controller;
use App\Services\TripBoardContract;
use App\Traits\SendMessageTrait;
use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use Helper;
use Validator;
use Session;
use Cookie;
use App\Services\SendMessageServiceContract;

class TripBoardController extends Controller
{
    public  $tripBoardService = null;



    /**
     * TripBoardController constructor.
     * @param TripBoardContract $tripBoardService
     */
    public function __construct(
        TripBoardContract $tripBoardService,
        SendMessageServiceContract $sendMessageServiceContract
    )
    {
        $this->tripBoardService = $tripBoardService;
        $this->sendMessageService = $sendMessageServiceContract;
    }

    public function tripboards(Request $request){
        $sharedTripBoard = NULL;

        if(isset($request->id) && Auth::guard('rentaluser')->user()){
            $input = $request->all();
            $id = $request->id;
            $userId = Auth::guard('rentaluser')->user()->id;
            $sharedTripBoard = $this->tripBoardService->checkForSharedTripBoard($id, $userId);
        }
        return view('rentaluser.tripboards',['tripboard_id'=> $request->id,'sharedTripBoard' => $sharedTripBoard]);
    }

    function getTripboards(){
        $rentalUser = Auth::guard('rentaluser')->user();
        $userId = $rentalUser->id;
        $tripboard = [];
        $favtripboard = [];
        if($rentalUser){
            $tripboard = $this->tripBoardService->getUserTripBoards($userId);
            $favtripboard = $this->tripBoardService->getFavouriteTripBoards($userId);
        }
        return response()->json(['success'=>true,'data'=>['fav'=> $favtripboard,'user'=>$tripboard] ]);
    }

    public function viewImages(Request $request) {
        if($request->ajax())
        {
            try
            {
                $id = $request->tripboardId;
                $topImages = $this->tripBoardService->getTripBoardPropertyImages($id);
                $tripboard = $this->tripBoardService->getTripBoardById($id);
                return response()->json(['success'=>true,'data'=>$topImages,'id' => $tripboard]);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'message'=>$th->getMessage()]);
            }
        }
    }

    function addTripboards(Request $request) {
        $insertId = NULL;

        $request->validate([
            'tripboard_name' => 'required',
            'property_id' => 'nullable|numeric'
        ]);

        $tripboardName = $request->tripboard_name;
        $userId =  Auth::guard('rentaluser')->user()->id;
        $propertyId = isset($request->property_id) ?  $request->property_id : '';
        $insertId = $this->tripBoardService->addTripboard($tripboardName, $userId, $propertyId);

        $redirect = route('tripboard.detail', ['id' => $insertId]);
        return response()->json(['success'=>true,'data'=>['insertId'=> $insertId,"redirect_url" => $redirect]]);
    }

    function tripboarddetail(Request $request){
        $rentalUser = Auth::guard('rentaluser')->user();
        if(!$rentalUser){
            return redirect()->route('tripboards', ['id' => $request->route('id')]);
        }
        if(!isset($request->id))
        {
            return  redirect()->route('tripboards');
        }
        $userId =  $rentalUser->id;
        $tripboardId =  $request->id;
        $res = $this->tripBoardService->tripboarddetail($tripboardId, $userId);
        if(isset($res['redirect_url']))
        {
           return  redirect()->route('tripboards');
        }
        return view('rentaluser.tripboard-details',(array)$res);

    }

    function saveTripboards(Request $request){

        $request->validate([
            'tripboard_name' => 'required',
            'tripboard_id' => 'required|numeric'
        ]);
        $input = $request->all();
        $this->tripBoardService->saveTripboardDetail($input);

        return response()->json(['success'=>true,'data'=>[]]);
    }

    function saveTravelBuddy(Request $request){
        $request->validate([
            'buddy_name' => 'required',
            'tripboard_id' => 'required|numeric'
        ]);
        $input = $request->all();
        $userId = Auth::guard('rentaluser')->user()->id;
        $buddyId = $this->tripBoardService->saveTravelBuddy($input, $userId);

        return response()->json(['success'=>true,'data'=>['insertId'=> $buddyId]]);
    }

    function deleteTravelBuddy(Request $request){
        $request->validate([

            'buddy_id' => 'required|numeric'
        ]);
        $buddyId = $request->buddy_id;
        $buddyId = $this->tripBoardService->deleteTravelBuddy($buddyId);
        return response()->json(['success'=>true,'data'=>[]]);
    }

    function likeTripBoard(Request $request){

        $request->validate([
            'property_id' => 'required|numeric'
        ]);
        $rentalUser = Auth::guard('rentaluser')->user();
        if($rentalUser){
            $param = [
                'user_id' =>  $rentalUser->id,
                'property_id' => $request->property_id
            ];
            return $this->tripBoardService->likeUnlikeProperties($param);
        }
        return response()->json(['success'=>false,'data'=>[]]);
    }

    function addPropertyToTripboard(Request $request){
        $request->validate([
            'property_id' => 'required|numeric',
            'tripboard_id' => 'required|numeric'
        ]);
        $param = [
            'tripboard_id' => $request->tripboard_id,
            'property_id' => $request->property_id
        ];
        return $this->tripBoardService->AddOrRemovePropertiesFromTripboard($param);
    }

    public function voteTripboard(Request $request) {
        $request->validate([
            'property_id' => 'required|numeric',
            'tripboard_id' => 'required|numeric'
        ]);
        if($request->ajax())
        {
            try {
                $rentalUser = Auth::guard('rentaluser')->user();
                $param = [
                    'tripboard_id' => $request->tripboard_id,
                    'user_id' => $rentalUser->id,
                    'property_id' => $request->property_id
                ];
                $result = $this->tripBoardService->voteTripboard($param);
                return response()->json($result);
            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'message'=>$th->getMessage()]);
            }
        }
    }

    public function tripBoardRemoveListing(Request $request) {
        $request->validate([
            'property_id' => 'required|numeric',
            'tripboard_id' => 'required|numeric'
        ]);
        $input = $request->all();
        if($request->ajax())
        {
            $result = $this->tripBoardService->tripBoardRemoveListing($input);
            return response()->json($result);
        }
    }

    public function postSendMessage(Request $request)
    {
         $validator = Validator::make($request->all(),[
            'id' => 'required',
            'message'=>'required|min:2|max:1000'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }

        /* Send Message */
        $tripboardId = $request->id;
        $userMessage = $request->message;
        $messageArray = $this->tripBoardService->postSendMessage($tripboardId,$userMessage);
        $this->sendMessageService->sendGroupMessage($messageArray,'rentaluser');
        /* End of Send Message */
        return response()->json(['success'=>true]);

    }

    public function getLoadLatestMessages(Request $request)
    {
        Session::put('timezone',$request->header('Timezone'));
        $tripboardId = Helper::urlsafe_b64decode($request->user_id);
        $requestCheck = ['id'=>$tripboardId];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:group_messages,tripboard_id'.$requestCheck['id'],
        ]);
        $return = $this->tripBoardService->getLoadLatestMessages($tripboardId);
        return response()->json(['state' => 1, 'messages' => $return]);
    }

    public function tripBoardAdd(Request $request){

        $request->validate([
            'id' => 'required|numeric'
        ]);
        $userId =  Auth::guard('rentaluser')->user()->id;
        $tripboardId =  $request->id;
        $return = $this->tripBoardService->saveTripBoard($tripboardId, $userId);
        return response()->json(['success'=>true,'message'=>'Tripboard added successfully']);
    }


}
