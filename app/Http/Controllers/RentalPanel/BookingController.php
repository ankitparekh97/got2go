<?php

namespace App\Http\Controllers\RentalPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\OwnerTimeshareProperty;

use App\Booking;
use Validator;
use App\Traits\EmailsendTrait;
use Session;
use Auth;
use DB;
use Carbon\Carbon;

class BookingController extends Controller
{


    public function bookingList(Request $request)
    {
        $rental_user = Auth::guard('rentaluser')->user()->id;

        // $bookings = Booking::with(['rentaluser','property'=> function($owner) use ($rental_user) {
        //                 $owner->where(['owner_id'=>$owner_id]);
        //                 return $owner;
        //          },'property.ownerproperty', 'property.ownertimeshare', 'property.propertyimages'])
        //          //->join('rental_user', 'rental_user.id', '=', 'Booking.rental_user_id')
        //          ->get();
       
        $bookings = Booking::with('property','property.ownerproperty','property.ownertimeshare')
                    ->where('rental_user_id',Auth::guard('rentaluser')->user()->id)->get();
                    // ->where('booking.booking_type','property')->get();
                   
        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$bookings->toArray()]);
        }else{
            return view('rentaluser.booking.property',compact('bookings'));
        }
    }

    public function timesharelist(Request $request)
    {
        $bookings = Booking::join('owner_timeshare_property', 'owner_timeshare_property.id', '=', 'Booking.property_id')
                    ->select(
                        'booking.*',
                        'owner_timeshare_property.*',
                        'booking.id AS booking_id'
                    )
                    ->where('rental_user_id',Auth::guard('rentaluser')->user()->id)
                    ->where('booking.booking_type','timeshare')->get();

        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>$bookings->toArray()]);
        }else{
            return view('rentaluser.booking.timeshare',compact('bookings'));
        }
    }

    public function cancleBooking(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'booking_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        $booking = Booking::where('id',$request->booking_id)
                            ->where('rental_user_id',Auth::guard('rentaluser')->user()->id)
                            ->first();

        $booking->is_cancelled = '1';
        $booking->cancellation_date = Carbon::now();
        $booking->save();

        if($request->method() == 'POST'){
            return response()->json(['success'=>true,'data'=>'Booking cancle Successfully!']);
        }else{
            return view('rentaluser.booking.timeshare',compact('bookings'));
        }
    }

}
