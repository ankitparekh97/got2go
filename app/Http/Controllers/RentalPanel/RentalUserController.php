<?php

namespace App\Http\Controllers\RentalPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Exception;
use Image;
use App\Http\DTO\Api\RentalUserDTO;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Helpers\Helper;
use DB;
use App\Services\RentalUserServiceContract;
class RentalUserController extends Controller
{

    protected $rental_user;
    const BOOKING_PROPERTY = 'property', BOOKING_TIMESHARE ='timeshare';
    public $rentalRepository;

    /**
     * Create a new controller instance.
     *
     * @param RentalInterface $rentalRepository
     */
    public function __construct(

        RentalUserServiceContract $rentalUserService
        )
    {


        $this->rentalUserService = $rentalUserService;
    }

    public function index(Request $request)
    {
        $hotDealsSlider = $this->rentalUserService->getHotDealsByLimit();


        return view('rentaluser.home', compact('hotDealsSlider'));
    }



    public function viewStays(Request $request){
        $hotDealsSlider = $this->rentalUserService->getHotDealsByLimit();
        $this->view = 'rentaluser.viewstays';
        return $this->renderData(['url' => 'rentaluser.viewstays','hotDealsSlider' => $hotDealsSlider]);
    }

    public function setting(Request $request) {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        $id = Auth::guard('rentaluser')->user()->id;
        $rentalUser = $this->rentalUserService->getUserById($id);
        $userType = 'rental';
        $paymentDetails = $this->rentalUserService->getCardDetails($id);

        $this->view = 'rentaluser.setting';
        return $this->renderData(['url' => 'rentaluser.setting','rentalUser' => $rentalUser,'paymentDetails' => $paymentDetails]);

    }

    public function dashboard(Request $request) {

        if(!Auth::guard('rentaluser')->user() || Auth::guard('rentaluser')->user()->is_tripper != 1){
            return redirect()->route('rental.home');
        }

        $id = Auth::guard('rentaluser')->user()->id;
        $rentalUser =$this->rentalUserService->getUserById($id);
        $this->view = 'rentaluser.dashboard';
        return $this->renderData(['url' => 'rentaluser.dashboard','rentalUser' => $rentalUser]);

    }

    public function saveSetting(Request $request) {
        if($request->ajax())
        {

            try {
                if(!Auth::guard('rentaluser')->user()){
                    return response()->json(['success'=>false,'error'=>'user not loged in']);
                }
                 $input = $request->all();
                 $setting =$this->rentalUserService->saveSetting($input);
                 return $this->renderData($setting);

            } catch (\Throwable $th) {
                return response()->json(['success'=>false,'error'=>$th->getMessage()]);
            }

        }
    }

    public function mystay(Request $request) {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }

        $userId = Auth::guard('rentaluser')->user()->id;
        $response =  $this->rentalUserService->getMyStay($userId);

        $this->view = 'rentaluser.mystay';
        return $this->renderData($response);
    }

    public function mystayDetails($id,$bid, Request $request)
    {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }

        if(!is_numeric($id) || !is_numeric($bid)){
            return redirect('/');
        }

        $requestCheck = ['id'=>$id, 'bid'=>$bid];
        $validator = Validator::make($requestCheck,[
            'bid' => 'required|numeric|exists:booking,id,property_id,'.$requestCheck['id'],

        ]);

        if($validator->fails()){
            return redirect(url()->previous());
        }
        $userId = Auth::guard('rentaluser')->user()->id;
        $response = $this->rentalUserService->mystayDetails($id,$bid, $userId);
        $this->view = 'rentaluser.mystay-details';
        return $this->renderData( $response);
    }


    public function myNotifications(Request $request) {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        try{
            $id = Auth::guard('rentaluser')->user()->id;
            $rentalUser = $this->rentalUserService->getRentalUserWithCommunicationPref($id);

            $this->view = 'rentaluser.notifications';
            return $this->renderData(['url' => 'rentaluser.notifications','rentalUser' => $rentalUser]);

         } catch (Exception $ex){
            \Log::error('My Notifications::'. $ex->getMessage());
            abort('500');
         }
    }

    public function saveNotifications(Request $request)
    {
        try {

            $id = Auth::guard('rentaluser')->user()->id;
            $requestCheck = ['id'=>$id];
            $validator = Validator::make($requestCheck,[
                'id' => 'required|numeric|exists:rental_user,id',

            ]);
            if($validator->fails()){
                return $this->renderData(['success'=>false , 'error'=> "User is not logged in."]);
            }
            $input = $request->all();
            $this->rentalUserService->saveNotificationSettings($id, $input);
            $error = '';
            $success = true;
            return $this->renderData(['success'=>$success , 'error'=> $error]);


        } catch (\Throwable $exception) {
            $error = $exception->getMessage();
            $success = false;
            \Log::error('Communication Preferences::'. $exception->getMessage());
        }
        return $this->renderData(['success'=>$success , 'error'=> $error]);
    }

    public function saveProfilepic (Request $request)
    {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        if($request->ajax())
        {
            try {
                    $validator =  Validator::make($request->all(), [
                        'profilePic'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ]);

                    if ($validator->fails()) {
                        return response()->json(['success'=>false,'message'=>$validator->errors()->all()]);
                    }

                    $id = Auth::guard('rentaluser')->user()->id;
                    $requestCheck = ['id'=>$id];
                    $validator = Validator::make($requestCheck,[
                        'id' => 'required|numeric|exists:rental_user,id',
                    ]);

                    if($validator->fails()){
                        return response()->json(['success'=>false,'message'=>'User not found!']);
                    }


                    if($request->hasFile('profilePic')){
                        $avatar = $request->file('profilePic');
                        $filename = time() . '.' . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/users/' . $filename ) );
                        $request->photo = $filename;
                    }

                    $this->rentalUserService->updateProfilePic($id, $request->photo);
                    return response()->json(['success'=>true,'message'=>'Profile picture updated successfully']);


                } catch (\Throwable $th) {
                    return response()->json(['success'=>false,'message'=>'error']);
                }
        }


    }

    public function savePayment(Request $request)
    {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        if($request->ajax())
        {
            try {
                $user = Auth::guard('rentaluser')->user();
                $id = Auth::guard('rentaluser')->user()->id;
                $requestCheck = ['id'=>$id];
                $validator = Validator::make($requestCheck,[
                    'id' => 'required|numeric|exists:rental_user,id',
                ]);

                if($validator->fails()){
                    return response()->json(['success'=>false,'message'=>'User not found!']);
                }
                if($user!= '')
                {

                   $PaymentService = new PaymentService();
                    if($user->customer_id != ''){
                        $cardRequestDTO = new CardRequestDTO();
                        $cardRequestDTO->token = $request->stripeToken;
                        $cardRequestDTO->customerId = $user->customer_id;
                        $card = $PaymentService->createCard($cardRequestDTO);
                        $duplicate = $this->checkDuplicateCard($card);

                        if($duplicate) {
                            $cardRequestDTO->token = $card['id'];
                            $PaymentService->deleteCard($cardRequestDTO);
                            $success = false;
                            $error = true;
                            $message ='Card details already exits, please add other card.';
                            return response()->json(['success'=>false,'error'=> $error, 'message'=>$message]);
                        }

                        $paymentCardsModel = $this->rentalUserService->SavePaymentCards($card, $user, $request->stripeToken);

                    }else{
                        $customerRequestDTO = new CustomerRequestDTO();
                        $customerRequestDTO->name = $request->name;
                        $customerRequestDTO->email = $user->email;
                        $customerRequestDTO->phone = $request->get('billing_phone');
                        $customerRequestDTO->source = $request->stripeToken;

                        $customer = $PaymentService->createCustomer($customerRequestDTO);
                        $this->rentalUserService->updateCustomerId($customer['id'], $user->id);

                        $card = $customer['sources']['data'][0];
                        $paymentCardsModel = $this->rentalUserService->SavePaymentCards($card, $user, $request->stripeToken);

                    }

                    if($paymentCardsModel->id != ''){
                        $success = true;
                        $error = '';
                        $message ='Your card details saved successfully!';
                    } else {
                        $success = false;
                        $error = true;
                        $message ='';
                    }
                }
            } catch (\Exception $exception) {
                $errorMsg = stripeErrorMessage($exception);
                $success = false;
                $message=$errorMsg;
                $error = array('error'=>$errorMsg);

            }
            $id = $request->id;
            return $this->renderData(['success'=>$success , 'error'=> $error, 'id'=> $id,'message'=>$message]);
        }

    }

    public function savePaymentReview(Request $request)
    {
        //TODO: Make common function for creadit card add
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }
        $user = Auth::guard('rentaluser')->user();
        $id = Auth::guard('rentaluser')->user()->id;
        $requestCheck = ['id'=>$id];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|numeric|exists:rental_user,id',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'message'=>'User not found!']);
        }
        if($user!= '')
        {

            try {
                $success = true;
                $error = false;
                $PaymentService = new PaymentService();
                //dd($token['card']['id']);
                if($user->customer_id != ''){
                    $cardRequestDTO = new CardRequestDTO();
                    $cardRequestDTO->token = $request->stripeToken;
                    $cardRequestDTO->customerId = $user->customer_id;
                    $card = $PaymentService->createCard($cardRequestDTO);
                    $duplicate = $this->checkDuplicateCard($card);

                    if($duplicate) {
                        $cardRequestDTO->token = $card['id'];
                        $PaymentService->deleteCard($cardRequestDTO);
                        $success = false;
                        $error = true;
                        $message ='Card details already exits, please add other card.';
                        return response()->json(['success'=>false,'error'=> $error, 'message'=>$message]);
                    }

                    $paymentCardsModel = $this->rentalUserService->SavePaymentCards($card, $user, $request->stripeToken);

                }else{
                    $customerRequestDTO = new CustomerRequestDTO();
                    $customerRequestDTO->name = $request->name;
                    $customerRequestDTO->email = $user->email;
                    $customerRequestDTO->phone = $request->get('billing_phone');
                    $customerRequestDTO->source = $request->stripeToken;

                    $customer = $PaymentService->createCustomer($customerRequestDTO);
                    $this->rentalUserService->updateCustomerId($customer['id'], $user->id);

                    $card = $customer['sources']['data'][0];
                    $paymentCardsModel = $this->rentalUserService->SavePaymentCards($card, $user, $request->stripeToken);
                }




            } catch (Exception $ex){
                $errorMsg = stripeErrorMessage($ex);
                $success = false;
                $error = true;
                return $this->renderData(['success'=>$success , 'error'=> $error, 'message'=>$errorMsg]);
            }


            if($paymentCardsModel->id != ''){
                $success = true;
                $error = '';
                $message ='Your card details saved successfully!';
            } else {
                $success = false;
                $error = true;
                $message ='';
            }
        }

        $paymentcardDetails = $paymentCardsModel;

        $id = $request->id;
        return $this->renderData(['success'=>$success , 'error'=> $error, 'id'=> $id,'message'=>$message,'paymentCardsModel'=>$paymentcardDetails]);

    }



    public function deletePayment(Request $request) {
        if(!Auth::guard('rentaluser')->user()){
            return redirect()->route('rental.home');
        }

        $id = Auth::guard('rentaluser')->user()->id;
        $requestCheck = ['id'=>$id];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|numeric|exists:rental_user,id',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'message'=>'User not found!']);
        }
        try {
            $PaymentService = new PaymentService();
            $user = Auth::guard('rentaluser')->user();

            $this->rentalUserService->deletePaymentCard($request->id, $user->customer_id);
            return response()->json(['success'=>true,'message'=>'Payment details deleted successfully','id' => $request->id]);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }

    }

    public function checkDuplicateCard($token) {
        $id = Auth::guard('rentaluser')->user()->id;
        $requestCheck = ['id'=>$id];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|numeric|exists:rental_user,id',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'message'=>'User not found!']);
        }
        $duplicate_found = false;
        $user = Auth::guard('rentaluser')->user();
        $userId = $user->id;
        return $this->rentalUserService->checkDuplicateCard($token, $userId);
    }

    public function calculateRefund(Request $request)
    {
        $userId = Auth::guard('rentaluser')->user()->id;
        $requestCheck = ['id'=>$request->id, 'rental_user_id'=>$userId ];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|numeric|exists:booking,id,rental_user_id,'.$requestCheck['rental_user_id'],
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'message'=>'User not found!']);
        }

        $booking = $this->rentalUserService->calculateRefund($request->id);

        return $this->renderData($booking);
    }

    public function cancelBooking(Request $request)
    {

        $refundAmmount = $this->calculateRefund($request);
        $refundAmmount = $refundAmmount->original['refundAmount'];
        $user = Auth::guard('rentaluser')->user();
        try {
            $transaction = DB::transaction(function() use($request,$refundAmmount,$user){
                $status = 'refunded';
                 $this->rentalUserService->cancelBooking($request->id, $refundAmmount, $user );
            });
            return $this->renderData($transaction);


        } catch (\Throwable $th) {
            $error = stripeErrorMessage($th);
            return response()->json(['success'=>false,'error'=>$error]);
        }
    }

    public function saveUpdateRentalProfile(Request $request){
        try
        {
            $dtoData = RentalUserDTO::formRequest($request);
            $this->rentalUserService->saveRenterUser($dtoData,getLoggedInRentalId());
            $success = true;
            $error = '';
        } catch (\Exception $exception) {
            $success = false;
            $error = $exception->getMessage();
        }
        return $this->renderData(['success'=>$success , 'error'=> $error]);
    }
}
