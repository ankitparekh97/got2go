<?php

namespace App\Http\Controllers;

use App\Helpers\GlobalConstantDeclaration;
use App\Helpers\Helper;
use App\Http\DTO\Api\BookingDTO;
use App\Services\BookingContract;
use App\Services\BulkUploadServiceContract;
use App\Services\PropertyServiceContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

abstract class DataTables extends Controller{


    public $bookingServiceContract;
    public $propertyServiceContract;
    public $bulkUploadServiceContract;
    public $bookingDTO;

    public function __construct(
        BookingContract $bookingServiceContract,
        PropertyServiceContract $propertyServiceContract,
        BulkUploadServiceContract $bulkUploadServiceContract,
        BookingDTO $bookingDTO
        )
    {
        $this->bookingServiceContract = $bookingServiceContract;
        $this->propertyServiceContract = $propertyServiceContract;
        $this->bulkUploadServiceContract = $bulkUploadServiceContract;
        $this->bookingDTO = $bookingDTO;
    }

    protected function ownerBookings($status,$block) {

        $bookings = $this->bookingServiceContract->getPropertyByOwnerId(getOwnerId(),$status);
        return datatables()->of($bookings,$block)
                ->editColumn('photo', function ($bookings) {
                    $photo = $bookings->photo == null ? url('media/images/no-photo.png') : url('uploads/users/'.$bookings->rentaluser->photo);
                    $avatar = '<img src="'. $photo .'" width="50px" style="border-radius : 50%;"/>';
                    $rentalName = '<span style="font-weight: bold; padding-left:10px;">'.$bookings->rentaluser->first_name . ' ' . $bookings->rentaluser->last_name . '</span>';
                    return $avatar . ' ' . $rentalName;
                })
                ->addColumn('duration', function ($bookings) {
                    $duration = carbonParseDateFormatMDY($bookings->check_in_date) . ' to ' . carbonParseDateFormatMDY($bookings->check_out_date);
                    $people = $bookings->adults + $bookings->childrens;

                    $guest = ($people==1) ? 'guest' : 'guests';
                    $days = carbonDateDiffInDays($bookings->check_in_date,$bookings->check_out_date);

                    $dayLabel = ($days==1) ? 'day' : 'days';
                    $nightsDays = carbonDateDiffInDays($bookings->check_out_date,$bookings->check_in_date);
                    $nightLabel = ($nightsDays==1) ? 'night' : 'nights';

                    $firstLine = $duration;
                    $secondLine = '<p style="margin-left: 15px;">'.$days . ' ' . $dayLabel .', '. $nightsDays. ' ' .$nightLabel . ', '. $people . ' ' .$guest .'</p>' ;
                    return '<i class="fa fa-calendar-alt"></i> '. $firstLine . '<br> ';
                })
                ->editColumn('location_detail', function ($bookings) {
                    return $bookings->property->city . ', ' . $bookings->property->state . ' ' . $bookings->property->zipcode;
                })
                ->editColumn('check_in_date', function ($bookings) {
                    return carbonParseDateFormatMDYWithComma($bookings->check_in_date);
                })
                ->editColumn('check_out_date', function ($bookings) {
                    return carbonParseDateFormatMDYWithComma($bookings->check_out_date);
                })
                ->editColumn('check_in', function ($bookings) {
                    return ($bookings->check_in == null ? '' : Carbon::parse($bookings->check_in)->format('g:i A'));
                })
                ->editColumn('check_out', function ($bookings) {
                    return ($bookings->check_out == null ? '' : carbonParseDateFormatGIA($bookings->check_out));
                })
                ->editColumn('total', function ($bookings) {
                    $days = carbonDateDiffInDays($bookings->check_in_date,$bookings->check_out_date);
                    if($bookings->owner_status !=GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED){
                        return Helper::formatMoney(($bookings->actual_price + $bookings->cleaning_fee),1);
                    }else{
                        $html = '<s>'.Helper::formatMoney(($bookings->actual_price + $bookings->cleaning_fee),1).'</s>';
                        return $html;
                    }
                })
                ->editColumn('price', function ($bookings) {
                    $days = carbonDateDiffInDays($bookings->check_in_date,$bookings->check_out_date);
                    $price = ($bookings->actual_price) / $days;
                    return Helper::formatMoney($price,0).' / night';

                })
                ->editColumn('propertyTitle', function ($bookings) {
                    $vacationClub = '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 23.999 23.999">
                                        <g id="Group_477" data-name="Group 477" transform="translate(-493 -1303)">
                                            <g id="Vacation_Club" data-name="Vacation Club" transform="translate(493 1303)">
                                            <path id="Shape" d="M24,24H0V22.5H1.5V7.5H0v-3L12,0,24,4.5v3H22.5v15H24V24ZM13.5,15v7.5H18V15ZM6,15v3.3h4.5V15Zm7.5-7.5V12H18V7.5ZM6,7.5V12h4.5V7.5Z" transform="translate(0 0)" fill="#3d3d3d"></path>
                                            </g>
                                        </g>
                                        </svg>';

                    $property = '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 25 24.999">
                                    <g id="Group_477" data-name="Group 477" transform="translate(-477 -1399)">
                                        <g id="Group_451" data-name="Group 451" transform="translate(-15)">
                                        <g id="Private_Residence" data-name="Private Residence" transform="translate(492 1399)">
                                            <path id="Path" d="M137.6,274.3l-11.562-11.083a.781.781,0,0,0-1.081,0l-11.574,11.095a1.32,1.32,0,0,0-.385.927,1.3,1.3,0,0,0,1.3,1.3h1.823v9.9A1.563,1.563,0,0,0,117.687,288h4.427a.782.782,0,0,0,.781-.781v-6.771a.261.261,0,0,1,.26-.26h4.688a.262.262,0,0,1,.26.26v6.771a.782.782,0,0,0,.781.781h4.427a1.563,1.563,0,0,0,1.562-1.562v-9.9H136.7a1.3,1.3,0,0,0,1.3-1.3A1.33,1.33,0,0,0,137.6,274.3Z" transform="translate(-113 -263)" fill="#3d3d3d"></path>
                                        </g>
                                        </g>
                                    </g>
                                    </svg>';

                    $title = '<span style="font-weight: bold; font-size:15px; margin-left: 5px;">' .$bookings->property->title.'</span>';
                    $icon = ($bookings->booking_type == 'property') ? $property : $vacationClub;
                    return $icon . '  ' . $title;
                })
                ->addColumn('actions', function ($bookings) use($block){

                    $to = Carbon::now();
                    if($bookings->owner_status =='pending'){
                        if($bookings->booking_type == GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY){
                            $from = Carbon::parse($bookings->created_at)->addDays(3);
                        } else  {
                            $from = Carbon::parse($bookings->created_at)->addDays(1);
                        }
                    } else if($bookings->owner_status=='approved'){
                        if($bookings->booking_type != GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY){
                            $from = Carbon::parse($bookings->updated_at)->addDays(3);
                        } else {
                            $from = Carbon::parse($bookings->created_at)->addDays(3);
                        }
                    } else {
                        $from = Carbon::parse($bookings->created_at)->addDays(3);
                    }


                    if($block == 'pending'){
                        $tick = ($bookings->owner_status=='pending') ? '<span><a href="javascript:void(0)" class="booking-icons bookingUpdate tick-class tooptip-sctm" data-id="'.$bookings->id.'" data-action="approved" data-type="'.$bookings->booking_type.'" data-renter="'.$bookings->first_name.' '.$bookings->last_name.'"><i class="fa fa-check" aria-hidden="true"></i></a></span>' : '';
                        $stop = ($bookings->owner_status=='pending') ? '<span><a href="javascript:void(0)" class="booking-icons bookingUpdate fie-class tooptip-sctm" data-id="'.$bookings->id.'" data-action="rejected" data-type="'.$bookings->booking_type.'" data-renter="'.$bookings->first_name.' '.$bookings->last_name.'"><svg width="20" height="20" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M22.5352 3.69881C18.9269 0.0900416 13.5 -0.989709 8.78515 0.963069C4.0703 2.91585 0.996094 7.51656 0.996094 12.6198C0.996094 17.7231 4.0703 22.3238 8.78515 24.2766C13.5 26.2293 18.9269 25.1496 22.5352 21.5408C27.4617 16.6137 27.4617 8.62593 22.5352 3.69881ZM7.25223 6.25381C10.2661 3.22188 15.0009 2.73847 18.5652 5.09881L6.09823 17.5668C3.75006 13.9996 4.23216 9.27339 7.25223 6.25381ZM8.66223 20.1278C12.2248 22.4932 16.9624 22.0117 19.9762 18.9778C22.9866 15.9638 23.4735 11.2528 21.1432 7.68681L8.66223 20.1278Z" fill="#B7BACA"/>
                                                                        </svg></a></span>' : '';

                        // pending bookings
                        if($to > $from){
                            $this->bookingServiceContract->updateBookingStatus($bookings->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
                            $pending = '';
                            $reject = '';
                        } else {
                            $pending = $tick;
                            $reject = $stop;
                        }

                    } else {

                        $pending = '';
                        $reject = '';

                         // confirmed bookings
                        if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED || $bookings->owner_status== GlobalConstantDeclaration::BOOKING_STATUS_INSTANT_BOOKING){

                                // property
                            if($bookings->booking_type == GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY){
                                $pending = '';
                            } else {

                                // vacation club
                                if($to > $from){
                                    $this->bookingServiceContract->updateBookingStatus($bookings->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
                                    return 'Auto-Rejected';
                                } else {
                                    $confirmButton = '<span><a href="javascript:void(0)" class="tooptip-sctm submit-class booking-icons bookingUpdate" data-id="'.$bookings->id.'" data-action="confirmed" data-type="'.$bookings->booking_type.'" data-renter="'.$bookings->first_name.' '.$bookings->last_name.'"><i class="fa fa-hashtag" aria-hidden="true"></i></a></span>';
                                    $pending = $confirmButton;
                                }
                            }
                        }

                        if($bookings->owner_status != GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED){
                            $cancelButton = ($to > $from == true) ? '' : '<span><a href="javascript:void(0)" class="tooptip-sctm fie-class booking-icons bookingUpdate" data-id="'.$bookings->id.'" data-action="cancelled" data-type="'.$bookings->booking_type.'" data-renter="'.$bookings->first_name.' '.$bookings->last_name.'" data-checkin="'.date('M jS, Y', strtotime($bookings->check_in_date)).'" data-checkout="'.date('M jS, Y', strtotime($bookings->check_out_date)).'"><svg width="20" height="20" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M22.5352 3.69881C18.9269 0.0900416 13.5 -0.989709 8.78515 0.963069C4.0703 2.91585 0.996094 7.51656 0.996094 12.6198C0.996094 17.7231 4.0703 22.3238 8.78515 24.2766C13.5 26.2293 18.9269 25.1496 22.5352 21.5408C27.4617 16.6137 27.4617 8.62593 22.5352 3.69881ZM7.25223 6.25381C10.2661 3.22188 15.0009 2.73847 18.5652 5.09881L6.09823 17.5668C3.75006 13.9996 4.23216 9.27339 7.25223 6.25381ZM8.66223 20.1278C12.2248 22.4932 16.9624 22.0117 19.9762 18.9778C22.9866 15.9638 23.4735 11.2528 21.1432 7.68681L8.66223 20.1278Z" fill="#B7BACA"/>
                                                                            </svg></a></span>';
                            $reject = $cancelButton;
                        }
                    }

                    $chat = '<span><a class="booking-icons chat-class tooptip-sctm" href="'.route('owner.chat').'?id='.(new \App\Helpers\Helper)->urlsafe_b64encode($bookings->messageid).'"><svg width="20" height="20" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.71917 0.399903H18.4332C19.0013 0.399374 19.5463 0.62454 19.9484 1.02587C20.3505 1.4272 20.5766 1.97181 20.5772 2.53991V13.9589C20.5769 14.5272 20.3508 15.072 19.9487 15.4736C19.5466 15.8751 19.0014 16.1004 18.4332 16.0999H10.6132L6.05417 20.1999C5.83402 20.3986 5.51398 20.4405 5.25008 20.3052C4.98618 20.17 4.83338 19.8856 4.86617 19.5909L5.25617 16.1009H2.71917C2.15108 16.1012 1.60615 15.8758 1.20426 15.4742C0.802366 15.0727 0.576437 14.528 0.576172 13.9599V2.54091C0.576437 1.97281 0.802366 1.42809 1.20426 1.02658C1.60615 0.62506 2.15108 0.39964 2.71917 0.399903ZM4.14917 12.5319H17.0052C17.2665 12.5417 17.5122 12.4079 17.6457 12.1831C17.7793 11.9583 17.7793 11.6785 17.6457 11.4537C17.5122 11.2289 17.2665 11.0951 17.0052 11.1049H4.14917C3.88789 11.0951 3.64216 11.2289 3.50862 11.4537C3.37507 11.6785 3.37507 11.9583 3.50862 12.1831C3.64216 12.4079 3.88789 12.5417 4.14917 12.5319ZM17.0052 8.96391H4.14917C3.88789 8.97369 3.64216 8.83988 3.50862 8.61509C3.37507 8.39031 3.37507 8.1105 3.50862 7.88572C3.64216 7.66093 3.88789 7.52712 4.14917 7.53691H17.0052C17.2665 7.52712 17.5122 7.66093 17.6457 7.88572C17.7793 8.1105 17.7793 8.39031 17.6457 8.61509C17.5122 8.83988 17.2665 8.97369 17.0052 8.96391ZM4.14917 5.39591H17.0052C17.2665 5.40569 17.5122 5.27188 17.6457 5.04709C17.7793 4.82231 17.7793 4.5425 17.6457 4.31772C17.5122 4.09293 17.2665 3.95912 17.0052 3.96891H4.14917C3.88789 3.95912 3.64216 4.09293 3.50862 4.31772C3.37507 4.5425 3.37507 4.82231 3.50862 5.04709C3.64216 5.27188 3.88789 5.40569 4.14917 5.39591Z" fill="#B7BACA"/></svg></a></span>';
                    return $pending . ' ' . $reject . ' ' . $chat;
                })
                ->addColumn('adults', function ($bookings) {
                    $people = $bookings->adults;
                    $guest = ($people==1) ? 'Guest' : 'Guests';

                    $adults = $people . ' ' . $guest;
                    return $adults;
                })
                ->addColumn('time', function ($bookings) {

                    $to = Carbon::now();
                    if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_PENDING){
                        $from = ($bookings->booking_type == GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY) ? Carbon::parse($bookings->created_at)->addDays(3) : Carbon::parse($bookings->created_at)->addDays(1);
                    } else if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED){
                        $from = ($bookings->booking_type != GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY) ? Carbon::parse($bookings->updated_at)->addDays(3) : Carbon::parse($bookings->created_at)->addDays(3);
                    } else {
                        $from = Carbon::parse($bookings->created_at)->addDays(3);
                    }

                    // calculate time left
                    $interval = $from->diff($to);
                    $days = $interval->format("%a");
                    $hours = $interval->format("%h");
                    $minutes = $interval->format("%i");
                    $diffInHrsMins = $days * 24 + $hours.':'.$minutes;

                    $time = '<span class="text-dark-75 font-weight-bolder font-size-sm"><i class="fa fa-clock"></i>';
                    if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_PENDING){
                        if($to > $from){
                            return '';
                        }
                        return ' Expires in ' . $diffInHrsMins .' hours</span>';

                    } else if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_REJECTED || $bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED){

                        $from = Carbon::now();
                        $to = ($bookings->booking_type != GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY) ? $bookings->updated_at : $bookings->created_at;

                        // time expired
                        $intervalExpired = $from->diff($to);
                        $daysExpired = $interval->format("%a");
                        $hoursExpired = $interval->format("%h");
                        $minutesExpired = $intervalExpired->format("%i");
                        $diffInHrsMinsExpired = $days * 24 + $hoursExpired.':'.$minutesExpired;

                        return 'Expired ' . $diffInHrsMinsExpired  .' hours ago</span>';

                    } else if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED || $bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_INSTANT_BOOKING){

                        if($bookings->booking_type == GlobalConstantDeclaration::BOOKING_TYPE_VACATION_RENTAL){
                            if($to > $from){
                                return '';
                            }
                            return $diffInHrsMins.'  hours remaining to submit timeshare confirmation</span>';
                        } else {
                            return '';
                        }

                    } else if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_CONFIRMED){
                        return 'Payment Deposited</span>';
                    } else {
                        return '';
                    }
                })
                ->addColumn('statuses', function ($bookings) use($block){

                    $to = Carbon::now();
                    if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_PENDING){
                        if($bookings->booking_type == GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY){
                            $from = Carbon::parse($bookings->created_at)->addDays(3);
                        } else  {
                            $from = Carbon::parse($bookings->created_at)->addDays(1);
                        }
                    }
                    else if($bookings->owner_status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED){
                        if($bookings->booking_type != GlobalConstantDeclaration::BOOKING_TYPE_PROPERTY){
                            $from = Carbon::parse($bookings->updated_at)->addDays(3);
                        } else {
                            $from = Carbon::parse($bookings->created_at)->addDays(3);
                        }
                    } else {
                        $from = Carbon::parse($bookings->created_at)->addDays(3);
                    }

                    $interval = $from->diff($to);
                    $days = $interval->format("%a");
                    $hours = $interval->format("%h");
                    $minutes = $interval->format("%i");
                    $diffInHours = $days * 24 + $hours;

                    // display status
                    if($block == 'pending'){

                        if($bookings->owner_status=='pending'){
                            if($to > $from){
                                $this->bookingServiceContract->updateBookingStatus($bookings->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
                                return 'Auto-Rejected';
                            } else {
                                return 'Requested';
                            }

                        } else if($bookings->owner_status=='rejected'){
                            return 'Rejected';
                        } else if($bookings->owner_status=='cancelled'){
                            return 'Cancelled';
                        } else if($bookings->owner_status == 'auto_reject'){
                            return 'Auto-Rejected';
                        }

                    } else {

                        // approved booking block
                        if($bookings->owner_status=='approved' || $bookings->owner_status=='instant_booking'){
                            if($bookings->booking_type == 'property'){
                                $cancelButton = 'Approved';
                                $confirmButton = '';
                            } else {
                                if($to > $from){
                                    $this->bookingServiceContract->updateBookingStatus($bookings->id,GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,null);
                                    return 'Auto-Rejected';
                                } else {
                                    $confirmButton = 'Pending Confirmation';
                                    $cancelButton = '';
                                }
                            }
                            return $confirmButton . $cancelButton;
                        } else if($bookings->owner_status=='cancelled'){
                            return 'Cancelled';
                        } else if($bookings->owner_status=='confirmed'){
                            return 'Confirmed';
                        }
                    }

                })
                ->rawColumns(['actions','propertyTitle','price','photo','duration','statuses','location_detail','time','check_in','check_out','total'])
                ->make(true);
    }

    protected function ownerListings($ownerId,$status) {
        $propertyListings = $this->propertyServiceContract->getListings($ownerId,$status);

        return datatables()->of($propertyListings)
                ->editColumn('cover_photo', function ($propertyListings) {
                    $photo = $propertyListings->cover_photo == null ? url('media/images/no_image.png') : url('uploads/property/'.$propertyListings->cover_photo);
                    $propertyPhoto = '<img src="'. $photo .'" />';
                    return $propertyPhoto;
                })
                ->editColumn('title', function ($propertyListings) {
                    $group = '';

                    // title
                    $title = '<h4>'.$propertyListings->title.'</h4>';

                    // icon
                    if($propertyListings->type_of_property == 'property'){
                        $propertyType = '';
                        if($propertyListings->property_type_id!=''){
                            $propertyType = ', '. ucfirst($propertyListings->propertytype->property_type);

                        }

                        $icon = '<span class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 25 24.999">
                                        <g id="Group_477" data-name="Group 477" transform="translate(-477 -1399)">
                                            <g id="Group_451" data-name="Group 451" transform="translate(-15)">
                                            <g id="Private_Residence" data-name="Private Residence" transform="translate(492 1399)">
                                                <path id="Path" d="M137.6,274.3l-11.562-11.083a.781.781,0,0,0-1.081,0l-11.574,11.095a1.32,1.32,0,0,0-.385.927,1.3,1.3,0,0,0,1.3,1.3h1.823v9.9A1.563,1.563,0,0,0,117.687,288h4.427a.782.782,0,0,0,.781-.781v-6.771a.261.261,0,0,1,.26-.26h4.688a.262.262,0,0,1,.26.26v6.771a.782.782,0,0,0,.781.781h4.427a1.563,1.563,0,0,0,1.562-1.562v-9.9H136.7a1.3,1.3,0,0,0,1.3-1.3A1.33,1.33,0,0,0,137.6,274.3Z" transform="translate(-113 -263)" fill="#3d3d3d"></path>
                                            </g>
                                            </g>
                                        </g>
                                    </svg>
                                </span> <span class="title"> Private Residence'.$propertyType.' </span>';

                    } else {
                        $icon = '<span class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 23.999 23.999">
                                        <g id="Group_477" data-name="Group 477" transform="translate(-493 -1303)">
                                            <g id="Vacation_Club" data-name="Vacation Club" transform="translate(493 1303)">
                                            <path id="Shape" d="M24,24H0V22.5H1.5V7.5H0v-3L12,0,24,4.5v3H22.5v15H24V24ZM13.5,15v7.5H18V15ZM6,15v3.3h4.5V15Zm7.5-7.5V12H18V7.5ZM6,7.5V12h4.5V7.5Z" transform="translate(0 0)" fill="#3d3d3d"></path>
                                            </g>
                                        </g>
                                    </svg>
                                </span> <span class="title"> '.strtoupper($propertyListings->resort->name).' </span>';
                    }

                    // price
                    $price = '<h5>Regular Price: '.Helper::formatMoney($propertyListings->price,1).' per night</h5>';

                    // location
                    if(!empty($propertyListings->location_detail)){
                        $addressSplit = explode(',',$propertyListings->location_detail);
                        $address = '<p class="location"><span><img src="'.url('media/images/location.png').'"/></span>'.$addressSplit[0].' | '.$propertyListings->city.', '.$propertyListings->state.' '.$propertyListings->zipcode.'</p>';
                    } else {
                        $address = '';
                    }

                    // ratings
                    $rating = '';
                    if(floor($propertyListings->getAverageReviews()) > 0){
                        for ($i = 1; $i <= floor($propertyListings->getAverageReviews()); $i++){
                            $rating .= '<i class="fa fa-star" aria-hidden="true"></i>';
                        }

                        for ($i = floor($propertyListings->getAverageReviews()); $i < 5; $i++){
                            $rating .= '<i class="fa fa-star no-fill" aria-hidden="true"></i>';
                        }

                        if(number_format($propertyListings->getAverageReviews(),0) > 0){
                            $rating .= number_format($propertyListings->getAverageReviews(),1);
                            $rating .= $propertyListings->getAverageReviews();
                        }
                    }

                    $totalRatings = '<p class="review"><a href="javascript:void(0)">('.$propertyListings->getTotalReviews().') Reviews</a></p>';

                    $avgRatings = $rating;
                    $group = $title . $icon . $price . $address . $avgRatings . $totalRatings;
                    return $group;
                })
                ->editColumn('status', function ($propertyListings) {
                    $status = '';
                    if($propertyListings->status == GlobalConstantDeclaration::BOOKING_STATUS_PENDING){
                        $status .= '<span class="orange-label-btn">Pending Approval</span>';
                        $created_at = '<p class="published-on">Created on '.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->created_at, Session::get('timezone'))).'</p>'; $created_at = '<p class="published-on">Created on '.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->created_at, Session::get('timezone'))).'</p>';
                    } else if($propertyListings->status == GlobalConstantDeclaration::BOOKING_STATUS_REJECTED){
                        $status .= '<span class="red-label-btn">Listing Denied</span>';
                        $created_at = '<p class="published-on">Created on '.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->created_at, Session::get('timezone'))).'</p>'; $created_at = '<p class="published-on">Created on '.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->created_at, Session::get('timezone'))).'</p>';
                    } else if($propertyListings->status == GlobalConstantDeclaration::PROPERTY_STATUS_DRAFT){
                        $status .= '<a href="'.route('property.edit',$propertyListings->id).'"><span class="black-label-btn">Continue where I left off</span></a>';
                        $created_at = '<p class="published-on">Created on '.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->created_at, Session::get('timezone'))).'</p>'; $created_at = '<p class="published-on">Created on '.date('m/d/Y h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($propertyListings->created_at, Session::get('timezone')))).'</p>';
                    } else if($propertyListings->status == GlobalConstantDeclaration::BOOKING_STATUS_APPROVED){
                        $publishState =  ($propertyListings->publish == "1") ? "Unpublish" : "Publish";
                        $publishButton = '<a class="red-btn text-uppercase publish-listing" id="publish-listing" data-publish="'.$propertyListings->publish.'" data-id="'.$propertyListings->id.'" href="javascript:void(0)" onclick="showPublishModal('.$propertyListings->id.','.$propertyListings->publish.')">'. $publishState .'</a>';
                        $publishedOn = '<p class="published-on" id="publish_'.$propertyListings->id.'">Published on <span>'.carbonParseDateFormatMDYWithHIA(Helper::convertToLocal($propertyListings->updated_at, Session::get('timezone'))).'</span> </p>';
                        $created_at = '';

                        $status .= $publishButton . $publishedOn;
                    }

                    return $status . $created_at;
                })
                ->addColumn('actions', function ($propertyListings) {
                    $edit = '';
                    if($propertyListings->status != GlobalConstantDeclaration::PROPERTY_STATUS_DRAFT){
                        $edit = '<a  href="'.route('property.edit',$propertyListings->id).'" class="click-icon"><svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.49815 15.5789C5.3667 15.9717 5.5785 16.3966 5.97132 16.5282L5.97384 16.5299C6.12802 16.5791 6.29368 16.5791 6.44786 16.5299L10.9468 15.0287C11.058 14.992 11.159 14.9299 11.2418 14.8472L21.118 4.96782C22.2391 3.84577 22.2391 2.02806 21.118 0.90601C20.5797 0.367356 19.8492 0.0646973 19.0875 0.0646973C18.3257 0.0646973 17.5953 0.367356 17.0569 0.90601L7.17906 10.7854C7.09623 10.8671 7.03401 10.9673 6.99752 11.0778L5.49815 15.5789ZM1.61737 20.4054C2.03936 20.8275 2.61188 21.0647 3.20888 21.0647H16.7074C17.3046 21.0649 17.8775 20.8279 18.2998 20.4058C18.7221 19.9836 18.9592 19.4111 18.959 18.8141V11.313C18.959 10.8987 18.623 10.5628 18.2085 10.5628C17.794 10.5628 17.4579 10.8987 17.4579 11.313V18.8141C17.4582 19.0132 17.3792 19.2042 17.2384 19.3451C17.0976 19.486 16.9066 19.5651 16.7074 19.5651H3.20888C2.79424 19.5647 2.45836 19.2286 2.45836 18.8141V3.81186C2.45882 3.39787 2.79471 3.0625 3.20888 3.0625H12.2076C12.6224 3.0625 12.9586 2.72644 12.9586 2.31189C12.9586 1.89733 12.6224 1.56127 12.2076 1.56127H3.20888C1.96603 1.5622 0.958985 2.56955 0.958985 3.81186V18.8141C0.95854 19.4108 1.19539 19.9833 1.61737 20.4054Z" fill="#B7BACA"/></svg></a>';
                    }
                    $delete = '<a href="javascript:void(0)" class="click-icon" data-id="'.$propertyListings->id.'" onclick="showModal('.$propertyListings->id.')"><svg width="19" height="23" viewBox="0 0 19 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18.5061 4.65103V3.50303C18.5034 3.18798 18.2469 2.93402 17.9316 2.93403H12.7618V1.78203C12.7618 1.14801 12.2473 0.634033 11.6127 0.634033H7.01741C6.38319 0.634585 5.86934 1.1484 5.86934 1.78203V2.92903H0.699534C0.382228 2.92903 0.125 3.18602 0.125 3.50303V4.65103C0.125 4.96804 0.382228 5.22503 0.699534 5.22503H17.9316C18.2489 5.22503 18.5061 4.96804 18.5061 4.65103ZM4.57407 22.589C3.36443 22.5921 2.3587 21.6594 2.27193 20.454L1.273 6.52003H17.362L16.3851 20.454C16.2983 21.6594 15.2926 22.5921 14.0829 22.589H4.57407Z" fill="#B7BACA"/></svg></a>';

                    return $edit . $delete;
                })
                ->rawColumns(['cover_photo','title','status','actions'])
                ->make(true);
    }

    protected function getBulkUploadListings($bulkUpload) {

        return datatables()->of($bulkUpload)
                    ->editColumn('id', function ($bulkUpload) {
                        return 'Bulk Upload ' .$bulkUpload->id;
                    })
                    ->editColumn('listings', function ($bulkUpload) {
                        $total = 0;
                        if($bulkUpload->logs!=''){
                            if($bulkUpload->logs['uploaded_logs']!=null){
                                $getJson = json_decode($bulkUpload->logs['uploaded_logs'], true);
                                $total = count($getJson);
                            } else {
                                $total = 0;
                            }
                        }
                        return $total;
                    })
                    ->editColumn('errors', function ($bulkUpload) {

                        $getErrors = ($bulkUpload->logs['failed_logs'] != null) ? json_decode($bulkUpload->logs['failed_logs'], true) : '';
                        $getMediaErrors = ($bulkUpload->logs['failed_logs_media'] != null) ? json_decode($bulkUpload->logs['failed_logs_media'], true) : '';
                        $getVerificationErrors = ($bulkUpload->logs['failed_logs_verification'] != null) ? json_decode($bulkUpload->logs['failed_logs_verification'], true) : '';
                        $getGovtIdErrors = ($bulkUpload->logs['failed_logs_govtId'] != null) ? json_decode($bulkUpload->logs['failed_logs_govtId'], true) : '';

                        $errorCnt = 0;
                        if((!empty($getErrors))) {

                            $csvErrorsCount = array();
                            if(!empty($getErrors)){
                                foreach ($getErrors as $key => $value) {
                                    if($key!='InvalidPropertyID'){
                                        $csvErrorsCount[] = $key;
                                    }
                                }
                            }

                        } else {
                            $csvErrorsCount[] = '';
                        }

                        $mediaCount = array();
                        if(!empty($getMediaErrors)){
                            foreach($getMediaErrors as $key => $value) {
                                $mediaCount[] = $key;
                            }
                        } else {
                            $mediaCount[] = '';
                        }

                        $verificationCount = array();
                        if(!empty($getVerificationErrors)){
                            foreach($getVerificationErrors as $key => $value) {
                                $verificationCount[] = $key;
                            }
                        } else {
                            $verificationCount[] = '';
                        }

                        $mergedArray = array_unique(array_merge(array_filter($csvErrorsCount),array_filter($mediaCount),array_filter($verificationCount)), SORT_REGULAR);
                        $errorCnt = ($getGovtIdErrors == '' ? count($mergedArray) : 1);
                        return $errorCnt;
                    })
                    ->editColumn('created_at', function ($bulkUpload) {
                        $createdAt = $bulkUpload->created_at;
                        $cdate = carbonParseDateFormatFDY($createdAt);
                        $ctime = date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($createdAt, Session::get('timezone'))));
                        return $cdate . ' at ' . $ctime;
                    })
                    ->editColumn('status', function ($bulkUpload) {
                        $createdAt = $bulkUpload->updated_at;
                        $cdate = carbonParseDateFormatFDY($createdAt);
                        $ctime = date('h:i A',strtotime((new \App\Helpers\Helper)->convertToLocal($createdAt, Session::get('timezone'))));
                        $uploadedTime = $cdate . ' ' . $ctime;

                        $status = '';
                        if($bulkUpload->status == 'pending'){
                            $queue = '0';  $rowPosition = '0';
                            if($bulkUpload->queue_id!='0'){
                                $queue = $this->bulkUploadServiceContract->getQueueCount();
                                $rank = $this->bulkUploadServiceContract->getrowPosition($bulkUpload->queue_id);
                                $rowPosition = $rank[0]->position;
                            }
                            $totalqueue = $queue;

                            $queueMsg = '<label class="orange" style="font-weight:normal !important;">Your bulk upload job is '.$rowPosition.' out of '.$totalqueue.' in the queue. We appreciate your patience.</label>';
                            $uploadStatus = '<label class="orange">Upload In Progress</label>';
                            $status = $uploadStatus . '<br>' . $queueMsg;

                        } else if($bulkUpload->status == GlobalConstantDeclaration::BOOKING_STATUS_COMPLETED){
                            $status = '<label class="green">Uploaded '.$uploadedTime.'</label>';
                        } else if($bulkUpload->status == 'failed'){
                            $status = '<label class="red">Failed</label>';
                        }
                        return $status;
                    })
                    ->addColumn('results', function ($bulkUpload) {
                        if($bulkUpload->logs!=''){
                            $uri = route('bulkUploadReview',['id' => $bulkUpload->logs['id']]);
                        } else {
                            $uri = '';
                        }

                        $results = ($bulkUpload->status == 'completed') ? '<a href="'.$uri.'" class="gray-btn">Review Upload Results</a>' : '';
                        return $results;
                    })
                    ->rawColumns(['id','created_at','status','results'])
                    ->make(true);
    }

    protected function paymentHistory($paymentHistory){

        return datatables()->of($paymentHistory)
                    ->editColumn('id', function ($paymentHistory) {
                        return $paymentHistory->id;
                    })
                    ->editColumn('paymentdate', function ($paymentHistory) {
                        return carbonParseDateFormatMDYWithComma($paymentHistory->created_at);
                    })
                    ->editColumn('name', function ($paymentHistory) {
                        return $paymentHistory->first_name . ' ' . $paymentHistory->last_name;
                    })
                    ->editColumn('amount', function ($paymentHistory) {
                        return Helper::formatMoney($paymentHistory->amount,0);
                    })
                    ->addColumn('renter', function ($paymentHistory) {
                        $isVerified = $paymentHistory->is_verified;
                        $isTripper = $paymentHistory->is_tripper;
                        if($isVerified){
                            if ($isTripper) {
                                $verified = 'Verified Tripper';
                            } else {
                                $verified = 'Verified Renter';
                            }
                        } else {
                            $verified = 'not Verified';
                        }
                        return $verified;
                    })
                    ->addColumn('duration', function ($paymentHistory) {
                        $nightsDays = carbonDateDiffInDays($paymentHistory->check_out_date,$paymentHistory->check_in_date);
                        if($nightsDays==1){
                            $nightLabel = ' night';
                        } else {
                            $nightLabel = ' nights';
                        }
                        return  $nightsDays.  $nightLabel ;
                    })
                    ->editColumn('location_detail', function ($paymentHistory) {
                        return $paymentHistory->city . ', ' . $paymentHistory->state;
                    })
                    ->editColumn('price', function ($paymentHistory) {
                        $days = carbonDateDiffInDays($paymentHistory->check_in_date,$paymentHistory->check_out_date);
                        $price = ($paymentHistory->actual_price) / $days;
                        return Helper::formatMoney($price,0).' / night';
                    })
                    ->addColumn('property', function ($paymentHistory) {
                        return '<a class="custom-link-blue" href=' .URL("propertydetail/".$paymentHistory->propertyId).' target="_blank">'.$paymentHistory->title.'</a>';
                    })
                    ->editColumn('status', function ($paymentHistory) {
                        if($paymentHistory->payment_type == 'completed'){
                            $status = 'Successful';
                        } else if ($paymentHistory->payment_type == 'refunded') {
                            $status = 'Refunded';
                        }
                        return $status;

                    })
                    ->addColumn('actions', function ($paymentHistory) {
                        $amount = Helper::formatMoney($paymentHistory->actual_price,0);
                        if($paymentHistory->payment_type == 'completed'){
                            $confirmButton = '<span><a data-toggle="modal" href="javascript:void(0)" class="booking-icons cancelBooking" data-id="'.$paymentHistory->id.'"  data-amount="'.$amount.'"><svg width="20" height="20" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.39333 22.5096C8.66174 26.3472 14.9951 26.7846 19.7505 23.5702L21.3895 25.2082L23.1555 18.6172L16.5635 20.3832L18.3225 22.1422C14.2582 24.6762 8.9947 24.1422 5.5222 20.8434C2.0497 17.5446 1.2464 12.3155 3.56865 8.12651C5.89089 3.93756 10.7513 1.84824 15.3889 3.04547C20.0264 4.24269 23.2682 8.42366 23.2725 13.2132H25.2505C25.2501 7.47339 21.3407 2.47146 15.771 1.0845C10.2012 -0.302461 4.40268 2.28205 1.71062 7.35143C-0.981439 12.4208 0.12492 18.6721 4.39333 22.5096ZM13.7725 5.57024H11.7355L11.7345 5.57324V7.32024C10.0475 7.57024 8.50852 8.50724 8.50852 10.4702C8.50852 12.4932 10.0575 13.3382 11.7345 13.8182V17.0252C10.9375 16.6595 10.4034 15.888 10.3415 15.0132L8.07552 15.2382C8.12363 17.2748 9.70465 18.9448 11.7355 19.1042V20.8532H13.7725V19.1332C14.3083 19.0832 14.8339 18.9548 15.3325 18.7522C16.6078 18.234 17.4394 16.9918 17.4325 15.6152C17.4325 13.1992 15.7665 12.4382 13.7725 11.9242V9.35824C14.3521 9.63405 14.7333 10.2061 14.7645 10.8472L17.0945 10.7672C17.0185 8.59824 15.6085 7.58724 13.7725 7.32124V5.57024ZM13.7725 14.3332V17.0992C14.5098 16.9935 15.0671 16.3774 15.0985 15.6332C15.0985 14.9432 14.4795 14.5672 13.7725 14.3332ZM11.7385 9.30024V11.3802C11.1915 11.1572 10.7305 10.8282 10.7305 10.3182C10.7305 9.74324 11.2075 9.44424 11.7385 9.30024Z" fill="#B7BACA"/>
                                </svg></a></span>';
                        } else {
                            $confirmButton = '';
                        }
                        $tick = $confirmButton;
                        $chat = '<span><a class="booking-icons" href="'.route('owner.chat').'?id='.(new \App\Helpers\Helper)->urlsafe_b64encode($paymentHistory->messageid).'"><svg width="20" height="20" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.71917 0.399903H18.4332C19.0013 0.399374 19.5463 0.62454 19.9484 1.02587C20.3505 1.4272 20.5766 1.97181 20.5772 2.53991V13.9589C20.5769 14.5272 20.3508 15.072 19.9487 15.4736C19.5466 15.8751 19.0014 16.1004 18.4332 16.0999H10.6132L6.05417 20.1999C5.83402 20.3986 5.51398 20.4405 5.25008 20.3052C4.98618 20.17 4.83338 19.8856 4.86617 19.5909L5.25617 16.1009H2.71917C2.15108 16.1012 1.60615 15.8758 1.20426 15.4742C0.802366 15.0727 0.576437 14.528 0.576172 13.9599V2.54091C0.576437 1.97281 0.802366 1.42809 1.20426 1.02658C1.60615 0.62506 2.15108 0.39964 2.71917 0.399903ZM4.14917 12.5319H17.0052C17.2665 12.5417 17.5122 12.4079 17.6457 12.1831C17.7793 11.9583 17.7793 11.6785 17.6457 11.4537C17.5122 11.2289 17.2665 11.0951 17.0052 11.1049H4.14917C3.88789 11.0951 3.64216 11.2289 3.50862 11.4537C3.37507 11.6785 3.37507 11.9583 3.50862 12.1831C3.64216 12.4079 3.88789 12.5417 4.14917 12.5319ZM17.0052 8.96391H4.14917C3.88789 8.97369 3.64216 8.83988 3.50862 8.61509C3.37507 8.39031 3.37507 8.1105 3.50862 7.88572C3.64216 7.66093 3.88789 7.52712 4.14917 7.53691H17.0052C17.2665 7.52712 17.5122 7.66093 17.6457 7.88572C17.7793 8.1105 17.7793 8.39031 17.6457 8.61509C17.5122 8.83988 17.2665 8.97369 17.0052 8.96391ZM4.14917 5.39591H17.0052C17.2665 5.40569 17.5122 5.27188 17.6457 5.04709C17.7793 4.82231 17.7793 4.5425 17.6457 4.31772C17.5122 4.09293 17.2665 3.95912 17.0052 3.96891H4.14917C3.88789 3.95912 3.64216 4.09293 3.50862 4.31772C3.37507 4.5425 3.37507 4.82231 3.50862 5.04709C3.64216 5.27188 3.88789 5.40569 4.14917 5.39591Z" fill="#B7BACA"/></svg></a></span>';
                        return $tick . ' ' . $chat;
                    })
                    ->rawColumns(['id','name','paymentdate', 'property', 'status','actions','amount'])
                    ->make(true);
    }

    protected function paymentMethods($paymentMethods){
        return datatables()->of($paymentMethods)
                ->editColumn('id', function ($paymentMethods) {
                    return $paymentMethods->id;
                })
                ->editColumn('institution_name', function ($paymentMethods) {
                    if($paymentMethods->default == GlobalConstantDeclaration::VALUE_TRUE) {
                        return $paymentMethods->institution_name .'<span class="subtitle">Default Payment Method</span>';
                    }
                    return $paymentMethods->institution_name;
                })
                ->editColumn('account_number', function ($paymentMethods) {
                    return '******'.substr($paymentMethods->account_number,-4);
                })
                ->editColumn('routing_number', function ($paymentMethods) {
                    return '******'.substr($paymentMethods->routing_number,-4);
                })
                ->addColumn('institutionType', function ($paymentMethods) {
                    return 'Bank';
                })
                ->addColumn('actions', function ($paymentMethods) {
                    $edit = '<a href="javascript:void(0)" class="click-icon" onClick="editBankDetails('.$paymentMethods->id.')"><svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.49815 15.5789C5.3667 15.9717 5.5785 16.3966 5.97132 16.5282L5.97384 16.5299C6.12802 16.5791 6.29368 16.5791 6.44786 16.5299L10.9468 15.0287C11.058 14.992 11.159 14.9299 11.2418 14.8472L21.118 4.96782C22.2391 3.84577 22.2391 2.02806 21.118 0.90601C20.5797 0.367356 19.8492 0.0646973 19.0875 0.0646973C18.3257 0.0646973 17.5953 0.367356 17.0569 0.90601L7.17906 10.7854C7.09623 10.8671 7.03401 10.9673 6.99752 11.0778L5.49815 15.5789ZM1.61737 20.4054C2.03936 20.8275 2.61188 21.0647 3.20888 21.0647H16.7074C17.3046 21.0649 17.8775 20.8279 18.2998 20.4058C18.7221 19.9836 18.9592 19.4111 18.959 18.8141V11.313C18.959 10.8987 18.623 10.5628 18.2085 10.5628C17.794 10.5628 17.4579 10.8987 17.4579 11.313V18.8141C17.4582 19.0132 17.3792 19.2042 17.2384 19.3451C17.0976 19.486 16.9066 19.5651 16.7074 19.5651H3.20888C2.79424 19.5647 2.45836 19.2286 2.45836 18.8141V3.81186C2.45882 3.39787 2.79471 3.0625 3.20888 3.0625H12.2076C12.6224 3.0625 12.9586 2.72644 12.9586 2.31189C12.9586 1.89733 12.6224 1.56127 12.2076 1.56127H3.20888C1.96603 1.5622 0.958985 2.56955 0.958985 3.81186V18.8141C0.95854 19.4108 1.19539 19.9833 1.61737 20.4054Z" fill="#B7BACA"/></svg></a>';
                    $delete = '<a href="javascript:void(0)" class="click-icon" onclick="deleteCard('.$paymentMethods->id.','.GlobalConstantDeclaration::TYPE_BANK_ACCOUNT.')" data-toggle="modal" data-target="#delete-credit"><svg width="19" height="23" viewBox="0 0 19 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18.5061 4.65103V3.50303C18.5034 3.18798 18.2469 2.93402 17.9316 2.93403H12.7618V1.78203C12.7618 1.14801 12.2473 0.634033 11.6127 0.634033H7.01741C6.38319 0.634585 5.86934 1.1484 5.86934 1.78203V2.92903H0.699534C0.382228 2.92903 0.125 3.18602 0.125 3.50303V4.65103C0.125 4.96804 0.382228 5.22503 0.699534 5.22503H17.9316C18.2489 5.22503 18.5061 4.96804 18.5061 4.65103ZM4.57407 22.589C3.36443 22.5921 2.3587 21.6594 2.27193 20.454L1.273 6.52003H17.362L16.3851 20.454C16.2983 21.6594 15.2926 22.5921 14.0829 22.589H4.57407Z" fill="#B7BACA"/></svg></a>';
                    return $edit . ' '. $delete;
                })
                ->rawColumns(['id','institution_name','account_number', 'routing_number', 'institutionType','actions'])
                ->make(true);
    }

    protected function creditCardPaymentMethods($paymentCards){
        return datatables()->of($paymentCards)
                ->editColumn('id', function ($paymentCards) {
                    return $paymentCards->id;
                })
                ->editColumn('expiration_date', function ($paymentCards) {
                    return $paymentCards->expiration_date;
                })
                ->editColumn('card_type', function ($paymentCards) {
                    return 'Credit';
                })
                ->editColumn('last_four', function ($paymentCards) {
                    return '******'.substr($paymentCards->last_four,-4);
                })
                ->addColumn('cardname', function ($paymentCards) {
                    if($paymentCards->default == GlobalConstantDeclaration::VALUE_TRUE) {
                        return $paymentCards->card_type .'<span class="subtitle">Default Payment Method</span>';
                    }
                    return $paymentCards->card_type;
                })
                ->addColumn('actions', function ($paymentCards) {
                    $edit = '<a href="javascript:void(0)" class="click-icon edit-cards" onClick="editCard('.$paymentCards->id.')"><svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M5.49815 15.5789C5.3667 15.9717 5.5785 16.3966 5.97132 16.5282L5.97384 16.5299C6.12802 16.5791 6.29368 16.5791 6.44786 16.5299L10.9468 15.0287C11.058 14.992 11.159 14.9299 11.2418 14.8472L21.118 4.96782C22.2391 3.84577 22.2391 2.02806 21.118 0.90601C20.5797 0.367356 19.8492 0.0646973 19.0875 0.0646973C18.3257 0.0646973 17.5953 0.367356 17.0569 0.90601L7.17906 10.7854C7.09623 10.8671 7.03401 10.9673 6.99752 11.0778L5.49815 15.5789ZM1.61737 20.4054C2.03936 20.8275 2.61188 21.0647 3.20888 21.0647H16.7074C17.3046 21.0649 17.8775 20.8279 18.2998 20.4058C18.7221 19.9836 18.9592 19.4111 18.959 18.8141V11.313C18.959 10.8987 18.623 10.5628 18.2085 10.5628C17.794 10.5628 17.4579 10.8987 17.4579 11.313V18.8141C17.4582 19.0132 17.3792 19.2042 17.2384 19.3451C17.0976 19.486 16.9066 19.5651 16.7074 19.5651H3.20888C2.79424 19.5647 2.45836 19.2286 2.45836 18.8141V3.81186C2.45882 3.39787 2.79471 3.0625 3.20888 3.0625H12.2076C12.6224 3.0625 12.9586 2.72644 12.9586 2.31189C12.9586 1.89733 12.6224 1.56127 12.2076 1.56127H3.20888C1.96603 1.5622 0.958985 2.56955 0.958985 3.81186V18.8141C0.95854 19.4108 1.19539 19.9833 1.61737 20.4054Z" fill="#B7BACA"/></svg></a>';
                    $delete = '<a href="javascript:void(0)" class="click-icon delete-cards"  onClick="deleteCard('.$paymentCards->id.','.GlobalConstantDeclaration::TYPE_CARD.')" data-toggle="modal" data-target="#delete-credit"><svg width="19" height="23" viewBox="0 0 19 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18.5061 4.65103V3.50303C18.5034 3.18798 18.2469 2.93402 17.9316 2.93403H12.7618V1.78203C12.7618 1.14801 12.2473 0.634033 11.6127 0.634033H7.01741C6.38319 0.634585 5.86934 1.1484 5.86934 1.78203V2.92903H0.699534C0.382228 2.92903 0.125 3.18602 0.125 3.50303V4.65103C0.125 4.96804 0.382228 5.22503 0.699534 5.22503H17.9316C18.2489 5.22503 18.5061 4.96804 18.5061 4.65103ZM4.57407 22.589C3.36443 22.5921 2.3587 21.6594 2.27193 20.454L1.273 6.52003H17.362L16.3851 20.454C16.2983 21.6594 15.2926 22.5921 14.0829 22.589H4.57407Z" fill="#B7BACA"/></svg></a>';
                    return $edit . ' '. $delete;
                })
                ->rawColumns(['id','cardname','last_four', 'card_type', 'expiration_date','actions'])
                ->make(true);
    }
}
