<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\DTO\Api\RentalUserDTO;
use App\Http\Requests\Api\Tripper\RegisterTripperRequest;
use App\Libraries\SMS;
use App\Providers\RouteServiceProvider;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use App\Services\PaymentServices\Braintree\Address\AddressRequestDTO;
use App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Braintree\Subscription\SubscriptionRequestDTO;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\RentalUserService;
use App\Services\RentalUserServiceContract;


use App\Models\User;
use App\Models\Owner;
use App\Models\RentalUser;
use App\Models\Admin;
use App\Models\Hotels;
use App\Models\TripboardShare;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use JWTAuth;
use Twilio\Rest\Client;
use Carbon\Carbon;
use App\Models\PaymentCards;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\CommunicationPreferences;
use App\Services\RegisterServiceContract;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;



    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public $rentalRepository;
    public $paymentService;
    public $rentalUserService;

    public function __construct(
        RegisterServiceContract $registerServiceContract
    )
    {

        $exceptArr = ['validateOtp', 'resendOtp','authenticateOtp','user.logout','tripperRegister','resendRenterTripperOtp','authenticateUserOtp','createRentalTripperUser'];
        $route = \Request::route()->getName();
        if(strpos($route,"owner") > -1){
            if(Session::has('owner_token') &&  !in_array($route, $exceptArr) )
            {
                Redirect::to('owner_panel')->send();
            }
        }

        else if(strpos($route,"admin") > -1){

            if(Session::has('admin_token') &&  !in_array($route, $exceptArr) )
            {
               Redirect::to('/admin/kpi-dashboard')->send();
            }
        }

        $this->registerServiceContract = $registerServiceContract;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }



    public function createRentalTripperUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'=>'email|nullable',
            'registerphoneno'=>'regex:/^([0-9]{3}\)-[0-9]{3}-[0-9]{4}$|nullable'
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
        }

        $email = $request->email;
        $phone = $request->registerphoneno;
        $returnArray = $this->registerServiceContract->createRentalTripperUser($email,$phone);
        return $this->renderData($returnArray);

    }

    protected function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'password' => 'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ],[
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'password.regex' => 'Password must be combination of uppercase, lowercase, digits & special characters',
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false ,'error'=>$validator->errors()->all()]);
        }
        $firstName = $request->first_name;
        $lastName = $request->last_name;
        $password= $request->password;
        $tripper = $request->tripper;
        $this->registerServiceContract->createUser($firstName,$lastName,$password,$tripper);

        return $this->renderData(['success'=>true ,'error'=>'','session' => Session::all()]);
    }



    public function registerTripper(
        RegisterTripperRequest $request
    )
    {

        try
        {
            $dtoData = RentalUserDTO::formRequest($request);
            $userId = getRegisterTripperId();
            $this->registerServiceContract->registerTripper($dtoData,$userId);
            $success = true;
            $error = '';
        } catch (\Exception $exception) {
            $success = false;
            $error = $exception->getMessage();
        }

        $id = $request->id;
        return $this->renderData(['success'=>$success , 'error'=> $error, 'id'=> $id]);
    }

    /**
     * validaate OTP after a valid registration.
     *
     * @param  Request
     */
    public function authenticateUserOtp(Request $request)
    {
        try {
            if($request->otp_code != '')
            {
                $otp = decrypt(Session::get('userOtp'));
                if($otp == $request->otp_code){
                    $seconds = round((strtotime(Carbon::now()) - strtotime(Session::get('otpTime'))), 1);
                    if($seconds <= 60){
                        $success = true;
                        $error = '';
                    } else{
                        $success = false;
                        $error = array('error' => 'This registration code expired, please click <a href="javascript:;" id="resendOtp" class="resendOtpRental" style="color: red;font-weight: bold;">Resend</a> for a new code.');
                    }
                } else {
                    $success = false;
                    $error = array('error' => 'Invalid code. Please enter valid registration code');
                }
            }else {
                $success = false;
                $error = array('error' => 'Registration code is required.');
            }

        } catch (Exception $exception) {
            $success = false;
            $error = array('error'=>'Could not validate your Request. Please contact site administrator' . $exception->getMessage());
        }

        return $this->renderData(['success'=>$success, 'error' => $error,'session' => Session::all()]);
    }

    public function resendRenterTripperOtp(Request $request)
    {
        try {
            $otp =  rand(10000,99999);

            if(Session::has('userOtp')== true){
                session()->forget('userOtp');
            }

            if(Session::has('otpTime')== true){
                session()->forget('otpTime');
            }

            Session::put('userOtp',encrypt($otp));
            Session::put('otpTime',Carbon::now());
            Session::save();
            if(Session::get('userEmail') != ''){
                sendUserEmail(Session::get('userEmail'),$otp);
            }
            if(Session::get('userPhone') != ''){
                $phone = preg_replace('/\D+/', '', Session::get('userPhone'));
                sendUserMessage('+1'.$phone,$otp);
            }
            $error = '';

        } catch (\Exception $exception) {
            $error = $exception->getMessage();
            \Log::error('OTP::'. $exception->getMessage());
        }

        return response()->json(['success'=>true, 'error' => $error]);
    }

}
