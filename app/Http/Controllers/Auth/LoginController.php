<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\SMS;
use App\Models\Owner;
use App\Models\Admin;
use App\Models\PaymentCards;
use App\Models\TripboardShare;
use App\Providers\RouteServiceProvider;
use App\Models\RentalUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Illuminate\Support\Facades\Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Hash;
use DB;
use Illuminate\Support\Facades\Cookie;
use Redirect;
use App\Services\LoginServiceContract;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;




    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        LoginServiceContract $loginServiceContract
    )
    {
        $this->loginServiceContract = $loginServiceContract;
        $exceptArr = ['validateOtp', 'user.logout','userlogin'];
        $route = \Request::route()->getName();
        if(strpos($route,"owner") > -1){
            if(Session::has('owner_token') &&  !in_array($route, $exceptArr) )
            {
                Redirect::to('owner_panel/property')->send();
            }
        }

        else if(strpos($route,"admin") > -1){

            if(Session::has('admin_token') &&  !in_array($route, $exceptArr) )
            {
               Redirect::to('/admin/kpi-dashboard')->send();
            }
        }


    }


    public function showRentalLoginForm()
    {
        $this->view = 'rentaluser.home';
        return $this->renderData(['url' => 'rentaluser']);
    }

    public function userLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'email'=>'email|nullable',
            'phone'=>'regex:/^([0-9]{3}\)-[0-9]{3}-[0-9]{4}$|nullable'
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
        }

        try {
            $loginAray =  $this->loginServiceContract->userLogin($request);
            return $this->renderData($loginAray);


        } catch (JWTException $e) {
            return $this->renderData(['success'=>false, 'error'=> array('You are not authorized to access. Try again or contact site administrator')]);
        }

    }



    public function showAdminLoginForm()
    {
        $this->view = 'admin.login';
        return $this->renderData(['url' => 'admin']);
    }
    public function adminLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        try {

            $adminLoginArray = $this->loginServiceContract->adminLogin($request);
            return $this->renderData($adminLoginArray);
        } catch (JWTException $e) {
            return $this->renderData(['success'=>false,'error' => array('Not authrozide. Try again or conatct admin')]);
        }
        return response()->json(['success'=>true , 'admin'=>$adminLoginArray,'redirect'=>'admin']);
    }

    public function showRentalRegisterForm()
    {
        $this->view = 'rentaluser.register';
        return $this->renderData(['url' => 'rentaluser']);
    }

    public function showAdminRegisterForm()
    {
        $this->view = 'admin.register';
        return $this->renderData(['url' => 'admin']);
    }

    public function adminLogout() {
        Auth::guard('admin')->logout();
        Session::forget('admin_token');
        Session::forget('admin');
        return response()->json(['success'=>true,'redirect_url'=>url('/admin')]);
    }

    public function userLogout() {
        Auth::guard('rentaluser')->logout();
        Auth::guard('owner')->logout();
        Session::forget(['rental_token','wasRecentlyCreated','user','rentalUserEmailTemp','tripboard_name','tripboard_share_id','owner_token','owner','unique_key']);

        // delete tripboard name cookie during log out if it is set
        if(Cookie::has('tripboardName')){
            Cookie::queue(Cookie::forget('tripboardName'));
        }
        return $this->renderData(['success'=>true,'redirect_url'=>url('/')]);
    }

    public function forgotRenterTripperPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'emailId'   => 'required|email|max:225|exists:rental_user,email'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        $this->loginServiceContract->forgotRenterTripperPassword($request->emailId);
    }

    public function forgotAdminPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'emailId'   => 'required|email|max:225|exists:admin,email'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        return $this->loginServiceContract->forgotAdminPassword($request->emailId);
    }



}
