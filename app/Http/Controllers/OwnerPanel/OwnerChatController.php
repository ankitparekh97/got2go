<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Helpers\GlobalConstantDeclaration;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Helper;
use Carbon\Carbon;
use App\Libraries\PusherFactory;
use DB;
use Session;
use App\Services\SendMessageServiceContract;
use App\Services\Renter\ChatServiceContract;
use Illuminate\Validation\Rule;
use Validator;

class OwnerChatController extends Controller
{

    public $sendMessageService;

    public function __construct(
        ChatServiceContract $chatServiceContract,
        SendMessageServiceContract $sendMessageServiceContract
    )
    {
        $this->chatService = $chatServiceContract;
        $this->sendMessageService = $sendMessageServiceContract;
    }

   /**
     * Show the application chat dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $this->view = 'owner.chat.index';
        return $this->renderData(['success'=>true]);
    }

    public function getData(Request $request)
    {
        $name = $request->name;
        $users = $this->chatService->getOwnerChatData($name);
        $html = view('owner.chat.chat-contact')->with('users', $users)->render();
        return $this->renderData(['success'=>true,'users'=>$users,'html'=>$html]);
    }

    /**
     * getLoadLatestMessages
     *
     *
     * @param Request $request
     */
    public function getLoadLatestMessages(Request $request)
    {
        Session::put('timezone',$request->header('Timezone'));
        $pairId = Helper::urlsafe_b64decode($request->user_id);
        $requestCheck = ['id'=>$pairId];
        $validator = Validator::make($requestCheck,[
            'id' => 'required|exists:owner_rental_pair,id',
        ]);

        if($validator->fails()){
            return response()->json(['state'=>0,'error'=>"something went wrong. Please Try again."]);
        }
        $return = $this->chatService->getOwnerMessages($pairId);
        return response()->json(['state' => 1, 'messages' => $return]);
    }

    /**
     * postSendMessage
     *
     * @param Request $request
     */
    public function postSendMessage(Request $request)
    {
        $pairId = Helper::urlsafe_b64decode($request->id);
        $requestValidation = ['id'=>$pairId,'message'=>$request->message];
        $validator = Validator::make($requestValidation,[
            'id' => 'required|exists:owner_rental_pair,id',
            'message'=>'required|min:2|max:1000'
        ]);
        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        $fromUser = Auth::guard('owner')->user()->id;
        $toUser = $request->to_user;
        $message = $request->message;
        $this->sendMessageService->postCommonSendMessage($pairId,'owner','rental',$fromUser,$toUser,'owner',$message);
        return response()->json(['success'=>true]);
    }

    public function unreadFlag(Request $request){
        $pairId = Helper::urlsafe_b64decode($request->id);
        $toUserId = getOwnerId();
        $requestCheck = ['pair_id'=>$pairId,'to_user'=>$toUserId];
        $validator = Validator::make($requestCheck,[
            'pair_id' => [
                        'required',
                        Rule::exists('messages')->where(function ($query) use($requestCheck) {
                            $query->where($requestCheck);
                        }),
                    ],
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }

       $this->chatService->unreadFalg($pairId,$toUserId);
        return $this->renderData(['success'=>true]);
    }
}
