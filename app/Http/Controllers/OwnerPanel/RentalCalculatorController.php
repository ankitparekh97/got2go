<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class RentalCalculatorController extends Controller
{
    public function index(Request $request){
        $host = env("AIRDNA_HOST");
        $token = env("AIRDNA_TOKEN");
        $version = env("AIRDNA_VERSION");

        $url = "$host/client/$version/rentalizer/estimate?access_token=$token&address=$request->address&bedrooms=$request->bedrooms&bathrooms=$request->bathrooms&accommodates=$request->guests&currency=usd&lat=39.739235&lng=-104.990250&show_urls=True";
        
        $url = "$host/client/$version/rentalizer/estimate?access_token=$token&address=525 Pier Ave Santa Monica CA&bedrooms=3&bathrooms=2&accommodates=6&currency=usd&lat=39.739235&lng=-104.990250&show_urls=True";
        
        echo $url;
         /* $response = Curl::to('https://jsonplaceholder.typicode.com/posts')
                            ->get();


        dd($response); */
        $response = Curl::to("https://api.airdna.co/client/v1/rentalizer/estimate")
                            ->get();


        dd($response);
    }
}
