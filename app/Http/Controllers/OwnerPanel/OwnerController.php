<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\SMS;
use App\Models\Owner;
use Hash;
use JWTAuth;
use Auth;
use Illuminate\Support\Str;


class OwnerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->view = 'owner.home';
        return $this->renderData([]);
    }

}
