<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\PropertyType;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PaymentCards;
use App\Models\Booking;
use App\Models\Subscription;
use Validator;

use Session;
use Auth;
use DB;
use Carbon\Carbon;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use App\Models\BookingPayment;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\Controllers\DataTables;
use App\Http\DTO\Api\BookingDTO;
use App\Services\PaymentServices\Stripe\Refund\RefundRequestDTO;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use App\Repositories\BookingInterface;
use App\Services\BookingContract;
use App\Services\BookingService;
use App\Services\PaymentCardContract;
use App\Services\PropertyServiceContract;

use App\Services\SendMessageServiceContract;

class BookingController extends DataTables
{



    public $propertyServiceContract;
    public $bookingServiceContract;
    public $paymentServiceContract;

    public function __construct(
        PropertyServiceContract $propertyServiceContract,
        BookingContract $bookingServiceContract,
        PaymentCardContract $paymentServiceContract,
        SendMessageServiceContract $sendMessageServiceContract
    )
    {
        $this->propertyServiceContract = $propertyServiceContract;
        $this->bookingServiceContract = $bookingServiceContract;
        $this->paymentServiceContract = $paymentServiceContract;
        $this->sendMessageService = $sendMessageServiceContract;
    }

    public function bookingList()
    {
        $propertyType = $this->propertyServiceContract->getPropertyTypes();
        $this->view = 'owner.booking.property';
        return $this->renderData(compact('propertyType'));
    }

    public function propertyBooking()
    {
        try {

            $status = array(
                GlobalConstantDeclaration::BOOKING_STATUS_PENDING,
                GlobalConstantDeclaration::BOOKING_STATUS_AUTO_REJECTED,
                GlobalConstantDeclaration::PROPERTY_STATUS_REJECTED
            );

            $pendingBookings = $this->ownerBookings($status,'pending');
            return $pendingBookings;

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function confirmedPropertyBooking()
    {
        try {

            $status = array(
                GlobalConstantDeclaration::BOOKING_STATUS_APPROVED,
                GlobalConstantDeclaration::BOOKING_STATUS_CONFIRMED,
                GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED,
                GlobalConstantDeclaration::BOOKING_STATUS_INSTANT_BOOKING,
            );

            $confirmedBookings = $this->ownerBookings($status,'approved');
            return $confirmedBookings;

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function updatePropertyStatus(Request $request)
    {
        try {

            $transaction = DB::transaction(function() use($request){

                $bookingId = $request->id;
                $status = $request->action;
                $confirmationId = $request->confirmation_id;

                $requestCheck = ['id'=>$bookingId,'status'=>$request->action];
                $validator = Validator::make($requestCheck,[
                    'id' => 'required|integer|exists:booking,id',
                    'status'=> 'required|IN:pending,rejected,auto_reject,cancelled,approved,confirmed,instant_booking'
                ]);

                if($validator->fails()){
                    return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
                }

                $paymentService = new PaymentService();
                $bookings = $this->bookingServiceContract->updateBookingStatus($bookingId,$status,$confirmationId);
                $cardDetails = $this->paymentServiceContract->getcardDetails(GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_RENTAL,$bookings->rental_user_id);
                $this->bookingServiceContract->stripePayment($bookings,$cardDetails,$status);

                if($status == GlobalConstantDeclaration::BOOKING_STATUS_CANCELLED){
                    $request->total = GlobalConstantDeclaration::TOTAL;
                    $refundAmmount = $this->calculateRefund($request);
                    $refundAmmount = $refundAmmount->original['refundAmount'];
                    $bookingPayments = $this->bookingServiceContract->getByParamsObject(array('booking_id'=>$bookingId,'payment_type' => GlobalConstantDeclaration::BOOKING_STATUS_COMPLETED));

                    if($refundAmmount != 0){
                        $refundRequestDTO = RefundRequestDTO::convertModelToObject($bookingPayments,$refundAmmount);
                        $refundRequestDTO->amount = round($refundAmmount,2);
                        $refundRequestDTO->charge = $bookingPayments->transaction_number;
                        $refundRequestDTO->bookingId = $bookingPayments->booking_id;

                        $refund = $paymentService->createRefund($refundRequestDTO);
                        $this->bookingServiceContract->createBookingPayment($bookingPayments,$refundAmmount,$refund,GlobalConstantDeclaration::BOOKING_STATUS_REFUNDED);

                        $toUser = Helper::urlsafe_b64encode($bookingPayments->booking->property->owner_id);
                        $userMessage = "The host has canceled the reservation for ".$bookingPayments->booking->property->title." from ".date('M jS, Y',strtotime($bookingPayments->booking->check_in_date))." to ".date('M jS, Y',strtotime($bookingPayments->booking->check_out_date)).". A total of $".$refundAmmount." will be refunded.";
                        $this->sendMessageService->postOwnerRentalSendMessage($toUser,$userMessage);

                        $toUser = Helper::urlsafe_b64encode(getOwnerId());
                        $userMessage = "You have canceled the reservation for ".$bookingPayments->booking->property->title." from ".date('M jS, Y',strtotime($bookingPayments->booking->check_in_date))." to ".date('M jS, Y',strtotime($bookingPayments->booking->check_out_date)).". A total of $".$bookingPayments->booking->actual_price." will be refunded back to the guest.";
                        $this->sendMessageService->postAdminOwnerSendMessage($toUser,$userMessage);
                    }
                }

                return $this->renderData(['success'=>true,'error'=>'','booking'=>$bookings,'message'=>'Booking Status updated to '.ucfirst($status).' Successfully', 'owner_status'=>$status]);
            });

            return $transaction;
        } catch (\Throwable $th) {

            $error = stripeErrorMessage($th);
            return $this->renderData(['success'=>false,'error'=>$error]);
        }
    }

    public function calculateRefund(Request $request)
    {
        $requestBookingCheck = ['id'=>$request->id];
        $validator = Validator::make($requestBookingCheck,[
            'id' => 'required|exists:booking,id',
        ]);

        if($validator->fails()){
            return false;
        }

        $booking = $this->bookingServiceContract->getBookingById($requestBookingCheck['id']);
        if($booking->bookingPayment == null){
            $this->bookingServiceContract->updateBookingOwnerStatus($booking->id, 'cancelled');
                return $this->renderData([
                'refundAmount' => $refundAmount,
            ]);
        }
        $bookingChcekInDate = $booking->check_in_date.' '.date("H:i:s", strtotime($booking->property->check_in));
        $bookingChcekOutDate = $booking->check_out_date.' '.date("H:i:s", strtotime($booking->property->check_out));
        $checkInDate = carbonParseDate($bookingChcekInDate,'America/Chicago');
        $checkOutDate = carbonParseDate($bookingChcekOutDate,'America/Chicago');
        $paymentCreatedDate = $booking->bookingPayment->created_at;
        $days = carbonDateDiffInDays($checkInDate,$checkOutDate);
        $startingPrice = $booking->actual_price;
        $totalAmount = $booking->total;
        $refundAmount = calculateOwnerRefund($checkInDate,$days,$startingPrice,$paymentCreatedDate,$totalAmount);
        if($request->total == GlobalConstantDeclaration::TOTAL){
            $refundAmount = $totalAmount;
        }

        return $this->renderData([
            'refundAmount' => $refundAmount,
        ]);
    }


}
