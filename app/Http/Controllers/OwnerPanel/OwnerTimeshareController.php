<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\OwnerTimeshareProperty;
use App\PropertyAmenities;
use App\PropertyType;
use App\MasterAmenities;
use Illuminate\Support\Facades\Validator;
use File;
use App\Traits\EmailsendTrait;
use Auth;
use DB;
use Session;
use App\Hotels;
use Carbon\Carbon;
use Config;
class OwnerTimeshareController extends Controller
{
    use EmailsendTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $timeshares = Property::with(['propertyamenities','ownertimeshare.hotels'])->where(['owner_id'=>Auth::guard('owner')->user()->id,'type_of_property'=>'vacation_rental'])->get();
        //dd($timeshares->toarray());
        $this->view = 'owner.timeshare.index';
        return $this->renderData(['success'=>true,'timeshares'=>$timeshares]);
    }

    public function showHotelRequest(Request $request)
    {

        Session::put('requestReferrer', url()->previous());
        return view('owner.timeshare.hotelrequest');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('owner.timeshare.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!$request->has('partialPyament'))
        {
            $request['partialPyament'] = '0';
        }

        if(empty($request->ltdTimeValue))
        {
            $request['ltdTimeValue'] = '0';
        }
        //dd($request->all());
         $validator =  Validator::make($request->all(), [
            'hotel'=>'required',
            'propertyTypeId'=>'required',
            'description'=>'required',
            'timeshareDescripion'=>'required',
            'location'=>'required',
            'noOfGuest'=>'required',
            'unitSize'=>'required',
            'checkinDate'=>'required',
            'checkoutDate'=>'required',
            'minDays'=>'required',
            'bookingBeforeDays'=>'required',
            'price'=>'required',
            /* 'serviceFee'=>'required',
            'occupancyAndTaxFee'=>'required', */
            // 'partialPyament'=>'required',
            'partialPyamentAmount'=>'required_if:partialPyament,1',
            'cancellationType'=>'required',
//            'ltdTimeFrame'=>'required',
//            'ltdTimeValue'=>'required',
            'membershipNumber'=>'required',
            'document'=>'required|image|mimes:jpeg,png,jpg,gif,svg,pdf,doc|max:2048',
            'structureType'=>'required',
            'unit_aminities.*'=>'required',
            'resort_aminities.*'=>'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        if($request->hasFile('document')){
            $fileName = 'timeshare_document_'.time().'.'.$request->file('document')->getClientOriginalExtension();


            $path = public_path('uploads/timeshare');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('document')->move($path, $fileName);
            $request['document'] = $fileName;
            //dd($request['document']);
        }


     // Owner Property store.

        $data = array(
            'owner_id'=>Auth::guard('owner')->user()->id,
            'property_type_id'=>$request->propertyTypeId,
            'description'=>$request->description,
            'location_detail'=>$request->location,
            'price'=>$request->price,
            'service_fees'=>Config::get('property.service_fee'),
            'occupancy_taxes_and_fees'=>Config::get('property.occupancy_taxt_and_fee'),
            'is_partial_payment_allowed'=>$request->partialPyament,
            'partial_payment_amount'=>$request->partialPyamentAmount,
            'cancellation_type'=>$request->cancellationType,
            'type_of_property'=>'timeshare',
        ); 
        $timeshare = Property::create($data);

       OwnerTimeshareProperty::create([
        'property_id'=>$timeshare->id,
        'hotels_id' => $request->hotel,
        'timeshare_description' => $request->timeshareDescripion,
        'no_of_guest' => $request->noOfGuest,
        'unit_size' => $request->unitSize,
        'check_in_date' => date('Y-m-d',strtotime($request->checkinDate)),
        'check_out_date' => date('Y-m-d',strtotime($request->checkoutDate)),
        'min_days' => $request->minDays,
        'allow_booking_before' => $request->bookingBeforeDays,
        'ltd_time_frame' => $request->ltdTimeFrame,
        'ltd_value' => $request->ltdTimeValue,
        'membership_number' => $request->membershipNumber,
        'document' => $fileName,
        'structure_type'=>$request->structureType]); 

        if($request->has('unit_aminities')){

            $aminities = $request->unit_aminities;
            foreach($aminities as $key=>$value){
                $aminities_array[] = [
                    'property_id'=>$timeshare->id,
                    'property_type'=>'timeshare',
                    'amenity_id'=>$aminities[$key],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                    'amenities_type'=>'unit'
                ];
            }

             PropertyAmenities::insert($aminities_array);

        }

        if($request->has('resort_aminities')){

            $resort_aminities = $request->resort_aminities;
            foreach($resort_aminities as $key=>$value){
                $resort_aminities_array[] = [
                    'property_id'=>$timeshare->id,
                    'property_type'=>'timeshare',
                    'amenity_id'=>$resort_aminities[$key],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                    'amenities_type'=>'resort'
                ];
            }

             PropertyAmenities::insert($resort_aminities_array);


        }

        $hotel_name = Hotels::find($request->hotel)->name;

        $toMail = 'g2gadmin2020@yopmail.com';
        $toName = 'Timeshare Admin';
        $emailRecord = DB::table('email_template')->where('task','request_property')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body); 
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{owner_name}', Auth::guard('owner')->user()->first_name.' '.Auth::guard('owner')->user()->last_name, $body);
        $body = str_replace('{property_name}', 'Timeshare' , $body);
        $body = str_replace('{name}', $hotel_name , $body);


        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
        Session::flash('success', 'Timeshare saved successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/owner_panel/timeshare')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
            /* $timeshare = Property::with(['ownertimeshare' => function($ownertimeshare) use ($id) {
                $ownertimeshare->join('hotels', 'hotels.id','=','hotels_id');
                return $ownertimeshare;
            },'propertyimages','propertyamenities'=> function($amenities) use ($id) {
                $amenities->join('master_amenities', 'master_amenities.id','=','amenity_id');
                return $amenities;
            }])
            ->where(['id'=>$id,'owner_id'=>Auth::guard('owner')->user()->id])->first(); */

            $timeshare = Property::with(['ownertimeshare','propertyimages','propertyamenities'=>function($aminities){
                $aminities->where(['property_type'=>'hotel']);
                return $aminities;
            },'ownertimeshare.hotels','propertyamenities.masteramenity'])->where(['id'=>$id,'owner_id'=>Auth::guard('owner')->user()->id])->first();
        
            if($timeshare == null){
                Session::flash('error', 'Not authorized to access that page');
                $this->$redirect = '/owner_panel/timeshare';
                return $this->renderData(['success'=>false,'message'=>'Not authorized to access that page']);
                
            }

            $propertyType = PropertyType::get();
            $this->view = 'owner.timeshare.view';
            return $this->renderData(['success'=>true,'timeshare'=>$timeshare,'propertyType'=>$propertyType]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $timeshare = Property::with(['ownertimeshare','propertyamenities'])->where(['id'=>$id,'owner_id'=>Auth::guard('owner')->user()->id])->first();

        if($timeshare == null){
            Session::flash('error', 'Not authorized to access that page');
            $this->$redirect = '/owner_panel/timeshare';
            return $this->renderData(['success'=>false,'message'=>'Not authorized to access that page']);
        }

        $this->view = 'owner.timeshare.edit';
        return $this->renderData(['success'=>true,'timeshare'=>$timeshare]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator =  Validator::make($request->all(), [
            'hotel'=>'required',
            'propertyTypeId'=>'required',
            'description'=>'required',
            'timeshareDescripion'=>'required',
            'location'=>'required',
            'noOfGuest'=>'required',
            'unitSize'=>'required',
            'checkinDate'=>'required',
            'checkoutDate'=>'required',
            'minDays'=>'required',
            'bookingBeforeDays'=>'required',
            'price'=>'required',
            'serviceFee'=>'required',
            'occupancyAndTaxFee'=>'required',
            'partialPyament'=>'required',
            'partialPyamentAmount'=>'required',
            'cancellationType'=>'required',
            'ltdTimeFrame'=>'required',
            'ltdTimeValue'=>'required',
            'membershipNumber'=>'required',
            'document'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'structureType'=>'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        if($request->hasFile('document')){
            $fileName = 'timeshare_document_'.time().'.'.$request->file('document')->getClientOriginalExtension();

            $path = public_path('uploads/timeshare');

            if(!File::isDirectory($path)){

                File::makeDirectory($path, 0777, true, true);

            }
            $request->file('document')->move($path, $fileName);
            $request['document'] = $document = $fileName;
        }


     // Owner Property store.

        $data = array(
            'owner_id'=>Auth::guard('owner')->user()->id,
            'property_type_id'=>$request->propertyTypeId,
            'description'=>$request->description,
            'location_detail'=>$request->location,
            'price'=>$request->price,
            'service_fees'=>$request->serviceFee,
            'occupancy_taxes_and_fees'=>$request->occupancyAndTaxFee,
            'is_partial_payment_allowed'=>$request->partialPyament,
            'partial_payment_amount'=>$request->partialPyamentAmount,
            'cancellation_type'=>$request->cancellationType,
            'type_of_property'=>'timeshare',
        ); 

        $update_data = array('hotel_id' => $request->hotel,
        'property_id'=>$timeshare->id,
        'timeshare_description' => $request->timeshareDescripion,
        'no_of_guest' => $request->noOfGuest,
        'unit_size' => $request->unitSize,
        'check_in_date' => date('Y-m-d',strtotime($request->checkinDate)),
        'check_out_date' => date('Y-m-d',strtotime($request->checkoutDate)),
        'min_days' => $request->minDays,
        'allow_booking_before' => $request->bookingBeforeDays,
        'ltd_time_frame' => $request->ltdTimeFrame,
        'ltd_value' => $request->ltdTimeValue,
        'membership_number' => $request->membershipNumber,
        'structure_type'=>$request->structureType);

        if(isset($request->document))
            $update_data['document']=$request->document;

        Property::where('id',$id)->update($data);
        OwnerTimeshareProperty::where('property_id',$id)->update($update_data); 

        $oldUnitAmenities = PropertyAmenities::where(['property_id'=>$id,'amenities_type'=>'unit'])->latest()->get()->pluck('amenity_id')->toArray(); 
        
        if(!empty($oldUnitAmenities)){ 
           
           if(!empty($request->unit_aminities)){
            $removeAmenities=array_diff($oldUnitAmenities,$request->unit_aminities);
            $insertAmenities= array_diff($request->unit_aminities,$oldUnitAmenities);
            if(isset($removeAmenities)&& !empty($removeAmenities)){
                foreach($removeAmenities as $remove){
                    PropertyAmenities::where(array('property_id'=>$id,'amenity_id'=>$remove,'amenities_type'=>'unit','property_type'=>'timeshare'))->delete();
                }
            }
            if(isset($insertAmenities)&& !empty($insertAmenities)){
                foreach($insertAmenities as $insert){
                   PropertyAmenities::create(array('amenity_id'=>$insert,'property_id'=>$id,'amenities_type'=>'unit','property_type'=>'timeshare'));
                }
            }
        }else { PropertyAmenities::where(array('property_id'=>$id))->delete();}
        }else{
            if(!empty($request->unit_aminities)){
                foreach($request->unit_aminities as $amenity)
                {   
                   PropertyAmenities::create(array('amenity_id'=>$amenity,'property_id'=>$id,'amenities_type'=>'unit','property_type'=>'timeshare'));
                }
            }
        }

        $oldResortAmenities = PropertyAmenities::where(['property_id'=>$id,'amenities_type'=>'resort'])->latest()->get()->pluck('amenity_id')->toArray(); 
        
        if(!empty($oldResortAmenities)){ 
           

           if(!empty($request->resort_aminities)){
            $removeResortAmenities=array_diff($oldResortAmenities,$request->resort_aminities);
            $insertResortAmenities= array_diff($request->resort_aminities,$oldResortAmenities);
            if(isset($removeResortAmenities)&& !empty($removeResortAmenities)){
                foreach($removeResortAmenities as $remove){
                    PropertyAmenities::where(array('property_id'=>$id,'amenity_id'=>$remove,'amenities_type'=>'resort','property_type'=>'timeshare'))->delete();
                }
            }
            if(isset($insertResortAmenities)&& !empty($insertResortAmenities)){
                foreach($insertResortAmenities as $insert){
                   PropertyAmenities::create(array('amenity_id'=>$insert,'property_id'=>$id,'amenities_type'=>'resort','property_type'=>'timeshare'));
                }
            }
        }else { PropertyAmenities::where(array('property_id'=>$id))->delete();}
        }else{
            if(!empty($request->resort_aminities)){
                foreach($request->resort_aminities as $amenity)
                {   
                   PropertyAmenities::create(array('amenity_id'=>$amenity,'property_id'=>$id,'amenities_type'=>'resort','property_type'=>'timeshare'));
                }
            }
        }

        Session::flash('success', 'Timeshare updated successfully!');
        return response()->json(['success'=>true,'redirect_url'=>url('/owner_panel/timeshare')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $timeshare = Property::find($id);
        $timeshare->delete();
        Session::flash('success', 'Timeshare deleted successfully!');
        return response()->json(['success'=>true, 'redirect_url'=>url('/owner_panel/timeshare')]);
    }

    public function requestHotels(Request $request)
    {
       //dd(Auth::guard('owner')->user()->email);
        $this->validate($request, [
        'hotelname' =>  'required'
        ]);

        $toMail = 'g2gadmin2020@yopmail.com';
        $toName = 'Timeshare Admin';
        $emailRecord = DB::table('email_template')->where('task','request_hotel')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body); 
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', LIVE_SITE_URL, $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{owner_name}', Auth::guard('owner')->user()->first_name.' '.Auth::guard('owner')->user()->last_name, $body);
        $body = str_replace('{hotel_name}', $request->hotelname , $body);
        $body = str_replace('{property_type}', 'Resort/Hotel' , $body);
        $body = str_replace('{location}', $request->location , $body);
        $body = str_replace('{city}', $request->country , $body);
        $body = str_replace('{state}', $request->state , $body);

        $this->send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);
        Session::flash('success', 'Your Hotel/Resort Request has been sent successfully.');
        $this->$redirect = Session::get('requestReferrer');
        return $this->renderData(['success'=>false,'message'=>'Your Hotel/Resort Request has been sent successfully.']);
        /* return redirect(Session::get('requestReferrer'))->with('success', 'Your Hotel/Resort Request has been sent successfully.'); */
    }

    public function hotels()
    {
        $hotels = Hotels::all();
        return response()->json(['success'=>true,'data'=>$hotels->toArray()]);
    }

    public function propertyType()
    {
        $property_type = PropertyType::all();
        return response()->json(['success'=>true,'data'=>$property_type->toArray()]);
    }

    public function getHotelById(Request $request)
    {
        $hotel = Hotels::with(['propertyamenities'=>function($aminities){
             $aminities->where(['property_type'=>'hotel']);
             return $aminities;
        },'propertyamenities.masteramenity'])->find($request->id);
        //dd($hotel->toarray());
        return response()->json(['success'=>true,'data'=>$hotel->toArray()]);
    }

    public function amenities()
    {
        $amenities = MasterAmenities::where(['type'=>'unit'])->get();
        return response()->json(['success'=>true,'data'=>$amenities->toArray()]);
    }

    public function publish(Request $request)
    {
        //dd($request->all());
        DB::statement( "UPDATE property SET publish = if(publish='1','0','1')  Where id=".$request->id );
        return response()->json(['success'=>true]);
    }

}
