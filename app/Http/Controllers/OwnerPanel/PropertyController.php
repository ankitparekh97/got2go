<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Models\BulkUpload as AppBulkUpload;
use App\Models\BulkUploadLogs;
use App\Helpers\GlobalConstantDeclaration;
use App\Helpers\Helper;
use App\Models\Hotels;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DataTables;
use App\Http\DTO\Api\AmenitiesDTO;
use App\Http\DTO\Api\AvaiblitiesDTO;
use App\Http\DTO\Api\BulkUploadDTO;
use App\Http\DTO\Api\HotelsDTO;
use App\Http\DTO\Api\PropertyBedroomsDTO;
use App\Http\DTO\Api\PropertyDTO;
use App\Http\DTO\Api\PropertyMediaDTO;
use App\Http\DTO\Api\PropertyVerificationDTO;
use App\Models\Jobs;
use App\Jobs\BulkUpload;
use App\Models\MasterAmenities;
use App\Models\PropertyType;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PropertyAmenities;
use App\Models\PropertyAvaiblities;
use App\Models\PropertyImages;
use App\Models\PropertyBedrooms;
use App\Models\PropertyOwnershipDocuments;
use App\Services\BulkUploadService;
use App\Services\BulkUploadServiceContract;
use App\Services\PropertyServiceContract;
use App\Traits\BulkUploadValidation;
use Illuminate\Support\Facades\File;
use Validator;

use Illuminate\Support\Facades\Session;
use Auth;
use DB;
use Carbon\Carbon;
use Config;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Throwable;
use ZipArchive;

class PropertyController extends DataTables
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $propertySelect = [];
    protected $dir = 'app/bulkUpload/';
    protected $storagePath = '';

    public $propertyServiceContract;
    public $bulkUploadServiceContract;

    public function __construct(
        PropertyServiceContract $propertyServiceContract,
        BulkUploadServiceContract $bulkUploadServiceContract
    ){
        $this->storagePath = storage_path($this->dir);
        $this->propertyServiceContract = $propertyServiceContract;
        $this->bulkUploadServiceContract = $bulkUploadServiceContract;
    }

    public function index()
    {
        $this->view = 'owner.property.index';
        return $this->renderData(['success'=>true]);
    }

    private function propertyListing($timeZone,$status){

            $ownerId = getOwnerId();
            $requestPropertyCheck = ['timezone'=>$timeZone];
            $validator = Validator::make($requestPropertyCheck,[
                'timezone' => 'required'
            ]);

            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>'Cannot Process request. Please try again.']);
            }

            if(!Session::has('timezone')){
                Session::put('timezone',$timeZone);
                Session::save();
            }

            $pendingProperties = $this->ownerListings($ownerId,$status);
            return $pendingProperties;
    }

    public function pendingListings(Request $request){

        try {

            $timeZone = $request->timezone;
           $status = array(
                GlobalConstantDeclaration::PROPERTY_STATUS_PENDING,
                GlobalConstantDeclaration::PROPERTY_STATUS_DRAFT,
                GlobalConstantDeclaration::PROPERTY_STATUS_REJECTED
            );
            return $this->propertyListing($timeZone,$status);

        } catch (\Throwable $th) {
            return $this->renderData(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function currentListings(Request $request){

        try {
            $timeZone = $request->timezone;
            $status = array(
                GlobalConstantDeclaration::PROPERTY_STATUS_APPROVED
            );
            return $this->propertyListing($timeZone,$status);
        } catch (\Throwable $th) {
            return $this->renderData(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $propertyType = $this->propertyServiceContract->getPropertyTypes();
       $amenities = $this->propertyServiceContract->getMasterAmenities();
       $hotels = $this->propertyServiceContract->getHotels();
       $property = array();
       return view('owner.property.create', compact('propertyType','amenities','hotels', 'property'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadmedia(Request $request)
    {
        if($request->has('photos'))
        {
            $propertyMediaDTO = PropertyMediaDTO::convertToObject($request);
            $media = $propertyMediaDTO::rules($request);
            if (!$media){
                return response()->json(['success'=>false ,'error'=> $media->id]);
            }
        }

        if($request->has('proof_of_ownership') || $request->has('gov_id'))
        {
            $propertyVerificationDTO = PropertyVerificationDTO::convertToObject($request);
            $verification = $propertyVerificationDTO::rules($request);
            if (!$verification){
                return response()->json(['success'=>false ,'error'=> $verification->id]);
            }
        }

        return $this->propertyServiceContract->savePropertyMediaDocuments($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name_your_listing' => 'required|max:50',
            'describe_listing' => 'required|min:50',
            'about_space'=>'required|min:50',
        ]);

        if($validator->fails()){
            return response()->json(['validation'=> 'true','error'=>$validator->errors()->all()]);
        }

        $propertyDtoArr = PropertyDTO::formRequest($request);

        $resortId = 0;
        if($request->has('txt_resort') && $request->get('txt_resort')!= ''
            && $request->get('type_of_property') == GlobalConstantDeclaration::BOOKING_TYPE_VACATION_RENTAL)
        {
            $getHotel = $this->propertyServiceContract->getHotelsByParamsObject(array('name'=>$request->get('txt_resort')));

            if(empty($getHotel)){
                $hotelDTO = HotelsDTO::convertToObject($request);
                $hotel = $this->propertyServiceContract->saveHotel($hotelDTO);
                $resortId = (int)$hotel->id;
            } else {
                $resortId = (int)$getHotel->id;
            }
        }

        $propertyDtoArr->resortId = $resortId;
        $propertyDtoArr->publish = '1';
        $property = $this->propertyServiceContract->saveProperty((array)$propertyDtoArr);

        if(!empty($property))
        {
            // avaibility dates
            if($request->has('property_available_when') && $request->has('property_available_when_to')){
                $this->propertyServiceContract->saveAvaiblities($request,$property);
            }

            // amenities
            $propertyAmenitiesDTO = AmenitiesDTO::convertToObject($request);
            if(!empty($propertyAmenitiesDTO->amenityId)){
                $this->propertyServiceContract->saveAmenities($propertyAmenitiesDTO,$property);
            }

            // bedrooms
            $propertyBedroomsDTO = PropertyBedroomsDTO::convertToObject($request);
            if(!empty(array_filter($propertyBedroomsDTO->bedType))){
                $this->propertyServiceContract->savepropertyBedRooms($propertyBedroomsDTO,$propertyDtoArr->totalBeds,$property);
            }

            // delete removed documents
            $this->propertyServiceContract->deleteDocuments($request);
            $redirect_url = route('property.edit', $property->id);
            return response()->json(['success'=>true,'property'=>$property,'redirect_url'=> $redirect_url]);

        } else {
            return response()->json(['success'=>false]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $requestPropertyCheck = ['id'=>$id,'owner_id'=>getOwnerId()];
        $validator = Validator::make($requestPropertyCheck,[
            'id' => 'required|integer|exists:property,id,owner_id,'.$requestPropertyCheck['owner_id'],
        ]);

        if($validator->fails()){
            return view('errors.500');
        }

        $property = $this->propertyServiceContract->findPropertyById($id);
        $propertyType = $this->propertyServiceContract->getPropertyTypes();
        $amenities = $this->propertyServiceContract->getMasterAmenities();
        $hotels = $this->propertyServiceContract->getHotels();

        $this->view = 'owner.property.create';
        $arr = (compact('propertyType','amenities','hotels', 'property'));
        $arr['success'] = true;
        return $this->renderData($arr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $requestPropertyCheck = ['id'=>$id,'owner_id'=>getOwnerId()];
        $validator = Validator::make($requestPropertyCheck,[
            'id' => 'required|integer|exists:property,id,owner_id,'.$requestPropertyCheck['owner_id']
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false,'error'=>'Cannot Process request. Please try again.']);
        }

        $property = $this->propertyServiceContract->deleteProperty($id);
        return $this->renderData(['success'=>true,'property'=>$property]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request)
    {
        $id = $request->id;
        $requestPropertyCheck = ['id'=>$id,'owner_id'=>getOwnerId()];
        $validator = Validator::make($requestPropertyCheck,[
            'id' => 'required|integer|exists:property,id,owner_id,'.$requestPropertyCheck['owner_id']
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false,'error'=>'Cannot Process request. Please try again.']);
        }

        $property = $this->propertyServiceContract->publishProperty($id);
        return $this->renderData(['success'=>true,'property'=>$property['property'],'updated_dt'=>$property['updatedAt']]);
    }

    public function bulkUploadForm($id = null){
        $this->view = 'owner.property.bulkUpload';

        if($id!= ''){

            $requestBulkUploadCheck = ['id'=>$id,'owner_id'=>getOwnerId()];
            $validator = Validator::make($requestBulkUploadCheck,[
                'id' => 'required|integer|exists:bulk_upload,id,owner_id,'.$requestBulkUploadCheck['owner_id']
            ]);
            $reuploadRow = $this->bulkUploadServiceContract->getBulkUploadListings(null,$id);
            if($validator->fails()){
                return view('errors.500');
            }

            $uniqueKey = $reuploadRow[0]->unique_key;
            $reuploadCsv = ($reuploadRow[0]->logs['failed_logs'] == null) ? $reuploadRow[0]->property_doc : '';
            $reuploadMedia = ($reuploadRow[0]->logs['failed_logs_media'] == null) ? $reuploadRow[0]->property_images : '';
            $reuploadVerfication = ($reuploadRow[0]->logs['failed_logs_verification'] == null) ? $reuploadRow[0]->verfication_doc : '';
            $reuploadGovtid = ($reuploadRow[0]->logs['failed_logs_govtId'] == null) ? $reuploadRow[0]->govtid_doc : '';

        } else {
            $reuploadCsv = '';
            $reuploadMedia = '';
            $reuploadVerfication = '';
            $reuploadGovtid = '';
            $uniqueKey = '';
        }

        return $this->renderData(['success'=>true,'unique_key'=>$uniqueKey,'reuploadCsv'=>$reuploadCsv,'reuploadMedia'=>$reuploadMedia,'reuploadVerfication'=>$reuploadVerfication,'reuploadGovtid'=>$reuploadGovtid]);
    }

    public function bulkDownload(Request $request){

        $bulkUploadDTO = BulkUploadDTO::formRequest($request);
        if($request->hasFile('propertyData')){
            $requestPropertyDataCheck = [
                'propertyData'=>$bulkUploadDTO->propertyData,
                'extension' => $request->file('propertyData')->getClientOriginalExtension()
            ];

            $validator = Validator::make($requestPropertyDataCheck,[
                'propertyData' => 'required',
                'extension' => 'required|in:csv',
            ]);

            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
            }
        }

        if($request->hasFile('propertyImages')){
            $requestPropertyImagesCheck = [
                'propertyImages'=>$bulkUploadDTO->propertyImages,
                'extension' => $request->file('propertyImages')->getClientOriginalExtension()
            ];

            $validator = Validator::make($requestPropertyImagesCheck,[
                'propertyImages' => 'required',
                'extension' => 'required|in:zip',
            ]);
            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
            }
        }

        if($request->hasFile('propertyVerficationDocs')){
            $requestpropertyVerficationDocs = [
                'propertyVerficationDocs'=>$bulkUploadDTO->propertyVerficationDocs,
                'extension' => $request->file('propertyVerficationDocs')->getClientOriginalExtension()
            ];

            $validator = Validator::make($requestpropertyVerficationDocs,[
                'propertyVerficationDocs' => 'required',
                'extension' => 'required|in:zip',
            ]);

            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
            }
        }

        if($request->hasFile('propertyGovtId')){
            $requestPropertyGovtCheck = [
                'propertyGovtId'=>$bulkUploadDTO->propertyGovt,
                'extension' => $request->file('propertyGovtId')->getClientOriginalExtension()
            ];

            $validator = Validator::make($requestPropertyGovtCheck,[
                'propertyGovtId' => 'required|mimes:zip',
                'extension' => 'required|in:zip',
            ]);
            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
            }
        }


        return $this->bulkUploadServiceContract->bulkDownload($request);
    }

    public function bulkUploadSubmit(Request $request){

        $bulkUploadDTO = BulkUploadDTO::convertToObject($request);
        $requestBulkUploadCheck = [
                    'unique_key'=>$bulkUploadDTO->uniqueKey,
                    'UploadedCsv'=>$bulkUploadDTO->propertyDoc,
                    'UploadedImages'=>$bulkUploadDTO->propertyMedia,
                    'UploadedGovtId'=>$bulkUploadDTO->propertyVerification,
                    'UploadedVerificationDocs'=>$bulkUploadDTO->propertyGovtId];

        $validator = Validator::make($requestBulkUploadCheck,[
            'unique_key' => 'required',
            'UploadedCsv' => 'required',
            'UploadedImages' => 'required',
            'UploadedGovtId' => 'required',
            'UploadedVerificationDocs' => 'required',
        ]);

        if($validator->fails()){
            return $this->renderData(['success'=>false,'error'=>$validator->errors()->all()]);
        }

        $uniqueId = $bulkUploadDTO->uniqueKey;
        $getUniqueKey = $this->bulkUploadServiceContract->getByUniqueId($uniqueId);
        if(!$getUniqueKey || $request->is_reupload == 1){
            $bulkUploadId =  $this->bulkUploadServiceContract->saveBulkUpload($bulkUploadDTO);
            $result = $this->bulkUpload($bulkUploadId);

            if($result!=''){
                $bulkUploadDTO->id = $bulkUploadId;
                $bulkUploadDTO->queue_id = $result;
                $this->bulkUploadServiceContract->saveBulkUpload($bulkUploadDTO);
            }
            return $this->renderData(['success'=>true,'result'=>$result]);
        }
    }

    public function bulkUploadReview($id){
        $this->view = 'owner.property.bulkreviewlistings';

        try {

            $requestBulkUploadReviewCheck = ['id'=>$id,'owner_id'=>getOwnerId()];
            $validator = Validator::make($requestBulkUploadReviewCheck,[
                'id' => 'required|integer|exists:bulk_upload_logs,id'
            ]);

            if($validator->fails()){
                return view('errors.500');
            }

            $logs = $this->bulkUploadServiceContract->getBulkUploadLogs($id);
            return $this->renderData([
                    'success'=>true,
                    'total'=>$logs['total'],
                    'listings'=>$logs['listings'],
                    'property'=>$logs['property'],
                    'errorCnt'=>$logs['errorCnt'],
                    'errors'=>$logs['errors'],
                    'mediaErrors'=>$logs['mediaErrors'],
                    'verificationErrors'=>$logs['verificationErrors'],
                    'govtErrors'=>$logs['govtErrors'],
                ]);

        } catch (Exception $ex){

            Session::flash('error', 'Not authorized to access that page');
            return Redirect::route('owner.property');
        }
    }

    private function bulkUpload($bulkUploadId){
       Log::info("Bulk Upload Queues Begins");
       $upload = $this->dispatch(new BulkUpload($this->propertyServiceContract,$this->bulkUploadServiceContract,$bulkUploadId));
       return $upload;
    }

    public function bulkUploadListing(Request $request){

        try {

            $timeZone = $request->timezone;
            $requestTimezoneCheck = ['timezone'=>$timeZone];
            $validator = Validator::make($requestTimezoneCheck,[
                'timezone' => 'required'
            ]);

            if($validator->fails()){
                return $this->renderData(['success'=>false,'error'=>'Cannot Process request. Please try again.']);
            }

            if(!Session::has('timezone')){
                Session::put('timezone',$timeZone);
                Session::save();
            }

            $bulkUpload = $this->bulkUploadServiceContract->getBulkUploadListings(getOwnerId(),null);
            $bulkUploadLogs = $this->getBulkUploadListings($bulkUpload);
            return $bulkUploadLogs;

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);

        }
    }
}
