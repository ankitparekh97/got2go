<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\PropertyType;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\OwnerTimeshareProperty;
use App\Models\Owner;
use App\Models\PaymentSavingAccounts;
use App\Models\PaymentCards;
use App\Http\DTO\Api\OwnerUserDTO;
use App\Http\DTO\Api\PaymentCardDTO;
use App\Http\Requests\Api\Owner\CardRequest;
use App\Models\Booking;
use App\Helpers\GlobalConstantDeclaration;
use App\Http\Controllers\DataTables;
use App\Http\DTO\Api\PaymentHistoryDTO;
use Validator;

use Session;
use Auth;
use DB;
use Carbon\Carbon;
use App\Repositories\OwnerInterface;
use App\Repositories\OwnerRepository;
use App\Services\PaymentCardContract;
use App\Services\PaymentHistoryContract;
use App\Services\PaymentServices\Braintree\Address\AddressRequestDTO;
use App\Services\PaymentServices\Braintree\CreditCard\CreditCardResponseDTO;
use App\Services\PaymentServices\Braintree\Customer\CustomerResponseDTO;
use App\Services\PaymentServices\Braintree\PaymentMethod\PaymentMethodRequestDTO;
use App\Services\PaymentServices\Stripe\Account\AccountRequestDTO;
use App\Services\PaymentServices\Stripe\Transfer\TransferRequestDTO;
use App\Services\TransferPaymentServiceContract;
use App\Services\PaymentServices\Stripe\PaymentService;
use DateTime;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PropertyServiceContract;

class PaymentHistoryController extends DataTables
{
    public $ownerRepository;
    public $propertyServiceContract;
    public $paymentHistoryContract;
    public $paymentCardContract;

    public function __construct(
        OwnerInterface $ownerRepository,
        PropertyServiceContract $propertyServiceContract,
        PaymentHistoryContract $paymentHistoryContract,
        PaymentCardContract $paymentCardContract
    )
    {
        $this->ownerRepository = $ownerRepository;
        $this->propertyServiceContract = $propertyServiceContract;
        $this->paymentHistoryContract = $paymentHistoryContract;
        $this->paymentCardContract = $paymentCardContract;
    }

    public function historyList(Request $request)
    {
        $propertyType = $this->propertyServiceContract->getPropertyTypes();

        $this->view = 'owner.history.list';
        return $this->renderData(compact('propertyType'));
    }

    public function bookingHistory()
    {
        try {

            $paymentHistory = $this->paymentHistoryContract->bookingHistory();
            return $this->paymentHistory($paymentHistory);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function paymentMethod() {

        try {
                $params = array(
                    'user_id' => getOwnerId(),
                    'user_type' => GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER
                );

                $paymentMethods = $this->paymentHistoryContract->paymentMethod($params);
                return $this->paymentMethods($paymentMethods);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function paymentMethodCredit() {

        try {

            $paymentCards = $this->paymentCardContract->getAllPaymentCards(array('user_id' => getOwnerId()));
            return $this->creditCardPaymentMethods($paymentCards);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function deletePayment(Request $request) {

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ],[
            'id.required' => 'Cannot Process request. Please try again',
            'id.numeric' => 'Cannot Process request. Please try again'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false ,'error'=>$validator->errors()->all()]);
        }

        if ($request->card_type == GlobalConstantDeclaration::TYPE_BANK_ACCOUNT) {
            $getId = $this->paymentHistoryContract->paymentMethodObject(array('id'=>$request->id, 'user_id'=>getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));
            if(!$getId){
                return response()->json(['success'=>false,'message'=>'Cannot Process request. Please try again']);
            }
            $this->paymentHistoryContract->deletepaymentHistory($request->id);

        } else {
            $getId = $this->paymentCardContract->paymentMethodObject(array('id'=>$request->id, 'user_id'=>getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));
            if(!$getId){
                return response()->json(['success'=>false,'message'=>'Cannot Process request. Please try again']);
            }
            $this->paymentCardContract->deletePaymentCard($request->id);
        }

        return response()->json(['success'=>true,'message'=>'Card deleted successfully']);
    }

    public function saveCard(CardRequest $request) {

        try
        {
            $user = getOwner();
            $paymentService = new PaymentService();
            $cardRequestDTO = new CardRequestDTO();
            $paymentCardDTO = PaymentCardDTO::formRequest($request);

            $validate = $paymentCardDTO->validate($request->all());
            if (!$validate){
                return response()->json(['success'=>false ,'error'=> $validate]);
            }

            if($paymentCardDTO->id !='') {

                $paymentCard = $this->paymentCardContract->paymentMethodObject(array('id' => $paymentCardDTO->id, 'user_id'=>getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));
                $cardRequestDTO->token = $paymentCard->card_token;
                $cardRequestDTO->customerId = $user->customer_id;
                $cardRequestDTO->name = $paymentCardDTO->nameOnCard;
                $token = $paymentService->updateCard($cardRequestDTO);

                $paymentCardDTO->debit = $token['funding'];

            } else {

                $tokenRequestDTO = $paymentCardDTO->getTokenRequestDTO();
                $token = $paymentService->createToken($tokenRequestDTO);

                $duplicate = $this->checkDuplicateCard($token, $tokenRequestDTO->expMonth, $tokenRequestDTO->expYear);
                if($duplicate)
                    return response()->json(['success'=> false,'error'=> true,'message'=>'Card details already exits, please add other card.']);


                if($user->customer_id != ''){
                    $cardRequestDTO->token = $token['id'];
                    $cardRequestDTO->customerId = $user->customer_id;
                    $paymentService->createCard($cardRequestDTO);
                } else {
                    $customerRequestDTO = $paymentCardDTO->getCustomerRequestDTO($token,$user);
                    $customer = $paymentService->createCustomer($customerRequestDTO);

                    $ownerDTO = new OwnerUserDTO();
                    $ownerDTO->id = $user->id;
                    $ownerDTO->customerId = $customer['id'];
                    $this->paymentCardContract->saveOwnerCustomerId($ownerDTO);
                }
            }

            $paymentCardDTO->userType = GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER;
            $paymentCard = $this->paymentCardContract->saveCard($paymentCardDTO,$token,$user);

            if($paymentCard != '')
                return $this->renderData(['success'=>true , 'error'=> false, 'message'=>'Your payment details have saved successfully!']);

        } catch (\Exception $exception) {
            return $this->renderData(['success'=>false , 'error'=> 'Your payment information did not go through. Please try again.', 'message'=>$exception->getMessage()]);
        }
    }

    public function saveBank(Request $request) {

        try
        {
            $user = getOwner();
            $paymentService = new PaymentService();
            $paymentHistoryDTO = PaymentHistoryDTO::convertPostDataToObject($request);
            $accountRequestDTO = AccountRequestDTO::formRequest($request);

            $validate = $paymentHistoryDTO->validate($request->all());
            if (!$validate){
                return response()->json(['success'=>false ,'error'=> $validate]);
            }

            if($paymentHistoryDTO->id !='') {
                $paymentbank = $this->paymentHistoryContract->paymentMethodObject(array('id' => $paymentHistoryDTO->id, 'user_id'=>getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));

                $accountRequestDTO->account = $paymentbank[0]->stripe_account_id;
                $accountRequestDTO->firstName = $paymentHistoryDTO->firstName;
                $accountRequestDTO->customerId = $user->stripe_account_id;
                $account = $paymentService->updateAccount($accountRequestDTO);

            } else {

                $account = $paymentService->createAccount($accountRequestDTO);
                $duplicate = $this->checkDuplicateAccount($paymentHistoryDTO->routingNumber,$paymentHistoryDTO->accountNumber);
                if($duplicate) {
                    return response()->json(['success'=>false,'error'=> true, 'message'=>'Bank account details already exits, please add other bank details.']);
                }

                if($user->stripe_account_id == ''){
                    $ownerDTO = new OwnerUserDTO();
                    $ownerDTO->id = $user->id;
                    $ownerDTO->stripeAcountId = $account['external_accounts']['data'][0]['id'];
                    $this->paymentCardContract->saveOwnerCustomerId($ownerDTO);
                }
            }

            $paymentHistory = $this->paymentHistoryContract->saveCard($paymentHistoryDTO,$account,$user);
            if($paymentHistory)
                return $this->renderData(['success'=>true , 'error'=> false, 'message' =>'Your bank details have saved successfully!']);

        } catch (\Exception $exception) {
            return $this->renderData(['success'=>false, 'error'=> 'Your transaction did not go through. Please try again.', 'message' =>$exception->getMessage()]);
        }
    }

    public function editCard(Request $request) {

        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric'
            ],[
                'id.required' => 'Cannot Process request. Please try again',
                'id.numeric' => 'Cannot Process request. Please try again'
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false ,'error'=>$validator->errors()->all()]);
            }

            $getId = $this->paymentCardContract->paymentMethodObject(array('id' => $request->id, 'user_id' => getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));
            if(!$getId){
                return response()->json(['success'=>false,'message'=>'Cannot Process request. Please try again']);
            }

            $paymentCards = $this->paymentCardContract->getPaymentCard(array('id' => $request->id));
            return response()->json(['success'=>true,'paymentCards' => $paymentCards]);
        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function editBankDetails(Request $request) {

        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric'
            ],[
                'id.required' => 'Cannot Process request. Please try again',
                'id.numeric' => 'Cannot Process request. Please try again'
            ]);

            if($validator->fails()){
                return response()->json(['success'=>false ,'error'=>$validator->errors()->all()]);
            }

            $getId = $this->paymentHistoryContract->paymentMethodObject(array('id' => $request->id, 'user_id' => getOwnerId(), 'user_type'=> GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER));
            if(!$getId){
                return response()->json(['success'=>false,'message'=>'Cannot Process request. Please try again']);
            }

            $bankDetails = $this->paymentHistoryContract->paymentMethodObject(array('id' => $request->id));
            return response()->json(['success'=>true,'bankDetails' => $bankDetails]);

        } catch (\Throwable $th) {
            return response()->json(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    public function checkDuplicateCard($token, $exp_month, $exp_year) {

        $params = array(
            'user_id' => getOwnerId(),
            'user_type' => GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER
        );

        return $this->paymentCardContract->checkDuplicateCard($params,$token, $exp_month, $exp_year);
    }

    public function checkDuplicateAccount($routingNumber, $accountNumber) {

        $params = array(
            'user_id' => getOwnerId(),
            'user_type' => GlobalConstantDeclaration::PROPERTY_OFFER_ACTION_BY_OWNER
        );

        return $this->paymentHistoryContract->checkDuplicateAccount($params,$accountNumber);
    }
}
