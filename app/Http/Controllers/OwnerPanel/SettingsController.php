<?php

namespace App\Http\Controllers\OwnerPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->view = 'owner.settings';
        return $this->renderData([]);
    }
}
