<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingPayment;
use App\Models\Configuration;
use App\Helpers\GlobalConstantDeclaration;
use App\Helpers\Helper;
use App\Models\CouponCode;
use App\Models\PropertyOffers;
use App\Models\PaymentCards;
use App\Models\Property;
use App\Models\RentalUser;
use App\Services\CouponCodeContract;
use App\Services\PropertyOfferServiceContract;
use App\Services\Renter\PaymentbookingServiceContract;
use App\Services\Renter\PropertyDetailServiceContract;
use App\Services\SendMessageServiceContract;
use App\Services\BookingAvaibilityServiceContract;
use App\Models\Subscription;


use Carbon\Carbon;
use DateTime;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use App\Libraries\PusherFactory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PaymentServices\Stripe\Charge\ChargeRequestDTO;
use DB;
use Validator;


class PaymentController extends Controller
{



    public $propertyOfferService;
    public $couponCodeService;
    public $paymentService;
    public $propertyDetailService;
    public $sendMessageService;
    public $bookingAvaibilityServiceContract;

    public function __construct(
        PropertyOfferServiceContract $propertyOfferServiceContract,
        CouponCodeContract $couponCodeService,
        PaymentbookingServiceContract $paymentServiceContract,
        PropertyDetailServiceContract $propertyDetailServiceContract,
        SendMessageServiceContract $sendMessageServiceContract,
        BookingAvaibilityServiceContract $bookingAvaibilityServiceContract
    )
    {
        $this->propertyOfferService = $propertyOfferServiceContract;
        $this->couponCodeService = $couponCodeService;
        $this->paymentService = $paymentServiceContract;
        $this->propertyDetailService = $propertyDetailServiceContract;
        $this->sendMessageService = $sendMessageServiceContract;
        $this->bookingAvaibilityServiceContract = $bookingAvaibilityServiceContract;
    }

    public function index(Request $request)
    {
        if(!Auth::guard('rentaluser')->user()){
            return Redirect::route('rental.home');
        }

        $propertyOfferId = $request->get('propertyOfferId');
        $propertyOfferService = null;
        if($propertyOfferId) { // checking which amount should take as final price to bill

            $requestCheck = ['propertyOfferId'=>$propertyOfferId];
            $validator = Validator::make($requestCheck,[
                    'propertyOfferId' => 'required|numeric|exists:property_offers,id',
            ]);

            if($validator->fails()){
                Session::flash('warning', 'Something went Wrong! Try again after some time!');
                return redirect(url()->previous());
            }

            /**
             * @var PropertyOffers $propertyOfferService
             */
            $propertyOfferService = $this->propertyOfferService->getPropertyOfferById($propertyOfferId);
            $id = $propertyOfferService->property_id;
            $guest = $propertyOfferService->number_of_guests;
            $getCheckInCheckoutDate = getStartDateAndEndDateFromPropertyOffer($propertyOfferService);
            $checkInDate = carbonParseDateFormatMDY($getCheckInCheckoutDate['startDate']);
            $checkOutDate = carbonParseDateFormatMDY($getCheckInCheckoutDate['endDate']);

        } else {
            ## Value getting from session
                $id = Session::get('propertyId');
                $guest= Session::get('no_of_guest');
                $checkInDate = Session::get('check_in_date');
                $checkOutDate= Session::get('check_out_date');
            ## End of value getting from session
        }

        $reviewArray = $this->paymentService->getPaymentDetails($id,$checkInDate,$checkOutDate,$propertyOfferService,null);

        $propertyAvaliblity = $this->bookingAvaibilityServiceContract->propertyAvailablity($id,$checkInDate,$checkOutDate);
        $checkbooking = $this->bookingAvaibilityServiceContract->checkBookings($id,$checkInDate,$checkOutDate);
        $reviewArray['no_of_guest'] = $guest;
        $reviewArray['propertyAvaliblity'] = $propertyAvaliblity;
        $reviewArray['propertyOfferId'] = $propertyOfferId;
        $reviewArray['checkbooking'] = $checkbooking;

        $this->view = 'booking.preview';
        return $this->renderData($reviewArray);
    }

    public function propertyBooking(Request $request)
    {

        $validator = Validator::make($request->all(),[
                'id' => 'required|numeric',
                'propertyOfferId'=>'numeric',
                'check_in_date'=>'required|date|before:check_out_date',
                'check_out_date'=>'required|date|after:check_in_date',
                'couponCodeName'=>'min:5|max:20'
        ]);

        if($validator->fails()){
            Session::flash('warning', 'Something went Wrong! Try again after some time!');
            return redirect(url()->previous());
        }

        $id = $request->id;
        $propertyArray = $this->propertyDetailService->getPropertyDetailsById($id);
        $propertyDetails = $propertyArray['propertyDetails'];
        $returnArray = array();

       /*  try { */

            $transaction = DB::transaction(function() use($request,$id,$propertyDetails){

                $propertyOfferId = $request->post('propertyOfferId');
                $checkInDate = $request->post('check_in_date');
                $checkOutDate = $request->post('check_out_date');
                $couponCodeName = $request->post('couponCodeName');
                $guest = $request->guest;
                $selectedCard = $request->selectedCard;
                $couponId = 0;
                $message = $request->message;
                $propertyOfferService = [];
                $couponData = [];
                if($propertyOfferId) {
                    /**
                     * @var PropertyOffers $propertyOfferService
                     */
                    $propertyOfferService = $this->propertyOfferService->getPropertyOfferById($propertyOfferId);
                }

                /**
                 * @var CouponCode $couponData
                 */
                if(!empty($couponCodeName))
                {
                    $couponData = $this->couponCodeService->applyCouponCode($couponCodeName,getLoggedInRental());
                    $couponId = $couponData->id;
                }

                /* Fetch Booking Details */
                $bookingDetails = $this->paymentService->getPaymentDetails($id,$checkInDate,$checkOutDate,$propertyOfferService,$couponData);


                if(empty($bookingDetails['rentalUser']->toarray())){
                    return $this->renderData(['success'=> 'false' , 'propertyDetail'=> $propertyDetails, 'error'=> 'Please add payment details to proceed with booking.']);
                }

                /* Create Booking */
                $booking = $this->paymentService->createBooking($bookingDetails,$checkInDate,$checkOutDate,$propertyOfferId,$guest,$selectedCard,$couponId);

                /* Send Message */
                $toUser = Helper::urlsafe_b64encode($propertyDetails->owner_id);
                $userMessage = Auth::guard('rentaluser')->user()->first_name." ". Auth::guard('rentaluser')->user()->last_name." has book the reservation for ".$propertyDetails->title." from ".date('M jS, Y',strtotime($checkInDate))." to ".date('M jS, Y',strtotime($checkOutDate)).".";

                $this->sendMessageService->postRentalOwnerSendMessage($toUser,$userMessage);
                if($message != ''){
                    $userMessage = $message;
                    $this->sendMessageService->postRentalOwnerSendMessage($toUser,$userMessage);
                }
                /* End of Send Message */


                /* Create Paymnet for instant booking */
                if($propertyDetails->is_instant_booking_allowed == '1'){

                    $this->paymentService->createBookingPayment($bookingDetails,$booking,$selectedCard);
                }
                /* End of create payment */

                if($propertyOfferId) {
                    $this->propertyOfferService->acceptTripperPropertyOffer($propertyOfferId,$booking->id);
                }

                $returnArray['success'] = true;
                $returnArray['error'] = '';
                return $returnArray;

            });
            return $this->renderData(['success'=> $transaction['success'] , 'propertyDetail'=> $propertyDetails, 'error'=> $transaction['error']]);

        /* } catch (\Exception $exception) {
            \Log::error('Booking ::'. $exception->getMessage());
            $error = stripeErrorMessage($exception);
            $returnArray['success'] = false;
            $returnArray['error'] = $error;
            return $this->renderData(['success'=> $returnArray['success'] , 'propertyDetail'=> $propertyDetails, 'error'=> $returnArray['error'] ]);

        } */

    }
}
