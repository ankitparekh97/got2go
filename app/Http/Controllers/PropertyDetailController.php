<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingPayment;
use App\Models\FavTripboards;
use App\Http\Controllers\Controller;
use App\Models\PropertyOffers;
use App\Services\PropertyOfferServiceContract;
use App\Services\Renter\PropertyDetailServiceContract;
use App\Services\SendMessageServiceContract;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\OwnerProperty;
use App\Models\PropertyAmenities;
use App\Models\PropertyImages;
use App\Models\PropertyBedrooms;
use App\Models\OwnerDetails;
use App\Models\TripboardProperty;
use App\Models\Tripboards;
use App\Models\Subscription;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Configuration;
use App\Models\OwnerRentalPair;
use App\Models\Messages;
use App\Helpers\Helper;
use DateTime;
use App\Libraries\PusherFactory;
use App\Models\PropertyAvaiblities;


use Illuminate\Support\Facades\Config;
use App\Repositories\MakeOfferInterface;
use App\Repositories\MakeOfferRepository;
use App\Http\Requests\Api\Tripper\MakeOfferRequest;
use App\Http\DTO\Api\MakeOfferDTO;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie as Cookie;
use Validator;
use Illuminate\Support\Facades\Redirect;

class PropertyDetailController extends Controller
{
    public $propertyOfferServiceContract;
    public $propertyDetailServiceContract;
    public $sendMessageServiceContract;

    public function __construct(
        PropertyOfferServiceContract  $propertyOfferServiceContract,
        PropertyDetailServiceContract $propertyDetailServiceContract,
        SendMessageServiceContract $sendMessageServiceContract
    )
    {
        $this->propertyOfferServiceContract = $propertyOfferServiceContract;
        $this->propertyDetailServiceContract = $propertyDetailServiceContract;
        $this->sendMessageService = $sendMessageServiceContract;
    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        $requestCheck = ['id'=>$id];
        $validator = Validator::make($requestCheck,[
                'id' => 'required|numeric|exists:property,id',
        ]);

        if($validator->fails()){
            Session::flash('warning', 'Something went Wrong! Try again after some time!');
            return redirect(url()->previous());
        }
        try {
            // recently views prodcts
            $this->recentlyViewed($id);

            $propertyArray = $this->propertyDetailServiceContract->getPropertyDetailsById($id);
            $this->view = 'propertydetails.index';
            return $this->renderData($propertyArray);

        } catch (\Throwable $th) {
            return $this->renderData(['success'=>false,'error'=>$th->getMessage()]);
        }
    }

    private function recentlyViewed($id){

        $maxproperty = 3;
        if (isset($id) && $id <> "") {

            if(Session::get('recentlyViewed')!=''){
                $recentlyViewed = Session::get('recentlyViewed');
            } else {
                $recentlyViewed = array();
            }

            if (in_array($id, $recentlyViewed)) {                            // if property is already in the array
                $recentlyViewed = array_diff($recentlyViewed,array($id));       // remove it
                $recentlyViewed = array_values($recentlyViewed);                //optionally, re-index the array
            }

            if (count($recentlyViewed) >= $maxproperty) {                    //check the number of array elements
                $lastId = end($recentlyViewed);
                unset($lastId);

                $removeCookies = array_slice($recentlyViewed,1);               // remove the first element if we have 3 already
                $recentlyViewed = $removeCookies;
                array_push($recentlyViewed,$id);                            //add the current property to the array
            } else {
                array_push($recentlyViewed,$id);
            }
            Session::put('recentlyViewed',$recentlyViewed);
        }
    }


    public function postSendMessage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'to_user' => 'required',
            'message'=>'required|min:5|max:1000'
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false,'error'=>$validator->errors()->all()]);
        }
        /* Send Message */
        $toUser = Helper::urlsafe_b64encode($request->to_user);
        $userMessage = $request->message;
        $this->sendMessageService->postRentalOwnerSendMessage($toUser,$userMessage);
        /* End of Send Message */
        return response()->json(['success'=>true]);
    }

    public function propertyCheckBooking(Request $request){


         $validator = Validator::make($request->all(),[
                'id' => 'required|numeric|exists:property,id',
                'check_in_date'=>'required|date|before:check_out_date',
                'check_out_date'=>'required|date|after:check_in_date',
        ]);

        if($validator->fails()){
            Session::flash('warning', 'Something went Wrong! Try again after some time!');
            return redirect(url()->previous());
        }

        try {

            $propertyId = $request->id;
            $checkInDate = $request->check_in_date;
            $checkOutDate = $request->check_out_date;
            $needOfferValidation = $request->offerValidation;

            $propertyChecking = $this->propertyDetailServiceContract->propertyCheckBooking($propertyId,$checkInDate,$checkOutDate,$needOfferValidation);

            Session::forget('propertyId');
            Session::forget('no_of_guest');
            Session::forget('check_in_date');
            Session::forget('check_out_date');

            Session::put('propertyId', $request->id);
            Session::put('no_of_guest', $request->guest);
            Session::put('check_in_date', $request->check_in_date);
            Session::put('check_out_date', $request->check_out_date);
            Session::save();
            return $this->renderData($propertyChecking);
        } catch (\Exception $exception) {
            $error = $exception->getMessage();
            \Log::error('Booking Instnt Booking::'. $exception->getMessage());
        }
        return $this->renderData(['success'=>true , 'error'=> $error]);
    }
}
