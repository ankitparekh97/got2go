<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\DataTransferObject\DataTransferObject;

class PropertyBedroomsDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var int|null $bed
     */
    public $bed;

    /**
     * @var array|null $bedType
     */
    public $bedType;

    /**
     * @var int|null $bedroomNo
     */
    public $bedroomNo;

    public static function convertToObject($request)
    {
        $propertyBedroomstoArr = array();
        $propertyBedroomstoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $propertyBedroomstoArr['bedType'] = !empty($request->bedType) ? $request->bedType : null;

        return new self($propertyBedroomstoArr);
     }

}
