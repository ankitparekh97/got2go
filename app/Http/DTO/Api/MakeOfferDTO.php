<?php


namespace App\Http\DTO\Api;


use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class MakeOfferDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var int|null $ownerId
     */
    public $ownerId;

    /**
     * @var string|null $message
     */
    public $message;

    /**
     * @var int|null $rentalId
     */
    public $rentalId;

    /**
     * @var string|null $actionBy
     */
    public $actionBy;

    /**
     * @var bool|null $status
     */
    public $status;

    /**
     * @var float|null $tripperOfferNightlyPrice
     */
    public $tripperOfferNightlyPrice;

    /**
     * @var float|null $ownerOfferNightlyPrice
     */
    public $ownerOfferNightlyPrice;

    /**
     * @var string|null $tripperOfferStartDate
     */
    public $tripperOfferStartDate;

    /**
     * @var string|null $tripperofferEndDate
     */
    public $tripperOfferEndDate;

    /**
     * @var string|null $ownerOfferStartDate
     */
    public $ownerOfferStartDate;

    /**
     * @var string|null $ownerOfferEndDate
     */
    public $ownerOfferEndDate;

    /**
     * @var float|null $ownerOfferProposedTotal
     */
    public $ownerOfferProposedTotal;

    /**
     * @var float|null $tripperOfferProposedTotal
     */
    public $tripperOfferProposedTotal;

    /**
     * @var int|null $numberOfGuests
     */
    public $numberOfGuests;

    /**
     * @var bool|null $flexibleDates
     */
    public $flexibleDates;

    /**
     * @var float|null $finalAcceptedNightlyPrice
     */
    public $finalAcceptedNightlyPrice;

     public static function convertPostDataToObject($request)
     {
            $timeZone = $request->post('timezone',\Session::get('timezone'));

            $rentalUserDtoArr = array();
            $rentalUserDtoArr['id'] = !empty($request->post('id')) ? (int)$request->post('id') : null;
            $rentalUserDtoArr['propertyId'] = (int)$request->post('propertyId');
            $rentalUserDtoArr['ownerId'] = (int)$request->post('ownerId');
            $rentalUserDtoArr['message'] = $request->post('messgae');
            $rentalUserDtoArr['rentalId'] = !empty($request->post('rentalId')) ? (int)$request->post('rentalId') : null;
            $rentalUserDtoArr['actionBy'] = $request->post('actionBy');
            $rentalUserDtoArr['status'] = $request->post('status');
            $rentalUserDtoArr['tripperOfferNightlyPrice'] = !empty($request->post('tripperOfferNightlyPrice')) ? (float)$request->post('tripperOfferNightlyPrice') : null;
            $rentalUserDtoArr['ownerOfferNightlyPrice'] = !empty($request->post('ownerOfferNightlyPrice')) ? (float)$request->post('ownerOfferNightlyPrice') : null;
            $rentalUserDtoArr['tripperOfferStartDate'] = !empty($request->post('tripperOfferStartDate')) ? carbonParseDateFormatYMD($request->post('tripperOfferStartDate')) : null;
            $rentalUserDtoArr['tripperOfferEndDate'] = !empty($request->post('tripperOfferEndDate')) ? carbonParseDateFormatYMD($request->post('tripperOfferEndDate')) : null;
            $rentalUserDtoArr['ownerOfferStartDate'] = !empty($request->post('ownerOfferStartDate')) ? carbonParseDateFormatYMD($request->post('ownerOfferStartDate')) : null;
            $rentalUserDtoArr['ownerOfferEndDate'] = !empty($request->post('ownerOfferEndDate')) ? carbonParseDateFormatYMD($request->post('ownerOfferEndDate')) : null;
            $rentalUserDtoArr['ownerOfferProposedTotal'] = !empty($request->post('ownerOfferProposedTotal')) ? (float)$request->post('ownerOfferProposedTotal') : null;
            $rentalUserDtoArr['tripperOfferProposedTotal'] = !empty($request->post('tripperOfferProposedTotal')) ? (float)$request->post('tripperOfferProposedTotal') : null;
            $rentalUserDtoArr['numberOfGuests'] = !empty($request->post('numberOfGuests')) ? (int)$request->post('numberOfGuests') : null;

            $rentalUserDtoArr['flexibleDates'] = false;
            if(!empty($request->post('flexibleDates'))) {
                if(
                    $request->post('flexibleDates') == '1' ||
                    $request->post('flexibleDates') == 1 ||
                    $request->post('flexibleDates') == 'true' ||
                    $request->post('flexibleDates') == 'true'
                )
                {
                    $rentalUserDtoArr['flexibleDates'] = true;
                }
            }

            $rentalUserDtoArr['finalAcceptedNightlyPrice'] = !empty($request->post('finalAcceptedNightlyPrice')) ? (float)$request->post('finalAcceptedNightlyPrice') : null;

            return new self($rentalUserDtoArr);
     }

}
