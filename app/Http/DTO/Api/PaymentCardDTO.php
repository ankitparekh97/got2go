<?php


namespace App\Http\DTO\Api;

use App\Services\PaymentServices\Stripe\Card\CardRequestDTO;
use App\Services\PaymentServices\Stripe\Token\TokenRequestDTO;
use App\Services\PaymentServices\Stripe\Customer\CustomerRequestDTO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\DataTransferObject;

class PaymentCardDTO extends DataTransferObject
{

    public $id;

     /**
     * @var string|null $id
     */
    public $userId;

    /**
     * @var string|null $userType
     */
    public $userType;

    /**
     * @var string|null $nameOnCard
     */
    public $nameOnCard;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $cardType
     */
    public $cardType;

    /**
     * @var string|null $lastFour
     */
    public $lastFour;

     /**
     * @var string|null $cvv
     */
    public $cvv;

    /**
     * @var string|null $expirationDate
     */
    public $expirationDate;

    /**
     * @var string|null $debit
     */
    public $debit;

    /**
     * @var bool|null $default
     */
    public $default;

    /**
     * @var bool|null $imageUrl
     */
    public $imageUrl;


    public $maskedNumber;

    /**
     * @var string|null $token
     */
    public $token;

    /**
     * @var bool|null $uniqueIdentifier
     */
    public $uniqueIdentifier;

    /**
     * @var string|null $address
     */
    public $address;

    /**
     * @var string|null $address2
     */
    public $address2;

    /**
     * @var string|null $state
     */
    public $state;

    /**
     * @var string|null $city
     */
    public $city;

    /**
     * @var string|null $billingPhone
     */
    public $billingPhone;

    /**
     * @var string|null
     */
    public $cardToken;

     /**
     * @var string|null
     */
    public $fingerPrint;

    public function validate($data){

        $validate = Validator::make($data, [
                                'name_on_card' => 'required|regex:/^[a-zA-Z]+$/u|min:3',
                                'last_four' => 'required|max:19',
                                'cvv' => 'required|min:3|max:4',
                                'expiration_date' => 'required',
                            ],[
                                'name_on_card.required' => 'Card Holder Name is required',
                                'last_four.required' => 'Credit Card Number is required',
                                'cvv.required' => 'Please enter in a valid security code',
                                'expiration_date.required' => 'Please enter in a valid expiration date',
                            ]);

        if ($validate->fails()){
            return $validate->errors()->all();
        }

        return true;
    }

    public static function formRequest(Request $request)
    {
        $default = false;
        if($request->post('is_default_card') == 'on'){
            $default = true;
        }

        $card_No = explode(' ',$request->last_four);
        if(isset($request->card_type) && $request->card_type!= ''){

            if($request->card_type == 'American Express'){
                $last = substr($card_No[2], -1);
                $cardNo = $last.$card_No[3];
            } else if($request->card_type == 'Diners Club'){
                $last = substr($card_No[2], -2);
                $cardNo = $last.$card_No[3];
            } else {
                $cardNo = $card_No[3];
            }

            $maskedNo = $card_No[0] . $card_No[1] . '******' . $cardNo;

        } else {
            $maskedNo = null;
        }

        $paymentCardtoArr = array();
        $paymentCardtoArr['id'] = !empty($request->post('id')) ? $request->post('id') : null;
        $paymentCardtoArr['nameOnCard'] = $request->post('name_on_card');
        $paymentCardtoArr['lastFour'] = !empty($request->post('last_four')) ? $request->post('last_four') : null;
        $paymentCardtoArr['cvv'] = $request->post('cvv');
        $paymentCardtoArr['expirationDate'] = $request->post('expiration_date');
        $paymentCardtoArr['address'] = !empty($request->post('address')) ? $request->post('address') : null;
        $paymentCardtoArr['address2'] = !empty($request->post('address2')) ? $request->post('address2') : null;
        $paymentCardtoArr['state'] = $request->post('state');
        $paymentCardtoArr['city'] = $request->post('city');
        $paymentCardtoArr['billingPhone'] = $request->post('billingPhone');
        $paymentCardtoArr['default'] = $default;
        $paymentCardtoArr['maskedNumber'] = $maskedNo;
        $paymentCardtoArr['token'] = !empty($request->post('stripeToken')) ? (int)$request->post('stripeToken') : null;
        return new self($paymentCardtoArr);
    }

    public function getTokenRequestDTO()
    {
        $expMonth = explode("/", $this->expirationDate, 2)[0];
        $expYear = explode("/", $this->expirationDate, 2)[1];

        // get token
        $tokenRequestDTO = new TokenRequestDTO();

        $tokenRequestDTO->number = $this->lastFour;
        $tokenRequestDTO->expMonth = (int)$expMonth;
        $tokenRequestDTO->expYear = (int)$expYear;
        $tokenRequestDTO->cvv = $this->cvv;
        $tokenRequestDTO->name = $this->nameOnCard;
        $tokenRequestDTO->addressLine1 = $this->address;
        $tokenRequestDTO->addressLine2 = $this->address2;
        $tokenRequestDTO->addressCity = $this->city;
        $tokenRequestDTO->addressState = $this->state;

        return $tokenRequestDTO;
    }

    public function getCustomerRequestDTO($token,$user)
    {
        $customerRequestDTO = new CustomerRequestDTO();

        $customerRequestDTO->number = $this->nameOnCard;
        $customerRequestDTO->email = $user->email;
        $customerRequestDTO->phone = $this->billingPhone;
        $customerRequestDTO->source = $token['id'];

        return $customerRequestDTO;
    }

}
