<?php


namespace App\Http\DTO\Api;


use Spatie\DataTransferObject\DataTransferObject;

class RentalUserSubscriptionDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
     public $id;

    /**
     * @var int|null $rentalId
     */
     public $rentalId;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodStartDate
     */
     public $billingPeriodStartDate;

    /**
     * @var \Illuminate\Support\Carbon|null $billingPeriodEndDate
     */
     public $billingPeriodEndDate;

    /**
     * @var \Illuminate\Support\Carbon|null $firstBillingDate
     */
     public $firstBillingDate;

    /**
     * @var int|null $currentBillingCycle
     */
     public $currentBillingCycle;

    /**
     * @var string|null $description
     */
     public $description;

    /**
     * @var string|null $subscriptionId
     */
     public $subscriptionId;

    /**
     * @var \Illuminate\Support\Carbon|null $nextBillingDate
     */
     public $nextBillingDate;

    /**
     * @var string|null $nextBillingAmount
     */
     public $nextBillingAmount;

    /**
     * @var \Illuminate\Support\Carbon|null $paidThroughDate
     */
     public $paidThroughDate;

    /**
     * @var string|null $planId
     */
     public $planId;

    /**
     * @var string|null $price
     */
     public $price;

    /**
     * @var string|null $status
     */
     public $status;

    /**
     * @var string|null $paidThrough
     */
     public $paidThrough;

     public function convertPostDataToObject($request)
     {
            $rentalUserDtoArr = array();
            $rentalUserDtoArr['id'] = $request->post('id');
            $rentalUserDtoArr['rentalId'] = $request->post('rentalId');
            $rentalUserDtoArr['billingPeriodStartDate'] = $request->post('billingPeriodStartDate');
            $rentalUserDtoArr['billingPeriodEndDate'] = $request->post('billingPeriodEndDate');
            $rentalUserDtoArr['firstBillingDate'] = $request->post('firstBillingDate');
            $rentalUserDtoArr['currentBillingCycle'] = $request->post('currentBillingCycle');
            $rentalUserDtoArr['description'] = $request->post('description');
            $rentalUserDtoArr['subscriptionId'] = $request->post('subscriptionId');
            $rentalUserDtoArr['nextBillingDate'] = $request->post('nextBillingDate');
            $rentalUserDtoArr['nextBillingAmount'] = $request->post('nextBillingAmount');
            $rentalUserDtoArr['paidThroughDate'] = $request->post('paidThroughDate');
            $rentalUserDtoArr['planId'] = $request->post('planId');
            $rentalUserDtoArr['price'] = $request->post('price');
            $rentalUserDtoArr['status'] = $request->post('status');
            $rentalUserDtoArr['paidThrough'] = $request->post('paid_through');

            return new self($rentalUserDtoArr);
     }

}
