<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\DataTransferObject\DataTransferObject;

class HotelsDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var string|null $email
     */
    public $email;

    /**
     * @var string|null $password
     */
    public $password;

    /**
     * @var string|null $location
     */
    public $location;

     /**
     * @var string|null $city
     */
    public $city;

     /**
     * @var string|null $state
     */
    public $state;


    public static function convertToObject($request)
    {
        $hotelstoArr = array();
        $hotelstoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $hotelstoArr['name'] = !empty($request->txt_resort) ? $request->txt_resort : null;
        $hotelstoArr['email'] = 'testemail@test.test';
        $hotelstoArr['password'] = Hash::make('123456');
        $hotelstoArr['location'] = $request->where_is_listing;
        $hotelstoArr['city'] = $request->city;
        $hotelstoArr['state'] =  $request->state;

        return new self($hotelstoArr);
     }

}
