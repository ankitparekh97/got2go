<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\DataTransferObject\DataTransferObject;

class BulkUploadDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $ownerId
     */
    public $ownerId;

    /**
     * @var string|null $propertyDoc
     */
    public $propertyDoc;

    /**
     * @var string|null $propertyMedia
     */
    public $propertyMedia;

    /**
     * @var string|null $propertyVerification
     */
    public $propertyVerification;

     /**
     * @var string|null $propertyGovtId
     */
    public $propertyGovtId;

     /**
     * @var string|null $status
     */
    public $status;

     /**
     * @var string|null $uniqueKey
     */
    public $uniqueKey;

     /**
     * @var int|null $isReupload
     */
    public $isReupload;

     /**
     * @var int|null $queue_id
     */
    public $queue_id;

     /**
     * @var object|null $propertyData
     */
    public $propertyData;

     /**
     * @var object|null $propertyImages
     */
    public $propertyImages;

     /**
     * @var object|null $propertyVerficationDocs
     */
    public $propertyVerficationDocs;

    /**
     * @var object|null $propertyGovt
     */
    public $propertyGovt;


    public static function convertToObject($request)
     {
        $uniqueId = ($request->unique_key == '') ? uniqid() : $request->unique_key;

        $bulkUploadtoArr = array();
        $bulkUploadtoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $bulkUploadtoArr['ownerId'] = getOwnerId();
        $bulkUploadtoArr['propertyDoc'] = $request->UploadedCsv;
        $bulkUploadtoArr['propertyMedia'] = $request->UploadedImages;
        $bulkUploadtoArr['propertyVerification'] = $request->UploadedVerificationDocs;
        $bulkUploadtoArr['propertyGovtId'] = $request->UploadedGovtId;
        $bulkUploadtoArr['status'] = 'pending';
        $bulkUploadtoArr['uniqueKey'] = $uniqueId;
        $bulkUploadtoArr['isReupload'] = (int)$request->is_reupload;
        $bulkUploadtoArr['queue_id'] = 0;

        return new self($bulkUploadtoArr);
     }

     public static function formRequest($request)
     {
        $bulkUploadtoArr = array();
        $bulkUploadtoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $bulkUploadtoArr['ownerId'] = getOwnerId();
        $bulkUploadtoArr['propertyData'] = !empty($request->propertyData) ? $request->propertyData : null;
        $bulkUploadtoArr['propertyImages'] = !empty($request->propertyImages) ? $request->propertyImages : null;
        $bulkUploadtoArr['propertyVerficationDocs'] = !empty($request->propertyVerficationDocs) ? $request->propertyVerficationDocs : null;
        $bulkUploadtoArr['propertyGovt'] = !empty($request->propertyGovtId) ? $request->propertyGovtId : null;
        $bulkUploadtoArr['status'] = 'pending';
        $bulkUploadtoArr['uniqueKey'] = !empty($request->unique_key) ? $request->unique_key : null;
        $bulkUploadtoArr['isReupload'] = (int)$request->is_reupload;
        $bulkUploadtoArr['queue_id'] = 0;

        return new self($bulkUploadtoArr);
     }
}
