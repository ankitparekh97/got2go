<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;
use Validator;

class PropertyMediaDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $propertyId
     */
    public $property_id;

    /**
     * @var array|null $photo
     */
    public $photo;

    /**
     * @var int|null $originalName
     */
    public $originalName;

    /**
     * @var string|null $mediaType
     */
    public $mediaType;

    /**
     * @var string|null $photosDeleted
     */
    public $photosDeleted;


    public static function rules($request){

        if($request->file('photos')){
            $validate = Validator::make($request->file('photos'),[
                'photos' => 'required',
                'photos.*' => 'mimes:jpeg,jpg,png'
            ]);

            if ($validate->fails()){
                return $validate->errors()->all();
            }
        }

        return true;
    }

    public static function convertToObject($request)
    {
        $propertyMediaDtoArr = array();
        $propertyMediaDtoArr['id'] = !empty($request->post('id')) ? (int)$request->post('id') : null;
        $propertyMediaDtoArr['photo'] = $request->post('photos');
        $propertyMediaDtoArr['mediaType'] = $request->post('photos_filetype');
        $propertyMediaDtoArr['photosDeleted'] = !empty($request->post('photos_deleted')) ? $request->post('photos_deleted') : null;

        return new self($propertyMediaDtoArr);
     }

}
