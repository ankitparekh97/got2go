<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\DataTransferObject\DataTransferObject;

class AmenitiesDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

     /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var array|null $amenityId
     */
    public $amenityId;

    /**
     * @var string|null $propertyType
     */
    public $propertyType;

    /**
     * @var string|null $amenitiesType
     */
    public $amenitiesType;


    public static function convertToObject($request)
    {
        $amenitytoArr = array();
        $amenitytoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $amenitytoArr['amenityId'] = !empty($request->amenity_id) ? $request->amenity_id : null;
        $amenitytoArr['propertyType'] = ($request->get('type_of_property') ? 'property' : 'timeshare');
        $amenitytoArr['amenitiesType'] = ($request->get('type_of_property') ? 'unit' : 'resort');

        return new self($amenitytoArr);
     }

}
