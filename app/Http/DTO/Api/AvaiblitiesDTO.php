<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\DataTransferObject\DataTransferObject;

class AvaiblitiesDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

     /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var array|null $availableFrom
     */
    public $availableFrom;

    /**
     * @var array|null $availableTo
     */
    public $availableTo;

    /**
     * @var int|null $isPartialBookingAllowed
     */
    public $isPartialBookingAllowed;

     /**
     * @var array|null $minimumNight
     */
    public $minimumNight;

    public static function convertToObject($request)
    {
        $avaiblitiestoArr = array();
        $avaiblitiestoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $avaiblitiestoArr['availableFrom'] = !empty($request->property_available_when) ? $request->property_available_when : null;
        $avaiblitiestoArr['availableTo'] = !empty($request->property_available_when_to) ? $request->property_available_when_to : null;
        $avaiblitiestoArr['minimumNight'] = !empty($request->min_night) ? $request->min_night : null;

        return new self($avaiblitiestoArr);
     }

}
