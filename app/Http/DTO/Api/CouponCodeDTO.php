<?php


namespace App\Http\DTO\Api;


use App\Models\CouponCode;
use Spatie\DataTransferObject\DataTransferObject;

class CouponCodeDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var string|null $name
     */
    public $name;

    /**
     * @var float|null $discountPercentage
     */
    public $discountPercentage;

    /**
     * @var bool|null $isSingleUse
     */
    public $isSingleUse;

    /**
     * @var bool|null $status
     */
    public $status;

    public static function convertToObject($request)
    {
        $couponCodesArr = array();
        $couponCodesArr['id'] = !empty($request->post('id')) ? (int)$request->post('id') : null;
        $couponCodesArr['name'] = !empty($request->post('name')) ? $request->post('name') : null;
        $couponCodesArr['discountPercentage'] = !empty($request->post('discountPercentage')) ? (float)$request->post('discountPercentage') : null;

        $couponCodesArr['status'] = false;
        if(!empty($request->post('status'))){
            if(
                $request->post('status') === '1' ||
                $request->post('status') === 1 ||
                $request->post('status') === 'true' ||
                $request->post('status') === true
            )
            {
                $couponCodesArr['status'] = true;
            }
        }

        $couponCodesArr['isSingleUse'] = false;
        if(!empty($request->post('isSingleUse'))){
            if(
                $request->post('isSingleUse') === '1' ||
                $request->post('isSingleUse') === 1 ||
                $request->post('isSingleUse') === 'true' ||
                $request->post('isSingleUse') === true
            )
            {
                $couponCodesArr['isSingleUse'] = true;
            }
        }

        return new self($couponCodesArr);
    }

    public static function convertModelToObject(CouponCode  $couponCode)
    {
        $couponCodesArr = array();
        $couponCodesArr['id'] = $couponCode->id;
        $couponCodesArr['name'] = $couponCode->name;
        $couponCodesArr['discountPercentage'] = $couponCode->discount_percentage;
        $couponCodesArr['status'] = false;
        if(!empty($couponCode->status)){
            if(
                $couponCode->status == '1' ||
                $couponCode->status == 1 ||
                $couponCode->status == 'true' ||
                $couponCode->status == true
            )
            {
                $couponCodesArr['status'] = true;
            }
        }

        $couponCodesArr['isSingleUse'] = $couponCode->is_single_use;
        if(!empty($couponCode->is_single_use)){
            if(
                $couponCode->is_single_use == '1' ||
                $couponCode->is_single_use == 1 ||
                $couponCode->is_single_use == 'true' ||
                $couponCode->is_single_use == true
            )
            {
                $couponCodesArr['isSingleUse'] = true;
            }
        }

        return new self($couponCodesArr);
    }

}
