<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Validator;
use Spatie\DataTransferObject\DataTransferObject;

class PropertyVerificationDTO extends DataTransferObject
{

    /**
     * @var int|null $id
     */
    public $id;

     /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var array|null $documentName
     */
    public $documentName;

    /**
     * @var string|null $proofOfOwnershipFiletype
     */
    public $proofOfOwnershipFiletype;

     /**
     * @var string|null $isGovtId
     */
    public $isGovtId;

     /**
     * @var string|null $proofOfOwnershipDeleted
     */
    public $proofOfOwnershipDeleted;


    public static function rules($request){

        if($request->file('proof_of_ownership')){
            $validate = Validator::make($request->file('proof_of_ownership'),[
                'proof_of_ownership' => 'required',
                'proof_of_ownership.*' => 'mimes:jpeg,jpg,png,pdf'
            ]);

            if ($validate->fails()){
                return $validate->errors()->all();
            }
        }

        if($request->file('gov_id')){
            $validate = Validator::make($request->file('gov_id'),[
                'gov_id' => 'required',
                'gov_id.*' => 'mimes:jpeg,jpg,png,pdf'
            ]);

            if ($validate->fails()){
                return $validate->errors()->all();
            }
        }

        return true;
    }

    public static function convertToObject($request)
    {
        $avaiblitiestoArr = array();
        $avaiblitiestoArr['id'] = !empty($request->id) ? (int)$request->id : null;
        $avaiblitiestoArr['documentName'] = !empty($request->proof_of_ownership) ? $request->proof_of_ownership : null;
        $avaiblitiestoArr['proofOfOwnershipFiletype'] = !empty($request->proof_of_ownership_filetype) ? $request->proof_of_ownership_filetype : null;
        $avaiblitiestoArr['proofOfOwnershipDeleted'] = !empty($request->proof_of_ownership_deleted) ? $request->proof_of_ownership_deleted : null;

        return new self($avaiblitiestoArr);
     }

}
