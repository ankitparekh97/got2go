<?php


namespace App\Http\DTO\Api;


use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class OwnerUserDTO extends DataTransferObject
{

    /**
     * @var string|null $id
     */
    public $id;

     /**
     * @var string|null
     */
    public $customerId;

    /**
     * @var \App\Http\DTO\Api\PaymentCardDTO|null
     */
    public $paymentCard;

    /**
     * @var string|null
     */
    public $paymentMethodNonce;

    /**
     * @var string|null
     */
    public $braintreeCustomerId;

     /**
     * @var int|null $stripeAcountId
     */
    public $stripeAcountId;

    public static function formRequest(Request $request)
    {
        return new self([
            'id' => !empty($request->post('id')) ? (int)$request->post('id') : null,
            'phoneNumber' => $request->post('phoneNumber'),
            'birthdate' => carbonParseDate($request->post('birthdate')),
            'braintreeCustomerId' => $request->post('braintreeCustomerId'),
            'paymentCard' => [
                'address' => $request->post('address'),
                'address2' => $request->post('address2'),
                'city' => $request->post('city'),
                'state' => $request->post('state'),
                'billingPhone' => $request->post('billingPhone'),
                'paymentMethodNonce' => $request->post('payloadNonce')
            ]
        ]);
    }
}
