<?php


namespace App\Http\DTO\Api;


use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class ReadNotificationDTO extends DataTransferObject
{
    /**
     * @var string|null $type
     */
    public $type;

    public static function convertToObj(Request $request)
    {
        $readNotificationDTOArr = [
            'type' => $request->post('type')
        ];

        return new self($readNotificationDTOArr);
    }
}
