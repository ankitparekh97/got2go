<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\DataTransferObject;

class PaymentHistoryDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
    public $id;

     /**
     * @var int|null $id
     */
    public $userId;

    /**
     * @var string|null $userType
     */
    public $userType;

    /**
     * @var string|null $nameOnCard
     */
    public $firstName;

    /**
     * @var int|null $routingNumber
     */
    public $routingNumber;

    /**
     * @var string|null $institutionName
     */
    public $institutionName;

    /**
     * @var string|null $accountNumber
     */
    public $accountNumber;

    /**
     * @var string|null $ssn
     */
    public $ssn;

     /**
     * @var bool|null $default
     */
    public $default;

    /**
     * @var string|null $state
     */
    public $state;

    /**
     * @var string|null $city
     */
    public $city;

     /**
     * @var string|null $zipcode
     */
    public $zipcode;

    /**
     * @var int|null $stripeAcountId
     */
    public $stripeAcountId;

    /**
     * @var int|null
     */
    public $fingerPrint;

    public function validate($data){

        $validate = Validator::make($data, [
                                'accountHolderName' => 'required|min:3',
                                'routingNumber' => 'required',
                                'accountHolderType' => 'required',
                                'accountNumber' => 'required|min:8|max:16',
                            ],[
                                'accountHolderName.required' => 'Please enter account holder name',
                                'routingNumber.required' => 'Please enter in a valid routing number',
                                'accountNumber.required' => 'Please enter in a valid account number',
                                'accountNumber.min' => 'Please enter valid account number',
                                'accountNumber.max' => 'Please enter valid account number',
                            ]);

        if ($validate->fails()){
            return $validate->errors()->all();
        }

        return true;
    }


    public static function convertPostDataToObject($request)
     {
        $default = false;
        if($request->post('is_default_card') == 'on'){
            $default = true;
        }

        $paymentHistoryDTOArr = array();
        $paymentHistoryDTOArr['id'] = !empty($request->post('id')) ? (int)$request->post('id') : null;
        $paymentHistoryDTOArr['firstName'] = $request->post('accountHolderName');
        $paymentHistoryDTOArr['routingNumber'] = !empty($request->post('routingNumber')) ? (int)$request->post('routingNumber') : null;
        $paymentHistoryDTOArr['institutionName'] = $request->post('accountHolderType');
        $paymentHistoryDTOArr['accountNumber'] = $request->post('accountNumber');
        $paymentHistoryDTOArr['ssn'] = '0000';
        $paymentHistoryDTOArr['default'] = $default;
        $paymentHistoryDTOArr['city'] = 'Chicago';
        $paymentHistoryDTOArr['state'] = 'IL';
        $paymentHistoryDTOArr['zipcode'] = '07950';

        return new self($paymentHistoryDTOArr);
     }
}
