<?php


namespace App\Http\DTO\Api;


use Spatie\DataTransferObject\DataTransferObject;

class SubscriptionPlanDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
    public $id;


    /**
     * @var string|null $name
     */
    public $name;


    /**
     * @var int|null $amount
     */
    public $amount;


    /**
     * @var string|null $currency
     */
    public $currency;


    /**
     * @var string|null $interval
     */
    public $interval;


    /**
     * @var string|null $statementDescriptor
     */
    public $statementDescriptor;

}
