<?php


namespace App\Http\DTO\Api;


use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class RentalUserDTO extends DataTransferObject
{

    /**
     * @var string|null
     */
    public $firstName;

    /**
     * @var string|null
     */
    public $lastName;

    /**
     * @var string|null
     */
    public $fullName;

    /**
     * @var string|null
     */
    public $googleId;

    /**
     * @var string|null
     */
    public $facebookId;

    /**
     * @var string|null
     */
    public $email;

    /**
     * @var string|null
     */
    public $bio;

    /**
     * @var string|null
     */
    public $isOtpVerified;

    /**
     * @var \Illuminate\Support\Carbon|null
     */
    public $birthdate;

    /**
     * @var string|null
     */
    public $gender;

    /**
     * @var string|null
     */
    public $phoneNumber;

    /**
     * @var string|null
     */
    public $emergencyNumber;

    /**
     * @var string|null
     */
    public $address;

    /**
     * @var string|null
     */
    public $address2;

    /**
     * @var string|null
     */
    public $city;

    /**
     * @var string|null
     */
    public $state;

    /**
     * @var string|null
     */
    public $zip;

    /**
     * @var string|null
     */
    public $siteLanguage;

    /**
     * @var string|null
     */
    public $currency;

    /**
     * @var string|null
     */
    public $status;

    /**
     * @var string|null
     */
    public $photo;

    /**
     * @var string|null
     */
    public $governmentIdDoc;

    /**
     * @var boolean|null
     */
    public $shareOnFb;

    /**
     * @var string|null
     */
    public $marketingEmail;

    /**
     * @var boolean|null
     */
    public $isVerified;

    /**
     * @var boolean|null
     */
    public $isTripper;

    /**
     * options are active and paused
     * @var string|null
     */
    public $tripperStatus;

    /**
     * @var boolean|null
     */
    public $isSubscribed;

    /**
     * @var \App\Http\DTO\Api\PaymentCardDTO|null
     */
    public $paymentCard;

    /**
     * @var string|null
     */
    public $token;

    /**
     * @var string|null
     */
    public $braintreeCustomerId;

    /**
     * @var string|null
     */
    public $customerId;

    public static function formRequest(Request $request)
    {
        return new self([
            'phoneNumber' => $request->post('phoneNumber'),
            'birthdate' => carbonParseDate($request->post('birthdate')),
            'braintreeCustomerId' => $request->post('braintreeCustomerId'),
            'email' => $request->post('email'),
            'firstName'=>$request->post('first_name'),
            'lastName'=>$request->post('last_name'),
            'paymentCard' => [
                //'lastFour' => $request->post('last_four'),
                //'expirationDate' => $request->post('expiration_date'),
                //'cvv' => $request->post('cvv'),
                'nameOnCard' => $request->post('nameOnCard'),
                'address' => $request->post('address'),
                'address2' => $request->post('address2'),
                'city' => $request->post('city'),
                'state' => $request->post('state'),
                'billingPhone' => $request->post('billingPhone'),
                'token' => $request->post('stripeToken')
            ]
        ]);
    }
}
