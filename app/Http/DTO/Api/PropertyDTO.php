<?php

namespace App\Http\DTO\Api;

use App\Helpers\GlobalConstantDeclaration;
use App\Services\PropertyServiceContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\DataTransferObject;

class PropertyDTO extends DataTransferObject
{
    /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $ownerId
     */
    public $ownerId;

    /**
     * @var int|null $resortId
     */
    public $resortId;

    /**
     * @var int|null $propertyTypeId
     */
    public $propertyTypeId;

    /**
     * @var int|null $subPropertyTypeId
     */
    public $subPropertyTypeId;

    /**
     * @var string|null $title
     */
    public $title;

    /**
     * @var string|null $description
     */
    public $description;

    /**
     * @var string|null $aboutTheSpace
     */
    public $aboutTheSpace;

    /**
     * @var string|null $locationDetail
     */
    public $locationDetail;

    /**
     * @var string|null $isForGuest
     */
    public $isForGuest;

    /**
     * @var int|null $guestWillHave
     */
    public $guestWillHave;

    /**
     * @var string|null $listAsCo
     */
    public $listAsCo;

    /**
     * @var string|null $city
     */
    public $city;

    /**
     * @var string|null $state
     */
    public $state;

    /**
     * @var string|null $country
     */
    public $country;

    /**
     * @var int|null $zipcode
     */
    public $zipcode;

    /**
     * @var int|null $noOfGuest
     */
    public $noOfGuest;

    /**
     * @var int|null $totalBedroom
     */
    public $totalBedroom;

    /**
     * @var int|null $totalBatroom
     */
    public $totalBatroom;

    /**
     * @var int|null $totalBeds
     */
    public $totalBeds;

    /**
     * @var string|null $latlang
     */
    public $latlang;

    /**
     * @var string|null $isInstantBookingAllowed
     */
    public $isInstantBookingAllowed;

    /**
     * @var string|null $price
     */
    public $price;

    /**
     * @var int|null $offerprice
     */
    public $offerPrice;

    /**
     * @var string|null $isPriceNegotiable
     */
    public $isPriceNegotiable;

    /**
     * @var int|null $isPriceNegotiable
     */
    public $isPartialPaymentAllowed;

    /**
     * @var double|null $partialPaymentAmt
     */
    public $partialPaymentAmt;

    /**
     * @var string|null $checkIn
     */
    public $checkIn;

    /**
     * @var string|null $checkOut
     */
    public $checkOut;

     /**
     * @var string|null $cancellationType
     */
    public $cancellationType;

    /**
     * @var int|null $publish
     */
    public $publish;

    /**
     * @var string|null $status
     */
    public $status;

    /**
     * @var string|null $typeOfProperty
     */
    public $typeOfProperty;

    /**
     * @var string|null $coverPhoto
     */
    public $coverPhoto;

    /**
     * @var float|null $cleaningFee
     */
    public $cleaningFee;

    /**
     * @var int|null $isHotDeals
     */
    public $isHotDeals;

    /**
     * @var int|null $extraBeds
     */
    public $extraBeds;

    /**
     * @var int|null $reasonDecline
     */
    public $reasonDecline;

        /**
     * @var int|null $lastTabId
     */
    public $lastTabId;

    /**
     * @var string|null $bulkUploadId
     */
    public $bulkUploadId;

     /**
     * @var string|null $bulkUploadUniquekey
     */
    public $bulkUploadUniquekey;


    public static function formRequest($request)
     {
            if(Session::get('timezone') == ''){
                Session::put('timezone',$request->timezone);
                Session::save();
            }

            $propertyTypeId = 0;
            if($request->has('private_residence_this') && $request->get('type_of_property') == 'property')
            {
                $propertyTypeId = (int)$request->get('private_residence_this');
            } else if($request->has('vacation_club_is_this')){
                $propertyTypeId = (int)$request->get('vacation_club_is_this');
            }

            if(!$propertyTypeId){
                $propertyTypeId = NULL;
            }

            if($request->has('what_kind_cabin') && $request->get('what_kind_cabin')!=''){
                $subPropertyTypeId = (int)$request->get('what_kind_cabin');
            } else {
                $subPropertyTypeId = 0;
            }

            $address = $request->get('where_is_listing');
            $lat = NULL;  $lang = NULL;
            if($address != ''){
                $geoCode = env("GEOCODE");
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&key=".$geoCode;
                $result_string = file_get_contents($url);
                $result_G = json_decode($result_string, true);
                if(!empty($result_G['results']) && $result_G['status'] != 'ZERO_RESULTS')
                {
                    $resultG1[]=$result_G['results'][0];
                    $lat = $resultG1[0]['geometry']['location']['lat'];
                    $lang = $resultG1[0]['geometry']['location']['lng'];
                }
            }


            $chckIn = ($request->get('guest_checkin_time') !='') ? $request->get('guest_checkin_time') . ' ' . $request->get('guest_checkin_time_format') : null;
            $chckOut = ($request->get('guest_checkout_time') !='') ? $request->get('guest_checkout_time') . ' ' . $request->get('guest_checkout_time_format') : null;

            $propertyDtoArr = array();
            $propertyDtoArr['id'] = !empty($request->post('property_id')) ? (int)$request->post('property_id') : null;
            $propertyDtoArr['ownerId'] = getOwnerId();
            $propertyDtoArr['propertyTypeId'] = $propertyTypeId;
            $propertyDtoArr['subPropertyTypeId'] = $subPropertyTypeId;
            $propertyDtoArr['title'] = $request->post('name_your_listing');
            $propertyDtoArr['description'] = $request->post('describe_listing');
            $propertyDtoArr['aboutTheSpace'] = !empty($request->post('about_space')) ? $request->post('about_space') : null;
            $propertyDtoArr['locationDetail'] = !empty($request->post('where_is_listing')) ? $request->post('where_is_listing') : null;
            $propertyDtoArr['isForGuest'] = !empty($request->post('is_guest_only')) ? $request->post('is_guest_only') : '0';
            $propertyDtoArr['guestWillHave'] = !empty($request->post('guest_have')) ? (int)$request->post('guest_have') : 0;
            $propertyDtoArr['listAsCo'] = !empty($request->post('listing')) ? $request->post('listing') : '0';
            $propertyDtoArr['city'] = !empty($request->post('city')) ? $request->post('city') : null;
            $propertyDtoArr['state'] = !empty($request->post('state')) ? $request->post('state') : null;
            $propertyDtoArr['country'] = !empty($request->post('country')) ? $request->post('country') : null;
            $propertyDtoArr['zipcode'] = !empty($request->post('zipcode')) ? (int)$request->post('zipcode') : null;
            $propertyDtoArr['noOfGuest'] = !empty($request->post('how-many-guests-allowed')) ? (int)$request->post('how-many-guests-allowed') : 0;
            $propertyDtoArr['totalBedroom'] = !empty($request->post('how-many-bedrooms-there')) ? (int)$request->post('how-many-bedrooms-there') : 0;
            $propertyDtoArr['totalBatroom'] = !empty($request->post('how_many_bathrooms_available')) ? (int)$request->post('how_many_bathrooms_available') : 0;
            $propertyDtoArr['totalBeds'] = !empty($request->post('how_many_beds_there')) ? (int)$request->post('how_many_beds_there') : 0;
            $propertyDtoArr['latlang'] = ($lat != '' && $lang != '') ? $lat."#".$lang : NULL;
            $propertyDtoArr['isInstantBookingAllowed'] = !empty($request->post('is_instant_booking')) ? $request->post('is_instant_booking') :'0';
            $propertyDtoArr['price'] = !empty($request->post('price')) ? $request->post('price') : '0';
            $propertyDtoArr['offerPrice'] = $request->get('is_negotiable') == '1' ? (int)$request->get('offer_price') : 0;
            $propertyDtoArr['isPriceNegotiable'] = !empty($request->post('is_negotiable')) ? $request->post('is_negotiable') : '0';
            $propertyDtoArr['isPartialPaymentAllowed'] = 0;
            $propertyDtoArr['partialPaymentAmt'] = !empty($request->post('price')) ? (double)$request->post('price') : 0.0;
            $propertyDtoArr['checkIn'] = $chckIn;
            $propertyDtoArr['checkOut'] = $chckOut;
            $propertyDtoArr['cancellationType'] = !empty($request->post('cancellation_policy')) ? $request->post('cancellation_policy') : null;
            $propertyDtoArr['publish'] = !empty($request->post('publish')) ? (int)$request->post('publish') : 0;
            //$propertyDtoArr['status'] = GlobalConstantDeclaration::PROPERTY_STATUS_DRAFT;
            $propertyDtoArr['typeOfProperty'] = !empty($request->post('type_of_property')) ? $request->post('type_of_property') : null;
            $propertyDtoArr['isHotDeals'] = 0;
            $propertyDtoArr['cleaningFee'] = !empty($request->post('cleaning_fee')) ? (float)$request->post('cleaning_fee') : 0.0;
            $propertyDtoArr['extraBeds'] = !empty($request->post('how_many_beds_there')) ? (int)$request->post('how_many_beds_there') : 0;
            $propertyDtoArr['lastTabId'] = ($request->get('last_tab_id') == '') ? 0 : (int)$request->get('last_tab_id');

            return new self($propertyDtoArr);
     }

}
