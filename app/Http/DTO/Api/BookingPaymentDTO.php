<?php

namespace App\Http\DTO\Api;

use App\Helpers\GlobalConstantDeclaration;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\DataTransferObject\DataTransferObject;

class BookingPaymentDTO extends DataTransferObject
{

     /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var int|null $bookingId
     */
    public $bookingId;

    /**
     * @var int|null $renterUserId
     */
    public $renterUserId;

    /**
     * @var string|null $paymentType
     */
    public $paymentType;

     /**
     * @var int|null $transactionNumber
     */
    public $transactionNumber;

     /**
     * @var float|null $amount
     */
    public $amount;

     /**
     * @var int|null $refundId
     */
    public $refundId;

     /**
     * @var int|null $refundAmount
     */
    public $refundAmount;

     /**
     * @var date|null $createdAt
     */
    public $createdAt;

     /**
     * @var date|null $updatedAt
     */
    public $updatedAt;


    // public static function convertToObject($bookingDTO,$bookings,$charge)
    // {
    //     $bookingPaymentArr = array();
    //     $bookingPaymentArr['bookingId'] = $bookingDTO->id;
    //     $bookingPaymentArr['renterUserId'] = $bookings->rental_user_id;
    //     $bookingPaymentArr['paymentType'] = GlobalConstantDeclaration::BOOKING_PAYMENT_COMPLETED;
    //     $bookingPaymentArr['transactionNumber'] = $charge['id'];
    //     $bookingPaymentArr['amount'] = $bookings->total;
    //     $bookingPaymentArr['refundId'] = uniqid();
    //     $bookingPaymentArr['refundAmount'] = 0;

    //     return new self($bookingPaymentArr);
    //  }

}
