<?php

namespace App\Http\DTO\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\DataTransferObject\DataTransferObject;

class BookingDTO extends DataTransferObject
{

     /**
     * @var int|null $id
     */
    public $id;

    /**
     * @var string|null $bookingType
     */
    public $bookingType;

    /**
     * @var int|null $renterUserId
     */
    public $renterUserId;

    /**
     * @var int|null $propertyId
     */
    public $propertyId;

    /**
     * @var string|null $checkInDate
     */
    public $checkInDate;

     /**
     * @var string|null $checkOutDate
     */
    public $checkOutDate;

     /**
     * @var string|null $status
     */
    public $status;

     /**
     * @var int|null $adults
     */
    public $adults;

     /**
     * @var int|null $childrens
     */
    public $childrens;

     /**
     * @var int|null $rooms
     */
    public $rooms;

     /**
     * @var float|null $actualPrice
     */
    public $actualPrice;

     /**
     * @var float|null $offerAmount
     */
    public $offerAmount;

     /**
     * @var float|null $tax
     */
    public $tax;

    /**
     * @var float|null $occupancyTax
     */
    public $occupancyTax;

    /**
     * @var float|null $total
     */
    public $total;

    /**
     * @var int|null $queue_id
     */
    public $isCancelled;

    /**
     * @var string|null $ownerStatus
     */
    public $ownerStatus;

    /**
     * @var int|null $confirmationId
     */
    public $confirmationId;

    /**
     * @var string|null $message
     */
    public $message;

    /**
     * @var string|null $cancelledDate
     */
    public $cancelledDate;

    /**
     * @var date|null $createdAt
     */
    public $createdAt;

    /**
     * @var date|null $updatedDate
     */
    public $updatedDate;


}
