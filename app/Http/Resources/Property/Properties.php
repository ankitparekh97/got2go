<?php

namespace App\Http\Resources\Property;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Properties extends ResourceCollection
{
    public $collects = Property::class;
}
