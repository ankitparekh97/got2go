<?php

namespace App\Http\Resources\Property;

use App\Http\Resources\PropertyImage\PropertyImage;
use Illuminate\Http\Resources\Json\JsonResource;

class Property extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var \App\Models\Property $property
         */
        $property = $this;

        return [
            'id' => (int)$property->id,
            'ownerId' => (int)$property->owner_id,
            'resortId' => (int)$property->resort_id,
            'propertyTypeId' => $property->property_type_id,
            'subPropertyTypeId' => $property->sub_property_type_id,
            'title' => $property->title,
            'description' => $property->description,
            'aboutTheSpace' => $property->about_the_space,
            'locationDetail' => $property->location_detail,
            'isForGuest' => $property->is_for_guest,
            'guestWillHave' => $property->guest_will_have,
            'listAsCompany' => $property->list_as_company,
            'city' => $property->city,
            'state' => $property->state,
            'stateAbbrv' => $property->state_abbrv,
            'country' => $property->country,
            'zipcode' => $property->zipcode,
            'noOfGuest' => $property->no_of_guest,
            'totalBedroom' => $property->total_bedroom,
            'totalBathroom' => $property->total_bathroom,
            'totalBeds' => $property->total_beds,
            'latLang' => $property->lat_lang,
            'isInstantBookingAllowed' => $property->is_instant_booking_allowed,
            'price' => $property->price,
            'offerPrice' => $property->offer_price,
            'tripperPrice' => getTripperPrice($property->price),
            'isPriceNegotiable' => $property->is_price_negotiable,
            'isPartialPaymentAllowed' => $property->is_partial_payment_allowed,
            'partialPaymentAmount' => $property->partial_payment_amount,
            'checkIn' => $property->check_in,
            'checkOut' => $property->check_out,
            'availableFrom' => $property->available_from,
            'availableTo' => $property->available_to,
            'unavailableFrom' => $property->unavailable_from,
            'unavailableTo' => $property->unavailable_to,
            'proofOfOwnershipOrLease' => $property->proof_of_ownership_or_lease,
            'govIssueId' => $property->gov_issue_id,
            'cancellationType' => $property->cancellation_type,
            'publish' => $property->publish,
            'status' => $property->status,
            'createdAt' => $property->created_at,
            'updatedAt' => $property->updated_at,
            'typeOfProperty' => $property->type_of_property,
            'coverPhoto' => $property->cover_photo,
            'isHotDeals' => $property->is_hotdeals,
            'video' => $property->video,
            'cleaningFee' => $property->cleaning_fee,
            'extraBeds' => $property->extra_beds,
            'lastTabId' => $property->last_tab_id,
            'images' => PropertyImage::collection($this->whenLoaded('propertyimages'))
        ];
    }
}
