<?php

namespace App\Http\Resources\CouponCode;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CouponCodesResource extends ResourceCollection
{
    public $collects = CouponCodeResource::class;
}
