<?php

namespace App\Http\Resources\CouponCode;

use App\Models\CouponCode;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var CouponCode $couponCode
         */
        $couponCode = $this;

        return [
            'id' => $couponCode->id,
            'name' => $couponCode->name,
            'status' => $couponCode->status,
            'discountPercentage' => $couponCode->discount_percentage,
            'isSingleUse' => $couponCode->is_single_use,
            'createdAt' => $couponCode->created_at,
        ];
    }
}
