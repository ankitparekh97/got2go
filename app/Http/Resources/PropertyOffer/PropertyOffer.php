<?php

namespace App\Http\Resources\PropertyOffer;

use App\Http\Resources\Booking\BookingResource;
use App\Http\Resources\Property\Property;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyOffer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var \App\Models\PropertyOffers $propertyOffer
         */
        $propertyOffer = $this;

        return [
            'id' => $propertyOffer->id,
            'propertyId' => (int)$propertyOffer->property_id,
            'ownerId' => (int)$propertyOffer->owner_id,
            'rentalId' => (int)$propertyOffer->rental_id,
            'actionBy' => $propertyOffer->action_by,
            'status' => $propertyOffer->status,
            'statusMessage' => getPropertyOfferStatusMessage(
                $propertyOffer->status,
                $propertyOffer->action_by
            ),
            'tripperOfferNightlyPrice' => (float)$propertyOffer->tripper_offer_nightly_price,
            'ownerOfferNightlyPrice' => (float)$propertyOffer->owner_offer_nightly_price,
            'tripperOfferStartDate' => !empty($propertyOffer->tripper_offer_end_date) ? carbonParseDate($propertyOffer->tripper_offer_start_date) : null,
            'tripperOfferEndDate' => !empty($propertyOffer->tripper_offer_end_date) ? carbonParseDate($propertyOffer->tripper_offer_end_date) : null,
            'ownerOfferStartDate' => !empty($propertyOffer->owner_offer_start_date) ? carbonParseDate($propertyOffer->owner_offer_start_date) : null,
            'ownerOfferEndDate' => !empty($propertyOffer->owner_offer_start_date) ? carbonParseDate($propertyOffer->owner_offer_end_date) : null,
            'tripperOfferProposedTotal' => (float)$propertyOffer->tripper_offer_proposed_total,
            'ownerOfferProposedTotal' => (float)$propertyOffer->owner_offer_proposed_total,
            'numberOfGuests' => (int)$propertyOffer->number_of_guests,
            'flexibleDates' => (bool)$propertyOffer->flexible_dates,
            'finalAcceptedNightlyPrice' => (float)$propertyOffer->final_accepted_nightly_price,
            'finalAcceptedStartDate' => carbonParseDate($propertyOffer->final_accepted_start_date),
            'finalAcceptedEndDate' => carbonParseDate($propertyOffer->final_accepted_end_date),
            'createdAt' => carbonParseDate($propertyOffer->created_at),
            'updatedAt' => carbonParseDate($propertyOffer->updated_at),
            'property' => new Property($this->whenLoaded('property')),
            'booking' => new BookingResource($this->whenLoaded('booking'))
        ];
    }
}
