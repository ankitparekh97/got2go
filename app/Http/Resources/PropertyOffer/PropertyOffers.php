<?php

namespace App\Http\Resources\PropertyOffer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyOffers extends ResourceCollection
{
    public $collects = PropertyOffer::class;
}
