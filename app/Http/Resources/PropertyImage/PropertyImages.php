<?php

namespace App\Http\Resources\PropertyImage;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyImages extends ResourceCollection
{
    public $collects = PropertyImage::class;
}
