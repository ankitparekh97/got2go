<?php

namespace App\Http\Resources\PropertyImage;

use Illuminate\Http\Resources\Json\JsonResource;

class PropertyImage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var \App\Models\PropertyImages $propertyImage
         */
        $propertyImage = $this;

        return [
            'id' => (int)$propertyImage->id,
            'propertyId' => (int)$propertyImage->property_id,
            'photo' => getPropertyImageLink($propertyImage->photo),
            'mediaType' => $propertyImage->media_type,
        ];
    }
}
