<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BookingsResource extends ResourceCollection
{
    public $collects = BookingResource::class;
}
