<?php

namespace App\Http\Resources\Booking;

use App\Models\Booking;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var Booking $booking
         */
        $booking = $this;

        return [
            'id' => $booking->id,
            'bookingType' => $booking->booking_type,
            'rentalUserId' => $booking->rental_user_id,
            'propertyId' => $booking->property_id,
            'checkInDate' => $booking->check_in_date,
            'checkOutDate' => $booking->check_out_date,
            'adults' => $booking->adults,
            'childrens' => $booking->childrens,
            'rooms' => $booking->rooms,
            'actualPrice' => $booking->actual_price,
            'offerAmount' => $booking->offer_amount,
            'tax' => $booking->tax,
            'occupancyTaxFees' => $booking->occupancy_tax_fees,
            'total' => $booking->total,
            'status' => $booking->status,
            'isCancelled' => $booking->is_cancelled,
            'cancellationDate' => $booking->cancellation_date,
            'ownerStatus' => $booking->owner_status,
            'propertyStatus' => $booking->property_status,
            'confirmationId' => $booking->confirmation_id,
            'message' => $booking->message,
            'propertyOffersId' => $booking->property_offers_id,
            'createdAt' => $booking->created_at,
        ];
    }
}
