<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Request;
class JwtHotelMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
       
        if ($request->session()->has('token')) {
            $request->headers->set('Authorization','Bearer '.$request->session()->get('token'));
            //
        }
       
        if ($guard == "hotel" && Auth::guard($guard)->check()) {
        try {
                $user = JWTAuth::parseToken()->authenticate();
                 
            } catch (Exception $e) {
               
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    return response()->json(['status' => 'Token is Invalid']);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                    return response()->json(['status' => 'Token is Expired']);
                }else{
                    return response()->json(['status' => 'Authorization Token not found']);
                }
            }
            
        return $next($request);
        }else{
            if(! Request::is('api*')){
               return redirect('login/hotel');
           }
            return response()->json(['status' => 'Authorization Token not found']);
        }
    }
}
