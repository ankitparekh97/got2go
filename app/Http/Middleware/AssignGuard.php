<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AssignGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       if ($guard == "admin" && Auth::guard($guard)->check()) {
                dd(1);
            }
            if ($guard == "owner" && Auth::guard($guard)->check()) {
                dd(2);
            }
            dd(3);
        if($guard != null)
            auth()->shouldUse($guard);
        return $next($request);
    }
}
