<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Auth;
use Illuminate\Support\Facades\Session;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Request;

class JwtRentalMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        if ($request->session()->has('rental_token')) {
            $request->headers->set('Authorization','Bearer '.$request->session()->get('rental_token'));
        }
        if ($request->session()->has('rental_token') && $guard == "rentaluser" && Auth::guard($guard)->check()) {

        try {
                $user = JWTAuth::parseToken()->authenticate();
            } catch (Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    return response()->json(['status' => 'Token is Invalid']);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                     // If the token is expired, then it will be refreshed and added to the headers
                    try
                    {
                    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                    $user = JWTAuth::setToken($refreshed)->toUser();
                    Session::put('rental_token', $refreshed);
                    $request->headers->set('Authorization','Bearer '.$refreshed);
                    }catch (JWTException $e){
                        return response()->json([
                            'code'   => 103,
                            'message' => 'Token cannot be refreshed, please Login again'
                        ]);
                    }
                }else{
                    return response()->json(['status' => 'Authorization Token not found']);
                }
            }

            $userDetails = \Auth::guard('rentaluser')->user();
            if(!Request::is('api*') && \Session::has('rental_token')){
                if(!$userDetails)
                {
                    Session::forget('rental_token');
                }
                 redirect(route('rental.home'));
            }
            return $next($request);
        }
        elseif(!$request->session()->has('rental_token')){
            return $next($request);
        }
        else{
            Session::forget('rental_token');
            if(! Request::is('api*')){
               return redirect('/');
           }
            return response()->json(['status' => 'Authorization Token not found']);
        }
    }
}
