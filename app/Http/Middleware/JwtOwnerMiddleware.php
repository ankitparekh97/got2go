<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use Exception;
use Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Request;

class JwtOwnerMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        //dd('hi');
        //dd($request->session()->get('owner_token'));
        if ($request->session()->has('owner_token')) {
            $request->headers->set('Authorization','Bearer '.$request->session()->get('owner_token'));
//            dd(Session::all());
        }

        //       dd(Auth::guard($guard)->check());
       if ($guard == "owner" && Auth::guard($guard)->check()) {

        try {
                $user = JWTAuth::parseToken()->authenticate();
//                    dd(\Auth::guard('owner')->user());

            } catch (Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    return response()->json(['status' => 'Token is Invalid']);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                     // If the token is expired, then it will be refreshed and added to the headers
                    try
                    {
                    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                    $user = JWTAuth::setToken($refreshed)->toUser();
                    Session::put('owner_token', $refreshed);
                    $request->headers->set('Authorization','Bearer '.$refreshed);
                    }catch (JWTException $e){
                        return response()->json([
                            'code'   => 103,
                            'message' => 'Token cannot be refreshed, please Login again'
                        ]);
                    }
                }else{
                    return response()->json(['status' => 'Authorization Token not found']);
                }
            }

           $userDetails = \Auth::guard('owner')->user();           
            
           if(! Request::is('api*') && $userDetails->is_otp_verified == '0'){
              return redirect(route('validateOtp',['id' => $userDetails->unique_key]));
           } elseif($userDetails->is_otp_verified == '0') {
              return response()->json(['status' => 'User not verified']);
           } elseif(!$userDetails) {
                Session::forget('owner_token');              
                redirect(route('owner.list-your-stay'));
            }

        return $next($request);
       }
      /*  elseif(!$request->session()->has('owner_token')){
        return $next($request);    
       } */else{
            
           Session::forget('owner_token');
           if(! Request::is('api*')){
               return redirect('/');
           }
            return response()->json(['status' => 'Authorization Token not found']);
        }
    }
}
