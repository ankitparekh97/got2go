<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Request;
class JwtAdminMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        if ($request->session()->has('admin_token')) {
            $request->headers->set('Authorization','Bearer '.$request->session()->get('admin_token'));
            //
        }
        if ($guard == "admin" && Auth::guard($guard)->check()) {
        try {
                $user = JWTAuth::parseToken()->authenticate();
            } catch (Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    return response()->json(['status' => 'Token is Invalid']);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                     // If the token is expired, then it will be refreshed and added to the headers
                    try
                    {
                    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                    $user = JWTAuth::setToken($refreshed)->toUser();
                    Session::put('admin_token', $refreshed);
                    $request->headers->set('Authorization','Bearer '.$refreshed);
                    }catch (JWTException $e){
                        return response()->json([
                            'code'   => 103,
                            'message' => 'Token cannot be refreshed, please Login again'
                        ]);
                    }
                }else{
                    return response()->json(['status' => 'Authorization Token not found']);
                }
            }
        return $next($request);
        } else {           
            $request->session()->forget('admin_token');
            $request->session()->forget('admin');
            if(! Request::is('api*')){
               return redirect('/admin');
           }
            return response()->json(['status' => 'Authorization Token not found']);
        }
    }
}
