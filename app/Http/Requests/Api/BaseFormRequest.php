<?php


namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as DefaultFormRequest;

abstract class BaseFormRequest extends DefaultFormRequest
{
    /*
 * Get the validation rules that apply to the request.
 * @return array
 */
    abstract public function rules();

    /*
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    abstract public function authorize();


    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json(
                [
                    'success'=> false,
                    'errors' => $errors
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
