<?php

namespace App\Http\Requests\Api\Tripper;

use App\Http\Requests\Api\BaseFormRequest;

class MakeOfferRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offeredNightlyPrice' => 'numeric|min:1',
        ];
    }

    public function messages()
    {
        return [
            'offeredNightlyPrice.numeric' => 'Please enter digits only',
            'offeredNightlyPrice.min' => 'More than 1',
        ];
    }
}
