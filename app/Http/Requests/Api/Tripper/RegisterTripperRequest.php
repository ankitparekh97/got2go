<?php

namespace App\Http\Requests\Api\Tripper;

use App\Http\Requests\Api\BaseFormRequest;

class RegisterTripperRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phoneNumber' => 'required',
            //'birthdate' => 'required',
            //'ccNumber' => 'required',
            //'expirationDate' => 'required',
            //'cvv' => 'required|max:4',
            //'nameOnCard' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phoneNumber.required' => 'Please enter in a phone number',
            //'last_four.required' => 'Credit Card Number is required',
            //'birthdate.required' => 'Please enter in a birthdate',
            //'cvv.required' => 'Please enter in a valid security code',
            //'name_on_card.required' => 'Please enter in a Card Name',
            'address.required' => 'Please enter in a address',
            'city.required' => 'Please enter in a city',
            'state.required' => 'Please enter in a state',
        ];
    }
}
