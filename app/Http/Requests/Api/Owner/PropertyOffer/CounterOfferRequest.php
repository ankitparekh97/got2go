<?php

namespace App\Http\Requests\Api\Owner\PropertyOffer;

use Illuminate\Foundation\Http\FormRequest;

class CounterOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rentalId' => ['required'],
            'ownerOfferStartDate' => ['required'],
            'ownerOfferEndDate' => ['required'],
            'ownerOfferProposedTotal' => ['required']
        ];
    }
}
