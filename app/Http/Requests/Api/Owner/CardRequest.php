<?php

namespace App\Http\Requests\Api\Owner;

use App\Http\Requests\Api\BaseFormRequest;

class CardRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'phoneNumber' => 'required',            
        ];
    }

    public function messages()
    {
        return [
            //'phoneNumber.required' => 'Please enter in a phone number'            
        ];
    }
}
