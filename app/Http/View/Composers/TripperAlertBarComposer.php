<?php


namespace App\Http\View\Composers;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\PropertyOffers;
use App\Notifications\PropertyOffer\Owner\OwnerAcceptedPropertyOfferNotification;
use App\Services\PropertyOfferService;
use App\Services\PropertyOfferServiceContract;
use Illuminate\View\View;

class TripperAlertBarComposer
{
    /**
     * @var PropertyOffers $propertyOfferModel
     * @var PropertyOfferService $propertyOfferService
     */
    public $propertyOfferModel;
    public $propertyOfferService;

    /**
     * Create a new notification instance.
     *
     * @param PropertyOffers $propertyOfferModel
     * @param PropertyOfferService $propertyOfferService
     */
    public function __construct(PropertyOffers $propertyOfferModel,PropertyOfferServiceContract $propertyOfferService)
    {
        $this->propertyOfferModel = $propertyOfferModel;
        $this->propertyOfferService = $propertyOfferService;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $tripperUnreadNotifications = getLoggedInRental()->unreadNotifications->whereIn('type',[
            OwnerAcceptedPropertyOfferNotification::class
        ]);

        $count = 0;
        if($tripperUnreadNotifications->count() > 0){

            $propertyOffersCount = $this->propertyOfferService->getPendingResponseOnOfferByTripper(getLoggedInRentalId());
            //$view->with('count',$tripperUnreadNotifications->count());
            $count = $propertyOffersCount;
        }

        $view->with('count',$count);
    }
}
