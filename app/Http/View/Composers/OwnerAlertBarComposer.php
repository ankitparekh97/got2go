<?php


namespace App\Http\View\Composers;


use App\Helpers\GlobalConstantDeclaration;
use App\Models\PropertyOffers;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnCounterPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferNotification;
use App\Notifications\PropertyOffer\Tripper\TripperMadeAnPropertyOfferOnExistingOfferNotification;
use App\Services\PropertyOfferService;
use App\Services\PropertyOfferServiceContract;
use Illuminate\View\View;

class OwnerAlertBarComposer
{
    /**
     * @var PropertyOffers $propertyOfferModel
     * @var PropertyOfferService $propertyOfferService
     */
    public $propertyOfferModel;
    public $propertyOfferService;

    /**
     * Create a new notification instance.
     *
     * @param PropertyOffers $propertyOfferModel
     * @param PropertyOfferService $propertyOfferService
     */
    public function __construct(PropertyOffers $propertyOfferModel,PropertyOfferServiceContract $propertyOfferService)
    {
        $this->propertyOfferModel = $propertyOfferModel;
        $this->propertyOfferService = $propertyOfferService;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $ownerUnreadNotifications = getOwner()->unreadNotifications->whereIn('type',[
            TripperMadeAnPropertyOfferNotification::class,
            TripperMadeAnPropertyOfferOnExistingOfferNotification::class,
            TripperMadeAnCounterPropertyOfferNotification::class
        ]);

        $count = 0;
        if($ownerUnreadNotifications->count() > 0){

            $propertyOffersCount = $this->propertyOfferService->getPendingResponseOnOfferByOwner(getOwnerId());
            //$view->with('count',$ownerUnreadNotifications->count());
            $count = $propertyOffersCount;
        }
        $view->with('count',$count);
    }
}
