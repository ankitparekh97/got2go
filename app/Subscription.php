<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    protected $fillable = [
        'annual_subscription', 'discount_percentage', 'rental_user_id', '# id, annual_subscription, discount_percentage, rental_user_id, created_at, updated_at'
    ];

  
}
