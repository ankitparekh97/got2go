<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\Image\Image;
use Illuminate\Support\Facades\Log;

class ImageOptimize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $properties = Property::with('propertyimages','propertyownershipdoc')->get();
        $supportedMediaType = ['image/jpeg','image/png', 'image/jpg'];
        if(count($properties) > 0){
            foreach($properties as $property){

                set_time_limit(20000);

                // cover photo
                $optimizerChain = OptimizerChainFactory::create();
                $cover_photo_path = public_path() .'/uploads/property/'.$property->cover_photo;
                if(File::exists($cover_photo_path)) {
                    $imgWidth = Image::load($cover_photo_path)->getWidth();
                    if($imgWidth > 1920){
                        Image::load($cover_photo_path)->width(1920)->save();
                    }
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize($cover_photo_path);
                }

                if(count($property->propertyimages) > 0){

                    // media
                    foreach($property->propertyimages as $images){
                        $image_path = public_path() .'/uploads/property/'.$images->photo;

                        if(!in_array($images->media_type,$supportedMediaType)){
                            continue;
                        }

                        if(File::exists($image_path)) {

                            $imgWidth = Image::load($image_path)->getWidth();
                            if($imgWidth > 1920){
                                Image::load($image_path)->width(1920)->save();
                            }

                            $originalSize = filesize($image_path);

                            // the image will be replaced with an optimized version which should be smaller
                            $optimizerChain = OptimizerChainFactory::create();
                            $optimizerChain->optimize($image_path);

                            clearstatcache();

                            // Check the optimized size
                            $optimizedSize = filesize($image_path);
                            $percentChange = (1 - $optimizedSize / $originalSize) * 100;
                            Log::info($percentChange);
                            Log::info($images->id);
                                                    }
                    }

                    // verfication docs
                    foreach($property->propertyownershipdoc as $images){
                        $verfication_image_path = public_path() .'/uploads/property/'.$images->document_name;
                        if(!in_array($images->media_type,$supportedMediaType)){
                            continue;
                        }
                        if(File::exists($verfication_image_path)) {

                           // the image will be replaced with an optimized version which should be smaller
                            $optimizerChain = OptimizerChainFactory::create();
                            $optimizerChain->optimize($verfication_image_path);
                        }
                    }
                }
            }

        }

        return 0;
    }
}
