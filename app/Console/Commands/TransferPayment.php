<?php

namespace App\Console\Commands;

use App\Models\RentalUser;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use Illuminate\Console\Command;
use App\Services\TransferPaymentServiceContract;
use App\Services\PaymentServices\Stripe\Transfer\TransferRequestDTO;
use App\Services\PaymentServices\Stripe\PaymentService;

class TransferPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TransferPayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will transfer payemnt to owner once check out done';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param TransferPaymentServiceContract $transferPaymentServiceContract
     * @return void
     */
    public function handle(
        TransferPaymentServiceContract $transferPaymentServiceContract
    )
    {

       $bookings = $transferPaymentServiceContract->makeTransfer(carbonGetYesterDay());
        \Log::info(print_r($bookings,true));

    }
}
