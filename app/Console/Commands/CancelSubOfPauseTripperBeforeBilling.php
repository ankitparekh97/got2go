<?php

namespace App\Console\Commands;

use App\Models\RentalUser;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use Illuminate\Console\Command;

class CancelSubOfPauseTripperBeforeBilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CancelSubOfPauseTripperBeforeBilling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will cancel subscription from payment gateway before next billing charge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param RentalInterface $rentalRepository
     * @return void
     */
    public function handle(
        RentalInterface $rentalRepository
    )
    {
        /**
         * @var RentalUser $rentalUsers
         */
        $rentalUsers = $rentalRepository->getRentalUserSubscriptionExpireByDate(
            carbonGetTomorrow()
        );

        if(!empty($rentalUsers)){
            foreach ($rentalUsers as $rentalUser){
                $rentalRepository->cancelPaymentGatewaySubscription($rentalUser->id);
            }
        }
    }
}
