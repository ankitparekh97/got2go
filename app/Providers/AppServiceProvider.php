<?php

namespace App\Providers;

use App\Models\PropertyOffers;
use App\Observers\PropertyOfferObserve;
use App\Repositories\AmenitiesInterface;
use App\Repositories\AmenitiesRepository;
use App\Repositories\BulkUploadInterface;
use App\Repositories\BulkUploadRepository;
use App\Repositories\CouponCodeInterface;
use App\Repositories\CouponCodeRepository;
use App\Repositories\HotelsInterface;
use App\Repositories\HotelsRepository;
use App\Repositories\PaymentCardInterface;
use App\Repositories\PaymentCardRepository;
use App\Repositories\PaymentInterface;
use App\Repositories\PaymentRepository;
use App\Repositories\PropertyOfferInterface;
use App\Repositories\PropertyOfferRepository;
use App\Repositories\RentalInterface;
use App\Repositories\RentalRepository;
use App\Repositories\MakeOfferInterface;
use App\Repositories\MakeOfferRepository;
use App\Repositories\OwnerInterface;
use App\Repositories\OwnerRepository;
use App\Repositories\PropertyInterface;
use App\Repositories\PropertyRepository;
use App\Repositories\PropertyTypeInterface;
use App\Repositories\PropertyTypeRepository;
use App\Repositories\RentalUserSubscriptionInterface;
use App\Repositories\RentalUserSubscriptionRepository;
use App\Repositories\SubscriptionPlanInterface;
use App\Repositories\SubscriptionPlanRepository;
use App\Services\CouponCodeContract;
use App\Services\CouponCodeService;
use App\Services\PaymentCardContract;
use App\Services\PaymentCardService;
use App\Services\PaymentServices\Braintree\BraintreePaymentService;
use App\Services\PaymentServices\PaymentServiceInterface;
use App\Services\PaymentServices\Stripe\PaymentService;
use App\Services\PropertyOfferService;
use App\Services\PropertyOfferServiceContract;
use App\Services\PropertyService;
use App\Services\PropertyServiceContract;
use App\Services\RentalUserService;
use App\Services\RentalUserServiceContract;
use App\Services\SubscriptionPlanContract;
use App\Services\SubscriptionPlanService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Services\TransferPaymentService;
use App\Services\TransferPaymentServiceContract;
use App\Services\Renter\PropertyDetailServiceContract;
use App\Services\Renter\PropertyDetailService;
use App\Services\Renter\PaymentbookingServiceContract;
use App\Services\Renter\PaymentbookingService;
use App\Repositories\ConfigurationInterface;
use App\Repositories\ConfigurationRepository;
use App\Repositories\FavrioteTripboardInterface;
use App\Repositories\FavrioteTripboardRepository;
use App\Repositories\SubscriptionInterface;
use App\Repositories\SubscriptionRepository;
use App\Repositories\BookingInterface;
use App\Repositories\BookingRepository;
use App\Repositories\GroupMessageInterface;
use App\Repositories\GroupMessageRepository;
use App\Repositories\PaymentSavingAccountsInterface;
use App\Repositories\PaymentSavingAccountsRepository;
use App\Services\BookingContract;
use App\Services\BookingService;
use App\Services\PaymentHistoryContract;
use App\Services\PaymentHistoryService;
use App\Repositories\SearchInterface;
use App\Repositories\SearchRepository;
use App\Services\SearchContract;
use App\Services\SearchService;
use App\Services\TripBoardContract;
use App\Services\TripBoardService;
use App\Repositories\TripBoardInterface;
use App\Repositories\TripBoardRepository;
use App\Repositories\BookingPaymentRepository;
use App\Repositories\BookingPaymentInterface;
use App\Repositories\MessagesInterface;
use App\Repositories\MessagesRepository;
use App\Repositories\OwnerRentalPairInterface;
use App\Repositories\OwnerRentalPairRepository;
use App\Repositories\PropertyAmenitiesInterface;
use App\Repositories\PropertyAmenitiesRepository;
use App\Repositories\PropertyAvablityInterface;
use App\Repositories\PropertyAvaiblitiesRepository;
use App\Repositories\PropertyBedroomsInterface;
use App\Repositories\PropertyBedroomsRepository;
use App\Repositories\PropertyDocumentsInterface;
use App\Repositories\PropertyDocumentsRepository;
use App\Repositories\PropertyImagesInterface;
use App\Repositories\PropertyImagesRepository;
use App\Services\SendMessageService;
use App\Services\SendMessageServiceContract;
use App\Services\Renter\FavoriteServiceContract;
use App\Services\Renter\FavoriteService;
use App\Services\Renter\TripperServiceContract;
use App\Services\Renter\TripperService;
use App\Services\Renter\SubscriptionServiceContract;
use App\Services\Renter\SubscriptionService;
use App\Services\TimeShareContract;
use App\Services\TimeShareService;
use App\Services\ServiceFeeContract;
use App\Services\ServiceFeeService;
use App\Services\Renter\ChatServiceContract;
use App\Services\Renter\ChatService;
use App\Services\BookingAvaibilityServiceContract;
use App\Services\BookingAvaibilityService;
use App\Services\LoginServiceContract;
use App\Services\LoginService;
use App\Repositories\AdminInterface;
use App\Repositories\AdminRepository;
use App\Repositories\BulkUploadLogsInterface;
use App\Repositories\BulkUploadLogsRepository;
use App\Services\RegisterServiceContract;
use App\Services\RegisterService;
use App\Repositories\CommunicationPreferencesInterface;
use App\Repositories\CommunicationPreferencesRepository;

use App\Services\TransactionLogsContract;
use App\Services\TransactionLogsService;

use App\Services\UserManagementContract;
use App\Services\UserManagementService;

use App\Repositories\PropertyOwnershipDocumentRepository;
use App\Repositories\PropertyOwnershipDocumentInterface;
use App\Repositories\TripBoardPropertyInterface;
use App\Repositories\TripBoardPropertyRepository;
use App\Repositories\TripBoardVoteInterface;
use App\Repositories\TripBoardVoteRepository;
use App\Repositories\TripBoardBuddyInterface;
use App\Repositories\TripBoardBuddyRepository;
use App\Repositories\TripBoardSharedInterface;
use App\Repositories\TripBoardSharedRepository;
use App\Repositories\GroupMessagesInterface;
use App\Repositories\GroupMessagesRepository;
use App\Services\BulkUploadService;
use App\Services\BulkUploadServiceContract;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            BraintreePaymentService::class,
            function() {
                return new BraintreePaymentService();
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        Schema::defaultStringLength(191);

        $this->app->bind(
            RentalUserServiceContract::class,
            RentalUserService::class
        );

        $this->app->bind(
            RentalInterface::class,
            RentalRepository::class
        );

        $this->app->bind(
            PaymentCardInterface::class,
            RentalRepository::class
        );
        $this->app->bind(

            RentalUserSubscriptionInterface::class,
            RentalRepository::class
        );

        $this->app->bind(
            PaymentCardContract::class,
            PaymentCardService::class
        );

        $this->app->bind(
            PaymentServiceInterface::class,
            PaymentService::class
        );

        $this->app->bind(
            MakeOfferInterface::class,
            MakeOfferRepository::class
        );

        $this->app->bind(
            OwnerInterface::class,
            OwnerRepository::class
        );

        $this->app->bind(
            PropertyOfferInterface::class,
            PropertyOfferRepository::class
        );

        $this->app->bind(
            PropertyOfferServiceContract::class,
            PropertyOfferService::class
        );

        $this->app->bind(
            TransferPaymentServiceContract::class,
            TransferPaymentService::class
        );

        $this->app->bind(
            SubscriptionPlanInterface::class,
            SubscriptionPlanRepository::class
        );

        $this->app->bind(
            SubscriptionPlanContract::class,
            SubscriptionPlanService::class
        );

        $this->app->bind(
            PaymentInterface::class,
            PaymentRepository::class
        );

        $this->app->bind(
            CouponCodeContract::class,
            CouponCodeService::class
        );

        $this->app->bind(
            CouponCodeInterface::class,
            CouponCodeRepository::class
        );

        $this->app->bind(
            PropertyServiceContract::class,
            PropertyService::class
        );

        $this->app->bind(
            PropertyInterface::class,
            PropertyRepository::class
        );

        $this->app->bind(
            BulkUploadInterface::class,
            BulkUploadRepository::class
        );

        $this->app->bind(
            PropertyTypeInterface::class,
            PropertyTypeRepository::class
        );

        $this->app->bind(
            AmenitiesInterface::class,
            AmenitiesRepository::class
        );

        $this->app->bind(
            HotelsInterface::class,
            HotelsRepository::class
        );

        $this->app->bind(
            ConfigurationInterface::class,
            ConfigurationRepository::class
        );

        $this->app->bind(
            FavrioteTripboardInterface::class,
            FavrioteTripboardRepository::class
        );

        $this->app->bind(
            TripBoardInterface::class,
            TripBoardRepository::class
        );

        $this->app->bind(
            TripBoardPropertyInterface::class,
            TripBoardPropertyRepository::class
        );

        $this->app->bind(
            TripBoardVoteInterface::class,
            TripBoardVoteRepository::class
        );

        $this->app->bind(
            TripBoardBuddyInterface::class,
            TripBoardBuddyRepository::class
        );

        $this->app->bind(
            TripBoardSharedInterface::class,
            TripBoardSharedRepository::class
        );

        $this->app->bind(
            TripBoardContract::class,
            TripBoardService::class
        );

        $this->app->bind(

            SubscriptionInterface::class,
            SubscriptionRepository::class
        );

        $this->app->bind(
            BookingInterface::class,
            BookingRepository::class
        );

        $this->app->bind(
            BookingContract::class,
            BookingService::class
        );

        $this->app->bind(
            OwnerRentalPairInterface::class,
            OwnerRentalPairRepository::class
        );

        $this->app->bind(
            PaymentbookingServiceContract::class,
            PaymentbookingService::class
        );

        $this->app->bind(
            SearchInterface::class,
            SearchRepository::class
        );

        $this->app->bind(
            BookingPaymentInterface::class,
            BookingPaymentRepository::class
        );

        $this->app->bind(
            MessagesInterface::class,
            MessagesRepository::class
        );

        $this->app->bind(
            GroupMessageInterface::class,
            GroupMessageRepository::class
        );

        $this->app->bind(
            BookingPaymentInterface::class,
            BookingPaymentRepository::class
        );

        $this->app->bind(
            PaymentHistoryContract::class,
            PaymentHistoryService::class
        );

        $this->app->bind(
            PaymentSavingAccountsInterface::class,
            PaymentSavingAccountsRepository::class
        );

        $this->app->bind(
            OwnerRentalPairInterface::class,
            OwnerRentalPairRepository::class
        );

        $this->app->bind(
            SendMessageServiceContract::class,
            SendMessageService::class
        );

        $this->app->bind(
            FavoriteServiceContract::class,
            FavoriteService::class
        );

        $this->app->bind(
            TripperServiceContract::class,
            TripperService::class
        );

        $this->app->bind(
            SubscriptionServiceContract::class,
            SubscriptionService::class
        );

        $this->app->bind(
            SearchContract::class,
            SearchService::class
        );

        $this->app->bind(
            TimeShareContract::class,
            TimeShareService::class

        );

        $this->app->bind(
            ServiceFeeContract::class,
            ServiceFeeService::class
        );

        $this->app->bind(
            PropertyImagesInterface::class,
            PropertyImagesRepository::class
        );

        $this->app->bind(
            PropertyBedroomsInterface::class,
            PropertyBedroomsRepository::class
        );

        $this->app->bind(
            PropertyAmenitiesInterface::class,
            PropertyAmenitiesRepository::class
        );

        $this->app->bind(
            PropertyAvablityInterface::class,
            PropertyAvaiblitiesRepository::class
        );

        $this->app->bind(
            PropertyDocumentsInterface::class,
            PropertyDocumentsRepository::class
        );


        $this->app->bind(
            TransactionLogsContract::class,
            TransactionLogsService::class
        );

        $this->app->bind(
            UserManagementContract::class,
            UserManagementService::class
        );

        $this->app->bind(
            PropertyOwnershipDocumentInterface::class,
            PropertyOwnershipDocumentRepository::class
        );

        $this->app->bind(
            ChatServiceContract::class,
            ChatService::class
        );

        $this->app->bind(
            BookingAvaibilityServiceContract::class,
            BookingAvaibilityService::class
        );

        $this->app->bind(
            LoginServiceContract::class,
            LoginService::class
        );

        $this->app->bind(
            AdminInterface::class,
            AdminRepository::class
        );

        $this->app->bind(
            RegisterServiceContract::class,
            RegisterService::class
        );

        $this->app->bind(
            CommunicationPreferencesInterface::class,
            CommunicationPreferencesRepository::class
        );

        $this->app->bind(
            GroupMessagesInterface::class,
            GroupMessagesRepository::class
        );

        $this->app->bind(
            BulkUploadLogsInterface::class,
            BulkUploadLogsRepository::class
        );

        $this->app->bind(
            PropertyDetailServiceContract::class,
            PropertyDetailService::class
        );

        $this->app->bind(
            BulkUploadServiceContract::class,
            BulkUploadService::class
        );

        ## This block is for Observer
        PropertyOffers::observe(PropertyOfferObserve::class);
        ## End of this block is for Observer
    }
}
