<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyReviews extends Model
{
    protected $fillable = [
        'owner_property_id', 'rating', 'reviews'
    ];

    public function ownerproperty()
    {
        return $this->belongsTo(OwnerProperty::class);
    }
}
