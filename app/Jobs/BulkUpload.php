<?php

namespace App\Jobs;

use App\Models\BulkUpload as AppBulkUpload;
use App\Models\BulkUploadLogs;
use App\Helpers\GlobalConstantDeclaration;
use App\Models\Hotels;
use App\Models\MasterAmenities;
use App\Models\Owner;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Models\Property;
use App\Models\PropertyAmenities;
use App\Models\PropertyAvaiblities;
use App\Models\PropertyBedrooms;
use App\Models\PropertyImages;
use App\Models\PropertyOwnershipDocuments;
use App\Models\PropertyType;
use App\Services\BulkUploadServiceContract;
use App\Services\PropertyServiceContract;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use ZipArchive;
use Illuminate\Support\Facades\File;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use DB;
use Exception;
use Throwable;

class BulkUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $storagePath;
    protected $getBulkRowId;
    protected $csvDataLogs = [];
    protected $ownerUser;
    protected $csvErrLogs = [];
    protected $csvErrMediaLogs = [];
    protected $csvErrVerificationLogs = [];
    protected $csvErrGovtIdLogs = [];
    protected $propertyCategory = array('property'=>'Private Residence','vacation_rental'=>'Vacation Club');
    protected $cancellationPolicy = array('Flexible','Moderate','Strict');
    protected $YesNo = array('YES','NO');
    protected $checkOut= array('9:00','9:30','10:00','10:30','11:00','11:30','12:00');
    protected $checkIn = array('11:00','11:30','12:00','12:30','1:00','1:30','2:00','2:30','3:00','3:30','4:00','4:30','5:00','5:30','6:00');
    protected $duration = array('AM','PM');
    protected $latlong;

    protected $masterAmenities = [];
    protected $propertyTypes = [];
    protected $excludeIds = [];
    protected $insertedIds = [];
    protected $mediaZipName;
    protected $verificationZipName;
    public $propertyServiceContract;
    public $bulkUploadServiceContract;


    public function __construct(
        PropertyServiceContract $propertyServiceContract,
        BulkUploadServiceContract $bulkUploadServiceContract,
        $bulkUploadId
    )
    {
        $this->propertyServiceContract = $propertyServiceContract;
        $this->bulkUploadServiceContract = $bulkUploadServiceContract;

        $this->getBulkRowId = $this->bulkUploadServiceContract->getBulkUploadByParamsObject(array('id' => $bulkUploadId));
        $this->ownerUser = $this->propertyServiceContract->getOwnerById($this->getBulkRowId->owner_id);

        $dir = 'app/bulkUpload/'. $this->getBulkRowId->unique_key .'/';
        $this->storagePath = storage_path($dir);
        $this->masterAmenities = array_map('strtolower', $this->propertyServiceContract->getMasterAmenitiesByArray());
        $this->propertyTypes = collect($this->propertyServiceContract->getPropertyTypesByParams());
        if($this->getBulkRowId->is_reupload == 1){
            $this->excludeIds = array_keys($this->propertyServiceContract->getPropertyByUniqueKey($this->getBulkRowId->unique_key));
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $isValid = $this->validateGovId();
        if(!$isValid){
            Log::info('gov id not found');
            $this->jobDone('completed');
            return false;
        }

        // MEdia
        $this->mediaZipName =  $this->getBulkRowId->property_images;
        $zip = new ZipArchive;

        $mediaRes = $zip->open($this->storagePath . $this->mediaZipName);
        if ($mediaRes === TRUE) {

            // extract zip
            $zip->extractTo($this->storagePath);
            $zip->close();
        }

        // Verification Zip
        $this->verificationZipName =  $this->getBulkRowId->verfication_doc;
        $res = $zip->open($this->storagePath . $this->verificationZipName);
        if ($res === TRUE) {

            // extract zip
            $zip->extractTo($this->storagePath);
            $zip->close();
        }

        $filename = $this->getBulkRowId->unique_key . '/' . $this->getBulkRowId->property_doc;
        $csv = $this->getBulkRowId->property_doc;

        if (Storage::disk('local')->exists('bulkUpload/'.$filename))
        {
            $handle = fopen($this->storagePath . $csv, 'r');
            $first_row = true;
            $final_ata = array();
            $headers = array();

            while (!feof($handle) ) {
            {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)

                    if($first_row) {
                        $headings = array(
                            'ID',
                            'PropertyName',
                            'Description',
                            'AboutTheSpace',
                            'StreetAddress',
                            'City',
                            'State',
                            'Zipcode',
                            'Availiblities',
                            'PropertyCategory',
                            'ResortName',
                            'PropertyType',
                            'CleaningFee',
                            'NightlyRate',
                            'CancellationPolicy',
                            'Guests',
                            'Bedrooms',
                            'Bathrooms',
                            'PrivateSpace',
                            'GuestsOnly',
                            'InstantBooking',
                            'PartialBooking',
                            'CheckInTime',
                            'CheckOutTime',
                            'TimeZone',
                            'Amenity1',
                            'Amenity2',
                            'Amenity3',
                            'Amenity4',
                            'Amenity5',
                            'Amenity6',
                            'Amenity7',
                            'Amenity8',
                            'Amenity9',
                            'Amenity10',
                            'Amenity11',
                            'Amenity12',
                        );

                        $headers = $headings;
                        $first_row = false;

                    } else {

                        if(!empty($data)){

                            try {

                                $refinedData = array_slice(array_values($data),0,count($headers));
                                $final_ata = array_combine($headers, $refinedData);
                            } catch (Throwable $exception){
                                if (count($headers) != count($data)) {
                                   Log::info($data);
                                   continue;
                                }

                                Log::error('Some issue with => ' . $exception->getMessage());

                                // job failed due to csv error
                                $this->jobDone(GlobalConstantDeclaration::STATUS_FAILED);
                                break 2;
                            }

                            if(count($final_ata)>0)
                            {
                                if(!in_array($final_ata['ID'],$this->excludeIds)){
                                    $this->addproperty($final_ata);
                                }
                            }

                        } else {
                            continue;
                        }
                    }
                }

                // update the status of the job and send email
                $this->jobDone(GlobalConstantDeclaration::STATUS_COMPLETED);
            }
            fclose($handle);
        }

    }

    private function addproperty($records)
    {
        if(count($records) > 0)
        {
            $isvalid = $this->validateFields($records);
            if($isvalid == false){
                return false;
            }

            $this->insertedIds[] = $records['ID'];
            $property = $this->bulkUploadServiceContract->bulkUploadJob(
                                                    $this->ownerUser->id,
                                                    $this->latlong[$records['ID']],
                                                    $this->getBulkRowId->unique_key,
                                                    $records
                                                );

            $this->bulkUploadServiceContract->bulkUploadPropertyAttributes($records,$property->id);

            // store logs for correct properties
            $this->csvDataLogs[] =  $property->id;
            Log::info('Property : '. $records['ID'] . ' uploaded');

            // Property Photos
            $this->addMedia($property);

            // Property Verification Docs
            $this->addVerificationDocs($property);

            // Govt Id Documents
            $this->addGovtIdDocs($property->id);

        } else {
            Log::info('Upload Done');
        }
    }

    private function validateGovId(){
        // Govt ID Zip
        $zipname =  $this->getBulkRowId->govtid_doc;

        $zip = new ZipArchive;
        $res = $zip->open($this->storagePath .$zipname);
        if( $res  === TRUE){

             // extract zip
             $zip->extractTo($this->storagePath);
             $zip->close();

              // get the filename witout extension
            $filename = pathinfo($zipname, PATHINFO_FILENAME);
            $govtIdStoragePath = $this->storagePath . $filename;
            $isGovtIdValid = array();

            $isExists = File::exists($govtIdStoragePath);
            if($isExists === true){

                // get list of all the directories and files from unzipped directory
                $files = File::allfiles($govtIdStoragePath);
                foreach($files as $file){

                    // check document type
                    if($file->getExtension() != 'doc' && $file->getExtension() != 'docx' && $file->getExtension() != 'xls' &&
                    $file->getExtension() != 'xlsx' && $file->getExtension() != 'csv' && $file->getExtension() != 'mp3' && $file->getExtension() != 'mp4'){
                        $isGovtIdValid[] = $file;
                    }
                }

                if(count($isGovtIdValid) == 0){
                    $this->csvErrGovtIdLogs['Govt'][] = 'Please resubmit a valid government-issued ID, in either PDF, PNG, or JPG format. <br><br>Please make sure to reupload and resubmit all the required Bulk Upload files to ensure a successful upload, not just your Government ID!.';
                }

            } else {
                $this->csvErrGovtIdLogs['Govt'][] = '"Please resubmit a valid government-issued ID, in either PDF, PNG, or JPG format. <br><br>Please make sure to reupload and resubmit all the required Bulk Upload files to ensure a successful upload, not just your Government ID!"';
            }

        } else {
            $this->csvErrGovtIdLogs['Govt'][] = '"There is an issue with Govt ID zip.';
        }

        return (count($this->csvErrGovtIdLogs) > 0) ? false : true;
    }

    private function validateFields($data){

        // ID
        if(trim($data['ID']) == null){
            $this->csvErrLogs[$data['ID']][] = 'Property ID is empty.';
            $this->csvErrLogs['InvalidPropertyID'][$data['ID']] = 'Property ID is empty.';
            return false;
        } else if(!preg_match("/^\d+$/",substr($data['ID'], 3)) || strlen(substr($data['ID'], 3)) != 5){
            $this->csvErrLogs[$data['ID']][] = 'Please be sure to submit a unique valid 5-digit Property ID# with each listing.';
            $this->csvErrLogs['InvalidPropertyID'][$data['ID']] = 'Please be sure to submit a unique valid 5-digit Property ID# with each listing.';
            return false;
        } else if(isset($this->csvErrLogs[$data['ID']]) || in_array($data['ID'],$this->insertedIds)){
            // Log::info($data['ID'].' Duplicate Already Exist!!');
            $this->csvErrLogs[$data['ID']]['CSV'][] = $data['ID'].' was submitted as a duplicate. Please review, fix, and reupload as needed.';
            return false;
        }

        // Property Name
        if(trim($data['PropertyName']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Name is empty.';
        } else if(strlen(trim($data['PropertyName'])) > 50){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property names cannot be longer than 50 characters.';
        }

        // Property Desciprtion
        if(trim($data['Description']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Description is empty';
        } else if(strlen(trim($data['Description'])) > 250){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property descriptions cannot be longer than 250 characters.';
        } else if(strlen(trim($data['Description'])) < 100){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property descriptions cannot be less than 100 characters.';
        }

        // About The Space
        if(trim($data['AboutTheSpace']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'About The Space is empty';
        } else if(strlen(trim($data['AboutTheSpace'])) > 250){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'The "About the Space" section cannot be longer than 250 characters.';
        } else if(strlen(trim($data['AboutTheSpace'])) < 100){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'The "About the Space" section cannot be less than 100 characters.';
        }

        // Street Address
        $geoCode = env("GEOCODE");
        if(trim($data['StreetAddress']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Street address is empty.';
        }

        // City
        if(trim($data['City']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'City is empty.';
        }

        // State
        if(trim($data['State']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'State is empty.';
        }

        // Zip code
        if(trim($data['Zipcode']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Zip code is empty.';
        }

        $this->getLocation($data,$geoCode);


         // Availiblities
         $isvalidAvailiblity = true;
         $property_available_array=[];
         if(trim($data['Availiblities'] != '')){
             $checkAlphanumeric = preg_match('/[A-Za-z_!"#$%&()*+\.\\:\;=?@^_]+/',trim($data['Availiblities']));
             if($checkAlphanumeric){
                 $isvalidAvailiblity = false;
             } else {

                 $propertyAvailiblities = ($data['Availiblities'] != '') ? explode(',',trim($data['Availiblities'])) : '';
                 if(count($propertyAvailiblities)> 0){
                     foreach($propertyAvailiblities as $key=>$val){

                         $availblty[$key] = explode(' - ',$val);
                         if(count($availblty[$key]) == 2){
                             $fromDate = trim($availblty[$key][0]);
                             $toDate = trim($availblty[$key][1]);
                             $date_now = date("m/d/Y");
                             // date format is not mm/dd/yyyy

                             if( count(explode('/',$fromDate)) == 3 && count(explode('/',$toDate)) == 3)
                             {
                                 // date format is not mm/dd/yyyy
                                 list($mm,$dd,$yyyy) = explode('/',$fromDate);
                                 if (!checkdate($mm,$dd,$yyyy)) {
                                     $isvalidAvailiblity = false;
                                 }

                                 // date format is not mm/dd/yyyy
                                 list($mm,$dd,$yyyy) = explode('/',$toDate);
                                 if (!checkdate($mm,$dd,$yyyy)) {
                                     $isvalidAvailiblity = false;
                                 }

                             } else {
                                 $isvalidAvailiblity = false;
                             }

                             // if date is past
                             if(strtotime($date_now) > strtotime($fromDate)){
                                 $isvalidAvailiblity = false;
                             }

                             // from date greater than to date
                             if(strtotime($toDate) < strtotime($fromDate)){
                                 $isvalidAvailiblity = false;
                             }

                             $property_available_array[] = array(trim($fromDate),trim($toDate));
                         } else {
                             $isvalidAvailiblity = false;
                         }
                     }

                 } else {
                     $isvalidAvailiblity = false;
                 }
             }

         } else {
             $this->csvErrLogs[$data['ID']]['CSV'][] = 'Availabilities field is empty.';
         }

         if($isvalidAvailiblity == false){
             $this->csvErrLogs[$data['ID']]['CSV'][] = "Please be sure to follow the correct format for entering in availabilities. All availabilities must contain check-in and check-out dates in the format MM/DD/YYYY and be separated by a dash (-). Each availability needs to have a comma (,) in between for it to be recorded. <br><br>Example: 11/02/2020 - 11/15/2020, 11/20/2020 - 01/21/2021, 03/05/2021 - 04/01/2021. <br><br>Please be sure all availabilities don't include any past dates!";
         } else {

             foreach($property_available_array as $key=>$val){

                 $fromDate = date('Y-m-d',strtotime($val[0]));
                 $toDate = date('Y-m-d',strtotime($val[1]));

                 foreach($property_available_array as $key2=>$val2){
                     if($key != $key2)
                     {
                         $fromDate2 = date('Y-m-d',strtotime($val2[0]));
                         $toDate2 = date('Y-m-d',strtotime($val2[1]));

                         if ( (($fromDate >= $fromDate2) && ($fromDate <= $toDate2)) || (($toDate >= $fromDate2) && ($toDate <= $toDate2))){
                             $this->csvErrLogs[$data['ID']]['CSV'][] = 'One or more of your availabilties are overlapping with dates. ';
                             break 2;
                         }
                     }

                 }

             }
         }

        // Property Category
        if(trim($data['PropertyCategory']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Category is empty.';
        } else if(!in_array(trim($data['PropertyCategory']),$this->propertyCategory)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Category is not valid.';
        }

        // Resort
        if(trim($data['ResortName'])== null && $data['PropertyCategory'] == 'Vacation Club') {
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Resort Name is empty.';
        }

        // Property Type
        if(trim($data['PropertyType']) != null){
            $property_type = trim($data['PropertyType']);

            $propertyType = $this->propertyTypes->pluck('type_of_property','property_type')->toArray();
            $type_of_property = (isset($propertyType[strtolower($property_type)])) ? $propertyType[strtolower($property_type)] : null;

            //PropertyType::whereRaw("binary property_type = '".$property_type."' ")->first();
            if($type_of_property){
                $property_category = $this->propertyCategory[$type_of_property];
                if($property_category != $data['PropertyCategory']){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Type does not belong to specified Property Category.';
                }
            } else {
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Type is not valid.';
            }

        } else {
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Property Type is empty.';
        }


        // Cleaning Fee
        $cleaningFee = preg_replace("/[^0-9\.]/", "", trim($data['CleaningFee']));
        if(trim($data['CleaningFee']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Cleaning Fee is empty.';
        } else if(!preg_match("/^\d+$/",$cleaningFee) || ($cleaningFee > 10000)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please enter in a valid cleaning fee that is only an integer value, and less than $10,000.';
        }

        // Nightly Rate
        $nightlyPrice = preg_replace("/[^0-9\.]/", "", trim($data['NightlyRate']));
        if(trim($data['NightlyRate']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Nightly Rate is empty.';
        } else if(!preg_match("/^\d+$/",$nightlyPrice) || ($nightlyPrice > 3000)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please enter in a valid nightly rate that is only an integer value, and less than $3,000.';
        }

        // Cancellation Policy
        if(trim($data['CancellationPolicy']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Cancellation Policy is empty.';
        } else if(!in_array(trim($data['CancellationPolicy']),$this->cancellationPolicy)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Cancellation Policy is not valid.';
        }

        // # Guests
        $guests = trim($data['Guests']);
        if(trim($data['Guests']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Guests is empty.';
        } else if(!preg_match('/^[1-9]\d*$/',$guests) || ($guests > 30)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please enter a valid # of guests as an integer value less than 30.';
        }

        // # Bedrooms
        $bedrooms = trim($data['Bedrooms']);
        if(trim($data['Bedrooms']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Bedrooms is empty.';
        } else if(!preg_match('/^[1-9]\d*$/',$bedrooms) || ($bedrooms > 30)){
             $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please enter a valid # of bedrooms as an integer value less than 30.';
        }

        // # Bathrooms
        $bathooms = trim($data['Bathrooms']);
        if(trim($data['Bathrooms']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Bathrooms is empty.';
        } else if(!preg_match('/^[1-9]\d*$/',$bathooms) || ($bathooms > 30)){
             $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please enter a valid # of bathrooms as an integer value less than 30.';
        }

        // Private Space
        if(trim($data['PrivateSpace']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Private Space is empty.';
        } else if(!in_array(trim($data['PrivateSpace']),$this->YesNo)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = '"Private Space?" must have a "YES" or "NO" value.';
        }

        // Guests Only
        if(trim($data['GuestsOnly']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Guests Only is empty.';
        } else if(!in_array(trim($data['GuestsOnly']),$this->YesNo)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = '"Guests Only?" must have a "YES" or "NO" value.';
        }

        // Instant Booking
        if(trim($data['InstantBooking']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Instant Booking is empty.';
        } else if(!in_array(trim($data['InstantBooking']),$this->YesNo)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = '"Instant Booking?" must have a "YES" or "NO" value.';
        }

         // Partial Booking
        if(trim($data['PartialBooking']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Partial Booking is empty.';
        } else if(!in_array(trim($data['PartialBooking']),$this->YesNo)){
            $this->csvErrLogs[$data['ID']]['CSV'][] = '"Partial Booking Allowed?" must have a "YES" or "NO" value.';
        }

        // CheckIn Time
        if(trim($data['CheckInTime']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-In Time is empty.';
        } else {

            $checkinTime = explode(' ', trim($data['CheckInTime']));
            if(count($checkinTime) == 2){
                $inTime = $checkinTime[0];
                $induration = $checkinTime[1];

                if(!in_array($inTime,$this->checkIn)){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-in times must be a time selected from the dropdown.';
                } else if(!in_array($induration,$this->duration)){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-in times must be a time selected from the dropdown.';
                } else {

                    if(($inTime == '1:00' || $inTime == "2:00" ||
                        $inTime == "3:00" || $inTime == "4:00" ||
                        $inTime == "5:00" || $inTime == "6:00") && ($induration == "AM")){
                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                    } else if(($inTime == '12:00' || $inTime == '12:30' || $inTime == '1:30' || $inTime == "2:30" ||
                               $inTime == "3:30" || $inTime == "4:30" || $inTime == "5:30") && ($induration == "AM")){
                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                    }
                }

            } else {
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'check-in time is not valid.';
            }
        }


        // CheckOut Time
        if(trim($data['CheckOutTime']) == null){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-Out Time is empty.';
        } else {

            $checkoutTime = explode(' ', trim($data['CheckOutTime']));
            if(count($checkinTime) == 2 && count($checkoutTime) == 2){
                $inTime = $checkinTime[0];
                $induration = $checkinTime[1];

                $outTime = $checkoutTime[0];
                $outduration = $checkoutTime[1];

                if(!in_array($outTime,$this->checkOut)){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                } else if(!in_array($outduration,$this->duration)){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                } else {

                    // past check-in time
                    if(($inTime == '11:00' && $induration == 'AM') &&
                        (($outTime == '11:00' && $outduration == 'AM') ||
                        ($outTime == '11:30' && $outduration == 'AM') ||
                        ($outTime == '12:00' && $outduration == 'PM')) ){

                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please make sure that the check-in time is past the check-out time.';

                    } else if(($inTime == '11:30' && $induration == 'AM') &&
                            (($outTime == '11:30' && $outduration == 'AM') ||
                            ($outTime == '12:00' && $outduration == 'PM')) ){

                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please make sure that the check-in time is past the check-out time.';
                    } else if($inTime == '12:00' && $induration == 'PM' && $outTime == '12:00' && $outduration == "PM"){
                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Please make sure that the check-in time is past the check-out time.';
                    }

                    // check out
                    if(($outTime == '9:00' || $outTime == "10:00" || $outTime == "11:00" ||
                        $outTime == '9:30' || $outTime == '10:30' || $outTime == "11:30") && ($outduration == "PM")){
                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                    } else if($outTime == '12:00' && $outduration == "AM"){
                        $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
                    }
                }

            } else {
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'Check-out times must be a time selected from the dropdown.';
            }
        }


        // Amenities
        $amenities = array( $data['Amenity1'],$data['Amenity2'],$data['Amenity3'],$data['Amenity4'],$data['Amenity5'],
                            $data['Amenity6'],$data['Amenity7'],$data['Amenity8'],$data['Amenity9'],$data['Amenity10'],
                            $data['Amenity11'],$data['Amenity12']
                         );

        $amenitiesArray = array_filter($amenities, function($v){
            return trim($v);
        });

        if(count($amenitiesArray) == 0){
            $countAmenities[] = array();
        } else {
            foreach($amenitiesArray as $amenity){
                if($amenity!=''){
                    $chckAmenities = array_search(strtolower($amenity),$this->masterAmenities);
                    if($chckAmenities){
                        $countAmenities[] = $chckAmenities;
                    } else {
                        $errAmenities[] = $amenity;
                    }
                }
            }
        }

        if(count($amenitiesArray) == 0){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Amenities are empty.';
        } else if(count($countAmenities) < 6){
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'Minimum of 6 valid amenities (selected from the dropdown) are required.';
            if(!empty($errAmenities)){
                foreach($errAmenities as $erramenity){
                    $this->csvErrLogs[$data['ID']]['CSV'][] = 'Amenity "'.$erramenity.'" is not valid. Please select an amenity from the dropdown.';
                }
            }
        }

        // MEdia
        // get the filename without extension
        $filename = pathinfo($this->mediaZipName, PATHINFO_FILENAME);
        $storagePath = $this->storagePath . $filename;
        $propertyIDfolder = $storagePath . '/' . $data['ID'];

        // get list of all the directories and files from unzipped directory
        $isExists = File::exists($propertyIDfolder);
        if($isExists === true){
            $files = File::allfiles($propertyIDfolder);

            $isMediaValid = array();
            $isMediaInValid = array();
            $isMediaInValidDimension = array();
            foreach($files as $file){

                // check document type
                if($file->getExtension() != 'pdf' && $file->getExtension() != 'doc' && $file->getExtension() != 'docx'
                    && $file->getExtension() != 'xlsx' && $file->getExtension() != 'csv' && $file->getExtension() != 'mp3'){

                        // get image dimensions
                        list($width, $height) = getimagesize($file->getRealPath());
                        if(($height < 683 || $width < 1024) && $file->getExtension() != 'mp4'){
                            $isMediaInValidDimension[] = $file->getFilename();
                        } else {
                            $isMediaValid[] = $file;
                        }

                } else {
                    $isMediaInValid[] = $file->getFilename();
                }

            }

            if(count($isMediaValid) < 6){
                $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'There need to be at least 6 property images for each listing.';

                if(count($isMediaInValid) > 0){
                    $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'One or more of your property images were rejected due to an incorrect file format. Please make sure only PNG or JPG images (and MP4 for video files) are uploaded with your listings.';
                    // foreach($isMediaInValid as $invalidFile){
                    //     $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'One or more of your property images were rejected due to an incorrect file format. Please make sure only PNG or JPG images (and MP4 for video files) are uploaded with your listings.';
                    // }
                }

                if(count($isMediaInValidDimension) > 0){
                    $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'One or more of your images are too small. Please make sure the minimum dimensions for each image are 1024x683px.';
                    // foreach($isMediaInValidDimension as $invalidFileD){
                    //     $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'One or more of your images are too large. Please make sure the minimum dimensions for each image are 1024x683px.';
                    // }
                }
            }

        } else {
            if(!preg_match("/^\d+$/",substr($data['ID'], 3)) || strlen(substr($data['ID'], 3)) != 5){
                return false;
            }

            $this->csvErrMediaLogs[$data['ID']]['Media'][] = 'There were no property images found for this listing. Please upload a valid ZIP file with at least 6 images, with the minimum dimensions of 1024x683px.';
        }

        // Verification Zip
        // get the filename without extension
        $filename = pathinfo($this->verificationZipName, PATHINFO_FILENAME);
        $storagePath = $this->storagePath . $filename;
        $folder = $storagePath . '/' .$data['ID'];

        // get list of all the directories and files from unzipped directory
        $isExists = File::exists($folder);
        if($isExists === true){
            $files = File::allfiles($folder);

            $isVerficationValid = array();
            foreach($files as $file){

                // check document type
                if($file->getExtension() != 'doc' && $file->getExtension() != 'docx' && $file->getExtension() != 'xls' && $file->getExtension() != 'xlsx'
                    && $file->getExtension() != 'csv' && $file->getExtension() != 'mp3' && $file->getExtension() != 'mp4'){
                    $isVerficationValid[] = $file;
                }
            }

            if(count($isVerficationValid) == 0){
                $this->csvErrVerificationLogs[$data['ID']]['Verification'][] = 'Please make sure all documents uploaded are either in PDF, PNG,or JPG format. ';
            }

        } else {
            if(!preg_match("/^\d+$/",substr($data['ID'], 3)) || strlen(substr($data['ID'], 3)) != 5){
                return false;
            }

            $this->csvErrVerificationLogs[$data['ID']]['Verification'][] = 'There were no verification documents found for this listing.';
        }


        // log all errors
        $countErr = 0;
        if(isset($this->csvErrLogs[$data['ID']])){
            $countErr = count($this->csvErrLogs[$data['ID']]);
        }

        if(isset($this->csvErrMediaLogs[$data['ID']])) {
            $countErr = count($this->csvErrMediaLogs[$data['ID']]);
        }

        if(isset($this->csvErrVerificationLogs[$data['ID']])) {
            $countErr = count($this->csvErrVerificationLogs[$data['ID']]);
        }

        if(isset($this->csvErrGovtIdLogs['Govt'])) {
            $countErr = count($this->csvErrGovtIdLogs['Govt']);
        }

        return ($countErr > 0) ? false : true;
    }

    private function addMedia($property){
        $zipname =  $this->getBulkRowId->property_images;

        // get the filename wihtout extension
        $filename = pathinfo($zipname, PATHINFO_FILENAME);
        $storagePath = $this->storagePath . $filename;
        $folder = $storagePath . '/' .$property->bulk_upload_id;

        $this->bulkUploadServiceContract->bulkUploadMedia($folder,$property);
    }

    private function addVerificationDocs($property){
        $zipname =  $this->getBulkRowId->verfication_doc;

        // get the filename without extension
        $filename = pathinfo($zipname, PATHINFO_FILENAME);
        $storagePath = $this->storagePath . $filename;
        $folder = $storagePath . '/' .$property->bulk_upload_id;

        $this->bulkUploadServiceContract->bulkUploadDocs($folder,$property);
    }

    private function addGovtIdDocs($propertyId){
        $zipname =  $this->getBulkRowId->govtid_doc;

        // get the filename witout extension
        $filename = pathinfo($zipname, PATHINFO_FILENAME);
        $folder = $this->storagePath . $filename;

        $this->bulkUploadServiceContract->bulkUploadGovtDocs($folder,$propertyId);
    }

    private function getLocation($data,$key){

        $address = $data['StreetAddress'] . ',' . $data['City']. ',' . $data['State']. ',' . $data['Zipcode'];
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&key=".$key;

        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);

        $state = '';
        $city = '';
        $zipcode = '';
        $route = '';
        $isValidAddress = true;
        $lat = '';
        $lang = '';
        if(!empty($result['results']) && $result['status'] != 'ZERO_RESULTS')
        {
            $result1[]=$result['results'][0];
            $lat = $result1[0]['geometry']['location']['lat'];
            $lang = $result1[0]['geometry']['location']['lng'];

            $address_components = $result1[0]['address_components'];
            foreach($address_components as $component){
                $addressType = $component['types'][0];

                if($addressType == "locality"){
                    $city = $component['long_name'];
                }
                else if($addressType == "administrative_area_level_1"){
                    if($city == ''){
                        $city = $component['long_name'];
                    }
                    $state = $component['short_name'];
                } else if($addressType == "postal_code"){
                    $zipcode = $component['long_name'];
                } else if($addressType == "route"){
                    $route = $component['long_name'];
                }
            }

            if($route == ''){
                $isValidAddress = false;
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this street address. Please re-enter.';
            }

            if($city != $data['City']){
                $isValidAddress = false;
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this city. Please re-enter.';
            }

            if($state != $data['State']){
                $isValidAddress = false;
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this state abbreviation. Please re-enter.';
            }

            if($zipcode != $data['Zipcode']){
                $isValidAddress = false;
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this zip code. Please re-enter.';
            }

            if($isValidAddress == true){
                $this->latlong[$data['ID']] = $lat . ',' . $lang;
                return true;
            } else {
                $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this location with the street address, city, state, and zip code provided. Please re-submit.';
                return false;
            }

        } else {

            $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this street address. Please re-enter.';
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this city. Please re-enter.';
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this state abbreviation. Please re-enter.';
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this zip code. Please re-enter.';
            $this->csvErrLogs[$data['ID']]['CSV'][] = 'We did not recognize this location with the street address, city, state, and zip code provided. Please re-submit.';
            return false;
        }
    }

    private function jobDone($status){

        try {

            Log::info($this->csvErrLogs);
            Log::info($this->csvErrMediaLogs);
            Log::info($this->csvErrVerificationLogs);
            Log::info($this->csvErrGovtIdLogs);

            $this->bulkUploadServiceContract->bulkUploadComplete(
                        $this->csvDataLogs,
                        $this->csvErrLogs,
                        $this->csvErrMediaLogs,
                        $this->csvErrVerificationLogs,
                        $this->csvErrGovtIdLogs,
                        $this->getBulkRowId->id,
                        $status
            );

        } catch (Throwable $exception){

            Log::error('Job failed due to ' . $exception->getMessage());
            $this->bulkUploadServiceContract->bulkUploadComplete(null,null,null,null,null,$this->getBulkRowId->id,'failed');
        }

        // email
        $toMail = $this->ownerUser['email'];
        $toName =  $this->ownerUser['first_name'].' '.$this->ownerUser['last_name'];
        $emailRecord = DB::table('email_template')->where('task','bulk_upload_notification')->first();
        $body = $emailRecord->message;
        $body = str_replace('{break}', '<br/>', $body);
        $body = str_replace('{footer_year}', date('Y'), $body);
        $body = str_replace('{site_url}', getLiveSiteUrl(), $body);
        $body = str_replace('{site_name}', 'Timeshare', $body);
        $body = str_replace('{logo_url}', 'Timeshare' , $body);
        $body = str_replace('{owner}', $this->ownerUser['first_name'].' '.$this->ownerUser['last_name'], $body);

        // send_email($body,$toMail,$toName,$emailRecord->subject,$emailRecord->from_address);

        // delete zip and csv once job gets completed
        if(count($this->csvDataLogs) > 0 && count($this->csvErrLogs) == 0 && count($this->csvErrMediaLogs) == 0 && count($this->csvErrVerificationLogs) == 0 && count($this->csvErrGovtIdLogs) == 0){
            Storage::disk('local')->deleteDirectory('bulkUpload/' .$this->getBulkRowId->unique_key);
        }
    }

    public function failed(Throwable $exception)
    {
        Log::error('Failed Process => ' . $exception->getMessage());

        // Send user notification of failure, etc...
        $this->jobDone(GlobalConstantDeclaration::STATUS_FAILED);
    }
}
